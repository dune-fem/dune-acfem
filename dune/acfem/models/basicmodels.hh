#ifndef __DUNE_ACFEM_MODULES_BASICMODELS_HH__
#define __DUNE_ACFEM_MODULES_BASICMODELS_HH__

#include "ostream.hh"

#include "modules/bulkloadmodel.hh"
#include "modules/deformationtensormodel.hh"
#include "modules/dirichletmodel.hh"
#include "modules/divergenceloadmodel.hh"
#include "modules/divergencemodel.hh"
#include "modules/fluidselftransportmodel.hh"
#include "modules/gradientloadmodel.hh"
#include "modules/gradientmodel.hh"
#include "modules/incompressibleselftransportmodel.hh"
#include "modules/incompressibletransportmodel.hh"
#include "modules/laplacianmodel.hh"
#include "modules/massmodel.hh"
#include "modules/meancurvaturemodel.hh"
#include "modules/neumannmodel.hh"
#include "modules/nitschedirichletmodel.hh"
#include "modules/plaplacianmodel.hh"
#include "modules/pmassmodel.hh"
#include "modules/robinmodel.hh"
#include "modules/transportmodel.hh"
#include "modules/weakdivergenceloadmodel.hh"
#include "modules/weakdirichletmodel.hh"
#include "modules/zeromodel.hh"

#include "modelfacade.hh"

// this probably is of no use without the expression template, so just
// inlcude them also for convenience
#include "expressions.hh"

// for operator* wrapping a FieldVector into a quadrature-point
#include "../common/quadraturepoint.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup PDE-Models
     * @{
     */

    /**@addtogroup BasicModels Model Building Blocks
     *
     * Basic PDE-Models which can be used to conveniently form more
     * complicated models by means of ModelExpressions. Provided you
     * already have some Fem::DiscreteFunction U at hand, a
     * grid-function F for the bulk-forces and a grid-function G for
     * Dirichlet boundary values, then you may some diffusion-reaction
     * model in the following way:
     *
     * @code
       auto bc = dirichletBoundaryModel(G);
       auto myModel = laplaceModel(U, "-Delta_u") + 2.0 * massModel(U) - F + bc;
       @endcode
     *
     * The resulting `myModel` will then model the following boundary
     * value problem:
     * @f[
     * \begin{split}
     * -\Delta\,U + 2\,U & = F\text{ in }\Omega\\
     * U &= G\text{ on }\partial\Omega.
     * \end{split}
     * @f]
     *
     * \note It is of course possible to form more complicated models,
     * have different kind of boundary conditions on parts of the
     * boundary and define non linear models. In addition, it is --
     * also intentionally -- ever possible to simply code your
     * most-precious model in the most-efficient way **without**
     * expression templates by following the layout prescribe by
     * ModelInterface. Our believe is that efficiency is not an issue
     * here, but **flexibility** is. If something cannot be formed
     * conveniently from the BasicModels, then feel free to do
     * "hand-work".
     *
     * \see ModelInterface, EllipticOperator, EllipticFemScheme,
     * ParabolicFemScheme, ModelExpressions.
     *
     * @{
     */

    /**@defgroup BulkModels Building Blocks for the Bulk-Phase
     *
     * Define some "germs" for the bulk-contributions for
     * differentiable operators.
     *
     * List of defined models:
     *
     * - ZeroModel, dummy model primarily meant for expression templates.
     * - MassModel
     * - LaplacianModel
     * - P_MassModel
     * - P_LaplacianModel
     * - TransportModel
     * - IncompressibleTransportModel
     * - DeformationTensorModel
     * - IncompressibleSelfTransportModel
     * - FluidSelfTransportModel
     * - DivergenceModel
     * - GradientModel
     * - MeanCurvatureModel
     */

    /**@defgroup RightHandSideModels Building Blocks for the Driving Forces
     *
     * Some models for the "right hand side", L2 "bulk forces" and
     * DiscreteLinearFunctionals. The two basic models defined here
     * are
     *
     * - BulkForcesFunctionModel in order to add an L2-function to the
     *   right hand side.
     *
     * - ForcesFunctionalModel in order to add a globally defined functional.
     *
     * The most simple way to add these models is to use
     * ModelExpression templates, see
     *
     * - operator-(const ModelInterface<Model>&, const Fem::Function<typename Model::FunctionSpaceType, GridFunction>&)
     * - operator-(const ModelInterface<Model>&, const DiscreteLinearFunctional<DiscreteFunctionSpace, Traits>&).
     *
     * like follows, given an existing model and grid-function F:
     * @code

       MyGridFunctionObject F;
       auto newModel = existingModel - F;

       @endcode
     */

    /**@defgroup BoundaryValueModels Building Blocks for Boundary Conditions
     *
     * \brief Models for some common-case boundary conditions.
     *
     * List of defined models:
     *
     * - DirichletBoundaryModel
     * - RobinBoundaryModel
     * - NeumannBoundaryModel
     * - ConstantRobinModel
     */

    /**@defgroup ModelGenerators Generate Model-Building-Blocks Conveniently
     *
     * Some utility function in order to conveniently define some
     * standard models without having to go through the "typedef"
     * trouble.
     */

    //@} BasicModels

    //@} PDE-Models

  } // namespace ACFem

}  //Namespace Dune

#endif // __DUNE_ACFEM_BASIC_MODELS_HH__
