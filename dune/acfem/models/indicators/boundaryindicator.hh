#ifndef __DUNE_ACFEM_INDICATORS_BOUNDARYINDICATOR_HH__
#define __DUNE_ACFEM_INDICATORS_BOUNDARYINDICATOR_HH__

#include "../../common/tostring.hh"
#include "../../expressions/expressionoperations.hh"
#include "../../expressions/storage.hh"
#include "../../expressions/interface.hh"
#include "../../expressions/terminal.hh"
#include "../../expressions/traitsdefault.hh"
#include "../../expressions/optimization.hh"

namespace Dune {

  namespace ACFem {

    namespace BoundaryIndicator {

      using namespace Literals;
      using Expressions::Storage;
      using Expressions::operate;

      /**@addtogroup PDE-Models
       *@{
       */

      /**@addtogroup BoundaryIndicators
       *
       * see BoundaryIndicatorInterface
       *
       * @{
       */

      /**Boundary indicators need to inherit this tag-class in order
       * to signal that they are boundary indicators. Boundary
       * indicators need also to model the expresion storage interface
       * as modelled by Expressions::Storage.
       */
      struct BoundaryIndicator
      {};

      /**@c TrueType if T is a BoundaryIndicator.
       *
       * @note The implementation avoids && as we overload operator&&
       * for boundary indicators.
       */
      template<class T>
      struct IsIndicator
        : BoolConstant<IsBaseOfDecay<BoundaryIndicator, T>::value>
      {};

      template<class T>
      struct IsProperOperand
        : BoolConstant<IsIndicator<T>::value && !Expressions::IsPromotedTopLevel<T>::value>
      {};

      //!@internal Default FalseType.
      template<class T0, class T1>
      struct AreProperOperands
        : BoolConstant<IsProperOperand<T0>::value && IsProperOperand<T1>::value>
      {};

      /**BoundaryIndicators do not need a closure.*/
      template<class T, std::enable_if_t<(IsIndicator<T>::value && !Expressions::IsClosure<T>::value), int> = 0>
      constexpr decltype(auto) expressionClosure(T&& t)
      {
        return std::forward<T>(t);
      }

      template<bool Answer = true>
      struct Constant;

      template<>
      struct Constant<true>
        : public Expressions::SelfExpression<Constant<true> >
        , public BoundaryIndicator
        , public OneExpression
      {
       private:
        enum { answer = true };
       public:

        template<class Intersection>
        bool applies(const Intersection &intersection) const
        {
          return answer;
        }

        static std::string name()
        {
          return "\\Chi[true]";
        }
      };

      template<>
      struct Constant<false>
        : public Expressions::SelfExpression<Constant<false> >
        , public BoundaryIndicator
        , public ZeroExpression
      {
       private:
        enum { answer = false };
       public:

        template<class Intersection>
        bool applies(const Intersection &intersection) const
        {
          return answer;
        }

        static std::string name()
        {
          return "\\Chi[false]";
        }
      };

      /**Define the canonical zero object.*/
      template<class F, class T>
      constexpr auto zero(F&&, T&&)
      {
        return Constant<false>{};
      }

      /**Define the canonical zero object for product operations.*/
      template<class T0, class T1>
      constexpr auto zero(OperationTraits<LogicalAndOperation>, T0&&, T1&&)
      {
        return Constant<false>{};
      }

      /**Define the canonical one object.*/
      template<class T>
      constexpr auto one(T&&)
      {
        return Constant<true>{};
      }

      /**Paraphrase isOne and isZero to indicator function talk.*/
      template<class T>
      struct IndicatorTraits
      {
        static constexpr bool globalSupport = ExpressionTraits<T>::isOne;
        static constexpr bool emptySupport = ExpressionTraits<T>::isZero;
      };

      //! Apply to boundary segments which carry the respective id
      struct BoundaryIdIndicator
        : public Expressions::SelfExpression<BoundaryIdIndicator>
        , public BoundaryIndicator
        , public SemiPositiveExpression
      {
        //! Construct the indicator with the given id.
        BoundaryIdIndicator(int id)
          : id_(id)
        {};

        /**@copydoc BoundaryIndicatorInterface::applies() */
        template<class Intersection>
        bool applies(const Intersection &intersection) const
        {
          return intersection.impl().boundaryId() == id_;
        }

        std::string name() const
        {
          return "\\Chi["+toString(id_)+"]";
        }
       protected:
        const int id_;
      };

      //!Turn any boundary-indicator into its complement.
      template<class Indicator>
      struct Complement
        : public Expressions::Storage<OperationTraits<LogicalNotOperation>, Indicator>
        , public BoundaryIndicator
        , public std::conditional_t<ExpressionTraits<Indicator>::isZero,
                                    OneExpression,
                                    std::conditional_t<ExpressionTraits<Indicator>::isOne,
                                                       ZeroExpression,
                                                       SemiPositiveExpression> >
      {
        using StorageType = Expressions::Storage<OperationTraits<LogicalNotOperation>, Indicator>;
        using StorageType::operand;
        using StorageType::operation;

        Complement(Indicator&& indicator)
          : StorageType(OperationTraits<LogicalNotOperation>{}, std::forward<Indicator>(indicator))
        {}

        /**@copydoc BoundaryIndicatorInterface::applies() */
        template<class Intersection>
        bool applies(const Intersection &intersection) const
        {
          return !operand(0_c).applies(intersection);
        }

        std::string name() const
        {
          std::string pfx = std::is_reference<Indicator>::value ? (RefersConst<Indicator>::value ? "cref" : "ref") : "";
          return operationName(operation(), pfx+operand(0_c).name());
        }
      };

      //!Union of two indicators, apply to the union of both boundary parts.
      template<class T0, class T1>
      struct UnionIndicator
        : public Expressions::Storage<OperationTraits<LogicalOrOperation>, T0, T1>
        , public BoundaryIndicator
        , public std::conditional_t<ExpressionTraits<T0>::isOne || ExpressionTraits<T1>::isOne,
                                    OneExpression,
                                    std::conditional_t<ExpressionTraits<T0>::isZero && ExpressionTraits<T1>::isZero,
                                                       ZeroExpression,
                                                       SemiPositiveExpression> >
      {
        using StorageType = Expressions::Storage<OperationTraits<LogicalOrOperation>, T0, T1>;
        using StorageType::operand;
        using StorageType::operation;

        UnionIndicator(T0&& t0, T1&& t1)
          : StorageType(OperationTraits<LogicalOrOperation>{}, std::forward<T0>(t0), std::forward<T1>(t1))
        {}

        /**@copydoc BoundaryIndicatorInterface::applies() */
        template<class Intersection>
        bool applies(const Intersection &intersection) const
        {
          return operand(0_c).applies(intersection) || operand(1_c).applies(intersection);
        }

        std::string name() const
        {
          std::string pfxL = std::is_reference<T0>::value ? (RefersConst<T0>::value ? "cref" : "ref") : "";
          std::string pfxR = std::is_reference<T1>::value ? (RefersConst<T1>::value ? "cref" : "ref") : "";

          return operationName(operation(), pfxL+operand(0_c).name(), pfxR+operand(1_c).name());
        }
      };

      //!Intersection of two indicators, apply iff both apply.
      template<class T0, class T1>
      struct IntersectionIndicator
        : public Expressions::Storage<OperationTraits<LogicalAndOperation>, T0, T1>
        , public BoundaryIndicator
        , public std::conditional_t<ExpressionTraits<T0>::isZero || ExpressionTraits<T1>::isZero,
                                    ZeroExpression,
                                    std::conditional_t<ExpressionTraits<T0>::isOne && ExpressionTraits<T1>::isOne,
                                                       OneExpression,
                                                       SemiPositiveExpression> >
      {
        using StorageType = Expressions::Storage<OperationTraits<LogicalAndOperation>, T0, T1>;
        using StorageType::operand;
        using StorageType::operation;

        IntersectionIndicator(T0&& t0, T1&& t1)
          : StorageType(OperationTraits<LogicalAndOperation>{}, std::forward<T0>(t0), std::forward<T1>(t1))
        {}

        template<class Intersection>
        bool applies(const Intersection &intersection) const
        {
          return operand(0_c).applies(intersection) && operand(1_c).applies(intersection);
        }

        std::string name() const
        {
          std::string pfxL = std::is_reference<T0>::value ? (RefersConst<T0>::value ? "cref" : "ref") : "";
          std::string pfxR = std::is_reference<T1>::value ? (RefersConst<T1>::value ? "cref" : "ref") : "";

          return operationName(operation(), pfxL+operand(0_c).name(), pfxR+operand(1_c).name());
        }
      };

      //!@} BoundaryIndicators

      //!@} PDE-Models

      /**@addtogroup ExpressionTemplates
       *
       * @{
       */

      /**@addtogroup IndicatorExpressions
       *
       * Boolean expression for indicators, corresponding to the
       * usual union, intersection, complement set operations of the
       * boundary segments the indicator applies to.
       *
       * @{
       */

      //!Boolean negation, take the complement
      template<class T, std::enable_if_t<IsProperOperand<T>::value, int> = 0>
      constexpr auto operator!(T&& t)
      {
        return operate<LogicalNotOperation>(std::forward<T>(t));
      }

      //!@internal Optimization end-point.
      template<class T, std::enable_if_t<IsProperOperand<T>::value, int> = 0>
      constexpr auto operate(Expressions::DontOptimize, OperationTraits<LogicalNotOperation>, T&& t)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        return Complement<T>(std::forward<T>(t));
      }

      //!Unary - is like !
      template<class T, std::enable_if_t<IsProperOperand<T>::value, int> = 0>
      auto operator-(T&& t)
      {
        return !std::forward<T>(t);
      }

      //!Union of two indicators
      template<class T0, class T1, std::enable_if_t<AreProperOperands<T0, T1>::value, int> = 0>
      auto operator||(T0&& t0, T1&& t1)
      {
        return operate<LogicalOrOperation>(std::forward<T0>(t0), std::forward<T1>(t1));
      }

      template<class T0, class T1, std::enable_if_t<AreProperOperands<T0, T1>::value, int> = 0>
      constexpr auto operate(Expressions::DontOptimize, OperationTraits<LogicalOrOperation>, T0&& t0, T1&& t1)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        return UnionIndicator<T0, T1>(std::forward<T0>(t0), std::forward<T1>(t1));
      }

      //!Union of two indicators
      template<class T0, class T1, std::enable_if_t<AreProperOperands<T0, T1>::value, int> = 0>
      auto operator+(T0&& t0, T1&& t1)
      {
        return std::forward<T0>(t0) || std::forward<T1>(t1);
      }

      //!Intersection of two indicators
      template<class T0, class T1, std::enable_if_t<AreProperOperands<T0, T1>::value, int> = 0>
      auto operator&&(T0&& t0, T1&& t1)
      {
        return operate<LogicalAndOperation>(std::forward<T0>(t0), std::forward<T1>(t1));
      }

      //!@internal Optimization end-point.
      template<class T0, class T1, std::enable_if_t<AreProperOperands<T0, T1>::value, int> = 0>
      constexpr auto operate(Expressions::DontOptimize, OperationTraits<LogicalAndOperation>, T0&& t0, T1&& t1)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        return IntersectionIndicator<T0, T1>(std::forward<T0>(t0), std::forward<T1>(t1));
      }

      //!Intersection of two indicators
      template<class T0, class T1, std::enable_if_t<AreProperOperands<T0, T1>::value, int> = 0>
      auto operator*(T0&& t0, T1&& t1)
      {
        return std::forward<T0>(t0) && std::forward<T1>(t1);
      }

      //! Difference in the sense of @f$A \setminus B@f$.
      template<class T0, class T1, std::enable_if_t<AreProperOperands<T0, T1>::value, int> = 0>
      auto operator-(T0&& t0, T1&& t1)
      {
        return std::forward<T0>(t0) && !std::forward<T1>(t1);
      }

      //!@} IndicatorExpressions

      //!@} ExpressionTemplates

    } // NS BoundaryIndicator

    //!A boundary indicator applying to no part of the boundary.
    using EmptyBoundaryIndicator = BoundaryIndicator::Constant<false>;

    //!A boundary indicator applying to all parts of the boundary.
    using EntireBoundaryIndicator = BoundaryIndicator::Constant<true>;

    //!Identify a boundary indicator
    template<class T>
    using IsBoundaryIndicator = BoundaryIndicator::IsIndicator<T>;

    //!Indentify a boundary indicator after type-normalization
    template<class T>
    using IsProperBoundaryIndicator = BoundaryIndicator::IsProperOperand<T>;

    //!Export BoundaryIdIndicator
    using BoundaryIndicator::BoundaryIdIndicator;

    //!
    using BoundaryIndicator::IndicatorTraits;

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_INDICATORS_BOUNDARYINDICATOR_HH__
