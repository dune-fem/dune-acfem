#ifndef __DUNE_ACFEM_MODELS_OPERATIONS_ASSUME_HH__
#define __DUNE_ACFEM_MODELS_OPERATIONS_ASSUME_HH__

#include "../expressiontraits.hh"
#include "../expressionoperations.hh"

namespace Dune {

  namespace ACFem {

    namespace PDEModel {

      /**copydoc LoadModel*/
      template<class... Property, class Model,
               std::enable_if_t<IsProperPDEModel<Model>::value, int>  = 0>
      auto assume(Model&& m, MPL::TagContainer<Property...> = MPL::TagContainer<Property...>{})
      {
        if constexpr (!std::is_same<MPL::TagContainer<Property...>, MPL::UniqueTags<Property...> >::value) {
          return assume(std::forward<Model>(m), MPL::UniqueTags<Property...>{});
        } else {
          return Expressions::finalize<AssumeOperation<Property...> >(std::forward<Model>(m));
        }
      }

    } // NS PDEModel

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_MODELS_OPERATIONS_ASSUME_HH__
