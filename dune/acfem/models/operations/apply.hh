#ifndef __DUNE_ACFEM_MODELS_OPERATIONS_APPLY_HH__
#define __DUNE_ACFEM_MODELS_OPERATIONS_APPLY_HH__

#include "../applyexpression.hh"
#include "../expressiontraits.hh"
#include "../expressionoperations.hh"

namespace Dune {

  namespace ACFem {

    namespace PDEModel {

      /**copydoc LoadModel*/
      template<class Flavour, class Model, class Function,
               std::enable_if_t<(IsProperPDEModel<Model>::value
                                 && !Expressions::IsPromotedTopLevel<Function>::value
        ), int>  = 0>
      auto apply(Model&& m, Function&& f, Flavour = Flavour{})
      {
        return Expressions::finalize<ApplyOperation<Flavour> >(std::forward<Model>(m), std::forward<Function>(f));
      }

      /**copydoc LoadModelExpression*/
      template<class Model, class Function>
      constexpr decltype(auto) loadModel(Model&& m, Function&& f)
      {
        return apply<L2LoadFlavour>(std::forward<Model>(m), std::forward<Function>(f));
      }

      /**copydoc LoadModel*/
      template<class Model, class Function>
      constexpr decltype(auto) ritzLoadModel(Model&& m, Function&& f)
      {
        return apply<RitzLoadFlavour>(std::forward<Model>(m), std::forward<Function>(f));
      }

    } // NS PDEModel

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_MODELS_OPERATIONS_APPLY_HH__
