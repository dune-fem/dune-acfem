#ifndef __DUNE_ACFEM_MODELS_OPERATIONS_CLOSURE_HH__
#define __DUNE_ACFEM_MODELS_OPERATIONS_CLOSURE_HH__

#include "../unaryexpression.hh"
#include "../expressiontraits.hh"

namespace Dune {

  namespace ACFem {

    namespace PDEModel {

      using namespace ModelIntrospection;

      namespace {

        /**@internal Functor in order to add the requested
         * call-signatures to the call-signature index-sequence finally
         * fed into UnaryModelExpression.
         */
        template<class MethodTags,
                 class ClosureCallSignatures,
                 class ModelClosureCallSignatures>
        struct ClosureSignatureFunctor
        {
          static_assert(MethodTags::size() == ClosureCallSignatures::size(),
                        "Number of method-tags and call-signatures must coincide");

          /**@internal Add the arguments corresponding to the set bits
           * in ClosureCallSignatures. The mask argument is intentially
           * ignored as it is already contained in
           * ModelClosureCallSignatures which also take into account the
           * default implementation of non-linear methods by the
           * linearized methods.
           */
          template<class T, T mask, std::size_t Tag>
          using Apply = IndexConstant<((Get<Tag, ModelClosureCallSignatures>::value
                                        |
                                        Get<Tag, ClosureCallSignatures>::value)
                                       &
                                       ModelIntrospection::MethodSignatureClosure<Get<Tag, MethodTags>::value>::value)>;
        };

        template<class Model, class ClosureCallSignatures, std::size_t... Tag>
        using ModelClosureSignatures = TransformOnly<
          ClosureSignatureFunctor<
            IndexSequence<Tag...>,
            ClosureCallSignatures,
            IndexSequence<ModelMethodSignatureClosure<Model, Tag>::value...> >,
          typename ModelTraits<Model>::MethodCallSignatures,
          Tag...>;

      }

      /**Add the arguments designated by the bits set in the elements of
       * ClosureCallSignatures to the respective methods, designated by
       * the "method-tags" designated by Tag. If the closure of a
       * non-linear method is requested, its default implementation by
       * the respective "linearized" method has to be taken into
       * account.
       *
       * The functions creates a wrapped model in any case, i.e. the
       * result will be some UnaryModelExpression<IdentityOperation,...>
       * in order to provide a well defined access to the structure of
       * the underlying model. Successive call of already "closured"
       * models will be folded in to a single closure,
       * decltype(closure(model,...)::ModelType is alway the original
       * model.
       */
      template<std::size_t... MethodTag, class T,
               class ClosureCallSignatures = IndexSequence<MethodSignatureClosure<MethodTag>::value...>,
               std::enable_if_t<IsPDEModel<T>::value && !Expressions::IsPromotedTopLevel<T>::value, int> = 0>
      constexpr decltype(auto) closure(T&& t,
                                       IndexSequence<MethodTag...> methods = IndexSequence<MethodTag...>{},
                                       ClosureCallSignatures = ClosureCallSignatures{})
      {
        using Operation = ClosureOperation<IndexSequence<MethodTag...>, ClosureCallSignatures>;
        return Expressions::operate<Operation>(std::forward<T>(t));
      }

      //!@internal Optimization end-point which actually just generates the requested expression.
      template<class T, std::size_t... MethodTag, class ClosureCallSignatures>
      constexpr auto operate(Expressions::DontOptimize, OperationTraits<ClosureOperation<IndexSequence<MethodTag...>, ClosureCallSignatures> >, const T& t)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        using Functor = OperationTraits<ClosureOperation<IndexSequence<MethodTag...>, ClosureCallSignatures> >;

        return UnaryModelExpression<
          Functor, T,
          MaskSequence<(ModelTraits<T>::methodsMask | SequenceMask<MethodTag...>::value)>,
          ModelClosureSignatures<T, ClosureCallSignatures, MethodTag...> >(
            t
            );
      }

      /**Variant which folds nested closure-calls into one.
       */
      template<class T, std::size_t... MethodTag, class ClosureCallSignatures,
               std::enable_if_t<IsClosureExpression<T>::value, int> = 0>
      constexpr auto operate(Expressions::OptimizeFurther, OperationTraits<ClosureOperation<IndexSequence<MethodTag...>, ClosureCallSignatures> >, T&& t)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        // simply pass the original model, but add the additional
        // closure requests to the one already present in the closure defined by Model.
        using Methods = MaskSequence<(ModelTraits<T>::methodsMask | SequenceMask<MethodTag...>::value)>;
        using Signatures = SequenceSlice<
          ModelClosureSignatures<T, ClosureCallSignatures, MethodTag...>,
          Methods>;
        using Operation = ClosureOperation<Methods, Signatures>;
        return operate<Operation>(std::forward<T>(t).operand(0_c));
      }

    } // NS PDEModel

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_MODELS_OPERATIONS_CLOSURE_HH__
