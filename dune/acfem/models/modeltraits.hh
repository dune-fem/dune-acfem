#ifndef __DUNE_ACFEM_MODELTRAITS_HH__
#define __DUNE_ACFEM_MODELTRAITS_HH__

#include "../mpl/mpl.hh"
#include "../common/quadraturepoint.hh"
#include "../expressions/traitsdefault.hh"

#include "modelbase.hh"

namespace Dune {

  namespace ACFem {

    /**Deduce methods and call-signatures from an implemented model.
     */
    namespace ModelIntrospection {

      /**An enum in order to parametrize the call-signature
       * introspection. One value for each method. Finally all
       * introspections are defined in one call by expanding an
       * index-sequence.
       */
      enum MethodTag {
        flux, linearizedFlux,
        source, linearizedSource,
        robinFlux, linearizedRobinFlux,
        singularFlux, linearizedSingularFlux,
        dirichlet, linearizedDirichlet,
        fluxDivergence,
        numMethodTags,
      };

      /**Wrap into integral_constant in order to have a type. Check
       * also the range of the supplied value. We use std::size_t as
       * template arguments as you cannot (without compiler warning)
       * do arithmetic with enum values.
       */
      template<MethodTag tag>
      using MethodTagConstant = Constant<MethodTag, tag>;

      /**Alias, used where we want to express that we do check the
       * value.
       */
      template<std::size_t tag>
      using CheckMethodTag = MethodTagConstant<(MethodTag)tag>;

      /**Evaluate to std::true_type for tags corresponding to linear methods.*/
      template<std::size_t tag>
      using IsLinearMethodTag = BoolConstant<CheckMethodTag<tag>::value & 1>;

      /**Compute the tag value of the respective linearized
       * method. Act as identity if the tag already corresponds to a
       * linearized method.
       */
      template<std::size_t tag>
      using LinearizedTag = CheckMethodTag<(
        IsLinearMethodTag<tag>::value
        ? tag
        : (tag == fluxDivergence
           ? linearizedFlux
           : tag+1))>;

      /**Compute the tag value of the respective non-linear
       * method. Act as identity if the tag already corresponds to a
       * non-linear method.
       */
      template<std::size_t tag>
      using NonLinearTag = CheckMethodTag<(IsLinearMethodTag<tag>::value ? (tag-1) : tag == fluxDivergence ? flux : tag)>;

      /**Sequence alias for method-tags. */
      template<MethodTag... Tags>
      using MethodTagSequence = Sequence<MethodTag, Tags...>;

      /**Sequence of all supported methods.*/
      using AllMethodTags = MakeIndexSequence<numMethodTags>;

      namespace {
        template<std::size_t methods>
        struct NonLinearClosureFunctor
        {
          template<class T, T tag, std::size_t N>
          using Apply = BoolConstant<
            (!IsLinearMethodTag<tag>::value
             &&
             (hasBit(methods, tag) || hasBit(methods, LinearizedTag<tag>::value)))>;
        };
      }

      /**Compute a bit-mask corresponding to the "closure" of
       * non-linear methods. This means a bit is set if either the
       * non-linear method or the linearized method is implemented as
       * the latter is the default implementation for the non-linear
       * method.
       */
      template<std::size_t methods>
      using NonLinearMethodsClosure =
        IndexConstant<sequenceMask(FilteredSequence<NonLinearClosureFunctor<methods>, AllMethodTags>{})>;

      /**Positions of the respective arguments inside the argument
       * closure tuple.
       */
      enum ArgumentPositions {
        linearizationValueIndex = 0, linearizationJacobianIndex,
        pointIndex, normalIndex, valueIndex, jacobianIndex, hessianIndex,
        maxNumArguments
      };

      /**Integral constant holding an argument mask.*/
      template<std::size_t mask>
      class ArgumentMask
        : public IndexConstant<mask>
      {
        static_assert((mask & (~(std::size_t)0 << maxNumArguments)) == 0,
                      "Unsupported argument mask");
        using BaseType = IndexConstant<mask>;
        static constexpr std::size_t linArgsMask =
          SequenceMask<linearizationValueIndex, linearizationJacobianIndex>::value;
       public:
        using BaseType::value;
        using typename BaseType::value_type;

        //!Default constructor
        ArgumentMask() {}

        //!Allow implicit initialization from base type.
        ArgumentMask(const BaseType&) {}

        //!The part of the mask referring to the point of linearization.
        using LinearizationSignature = IndexConstant<value & linArgsMask> ;

        //!The part of the mask referring to the point of linearization.
        static constexpr value_type linearizationSignature()
        {
          return LinearizationSignature::value;
        }

        /**The part of the mask referring to the arguments except the
         * point of linearization.
         */
        using Signature = IndexConstant<value & ~linArgsMask>;

        /**The part of the mask referring to the arguments except the
         * point of linearization.
         */
        static constexpr value_type signature()
        {
          return Signature::value;
        }
      };

      namespace {

        struct TraitsHelper
        {
         protected:
#if DUNE_ACFEM_IS_CLANG(0, 4) // seemingly 5 does not need it.
# warning Clang < 5 will very likely not work.
#endif
          template<class Model>
          using DomainFieldType = typename Model::FunctionSpaceType::DomainFieldType;

          template<class Model>
          using DomainType = typename Model::FunctionSpaceType::DomainType;

          template<class Model>
          using RangeFieldType = typename Model::FunctionSpaceType::RangeFieldType;
          template<class Model>
          using DomainRangeFieldType = typename Model::DomainFunctionSpaceType::RangeFieldType;
          template<class Model>
          using RangeRangeFieldType = typename Model::RangeFunctionSpaceType::RangeFieldType;

          template<class Model>
          using RangeType = typename Model::FunctionSpaceType::RangeType;
          template<class Model>
          using DomainRangeType = typename Model::DomainFunctionSpaceType::RangeType;
          template<class Model>
          using RangeRangeType = typename Model::RangeFunctionSpaceType::RangeType;

          template<class Model>
          using JacobianRangeType = typename Model::FunctionSpaceType::JacobianRangeType;
          template<class Model>
          using DomainJacobianRangeType = typename Model::DomainFunctionSpaceType::JacobianRangeType;
          template<class Model>
          using RangeJacobianRangeType = typename Model::RangeFunctionSpaceType::JacobianRangeType;

          template<class Model>
          using HessianRangeType = typename Model::HessianRangeType;
          template<class Model>
          using RangeHessianRangeType = typename Model::RangeHessianRangeType;
          template<class Model>
          using DomainHessianRangeType = typename Model::DomainHessianRangeType;

          /**@internal A tuple with all possible arguments, following
           * type-defs witll extract sub-tuples from this one. ATM we do
           * not support ommitting the DomainType but we already
           * incorporate it. Also, though the hessian is needed but the
           * flux-divergence it will not be used in the sub-types, the
           * call-pattern for the flux-divergence is deduced from the
           * call-pattern for the flux.
           */
          template<class Model, class Quadrature = PointWrapperQuadrature<DomainType<Model> > >
          using AllArguments = std::tuple<DomainRangeType<Model>,
                                          DomainJacobianRangeType<Model>,
                                          QuadraturePoint<Quadrature>,
                                          DomainType<Model>,
                                          DomainRangeType<Model>,
                                          DomainJacobianRangeType<Model>,
                                          DomainHessianRangeType<Model> >;

          /**A tuple with the return types in the order specified by
           * the  MethodTag enum.
           */
          template<class Model>
          using MethodReturnType =
            std::tuple<JacobianRangeType<Model>, // flux
                       JacobianRangeType<Model>,
                       RangeType<Model>, // source
                       RangeType<Model>,
                       RangeType<Model>, // robinFlux
                       RangeType<Model>,
                       JacobianRangeType<Model>, // singularFlux
                       JacobianRangeType<Model>,
                       RangeType<Model>, // dirichlet
                       RangeType<Model>,
                       RangeType<Model> // fluxDivergence
                       >;

          /**DomainType equals DomainRangeType which may lead to ambiguouties. */
          template<class Model>
          using DomainEqRange = std::is_same<DomainType<Model>, DomainRangeType<Model> >;

          /**A struct defining all supported call-signatures for each
           * method-tag. This is the placeholder which needs to be
           * specialized "by hand".
           */
          template<MethodTag tag, class dummy = void>
          struct SupportedCallSignatures;

         public:
          /**Type alias extracting the index_sequence holding all allowed
           * call-signatures for the given tag.
           */
          template<MethodTag tag>
          using MethodSignatures = typename SupportedCallSignatures<tag>::Type;

          /**Type alias extracting the "closure" of all supported
           * call-signatures.
           */
          template<MethodTag tag>
          using MethodSignatureClosure = IndexConstant<SupportedCallSignatures<tag>::closure>;

         protected:
          /**@name NonLinearCallSignatures
           *
           * These need to be defined "by hand".
           *
           * @{
           */

          template<class dummy>
          struct SupportedCallSignatures<flux, dummy>
          {
            static constexpr std::size_t closure = SequenceMask<pointIndex, valueIndex, jacobianIndex>::value;
            using Type = SubMaskSequence<closure>;
            static_assert(closure == AccumulateSequence<BitwiseOrFunctor, Type>::value,
                          "Implementation error: closure does not close");
          };

          template<class dummy>
          struct SupportedCallSignatures<fluxDivergence, dummy>
          {
            static constexpr std::size_t closure = SequenceMask<pointIndex, valueIndex, jacobianIndex, hessianIndex>::value;
            using Type = SubMaskSequence<closure>;
            static_assert(closure == AccumulateSequence<BitwiseOrFunctor, Type>::value,
                          "Implementation error: closure does not close");
          };

          template<class dummy>
          struct SupportedCallSignatures<source, dummy>
          {
            static constexpr std::size_t closure = SequenceMask<pointIndex, valueIndex, jacobianIndex>::value;
            using Type = SubMaskSequence<closure>;
            static_assert(closure == AccumulateSequence<BitwiseOrFunctor, Type>::value,
                          "Implementation error: closure does not close");
          };

          template<class dummy>
          struct SupportedCallSignatures<robinFlux, dummy>
          {
            // normal has always to be there in order to avoid ambiguouties
            static constexpr std::size_t fixed = SequenceMask<normalIndex>::value;
            static constexpr std::size_t varying = SequenceMask<pointIndex, valueIndex, jacobianIndex>::value;
            static constexpr std::size_t closure = fixed|varying;
            using Type = SubMaskSequence<varying, fixed>;
            static_assert(closure == AccumulateSequence<BitwiseOrFunctor, Type>::value,
                          "Implementation error: closure does not close");
          };

          template<class dummy>
          struct SupportedCallSignatures<singularFlux, dummy>
          {
            // normal has always to be there in order to avoid ambiguouties
            static constexpr std::size_t fixed = SequenceMask<normalIndex>::value;
            static constexpr std::size_t varying = SequenceMask<pointIndex, valueIndex, jacobianIndex>::value;
            static constexpr std::size_t closure = fixed|varying;
            using Type = SubMaskSequence<varying, fixed>;
            static_assert(closure == AccumulateSequence<BitwiseOrFunctor, Type>::value,
                          "Implementation error: closure does not close");
          };

          template<class dummy>
          struct SupportedCallSignatures<dirichlet, dummy>
          {
            static constexpr std::size_t closure = SequenceMask<pointIndex, valueIndex>::value;
            using Type = SubMaskSequence<closure>;
          };

          /**@}*/

          /**@name LinerarizedCallSignatures
           *
           * Automatically computed call-signatures for the linearizations.
           *
           * @{
           */

          /**Define the tag for the linearized method. */
          template<MethodTag tag>
          using LinearizedMethodTag = std::integral_constant<std::enable_if_t<tag != fluxDivergence, MethodTag>, tag+1>;

          /**The default implementation is the automation for the
           * linearization. As all non-linearized terms have a
           * specialization this one here simply defines the rest. We
           * compute from the call-pattern of the non-linear method all
           * patterns which depend optionally on the values and/or the
           * jacobians of the point of linearization.
           */
          template<MethodTag tag, class dummy>
          struct SupportedCallSignatures
          {
            static_assert(IsLinearMethodTag<tag>::value,
                          "Implementation failure: non-linear call-signatures not specialized.");
            typedef
            CatTransformUnique<
              AddBitShiftRightFunctor<valueIndex, valueIndex>,
              CatTransformUnique<
                AddBitShiftRightFunctor<jacobianIndex, valueIndex>,
                MethodSignatures<NonLinearTag<tag>::value> > >
            Type;
            static constexpr std::size_t closure = AccumulateSequence<BitwiseOrFunctor, Type>::value;
          };

          /**@}*/

          struct GetSignatureClosure
          {
            template<class T, T tag, std::size_t N>
            using Apply = MethodSignatureClosure<CheckMethodTag<tag>::value>;
          };

         public:
          /**A sequence with the closure patterns for each method.*/
          using MethodSignaturesClosureType = TransformedSequence<GetSignatureClosure, AllMethodTags>;

         protected:
          /*********************************************************************
           *
           * Individual method-introspection proto-below, one
           * packer-expander pair for each method.
           *
           */

          /**Introspection helper for expansion of tuple into parameter pack. */
          template<class Model, class Tuple, std::size_t... I>
          static
          decltype(std::declval<Model>().flux(std::declval<const decltype(std::get<I>(std::declval<Tuple>()))&>()...))
          fluxHelper(Model&&, Tuple&&, std::index_sequence<I...>);

          /**Introspection method used when model defines the method. */
          template<class Model, std::size_t ArgMaskValue>
          static std::true_type methodIntrospection(const Model&, const MethodTagConstant<flux>&, const ArgumentMask<ArgMaskValue>&,
                                                    decltype(fluxHelper(std::declval<const Model&>(),
                                                                        std::declval<const AllArguments<Model> >(),
                                                                        MaskSequence<ArgMaskValue>())) * = nullptr);

          /**@copydoc fluxHelper(). */
          template<class Model, class Tuple, std::size_t... I>
          static
          decltype(std::declval<Model>().linearizedFlux(std::declval<const decltype(std::get<I>(std::declval<Tuple>()))&>()...))
            linearizedFluxHelper(Model&&, Tuple&&, std::index_sequence<I...>);

          /**@copydoc flux(). */
          template<class Model, std::size_t ArgMaskValue>
          static std::true_type methodIntrospection(const Model&, const MethodTagConstant<linearizedFlux>&, const ArgumentMask<ArgMaskValue>&,
                                                    decltype(linearizedFluxHelper(std::declval<const Model&>(),
                                                                                  std::declval<const AllArguments<Model> >(),
                                                                                  MaskSequence<ArgMaskValue>())) * = nullptr);

          /**@copydoc fluxHelper(). */
          template<class Model, class Tuple, std::size_t... I>
          static
          decltype(std::declval<Model>().fluxDivergence(std::declval<const decltype(std::get<I>(std::declval<Tuple>()))&>()...))
          fluxDivergenceHelper(Model&&, Tuple&&, std::index_sequence<I...>);

          /**@copydoc flux(). */
          template<class Model, std::size_t ArgMaskValue>
          static std::true_type methodIntrospection(const Model&, const MethodTagConstant<fluxDivergence>&, const ArgumentMask<ArgMaskValue>&,
                                                    decltype(fluxDivergenceHelper(std::declval<const Model&>(),
                                                                                  std::declval<const AllArguments<Model> >(),
                                                                                  MaskSequence<ArgMaskValue>())) * = nullptr);

          /**@copydoc fluxHelper(). */
          template<class Model, class Tuple, std::size_t... I>
          static
          decltype(std::declval<Model>().source(std::declval<const decltype(std::get<I>(std::declval<Tuple>()))&>()...))
            sourceHelper(Model&&, Tuple&&, std::index_sequence<I...>);

          /**@copydoc flux(). */
          template<class Model, std::size_t ArgMaskValue>
          static std::true_type methodIntrospection(const Model&, const MethodTagConstant<source>&, const ArgumentMask<ArgMaskValue>&,
                                                    decltype(sourceHelper(std::declval<const Model&>(),
                                                                          std::declval<const AllArguments<Model> >(),
                                                                          MaskSequence<ArgMaskValue>())) * = nullptr);

          /**@copydoc fluxHelper(). */
          template<class Model, class Tuple, std::size_t... I>
          static
          decltype(std::declval<Model>().linearizedSource(std::declval<const decltype(std::get<I>(std::declval<Tuple>()))&>()...))
            linearizedSourceHelper(Model&&, Tuple&&, std::index_sequence<I...>);

          /**@copydoc flux(). */
          template<class Model, std::size_t ArgMaskValue>
          static std::true_type methodIntrospection(const Model&, const MethodTagConstant<linearizedSource>&, const ArgumentMask<ArgMaskValue>&,
                                                    decltype(linearizedSourceHelper(std::declval<const Model&>(),
                                                                                    std::declval<const AllArguments<Model> >(),
                                                                                    MaskSequence<ArgMaskValue>())) * = nullptr);

          /**@copydoc fluxHelper(). */
          template<class Model, class Tuple, std::size_t... I>
          static
          decltype(std::declval<Model>().robinFlux(std::declval<const decltype(std::get<I>(std::declval<Tuple>()))&>()...))
            robinFluxHelper(Model&&, Tuple&&, std::index_sequence<I...>);

          /**@copydoc flux(). */
          template<class Model, std::size_t ArgMaskValue>
          static std::true_type methodIntrospection(const Model&, const MethodTagConstant<robinFlux>&, const ArgumentMask<ArgMaskValue>&,
                                                    decltype(robinFluxHelper(std::declval<const Model&>(),
                                                                             std::declval<const AllArguments<Model> >(),
                                                                             MaskSequence<ArgMaskValue>())) * = nullptr);

          /**@copydoc fluxHelper(). */
          template<class Model, class Tuple, std::size_t... I>
          static
          decltype(std::declval<Model>().linearizedRobinFlux(std::declval<const decltype(std::get<I>(std::declval<Tuple>()))&>()...))
            linearizedRobinFluxHelper(Model&&, Tuple&&, std::index_sequence<I...>);

          /**@copydoc flux(). */
          template<class Model, std::size_t ArgMaskValue>
          static std::true_type methodIntrospection(const Model&, const MethodTagConstant<linearizedRobinFlux>&, const ArgumentMask<ArgMaskValue>&,
                                                    decltype(linearizedRobinFluxHelper(std::declval<const Model&>(),
                                                                                       std::declval<const AllArguments<Model> >(),
                                                                                       MaskSequence<ArgMaskValue>())) * = nullptr);

          /**@copydoc fluxHelper(). */
          template<class Model, class Tuple, std::size_t... I>
          static
          decltype(std::declval<Model>().singularFlux(std::declval<const decltype(std::get<I>(std::declval<Tuple>()))&>()...))
            singularFluxHelper(Model&&, Tuple&&, std::index_sequence<I...>);

          /**@copydoc flux(). */
          template<class Model, std::size_t ArgMaskValue>
          static std::true_type methodIntrospection(const Model&, const MethodTagConstant<singularFlux>&, const ArgumentMask<ArgMaskValue>&,
                                                    decltype(singularFluxHelper(std::declval<const Model&>(),
                                                                                std::declval<const AllArguments<Model> >(),
                                                                                MaskSequence<ArgMaskValue>())) * = nullptr);
          /**@copydoc fluxHelper(). */
          template<class Model, class Tuple, std::size_t... I>
          static
          decltype(std::declval<Model>().linearizedSingularFlux(std::declval<const decltype(std::get<I>(std::declval<Tuple>()))&>()...))
            linearizedSingularFluxHelper(Model&&, Tuple&&, std::index_sequence<I...>);

          /**@copydoc flux(). */
          template<class Model, std::size_t ArgMaskValue>
          static std::true_type methodIntrospection(const Model&, const MethodTagConstant<linearizedSingularFlux>&, const ArgumentMask<ArgMaskValue>&,
                                                    decltype(linearizedSingularFluxHelper(std::declval<const Model&>(),
                                                                                          std::declval<const AllArguments<Model> >(),
                                                                                          MaskSequence<ArgMaskValue>())) * = nullptr);

          /**@copydoc fluxHelper(). */
          template<class Model, class Tuple, std::size_t... I>
          static
          decltype(std::declval<Model>().dirichlet(std::declval<const decltype(std::get<I>(std::declval<Tuple>()))&>()...))
            dirichletHelper(Model&&, Tuple&&, std::index_sequence<I...>);

          /**@copydoc flux(). */
          template<class Model, std::size_t ArgMaskValue>
          static std::true_type methodIntrospection(const Model&, const MethodTagConstant<dirichlet>&, const ArgumentMask<ArgMaskValue>&,
                                                    decltype(dirichletHelper(std::declval<const Model&>(),
                                                                             std::declval<const AllArguments<Model> >(),
                                                                             MaskSequence<ArgMaskValue>())) * = nullptr);
          /**@copydoc fluxHelper(). */
          template<class Model, class Tuple, std::size_t... I>
          static
          decltype(std::declval<Model>().linearizedDirichlet(std::declval<const decltype(std::get<I>(std::declval<Tuple>()))&>()...))
            linearizedDirichletHelper(Model&&, Tuple&&, std::index_sequence<I...>);

          /**@copydoc flux(). */
          template<class Model, std::size_t ArgMaskValue>
          static std::true_type methodIntrospection(const Model&, const MethodTagConstant<linearizedDirichlet>&, const ArgumentMask<ArgMaskValue>&,
                                                    decltype(linearizedDirichletHelper(std::declval<const Model&>(),
                                                                                       std::declval<const AllArguments<Model> >(),
                                                                                       MaskSequence<ArgMaskValue>())) * = nullptr);

          /**Catch all default used when method in question is not
           * implemented. This method is specialized for all supported
           * model-contributions and evaluates to std::true_type if it
           * is present and one of the supported call-signatures match.
           */
          template<class Model, MethodTag tag>
          static std::false_type
          methodIntrospection(const Model&, const MethodTagConstant<tag>&, ...);

          /*********************************************************************
           *
           * Post-process the detected call-signatures.
           *
           * @note clang fails to compile the stuff if the
           * post-processing comes right after the "catch-all" and the
           * specializations are placed in-class after the
           * post-processing templates.
           *
           */

          /**Filter-functor which takes care of filtering out ambiguous
           * call-signatures if DimRange == DimDomain.
           */
          template<class Model, MethodTag tag>
          struct SignatureFilter
          {
            template<class T, T mask, std::size_t N = 0>
            struct Apply
              : BoolConstant<decltype(methodIntrospection(std::declval<Model>(),
                                                          MethodTagConstant<tag>{},
                                                          ArgumentMask<mask>{}))::value>
            {};
          };

          /**Define a sequence with all detected "legaL" call-signatures
           * for the given method.
           */
          template<class Model, MethodTag tag>
          using FilteredSignatures = FilteredSequence<SignatureFilter<Model, tag>, MethodSignatures<tag> >;

#if false && defined(__clang__)
          // ?????? NumCallSignatures is only access in THIS
          // struct. So what is this? Seemingly clang has problems
          // with variable templates.
#if 0
//modeltraits.hh:674:42: error: 'NumCallSignatures' is a protected member of
//      'Dune::ACFem::ModelIntrospection::(anonymous namespace)::TraitsHelper'
//          using HasMethod = BoolConstant<NumCallSignatures<Model, tag> == 1>;
#endif
          /**Count number of found methods. */
          template<class Model, MethodTag tag>
          static constexpr std::size_t NumCallSignatures = size<FilteredSignatures<Model, tag> >();
#else
          /**Count number of found methods. */
          template<class Model, MethodTag tag>
          using NumCallSignatures = IndexConstant<size<FilteredSignatures<Model, tag> >()>;
#endif

          /**Generate std::true_type if method is implemented.*/
          template<class Model, MethodTag tag>
          using HasMethod = BoolConstant<NumCallSignatures<Model, tag>::value == 1>;

          /**Functor in order to detect whether a method exists. */
          template<class Model>
          struct HasMethodFunctor
          {
            template<class T, T tag, std::size_t>
            using Apply = HasMethod<Model, CheckMethodTag<tag>::value>;
          };

          template<class Model>
          struct GetMethodSignature
          {
            template<class T, T tag, class = void>
            struct ApplyHelper {
              static_assert(!std::is_convertible<RangeType<Model>,
                                                 HessianRangeType<Model> >::value,
                            "RangeType convertible to HessianRangeType");
              static_assert(NumCallSignatures<Model, CheckMethodTag<tag>::value>::value == 0,
                            "More than one call-signature found for method-tag");
              using Type = Constant<T, 0>;
            };

            template<class T, T tag>
            struct ApplyHelper<T, tag, std::enable_if_t<HasMethod<Model, CheckMethodTag<tag>::value>::value> >
            {
              using Type = Head<FilteredSignatures<Model, CheckMethodTag<tag>::value> >;
            };

            template<class T, T V, std::size_t>
            using Apply = typename ApplyHelper<T, V>::Type;
          };

          /**The call signatures of all methods. Get<method,
           * MethodCallSignatures> set to 0 if method is not
           * implemented, meant as offset sequence for ModelMethods.
           */
          template<class Model>
          using MethodCallSignatures = TransformedSequence<GetMethodSignature<Model>, AllMethodTags>;

          /**A sequence with all implemented methods. */
          template<class Model>
          using ModelMethods = FilteredSequence<HasMethodFunctor<Model>, AllMethodTags>;
        }; // TraitsHelper class

      } // namespace

      /**Type alias extracting the index_sequence holding all allowed
       * call-signatures for the given tag.
       */
      template<MethodTag tag>
      using MethodSignatures = typename TraitsHelper::SupportedCallSignatures<tag>::Type;

      /**Type alias extracting the "closure" of all supported
       * call-signatures.
       */
      template<std::size_t tag>
      using MethodSignatureClosure = TraitsHelper::MethodSignatureClosure<CheckMethodTag<tag>::value>;

      /**A sequence with the closure patterns for each method.*/
      using MethodSignaturesClosureType = TraitsHelper::MethodSignaturesClosureType;

      /**Shortcut to the methods implemented by the model.*/
      template<class Model>
      using Methods = TraitsHelper::template ModelMethods<std::decay_t<Model> >;

      /**Shortcut identifying a zero model.*/
      template<class Model>
      constexpr inline bool isZero = Methods<Model>::size() == 0;

      /**Shortcut the the call-signature tuple.*/
      template<class Model>
      using MethodCallSignatures = TraitsHelper::template MethodCallSignatures<std::decay_t<Model> >;

      template<class Model, class SFINAE = void>
      struct IsModel
        : FalseType
      {};

      template<class Model>
      struct IsModel<Model, std::enable_if_t<!IsDecay<Model>::value> >
        : IsModel<std::decay_t<Model> >
      {};                       //

      /**std::true_type if Model is derived from ModelBase.*/
      template<class Model>
      struct IsModel<
        Model,
        std::enable_if_t<(IsDecay<Model>::value
                          && std::is_base_of<ModelBase<typename Model::DomainFunctionSpaceType,
                                                       typename Model::RangeFunctionSpaceType>,
                                             Model>::value
        )> >
        : TrueType
      {};

      template<class Model, class SFINAE = void>
      struct Traits
      {};

      template<class T>
      struct Traits<T, std::enable_if_t<!IsDecay<T>::value> >
        : Traits<std::decay_t<T> >
      {};

      /**Traits class extracting structural information from a given
       * PDE-model information. The class examines certain structure
       * flags and in particular the call signatures of the
       * implemented models and also determines if a method is
       * implemented at all.
       */
      template<class Model>
      struct Traits<Model, std::enable_if_t<IsModel<Model>::value && IsDecay<Model>::value> >
        : private TraitsHelper
      {
       private:
        using BaseType = TraitsHelper;
       public:
        using ModelType = Model;

        using FunctionSpaceType = typename ModelType::FunctionSpaceType;
        using DomainFunctionSpaceType = typename ModelType::DomainFunctionSpaceType;
        using RangeFunctionSpaceType = typename ModelType::RangeFunctionSpaceType;

        static constexpr std::size_t dimDomain = static_cast<std::size_t>(FunctionSpaceType::dimDomain);
        static constexpr std::size_t dimRange = static_cast<std::size_t>(FunctionSpaceType::dimRange);
        static constexpr std::size_t dimRangeRange = static_cast<std::size_t>(RangeFunctionSpaceType::dimRange);
        static constexpr std::size_t dimDomainRange = static_cast<std::size_t>(DomainFunctionSpaceType::dimRange);

        using DomainFieldType = BaseType::DomainFieldType<Model>;
        using DomainType = BaseType::DomainType<Model>;

        using RangeFieldType = BaseType::RangeFieldType<Model>;
        using RangeType = BaseType::RangeType<Model>;
        using JacobianRangeType = BaseType::JacobianRangeType<Model>;
        using HessianRangeType = BaseType::HessianRangeType<Model>;

        using RangeRangeFieldType = BaseType::RangeRangeFieldType<Model>;
        using RangeRangeType = BaseType::RangeRangeType<Model>;
        using RangeJacobianRangeType = BaseType::RangeJacobianRangeType<Model>;
        using RangeHessianRangeType = BaseType::RangeHessianRangeType<Model>;

        // Check alias data types
        static_assert(std::is_same<RangeType, RangeRangeType>::value,
                      "RangeType and RangeRangeType must coincide");
        static_assert(std::is_same<JacobianRangeType, RangeJacobianRangeType>::value,
                      "JacobianRangeType and RangeJacobianRangeType must coincide");
        static_assert(std::is_same<HessianRangeType, RangeHessianRangeType>::value,
                      "HessianRangeType and RangeHessianRangeType must coincide");

        using DomainRangeFieldType = BaseType::DomainRangeFieldType<Model>;
        using DomainRangeType = BaseType::DomainRangeType<Model>;
        using DomainJacobianRangeType = BaseType::DomainJacobianRangeType<Model>;
        using DomainHessianRangeType = BaseType::DomainHessianRangeType<Model>;

        /**Generate the closure tuple of all possible arguments, given
         * quadrature point.
         */
        template<class Quadrature>
        using AllArguments = typename BaseType::AllArguments<Model, Quadrature>;

        /**For debugging: signatures found. */
        template<MethodTag tag>
        using FilteredSignatures = typename BaseType::FilteredSignatures<Model, tag>;

        static_assert(std::is_same<typename Model::FunctionSpaceType,
                                   typename Model::RangeFunctionSpaceType>::value,
                      "Implementation bug: FunctionSpaceType MUST be an alias of RangeFunctionSpaceType");

        /**Generate the return type for the given method tag.*/
        template<MethodTag tag>
        using ReturnType = TupleElement<tag, BaseType::MethodReturnType<Model> >;

        /**The index sequence with all implemented methods. The call
         * signature of the respective method is found by lookup into
         * MethodCallSignatures:
         *
         * Get<Get<I, Methods>::value, MethodCallSignatures>
         *
         * is the call signature of the I-th implemented method. The
         * values of the integers stored in methods are given by the
         * MethodTag enum (flux, linearizedFlux etc.).
         */
        using Methods = typename BaseType::ModelMethods<Model>;

        /**The index-sequence with all determined call-signatures, can
         * be indexed by Get<I, Methods>::value. The signature for
         * unimplemented methods is the 0-mask.
         */
        using MethodCallSignatures = typename BaseType::MethodCallSignatures<Model>;

        /**Get the call signature for the method specified by tag.*/
        template<std::size_t tag>
        using CallSignature = Get<CheckMethodTag<tag>::value, MethodCallSignatures>;

        /**Compute the closure call-signature a call to the method
         * designated by tag, taking into account that non-linear methods
         * are default implemented by the linearized methods.
         */
        template<std::size_t tag>
        using CallSignatureClosure = ArgumentMask<
          (CallSignature<tag>::value
           |
           ((tag == fluxDivergence || CallSignature<tag>::value != (std::size_t)0)
            ? (std::size_t)0
            : CallSignature<LinearizedTag<tag>::value>::value))>;

        /**A bit mask, bit number N set if the method corresponding to
         * (MethodTag)N is implemented.
         */
        static constexpr std::size_t methodsMask = sequenceMask(Methods{});

        /**Decide whether just this method exists.*/
        template<std::size_t tag>
        using HasMethod = BaseType::HasMethod<Model, CheckMethodTag<tag>::value>;

        /**Helper functor for global property.*/
        struct ExistsFunctor
        {
          template<class T, T tag, std::size_t N = 0>
          using Apply = BoolConstant<
            HasMethod<NonLinearTag<tag>::value>::value ||
            HasMethod<LinearizedTag<tag>::value>::value>;
        };

        /**Linearized or non-linearized method is implemented. */
        template<std::size_t tag>
        using Exists = typename ExistsFunctor::template Apply<MethodTag, CheckMethodTag<tag>::value>;

        /**Helper functor for global property.*/
        struct IsLoadFunctor
        {
          template<class T, T tag, std::size_t N = 0>
          using Apply = BoolConstant<
            !Exists<CheckMethodTag<tag>::value>::value
            ||
            (HasMethod<NonLinearTag<CheckMethodTag<tag>::value>::value>::value
             &&
             (Get<NonLinearTag<CheckMethodTag<tag>::value>::value, MethodCallSignatures>::value
              &
              SequenceMask<valueIndex, jacobianIndex>::value) == 0)>;
        };

        /**Method defines only a load contribution. */
        template<std::size_t tag>
        using IsLoad = typename IsLoadFunctor::template Apply<MethodTag, CheckMethodTag<tag>::value>;

        /**Helper functor for global property.*/
        struct IsPiecewiseConstantFunctor
        {
          template<class T, T tag, std::size_t N = 0>
          using Apply = BoolConstant<
            !HasMethod<CheckMethodTag<tag>::value>::value
            ||
            (Get<CheckMethodTag<tag>::value, MethodCallSignatures>::value
             &
             SequenceMask<pointIndex>::value) == 0>;
        };

        /**Method does not depend on the quadrature point.*/
        template<std::size_t tag>
        using IsPiecewiseConstant = typename IsPiecewiseConstantFunctor::template Apply<MethodTag, CheckMethodTag<tag>::value>;

        /**Helper functor for global property.*/
        struct IsLinearFunctor
        {
          template<class T, T tag, std::size_t N = 0>
          using Apply = BoolConstant<!HasMethod<NonLinearTag<CheckMethodTag<tag>::value>::value>::value>;
        };

        /**Non-linear method is not implemented. */
        template<std::size_t tag>
        using IsLinear = typename IsLinearFunctor::template Apply<MethodTag, CheckMethodTag<tag>::value>;

        /**Helper functor for global property.*/
        struct IsAffineLinearFunctor
        {
          template<class T, T tag, std::size_t N = 0>
          using Apply = BoolConstant<
            (Get<LinearizedTag<CheckMethodTag<tag>::value>::value, MethodCallSignatures>::value
             &
             SequenceMask<linearizationValueIndex, linearizationJacobianIndex>::value) == 0>;
        };

        /**Linear method does not need point of linearization. */
        template<std::size_t tag>
        using IsAffineLinear = typename IsAffineLinearFunctor::template Apply<MethodTag, CheckMethodTag<tag>::value>;

        /**@name GlobalStructure
         *
         * Flags summarizing the over-all structure of the model.
         *
         * @{
         */

        /**Define to true if the model is affine-linear. */
        static constexpr bool isAffineLinear = AccumulateSequence<LogicalAndFunctor, TransformedSequence<IsAffineLinearFunctor, Methods> >::value;

        /**Define to true if the model is really linear */
        static constexpr bool isLinear = AccumulateSequence<LogicalAndFunctor, TransformedSequence<IsLinearFunctor, Methods> >::value;

        /**Define to true if no methods are implemented.*/
        static constexpr bool isZero = Methods::size() == 0;

        /**Define to true is the model is "constant" and defines only a load-contribution. */
        static constexpr bool isLoad = AccumulateSequence<LogicalAndFunctor, TransformedSequence<IsLoadFunctor, Methods> >::value;

        /**Define to true is the model has constant coefficients in
         * the sense that no contribution depends on the quadrature
         * point.
         */
        static constexpr bool isPiecewiseConstant = AccumulateSequence<LogicalAndFunctor, TransformedSequence<IsPiecewiseConstantFunctor, Methods> >::value;

        /**@} GlobalStructure */
      }; // Traits

    } // namespace ModelIntrospection

    /**std::true_type if Model is derived from ModelBase.*/
    template<class Model>
    using IsPDEModel = ModelIntrospection::template IsModel<Model>;

    /**Traits class for models.*/
    template<class Model>
    using ModelTraits = ModelIntrospection::Traits<Model>;

    /**Check for method.*/
    template<class Model, std::size_t tag>
    using ModelHasMethod = typename ModelTraits<Model>::template HasMethod<ModelIntrospection::CheckMethodTag<tag>::value>;

    /**Check for either non-linear or linearized method.*/
    template<class Model, std::size_t tag>
    using ModelMethodExists = typename ModelTraits<Model>::template Exists<ModelIntrospection::CheckMethodTag<tag>::value>;

    /**Call signature for given model and method.*/
    template<class Model, std::size_t tag>
    using ModelMethodSignature =
      typename ModelTraits<Model>::template CallSignature<ModelIntrospection::CheckMethodTag<tag>::value>;

    /**Compute the closure call-signature for the given model for
     * a call to the method designated by tag, taking into account
     * that non-linear methods are default implemented by the
     * linearized methods.
     */
    template<class Model, std::size_t tag>
    using ModelMethodSignatureClosure =
      typename ModelTraits<Model>::template CallSignatureClosure<ModelIntrospection::CheckMethodTag<tag>::value>;

    /**A sequence with the tags of all possible model-methods.*/
    using ModelAdmissibleMethods = ModelIntrospection::AllMethodTags;

    /**A sequence with the closure signatures for all methods.*/
    using ModelMethodClosureSignatures = ModelIntrospection::MethodSignaturesClosureType;

    //!@} ModelInterface

    //!@} PDE-Models

  } // namespace ACFem

}  //Namespace Dune

#endif // __DUNE_ACFEM_MODELTRAITS_HH__
