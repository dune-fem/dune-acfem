#ifndef __DUNE_ACFEM_MODELS_BINARYEXPRESION_HH__
#define __DUNE_ACFEM_MODELS_BINARYEXPRESION_HH__

#include <dune/fem/function/localfunction/const.hh>

#include "../functions/localfunctiontraits.hh"
#include "unaryexpression.hh"

namespace Dune
{

  namespace ACFem
  {

    /**@addtogroup ExpressionTemplates
     *
     * @{
     */

    /**@addtogroup ModelExpressions
     *
     *@{
     */

    namespace PDEModel {

// Suck in using declarations. This is needed as we defined operator
// functions of the same name in our own namespace.
#include "../expressions/usingpromotion.hh"
#include "../mpl/usingcomparisons.hh"

      namespace {

        /**@name BinaryOperationHelper
         *
         * Helper functors specializing for the case where either
         * model has or has not implemented the repective method.
         *
         * @{
         */

        /**Helper to determine the resulting call-signature in order
         * to distinguish between s-multiplication by functions and
         * combination of two models.
         */
        template<class Left, class Right, std::size_t tag, class Enable = void>
        struct BinaryClosureCallSignatureHelper;

        /**@internal Deduce call signature for method designated by @a
         * tag for given model expression.
         */
        template<class Left, class Right, std::size_t tag>
        struct BinaryClosureCallSignatureHelper<
          Left, Right, tag,
          std::enable_if_t<(IsPDEModel<Left>::value
                            && IsPDEModel<Right>::value
            )> >
        {
          using Type = ArgumentMask<ModelMethodSignatureClosure<Left, tag>::value
                                    |
                                    ModelMethodSignatureClosure<Right, tag>::value>;
        };

        /**@internal Deduce call signature for method designated by @a
         * tag when Left is a (constant) tensor.
         */
        template<class Left, class Right, std::size_t tag>
        struct BinaryClosureCallSignatureHelper<
          Left, Right, tag,
          std::enable_if_t<(IsTensor<Left>::value
                            && IsPDEModel<Right>::value
            )> >
        {
          using Type = ArgumentMask<ModelMethodSignatureClosure<Right, tag>::value>;
        };

        // We need the quadrature point in order to be able to
        // evaluate the function.
        template<class Left, class Right, std::size_t tag>
        struct BinaryClosureCallSignatureHelper<
          Left, Right, tag,
          std::enable_if_t<(IsConstLocalFunction<Left>::value
                            && IsPDEModel<Right>::value
            )> >
        {
          using Type = ArgumentMask<SequenceMask<pointIndex>::value
                                    |
                                    ModelMethodSignature<Right, tag>::value>;
        };

        // In order to implement the chain rule we need all arguments
        // for flux, fluxDivergence and the quadrature point for the
        // function.
        template<class Left, class Right>
        struct BinaryClosureCallSignatureHelper<
          Left, Right, fluxDivergence,
          std::enable_if_t<(IsConstLocalFunction<Left>::value
                            && IsPDEModel<Right>::value
            )> >
        {
          using Type = ArgumentMask<SequenceMask<pointIndex>::value
                                    |
                                    ModelMethodSignatureClosure<Right, flux>::value
                                    |
                                    ModelMethodSignature<Right, fluxDivergence>::value>;
        };

        /**Compute the closure call-signature for a binary expression
         * composed of the two models for a call to the method
         * designated by tag, taking into account that non-linear
         * methods are default implemented by the linearized methods.
         *
         * This is for the case that the method needs to be
         * instantiated at all, i.e. at least one of the two models
         * have it.
         */
        template<class Left, class Right, std::size_t tag>
        using BinaryClosureCallSignature = typename BinaryClosureCallSignatureHelper<Left, Right, tag>::Type;

        //!Shortcut booleans for passing to std::enable_if_t
        template<class Left, class Right, std::size_t tag, class SFINAE = void>
        struct HaveMethod
        {
          static constexpr bool left = false;
          static constexpr bool right = false;
          static constexpr bool both = false;
          static constexpr bool onlyLeft = false;
          static constexpr bool onlyRight = false;
        };

        template<class Left, class Right, std::size_t tag>
        struct HaveMethod<Left, Right, tag,
                          std::enable_if_t<(IsPDEModel<Left>::value && IsPDEModel<Right>::value)> >
        {
          static bool constexpr nonLinLeft = Traits<Left>::template HasMethod<tag>::value;
          static bool constexpr nonLinRight = Traits<Right>::template HasMethod<tag>::value;
          static bool constexpr linLeft = Traits<Left>::template HasMethod<LinearizedTag<tag>::value>::value && tag != fluxDivergence;
          static bool constexpr linRight = Traits<Right>::template HasMethod<LinearizedTag<tag>::value>::value && tag != fluxDivergence;
          static bool constexpr left = nonLinLeft || (nonLinRight && linLeft);
          static bool constexpr right = nonLinRight || (nonLinLeft && linRight);

          static bool constexpr both = left && right;
          // static bool constexpr none = !left && !right; not implemented
          static bool constexpr onlyLeft = !both && left;
          static bool constexpr onlyRight = !both && right;
        };

        template<class Operation, class Left, class Right,
                 std::size_t tag, class ArgSeq, class Enable = void>
        struct BinaryModelOperation;

        /**Specialization for binary +/- where only the left operand
         * has implemented the method.
         */
        template<class Operation, class Left, class Right,
                 std::size_t tag,  std::size_t... ArgsIdx>
        struct BinaryModelOperation<Operation, Left, Right,
                                    tag, IndexSequence<ArgsIdx...>,
                                    std::enable_if_t<HaveMethod<Left, Right, tag>::onlyLeft> >
        {
          static_assert(std::is_same<Operation, PlusOperation>::value ||
                        std::is_same<Operation, MinusOperation>::value,
                        "Unsupported operation");
          BinaryModelOperation(const Left& left, const Right& right)
            : left_(left)
          {}

          template<class... T>
          auto operator()(const T&... args) const
          {
            return left_(args...);
          }
         private:
          const TaggedModelMethod<Left, tag, SequenceMask<ArgsIdx...>::value> left_;
        };

        /**Specialization for binary +/- where only the right operand
         * has implemented the method.
         */
        template<class Operation, class Left, class Right,
                 std::size_t tag,  std::size_t... ArgsIdx>
        struct BinaryModelOperation<Operation, Left, Right,
                                    tag, IndexSequence<ArgsIdx...>,
                                    std::enable_if_t<HaveMethod<Left, Right, tag>::onlyRight> >
        {
          static_assert(std::is_same<Operation, PlusOperation>::value || std::is_same<Operation, MinusOperation>::value,
                        "Unsupported operation");
          BinaryModelOperation(const Left& left, const Right& right)
            : right_(right)
          {}

          template<class... T>
          auto operator()(const T&... args) const
          {
            return right_(args...);
          }
         private:
          const TaggedModelMethod<Right, tag, SequenceMask<ArgsIdx...>::value, OperationTraits<Operation> > right_;
        };

        /**Specialization for binary + where both methods are
         * implemented.
         */
        template<class Left, class Right,
                 std::size_t tag,  std::size_t... ArgsIdx>
        struct BinaryModelOperation<PlusOperation, Left, Right,
                                    tag, IndexSequence<ArgsIdx...>,
                                    std::enable_if_t<HaveMethod<Left, Right, tag>::both> >
        {
          BinaryModelOperation(const Left& left, const Right& right)
            : left_(left), right_(right)
          {}

          template<class... T>
          auto operator()(const T&... args) const
          {
            return left_(args...) + right_(args...);
          }
         private:
          const TaggedModelMethod<Left, tag, SequenceMask<ArgsIdx...>::value> left_;
          const TaggedModelMethod<Right, tag, SequenceMask<ArgsIdx...>::value> right_;
        };

        /**Specialization for binary "-" where both methods are
         * implemented. We do not parametrize with the operation
         * parameter in order to use "-=" straight ahead instead of
         * using "left += (right *= -1)".
         */
        template<class Left, class Right,
                 std::size_t tag,  std::size_t... ArgsIdx>
        struct BinaryModelOperation<MinusOperation, Left, Right,
                                    tag, IndexSequence<ArgsIdx...>,
                                    std::enable_if_t<HaveMethod<Left, Right, tag>::both> >
        {
          BinaryModelOperation(const Left& left, const Right& right)
            : left_(left), right_(right)
          {}

          template<class... T>
          auto operator()(const T&... args) const
          {
            return left_(args...) - right_(args...);
          }
         private:
          const TaggedModelMethod<Left, tag, SequenceMask<ArgsIdx...>::value> left_;
          const TaggedModelMethod<Right, tag, SequenceMask<ArgsIdx...>::value> right_;
        };

        /**Specialization for s-multiplication by tensors. Always Left is a
         * tensor.
         */
        template<class Left, class Right, std::size_t tag, std::size_t... ArgsIdx>
        struct BinaryModelOperation<
          SMultiplyOperation, Left, Right,
          tag, IndexSequence<ArgsIdx...>,
          std::enable_if_t<(IsTensor<Left>::value
                            && Traits<Right>::template HasMethod<tag>::value
          )> >
        {
          BinaryModelOperation(const Left& left, const Right& right)
            : left_(left), right_(right)
          {}

          template<class... T>
          auto operator()(const T&... args) const
          {
            return left_ * right_(args...);
          }

         private:
          const Left& left_; // stored and initialized in model-operation
          const TaggedModelMethod<Right, tag, SequenceMask<ArgsIdx...>::value> right_;
        };

        /**Specialization for s-multiplication by functions. This will
         * need some care, and for the fluxDivergence we need to
         * take the product-rule into account. Always Left is a
         * function. It is assumed to have the HasLocalFunction property.
         */
        template<class Left, class Right,
                 std::size_t tag, std::size_t... ArgsIdx>
        struct BinaryModelOperation<SMultiplyOperation, Left, Right,
                                    tag, IndexSequence<ArgsIdx...>,
                                    std::enable_if_t<(IsConstLocalFunction<Left>::value
                                                      && Traits<Right>::template HasMethod<tag>::value
                                                      && tag != fluxDivergence
                                      )> >
        {
          BinaryModelOperation(const Left& left, const Right& right)
            : left_(left), right_(right)
          {}

          template<class... T>
          auto operator()(const T&... args) const
          {
            static constexpr std::size_t innerPointIndex =
              Head<MaskSequence<CondensedMask<SequenceMask<ArgsIdx...>::value,
                                              SequenceMask<pointIndex>::value>::value>
                   >::value;
            // NOTE: the move() is essential here as operator[] of the
            // FieldVector does not respect rvalueness and we capture
            // expressions respecting their "valueness"
            return std::move(left_.evaluate(std::get<innerPointIndex>(std::forward_as_tuple(args...)))[0]) * right_(args...);
          }

         private:
          const Left& left_; // stored and initialized in model-operation
          const TaggedModelMethod<Right, tag, SequenceMask<ArgsIdx...>::value> right_;
        };

        /**Specialization for s-multiplication by functions. This will
         * need some care, and the for the fluxDivergence we need to
         * take the product-rule into account. Always Left is a
         * function. It assume to have the HasLocalFunction property.
         */
        template<class Left, class Right, std::size_t... ArgsIdx>
        struct BinaryModelOperation<SMultiplyOperation, Left, Right,
                                    fluxDivergence, IndexSequence<ArgsIdx...>,
                                    std::enable_if_t<(IsConstLocalFunction<Left>::value
                                                      && Traits<Right>::template HasMethod<fluxDivergence>::value
          )> >
        {
          BinaryModelOperation(const Left& left, const Right& right)
            : left_(left), flux_(right), fluxDivergence_(right)
          {}

          template<class... T>
          auto operator()(const T&... args) const
          {
            // TODO: use tensor expressions.
            static constexpr std::size_t innerPointIndex =
              Head<MaskSequence<CondensedMask<SequenceMask<ArgsIdx...>::value,
                                              SequenceMask<pointIndex>::value>::value>
                   >::value;
            const auto& point = std::get<innerPointIndex>(std::forward_as_tuple(args...));

            return
              left_.evaluate(point)[0] * fluxDivergence_(args...)
              +
              contractInner(flux_(args...), tensor(left_.jacobian(point))[0_c]);
          }

          // TODO: do the right things ...
         private:
          const Left& left_; // stored and initialized in model-operation
          const TaggedModelMethod<Right, flux, SequenceMask<ArgsIdx...>::value> flux_;
          const TaggedModelMethod<Right, fluxDivergence, SequenceMask<ArgsIdx...>::value> fluxDivergence_;
        };

        template<class Operation, class Left, class Right, std::size_t tag, std::size_t... ArgsIdx>
        using BinaryOperation = BinaryModelOperation<Operation, Left, Right, tag, IndexSequence<ArgsIdx...> >;

        /**Implement the method specified by @æ tag with the signature
         * defined by @a OuterArgsSeq. See also ModelMethod.
         *
         * @param[in]
         *
         * @param[in] Left Left model argument type.
         *
         * @param[in] Right Right model argument type.
         *
         * @param[in] LinArgSeq Indices into to closure argument tuple
         * to get the point of linearization.
         *
         * @param[in] OuterArgSeq Indices into the closure argument
         * tuple.
         *
         * @param[in] InnerArgSeq Indices into the sub-tuple of the
         * closure argument tuple defined by OuterArgSeq.
         */
        template<class Operation, class Left, class Right,
                 std::size_t tag,
                 class LinArgSeq, class ArgSeq>
        class BinaryModelMethod;

        /**@name BinaryFluxCaller
         *
         * The two specializations for the respective method:
         *
         * -# One of the underlying model implements the method and
         *    takes the quadrature point as argument. This results in
         *    a template method forwarding all arguments to the
         *    original method of the models, possibly applying a
         *    functor on the return value.
         *
         * -# One of the underlying models implements the method but
         *    does not take a quadrature point as argument. This
         *    results in a non-template method forwarding all
         *    arguments to the original method of the models, possibly
         *    applying a functor on the return value.
         *
         * The remaing specializations (left has method or not, right
         * has method or not) or forwarded to a helper class.
         *
         * @{
         */

        /**@copydoc BinaryModelMethod*/
        template<class Operation, class Left, class Right, std::size_t... OuterArgsIdx>
        class BinaryModelMethod<Operation, Left, Right, flux,
                                IndexSequence<>,
                                IndexSequence<pointIndex, OuterArgsIdx...> >
        {
         public:
          BinaryModelMethod(const Left& left, const Right& right) : f_(left, right) {}

          template<class Quadrature>
          auto flux(const QuadraturePoint<Quadrature>& x, const TupleElement<OuterArgsIdx, AllArgs<Right> >&... args) const
          {
            return f_(x, args...);
          }
         private:
          const BinaryOperation<Operation, Left, Right, ModelIntrospection::flux, pointIndex, OuterArgsIdx...> f_;
        };

        /**@copydoc BinaryModelMethod*/
        template<class Operation, class Left, class Right, std::size_t... OuterArgsIdx>
        class BinaryModelMethod<Operation, Left, Right, flux,
                                IndexSequence<>,
                                IndexSequence<OuterArgsIdx...> >
        {
         public:
          BinaryModelMethod(const Left& left, const Right& right) : f_(left, right) {}

          auto flux(const TupleElement<OuterArgsIdx, AllArgs<Right> >&... args) const
          {
            return f_(args...);
          }
         private:
          const BinaryOperation<Operation, Left, Right, ModelIntrospection::flux, OuterArgsIdx...> f_;
        };

        //!@}

        /**@name LinearizedBinaryFluxCaller
         *
         * @copydoc BinaryFluxCaller
         *
         * @{
         */

        /**@copydoc BinaryModelMethod*/
        template<class Operation, class Left, class Right, std::size_t... LinArgsIdx, std::size_t... OuterArgsIdx>
        class BinaryModelMethod<Operation, Left, Right, linearizedFlux,
                                IndexSequence<LinArgsIdx...>,
                                IndexSequence<pointIndex, OuterArgsIdx...> >
        {
         public:
          BinaryModelMethod(const Left& left, const Right& right) : f_(left, right) {}

          template<class Quadrature>
          auto linearizedFlux(const TupleElement<LinArgsIdx, AllArgs<Right> >&... linArgs,
                              const QuadraturePoint<Quadrature>& x,
                              const TupleElement<OuterArgsIdx, AllArgs<Right> >&... args) const
          {
            return f_(linArgs..., x, args...);
          }
         private:
          const BinaryOperation<Operation, Left, Right, ModelIntrospection::linearizedFlux, LinArgsIdx..., pointIndex, OuterArgsIdx...> f_;
        };

        /**@copydoc BinaryModelMethod*/
        template<class Operation, class Left, class Right, std::size_t... LinArgsIdx, std::size_t... OuterArgsIdx>
        class BinaryModelMethod<Operation, Left, Right, linearizedFlux,
                                IndexSequence<LinArgsIdx...>,
                                IndexSequence<OuterArgsIdx...> >
        {
         public:
          BinaryModelMethod(const Left& left, const Right& right) : f_(left, right) {}

          auto linearizedFlux(const TupleElement<LinArgsIdx, AllArgs<Right> >&... linArgs,
                              const TupleElement<OuterArgsIdx, AllArgs<Right> >&... args) const
          {
            return f_(linArgs..., args...);
          }
         private:
          const BinaryOperation<Operation, Left, Right, ModelIntrospection::linearizedFlux, LinArgsIdx..., OuterArgsIdx...> f_;
        };

        //!@}

        /**@name BinarySourceCaller
         *
         * @copydoc BinaryFluxCaller
         *
         * @{
         */

        /**@copydoc BinaryModelMethod*/
        template<class Operation, class Left, class Right, std::size_t... OuterArgsIdx>
        class BinaryModelMethod<Operation, Left, Right, source,
                                IndexSequence<>,
                                IndexSequence<pointIndex, OuterArgsIdx...> >
        {
         public:
          BinaryModelMethod(const Left& left, const Right& right) : f_(left, right) {}

          template<class Quadrature>
          auto source(const QuadraturePoint<Quadrature>& x, const TupleElement<OuterArgsIdx, AllArgs<Right> >&... args) const
          {
            return f_(x, args...);
          }
         private:
          const BinaryOperation<Operation, Left, Right, ModelIntrospection::source, pointIndex, OuterArgsIdx...> f_;
        };

        /**@copydoc BinaryModelMethod*/
        template<class Operation, class Left, class Right, std::size_t... OuterArgsIdx>
        class BinaryModelMethod<Operation, Left, Right, source,
                                IndexSequence<>,
                                IndexSequence<OuterArgsIdx...> >
        {
         public:
          BinaryModelMethod(const Left& left, const Right& right) : f_(left, right) {}

          auto source(const TupleElement<OuterArgsIdx, AllArgs<Right> >&... args) const
          {
            return f_(args...);
          }
         private:
          const BinaryOperation<Operation, Left, Right, ModelIntrospection::source, OuterArgsIdx...> f_;
        };

        //!@}

        /**@name LinearizedBinarySourceCaller
         *
         * @copydoc BinaryFluxCaller
         *
         * @{
         */

        /**@copydoc BinaryModelMethod*/
        template<class Operation, class Left, class Right, std::size_t... LinArgsIdx, std::size_t... OuterArgsIdx>
        class BinaryModelMethod<Operation, Left, Right, linearizedSource,
                                IndexSequence<LinArgsIdx...>,
                                IndexSequence<pointIndex, OuterArgsIdx...> >
        {
         public:
          BinaryModelMethod(const Left& left, const Right& right) : f_(left, right) {}

          template<class Quadrature>
          auto linearizedSource(const TupleElement<LinArgsIdx, AllArgs<Right> >&... linArgs,
                                const QuadraturePoint<Quadrature>& x,
                                const TupleElement<OuterArgsIdx, AllArgs<Right> >&... args) const
          {
            return f_(linArgs..., x, args...);
          }
         private:
          const BinaryOperation<Operation, Left, Right, ModelIntrospection::linearizedSource, LinArgsIdx..., pointIndex, OuterArgsIdx...> f_;
        };

        /**@copydoc BinaryModelMethod*/
        template<class Operation, class Left, class Right, std::size_t... LinArgsIdx, std::size_t... OuterArgsIdx>
        class BinaryModelMethod<Operation, Left, Right, linearizedSource,
                                IndexSequence<LinArgsIdx...>,
                                IndexSequence<OuterArgsIdx...> >
        {
         public:
          BinaryModelMethod(const Left& left, const Right& right) : f_(left, right) {}

          auto linearizedSource(const TupleElement<LinArgsIdx, AllArgs<Right> >&... linArgs,
                                const TupleElement<OuterArgsIdx, AllArgs<Right> >&... args) const
          {
            return f_(linArgs..., args...);
          }
         private:
          const BinaryOperation<Operation, Left, Right, ModelIntrospection::linearizedSource, LinArgsIdx..., OuterArgsIdx...> f_;
        };

        //!@}

        /**@name BinaryRobinFluxCaller
         *
         * @copydoc BinaryFluxCaller
         *
         * @{
         */

        /**@copydoc BinaryModelMethod*/
        template<class Operation, class Left, class Right, std::size_t... OuterArgsIdx>
        class BinaryModelMethod<Operation, Left, Right, robinFlux,
                                IndexSequence<>,
                                IndexSequence<pointIndex, OuterArgsIdx...> >
        {
         public:
          BinaryModelMethod(const Left& left, const Right& right) : f_(left, right) {}

          template<class Quadrature>
          auto robinFlux(const QuadraturePoint<Quadrature>& x, const TupleElement<OuterArgsIdx, AllArgs<Right> >&... args) const
          {
            return f_(x, args...);
          }
         private:
          const BinaryOperation<Operation, Left, Right, ModelIntrospection::robinFlux, pointIndex, OuterArgsIdx...> f_;
        };

        /**@copydoc BinaryModelMethod*/
        template<class Operation, class Left, class Right, std::size_t... OuterArgsIdx>
        class BinaryModelMethod<Operation, Left, Right, robinFlux,
                                IndexSequence<>,
                                IndexSequence<OuterArgsIdx...> >
        {
         public:
          BinaryModelMethod(const Left& left, const Right& right) : f_(left, right) {}

          auto robinFlux(const TupleElement<OuterArgsIdx, AllArgs<Right> >&... args) const
          {
            return f_(args...);
          }
         private:
          const BinaryOperation<Operation, Left, Right, ModelIntrospection::robinFlux, OuterArgsIdx...> f_;
        };

        //!@}

        /**@name LinearizedBinaryRobinFluxCaller
         *
         * @copydoc BinaryFluxCaller
         *
         * @{
         */

        /**@copydoc BinaryModelMethod*/
        template<class Operation, class Left, class Right, std::size_t... LinArgsIdx, std::size_t... OuterArgsIdx>
        class BinaryModelMethod<Operation, Left, Right, linearizedRobinFlux,
                                IndexSequence<LinArgsIdx...>,
                                IndexSequence<pointIndex, OuterArgsIdx...> >
        {
         public:
          BinaryModelMethod(const Left& left, const Right& right) : f_(left, right) {}

          template<class Quadrature>
          auto linearizedRobinFlux(const TupleElement<LinArgsIdx, AllArgs<Right> >&... linArgs,
                                   const QuadraturePoint<Quadrature>& x,
                                   const TupleElement<OuterArgsIdx, AllArgs<Right> >&... args) const
          {
            return f_(linArgs..., x, args...);
          }
         private:
          const BinaryOperation<Operation, Left, Right, ModelIntrospection::linearizedRobinFlux, LinArgsIdx..., pointIndex, OuterArgsIdx...> f_;
        };

        /**@copydoc BinaryModelMethod*/
        template<class Operation, class Left, class Right, std::size_t... LinArgsIdx, std::size_t... OuterArgsIdx>
        class BinaryModelMethod<Operation, Left, Right, linearizedRobinFlux,
                                IndexSequence<LinArgsIdx...>,
                                IndexSequence<OuterArgsIdx...> >
        {
         public:
          BinaryModelMethod(const Left& left, const Right& right) : f_(left, right) {}

          auto linearizedRobinFlux(const TupleElement<LinArgsIdx, AllArgs<Right> >&... linArgs,
                                   const TupleElement<OuterArgsIdx, AllArgs<Right> >&... args) const
          {
            return f_(linArgs..., args...);
          }
         private:
          const BinaryOperation<Operation, Left, Right, ModelIntrospection::linearizedRobinFlux, LinArgsIdx..., OuterArgsIdx...> f_;
        };

        //!@}

        /**@name BinarySingularFluxCaller
         *
         * @copydoc BinaryFluxCaller
         *
         * @{
         */

        /**@copydoc BinaryModelMethod*/
        template<class Operation, class Left, class Right, std::size_t... OuterArgsIdx>
        class BinaryModelMethod<Operation, Left, Right, singularFlux,
                                IndexSequence<>,
                                IndexSequence<pointIndex, OuterArgsIdx...> >
        {
         public:
          BinaryModelMethod(const Left& left, const Right& right) : f_(left, right) {}

          template<class Quadrature>
          auto singularFlux(const QuadraturePoint<Quadrature>& x, const TupleElement<OuterArgsIdx, AllArgs<Right> >&... args) const
          {
            return f_(x, args...);
          }
         private:
          const BinaryOperation<Operation, Left, Right, ModelIntrospection::singularFlux, pointIndex, OuterArgsIdx...> f_;
        };

        /**@copydoc BinaryModelMethod*/
        template<class Operation, class Left, class Right, std::size_t... OuterArgsIdx>
        class BinaryModelMethod<Operation, Left, Right, singularFlux,
                                IndexSequence<>,
                                IndexSequence<OuterArgsIdx...> >
        {
         public:
          BinaryModelMethod(const Left& left, const Right& right) : f_(left, right) {}

          auto singularFlux(const TupleElement<OuterArgsIdx, AllArgs<Right> >&... args) const
          {
            return f_(args...);
          }
         private:
          const BinaryOperation<Operation, Left, Right, ModelIntrospection::singularFlux, OuterArgsIdx...> f_;
        };

        //!@}

        /**@name LinearizedBinarySingularFluxCaller
         *
         * @copydoc BinaryFluxCaller
         *
         * @{
         */

        /**@copydoc BinaryModelMethod*/
        template<class Operation, class Left, class Right, std::size_t... LinArgsIdx, std::size_t... OuterArgsIdx>
        class BinaryModelMethod<Operation, Left, Right, linearizedSingularFlux,
                                IndexSequence<LinArgsIdx...>,
                                IndexSequence<pointIndex, OuterArgsIdx...> >
        {
         public:
          BinaryModelMethod(const Left& left, const Right& right) : f_(left, right) {}

          template<class Quadrature>
          auto linearizedSingularFlux(const TupleElement<LinArgsIdx, AllArgs<Right> >&... linArgs,
                                      const QuadraturePoint<Quadrature>& x,
                                      const TupleElement<OuterArgsIdx, AllArgs<Right> >&... args) const
          {
            return f_(linArgs..., x, args...);
          }
         private:
          const BinaryOperation<Operation, Left, Right, ModelIntrospection::linearizedSingularFlux, LinArgsIdx..., pointIndex, OuterArgsIdx...> f_;
        };

        /**@copydoc BinaryModelMethod*/
        template<class Operation, class Left, class Right, std::size_t... LinArgsIdx, std::size_t... OuterArgsIdx>
        class BinaryModelMethod<Operation, Left, Right, linearizedSingularFlux,
                                IndexSequence<LinArgsIdx...>,
                                IndexSequence<OuterArgsIdx...> >
        {
         public:
          BinaryModelMethod(const Left& left, const Right& right) : f_(left, right) {}

          auto linearizedSingularFlux(const TupleElement<LinArgsIdx, AllArgs<Right> >&... linArgs,
                                      const TupleElement<OuterArgsIdx, AllArgs<Right> >&... args) const
          {
            return f_(linArgs..., args...);
          }
         private:
          const BinaryOperation<Operation, Left, Right, ModelIntrospection::linearizedSingularFlux, LinArgsIdx..., OuterArgsIdx...> f_;
        };

        //!@}

        /**@name BinaryDirichletCaller
         *
         * @copydoc BinaryFluxCaller
         *
         * @{
         */

        /**@copydoc BinaryModelMethod*/
        template<class Operation, class Left, class Right, std::size_t... OuterArgsIdx>
        class BinaryModelMethod<Operation, Left, Right, ModelIntrospection::dirichlet,
                                IndexSequence<>,
                                IndexSequence<pointIndex, OuterArgsIdx...> >
        {
         public:
          BinaryModelMethod(const Left& left, const Right& right) : f_(left, right) {}

          template<class Quadrature>
          auto dirichlet(const QuadraturePoint<Quadrature>& x, const TupleElement<OuterArgsIdx, AllArgs<Right> >&... args) const
          {
            return f_(x, args...);
          }
         private:
          const BinaryOperation<Operation, Left, Right, ModelIntrospection::dirichlet, pointIndex, OuterArgsIdx...> f_;
        };

        /**@copydoc BinaryModelMethod*/
        template<class Operation, class Left, class Right, std::size_t... OuterArgsIdx>
        class BinaryModelMethod<Operation, Left, Right, ModelIntrospection::dirichlet,
                                IndexSequence<>,
                                IndexSequence<OuterArgsIdx...> >
        {
         public:
          BinaryModelMethod(const Left& left, const Right& right) : f_(left, right) {}

          auto dirichlet(const TupleElement<OuterArgsIdx, AllArgs<Right> >&... args) const
          {
            return f_(args...);
          }
         private:
          const BinaryOperation<Operation, Left, Right, ModelIntrospection::dirichlet, OuterArgsIdx...> f_;
        };

        //!@}

        /**@name LinearizedBinaryDirichletCaller
         *
         * @copydoc BinaryFluxCaller
         *
         * @{
         */

        /**@copydoc BinaryModelMethod*/
        template<class Operation, class Left, class Right, std::size_t... LinArgsIdx, std::size_t... OuterArgsIdx>
        class BinaryModelMethod<Operation, Left, Right, linearizedDirichlet,
                                IndexSequence<LinArgsIdx...>,
                                IndexSequence<pointIndex, OuterArgsIdx...> >
        {
         public:
          BinaryModelMethod(const Left& left, const Right& right) : f_(left, right) {}

          template<class Quadrature>
          auto linearizedDirichlet(const TupleElement<LinArgsIdx, AllArgs<Right> >&... linArgs,
                                   const QuadraturePoint<Quadrature>& x,
                                   const TupleElement<OuterArgsIdx, AllArgs<Right> >&... args) const
          {
            return f_(linArgs..., x, args...);
          }
         private:
          const BinaryOperation<Operation, Left, Right, ModelIntrospection::linearizedDirichlet, LinArgsIdx..., pointIndex, OuterArgsIdx...> f_;
        };

        /**@copydoc BinaryModelMethod*/
        template<class Operation, class Left, class Right, std::size_t... LinArgsIdx, std::size_t... OuterArgsIdx>
        class BinaryModelMethod<Operation, Left, Right, linearizedDirichlet,
                                IndexSequence<LinArgsIdx...>,
                                IndexSequence<OuterArgsIdx...> >
        {
         public:
          BinaryModelMethod(const Left& left, const Right& right) : f_(left, right) {}

          auto linearizedDirichlet(const TupleElement<LinArgsIdx, AllArgs<Right> >&... linArgs,
                                   const TupleElement<OuterArgsIdx, AllArgs<Right> >&... args) const
          {
            return f_(linArgs..., args...);
          }
         private:
          const BinaryOperation<Operation, Left, Right, ModelIntrospection::linearizedDirichlet, LinArgsIdx..., OuterArgsIdx...> f_;
        };

        //!@}

        /**@name BinaryFluxDivergenceCaller
         *
         * @copydoc BinaryFluxCaller
         *
         * @{
         */

        /**@copydoc BinaryModelMethod*/
        template<class Operation, class Left, class Right, std::size_t... OuterArgsIdx>
        class BinaryModelMethod<Operation, Left, Right, fluxDivergence,
                                IndexSequence<>,
                                IndexSequence<pointIndex, OuterArgsIdx...> >
        {
         public:
          BinaryModelMethod(const Left& left, const Right& right) : f_(left, right) {}

          template<class Quadrature>
          auto fluxDivergence(const QuadraturePoint<Quadrature>& x, const TupleElement<OuterArgsIdx, AllArgs<Right> >&... args) const
          {
            return f_(x, args...);
          }
         private:
          const BinaryOperation<Operation, Left, Right, ModelIntrospection::fluxDivergence, pointIndex, OuterArgsIdx...> f_;
        };

        /**@copydoc BinaryModelMethod*/
        template<class Operation, class Left, class Right, std::size_t... OuterArgsIdx>
        class BinaryModelMethod<Operation, Left, Right, fluxDivergence,
                                IndexSequence<>,
                                IndexSequence<OuterArgsIdx...> >
        {
         public:
          BinaryModelMethod(const Left& left, const Right& right) : f_(left, right) {}

          auto fluxDivergence(const TupleElement<OuterArgsIdx, AllArgs<Right> >&... args) const
          {
            return f_(args...);
          }
         private:
          const BinaryOperation<Operation, Left, Right, ModelIntrospection::fluxDivergence, OuterArgsIdx...> f_;
        };

        //!@}

        template<class Operation, class Left, class Right, std::size_t tag>
        using TaggedBinaryModelMethod = BinaryModelMethod<
          Operation, Left, Right, tag,
          MaskSequence<BinaryClosureCallSignature<Left, Right, tag>::linearizationSignature()>,
          MaskSequence<BinaryClosureCallSignature<Left, Right, tag>::signature()> >;

        /**@internal Inject BindableGridFunction as reference into
         * Fem::ConstLocalFunction as we already hold a copy in the BinaryModelExpression.
         *
         * Note: we cannot use std::conditional as the instantiation
         * of Fem::ConstLocalFunction<T&> will fail for everything but
         * Fem::BindableGridFunction.
         */
        template<class T, class SFINAE = void>
        struct LocalFunctionStorageHelper
        {
          using Type = Fem::ConstLocalFunction<std::decay_t<T> >;
        };

        template<class T>
        struct LocalFunctionStorageHelper<
          T,
          std::enable_if_t<std::is_base_of<Fem::BindableFunction, std::decay_t<T> >::value> >
        {
          using Type = Fem::ConstLocalFunction<std::decay_t<T>&>;
        };

        template<class T>
        using LocalFunctionStorage = typename LocalFunctionStorageHelper<T>::Type;

      }


      template<class Operation, class Left, class Right>
      struct BinaryModelMethods
      {
        using Type = MaskSequence<ModelTraits<Left>::methodsMask|ModelTraits<Right>::methodsMask>;
      };

      template<class Left, class Right>
      struct BinaryModelMethods<SMultiplyOperation, Left, Right>
      {
        using Type = MaskSequence<ModelTraits<Right>::methodsMask>;
      };

      template<class Operation,
               class Left, class Right,
               class Methods = typename BinaryModelMethods<Operation, Left, Right>::Type,
               class Enable = void>
      class BinaryModelExpression;

      template<class Operation, class Left, class Right, std::size_t... tag>
      class BinaryModelExpression<Operation, Left, Right,
                                  IndexSequence<tag...>,
                                  std::enable_if_t<(IsPDEModel<Left>::value && IsPDEModel<Right>::value)> >
        : public ModelBase<typename std::decay_t<Left>::DomainFunctionSpaceType,
                           typename std::decay_t<Left>::RangeFunctionSpaceType>
        , public Expressions::Storage<OperationTraits<Operation>, Left, Right>
        , public TaggedBinaryModelMethod<Operation, std::decay_t<Left>, std::decay_t<Right>, tag>...
        , public MPL::UniqueTags<
                   ConditionalType<(IsConstantExprArg<Left>::value
                                    && IsConstantExprArg<Right>::value
                     ), ConstantExpression, void>,
                   ConditionalType<(IsTypedValue<Left>::value
                                    && IsTypedValue<Right>::value
                     ), TypedValueExpression, void>
        >
      {
       public:
        using LeftDecay = std::decay_t<Left>;
        using RightDecay = std::decay_t<Right>;
       private:
        static_assert(std::is_same<Operation, PlusOperation>::value ||
                      std::is_same<Operation, MinusOperation>::value,
                      "Unsupported operation");
        static_assert(std::is_same<AllArgs<LeftDecay>, AllArgs<RightDecay> >::value,
                      "Trying to combine models with incompatible function spaces");
        using ThisType = BinaryModelExpression;
        using BaseType = ModelBase<typename LeftDecay::DomainFunctionSpaceType, typename LeftDecay::RangeFunctionSpaceType>;
        using StorageType = Expressions::Storage<OperationTraits<Operation>, Left, Right>;
        using LeftTraitsType = ModelTraits<Left>;
        using RightTraitsType = ModelTraits<Right>;
       protected:
        using StorageType::t0_;
        using StorageType::t1_;
       public:
        using StorageType::operand;
        using StorageType::operation;
        using typename StorageType::OperationType;
        using typename StorageType::FunctorType;
        using LeftType = Left;
        using RightType = Right;

        template<class LeftArg, class RightArg, std::enable_if_t<std::is_constructible<StorageType, OperationTraits<Operation>, LeftArg, RightArg>::value, int> = 0>
        BinaryModelExpression(LeftArg&& left, RightArg&& right)
          : StorageType(FunctorType{}, std::forward<LeftArg>(left), std::forward<RightArg>(right))
          , TaggedBinaryModelMethod<Operation, LeftDecay, RightDecay, tag>(this->left(), this->right())...
        {}

        BinaryModelExpression(const ThisType& other)
          : StorageType(other)
          , TaggedBinaryModelMethod<Operation, LeftDecay, RightDecay, tag>(left(), right())...
        {}

        BinaryModelExpression(ThisType&& other)
          : StorageType(std::move(other))
          , TaggedBinaryModelMethod<Operation, LeftDecay, RightDecay, tag>(left(), right())...
        {}

        std::string name() const
        {
          return operationName(operation(), left().name(), right().name());
        }

        template<class Entity>
        void bind(const Entity& entity)
        {
          left().bind(entity);
          right().bind(entity);
        }

        void unbind()
        {
          left().unbind();
          right().unbind();
        }

        template<class Intersection>
        auto classifyBoundary(const Intersection& intersection)
        {
          // first check if nothing is to be done. This is
          // compile-time static.
          if (!LeftTraitsType::template Exists<ModelIntrospection::robinFlux>::value
              &&
              !LeftTraitsType::template Exists<ModelIntrospection::singularFlux>::value
              &&
              !LeftTraitsType::template Exists<ModelIntrospection::dirichlet>::value
              &&
              !RightTraitsType::template Exists<ModelIntrospection::robinFlux>::value
              &&
              !RightTraitsType::template Exists<ModelIntrospection::singularFlux>::value
              &&
              !RightTraitsType::template Exists<ModelIntrospection::dirichlet>::value) {
            return typename BaseType::BoundaryConditionsType();
          }

          // Otherwise runtime checks have to be performed.
          auto result = left().classifyBoundary(intersection);
          if (result.first) {
            auto rightResult = right().classifyBoundary(intersection);
            if (rightResult.first) {
              result.second |= rightResult.second;
            }
            return result;
          } else {
            return right().classifyBoundary(intersection);
          }
        }
       protected:
        constexpr auto&& left() && { return t0_; }
        constexpr auto& left() & { return t0_; }
        constexpr const auto& left() const& { return t0_; }
        constexpr auto&& right() && { return t1_; }
        constexpr auto& right() & { return t1_; }
        constexpr const auto& right() const& { return t1_; }
      };

      /**Binary model expression for S-multiplication with tensors.*/
      template<class Left, class Right, std::size_t... tag>
      class BinaryModelExpression<SMultiplyOperation, Left, Right,
                                  IndexSequence<tag...>,
                                  std::enable_if_t<(IsTensor<Left>::value
                                                    && TensorTraits<Left>::rank == 0
                                                    && IsPDEModel<Right>::value)> >
        : public ModelBase<typename std::decay_t<Right>::DomainFunctionSpaceType,
                           typename std::decay_t<Right>::RangeFunctionSpaceType>
        , public Expressions::Storage<OperationTraits<SMultiplyOperation>, Left, Right>
        , public TaggedBinaryModelMethod<SMultiplyOperation, std::decay_t<Left>, std::decay_t<Right>, tag>...
        , public MPL::UniqueTags<
                   ConditionalType<(IsConstantExprArg<Left>::value
                                    && IsConstantExprArg<Right>::value
                     ), ConstantExpression, void>,
                   ConditionalType<(IsTypedValue<Left>::value
                                    && IsTypedValue<Right>::value
                     ), TypedValueExpression, void>
        >
      {
        using ThisType = BinaryModelExpression;
        using LeftDecay = std::decay_t<Left>;
        using RightDecay = std::decay_t<Right>;
        using BaseType = ModelBase<typename RightDecay::DomainFunctionSpaceType,
                                   typename RightDecay::RangeFunctionSpaceType>;
        using RightTraitsType = ModelTraits<Right>;
        using StorageType = Expressions::Storage<OperationTraits<SMultiplyOperation>, Left, Right>;
       public:
        using StorageType::operand;
        using StorageType::operation;
        using typename StorageType::OperationType;
        using typename StorageType::FunctorType;
        using LeftType = Left;
        using RightType = Right;

        template<class LeftArg, class RightArg, std::enable_if_t<std::is_constructible<StorageType, FunctorType, LeftArg, RightArg>::value, int> = 0>
        BinaryModelExpression(LeftArg&& left, RightArg&& right)
          : StorageType(FunctorType{}, std::forward<LeftArg>(left), std::forward<RightArg>(right))
          , TaggedBinaryModelMethod<OperationType, LeftDecay, RightDecay, tag>(this->left(), this->right())...
        {}

        BinaryModelExpression(const ThisType& other)
          : StorageType(other)
          , TaggedBinaryModelMethod<OperationType, LeftDecay, RightDecay, tag>(left(), right())...
        {}

        BinaryModelExpression(ThisType&& other)
          : StorageType(std::move(other))
          , TaggedBinaryModelMethod<OperationType, LeftDecay, RightDecay, tag>(left(), right())...
        {}

        std::string name() const
        {
          return operationName(operation(), left().name(), right().name());
        }

        template<class Entity>
        void bind(const Entity& entity)
        {
          right().bind(entity);
        }

        void unbind()
        {
          right().unbind();
        }

        template<class Intersection>
        auto classifyBoundary(const Intersection& intersection)
        {
          return right().classifyBoundary(intersection);
        }

      protected:
        using StorageType::t0_;
        using StorageType::t1_;
        constexpr auto&& left() && { return t0_; }
        constexpr auto& left() & { return t0_; }
        constexpr const auto& left() const& { return t0_; }
        constexpr auto&& right() && { return t1_; }
        constexpr auto& right() & { return t1_; }
        constexpr const auto& right() const& { return t1_; }
      };

      /**Binary model expression for S-multiplication with functions.*/
      template<class Left, class Right, std::size_t... tag>
      class BinaryModelExpression<SMultiplyOperation, Left, Right,
                                  IndexSequence<tag...>,
                                  std::enable_if_t<(IsWrappableByConstLocalFunction<Left>::value
                                                    && std::decay_t<Left>::FunctionSpaceType::dimRange == 1
                                                    && IsPDEModel<Right>::value)> >
        : public ModelBase<typename std::decay_t<Right>::DomainFunctionSpaceType,
                           typename std::decay_t<Right>::RangeFunctionSpaceType>
        , public Expressions::Storage<OperationTraits<SMultiplyOperation>, Left, Right>
        , public TaggedBinaryModelMethod<SMultiplyOperation, LocalFunctionStorage<Left>, std::decay_t<Right>, tag>...
        , public MPL::UniqueTags<
                   ConditionalType<(IsConstantExprArg<Left>::value
                                    && IsConstantExprArg<Right>::value
                     ), ConstantExpression, void>,
                   ConditionalType<(IsTypedValue<Left>::value
                                    && IsTypedValue<Right>::value
                     ), TypedValueExpression, void>
        >
      {
        using ThisType = BinaryModelExpression;
        using LeftDecay = std::decay_t<Left>;
        using RightDecay = std::decay_t<Right>;
        using BaseType = ModelBase<typename RightDecay::DomainFunctionSpaceType,
                                   typename RightDecay::RangeFunctionSpaceType>;
        using RightTraitsType = ModelTraits<Right>;
        using LocalLeftType = LocalFunctionStorage<Left>;
        using StorageType = Expressions::Storage<OperationTraits<SMultiplyOperation>, Left, Right>;
       public:
        using typename StorageType::OperationType;
        using typename StorageType::FunctorType;
        using StorageType::operation;
        using StorageType::operand;
        using LeftType = Left;
        using RightType = Right;

        template<class LeftArg, class RightArg, std::enable_if_t<std::is_constructible<StorageType, FunctorType, LeftArg, RightArg>::value, int> = 0>
        BinaryModelExpression(LeftArg&& left, RightArg&& right)
          : StorageType(FunctorType{}, std::forward<LeftArg>(left), std::forward<RightArg>(right))
          , TaggedBinaryModelMethod<OperationType, LocalLeftType, RightDecay, tag>(localLeft_, this->right())...
          , localLeft_(this->left())
        {}

        BinaryModelExpression(const ThisType& other)
          : StorageType(other)
          , TaggedBinaryModelMethod<OperationType, LocalLeftType, RightDecay, tag>(localLeft_, right())...
          , localLeft_(left())
        {}

        BinaryModelExpression(ThisType&& other)
          : StorageType(std::move(other))
          , TaggedBinaryModelMethod<OperationType, LocalLeftType, RightDecay, tag>(localLeft_, right())...
          , localLeft_(left())
        {}

        std::string name() const
        {
          return operationName(operation(), left().name(), right().name());
        }

        template<class Entity>
        void bind(const Entity& entity)
        {
          localLeft_.bind(entity);
          right().bind(entity);
        }

        void unbind()
        {
          localLeft_.unbind();
          right().unbind();
        }

        template<class Intersection>
        auto classifyBoundary(const Intersection& intersection)
        {
          return right().classifyBoundary(intersection);
        }

       protected:
        using StorageType::t0_;
        using StorageType::t1_;
        constexpr auto&& left() && { return t0_; }
        constexpr auto& left() & { return t0_; }
        constexpr const auto& left() const& { return t0_; }
        constexpr auto&& right() && { return t1_; }
        constexpr auto& right() & { return t1_; }
        constexpr const auto& right() const& { return t1_; }

        LocalLeftType localLeft_;
      };

      template<class T>
      using ModelOperandDecayType = ConditionalType<
        (IsPDEModel<T>::value
         ||
         std::is_base_of<Fem::BindableFunction, std::decay_t<T> >::value),
        std::decay_t<T>,
        T>;

      /**Generate a binary model expression.*/
      template<class F, class T0, class T1,
               std::enable_if_t<IsPDEModel<T0>::value || IsPDEModel<T1>::value, int> = 0>
      constexpr auto operate(Expressions::DontOptimize, F, T0&& t0, T1&& t1)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        using OperationType = typename F::OperationType;
        using T0Type = ModelOperandDecayType<T0>;
        using T1Type = ModelOperandDecayType<T1>;

        return BinaryModelExpression<OperationType, T0Type, T1Type>(std::forward<T0>(t0), std::forward<T1>(t1));
      }

    } // PDEModel

    template<class Operation, class Left, class Right>
    using BinaryModelExpression = PDEModel::BinaryModelExpression<Operation, Left, Right>;

    //!@} ModelExpressions

    //!@} ExpressionTemplates

  } // namespace ACFem

} // namespace Dune


#endif // __DUNE_ACFEM_MODELS_BINARYEXPRESION_HH__
