#ifndef __DUNE_ACFEM_MODEL_BOUNDARYINDICATORS_HH__
#define __DUNE_ACFEM_MODEL_BOUNDARYINDICATORS_HH__

#include "indicators/boundaryindicator.hh"
#include "expressions.hh"

namespace Dune {

  namespace ACFem {

    namespace PDEModel {

      /**@addtogroup PDE-Models
       *@{
       */

      /**@addtogroup Model-Adapters
       *@{
       */

      /**A wrapper which constructs from the given model a Dirichlet
       * boundary indicator, using Model::classifyBoundary.
       */
      template<class Model>
      class DirichletIndicator
        : public BoundaryIndicator::BoundaryIndicator
      {
        static_assert(IsPDEModel<Model>::value,
                      "Model better would be a proper PDE-model ...");
       public:
        using ModelType = Model;
        using ModelTraits = EffectiveModelTraits<Model>;

        DirichletIndicator(ModelType& model)
          : model_(model)
        {}

        template<class Intersection>
        bool applies(const Intersection& intersection) const
        {
          if (!ModelTraits::template Exists<PDEModel::dirichlet>::value) {
            return false;
          }
          auto bdCond = model_.classifyBoundary(intersection);
          if (bdCond.first) {
            // ... later better (or other ...)
            assert(bdCond.second.all() || bdCond.second.none());
            return bdCond.second.all();
          } else {
            return false;
          }
        }

       private:
        ModelType& model_;
      };

      /**A wrapper which constructs from the given model a Robin
       * boundary indicator, using Model::classifyBoundary.
       */
      template<class Model>
      class RobinIndicator
        : public BoundaryIndicator::BoundaryIndicator
      {
        static_assert(IsPDEModel<Model>::value,
                      "Model better would be a proper PDE-model ...");
       public:
        using ModelType = Model;
        using ModelTraits = EffectiveModelTraits<Model>;

        RobinIndicator(ModelType& model)
          : model_(model)
        {}

        template<class Intersection>
        bool applies(const Intersection& intersection) const
        {
          if (!ModelTraits::template Exists<PDEModel::robinFlux>::value) {
            return false;
          }
          typename ModelType::BoundaryClassComponentsType components;
          if (model_.classifyBoundary(intersection, components)) {
            // ... later better (or other ...)
            assert(components.all() || components.none());
            return components.none();
          } else {
            return false;
          }
        }

       private:
        ModelType& model_;
      };

      /**Generate a boundary indicator evaluating to 1 on the Dirichlet
       * segment of the model.
       */
      template<class Model>
      auto modelDirichletIndicator(Model&& model)
      {
        return DirichletIndicator<Model>(std::forward<Model>(model));
      }

      /**Generate a boundary indicator evaluating to 1 on the Robin
       * segment of the model.
       */
      template<class Model>
      auto modelRobinIndicator(Model& model)
      {
        return RobinIndicator<Model>(model);
      }

      //!@} Model-Adapters

      //!@} PDE-Models

    } // PDEModel::

  } // ACFem::

} // Dune::

#endif //  __DUNE_ACFEM_MODEL_BOUNDARYINDICATORS_HH__
