#ifndef __DUNE_ACFEM_MODELS_EXPRESSIONTRAITS_HH__
#define __DUNE_ACFEM_MODELS_EXPRESSIONTRAITS_HH__

#include "../expressions/traitsdefault.hh"
#include "../tensors/tensorbase.hh"
#include "../functions/functiontraits.hh"

#include "expressionoperations.hh"
#include "modeltraits.hh"
//#include "closuretraits.hh"
#include "definiteness.hh"
#include "symmetry.hh"

namespace Dune
{

  namespace ACFem
  {

    /**@addtogroup ExpressionTemplates
     *
     * @{
     */

    /**@addtogroup ModelExpressions
     *
     *@{
     */

    /**@addtogroup ModelTypeTraits
     * @{
     */

    /**Override default expression traits s.t. the sign reflects the
     * definiteness of the linearization.
     */
    template<class Model>
    struct ExpressionTraits<Model,
                            std::enable_if_t<IsPDEModel<Model>::value>,
                            TraitsPriority::Secondary>
    : ConditionalType<ModelTraits<Model>::isZero,
                      ZeroExpressionTraits<Model>,
                      BaseExpressionTraits<Model> >
    {
      using Definiteness = decltype(definiteness(std::declval<Model>()));
      static constexpr bool isSymmetric = decltype(PDEModel::isSymmetric(std::declval<Model>()))::value;
    };

    namespace PDEModel {

      /**Evaluates to Model::ModelType if Model is some closure of
       * another model or to Model if Model is not a closure of another
       * model and is a PDE-model.
       */
      template<class Model>
      using EffectiveModel = ConditionalType<IsClosureExpression<Model>::value,
                                             Expressions::Operand<0, Model>,
                                             Model>;

      /**Evaluates to ModelTraits<Model::ModelType> if Model is some
       * closure of another model or to ModelTraits<Model> if Model is
       * not a closure of another model.
       */
      template<class Model>
      using EffectiveModelTraits = ModelTraits<EffectiveModel<Model> >;

      /**@c TrueType if T is not an expression closure and PDEModel.*/
      template<class T>
      using IsProperPDEModel = BoolConstant<(IsPDEModel<T>::value && !Expressions::IsPromotedTopLevel<T>::value)>;

      /**@c TrueType if T is something which can scale a model.*/
      template<class T>
      using IsProperMultiplicationOperand =
        BoolConstant<(!Expressions::IsPromotedTopLevel<T>::value
                      &&
                      ((IsTensor<T>::value && TensorTraits<T>::rank == 0)
                       || GridFunction::RangeTensorTraits<T>::rank == 0))>;

      /**TensorTraits for the RangeType of a model, keeping scalar
       * decay of 1d vectors into account.
       */
      template<class T, class SFINAE = void>
      struct RangeTensorTraits
        : TensorTraits<typename ModelTraits<T>::RangeType>
      {};

      template<class T>
      struct RangeTensorTraits<T, std::enable_if_t<ModelTraits<T>::dimRange == 1> >
        : TensorTraits<typename ModelTraits<T>::RangeFieldType>
      {};

      /**Whether or not to symmetrize skeleton terms arising from
       * integration by parts. This is using in DG IP methods and in the
       * NitscheDirichletModel.
       */
      template<class Model>
      using SkeletonSymmetrizeDefault =
        Sign<(ModelMethodExists<EffectiveModel<Model>, ModelIntrospection::flux>::value
              &&
              EffectiveModelTraits<Model>::template IsAffineLinear<ModelIntrospection::flux>::value
              &&
              ExpressionTraits<Model>::isSymmetric)>;

    } // PDEModel::

    //!@} ModelTypeTraits

    //!@} ModelExpressions

    //!@} ExpressionTemplates

  } // namespace ACFem

} // namespace Dune

#endif // __DUNE_ACFEM_MODELS_EXPRESSIONTRAITS_HH__
