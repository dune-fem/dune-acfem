#ifndef __DUNE_ACFEM_MODELS_DEFINITENESS_HH__
#define __DUNE_ACFEM_MODELS_DEFINITENESS_HH__

#include "../expressions/storage.hh"
#include "modeltraits.hh"

namespace Dune {

  namespace ACFem {

    namespace PDEModel {

      template<class T, std::enable_if_t<!IsPDEModel<T>::value, int> = 0>
      constexpr auto definiteness(T&&)
      {
        return typename ExpressionTraits<T>::Sign{};
      }

      template<class T, std::enable_if_t<(IsPDEModel<T>::value
                                          && (!IsExpression<T>::value || IsSelfExpression<T>::value)
        ), int> = 0>
      constexpr auto definiteness(T&&)
      {
        return ExpressionSign<BaseExpressionTraits<T>::Sign::isNonSingular && !ModelTraits<T>::isZero,
                              BaseExpressionTraits<T>::Sign::isSemiPositive || ModelTraits<T>::isLoad,
                              BaseExpressionTraits<T>::Sign::isSemiNegative || ModelTraits<T>::isLoad>{};
      }

      namespace {

        template<class T, std::size_t... Idx>
        constexpr auto definitenessExpander(T&& t, IndexSequence<Idx...>)
        {
          return Expressions::Functor<T>::signPropagation(definiteness(std::forward<T>(t).template operand<Idx>())...);
        }

      }

      template<class T, std::enable_if_t<(IsPDEModel<T>::value
                                          && IsExpression<T>::value
                                          && !IsSelfExpression<T>::value
        ), int> = 0>
      constexpr auto definiteness(T&& t)
      {
        using PromotedSign = decltype(definitenessExpander(std::forward<T>(t), MakeIndexSequence<Expressions::Arity<T>::value>{}));

        constexpr bool nonSingular =
          (BaseExpressionTraits<T>::isNonZero || PromotedSign::isNonSingular) && !ModelTraits<T>::isZero;
        constexpr bool semiPositive =
          (BaseExpressionTraits<T>::isSemiPositive || PromotedSign::isSemiPositive) || ModelTraits<T>::isLoad;
        constexpr bool semiNegative =
          (BaseExpressionTraits<T>::isSemiNegative || PromotedSign::isSemiNegative) || ModelTraits<T>::isLoad;

        return ExpressionSign<nonSingular, semiPositive, semiNegative>{};
      }

    } // PDEModel::

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_MODELS_DEFINITENESS_HH__
