#ifndef __DUNE_ACFEM_MODELS_UNARYEXPRESSION_HH__
#define __DUNE_ACFEM_MODELS_UNARYEXPRESSION_HH__

#include "../expressions/operationtraits.hh"
#include "../expressions/storage.hh"
#include "../expressions/interface.hh"
#include "../expressions/signutil.hh"
#include "../common/literals.hh"
#include "../tensors/tensor.hh"

#include "modeltraits.hh"
#include "expressionoperations.hh"

namespace Dune
{

  namespace ACFem
  {

    /**@addtogroup ExpressionTemplates
     *
     * @{
     */

    /**@addtogroup ModelExpressions
     *
     * Arithmetic with PDE-models.
     *
     *@{
     */

    namespace PDEModel {

      using namespace Literals;

      /**Should be ok, both namespaces are closely related ...*/
      using namespace ModelIntrospection;

      /**Shortcut alias template.*/
      template<class Model, class Quadrature = PointWrapperQuadrature<typename Model::DomainType> >
      using AllArgs = typename Traits<Model>::template AllArguments<Quadrature>;

      /**Implement the method specified by @a tag with the signature
       * defined by OuterArgSeq. If @a Model defines the corresponding
       * method then forward to the method defined by the model with
       * the call-signature defined by InnerArgSeq. Otherwise forward
       * to the linearized method or implement a dummy method with the
       * requested call-signature which returns 0.
       *
       * @note Unfortunately, the quadrature point comes as a free
       * template. This again doubles the number of specializations:
       * if the point is omitted, then the method is a non-template
       * method. Also: at least gcc fails to deduce the correct call
       * pattern when not specializing the quad-point/no quad-point
       * case.
       *
       * @param[in] Model The model class.
       *
       * @param[in] ReturnValueFunctor A function-like object
       * (e.g. a lambda) which is applied to the return value of the
       * respective model method in order to implement unary model
       * operations.
       *
       * @param[in] tag The "name" of the requested method.
       *
       * @param[in] LinArgSeq Indices into to closure argument tuple
       * to get the point of linearization.
       *
       * @param[in] OuterArgSeq Indices into the closure argument
       * tuple.
       *
       * @param[in] InnerArgSeq Indices into the sub-tuple of the
       * closure argument tuple defined by OuterArgSeq.
       *
       * @param[in] Disable Dummy template parameter in order to
       * disable template instantiation using the usual
       * std::enable_if_t technique.
       */
      template<class Model, class ReturnValueFunctor, std::size_t tag,
               class LinArgSeq, class OuterArgSeq, class InnerArgSeq,
               class Enable = void>
      class ModelMethod;

      /**Alias-template which computes the inner call-signature.*/
      template<class Model,
               std::size_t tag,
               std::size_t signature = Get<tag, typename ModelIntrospection::Traits<Model>::MethodCallSignatures>::value,
               class F = OperationTraits<IdentityOperation> >
      using TaggedModelMethod = ModelMethod<
        Model, F, tag,
        MaskSequence<ArgumentMask<signature>::linearizationSignature()>,
        MaskSequence<ArgumentMask<signature>::signature()>,
        MaskSequence<CondensedMask<signature, Get<tag, typename ModelIntrospection::Traits<Model>::MethodCallSignatures>::value>::value>
        >;

      /**@name FluxCaller
       *
       * The four specializations for the respective method:
       *
       * -# Underlying model implements the method and the requested
       *    call-signature takes the quadrature point argument. This
       *    results in a template method forwarding all arguments to
       *    the original method of the model, possibly applying a
       *    functor on the return value.
       *
       * -# Underlying model implements the method but the requested
       *    call-signature does not take a quadrature point as
       *    argument. This results in a non-template method forwarding
       *    all arguments to the original method of the model,
       *    possibly applying a functor on the return value.
       *
       * -# Underlying Model does not implement the method but
       *    requested call-signature requires the quadrature point
       *    argument. This results in a template method returning 0.
       *
       * -# Underlying Model does not implement the method and the
       *    requested call-signature does not need a quadrature point
       *    as argument. This results in a non-template method
       *    returning 0.
       *
       * The templates below also take care of forwarding to the
       * linearized method in case the non-linear method is not
       * implemented.
       *
       * @{
       */

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, flux,
                        IndexSequence<>,
                        IndexSequence<pointIndex, OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<Traits<Model>::template HasMethod<flux>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::flux;

        ModelMethod(const Model& model, const F& f = F())
          : model_(model), f_(f)
        {}

        //!The proper method inherited by the derived model.
        template<class Quadrature>
        auto flux(const QuadraturePoint<Quadrature>& x, const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return f_(model_.flux(std::get<InnerArgsIdx>(std::forward_as_tuple(x, args...))...));
        }

        //!Helper in order to use parametrized expression templates.
        template<class Quadrature>
        auto operator()(const QuadraturePoint<Quadrature>& x, const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return flux(x, args...);
        }

       private:
        const Model& model_;
        const F& f_;
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, flux,
                        IndexSequence<>,
                        IndexSequence<OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<Traits<Model>::template HasMethod<flux>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::flux;

        ModelMethod(const Model& model, const F& f = F())
          : model_(model), f_(f)
        {}

        //!The proper method inherited by the derived model.
        auto flux(const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return f_(model_.flux(std::get<InnerArgsIdx>(std::forward_as_tuple(args...))...));
        }

        //!Helper in order to use parametrized expression templates.
        auto operator()(const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return flux(args...);
        }

       private:
        const Model& model_;
        const F& f_;
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, flux,
                        IndexSequence<>,
                        IndexSequence<pointIndex, OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<!Traits<Model>::template HasMethod<flux>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::flux;

        ModelMethod(const Model& model, const F& f = F())
          : l_(model, f)
        {}

        //!The proper method inherited by the derived model.
        template<class Quadrature>
        auto flux(const QuadraturePoint<Quadrature>& x, const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return l_(x, args...);
        }

        //!Helper in order to use parametrized expression templates.
        template<class Quadrature>
        auto operator()(const QuadraturePoint<Quadrature>& x, const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return l_(x, args...);
        }

       private:
        const TaggedModelMethod<Model, LinearizedTag<tag>::value, SequenceMask<pointIndex, OuterArgsIdx...>::value, F> l_;
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, flux,
                        IndexSequence<>,
                        IndexSequence<OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<!Traits<Model>::template HasMethod<flux>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::flux;

        ModelMethod(const Model& model, const F& f = F())
          : l_(model, f)
        {}

        //!The proper method inherited by the derived model.
        auto flux(const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return l_(args...);
        }

        //!Helper in order to use parametrized expression templates.
        auto operator()(const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return l_(args...);
        }

       private:
        const TaggedModelMethod<Model, LinearizedTag<tag>::value, SequenceMask<OuterArgsIdx...>::value, F> l_;
      };

      /**@} */

      /**@name LinearizedFluxCaller
       *
       * @copydoc FluxCaller
       *
       * @{
       */

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... LinArgsIdx, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, linearizedFlux,
                        IndexSequence<LinArgsIdx...>,
                        IndexSequence<pointIndex, OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<Traits<Model>::template HasMethod<linearizedFlux>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::linearizedFlux;

        ModelMethod(const Model& model, const F& f = F())
          : model_(model), f_(f)
        {}

        //!The proper method inherited by the derived model.
        template<class Quadrature>
        auto linearizedFlux(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                            const QuadraturePoint<Quadrature>& x,
                            const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return f_(model_.linearizedFlux(std::get<InnerArgsIdx>(std::forward_as_tuple(linArgs..., x, args...))...));
        }

        //!Helper in order to use parametrized expression templates.
        template<class Quadrature>
        auto operator()(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                        const QuadraturePoint<Quadrature>& x,
                        const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return linearizedFlux(linArgs..., x, args...);
        }

       private:
        const Model& model_;
        const F& f_;
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... LinArgsIdx, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, linearizedFlux,
                        IndexSequence<LinArgsIdx...>,
                        IndexSequence<OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<Traits<Model>::template HasMethod<linearizedFlux>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::linearizedFlux;

        ModelMethod(const Model& model, const F& f = F())
          : model_(model), f_(f)
        {}

        //!The proper method inherited by the derived model.
        auto linearizedFlux(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                            const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return f_(model_.linearizedFlux(std::get<InnerArgsIdx>(std::forward_as_tuple(linArgs..., args...))...));
        }

        //!Helper in order to use parametrized expression templates.
        auto operator()(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                        const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return linearizedFlux(linArgs..., args...);
        }

       private:
        const Model& model_;
        const F& f_;
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... LinArgsIdx, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, linearizedFlux,
                        IndexSequence<LinArgsIdx...>,
                        IndexSequence<pointIndex, OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<!Traits<Model>::template HasMethod<linearizedFlux>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::linearizedFlux;

        ModelMethod(const Model& model, const F& f = F())
        {}

        //!The proper method inherited by the derived model.
        template<class Quadrature>
        auto linearizedFlux(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                            const QuadraturePoint<Quadrature>& x,
                            const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return typename Traits<Model>::template ReturnType<tag>(0);
        }

        //!Helper in order to use parametrized expression templates.
        template<class Quadrature>
        auto operator()(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                        const QuadraturePoint<Quadrature>& x,
                        const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return typename Traits<Model>::template ReturnType<tag>(0);
        }
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... LinArgsIdx, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, linearizedFlux,
                        IndexSequence<LinArgsIdx...>,
                        IndexSequence<OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<!Traits<Model>::template HasMethod<linearizedFlux>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::linearizedFlux;

        ModelMethod(const Model& model, const F& f = F())
        {}

        //!The proper method inherited by the derived model.
        auto linearizedFlux(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                            const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return typename Traits<Model>::template ReturnType<tag>(0);
        }

        //!Helper in order to use parametrized expression templates.
        auto operator()(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                        const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return typename Traits<Model>::template ReturnType<tag>(0);
        }
      };

      /**@} */

      /**@name SourceCaller
       *
       * @copydoc FluxCaller
       *
       * @{
       */

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, source,
                        IndexSequence<>,
                        IndexSequence<pointIndex, OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<Traits<Model>::template HasMethod<source>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::source;

        ModelMethod(const Model& model, const F& f = F())
          : model_(model), f_(f)
        {}

        //!The proper method inherited by the derived model.
        template<class Quadrature>
        auto source(const QuadraturePoint<Quadrature>& x, const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return f_(model_.source(std::get<InnerArgsIdx>(std::forward_as_tuple(x, args...))...));
        }

        //!Helper in order to use parametrized expression templates.
        template<class Quadrature>
        auto operator()(const QuadraturePoint<Quadrature>& x, const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return source(x, args...);
        }

       private:
        const Model& model_;
        const F& f_;
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, source,
                        IndexSequence<>,
                        IndexSequence<OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<Traits<Model>::template HasMethod<source>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::source;

        ModelMethod(const Model& model, const F& f = F())
          : model_(model), f_(f)
        {}

        //!The proper method inherited by the derived model.
        auto source(const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return f_(model_.source(std::get<InnerArgsIdx>(std::forward_as_tuple(args...))...));
        }

        //!Helper in order to use parametrized expression templates.
        auto operator()(const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return source(args...);
        }

       private:
        const Model& model_;
        const F& f_;
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, source,
                        IndexSequence<>,
                        IndexSequence<pointIndex, OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<!Traits<Model>::template HasMethod<source>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::source;

        ModelMethod(const Model& model, const F& f = F())
          : l_(model, f)
        {}

        //!The proper method inherited by the derived model.
        template<class Quadrature>
        auto source(const QuadraturePoint<Quadrature>& x, const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return l_(x, args...);
        }

        //!Helper in order to use parametrized expression templates.
        template<class Quadrature>
        auto operator()(const QuadraturePoint<Quadrature>& x, const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return l_(x, args...);
        }

       private:
        const TaggedModelMethod<Model, LinearizedTag<tag>::value, SequenceMask<pointIndex, OuterArgsIdx...>::value, F> l_;
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, source,
                        IndexSequence<>,
                        IndexSequence<OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<!Traits<Model>::template HasMethod<source>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::source;

        ModelMethod(const Model& model, const F& f = F())
          : l_(model, f)
        {}

        //!The proper method inherited by the derived model.
        auto source(const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return l_(args...);
        }

        //!Helper in order to use parametrized expression templates.
        auto operator()(const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return l_(args...);
        }

       private:
        const TaggedModelMethod<Model, LinearizedTag<tag>::value, SequenceMask<OuterArgsIdx...>::value, F> l_;
      };

      /**@} */

      /**@name LinearizedSourceCaller
       *
       * @copydoc FluxCaller
       *
       * @{
       */

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... LinArgsIdx, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, linearizedSource,
                        IndexSequence<LinArgsIdx...>,
                        IndexSequence<pointIndex, OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<Traits<Model>::template HasMethod<linearizedSource>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::linearizedSource;

        ModelMethod(const Model& model, const F& f = F())
          : model_(model), f_(f)
        {}

        //!The proper method inherited by the derived model.
        template<class Quadrature>
        auto linearizedSource(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                              const QuadraturePoint<Quadrature>& x,
                              const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return f_(model_.linearizedSource(std::get<InnerArgsIdx>(std::forward_as_tuple(linArgs..., x, args...))...));
        }

        //!Helper in order to use parametrized expression templates.
        template<class Quadrature>
        auto operator()(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                        const QuadraturePoint<Quadrature>& x,
                        const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return linearizedSource(linArgs..., x, args...);
        }

       private:
        const Model& model_;
        const F& f_;
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... LinArgsIdx, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, linearizedSource,
                        IndexSequence<LinArgsIdx...>,
                        IndexSequence<OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<Traits<Model>::template HasMethod<linearizedSource>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::linearizedSource;

        ModelMethod(const Model& model, const F& f = F())
          : model_(model), f_(f)
        {}

        //!The proper method inherited by the derived model.
        auto linearizedSource(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                              const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return f_(model_.linearizedSource(std::get<InnerArgsIdx>(std::forward_as_tuple(linArgs..., args...))...));
        }

        //!Helper in order to use parametrized expression templates.
        auto operator()(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                        const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return linearizedSource(linArgs..., args...);
        }

       private:
        const Model& model_;
        const F& f_;
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... LinArgsIdx, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, linearizedSource,
                        IndexSequence<LinArgsIdx...>,
                        IndexSequence<pointIndex, OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<!Traits<Model>::template HasMethod<linearizedSource>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::linearizedSource;

        ModelMethod(const Model& model, const F& f = F())
        {}

        //!The proper method inherited by the derived model.
        template<class Quadrature>
        auto linearizedSource(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                              const QuadraturePoint<Quadrature>& x,
                              const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return typename Traits<Model>::template ReturnType<tag>(0);
        }

        //!Helper in order to use parametrized expression templates.
        template<class Quadrature>
        auto operator()(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                        const QuadraturePoint<Quadrature>& x,
                        const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return typename Traits<Model>::template ReturnType<tag>(0);
        }
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... LinArgsIdx, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, linearizedSource,
                        IndexSequence<LinArgsIdx...>,
                        IndexSequence<OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<!Traits<Model>::template HasMethod<linearizedSource>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::linearizedSource;

        ModelMethod(const Model& model, const F& f = F())
        {}

        //!The proper method inherited by the derived model.
        auto linearizedSource(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                              const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return typename Traits<Model>::template ReturnType<tag>(0);
        }

        //!Helper in order to use parametrized expression templates.
        auto operator()(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                        const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return typename Traits<Model>::template ReturnType<tag>(0);
        }
      };

      /**@} */

      /**@name RobinFluxCaller
       *
       * @copydoc FluxCaller
       *
       * @{
       */

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, robinFlux,
                        IndexSequence<>,
                        IndexSequence<pointIndex, OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<Traits<Model>::template HasMethod<robinFlux>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::robinFlux;

        ModelMethod(const Model& model, const F& f = F())
          : model_(model), f_(f)
        {}

        //!The proper method inherited by the derived model.
        template<class Quadrature>
        auto robinFlux(const QuadraturePoint<Quadrature>& x, const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return f_(model_.robinFlux(std::get<InnerArgsIdx>(std::forward_as_tuple(x, args...))...));
        }

        //!Helper in order to use parametrized expression templates.
        template<class Quadrature>
        auto operator()(const QuadraturePoint<Quadrature>& x, const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return robinFlux(x, args...);
        }

       private:
        const Model& model_;
        const F& f_;
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, robinFlux,
                        IndexSequence<>,
                        IndexSequence<OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<Traits<Model>::template HasMethod<robinFlux>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::robinFlux;

        ModelMethod(const Model& model, const F& f = F())
          : model_(model), f_(f)
        {}

        //!The proper method inherited by the derived model.
        auto robinFlux(const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return f_(model_.robinFlux(std::get<InnerArgsIdx>(std::forward_as_tuple(args...))...));
        }

        //!Helper in order to use parametrized expression templates.
        auto operator()(const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return robinFlux(args...);
        }

       private:
        const Model& model_;
        const F& f_;
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, robinFlux,
                        IndexSequence<>,
                        IndexSequence<pointIndex, OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<!Traits<Model>::template HasMethod<robinFlux>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::robinFlux;

        ModelMethod(const Model& model, const F& f = F())
          : l_(model, f)
        {}

        //!The proper method inherited by the derived model.
        template<class Quadrature>
        auto robinFlux(const QuadraturePoint<Quadrature>& x, const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return l_(x, args...);
        }

        //!Helper in order to use parametrized expression templates.
        template<class Quadrature>
        auto operator()(const QuadraturePoint<Quadrature>& x, const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return l_(x, args...);
        }

       private:
        const TaggedModelMethod<Model, LinearizedTag<tag>::value, SequenceMask<pointIndex, OuterArgsIdx...>::value, F> l_;
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, robinFlux,
                        IndexSequence<>,
                        IndexSequence<OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<!Traits<Model>::template HasMethod<robinFlux>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::robinFlux;

        ModelMethod(const Model& model, const F& f = F())
          : l_(model, f)
        {}

        //!The proper method inherited by the derived model.
        auto robinFlux(const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return l_(args...);
        }

        //!Helper in order to use parametrized expression templates.
        auto operator()(const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return l_(args...);
        }

       private:
        const TaggedModelMethod<Model, LinearizedTag<tag>::value, SequenceMask<OuterArgsIdx...>::value, F> l_;
      };

      /**@} */

      /**@name LinearizedRobinFluxCaller
       *
       * @copydoc FluxCaller
       *
       * @{
       */

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... LinArgsIdx, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, linearizedRobinFlux,
                        IndexSequence<LinArgsIdx...>,
                        IndexSequence<pointIndex, OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<Traits<Model>::template HasMethod<linearizedRobinFlux>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::linearizedRobinFlux;

        ModelMethod(const Model& model, const F& f = F())
          : model_(model), f_(f)
        {}

        //!The proper method inherited by the derived model.
        template<class Quadrature>
        auto linearizedRobinFlux(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                                 const QuadraturePoint<Quadrature>& x,
                                 const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return f_(model_.linearizedRobinFlux(std::get<InnerArgsIdx>(std::forward_as_tuple(linArgs..., x, args...))...));
        }

        //!Helper in order to use parametrized expression templates.
        template<class Quadrature>
        auto operator()(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                        const QuadraturePoint<Quadrature>& x,
                        const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return linearizedRobinFlux(linArgs..., x, args...);
        }

       private:
        const Model& model_;
        const F& f_;
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... LinArgsIdx, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, linearizedRobinFlux,
                        IndexSequence<LinArgsIdx...>,
                        IndexSequence<OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<Traits<Model>::template HasMethod<linearizedRobinFlux>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::linearizedRobinFlux;

        ModelMethod(const Model& model, const F& f = F())
          : model_(model), f_(f)
        {}

        //!The proper method inherited by the derived model.
        auto linearizedRobinFlux(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                                 const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return f_(model_.linearizedRobinFlux(std::get<InnerArgsIdx>(std::forward_as_tuple(linArgs..., args...))...));
        }

        //!Helper in order to use parametrized expression templates.
        auto operator()(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                        const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return linearizedRobinFlux(linArgs..., args...);
        }

       private:
        const Model& model_;
        const F& f_;
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... LinArgsIdx, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, linearizedRobinFlux,
                        IndexSequence<LinArgsIdx...>,
                        IndexSequence<pointIndex, OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<!Traits<Model>::template HasMethod<linearizedRobinFlux>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::linearizedRobinFlux;

        ModelMethod(const Model& model, const F& f = F())
        {}

        //!The proper method inherited by the derived model.
        template<class Quadrature>
        auto linearizedRobinFlux(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                                 const QuadraturePoint<Quadrature>& x,
                                 const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return typename Traits<Model>::template ReturnType<tag>(0);
        }

        //!Helper in order to use parametrized expression templates.
        template<class Quadrature>
        auto operator()(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                        const QuadraturePoint<Quadrature>& x,
                        const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return typename Traits<Model>::template ReturnType<tag>(0);
        }
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... LinArgsIdx, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, linearizedRobinFlux,
                        IndexSequence<LinArgsIdx...>,
                        IndexSequence<OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<!Traits<Model>::template HasMethod<linearizedRobinFlux>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::linearizedRobinFlux;

        ModelMethod(const Model& model, const F& f = F())
        {}

        //!The proper method inherited by the derived model.
        auto linearizedRobinFlux(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                                 const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return typename Traits<Model>::template ReturnType<tag>(0);
        }

        //!Helper in order to use parametrized expression templates.
        auto operator()(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                        const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return typename Traits<Model>::template ReturnType<tag>(0);
        }
      };

      /**@} */

      /**@name SingularFluxCaller
       *
       * @copydoc FluxCaller
       *
       * @{
       */

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, singularFlux,
                        IndexSequence<>,
                        IndexSequence<pointIndex, OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<Traits<Model>::template HasMethod<singularFlux>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::singularFlux;

        ModelMethod(const Model& model, const F& f = F())
          : model_(model), f_(f)
        {}

        //!The proper method inherited by the derived model.
        template<class Quadrature>
        auto singularFlux(const QuadraturePoint<Quadrature>& x, const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return f_(model_.singularFlux(std::get<InnerArgsIdx>(std::forward_as_tuple(x, args...))...));
        }

        //!Helper in order to use parametrized expression templates.
        template<class Quadrature>
        auto operator()(const QuadraturePoint<Quadrature>& x, const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return singularFlux(x, args...);
        }

       private:
        const Model& model_;
        const F& f_;
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, singularFlux,
                        IndexSequence<>,
                        IndexSequence<OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<Traits<Model>::template HasMethod<singularFlux>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::singularFlux;

        ModelMethod(const Model& model, const F& f = F())
          : model_(model), f_(f)
        {}

        //!The proper method inherited by the derived model.
        auto singularFlux(const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return f_(model_.singularFlux(std::get<InnerArgsIdx>(std::forward_as_tuple(args...))...));
        }

        //!Helper in order to use parametrized expression templates.
        auto operator()(const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return singularFlux(args...);
        }

       private:
        const Model& model_;
        const F& f_;
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, singularFlux,
                        IndexSequence<>,
                        IndexSequence<pointIndex, OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<!Traits<Model>::template HasMethod<singularFlux>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::singularFlux;

        ModelMethod(const Model& model, const F& f = F())
          : l_(model, f)
        {}

        //!The proper method inherited by the derived model.
        template<class Quadrature>
        auto singularFlux(const QuadraturePoint<Quadrature>& x, const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return l_(x, args...);
        }

        //!Helper in order to use parametrized expression templates.
        template<class Quadrature>
        auto operator()(const QuadraturePoint<Quadrature>& x, const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return l_(x, args...);
        }

       private:
        const TaggedModelMethod<Model, LinearizedTag<tag>::value, SequenceMask<pointIndex, OuterArgsIdx...>::value, F> l_;
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, singularFlux,
                        IndexSequence<>,
                        IndexSequence<OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<!Traits<Model>::template HasMethod<singularFlux>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::singularFlux;

        ModelMethod(const Model& model, const F& f = F())
          : l_(model, f)
        {}

        //!The proper method inherited by the derived model.
        auto singularFlux(const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return l_(args...);
        }

        //!Helper in order to use parametrized expression templates.
        auto operator()(const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return l_(args...);
        }

       private:
        const TaggedModelMethod<Model, LinearizedTag<tag>::value, SequenceMask<OuterArgsIdx...>::value, F> l_;
      };

      /**@} */

      /**@name LinearizedSingularFluxCaller
       *
       * @copydoc FluxCaller
       *
       * @{
       */

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... LinArgsIdx, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, linearizedSingularFlux,
                        IndexSequence<LinArgsIdx...>,
                        IndexSequence<pointIndex, OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<Traits<Model>::template HasMethod<linearizedSingularFlux>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::linearizedSingularFlux;

        ModelMethod(const Model& model, const F& f = F())
          : model_(model), f_(f)
        {}

        //!The proper method inherited by the derived model.
        template<class Quadrature>
        auto linearizedSingularFlux(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                                    const QuadraturePoint<Quadrature>& x,
                                    const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return f_(model_.linearizedSingularFlux(std::get<InnerArgsIdx>(std::forward_as_tuple(linArgs..., x, args...))...));
        }

        //!Helper in order to use parametrized expression templates.
        template<class Quadrature>
        auto operator()(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                        const QuadraturePoint<Quadrature>& x,
                        const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return linearizedSingularFlux(linArgs..., x, args...);
        }

       private:
        const Model& model_;
        const F& f_;
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... LinArgsIdx, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, linearizedSingularFlux,
                        IndexSequence<LinArgsIdx...>,
                        IndexSequence<OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<Traits<Model>::template HasMethod<linearizedSingularFlux>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::linearizedSingularFlux;

        ModelMethod(const Model& model, const F& f = F())
          : model_(model), f_(f)
        {}

        //!The proper method inherited by the derived model.
        auto linearizedSingularFlux(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                                    const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return f_(model_.linearizedSingularFlux(std::get<InnerArgsIdx>(std::forward_as_tuple(linArgs..., args...))...));
        }

        //!Helper in order to use parametrized expression templates.
        auto operator()(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                        const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return linearizedSingularFlux(linArgs..., args...);
        }

       private:
        const Model& model_;
        const F& f_;
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... LinArgsIdx, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, linearizedSingularFlux,
                        IndexSequence<LinArgsIdx...>,
                        IndexSequence<pointIndex, OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<!Traits<Model>::template HasMethod<linearizedSingularFlux>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::linearizedSingularFlux;

        ModelMethod(const Model& model, const F& f = F())
        {}

        //!The proper method inherited by the derived model.
        template<class Quadrature>
        auto linearizedSingularFlux(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                                    const QuadraturePoint<Quadrature>& x,
                                    const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return typename Traits<Model>::template ReturnType<tag>(0);
        }

        //!Helper in order to use parametrized expression templates.
        template<class Quadrature>
        auto operator()(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                        const QuadraturePoint<Quadrature>& x,
                        const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return typename Traits<Model>::template ReturnType<tag>(0);
        }
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... LinArgsIdx, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, linearizedSingularFlux,
                        IndexSequence<LinArgsIdx...>,
                        IndexSequence<OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<!Traits<Model>::template HasMethod<linearizedSingularFlux>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::linearizedSingularFlux;

        ModelMethod(const Model& model, const F& f = F())
        {}

        //!The proper method inherited by the derived model.
        auto linearizedSingularFlux(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                                    const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return typename Traits<Model>::template ReturnType<tag>(0);
        }

        //!Helper in order to use parametrized expression templates.
        auto operator()(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                        const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return typename Traits<Model>::template ReturnType<tag>(0);
        }
      };

      /**@} */

      /**@name DirichletCaller
       *
       * @copydoc FluxCaller
       *
       * @{
       */

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, ModelIntrospection::dirichlet,
                        IndexSequence<>,
                        IndexSequence<pointIndex, OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<Traits<Model>::template HasMethod<ModelIntrospection::dirichlet>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::dirichlet;

        ModelMethod(const Model& model, const F& f = F())
          : model_(model), f_(f)
        {}

        //!The proper method inherited by the derived model.
        template<class Quadrature>
        auto dirichlet(const QuadraturePoint<Quadrature>& x, const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return f_(model_.dirichlet(std::get<InnerArgsIdx>(std::forward_as_tuple(x, args...))...));
        }

        //!Helper in order to use parametrized expression templates.
        template<class Quadrature>
        auto operator()(const QuadraturePoint<Quadrature>& x, const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return dirichlet(x, args...);
        }

       private:
        const Model& model_;
        const F& f_;
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, ModelIntrospection::dirichlet,
                        IndexSequence<>,
                        IndexSequence<OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<Traits<Model>::template HasMethod<ModelIntrospection::dirichlet>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::dirichlet;

        ModelMethod(const Model& model, const F& f = F())
          : model_(model), f_(f)
        {}

        //!The proper method inherited by the derived model.
        auto dirichlet(const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return f_(model_.dirichlet(std::get<InnerArgsIdx>(std::forward_as_tuple(args...))...));
        }

        //!Helper in order to use parametrized expression templates.
        auto operator()(const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return dirichlet(args...);
        }

       private:
        const Model& model_;
        const F& f_;
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, ModelIntrospection::dirichlet,
                        IndexSequence<>,
                        IndexSequence<pointIndex, OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<!Traits<Model>::template HasMethod<ModelIntrospection::dirichlet>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::dirichlet;

        ModelMethod(const Model& model, const F& f = F())
          : l_(model, f)
        {}

        //!The proper method inherited by the derived model.
        template<class Quadrature>
        auto dirichlet(const QuadraturePoint<Quadrature>& x, const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return l_(x, args...);
        }

        //!Helper in order to use parametrized expression templates.
        template<class Quadrature>
        auto operator()(const QuadraturePoint<Quadrature>& x, const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return l_(x, args...);
        }

       private:
        const TaggedModelMethod<Model, LinearizedTag<tag>::value, SequenceMask<pointIndex, OuterArgsIdx...>::value, F> l_;
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, ModelIntrospection::dirichlet,
                        IndexSequence<>,
                        IndexSequence<OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<!Traits<Model>::template HasMethod<ModelIntrospection::dirichlet>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::dirichlet;

        ModelMethod(const Model& model, const F& f = F())
          : l_(model, f)
        {}

        //!The proper method inherited by the derived model.
        auto dirichlet(const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return l_(args...);
        }

        //!Helper in order to use parametrized expression templates.
        auto operator()(const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return l_(args...);
        }

       private:
        const TaggedModelMethod<Model, LinearizedTag<tag>::value, SequenceMask<OuterArgsIdx...>::value, F> l_;
      };

      /**@} */

      /**@name LinearizedDirichletCaller
       *
       * @copydoc FluxCaller
       *
       * @{
       */

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... LinArgsIdx, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, linearizedDirichlet,
                        IndexSequence<LinArgsIdx...>,
                        IndexSequence<pointIndex, OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<Traits<Model>::template HasMethod<linearizedDirichlet>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::linearizedDirichlet;

        ModelMethod(const Model& model, const F& f = F())
          : model_(model), f_(f)
        {}

        //!The proper method inherited by the derived model.
        template<class Quadrature>
        auto linearizedDirichlet(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                                 const QuadraturePoint<Quadrature>& x,
                                 const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return f_(model_.linearizedDirichlet(std::get<InnerArgsIdx>(std::forward_as_tuple(linArgs..., x, args...))...));
        }

        //!Helper in order to use parametrized expression templates.
        template<class Quadrature>
        auto operator()(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                        const QuadraturePoint<Quadrature>& x,
                        const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return linearizedDirichlet(linArgs..., x, args...);
        }

       private:
        const Model& model_;
        const F& f_;
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... LinArgsIdx, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, linearizedDirichlet,
                        IndexSequence<LinArgsIdx...>,
                        IndexSequence<OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<Traits<Model>::template HasMethod<linearizedDirichlet>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::linearizedDirichlet;

        ModelMethod(const Model& model, const F& f = F())
          : model_(model), f_(f)
        {}

        //!The proper method inherited by the derived model.
        auto linearizedDirichlet(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                                 const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return f_(model_.linearizedDirichlet(std::get<InnerArgsIdx>(std::forward_as_tuple(linArgs..., args...))...));
        }

        //!Helper in order to use parametrized expression templates.
        auto operator()(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                        const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return linearizedDirichlet(linArgs..., args...);
        }

       private:
        const Model& model_;
        const F& f_;
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... LinArgsIdx, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, linearizedDirichlet,
                        IndexSequence<LinArgsIdx...>,
                        IndexSequence<pointIndex, OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<!Traits<Model>::template HasMethod<linearizedDirichlet>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::linearizedDirichlet;

        ModelMethod(const Model& model, const F& f = F())
        {}

        //!The proper method inherited by the derived model.
        template<class Quadrature>
        auto linearizedDirichlet(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                                 const QuadraturePoint<Quadrature>& x,
                                 const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return typename Traits<Model>::template ReturnType<tag>(0);
        }

        //!Helper in order to use parametrized expression templates.
        template<class Quadrature>
        auto operator()(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                        const QuadraturePoint<Quadrature>& x,
                        const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return typename Traits<Model>::template ReturnType<tag>(0);
        }
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... LinArgsIdx, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, linearizedDirichlet,
                        IndexSequence<LinArgsIdx...>,
                        IndexSequence<OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<!Traits<Model>::template HasMethod<linearizedDirichlet>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::linearizedDirichlet;

        ModelMethod(const Model& model, const F& f = F())
        {}

        //!The proper method inherited by the derived model.
        auto linearizedDirichlet(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                                 const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return typename Traits<Model>::template ReturnType<tag>(0);
        }

        //!Helper in order to use parametrized expression templates.
        auto operator()(const TupleElement<LinArgsIdx, AllArgs<Model> >&... linArgs,
                        const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return typename Traits<Model>::template ReturnType<tag>(0);
        }
      };

      /**@} */

      /**@name FluxDivergenceCaller
       *
       * @copydoc FluxCaller
       *
       * @{
       */

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, fluxDivergence,
                        IndexSequence<>,
                        IndexSequence<pointIndex, OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<Traits<Model>::template HasMethod<fluxDivergence>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::fluxDivergence;

        ModelMethod(const Model& model, const F& f = F())
          : model_(model), f_(f)
        {}

        //!The proper method inherited by the derived model.
        template<class Quadrature>
        auto fluxDivergence(const QuadraturePoint<Quadrature>& x, const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return f_(model_.fluxDivergence(std::get<InnerArgsIdx>(std::forward_as_tuple(x, args...))...));
        }

        //!Helper in order to use parametrized expression templates.
        template<class Quadrature>
        auto operator()(const QuadraturePoint<Quadrature>& x, const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return fluxDivergence(x, args...);
        }

       private:
        const Model& model_;
        const F& f_;
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, fluxDivergence,
                        IndexSequence<>,
                        IndexSequence<OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<Traits<Model>::template HasMethod<fluxDivergence>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::fluxDivergence;

        ModelMethod(const Model& model, const F& f = F())
          : model_(model), f_(f)
        {}

        //!The proper method inherited by the derived model.
        auto fluxDivergence(const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return f_(model_.fluxDivergence(std::get<InnerArgsIdx>(std::forward_as_tuple(args...))...));
        }

        //!Helper in order to use parametrized expression templates.
        auto operator()(const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return fluxDivergence(args...);
        }

       private:
        const Model& model_;
        const F& f_;
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, fluxDivergence,
                        IndexSequence<>,
                        IndexSequence<pointIndex, OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<!Traits<Model>::template HasMethod<fluxDivergence>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::fluxDivergence;

        ModelMethod(const Model& model, const F& f = F())
        {}

        //!The proper method inherited by the derived model.
        template<class Quadrature>
        auto fluxDivergence(const QuadraturePoint<Quadrature>& x, const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return typename Traits<Model>::template ReturnType<tag>(0);
        }

        //!Helper in order to use parametrized expression templates.
        template<class Quadrature>
        auto operator()(const QuadraturePoint<Quadrature>& x, const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return typename Traits<Model>::template ReturnType<tag>(0);
        }
      };

      /**@copydoc ModelMethod*/
      template<class Model, class F, std::size_t... OuterArgsIdx, std::size_t... InnerArgsIdx>
      class ModelMethod<Model, F, fluxDivergence,
                        IndexSequence<>,
                        IndexSequence<OuterArgsIdx...>,
                        IndexSequence<InnerArgsIdx...>,
                        std::enable_if_t<!Traits<Model>::template HasMethod<fluxDivergence>::value> >
      {
       public:
        static constexpr MethodTag tag = ModelIntrospection::fluxDivergence;

        ModelMethod(const Model& model, const F& f = F())
        {}

        //!The proper method inherited by the derived model.
        auto fluxDivergence(const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return typename Traits<Model>::template ReturnType<tag>(0);
        }

        //!Helper in order to use parametrized expression templates.
        auto operator()(const TupleElement<OuterArgsIdx, AllArgs<Model> >&... args) const
        {
          return typename Traits<Model>::template ReturnType<tag>(0);
        }
      };

      /**@} */

      /**General unary model expression template.
       *
       * @param[in] Model Type of model the unary expression is
       * applied to.
       *
       * @param[in] ReturnValueFunctor Functor to apply to the return
       * value of the origianl model-method. This essentially is the
       * implementation of the unary operation.
       *
       * @param[in] Methods Methods that must be implemented by the
       * resulting model. Defaults to the methods implemented by Model.
       *
       * @param[in] Signatures Call-signatures that must be
       * implemented by the result-model. The indices contained in
       * Methods are the index into this sequence. Defaults to the
       * call-signatures acutall implemented by Model.
       *
       * @param[in] Disable Dummy parameter for std::enable_if_t
       */
      template<class ReturnValueFunctor,
               class Model,
               class Methods = typename ModelTraits<Model>::Methods,
               class Signatures = typename ModelTraits<Model>::MethodCallSignatures>
      class UnaryModelExpression;

      /**Index-pack expansion helper and actual implementation.*/
      template<class F, class Model, std::size_t... tag, class Signatures>
      class UnaryModelExpression<F, Model, IndexSequence<tag...>, Signatures>
        : public ModelBase<typename std::decay_t<Model>::DomainFunctionSpaceType,
                           typename std::decay_t<Model>::RangeFunctionSpaceType>
        , public Expressions::Storage<F, Model>
        // Expressions model rvalues. We are constant if the
        // underlying expression claims to be so or if we store a copy
        // and the underlying expression is "independent"
        // (i.e. determined at runtime only by the contained data), or
        // if the value of the expression is just a compile-time
        // constant.
        , public MPL::UniqueTags<
            typename AssumeTraits<F>::Properties,
            ConditionalType<(IsConstantExprArg<Model>::value
                             && !FunctorHas<IsIndeterminateOperation, F>::value
                             && !FunctorHas<IsPlaceholderOperation, F>::value
              ), ConstantExpression, void>,
            ConditionalType<(IsTypedValue<Model>::value
                             && !FunctorHas<IsDynamicOperation, F>::value
                             && !FunctorHas<IsIndeterminateOperation, F>::value
                             && !FunctorHas<IsPlaceholderOperation, F>::value
              ), TypedValueExpression, void> >
        // end of tag promotion
        , public TaggedModelMethod<std::decay_t<Model>, tag, Get<tag, Signatures>::value, F>...
      {
        using StorageType = Expressions::Storage<F, Model>;
       protected:
        using StorageType::t0_;
       public:
        using StorageType::operand;
        using StorageType::operation;
        using typename StorageType::OperationType;
        using typename StorageType::FunctorType;
        using TraitsType = ModelTraits<Model>;
        using UnaryOperationType = typename F::OperationType;
        using ModelType = Model;
        using ModelDecay = std::decay_t<Model>;

        /* Models need to be captured by value as the entire
         * expression must be copyable in a way that bind(otherEntity)
         * does not affect the original copy.
         */
        static_assert(std::is_same<ModelType, ModelDecay>::value,
                      "Models need to be captured by value.");

        /**Constructor from given model and operation.
         *
         * @param[in] model The model to operator on.
         *
         * @param[in] f The functor to apply to the model. The functor
         * type defaults to OperationTraits<IdentityOperation> and
         * the value for @a f defaults to the default-construction of
         * the functor which need not exist. In the latter case a
         * properly constructed functor object has to be passed to the
         * constructor.
         */
        template<class ModelArg, class FArg, std::enable_if_t<std::is_constructible<StorageType, FArg, ModelArg>::value, int> = 0>
        UnaryModelExpression(ModelArg&& model, FArg&& f)
          : StorageType(std::forward<FArg>(f), std::forward<ModelArg>(model))
          , TaggedModelMethod<ModelDecay, tag, Get<tag, Signatures>::value, F>(this->model(), operation())...
        {}

        template<class ModelArg, std::enable_if_t<std::is_constructible<StorageType, F, ModelArg>::value, int> = 0>
        UnaryModelExpression(ModelArg&& model)
          : UnaryModelExpression(std::forward<ModelArg>(model), F{})
        {}

        /**Copy constructor. Copy model and functor from @a other and
         * reconstruct the "method-providers".
         */
        UnaryModelExpression(const UnaryModelExpression& other)
          : StorageType(other)
          , TaggedModelMethod<ModelDecay, tag, Get<tag, Signatures>::value, F>(model(), operation())...
        {}

        /**Move constructor. Move model and functor from @a other and
         * reconstruct the "method-providers".
         */
        UnaryModelExpression(UnaryModelExpression&& other)
          : StorageType(std::move(other))
          , TaggedModelMethod<ModelDecay, tag, Get<tag, Signatures>::value, F>(model(), operation())...
        {}

        UnaryModelExpression& operator=(const UnaryModelExpression& other)
        {
          static_cast<StorageType>(*this) = other;
          return *this;
        }

        UnaryModelExpression& operator=(UnaryModelExpression&& other)
        {
          static_cast<StorageType>(*this) = std::move(other);
          return *this;
        }

        /**Type-cast to underlying model.*/
        template<class T,
                 std::enable_if_t<(Expressions::EmitByValueV<T>
                                   && std::is_same<T, ModelDecay>::value
                                   && (IsIdentityOperation<OperationType>::value
                                       ||
                                       IsClosureOperation<OperationType>::value)
          ), int> = 0>
        operator T () const { return operand(0_c); }

        /**Type-cast to underlying model.*/
        template<class T,
                 std::enable_if_t<(!Expressions::EmitByValueV<T>
                                   && std::is_same<T, ModelDecay>::value
                                   && (IsIdentityOperation<OperationType>::value
                                       ||
                                       IsClosureOperation<OperationType>::value)
          ), int> = 0>
        operator T () && { return operand(0_c); }

        /**Type-cast to underlying model.*/
        template<class T,
                 std::enable_if_t<(!Expressions::EmitByValueV<T>
                                   && std::is_same<T, ModelDecay>::value
                                   && (IsIdentityOperation<OperationType>::value
                                       ||
                                       IsClosureOperation<OperationType>::value)
          ), int> = 0>
        operator T& () & { return operand(0_c); }

        /**Type-cast to underlying model.*/
        template<class T,
                 std::enable_if_t<(!Expressions::EmitByValueV<T>
                                   && std::is_same<T, ModelDecay>::value
                                   && (IsIdentityOperation<OperationType>::value
                                       ||
                                       IsClosureOperation<OperationType>::value)
          ), int> = 0>
        operator const T& () const& { return operand(0_c); }

        template<class Entity>
        void bind(const Entity& entity)
        {
          model().bind(entity);
        }

        void unbind()
        {
          model().unbind();
        }

        template<class Intersection>
        auto classifyBoundary(const Intersection& intersection)
        {
          return model().classifyBoundary(intersection);
        }

        std::string name() const
        {
          return operationName(operation(), model().name());
        }

       protected:
        constexpr auto&& model() && { return t0_; }
        constexpr auto& model() & { return t0_; }
        constexpr const auto& model() const& { return t0_; }
      };

      /**Generate a unary model expression.
       *
       * @param t The operand.
       *
       * @param f The operation functor.
       */
      template<class F, class T, std::enable_if_t<IsPDEModel<T>::value, int> = 0>
      constexpr auto operate(Expressions::DontOptimize, F, const T& t)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        return UnaryModelExpression<F, T>(t, F{});
      }

    } // PDEModel

    /**Exports from namespace.*/
    template<class Model,
             std::size_t tag,
             std::size_t signature = Get<tag, typename ModelTraits<Model>::MethodCallSignatures>::value>
    using ModelMethod = PDEModel::TaggedModelMethod<Model, tag, signature>;

    /**Exports from namespace.*/
    template<class Model, std::size_t tag>
    using ModelClosureMethod = PDEModel::TaggedModelMethod<
      Model, tag, ModelIntrospection::MethodSignatureClosure<tag>::value>;

    /**Exports from namespace.*/
    template<class F, class Model>
    using UnaryModelExpression = PDEModel::UnaryModelExpression<F, Model>;

    //!@} ModelExpressions

    //!@} ExpressionTemplates

  } // namespace ACFem

} // namespace Dune


#endif //  __DUNE_ACFEM_MODELES_UNARYEXPRESSION_HH__
