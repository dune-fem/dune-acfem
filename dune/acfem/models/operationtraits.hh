#ifndef __DUNE_ACFEM_MODELS_OPERATIONTRAITS_HH__
#define __DUNE_ACFEM_MODELS_OPERATIONTRAITS_HH__

/**@file Add-ons for expressions operations which do not make sense without models.*/

#include "../common/types.hh"
#include "../common/literals.hh"
#include "../expressions/storage.hh"
#include "../expressions/expressionoperations.hh"

#include "expressionoperations.hh"
#include "operations/crop.hh"
#include "operations/closure.hh"
#include "operations/apply.hh"

namespace Dune {

  namespace ACFem {

    using namespace Literals;

    /**@addtogroup ExpressionTemplates
     *
     * @{
     *
     */

    /**@addtogroup ExpressionOperations
     *
     * "Tag"-structures for each supported algebraic operation, used
     * to distinguish the various expression template classes from
     * each other.
     *
     * @{
     */

    ////////////////////////////////

    template<std::size_t... MethodTags>
    struct OperationTraits<PDEModel::CropOperation<IndexSequence<MethodTags...> > >
    {
      using MethodsSequence = IndexSequence<MethodTags...>;
      using InvertibleOperation = void; // not invertible
      using OperationType = PDEModel::CropOperation<MethodsSequence>;

      template<class T, class SFINAE = void>
      struct HasOperation
        : FalseType
      {
        using ResultType = T;
      };

      template<class T>
      struct HasOperation<T, VoidType<decltype(PDEModel::crop<MethodTags...>(std::declval<T>()))> >
        : TrueType
      {
        using ResultType = std::decay_t<decltype(PDEModel::crop<MethodTags...>(std::declval<T>()))>;
      };

      template<class T>
      using ResultType = typename HasOperation<T>::ResultType;

      template<class Sign>
      static constexpr auto signPropagation(Sign)
      {
        return ConditionalType<sizeof...(MethodTags) == 0, ZeroExpressionSign, UnknownExpressionSign>{};
      }

      static std::string name()
      {
        return "crop<"+toString(MethodsSequence{})+">";
      }

      // This one just does not apply ...
      template<class Order>
      static constexpr auto polynomialOrder(Order oldOrder)
      {
        return oldOrder;
      }

      template<class T, std::enable_if_t<HasOperation<T>::value, int> = 0>
      auto operator()(T&& t) const
      {
        return PDEModel::crop<MethodTags...>(std::forward<T>(t));
      }

      template<class T, std::enable_if_t<!HasOperation<T>::value, int> = 0>
      constexpr decltype(auto) operator()(T&& t) const
      {
        return std::forward<T>(t);
      }
    };

    ///////////////////////////////////////////////////////////////////////////

    template<std::size_t... MethodTags, std::size_t... ClosureSignatures>
    struct OperationTraits<PDEModel::ClosureOperation<IndexSequence<MethodTags...>, IndexSequence<ClosureSignatures...> > >
    {
      using MethodsSequence = IndexSequence<MethodTags...>;
      using ClosureSequence = IndexSequence<ClosureSignatures...>;
      using InvertibleOperation = void; // not invertible
      using OperationType = PDEModel::ClosureOperation<MethodsSequence, ClosureSequence>;

      template<class T, class SFINAE = void>
      struct HasOperation
        : FalseType
      {
        using ResultType = T;
      };

      template<class T>
      struct HasOperation<T, VoidType<decltype(PDEModel::closure<MethodTags...>(std::declval<T>(), ClosureSequence{}))> >
        : TrueType
      {
        using ResultType = std::decay_t<decltype(PDEModel::closure<MethodTags...>(std::declval<T>(), ClosureSequence{}))>;
      };

      template<class T>
      using ResultType = typename HasOperation<T>::ResultType;

      template<class Sign>
      static constexpr auto signPropagation(Sign)
      {
        return Sign{};
      }

      static std::string name()
      {
        if constexpr (std::is_same<MethodsSequence, ModelAdmissibleMethods>::value
                      &&
                      std::is_same<ClosureSequence, ModelMethodClosureSignatures>::value) {
          // use a short name if this is the full closure
          return "cls";
        } else {
          return "cls<"+toString(MethodsSequence{})+"@"+toString(ClosureSequence{})+">";
        }
      }

      // This one just does not apply ...
      template<class Order>
      static constexpr auto polynomialOrder(Order oldOrder)
      {
        return oldOrder;
      }

      template<class T, std::enable_if_t<HasOperation<T>::value, int> = 0>
      auto operator()(T&& t) const
      {
        return PDEModel::closure<MethodTags...>(std::forward<T>(t), ClosureSequence{});
      }

      template<class T, std::enable_if_t<!HasOperation<T>::value, int> = 0>
      constexpr decltype(auto) operator()(T&& t) const
      {
        return std::forward<T>(t);
      }
    };

    template<class Flavour>
    struct OperationTraits<PDEModel::ApplyOperation<Flavour> >
    {
      using FlavourType = Flavour;
      using InvertibleOperation = void; // not invertible
      using OperationType = PDEModel::ApplyOperation<FlavourType>;

      template<class T0, class T1, class SFINAE = void>
      struct HasOperation
        : FalseType
      {
        using ResultType = decltype(std::declval<T0>()(std::declval<T1>()));
      };

      template<class T0, class T1>
      struct HasOperation<T0, T1, VoidType<decltype(PDEModel::apply<FlavourType>(std::declval<T0>(), std::declval<T1>()))> >
        : TrueType
      {
        using ResultType = std::decay_t<decltype(PDEModel::apply<FlavourType>(std::declval<T0>(), std::declval<T1>()))>;
      };

      template<class T0, class T1>
      using ResultType = typename HasOperation<T0, T1>::ResultType;

      template<class Sign0, class Sign1>
      static constexpr auto signPropagation(Sign0, Sign1)
      {
        return UnknownExpressionSign{};
      }

      static std::string name()
      {
        return std::string("apply") + (std::is_same<FlavourType, PDEModel::L2LoadFlavour>::value ? "<L2>" : "<Ritz>");
      }

      // This one just does not apply ...
      template<class Order0, class Order1>
      static constexpr auto polynomialOrder(Order0 oldOrder0, Order1 oldOrder1)
      {
        return 0_f;
      }

      template<class T0, class T1, std::enable_if_t<HasOperation<T0, T1>::value, int> = 0>
      auto operator()(T0&& t0, T1&& t1) const
      {
        return PDEModel::apply<FlavourType>(std::forward<T0>(t0), std::forward<T1>(t1));
      }

      template<class T0, class T1, std::enable_if_t<!HasOperation<T0, T1>::value, int> = 0>
      constexpr decltype(auto) operator()(T0&& t0, T1&& t1) const
      {
        return std::forward<T0>(t0)(std::forward<T1>(t1));
      }
    };

    ///////////////////////////////////////////////////////////////////////////

    //! @} ExpressionOperations

    //! @} ExpressionTemplates

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_MODELS_OPERATIONTRAITS_HH__
