#ifndef __DUNE_ACFEM_MODELS_MODULES_TRANSPORTMODEL_HH__
#define __DUNE_ACFEM_MODELS_MODULES_TRANSPORTMODEL_HH__

#include <dune/fem/function/localfunction/const.hh>

#include "../../expressions/terminal.hh"

#include "../modelbase.hh"

namespace Dune {

  namespace ACFem::PDEModel {

    /**@addtogroup PDE-Models
     * @{
     */

    /**@addtogroup BasicModels
     * @{
     */

    /**@addtogroup BulkModel
     * @{
     */

    /**\brief Define a model for an advection term.
     *
     * In formulas: this fragment implements the right-hand-side of
     * the following equation:
     *
     * @f[
     * \int_\Omega \nabla\cdot(B\,U)\phi
     * =
     * -
     * \int_\Omega B_i\,U_j\,\partial_i\phi_j
     * +
     * \int_{\partial\Omega} B\cdot\nu\,U\cdot\phi
     * @f]
     *
     * Of course, the formula uses sum-convention.
     *
     * \see IncompressibleTransportModel, \ref TransportModelPage
     *
     * @param Velocity This is a grid-function implementing B.
     */
    template<class FunctionSpace, class GridFunction>
    class TransportModel
      : public ModelBase<FunctionSpace>
      , public Expressions::SelfExpression<TransportModel<FunctionSpace, GridFunction> >
      , public MPL::UniqueTags<ConditionalType<IsConstantExprArg<GridFunction>::value, ConstantExpression, void>,
                               ConditionalType<IsTypedValue<GridFunction>::value, TypedValueExpression, void> >
    {
      static_assert(HasTag<GridFunction, Fem::HasLocalFunction>::value,
                    "GridFunction must provide a local function");

      using ThisType = TransportModel;
      using BaseType = ModelBase<FunctionSpace>;
      using LocalFunctionType = Fem::ConstLocalFunction<std::decay_t<GridFunction> >;
     public:
      using GridFunctionType = GridFunction;
      using GridFunctionDecay = std::decay_t<GridFunctionType>;
      using GridPartType = typename GridFunctionDecay::GridPartType;

      using typename BaseType::RangeFieldType;
      using typename BaseType::DomainType;
      using typename BaseType::RangeType;
      using typename BaseType::JacobianRangeType;
      using BaseType::dimDomain;
      using BaseType::dimRange;

      enum {
        dimWorld = GridPartType::dimensionworld
      };

      static_assert((int)GridFunctionDecay::FunctionSpaceType::dimRange == (int)dimWorld
                    &&
                    (int)dimDomain == (int)dimWorld,
                    "This is meant for dimensionworld vector-fields, "
                    "like fluids, deformations etc.");

      // Interface methods that need to be reimplemented

      template<class FunctionArg, std::enable_if_t<std::is_constructible<LocalFunctionType, FunctionArg>::value, int> = 0>
      TransportModel(FunctionArg&& function, const std::string& name = "")
        : localFunction_(std::forward<FunctionArg>(function))
        , name_(name == "" ? "(-U_iU_jD_iPhi_j+Bndry)" : name)
      {}

      std::string name() const
      {
        return name_;
      }

      //!@copydoc ModelFacade::bind()
      template<class Entity>
      void bind(const Entity& entity)
      {
        localFunction_.bind(entity);
      }

      /**@copydoc ModelFacade::unbind() */
      void unbind()
      {
        localFunction_.unbind();
      }

      //!@copydoc ModelFacade::classifyBoundary
      template<class Intersection>
      auto classifyBoundary(const Intersection& intersection)
      {
        // true is correct as higher-level code has to take care of
        // the Dirichlet-(non-Dirichlet) splitting of the boundary.
        return std::make_pair(true, std::bitset<dimRange>());
      }

      //!@copydoc ModelFacade::linearizedFlux()
      template<class Quadrature>
      auto linearizedFlux(const QuadraturePoint<Quadrature> &x, const RangeType& value) const
      {
        // We need to provide the tensor-product of value with the
        // value of function_
        const auto velocity = localFunction_.evaluate(x);

        JacobianRangeType flux = 0;
        for (int i = 0; i < dimRange; ++i) {
          for (int j = 0; j < dimWorld; ++j) {
            flux[i][j] = - value[i] * velocity[j];
          }
        }
        return flux;
      }

      /**!@copydoc ModelFacade::fluxDivergence()
       *
       * The implementation also works for transport-velocities which
       * are not divergence free.
       */
      template<class Quadrature>
      auto fluxDivergence(const QuadraturePoint<Quadrature> &x,
                          const RangeType& value,
                          const JacobianRangeType& jacobian) const
      {
        const auto velocity = localFunction_.evaluate(x);
        const auto velocityJacobian = localFunction_.jacobian(x);

        RangeType result;
        jacobian.mv(velocity, result);

        RangeFieldType velocityDivergence = 0.;
//        for (int i = 0; i < dimDomain; ++i) {
        for (int i = 0; i < dimDomain; ++i) {
          velocityDivergence += velocityJacobian[i][i];
        }
        result.axpy(velocityDivergence, value);

        return result;
      }

      //!@copydoc ModelFacade::linearizedRobinFlux()
      template<class Quadrature>
      auto linearizedRobinFlux(const QuadraturePoint<Quadrature> &x,
                               const DomainType& unitOuterNormal,
                               const RangeType& value) const
      {
        const auto velocity = localFunction_.evaluate(x);
        return RangeType(value) *= (velocity * unitOuterNormal); // scalar product
      }

     protected:
      LocalFunctionType localFunction_;
      std::string name_;
    };

    //@} BulkModel

    /**@addtogroup ModelGenerators
     * @{
     */

    /**\brief Generate an advection-model object.
     *
     * \copydetails massModel()
     *
     * @param[in] velocity The advection-velocity. This must already
     * be something with a localFunction() method.
     */
    template<class Object, class GridFunction>
    constexpr auto
    transportModel(const Object& object,
                   GridFunction&& velocity,
                   const std::string& name = "")
    {
      return expressionClosure(TransportModel<typename Object::FunctionSpaceType, GridFunction>(std::forward<GridFunction>(velocity), name));
    }

    template<class Object, class GridFunction, std::enable_if_t<ExpressionTraits<GridFunction>::isZero, int> = 0>
    constexpr auto
    transportModel(const Object& object,
                   GridFunction&& velocity,
                   const std::string& name = "")
    {
      return zeroModel(object);
    }

    //@} ModelGenerators

    //@} BasicModels

    //@} PDE-Models

  } // namespace ACFem::PDEModel

  namespace ACFem {

    using PDEModel::transportModel;

  }

}  //Namespace Dune


#endif // __DUNE_ACFEM_MODELS_MODULES_TRANSPORTMODEL_HH__
