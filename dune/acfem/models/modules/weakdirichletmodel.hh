#ifndef __DUNE_ACFEM_MODELS_MODULES_WEAKDIRICHLETMODEL_HH__
#define __DUNE_ACFEM_MODELS_MODULES_WEAKDIRICHLETMODEL_HH__

#include <dune/fem/io/parameter.hh>

#include "robinmodel.hh"

namespace Dune {

  namespace ACFem::PDEModel {

    /**@addtogroup ModelGenerators
     * @{
     */

    /**\brief Generate homogeneous WeakDirichlet boundary conditions
     *fitting the specified object.
     *
     * In order for this to work the data-type of the given object
     * must have the two sub-types Object::FunctionSpaceType and
     * Object::GridPartType and a method object.gridPart().
     *
     * @param[in] object Super-object, the WeakDirichletBoundaryModel
     * generated will be compatible to this object.
     *
     * @param[in] where Indicator which decides where the WeakDirichlet
     * b.c. apply.
     *
     * Examples of suitable objects are something that satisfies the
     *
     * - ModelInterface (another model)
     *
     * - Fem::DiscreteFunctionSpaceInterface
     *
     * - Fem::DiscreteFunctionInterface (including any wrapped or
     *   adapted non-discrete function)
     *
     * \see CompatibleModel.
     */
    template<class GridFunction, class Indicator = EntireBoundaryIndicator,
             std::enable_if_t<HasTag<GridFunction, Fem::HasLocalFunction>::value, int> = 0>
    auto
    weakDirichletBoundaryModel(GridFunction&& values,
                               Indicator&& where = Indicator{},
                               const double penalty = Dune::Fem::Parameter::getValue<double>("acfem.dgPenalty"))
    {
      return
        penalty
        * meshPenalty(values.gridPart())
        * robinBoundaryModel<GridFunction, Indicator>(std::forward<GridFunction>(values), std::forward<Indicator>(where));
    }

    template<class Object, class Indicator = EntireBoundaryIndicator,
             std::enable_if_t<!HasTag<Object, Fem::HasLocalFunction>::value, int> = 0>
    auto
    weakDirichletZeroModel(const Object& object,
                           Indicator&& where = Indicator{},
                           const double& penalty = Dune::Fem::Parameter::getValue<double>("acfem.dgPenalty"))
    {
      return weakDirichletBoundaryModel(zero(typename Object::FunctionSpaceType{}, object.gridPart()),
                                        std::forward<Indicator>(where),
                                        penalty);
    }

    //@} ModelGenerators

    //@} BasicModels

    //@} PDE-Models

  } // namespace ACFem

  namespace ACFem {

    using PDEModel::weakDirichletZeroModel;

  }

}  //Namespace Dune

#endif // __DUNE_ACFEM_MODELS_MODULES_WEAKDIRICHLETMODEL_HH__
