#ifndef __DUNE_ACFEM_MODELS_MODULES_MASSMODEL_HH__
#define __DUNE_ACFEM_MODELS_MODULES_MASSMODEL_HH__

#include "../../expressions/terminal.hh"

#include "../modelbase.hh"
#include "../expressiontraits.hh"

namespace Dune {

  namespace ACFem::PDEModel {

    /**@addtogroup PDE-Models
     * @{
     */

    /**@addtogroup BasicModels
     * @{
     */

    /**@addtogroup BulkModel
     * @{
     */

    /**Define a simple mass-model, given function-space and
     * grid-part. The model carries no boundary data, it simply
     * defines the mass-term. Intentionally, this can be used (e.g.)
     * to form a time discretization together with another model which
     * implements the elliptic part of the parabolic equation (e.g.,
     * if it is parabolic). See Dune-Project acfem-heat for an example.
     */
    template<class FunctionSpace>
    class MassModel
      : public ModelBase<FunctionSpace>
      , public Expressions::SelfExpression<MassModel<FunctionSpace> >
      , public MPL::UniqueTags<TypedValueExpression, PositiveExpression, SymmetricModel>
    {
      typedef MassModel ThisType;
      typedef ModelBase<FunctionSpace> BaseType;
     public:
      using typename BaseType::RangeType;

      MassModel(const std::string& name = "")
        : name_(name == "" ? "Mass" : name)
      {}

      std::string name() const
      {
        return name_;
      }

      //!@copydoc ModelFacade::linearizedSource()
      RangeType linearizedSource(const RangeType& value) const
      {
        return value;
      }

     protected:
      std::string name_;
    };

    //@} BulkModel

    /**@addtogroup ModelGenerators
     * @{
     */

    /**\brief Generate a mass model fitting the specified object.
     *
     * In order for this to work the data-type of the given object
     * must have the two sub-types Object::FunctionSpaceType and
     * Object::GridPartType. The actual instance of the object is
     * ignore and simply has to be passed in order that the compiler
     * can deduce the correct types.
     *
     * @param[in] object Ignored. We use only the type to get hold of
     * the FunctionSpaceType and the GridPartType.
     *
     * @param[in] name Optional. A descriptive name for the generated
     * model. A suitable default will be chosen if omitted.
     *
     * Examples of suitable objects are something that satisfies the
     *
     * - ModelInterface (another model)
     *
     * - Fem::DiscreteFunctionSpaceInterface
     *
     * - Fem::DiscreteFunctionInterface (including any wrapped or
     *   adapted non-discrete function)
     *
     * \see CompatibleModel.
     */
    template<class Object>
    inline auto massModel(const Object& object, const std::string& name = "")
    {
      typedef MassModel<typename Object::FunctionSpaceType> ModelType;
      return expressionClosure(ModelType(name));
    }

    //@} ModelGenerators

    //@} BasicModels

    //@} PDE-Models

  } // namespace ACFem::PDEModel

  namespace ACFem {

    using PDEModel::massModel;

  }

}  //Namespace Dune


#endif // __DUNE_ACFEM_MODELS_MODULES_MASSMODEL_HH__
