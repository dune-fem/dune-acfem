#ifndef __DUNE_ACFEM_MODELS_MODULES_GRADIENTMODEL_HH__
#define __DUNE_ACFEM_MODELS_MODULES_GRADIENTMODEL_HH__

#include <dune/fem/gridpart/common/gridpart.hh>

#include "../../common/gridfunctionspace.hh"
#include "../../expressions/terminal.hh"

#include "../modelbase.hh"

namespace Dune {

  namespace ACFem::PDEModel {

    /**@addtogroup PDE-Models
     * @{
     */

    /**@addtogroup BasicModels
     * @{
     */

    /**@addtogroup BulkModel
     * @{
     */

    /**A gradient model, for example to implement gradient
     * constraints. In contrast to the GradientLoadModel which adds
     * the gradient of a given function as load-vector, this model
     * implements the right hand side of the following equation:
     *
     * @f[
     * \int_\Omega \nabla\phi \cdot \psi
     * =
     * -\int_\Omega \phi \nabla\cdot\psi
     * +
     * \int_{\partial\Omega} \phi\psi\cdot\nu
     * @f]
     *
     * where @f$\psi@f$ is a @c dimDomain valued test-function and
     * @f$\phi@f$ is a scalar ansatz-function and @f$\nu@f$ the outer
     * normal.
     */
    template<class FunctionSpace>
    class GradientModel
      : public ModelBase<FunctionSpace,
                         typename Fem::ToNewDimRangeFunctionSpace<FunctionSpace, FunctionSpace::dimDomain>::Type>
      , public Expressions::SelfExpression<GradientModel<FunctionSpace> >
      , public TypedValueExpression
    {
      static_assert(FunctionSpace::dimRange == 1, "Domain function space must be scalar");
      using ThisType = GradientModel;
      using BaseType = ModelBase<FunctionSpace,
                                 typename Fem::ToNewDimRangeFunctionSpace<FunctionSpace, FunctionSpace::dimDomain>::Type>;
     public:
      using typename BaseType::DomainType;
      using typename BaseType::DomainRangeType;
      using typename BaseType::DomainJacobianRangeType;
      using typename BaseType::RangeRangeType;
      using typename BaseType::RangeJacobianRangeType;
      using BaseType::rangeDimRange;

      GradientModel(const std::string& name)
        : name_(name == "" ? "(grad p)" : name)
      {}

      std::string name() const
      {
        return name_;
      }

      /**@copydoc ModelFacade::linearizedSource() */
      auto linearizedFlux(const DomainRangeType& value) const
      {
        RangeJacobianRangeType result(0);
        for (int i = 0; i < rangeDimRange; ++i) {
          result[i][i] = -value;
        }
        return result;
      }

      auto fluxDivergence(const DomainJacobianRangeType& jacobian) const
      {
        return jacobian[0];
      }

      auto linearizedRobinFlux(const DomainType& unitOuterNormal,
                               const DomainRangeType& value) const
      {
        return RangeRangeType(unitOuterNormal) *= value;
      }

      //!@copydoc ModelFacade::classifyBoundary
      template<class Intersection>
      auto classifyBoundary(const Intersection& intersection) const
      {
        // true is correct as higher-level code has to take care of
        // the Dirichlet-(non-Dirichlet) splitting of the boundary.
        return std::make_pair(true, std::bitset<rangeDimRange>());
      }

     protected:
      const std::string name_;
    };

    //@} BulkModel


    /**@addtogroup ModelGenerators
     * @{
     */

    /**@brief Generate a gradient model from some object which has a
     * function-space. The generated GradientModel uses the dimDomain
     * as its dimension.
     *
     * @copydetails massModel()
     */
    template<class Object,
             std::enable_if_t<Object::FunctionSpaceType::ScalarFunctionSpaceType::dimRange == 1,
                              int> = 0>
    static inline auto
    gradientModel(const Object& object, const std::string& name = "")
    {
      return expressionClosure(GradientModel<typename Object::FunctionSpaceType::ScalarFunctionSpaceType>(name));
    }

    /**Generate a GradientModel from a GridPart, using ctype as
     * field and dimensionWorld as dimension.
     */
    template<class GridPartTraits>
    static inline auto
    gradientModel(const Fem::GridPartInterface<GridPartTraits>& gridPart,
                  const std::string& name = "")
    {
      typedef typename Fem::GridPartInterface<GridPartTraits>::GridPartType GridPartType;
      return gradientModel(ScalarGridFunctionSpace<GridPartType>{}, name);
    }

    //@} ModelGenerators

    //@} BasicModels

    //@} PDE-Models

  } // namespace ACFem::PDEModel

  namespace ACFem {

    using PDEModel::gradientModel;

  }

}  //Namespace Dune


#endif // __DUNE_ACFEM_MODELS_MODULES_GRADIENTMODEL_HH__
