#ifndef __DUNE_ACFEM_MODELS_MODULES_FLUIDSELFTRANSPORTMODEL_HH__
#define __DUNE_ACFEM_MODELS_MODULES_FLUIDSELFTRANSPORTMODEL_HH__

#include <dune/fem/function/localfunction/const.hh>

#include "../../expressions/expressionoperations.hh"
#include "../../expressions/terminal.hh"

#include "../modelbase.hh"

namespace Dune {

  namespace ACFem::PDEModel {

    /**@addtogroup PDE-Models
     * @{
     */

    /**@addtogroup BasicModels
     * @{
     */

    /**@addtogroup BulkModel
     * @{
     */

    /**\brief Define a model for the "Navier-Stokes" non-lineariry.
     *
     * An alternate formulation of the the Navier-Stokes non-linearity
     * obtained by integration by parts, at the cost of introducting a
     * boundary integral for non-Dirichlet boundaries
     *
     * In formulas: this fragment implements the right-hand-side of
     * the following equation (where the equality only holds for @f$\nabla\cdot U =
     * 0@f$):
     *
     * @f[
     * \int_\Omega U_i\,\partial_i U_j\,\phi_j
     * =
     * -
     * \int_\Omega U_i\,U_j\,\partial_i\phi_j
     * +
     * \int_{\partial\Omega} U\cdot\nu\,U\cdot\phi
     * @f]
     *
     * Of course, the formula uses sum-convention. The left-hand-side
     * is implemented by IncompressibleSelfTransportModel.
     *
     * \see IncompressibleSelfTransportModel, DeformationTensorModel, \ref TransportModelPage
     *
     * @param GridPart The grid-part we live on.
     */
    template<class FunctionSpace>
    class FluidSelfTransportModel
      : public ModelBase<FunctionSpace>
      , public Expressions::SelfExpression<FluidSelfTransportModel<FunctionSpace> >
      , public TypedValueExpression
    {
      using ThisType = FluidSelfTransportModel;
      using BaseType = ModelBase<FunctionSpace>;
     public:
      using typename BaseType::RangeFieldType;
      using typename BaseType::DomainType;
      using typename BaseType::RangeType;
      using typename BaseType::JacobianRangeType;
      using BaseType::dimRange;
      using BaseType::dimDomain;

      enum {
        dimWorld = dimDomain
      };

      static_assert(dimDomain == dimRange && dimDomain == dimWorld,
                    "This is meant for dimensionworld vector-fields, "
                    "like fluids, deformations etc.");

      // Interface methods that need to be reimplemented

      FluidSelfTransportModel(const std::string& name = "")
        : name_(name == "" ? "(-U_iU_jD_iPhi_j+Bndry)" : name)
      {}

      std::string name() const
      {
        return name_;
      }

      //!@copydoc ModelFacade::classifyBoundary
      template<class Intersection>
      auto classifyBoundary(const Intersection& intersection)
      {
        // true is correct as higher-level code has to take care of
        // the Dirichlet-(non-Dirichlet) splitting of the boundary.
        return std::make_pair(true, std::bitset<dimRange>());
      }

      //!@copydoc ModelFacade::flux()
      JacobianRangeType flux(const RangeType& value) const
      {
        // We need to provide the tensor-product of value with itself.
        JacobianRangeType flux(0);
        for (int i = 0; i < dimWorld; ++i) {
          flux[i][i] = value[i] * value[i];
          for (int j = i+1; j < dimWorld; ++j) {
            flux[i][j] = flux[j][i] = - (value[i] * value[j]);
          }
        }
        return flux;
      }

      //!@copydoc ModelFacade::linearizedFlux()
      template<class Point>
      JacobianRangeType linearizedFlux(const RangeType& uBar, const RangeType& value) const
      {
        // actually the sum of tensor products ...
        JacobianRangeType flux(0);
        for (int i = 0; i < dimWorld; ++i) {
          flux[i][i] = 2.0 * uBar[i] * value[i];
          for (int j = i+1; j < dimWorld; ++j) {
            flux[i][j] = flux[j][i] = - (uBar[i] * value[j] + uBar[j] * value[i]);
          }
        }
        return flux;
      }

      //!@copydoc ModelFacade::fluxDivergence()
      RangeType fluxDivergence(const RangeType& value, const JacobianRangeType& jacobian) const
      {
        // I think this is correct then ....
        RangeType result;
        jacobian.mv(value, result);
        return result;
      }

      RangeType robinFlux(const DomainType& unitOuterNormal,
                          const RangeType& value) const
      {
        RangeType result = value;
        return result *= (value * unitOuterNormal); // scalar product
      }

      RangeType linearizedRobinFlux(const RangeType& uBar,
                                    const DomainType& unitOuterNormal,
                                    const RangeType& value) const
      {
        RangeType result;
        RangeFieldType flowBar(0), flow(0);
        for (int i = 0; i < dimWorld; ++i) {
          flowBar += unitOuterNormal[i] * uBar[i];
          flow += unitOuterNormal[i] * value[i];
        }
        for (int i = 0; i < dimWorld; ++i) {
          result[i] = flow * uBar[i] + flowBar * value[i];
        }
        return result;
      }

     protected:
      std::string name_;
    };

    //@} BulkModel

    /**@addtogroup ModelGenerators
     * @{
     */

    /**\brief Generate a Navier-Stokes non-linearity fitting the given
     * object.
     *
     * \copydetails massModel()
     */
    template<class Object>
    static inline auto
    fluidSelfTransportModel(const Object& object, const std::string& name = "")
    {
      return expressionClosure(FluidSelfTransportModel<typename Object::FunctionSpaceType>(name));
    }

    //@} ModelGenerators

    //@} BasicModels

    //@} PDE-Models

  } // namespace ACFem::PDEModel

  namespace ACFem {

    using PDEModel::fluidSelfTransportModel;

  }

}  //Namespace Dune


#endif // __DUNE_ACFEM_MODELS_MODULES_FLUIDSELFTRANSPORTMODEL_HH__
