#ifndef __DUNE_ACFEM_MODELS_MODULES_LAPLACIANMODEL_HH__
#define __DUNE_ACFEM_MODELS_MODULES_LAPLACIANMODEL_HH__

#include "../../expressions/terminal.hh"

#include "../modelbase.hh"
#include "../expressiontraits.hh"

namespace Dune {

  namespace ACFem::PDEModel {

    /**@addtogroup PDE-Models
     * @{
     */

    /**@addtogroup BasicModels
     * @{
     */

    /**@addtogroup BulkModel
     * @{
     */

    /**Define a simple Laplacian-model, given function-space and
     * grid-part. The model carries no boundary data, it simply
     * defines the flux-term. Intentionally, this can be used (e.g.)
     * to form a time discretization together with another model which
     * implements the mass-part of the parabolic equation (e.g., if it
     * is parabolic). See Dune-Project acfem-heat.
     */
    template<class FunctionSpace>
    class LaplacianModel
      : public ModelBase<FunctionSpace>
      , public Expressions::SelfExpression<LaplacianModel<FunctionSpace> >
      , public MPL::UniqueTags<TypedValueExpression, SemiPositiveExpression, SymmetricModel>
    {
      typedef LaplacianModel ThisType;
      typedef ModelBase<FunctionSpace> BaseType;
     public:
      using typename BaseType::RangeType;
      using typename BaseType::JacobianRangeType;
      using typename BaseType::HessianRangeType;

      using BaseType::dimRange;
      using BaseType::dimDomain;

      // Interface methods that need to be reimplemented
      LaplacianModel(const std::string& name = "")
        : name_(name == "" ? "Laplace" : name)
      {}

      std::string name() const
      {
        return name_;
      }

      //!@copydoc ModelFacade::linearizedFlux()
      auto linearizedFlux(const JacobianRangeType& jacobian) const
      {
        return jacobian;
      }

      //!@copydoc ModelFacade::fluxDivergence()
      auto fluxDivergence(const HessianRangeType& hessian) const
      {
        RangeType result;
        for (int i = 0; i < dimRange; ++i) {
          result[i] = -hessian[i][0][0];
          for (int j = 1; j < dimDomain; ++j) {
            result[i] -= hessian[i][j][j];
          }
        }
        return result;
      }

     protected:
      std::string name_;
    };

    //@} BulkModel

    /**@addtogroup ModelGenerators
     * @{
     */

    /**\brief Generate Model for a (weak, of course) Laplacian.
     *
     * @param[in] object Something with a public FunctionSpaceType typedef.
     *
     * @param[in] name An optional name for debugging and pretty-printing.
     */
    template<class Object>
    static inline auto
    laplacianModel(const Object& object, const std::string& name = "")
    {
      typedef LaplacianModel<typename Object::FunctionSpaceType> ModelType;
      return expressionClosure(ModelType(name));
    }

    //@} ModelGenerators

    //@} BasicModels

    //@} PDE-Models

  } // namespace ACFem::PDEModel::

  namespace ACFem {
    using PDEModel::laplacianModel;
  }

}  //Namespace Dune


#endif // __DUNE_ACFEM_MODELS_MODULES_LAPLACIANMODEL_HH__
