#ifndef __DUNE_ACFEM_MODELS_MODULES_BULKLOADMODEL_HH__
#define __DUNE_ACFEM_MODELS_MODULES_BULKLOADMODEL_HH__

#include "../../functions/localfunctiontraits.hh"
#include "../../functions/functiontraits.hh"
#include "../modelbase.hh"

namespace Dune {

  namespace ACFem::PDEModel {

    /**@addtogroup PDE-Models
     * @{
     */

    /**@addtogroup BasicModels
     * @{
     */

    /**@addtogroup RightHandSideModels
     * @{
     */

    /**Wrap an existing GridFunction into a model which only conatains
     * this ModelConstituent. The most convenient way to add a
     * grid-function to the "right-hande-side" of a model is the use
     * of algebraic expressions:
     *
     * @code

MyGridFunctionObject F;
auto newModel = existingModel - F;

       @endcode
     *
     * The underlying expression-templates will form a suitable
     * BulkLoadFunctionModel by means of the overloaded operator-()
     * function and add that to the existing model.
     */
    template<class GridFunction>
    class BulkLoadFunctionModel
      : public ModelBase<typename std::decay_t<GridFunction>::FunctionSpaceType>
      , public Expressions::SelfExpression<BulkLoadFunctionModel<GridFunction> >
      , public MPL::UniqueTags<ConditionalType<IsConstantExprArg<GridFunction>::value, ConstantExpression, void>,
                               ConditionalType<IsTypedValue<GridFunction>::value, TypedValueExpression, void> >
    {
      static_assert(HasTag<GridFunction, Fem::HasLocalFunction>::value,
                    "GridFunction must provide a local function");

      using ThisType = BulkLoadFunctionModel;
      using BaseType = ModelBase<typename std::decay_t<GridFunction>::FunctionSpaceType>;
      using LocalFunctionType = Fem::ConstLocalFunction<std::decay_t<GridFunction> >;
     public:
      using typename BaseType::RangeType;

      template<class FunctionArg, std::enable_if_t<std::is_constructible<LocalFunctionType, FunctionArg>::value, int> = 0>
      BulkLoadFunctionModel(FunctionArg&& function, const std::string& name = "")
        : localFunction_(std::forward<FunctionArg>(function))
        , name_(name == "" ? "(" + localFunction_.gridFunction().name() + ", . )" : name)
      {}

      std::string name() const
      {
        return name_;
      }

      /**@copydoc ModelInterface::bind() */
      template<class Entity>
      void bind(const Entity& entity)
      {
        localFunction_.bind(entity);
      }

      /**@copydoc ModelInterface::unbind() */
      void unbind()
      {
        localFunction_.unbind();
      }

      /**@copydoc ModelInterface::source() */
      template<class Quadrature>
      RangeType source(const QuadraturePoint<Quadrature> &x) const
      {
        return localFunction_.evaluate(x);
      }

     protected:
      LocalFunctionType localFunction_;
      std::string name_;
    };

    //@} RightHandSideModels

    /**@addtogroup ModelGenerators
     * @{
     */

    /**\brief Generate a BulkLoadFunctionModel for the "right hand
     *side". If Fct is a BindableTensorFunction then we first
     *construct a non-differentiable variant and then feed it into the
     *BulkLoadModel.
     *
     * @param[in] f The L2-function for the RHS.
     *
     * @param[in] name Optional. If left empty some sensible name is
     * built from f.name() for debugging purposes.
     */
    template<class Fct,
             std::enable_if_t<(IsWrappableByConstLocalFunction<Fct>::value
                               && !GridFunction::HasRegularity<Fct, 1>::value
      ), int> = 0>
    constexpr auto bulkLoadFunctionModel(Fct&& f, const std::string& name = "")
    {
      return expressionClosure(BulkLoadFunctionModel<Fct>(std::forward<Fct>(f), name));
    }

    /**If the supplied grid-function is a BindableTensorFunction then
     * first force the differentiability order to 0. This spares
     * stack-space and complexity.
     */
    template<class Fct, std::enable_if_t<GridFunction::HasRegularity<Fct, 1UL>::value, int> = 0>
    constexpr auto bulkLoadFunctionModel(Fct&& f, const std::string& name = "")
    {
      using namespace Literals;
      return bulkLoadFunctionModel(gridFunction(f.gridPart(), std::forward<Fct>(f), 0_c), name);
    }

    template<class Fct, std::enable_if_t<ExpressionTraits<Fct>::isZero, int> = 0>
    constexpr auto bulkLoadFunctionModel(Fct&& f, const std::string& name = "")
    {
      return zeroModel(f);
    }

    //@} ModelGenerators

    //@} BasicModels

    //@} PDE-Models

  } // namespace ACFem::PDEModel

  namespace ACFem {

    using PDEModel::bulkLoadFunctionModel;

  }

}  //Namespace Dune


#endif // __DUNE_ACFEM_MODELS_MODULES_BULKLOADMODEL_HH__
