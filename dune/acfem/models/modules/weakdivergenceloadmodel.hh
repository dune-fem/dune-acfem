#ifndef __DUNE_ACFEM_MODELS_MODULES_WEAKDIVERGENCELOADMODEL_HH__
#define __DUNE_ACFEM_MODELS_MODULES_WEAKDIVERGENCELOADMODEL_HH__

#include <dune/fem/function/localfunction/const.hh>

#include "../../expressions/terminal.hh"
#include "../../functions/functions.hh"
#include "../modelbase.hh"

namespace Dune {

  namespace ACFem::PDEModel {

    /**@addtogroup PDE-Models
     * @{
     */

    /**@addtogroup BasicModels
     * @{
     */

    /**@addtogroup BulkModel
     * @{
     */

    /**For a given grid-function define a model implementing the weak
     * divergence.
     *
     * In formulas: this fragment implements the bulk-part of the
     * right-hand-side of the following equation, where @f$\phi@f$ is
     * a scalar test-function:
     *
     * @f[
     * \int_\Omega \partial_i p_i \, \cdot \phi
     * =
     * -\int_\Omega p_i \, \partial_i\phi
     * +
     * \int_{\partial\Omega} p_i \, \phi \nu_i
     * @f]
     *
     * The boundary integral is not computed, intentionally it is
     * substituted by suitable boundary conditions.
     *
     * @f$p@f$ acts as data, so this model just acts as a "constant",
     * i.e. as a contribution to the right hand side.
     *
     * @param GridFunction The type of the function the weak divergence
     * has to be computed from. This must be a grid-function, i.e. it
     * has to carry a local function and must have dimRange == dimWorld.
     */
    template<class GridFunction>
    class WeakDivergenceLoadModel
      : public ModelBase<typename std::decay_t<GridFunction>::FunctionSpaceType::ScalarFunctionSpaceType>
      , public Expressions::SelfExpression<WeakDivergenceLoadModel<GridFunction> >
      , public MPL::UniqueTags<ConditionalType<ExpressionTraits<GridFunction>::isVolatile, VolatileExpression, void>,
                               ConditionalType<IsConstantExprArg<GridFunction>::value, ConstantExpression, void>,
                               ConditionalType<IsTypedValue<GridFunction>::value, TypedValueExpression, void> >
    {
      static_assert(HasTag<GridFunction, Fem::HasLocalFunction>::value,
                    "GridFunction must provide a local function");

      using ThisType = WeakDivergenceLoadModel;
      using BaseType = ModelBase<typename std::decay_t<GridFunction>::FunctionSpaceType::ScalarFunctionSpaceType>;
      using GridFunctionDecay = std::decay_t<GridFunction>;
      using LocalFunctionType = Fem::ConstLocalFunction<GridFunctionDecay>;
     public:
      using GridFunctionType = GridFunction;

      using typename BaseType::RangeType;
      using typename BaseType::DomainType;
      using typename BaseType::JacobianRangeType;
      using BaseType::dimDomain;
      using BaseType::dimRange;

      enum {
        dimWorld = GridFunctionDecay::GridPartType::dimensionworld
      };

      static_assert(dimRange == 1 && dimDomain == dimWorld,
                    "This is only meant for dimRange == 1 and dimDomain == dimWorld.");

      template<class FunctionArg, std::enable_if_t<std::is_constructible<LocalFunctionType, FunctionArg>::value, int> = 0>
      WeakDivergenceLoadModel(FunctionArg&& function, const std::string& name = "")
        : localFunction_(std::forward<FunctionArg>(function))
        , name_(name == "" ? "(div " + localFunction_.gridFunction().name() + ")" : name)
      {}

      std::string name() const
      {
        return name_;
      }

      // Interface methods that need to be reimplemented

      /**@copydoc ModelFacade::bind() */
      template<class Entity>
      void bind(const Entity& entity)
      {
        localFunction_.bind(entity);
      }

      /**@copydoc ModelFacade::unbind() */
      void unbind()
      {
        localFunction_.unbind();
      }

      //!@copydoc ModelFacade::flux()
      template<class Quadrature>
      JacobianRangeType flux(const QuadraturePoint<Quadrature> &x) const
      {
        const DomainType dataValue = localFunction_.evaluate(x);

        JacobianRangeType flux = 0;
        // value * IdentityMatrix
        for (int i = 0; i < dimRange; ++i) {
          flux[i] -= dataValue;
        }
        return flux;
      }

      //!@copydoc ModelFacade::fluxDivergence()
      //!
      //! This is the strong form, i.e. simply the divergence.
      template<class Quadrature>
      auto fluxDivergence(const QuadraturePoint<Quadrature> &x) const
      {
        const auto divergenceData = localFunction_.jacobian(x);

        RangeType result = 0;
        for (int i = 0; i < dimWorld; ++i) {
          result[0] += divergenceData[i][i];
        }
        return result;
      }

     protected:
      LocalFunctionType localFunction_;
      std::string name_;
    };

    //@} BulkModel


    /**@addtogroup ModelGenerators
     * @{
     */

    template<class GridFunction>
    constexpr auto weakDivergenceModel(GridFunction&& f, const std::string& name = "")
    {
      static_assert(IsWrappableByConstLocalFunction<GridFunction>::value,
                    "GridFunction is not suitable for Fem::ConstLocalFunction");

      return expressionClosure(WeakDivergenceLoadModel<GridFunction>(std::forward<GridFunction>(f), name));
    }

    template<class GridFunction, std::enable_if_t<ExpressionTraits<GridFunction>::isZero, int> = 0>
    constexpr auto weakDivergenceModel(GridFunction&& f, const std::string& name = "")
    {
      using FunctionSpaceType = typename std::decay_t<GridFunction>::FunctionSpaceType::ScalarFunctionSpaceType;
      return zeroModel(FunctionSpaceType{});
    }

    //@} ModelGenerators

    //@} BasicModels

    //@} PDE-Models

  } // namespace ACFem

}  //Namespace Dune


#endif // __DUNE_ACFEM_MODELS_MODULES_WEAKDIVERGENCELOADMODEL_HH__
