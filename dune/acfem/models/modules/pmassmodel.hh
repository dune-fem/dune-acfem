#ifndef __DUNE_ACFEM_MODELS_MODULES_PMASSMODEL_HH__
#define __DUNE_ACFEM_MODELS_MODULES_PMASSMODEL_HH__

#include "../../common/literals.hh"
#include "../../expressions/terminal.hh"

#include "../modelbase.hh"
#include "../expressiontraits.hh"

namespace Dune {

  namespace ACFem::PDEModel {

    using namespace Literals;

    /**@addtogroup PDE-Models
     * @{
     */

    /**@addtogroup BasicModels
     * @{
     */

    /**@addtogroup BulkModel
     * @{
     */

    /**\brief A simplistic non-linear example.
     *
     * This models form a zero-order model of the form
     *
     * @f[
     * \int_\Omega |U|^{p-2}\,U\cdot\phi\quad\forall \phi
     * @f]
     *
     * where @f$U@f$ is the unknown und @f$ß\phi@f$ denotes the test
     * functions. This is the formal first variation of the functional
     *
     * @f[
     * U\mapsto \int_\Omega |U|^p
     * @f]
     */
    template<class FunctionSpace, class PField>
    class P_MassModel
      : public ModelBase<FunctionSpace>
      , public Expressions::SelfExpression<P_MassModel<FunctionSpace, PField> >
      , public MPL::UniqueTags<ConditionalType<ExpressionTraits<PField>::isVolatile, VolatileExpression, void>,
                               ConditionalType<IsConstantExprArg<PField>::value, ConstantExpression, void>,
                               ConditionalType<IsTypedValue<PField>::value, TypedValueExpression, void>,
                               PositiveExpression,
                               SymmetricModel>
    {
      using ThisType = P_MassModel;
      using BaseType = ModelBase<FunctionSpace>;
     public:
      using typename BaseType::RangeFieldType;
      using typename BaseType::RangeType;
      using PFieldType = PField;
      using ExponentType = std::decay_t<decltype(std::declval<PFieldType>() - 2_f)>;

      template<class P, std::enable_if_t<std::is_constructible<PFieldType, P>::value, int> = 0>
      P_MassModel(P&& exponent, const std::string& name = "")
        : exponent_(PFieldType(exponent) - 2_f),
          name_(name == "" ? "(|U|^" + toString(exponent_) + " U)" : name)
      {}

      std::string name() const
      {
        return name_;
      }

      /**@copydoc ModelFacade::source() */
      RangeType source(const RangeType& value) const
      {
        auto factor = std::pow(value.two_norm2(), 0.5*exponent_);

        auto result = value;
        return result *= factor;
      }

      /**@copydoc ModelFacade::linearizedSource() */
      RangeType linearizedSource(const RangeType& uBar,
                                 const RangeType& value) const
      {
        auto norm = uBar.two_norm2();

        auto result = value;

        if (exponent_ == 0.0) {
          return result;
        }

        result *= std::pow(norm, 0.5 * exponent_);

        auto tmp = uBar;

        tmp *= exponent_*std::pow(norm, 0.5*exponent_ - 1.) * (uBar * value);

        return result += tmp;
      }

      const ExponentType& exponent() const { return exponent_; }

      constexpr auto p() const { return exponent_ + 2_f; }

     protected:
      ExponentType exponent_;
      std::string name_;
    };

    //@} BulkModel

    /**@addtogroup ModelGenerators
     * @{
     */

    /**\brief Generate Model for a (weak, of course) Mass.
     *
     * \copydetails massModel()
     *
     * \param[in] p The exponent, see P_MassModel.
     *
     * @param[in] object Something with a public FunctionSpaceType typedef.
     *
     * @param[in] name An optional name for debugging and pretty-printing.
     */
    template<class Object, class PField>
    constexpr auto p_MassModel(PField&& p, const Object& object, const std::string& name = "")
    {
      return expressionClosure(P_MassModel<typename Object::FunctionSpaceType, PField>(std::forward<PField>(p), name));
    }

    //@} ModelGenerators

    //@} BasicModels

    //@} PDE-Models

  } // namespace ACFem::PDEModel

  namespace ACFem {

    using PDEModel::p_MassModel;

  }

}  //Namespace Dune


#endif // __DUNE_ACFEM_MODELS_MODULES_PMASSMODEL_HH__
