#ifndef __DUNE_ACFEM_MODELS_MODULES_DIVERGENCEMODEL_HH__
#define __DUNE_ACFEM_MODELS_MODULES_DIVERGENCEMODEL_HH__

#include <dune/fem/gridpart/common/gridpart.hh>

#include "../../common/gridfunctionspace.hh"
#include "../../expressions/terminal.hh"
#include "../modelbase.hh"
#include "../expressiontraits.hh"

namespace Dune {

  namespace ACFem::PDEModel {

    /**@addtogroup PDE-Models
     * @{
     */

    /**@addtogroup BasicModels
     * @{
     */

    /**@addtogroup BulkModel
     * @{
     */

    /**A divergence model, for example to implement divergence
     * constraints. In contrast to the DivergenceLoadModel which add
     * the divergence of a given function as load-vector, this model
     * implements the contribution to the bilinear form:
     *
     * @f[
     * \int_\Omega \nabla\cdot\psi \,\phi
     * @f]
     *
     * where @f$\psi@f$ is a @c dimDomain valued ansatz-function and
     * @f$\phi@f$ is a scalar test function.
     *
     * @param[in] Field The domain and range field of the function space.
     *
     * @param[in] dim The domain and range dimension of the vector field.
     */
    template<class FunctionSpace>
    class DivergenceModel
      : public ModelBase<FunctionSpace, typename FunctionSpace::ScalarFunctionSpaceType>
      , public Expressions::SelfExpression<DivergenceModel<FunctionSpace> >
      , public TypedValueExpression
    {
      static_assert(FunctionSpace::dimRange == FunctionSpace::dimDomain,
                    "DomainFunctionSpace must be a vector field with dimRange == dimDomain");
      using ThisType = DivergenceModel;
      using BaseType = ModelBase<FunctionSpace, typename FunctionSpace::ScalarFunctionSpaceType>;
     public:
      using typename BaseType::DomainJacobianRangeType;
      using typename BaseType::RangeRangeType;
      using BaseType::domainDimRange;

      DivergenceModel(const std::string& name)
        : name_(name == "" ? "(div U)" : name)
      {}

      std::string name() const
      {
        return name_;
      }

      /**@copydoc ModelFacade::linearizedSource() */
      RangeRangeType linearizedSource(const DomainJacobianRangeType& jacobian) const
      {
        RangeRangeType result(jacobian[0][0]);
        for (int i = 0; i < domainDimRange; ++i) {
          result += jacobian[i][i];
        }
        return result;
      }

    protected:
      const std::string name_;
    };

    //@} BulkModel


    /**@addtogroup ModelGenerators
     * @{
     */

    /**@brief Generate a divergence model from some object which has a
     * function-space. The generated DivergenceModel uses the dimDomain
     * as its dimension.
     *
     * @copydetails massModel()
     */
    template<class Object,
             std::enable_if_t<Object::FunctionSpaceType::ScalarFunctionSpaceType::dimRange == 1, int> = 0>
    static inline auto
    divergenceModel(const Object& object, const std::string& name = "")
    {
      using FunctionSpace = typename Object::FunctionSpaceType;
      return expressionClosure(DivergenceModel<typename Fem::ToNewDimRangeFunctionSpace<FunctionSpace, FunctionSpace::dimDomain>::Type>(name));
    }

    /**Generate a DivergenceModel from a GridPart, using ctype as
     * field and dimensionWorld as dimension.
     */
    template<class GridPartTraits>
    static inline auto
    divergenceModel(const Fem::GridPartInterface<GridPartTraits>& gridPart,
                    const std::string& name = "")
    {
      using GridPartType = typename Fem::GridPartInterface<GridPartTraits>::GridPartType;
      return divergenceModel(GridFunctionSpace<GridPartType>{}, name);
    }

    //@} ModelGenerators

    //@} BasicModels

    //@} PDE-Models

  } // namespace ACFem::PDEModel

  namespace ACFem {

    using PDEModel::divergenceModel;

  }

}  //Namespace Dune


#endif // __DUNE_ACFEM_MODELS_MODULES_DIVERGENCEMODEL_HH__
