#ifndef __DUNE_ACFEM_MODELS_MODULES_NITSCHEDIRICHLETMODEL_HH__
#define __DUNE_ACFEM_MODELS_MODULES_NITSCHEDIRICHLETMODEL_HH__

#include <dune/fem/function/localfunction/const.hh>

#include "../../algorithms/modelparameters.hh"
#include "../indicators/boundaryindicator.hh"
#include "../modeltraits.hh"
#include "../expressions.hh"

namespace Dune {

  namespace ACFem::PDEModel {

    using namespace Literals;

    /**@addtogroup PDE-Models
     * @{
     */

    /**@addtogroup BasicModels
     * @{
     */

    /**@addtogroup BoundaryValueModels
     * @{
     */

    namespace {

      template<class Model, class Penalty, bool linear, class LinArgs, class Args>
      class RobinFluxProviderBase;

      template<class Model, class Penalty, bool linear, std::size_t... LinArgsIdx, std::size_t... ArgsIdx>
      class RobinFluxProviderBase<Model, Penalty, linear,
                                  IndexSequence<LinArgsIdx...>,
                                  IndexSequence<ModelIntrospection::pointIndex, ModelIntrospection::normalIndex, ArgsIdx...> >
      {
        static constexpr std::size_t pointIndex = ModelIntrospection::pointIndex;
        static constexpr std::size_t normalIndex = ModelIntrospection::normalIndex;
        static constexpr std::size_t fluxTag = linear ? ModelIntrospection::linearizedFlux : ModelIntrospection::flux;
        static constexpr std::size_t robinFluxTag = linear ? ModelIntrospection::linearizedRobinFlux : ModelIntrospection::robinFlux;
        static constexpr std::size_t dirichletTag = linear ? ModelIntrospection::linearizedDirichlet : ModelIntrospection::dirichlet;

        RobinFluxProviderBase(const RobinFluxProviderBase&);
       public:
        RobinFluxProviderBase(const Model& m, const Penalty& p)
          : flux_(m), robinFlux_(m), dirichlet_(m), p_(p)
        {}

        template<class Quadrature>
        auto robinMethod(const TupleElement<LinArgsIdx, PDEModel::AllArgs<Model> >&... linArgs,
                         const QuadraturePoint<Quadrature>& x,
                         const typename Model::DomainType& unitOuterNormal,
                         const TupleElement<ArgsIdx, PDEModel::AllArgs<Model> >&... args) const
        {
          auto result = robinFlux_(linArgs..., x, unitOuterNormal, args...);
          auto dirichlet = dirichlet_(linArgs..., x, args...);
          const auto flux = flux_(linArgs..., x, args...);
          const typename Model::RangeFieldType penalty = p_.evaluate(x);

#if 0
          flux.usmv(-1.0, unitOuterNormal, result);
          result += (dirichlet *= penalty);
#else
          result -= contractInner(flux, unitOuterNormal);
          result += penalty * dirichlet;
#endif

          return result;
        }

        const PDEModel::TaggedModelMethod<Model, fluxTag, SequenceMask<LinArgsIdx..., pointIndex, ArgsIdx...>::value> flux_;
        const PDEModel::TaggedModelMethod<Model, robinFluxTag, SequenceMask<LinArgsIdx..., pointIndex, normalIndex, ArgsIdx...>::value> robinFlux_;
        const PDEModel::TaggedModelMethod<Model, dirichletTag, SequenceMask<LinArgsIdx..., pointIndex, ArgsIdx...>::value> dirichlet_;
        const Penalty& p_;
      };

      template<class Model, class Penalty, bool linear, bool enable, class LinArgs, class Args>
      class RobinFluxProvider;

      template<class Model, class Penalty, bool linear, class LinArgs, class Args>
      class RobinFluxProvider<Model, Penalty, linear, false, LinArgs, Args>
      {
       public:
        RobinFluxProvider(const Model& m, const Penalty& p)
        {}
      };

      template<class Model, class Penalty, std::size_t... ArgsIdx>
      class RobinFluxProvider<Model, Penalty, false, true,
                              IndexSequence<>,
                              IndexSequence<ModelIntrospection::pointIndex, ArgsIdx...> >
        : RobinFluxProviderBase<Model, Penalty, false,
                                IndexSequence<>,
                                IndexSequence<ModelIntrospection::pointIndex, ArgsIdx...> >
      {
        using BaseType = RobinFluxProviderBase<Model, Penalty, false,
                                               IndexSequence<>,
                                               IndexSequence<ModelIntrospection::pointIndex, ArgsIdx...> >;
        using BaseType::robinMethod;
       public:
        RobinFluxProvider(const Model& m, const Penalty& p)
          : BaseType(m, p)
        {}

        template<class Quadrature>
        auto
        robinFlux(const QuadraturePoint<Quadrature>& x,
                  const TupleElement<ArgsIdx, PDEModel::AllArgs<Model> >&... args) const
        {
          return robinMethod(x, args...);
        }
      };

      template<class Model, class Penalty, std::size_t... LinArgsIdx, std::size_t... ArgsIdx>
      class RobinFluxProvider<Model, Penalty, true, true,
                              IndexSequence<LinArgsIdx...>,
                              IndexSequence<ModelIntrospection::pointIndex, ArgsIdx...> >
        : RobinFluxProviderBase<Model, Penalty, true,
                                IndexSequence<LinArgsIdx...>,
                                IndexSequence<ModelIntrospection::pointIndex, ArgsIdx...> >
      {
        using BaseType = RobinFluxProviderBase<Model, Penalty, true,
                                               IndexSequence<LinArgsIdx...>,
                                               IndexSequence<ModelIntrospection::pointIndex, ArgsIdx...> >;
        using BaseType::robinMethod;
       public:
        RobinFluxProvider(const Model& m, const Penalty& p)
          : BaseType(m, p)
        {}

        template<class Quadrature>
        auto
        linearizedRobinFlux(const TupleElement<LinArgsIdx, PDEModel::AllArgs<Model> >&... linArgs,
                            const QuadraturePoint<Quadrature>& x,
                            const TupleElement<ArgsIdx, PDEModel::AllArgs<Model> >&... args) const
        {
          return robinMethod(linArgs..., x, args...);
        }
      };

      template<class Model, std::ptrdiff_t symmetry, bool linear, class LinArgs, class PointArgs, class Args>
      class SingularFluxProviderBase;

      template<class Model, std::ptrdiff_t symmetry, bool linear, std::size_t... LinArgsIdx, std::size_t... PointIdx, std::size_t... ArgsIdx>
      class SingularFluxProviderBase<Model, symmetry, linear,
                                     IndexSequence<LinArgsIdx...>,
                                     IndexSequence<PointIdx...>,
                                     IndexSequence<ModelIntrospection::normalIndex, ArgsIdx...> >
      {
        static constexpr std::size_t pointIndex = ModelIntrospection::pointIndex;
        static constexpr std::size_t normalIndex = ModelIntrospection::normalIndex;
        static constexpr std::size_t jacobianIndex = ModelIntrospection::jacobianIndex;
        static constexpr std::size_t fluxTag = linear ? ModelIntrospection::linearizedFlux : ModelIntrospection::flux;
        static constexpr std::size_t singularFluxTag = linear ? ModelIntrospection::linearizedSingularFlux : ModelIntrospection::singularFlux;
        static constexpr std::size_t dirichletTag = linear ? ModelIntrospection::linearizedDirichlet : ModelIntrospection::dirichlet;

        template<class Quadrature>
        using AllArgs = PDEModel::AllArgs<Model, Quadrature>;

       protected:
        SingularFluxProviderBase(const Model& m)
          : flux_(m), singularFlux_(m), dirichlet_(m)
        {}

        template<class Quadrature = PointWrapperQuadrature<typename Model::DomainType> >
        auto singularFluxMethod(const TupleElement<LinArgsIdx, AllArgs<Quadrature> >&... linArgs,
                                const TupleElement<PointIdx, AllArgs<Quadrature> >&... xArg,
                                const typename Model::DomainType& unitOuterNormal,
                                const TupleElement<ArgsIdx, AllArgs<Quadrature> >&... args) const
        {
          using DomainRangeType = typename Model::DomainRangeType;
          using DomainJacobianRangeType = typename Model::DomainJacobianRangeType;

          DomainJacobianRangeType result = singularFlux_(linArgs..., xArg..., unitOuterNormal, args...);
          const auto dirichlet = dirichlet_(linArgs..., xArg..., args...);

          // we know that the first argument of args... must be the values.
          DomainJacobianRangeType un;

          un = outer(dirichlet, unitOuterNormal);

          result -= flux_(linArgs..., xArg..., DomainRangeType(0), un) * symmetry;
          if (!ModelTraits<Model>::template IsLinear<fluxTag>::value) {
            result += flux_(linArgs..., xArg..., DomainRangeType(0), DomainJacobianRangeType(0)) * symmetry;
          }

          return result;
        }

        const PDEModel::TaggedModelMethod<Model, fluxTag, SequenceMask<LinArgsIdx..., PointIdx..., ArgsIdx...>::value> flux_;
        const PDEModel::TaggedModelMethod<Model, singularFluxTag, SequenceMask<LinArgsIdx..., PointIdx..., normalIndex, ArgsIdx...>::value> singularFlux_;
        const PDEModel::TaggedModelMethod<Model, dirichletTag, SequenceMask<LinArgsIdx..., PointIdx..., ArgsIdx...>::value> dirichlet_;
      };

      template<class Model, std::ptrdiff_t symmetry, bool linear, bool enable, class LinArgs, class Args>
      class SingularFluxProvider;

      template<class Model, std::ptrdiff_t symmetry, bool linear, class LinArgs, class Args>
      class SingularFluxProvider<Model, symmetry, linear, false, LinArgs, Args>
      {
       public:
        SingularFluxProvider(const Model& m)
        {}
      };

      template<class Model, std::ptrdiff_t symmetry, std::size_t... ArgsIdx>
      class SingularFluxProvider<Model, symmetry, false, true,
                                 IndexSequence<>,
                                 IndexSequence<ModelIntrospection::pointIndex, ArgsIdx...> >
        : SingularFluxProviderBase<Model, symmetry, false,
                                   IndexSequence<>,
                                   IndexSequence<ModelIntrospection::pointIndex>,
                                   IndexSequence<ArgsIdx...> >
      {
        using BaseType = SingularFluxProviderBase<Model, symmetry, false,
                                                  IndexSequence<>,
                                                  IndexSequence<ModelIntrospection::pointIndex>,
                                                  IndexSequence<ArgsIdx...> >;
        using BaseType::singularFluxMethod;
       public:
        SingularFluxProvider(const Model& m)
          : BaseType(m)
        {}

        template<class Quadrature>
        auto singularFlux(const QuadraturePoint<Quadrature>& x,
                          const TupleElement<ArgsIdx, PDEModel::AllArgs<Model> >&... args) const
        {
          return BaseType::template singularFluxMethod<Quadrature>(x, args...);
        }
      };

      template<class Model, std::ptrdiff_t symmetry, std::size_t... LinArgsIdx, std::size_t... ArgsIdx>
      class SingularFluxProvider<Model, symmetry, true, true,
                                 IndexSequence<LinArgsIdx...>,
                                 IndexSequence<ModelIntrospection::pointIndex, ArgsIdx...> >
      : SingularFluxProviderBase<Model, symmetry, true,
                                   IndexSequence<LinArgsIdx...>,
                                   IndexSequence<ModelIntrospection::pointIndex>,
                                   IndexSequence<ArgsIdx...> >
      {
        using BaseType = SingularFluxProviderBase<Model, symmetry, true,
                                                  IndexSequence<LinArgsIdx...>,
                                                  IndexSequence<ModelIntrospection::pointIndex>,
                                                  IndexSequence<ArgsIdx...> >;
        using BaseType::singularFluxMethod;
       public:

        SingularFluxProvider(const Model& m)
          : BaseType(m)
        {}

        template<class Quadrature>
        auto linearizedSingularFlux(const TupleElement<LinArgsIdx, PDEModel::AllArgs<Model> >&... linArgs,
                                    const QuadraturePoint<Quadrature>& x,
                                    const TupleElement<ArgsIdx, PDEModel::AllArgs<Model> >&... args) const
        {
          return BaseType::template singularFluxMethod<Quadrature>(linArgs..., x, args...);
        }
      };

      template<class Model, std::ptrdiff_t symmetry, std::size_t... ArgsIdx>
      class SingularFluxProvider<Model, symmetry, false, true,
                                 IndexSequence<>,
                                 IndexSequence<ArgsIdx...> >
        : SingularFluxProviderBase<Model, symmetry, false,
                                   IndexSequence<>,
                                   IndexSequence<>,
                                   IndexSequence<ArgsIdx...> >
      {
        using BaseType = SingularFluxProviderBase<Model, symmetry, false,
                                                  IndexSequence<>,
                                                  IndexSequence<>,
                                                  IndexSequence<ArgsIdx...> >;
        using BaseType::singularFluxMethod;
       public:
        SingularFluxProvider(const Model& m)
          : BaseType(m)
        {}

        auto singularFlux(const TupleElement<ArgsIdx, PDEModel::AllArgs<Model> >&... args) const
        {
          return singularFluxMethod(args...);
        }
      };

      template<class Model, std::ptrdiff_t symmetry, std::size_t... LinArgsIdx, std::size_t... ArgsIdx>
      class SingularFluxProvider<Model, symmetry, true, true,
                                 IndexSequence<LinArgsIdx...>,
                                 IndexSequence<ArgsIdx...> >
      : SingularFluxProviderBase<Model, symmetry, true,
                                 IndexSequence<LinArgsIdx...>,
                                 IndexSequence<>,
                                 IndexSequence<ArgsIdx...> >
      {
        using BaseType = SingularFluxProviderBase<Model, symmetry, true,
                                                  IndexSequence<LinArgsIdx...>,
                                                  IndexSequence<>,
                                                  IndexSequence<ArgsIdx...> >;
        using BaseType::singularFluxMethod;
       public:

        SingularFluxProvider(const Model& m)
          : BaseType(m)
        {}

        auto linearizedSingularFlux(const TupleElement<LinArgsIdx, PDEModel::AllArgs<Model> >&... linArgs,
                                    const TupleElement<ArgsIdx, PDEModel::AllArgs<Model> >&... args) const
        {
          return singularFluxMethod(linArgs..., args...);
        }
      };

      template<class Model>
      using NeedRobinFlux = BoolConstant<ModelTraits<Model>::template HasMethod<ModelIntrospection::flux>::value
                                         ||
                                         ModelTraits<Model>::template HasMethod<ModelIntrospection::robinFlux>::value
                                         ||
                                         ModelTraits<Model>::template HasMethod<ModelIntrospection::dirichlet>::value>;
      template<class Model>
      using NeedLinearizedRobinFlux = BoolConstant<ModelTraits<Model>::template HasMethod<ModelIntrospection::linearizedFlux>::value
                                                   ||
                                                   ModelTraits<Model>::template HasMethod<ModelIntrospection::linearizedRobinFlux>::value
                                                   ||
                                                   ModelTraits<Model>::template HasMethod<ModelIntrospection::linearizedDirichlet>::value>;
      template<class Model>
      using RobinFluxSignature = ModelIntrospection::ArgumentMask<SequenceMask<ModelIntrospection::pointIndex, ModelIntrospection::normalIndex>::value
                                                                  |
                                                                  ModelMethodSignatureClosure<Model, ModelIntrospection::flux>::value
                                                                  |
                                                                  ModelMethodSignatureClosure<Model, ModelIntrospection::robinFlux>::value
                                                                  |
                                                                  ModelMethodSignatureClosure<Model, ModelIntrospection::dirichlet>::value>;
      template<class Model>
      using LinearizedRobinFluxSignature = ModelIntrospection::ArgumentMask<SequenceMask<ModelIntrospection::pointIndex, ModelIntrospection::normalIndex>::value
                                                                            |
                                                                            ModelMethodSignatureClosure<Model, ModelIntrospection::linearizedFlux>::value
                                                                            |
                                                                            ModelMethodSignatureClosure<Model, ModelIntrospection::linearizedRobinFlux>::value
                                                                            |
                                                                            ModelMethodSignatureClosure<Model, ModelIntrospection::linearizedDirichlet>::value>;

      template<class Model, class PenaltyFunction>
      using RobinFluxMethod = RobinFluxProvider<Model, Fem::ConstLocalFunction<PenaltyFunction>,
                                                false, NeedRobinFlux<Model>::value,
                                                IndexSequence<>, MaskSequence<RobinFluxSignature<Model>::signature()> >;
      template<class Model, class PenaltyFunction>
      using LinearizedRobinFluxMethod = RobinFluxProvider<Model, Fem::ConstLocalFunction<PenaltyFunction>,
                                                          true, NeedLinearizedRobinFlux<Model>::value,
                                                          MaskSequence<LinearizedRobinFluxSignature<Model>::linearizationSignature()>,
                                                          MaskSequence<LinearizedRobinFluxSignature<Model>::signature()> >;

      template<class Model>
      using NeedSingularFlux = BoolConstant<ModelTraits<Model>::template HasMethod<ModelIntrospection::flux>::value
                                            ||
                                            ModelTraits<Model>::template HasMethod<ModelIntrospection::singularFlux>::value
                                            ||
                                            ModelTraits<Model>::template HasMethod<ModelIntrospection::dirichlet>::value>;
      template<class Model>
      using NeedLinearizedSingularFlux = BoolConstant<ModelTraits<Model>::template HasMethod<ModelIntrospection::linearizedFlux>::value
                                                      ||
                                                      ModelTraits<Model>::template HasMethod<ModelIntrospection::linearizedSingularFlux>::value
                                                      ||
                                                      ModelTraits<Model>::template HasMethod<ModelIntrospection::linearizedDirichlet>::value>;
      template<class Model>
      using SingularFluxSignature = ModelIntrospection::ArgumentMask<SequenceMask<ModelIntrospection::normalIndex>::value
                                                                  |
                                                                  ModelMethodSignatureClosure<Model, ModelIntrospection::flux>::value
                                                                  |
                                                                  ModelMethodSignatureClosure<Model, ModelIntrospection::singularFlux>::value
                                                                  |
                                                                  ModelMethodSignatureClosure<Model, ModelIntrospection::dirichlet>::value>;
      template<class Model>
      using LinearizedSingularFluxSignature = ModelIntrospection::ArgumentMask<SequenceMask<ModelIntrospection::normalIndex>::value
                                                                               |
                                                                               ModelMethodSignatureClosure<Model, ModelIntrospection::linearizedFlux>::value
                                                                               |
                                                                               ModelMethodSignatureClosure<Model, ModelIntrospection::linearizedSingularFlux>::value
                                                                               |
                                                                               ModelMethodSignatureClosure<Model, ModelIntrospection::linearizedDirichlet>::value>;

      template<class Model, std::ptrdiff_t symmetry>
      using SingularFluxMethod = SingularFluxProvider<Model, symmetry,
                                                      false, NeedSingularFlux<Model>::value && symmetry != 0,
                                                      IndexSequence<>, MaskSequence<SingularFluxSignature<Model>::signature()> >;
      template<class Model, std::ptrdiff_t symmetry>
      using LinearizedSingularFluxMethod = SingularFluxProvider<Model, symmetry,
                                                                true, NeedLinearizedSingularFlux<Model>::value && symmetry != 0,
                                                                MaskSequence<LinearizedSingularFluxSignature<Model>::linearizationSignature()>,
                                                                MaskSequence<LinearizedSingularFluxSignature<Model>::signature()> >;
    }

    /**This model constructs from a given other model weak Dirichlet
     * conditions as first introduced by @cite Nitsche:71.
     *
     * Following the somewhat easier-to-read presentation in @cite ArBrCoMa:01
     * this model implement the following boundary
     * contributions
     *
     * @f[
     * -\int_{\Gamma_D}\sigma(u,\nabla u)\cdot\nu\,\phi
     * -\delta\,\int_{\Gamma_D}\big(\sigma(0,\,(u-g)\,\nu)-\sigma(0,\,0)\big)\cdot\nabla\phi
     * +\int_{\Gamma_D}\mu\,(u-g)\,\phi
     * @f]
     *
     * Here @f$\sigma@f$ denotes the flux() method from the model,
     * @f$\delta > 0@f$ is some constant and @f$\mu@f$ a suitable
     * penalty parameter. @f$u@f$ and @f$\phi@f$ denote ansatz- and
     * test-functions, respectively. @f$g@f$ denotes the Dirichlet
     * values.
     *
     * For linear symmetric problems @f$\delta@f$ can be chosen as @c
     * -1 in order to keep the resulting model symmetric. If the model
     * is linear asymmetric or even non-linear it is not so clear how to
     * choose @f$\delta@f$ in a suitable way.
     *
     * The first boundary integral guarantees consistency of the
     * method which leds to optimal convergence results if the penalty
     * parameter @f$\mu@f$ is chosen as @f$\approx 1/h@f$ where h
     * denotes the local mesh-size.
     *
     * The respective parameters are passed as arguments to the
     * constructor and/or generator function(s), @see
     * nitscheDirichletBoundaryModel().
     */
    template<class Model, class PenaltyFunction, class Symmetrize = SkeletonSymmetrizeDefault<Model> >
    class NitscheDirichletBoundaryModel
      : public ModelCrop<Model,
                         PDEModel::robinFlux,
                         PDEModel::linearizedRobinFlux,
                         PDEModel::singularFlux,
                         PDEModel::linearizedSingularFlux,
                         PDEModel::dirichlet,
                         PDEModel::linearizedDirichlet>
      , public RobinFluxMethod<std::decay_t<Model>, PenaltyFunction>
      , public LinearizedRobinFluxMethod<std::decay_t<Model>, PenaltyFunction>
      , public SingularFluxMethod<std::decay_t<Model>, Symmetrize{}>
      , public LinearizedSingularFluxMethod<std::decay_t<Model>, Symmetrize{}>
    {
      using ThisType = NitscheDirichletBoundaryModel;
      using TraitsType = ModelTraits<Model>;
      using BaseType = ModelCrop<Model,
                                 PDEModel::robinFlux,
                                 PDEModel::linearizedRobinFlux,
                                 PDEModel::singularFlux,
                                 PDEModel::linearizedSingularFlux,
                                 PDEModel::dirichlet,
                                 PDEModel::linearizedDirichlet>;
      using ModelDecay = std::decay_t<Model>;
      using PenaltyDecay = std::decay_t<PenaltyFunction>;
      using LocalPenalty = Fem::ConstLocalFunction<PenaltyDecay>;
      static constexpr std::ptrdiff_t symmetry = Symmetrize{};
      using RobinFluxBase = RobinFluxMethod<ModelDecay, PenaltyDecay>;
      using LinearizedRobinFluxBase = LinearizedRobinFluxMethod<ModelDecay, PenaltyDecay>;
      using SingularFluxBase = SingularFluxMethod<ModelDecay, symmetry>;
      using LinearizedSingularFluxBase = LinearizedSingularFluxMethod<ModelDecay, symmetry>;
      using ModelBaseType = ModelBase<typename BaseType::DomainFunctionSpaceType, typename BaseType::RangeFunctionSpaceType>;

      static_assert(HasTag<PenaltyDecay, Fem::HasLocalFunction>::value,
                    "PenaltyFunction must provide a local function");
      static_assert(std::is_convertible<typename PenaltyDecay::FunctionSpaceType::RangeFieldType,
                                        typename ModelDecay::RangeFieldType>::value,
                    "PenaltyFunction should be convertible to model's RangeFieldType");
      static_assert(IsSign<Symmetrize>::value,
                    "Symmetrize must be a sign.");

     public:
      using ModelType = Model;
      using typename BaseType::DomainType;
      using typename BaseType::DomainRangeType;
      using typename BaseType::DomainJacobianRangeType;
      using typename BaseType::RangeType;
      using typename BaseType::BoundaryConditionsType;

      using BaseType::model;

      template<class ModelArg, class FunctionArg,
               std::enable_if_t<(std::is_constructible<BaseType, ModelArg>::value
                                 && std::is_constructible<LocalPenalty, FunctionArg>::value
        ), int> = 0>
      NitscheDirichletBoundaryModel(ModelArg&& m, FunctionArg&& penalty,
                                    const std::string& name = "")
        : BaseType(m)
        , RobinFluxBase(model(), localPenalty_)
        , LinearizedRobinFluxBase(model(), localPenalty_)
        , SingularFluxBase(model())
        , LinearizedSingularFluxBase(model())
        , localPenalty_(std::forward<FunctionArg>(penalty))
        , name_(name == "" ? "NitscheDirichlet("+model().name()+")" : name)
      {}

      NitscheDirichletBoundaryModel(const ThisType& other)
        : BaseType(other)
        , RobinFluxBase(model(), localPenalty_)
        , LinearizedRobinFluxBase(model(), localPenalty_)
        , SingularFluxBase(model())
        , LinearizedSingularFluxBase(model())
        , localPenalty_(other.localPenalty_)
        , name_(other.name_)
      {}

      NitscheDirichletBoundaryModel(ThisType&& other)
        : BaseType(other)
        , RobinFluxBase(model(), localPenalty_)
        , LinearizedRobinFluxBase(model(), localPenalty_)
        , SingularFluxBase(model())
        , LinearizedSingularFluxBase(model())
        , localPenalty_(std::move(other.localPenalty_))
        , name_(std::move(other.name_))
      {}

      std::string name() const
      {
        return name_;
      }

      //!@copydoc ModelFacade::unbind
      template<class Entity>
      void bind(const Entity& entity)
      {
        BaseType::bind(entity);
        localPenalty_.bind(entity);
      }

      //!@copydoc ModelFacade::unbind
      void unbind()
      {
        localPenalty_.unbind();
        BaseType::unbind();
      }

      //!@copydoc ModelFacade::classifyBoundary
      template<class Intersection>
      auto classifyBoundary(const Intersection& intersection)
      {
        // The resulting model has only Robin kind boundary conditions.
        wrappedSupported_ = model().classifyBoundary(intersection);
        supported_.first = wrappedSupported_.first;
        return supported_;
      }

     protected:
      LocalPenalty localPenalty_;
      BoundaryConditionsType wrappedSupported_;
      BoundaryConditionsType supported_;
      std::string name_;
    };

    //!@} BoundaryValueModel

    /**@addtogroup ModelGenerators
     * @{
     */

    /**Wrap an existing model converting any Dirichlet boundary
     * conditions into weak Dirichlet boundary conditions with a
     * general penalty function.
     */
    template<class Model,
             class PenaltyFunction,
             class Symmetrize = SkeletonSymmetrizeDefault<Model>,
             std::enable_if_t<(IsProperPDEModel<Model>::value
                               && IsWrappableByConstLocalFunction<PenaltyFunction>::value
                               && !Expressions::IsPromotedTopLevel<PenaltyFunction>::value
                               && ModelMethodExists<Model, ModelIntrospection::dirichlet>::value
                               && IsSign<Symmetrize>::value
               ), int> = 0>
    auto nitscheDirichletModel(const Model& m, const PenaltyFunction& p, Symmetrize = Symmetrize{})
    {
      return expressionClosure(
        NitscheDirichletBoundaryModel<Model, PenaltyFunction, Symmetrize>(
          m, p
          )
        );
    }

    /**Wrap existing model converting any Dirichlet boundary
     * conditions into weak Dirichlet boundary conditions with a
     * standard penalty function.
     */
    template<class Model,
             class GridPart,
             class Param = decltype(ModelParameters::nitscheDirichletPenalty()),
             class Symmetrize = SkeletonSymmetrizeDefault<Model>,
             std::enable_if_t<(IsPDEModel<Model>::value
                               && ModelMethodExists<Model, ModelIntrospection::dirichlet>::value
                               && IsTensor<Param>::value
                               && IsSign<Symmetrize>::value
      ), int> = 0>
    auto nitscheDirichletModel(
      Model&& m,
      const GridPart& gridPart,
      Param&& p = ModelParameters::nitscheDirichletPenalty(),
      Symmetrize = Symmetrize{})
    {
      return nitscheDirichletModel(std::forward<Model>(m), std::forward<Param>(p) * meshPenalty(gridPart), Symmetrize{});
    }

    /**Just return the original model if it does not have Dirichlet
     * boundary conditions. This implies that Model is not an
     * expression closure.
     */
    template<class Model, class... T,
             std::enable_if_t<(IsProperPDEModel<Model>::value
                               && !ModelMethodExists<Model, ModelIntrospection::dirichlet>::value
      ), int> = 0>
    constexpr auto nitscheDirichletModel(Model&& m, T&&...)
    {
      return expressionClosure(std::forward<Model>(m));
    }

    /**Create a model supplying weak Dirichet conditions from a given
     * grid-function.
     */
    template<class GridFunction,
             class Param,
             class Indicator = EntireBoundaryIndicator,
             class Symmetrize = Sign<1>,
             std::enable_if_t<(IsWrappableByConstLocalFunction<GridFunction>::value
                               && IsBoundaryIndicator<Indicator>::value
                               && IsTensor<Param>::value
                               && IsSign<Symmetrize>::value
      ), int> = 0>
    auto
    nitscheDirichletModel(GridFunction&& values,
                          Param&& p = ModelParameters::nitscheDirichletPenalty(),
                          Indicator&& where = Indicator(),
                          Symmetrize = Symmetrize{})
    {
      return nitscheDirichletModel(
        dirichletBoundaryModel(std::forward<GridFunction>(values), std::forward<Indicator>(where)),
        std::forward<Param>(p) * meshPenalty(values.gridPart()),
        Symmetrize{});
    }

    //@} ModelGenerators

    //@} BasicModels

    //@} PDE-Models

  } // namespace ACFem::PDEModel

  namespace ACFem {

    using PDEModel::nitscheDirichletModel;

    template<class Model, class PenaltyFunction, class Symmetrize>
    struct ExpressionTraits<PDEModel::NitscheDirichletBoundaryModel<Model, PenaltyFunction, Symmetrize> >
      : ExpressionTraits<Model>
    {
      using ExpressionType = PDEModel::NitscheDirichletBoundaryModel<Model, PenaltyFunction, Symmetrize>;
      using Definiteness = typename ExpressionTraits<Model>::Definiteness;
      static constexpr bool isSymmetric = ExpressionTraits<Model>::isSymmetric && (Symmetrize{} == 1_f);
    };

  }

}  //Namespace Dune

#endif // __DUNE_ACFEM_MODELS_MODULES_NITSCHEDIRICHLETMODEL_HH__
