#ifndef __DUNE_ACFEM_MODELS_MODULES_INCOMPRESSIBLETRANSPORTMODEL_HH__
#define __DUNE_ACFEM_MODELS_MODULES_INCOMPRESSIBLETRANSPORTMODEL_HH__

#include <dune/fem/function/localfunction/const.hh>

#include "../../expressions/terminal.hh"

#include "../modelbase.hh"

namespace Dune {

  namespace ACFem::PDEModel {

    /**@addtogroup PDE-Models
     * @{
     */

    /**@addtogroup BasicModels
     * @{
     */

    /**@addtogroup BulkModel
     * @{
     */

    /**@brief Define a model for an advection term with a
     * divergence-free velocity field.
     *
     * In formulas: this fragment implements the right-hand-side of
     * the following equation:
     *
     * @f[
     * \int_\Omega \nabla\cdot(B\,U)\phi
     * =
     * \int_\Omega B\cdot\nabla U\,\phi
     * @f]
     *
     * This formula is only valid for divergence free velocity fields B.
     *
     * \see TransportModel, \ref TransportModelPage
     *
     * @param Velocity This is a grid-function implementing B.
     */
    template<class FunctionSpace, class GridFunction>
    class IncompressibleTransportModel
      : public ModelBase<FunctionSpace>
      , public Expressions::SelfExpression<IncompressibleTransportModel<FunctionSpace, GridFunction> >
    {
      static_assert(HasTag<GridFunction, Fem::HasLocalFunction>::value,
                    "GridFunction must provide a local function");

      using ThisType = IncompressibleTransportModel;
      using BaseType = ModelBase<FunctionSpace>;
     protected:
      using LocalFunctionType = Fem::ConstLocalFunction<std::decay_t<GridFunction> >;
     public:
      using GridFunctionType = GridFunction;
      using typename BaseType::RangeType;
      using typename BaseType::JacobianRangeType;
      using BaseType::dimDomain;
      using BaseType::dimRange;

      enum {
        dimWorld = dimDomain,
      };

      static_assert((int)std::decay_t<GridFunctionType>::FunctionSpaceType::dimRange == (int)dimWorld
                    &&
                    (int)dimDomain == (int)dimWorld,
                    "This is meant for dimensionworld vector-fields, "
                    "like fluids, deformations etc.");

      // Interface methods that need to be reimplemented

      template<class FunctionArg, std::enable_if_t<std::is_constructible<LocalFunctionType, FunctionArg>::value, int> = 0>
      IncompressibleTransportModel(FunctionArg&& velocity, const std::string& name = "")
        : localFunction_(std::forward<FunctionArg>(velocity))
        , name_(name == "" ? "(-U_iU_jD_iPhi_j+Bndry)" : name)
      {}

      std::string name() const
      {
        return name_;
      }

      /**@copydoc ModelFacade::bind() */
      template<class Entity>
      void bind(const Entity& entity)
      {
        localFunction_.bind(entity);
      }

      /**@copydoc ModelFacade::unbind() */
      void unbind()
      {
        localFunction_.unbind();
      }

      /**@copydoc ModelFacade::linearizedSource() */
      template<class Quadrature>
      auto linearizedSource(const QuadraturePoint<Quadrature> &x,
                            const JacobianRangeType& jacobian) const
      {
        const auto velocity = localFunction_.evaluate(x);

        RangeType result;
        jacobian.mv(velocity, result);
        return result;
      }

     protected:
      LocalFunctionType localFunction_;
      std::string name_;
    };

    //!@} BulkModel

    /**@addtogroup ModelGenerators
     * @{
     */

    /**\brief Generate an advection-model object.
     *
     * \copydetails massModel()
     *
     * @param[in] velocity The advection-velocity. This must already
     * be something with a localFunction() method. It is implicitly
     * assumed that the divergence of this object vanishes.
     */
    template<class Object, class Velocity>
    constexpr auto
    incompressibleTransportModel(Object&& object,
                                 Velocity&& velocity,
                                 const std::string& name = "")
    {
      return expressionClosure(IncompressibleTransportModel<typename std::decay_t<Object>::FunctionSpaceType, Velocity>(std::forward<Velocity>(velocity), name));
    }

    template<class Object, class Velocity, std::enable_if_t<ExpressionTraits<Velocity>::isZero, int> = 0>
    constexpr auto
    incompressibleTransportModel(Object&& object,
                                 Velocity&& velocity,
                                 const std::string& name = "")
    {
      return zeroModel(object, name);
    }

    //@} ModelGenerators

    //@} BasicModels

    //@} PDE-Models

  } // namespace ACFem::PDEModel

  namespace ACFem {

    using PDEModel::incompressibleTransportModel;

  }

}  //Namespace Dune


#endif // __DUNE_ACFEM_MODELS_MODULES_INCOMPRESSIBLETRANSPORTMODEL_HH__
