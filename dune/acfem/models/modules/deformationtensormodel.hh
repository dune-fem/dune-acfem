#ifndef __DUNE_ACFEM_MODELS_MODULES_DEFORMATIONTENSORMODEL_HH__
#define __DUNE_ACFEM_MODELS_MODULES_DEFORMATIONTENSORMODEL_HH__

#include "../../expressions/expressionoperations.hh"
#include "../../expressions/terminal.hh"

#include "../modelbase.hh"
#include "../expressiontraits.hh"

namespace Dune {

  namespace ACFem::PDEModel {

    /**@addtogroup PDE-Models
     * @{
     */

    /**@addtogroup BasicModels
     * @{
     */

    /**@addtogroup BulkModel
     * @{
     */

    /**\brief Define the a model where the flux-part is formed from the
     * symmetric gradient.
     *
     * The flux implements the second-order contribution
     *
     * @f[
     * \int_\Omega (\partial_i U_j + \partial_j U_i)\,\partial_i\phi_j
     * =
     * \frac12 \int_\Omega (\partial_i U_j + \partial_j U_i)\,(\partial_i\phi_j+\partial_j\phi_i)
     * @f]
     *
     * Of course, the above formula uses sum-convention. This is
     * indeed symmetric. Mind the factor 1/2. The
     * DeformationTensorModel::fluxDivergence() takes the form
     *
     * @f[
     * -\partial_i(\partial_i U_\alpha + \partial_\alpha U_i) = -\Delta U_\alpha - \partial_\alpha\nabla\cdot U.
     * @f]
     *
     * So for divergence free vector fields this would only be the
     * component-wise Laplacian.
     *
     * \note Obviously, this model make only sense for when dimDomain
     * = dimRange = GridPart::dimensionworld. For
     * GridPart::dimensionworld != GridPart::dimension one would also
     * have to discuss what the symmetric gradient means in terms of
     * tangential derivatives. In some sense also the vectorfield U
     * would have to be tangential.
     */
    template<class FunctionSpace>
    class DeformationTensorModel
      : public ModelBase<FunctionSpace>
      , public Expressions::SelfExpression<DeformationTensorModel<FunctionSpace> >
      , public MPL::UniqueTags<TypedValueExpression, SemiPositiveExpression, SymmetricModel>
    {
      using ThisType = DeformationTensorModel;
      using BaseType = ModelBase<FunctionSpace>;
     public:
      using typename BaseType::RangeType;
      using typename BaseType::JacobianRangeType;
      using typename BaseType::HessianRangeType;

      using BaseType::dimDomain;
      using BaseType::dimRange;
      enum {
        dimWorld = dimDomain
      };

      static_assert(dimDomain == dimRange && dimDomain == dimWorld,
                    "This is meant for dimensionworld vector-fields, "
                    "like fluids, deformations etc.");

      // Interface methods that need to be reimplemented

      DeformationTensorModel(const std::string& name = "")
        : name_(name == "" ? "(D_iU_j+ D_jU_i)" : name)
      {}

      std::string name() const
      {
        return name_;
      }

      //!@copydoc ModelFacade::linearizedFlux()
      JacobianRangeType linearizedFlux(const JacobianRangeType& jacobian) const
      {
        JacobianRangeType flux;
        // Why is there no transposeMatrix stuff, or an adaper??? anyhow ...
        for (int i = 0; i < dimWorld; ++i) {
          flux[i][i] = 2.*jacobian[i][i];
          for (int j = i+1; j < dimWorld; ++j) {
            flux[i][j] = flux[j][i] = jacobian[i][j] + jacobian[j][i];
          }
        }
        return flux;
      }

      //!@copydoc ModelFacade::fluxDivergence
      template<class Point>
      RangeType fluxDivergence(const HessianRangeType& hessian) const
      {
        RangeType result;
        // just do it in a straight-forward fashion ...
        for (int alpha = 0; alpha < dimWorld; ++alpha) {
          result[alpha] = 0;
          for (int i = 0; i < dimWorld; ++i) {
            // For divergence free vector fields the second term is zero.
            result[alpha] -= hessian[alpha][i][i] + hessian[i][alpha][i];
          }
        }
        return result;
      }

     protected:
      std::string name_;
    };

    //@} BulkModel

    /**@addtogroup ModelGenerators
     * @{
     */

    /**@brief Generate a deformation tensor model fitting the specified object.*/
    template<class Object>
    inline auto deformationTensorModel(const Object& object, const std::string& name = "")
    {
      typedef DeformationTensorModel<typename Object::FunctionSpaceType> ModelType;
      return expressionClosure(ModelType(name));
    }

    //@} ModelGenerators

    //@} BasicModels

    //@} PDE-Models

  } // namespace ACFem

  namespace ACFem {

    using PDEModel::deformationTensorModel;

  }

}  //Namespace Dune


#endif // __DUNE_ACFEM_MODELS_MODULES_DEFORMATIONTENSORMODEL_HH__
