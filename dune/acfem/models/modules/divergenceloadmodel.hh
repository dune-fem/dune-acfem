#ifndef __DUNE_ACFEM_MODELS_MODULES_DIVERGENCELOADMODEL_HH__
#define __DUNE_ACFEM_MODELS_MODULES_DIVERGENCELOADMODEL_HH__

#include <dune/fem/function/localfunction/const.hh>

#include "../../expressions/terminal.hh"
#include "../../functions/localfunctiontraits.hh"
#include "../modelbase.hh"
#include "../expressiontraits.hh"

namespace Dune {

  namespace ACFem::PDEModel {

    /**@addtogroup PDE-Models
     * @{
     */

    /**@addtogroup BasicModels
     * @{
     */

    /**@addtogroup BulkModel
     * @{
     */

    /**For a given grid-function define a model implementing the weak
     * divergence.
     *
     * In formulas: this fragment implements the integral
     *
     * @f[
     * \int_\Omega \partial_i v_i \, q
     * @f]
     *
     * Here q is a scalar test function.
     *
     * @param GridFunction The type of the function the weak
     * divergence has to be computed from. This must be a
     * grid-function, i.e. it has to carry a local function, and must
     * have dimRange == dimDomain.
     */
    template<class GridFunction>
    class DivergenceLoadModel
      : public ModelBase<typename std::decay_t<GridFunction>::FunctionSpaceType::ScalarFunctionSpaceType>
      , public Expressions::SelfExpression<DivergenceLoadModel<GridFunction> >
      , public MPL::UniqueTags<ConditionalType<IsConstantExprArg<GridFunction>::value, ConstantExpression, void>,
                               ConditionalType<IsTypedValue<GridFunction>::value, TypedValueExpression, void> >
    {
     public:
      using GridFunctionType = std::decay_t<GridFunction>;
     private:
      static_assert(IsWrappableByConstLocalFunction<GridFunction>::value,
                    "GridFunction must provide a local function");

      using ThisType = DivergenceLoadModel;
      using BaseType = ModelBase<typename GridFunctionType::FunctionSpaceType::ScalarFunctionSpaceType>;
     protected:
      using LocalFunctionType = Fem::ConstLocalFunction<GridFunctionType>;
     public:
      using typename BaseType::RangeType;
      using BaseType::dimDomain;
      using BaseType::dimRange;

      static constexpr auto dimWorld = dimDomain;

      static_assert(GridFunctionType::FunctionSpaceType::dimRange
                    ==
                    GridFunctionType::FunctionSpaceType::dimDomain,
                    "The divergence model is only for dimDomain-valued grid-functions defined");

      static_assert(dimRange == 1 && dimDomain == dimWorld,
                    "This is meant for dimRange = 1, i.e. velocity divergences and such.");

      template<class FctArg, std::enable_if_t<std::is_constructible<LocalFunctionType, FctArg>::value, int> = 0>
      DivergenceLoadModel(FctArg&& function, const std::string& name = "")
        : localFunction_(std::forward<FctArg>(function))
        , name_(name == "" ? "(div " + localFunction_.gridFunction().name() + ")" : name)
      {}

      std::string name() const
      {
        return name_;
      }

      // Interface methods that need to be reimplemented

      /**@copydoc ModelFacade::bind() */
      template<class Entity>
      void bind(const Entity& entity)
      {
        localFunction_.bind(entity);
      }

      /**@copydoc ModelFacade::unbind() */
      void unbind()
      {
        localFunction_.unbind();
      }

      /**@copydoc ModelFacade::source() */
      template<class Quadrature>
      RangeType source(const QuadraturePoint<Quadrature> &x) const
      {
        return trace(localFunction_.jacobian(x));
      }

     protected:
      LocalFunctionType localFunction_;
      std::string name_;
    };

    //@} BulkModel


    /**@addtogroup ModelGenerators
     * @{
     */

    /**\brief Generate a divergence model which only contributes to the load-vector.
     *
     * \copydetails massModel()
     */
    template<class GridFunction>
    constexpr auto
    divergenceLoadModel(GridFunction&& f, const std::string& name = "")
    {
      typedef DivergenceLoadModel<GridFunction> ModelType;
      return expressionClosure(ModelType(std::forward<GridFunction>(f), name));
    }

    template<class GridFunction, std::enable_if_t<ExpressionTraits<GridFunction>::isZero, int> = 0>
    constexpr auto
    divergenceLoadModel(GridFunction&& f, const std::string& name = "")
    {
      using FunctionSpaceType = typename std::decay_t<GridFunction>::FunctionSpaceType::ScalarFunctionSpaceType;
      return zeroModel(FunctionSpaceType{}, name);
    }

    //@} ModelGenerators

    //@} BasicModels

    //@} PDE-Models

  } // namespace ACFem::PDEModel

  namespace ACFem {

    using PDEModel::divergenceLoadModel;

  }

}  //Namespace Dune


#endif // __DUNE_ACFEM_MODELS_MODULES_DIVERGENCELOADMODEL_HH__
