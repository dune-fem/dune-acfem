#ifndef __DUNE_ACFEM_MODELS_MODULES_DIRICHLETMODEL_HH__
#define __DUNE_ACFEM_MODELS_MODULES_DIRICHLETMODEL_HH__

#include "../../expressions/expressionoperations.hh"
#include "../../functions/localfunctiontraits.hh"
#include "../../functions/modules/zero.hh"
#include "../indicators/boundaryindicator.hh"
#include "../modelbase.hh"
#include "../expressiontraits.hh"

namespace Dune {

  namespace ACFem::PDEModel {

    /**@addtogroup PDE-Models
     * @{
     */

    /**@addtogroup BasicModels
     * @{
     */

    /**@addtogroup BoundaryValueModel
     * @{
     */

    template<class GridFunction, class Indicator = EntireBoundaryIndicator, class SFINAE = void>
    class DirichletBoundaryModel;

    template<class GridFunction, class Indicator>
    class DirichletBoundaryModel<GridFunction, Indicator,
                                 std::enable_if_t<!IndicatorTraits<Indicator>::emptySupport
                                                  &&
                                                  !ExpressionTraits<GridFunction>::isZero> >
      : public ModelBase<typename std::decay_t<GridFunction>::FunctionSpaceType>
      , public Expressions::SelfExpression<DirichletBoundaryModel<GridFunction, Indicator> >
      , public MPL::UniqueTags<ConditionalType<IsConstantExprArg<GridFunction>::value
                                               &&
                                               IsConstantExprArg<Indicator>::value,
                                               ConstantExpression, void>,
                               ConditionalType<IsTypedValue<GridFunction>::value
                                               &&
                                               IsTypedValue<Indicator>::value,
                                               TypedValueExpression, void>,
                               VolatileExpression,
                               RegularZeroExpression,
                               SymmetricModel>
    {
      static_assert(HasTag<GridFunction, Fem::HasLocalFunction>::value,
                    "GridFunction must provide a local function");

      typedef ModelBase<typename std::decay_t<GridFunction>::FunctionSpaceType> BaseType;
      typedef DirichletBoundaryModel ThisType;
     public:
      using IndicatorType = Indicator;
      using GridFunctionType = GridFunction;
    protected:
      using LocalFunctionType = Fem::ConstLocalFunction<std::decay_t<GridFunction> >;
    public:
      using typename BaseType::FunctionSpaceType;
      using typename BaseType::RangeType;
      using typename BaseType::BoundaryConditionsType;
      using BaseType::dimRange;

      template<class FunctionArg, class IndicatorArg,
               std::enable_if_t<(std::is_constructible<LocalFunctionType, FunctionArg>::value
                                 && std::is_constructible<IndicatorType, IndicatorArg>::value
                                 && std::is_default_constructible<IndicatorArg>::value
        ), int> = 0>
      DirichletBoundaryModel(FunctionArg&& values,
                             IndicatorArg&& indicator = IndicatorArg{},
                             const std::string& name = "")
        : localFunction_(std::forward<FunctionArg>(values))
        , indicator_(std::forward<IndicatorArg>(indicator))
        , supported_(false, ~std::bitset<dimRange>())
        , name_(name == "" ? "Dirichlet(" + localFunction_.gridFunction().name() + ")" : name)
      {}

      template<class FunctionArg, class IndicatorArg,
               std::enable_if_t<(std::is_constructible<LocalFunctionType, FunctionArg>::value
                                 && std::is_constructible<IndicatorType, IndicatorArg>::value
                                 && !std::is_default_constructible<IndicatorArg>::value
        ), int> = 0>
      DirichletBoundaryModel(FunctionArg&& values,
                             IndicatorArg&& indicator,
                             const std::string& name = "")
        : localFunction_(std::forward<FunctionArg>(values))
        , indicator_(std::forward<IndicatorArg>(indicator))
        , supported_(false, ~std::bitset<dimRange>())
        , name_(name == "" ? "Dirichlet(" + localFunction_.gridFunction().name() + ")" : name)
      {}

      /**@copydoc ModelFacade::bind() */
      template<class Entity>
      void bind(const Entity& entity)
      {
        localFunction_.bind(entity);
      }

      /**@copydoc ModelFacade::unbind() */
      void unbind()
      {
        localFunction_.unbind();
      }

      //!@copydoc ModelFacade::classifyBoundary
      template<class Intersection>
      auto classifyBoundary(const Intersection& intersection)
      {
        supported_.first = IndicatorTraits<IndicatorType>::globalSupport || indicator_.applies(intersection);
        return supported_;
      }

      //!@copydoc ModelFacade::dirichlet()
      template<class Quadrature>
      RangeType dirichlet(const QuadraturePoint<Quadrature>& x, const RangeType& value) const
      {
        if (IndicatorTraits<IndicatorType>::globalSupport || supported_.first) {
          return value - localFunction_.evaluate(x);
        } else {
          return RangeType(0.);
        }
      }

      //!@copydoc ModelFacade::linearizedDirichlet()
      auto linearizedDirichlet(const RangeType& value) const
      {
        if (IndicatorTraits<IndicatorType>::globalSupport || supported_.first) {
          return value;
        } else {
          return RangeType(0.);
        }
      }

      std::string name() const
      {
        return name_;
      }

     protected:
      LocalFunctionType localFunction_;
      IndicatorType indicator_;
      BoundaryConditionsType supported_;
      std::string name_;
    };

    template<class GridFunction, class Indicator>
    class DirichletBoundaryModel<GridFunction, Indicator,
                                 std::enable_if_t<(!IndicatorTraits<Indicator>::emptySupport
                                                   &&
                                                   ExpressionTraits<GridFunction>::isZero)> >
      : public ModelBase<typename GridFunction::FunctionSpaceType>
      , public Expressions::SelfExpression<DirichletBoundaryModel<GridFunction, Indicator> >
      , public MPL::UniqueTags<ConditionalType<IsConstantExprArg<Indicator>::value,
                                               ConstantExpression, void>,
                               ConditionalType<IsTypedValue<Indicator>::value,
                                               TypedValueExpression, void>,
                               VolatileExpression,
                               RegularZeroExpression,
                               SymmetricModel>
    {
      typedef ModelBase<typename GridFunction::FunctionSpaceType> BaseType;
      typedef DirichletBoundaryModel ThisType;
     public:
      using IndicatorType = Indicator;
      using GridFunctionType = GridFunction;
      using typename BaseType::FunctionSpaceType;
      using typename BaseType::RangeType;
      using typename BaseType::BoundaryConditionsType;

      template<class IndicatorArg,
               std::enable_if_t<(std::is_constructible<IndicatorType, IndicatorArg>::value
                                 && std::is_default_constructible<IndicatorArg>::value
        ), int> = 0>
      DirichletBoundaryModel(const std::decay_t<GridFunction>&,
                             IndicatorArg&& indicator = IndicatorArg{},
                             const std::string& name = "")
        : indicator_(std::forward<IndicatorArg>(indicator))
        , supported_({})
        , name_(name == "" ? "Dirichlet(0)" : name)
      {}

      template<class IndicatorArg,
               std::enable_if_t<(std::is_constructible<IndicatorType, IndicatorArg>::value
                                 && !std::is_default_constructible<IndicatorArg>::value
        ), int> = 0>
      DirichletBoundaryModel(const std::decay_t<GridFunction>&,
                             IndicatorArg&& indicator,
                             const std::string& name = "")
        : indicator_(std::forward<IndicatorArg>(indicator))
        , supported_({})
        , name_(name == "" ? "Dirichlet(0)" : name)
      {}

      //!@copydoc ModelFacade::classifyBoundary
      template<class Intersection>
      auto classifyBoundary(const Intersection& intersection)
      {
        supported_.first = IndicatorTraits<IndicatorType>::globalSupport || indicator_.applies(intersection);
        if (supported_.first) {
          supported_.second.set(); // set all bits
        }
        return supported_;
      }

      //!@copydoc ModelFacade::linearizedDirichlet()
      auto linearizedDirichlet(const RangeType& value) const
      {
        if (IndicatorTraits<IndicatorType>::globalSupport || supported_.first) {
          return value;
        } else {
          return RangeType(0.);
        }
      }

      std::string name() const
      {
        return name_;
      }

     protected:
      IndicatorType indicator_;
      BoundaryConditionsType supported_;
      std::string name_;
    };

    //!@} BoundaryValueModel

    /**@addtogroup ModelGenerators
     * @{
     */

    //!Generate the zero model for the empty indicator.
    template<
      class T, class Indicator,
      std::enable_if_t<ExpressionTraits<Indicator>::isZero, int> = 0>
    constexpr auto dirichletBoundaryModel(T&& values, Indicator&& where, const std::string& name = "")
    {
      return zeroModel(values);
    }

    /**\brief Generate DirichletBoundaryModel from given grid-function and
     *  boundary indicator.
     *
     * @param[in] values The Dirichlet boundary values.
     *
     * @param[in] where Indicator which decides which part of the
     *                  boundary is affected
     */
    template<
      class T, class Indicator = EntireBoundaryIndicator,
      std::enable_if_t<(IsWrappableByConstLocalFunction<T>::value
                        && !GridFunction::HasRegularity<T, 1>::value
      ), int> = 0>
    constexpr auto dirichletBoundaryModel(T&& values, Indicator&& where = std::decay_t<Indicator>{},
                                          const std::string& name = "")
    {
      return expressionClosure(
        DirichletBoundaryModel<T, Indicator>(std::forward<T>(values), std::forward<Indicator>(where), name));
    }

    template<
      class T, class Indicator = EntireBoundaryIndicator,
      std::enable_if_t<GridFunction::HasRegularity<T, 1>::value, int> = 0>
    constexpr auto dirichletBoundaryModel(T&& values, Indicator&& where = std::decay_t<Indicator>{},
                                          const std::string& name = "")
    {
      using namespace Literals;
      return dirichletBoundaryModel(
        gridFunction(values.gridPart(), std::forward<T>(values), 0_c),
        std::forward<Indicator>(where),
        name);
    }

    /**\brief Generate homogeneous Dirichlet boundary conditions
     * fitting the specified object.
     *
     * In order for this to work the data-type of the given object
     * must have the two sub-types Object::FunctionSpaceType and
     * Object::GridPartType and a method object.gridPart().
     *
     * @param[in] object Super-object, the DirichletBoundaryModel
     * generated will be compatible to this object.
     *
     * @param[in] where Indicator which decides where the Dirichlet
     * b.c. apply.
     *
     * Examples of suitable objects are
     *
     * - another model
     *
     * - Fem::DiscreteFunctionSpace
     *
     * - Fem::BindableGridFunction
     *
     * \see CompatibleModel.
     */
    template<class Object, class Indicator = EntireBoundaryIndicator,
             std::enable_if_t<!IsWrappableByConstLocalFunction<Object>::value, int> = 0>
    auto
    dirichletZeroModel(const Object& object,
                       Indicator&& where = std::decay_t<Indicator>{},
                       const std::string& name = "")
    {
      return dirichletBoundaryModel(GridFunction::zeros(object), std::forward<Indicator>(where), name);
    }

    //@} ModelGenerators

    //@} BasicModels

    //@} PDE-Models

  } // namespace ACFem::PDEModel::

  namespace ACFem {
    using PDEModel::dirichletZeroModel;
    using PDEModel::dirichletBoundaryModel;
  }

}  //Namespace Dune


#endif // __DUNE_ACFEM_MODELS_MODULES_DIRICHLETMODEL_HH__
