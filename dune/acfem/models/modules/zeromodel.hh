#ifndef __DUNE_ACFEM_MODELS_MODULES_ZEROMODEL_HH__
#define __DUNE_ACFEM_MODELS_MODULES_ZEROMODEL_HH__

#include "../../expressions/terminal.hh"

#include "../modelbase.hh"
#include "../modeltraits.hh"

namespace Dune {

  namespace ACFem::PDEModel {

    /**@addtogroup PDE-Models
     * @{
     */

    /**@addtogroup BasicModels
     * @{
     */

    /**Define a simple zero model to optimize expression
     * templates. Actually, this is only of very little use: the
     * ModelInterface is only partly a @f$L^\infty@f$ module, only
     * partly a vector space, but nevertheless ModelExpression
     * templates ease your live.
     */
    template<class DomainFunctionSpace, class RangeFunctionSpace = DomainFunctionSpace>
    class ZeroModel
      : public ModelBase<DomainFunctionSpace, RangeFunctionSpace>
      , public Expressions::SelfExpression<ZeroModel<DomainFunctionSpace, RangeFunctionSpace> >
      , public ZeroExpression
    {
     public:
      ZeroModel(const std::string& name = "")
        : name_(name == "" ? "0" : name)
      {}

      std::string name() const
      {
        return name_;
      }

     protected:
      const std::string name_;
    };

    /**@addtogroup ModelGenerators
     * @{
     */

    /**\brief Generate a zero model fitting the specified object.
     *
     * In order for this to work the data-type of the given object
     * must have the sub-type Object::FunctionSpaceType. The actual
     * instance of the object is ignore and simply has to be passed in
     * order that the compiler can deduce the correct types.
     *
     * @param[in] t Ignored. We use only the type to get hold of
     * the FunctionSpaceType.
     *
     * @param[in] name Optional. A descriptive name for the generated
     * model. A suitable default will be chosen if omitted.
     *
     * Examples of suitable objects are something that satisfies the
     *
     * - ModelInterface (another model)
     *
     * - Fem::DiscreteFunctionSpaceInterface
     *
     * - Fem::DiscreteFunctionInterface (including any wrapped or
     *   adapted non-discrete function)
     *
     * \see CompatibleModel.
     */
    template<class T, class F = Expressions::Closure,
             std::enable_if_t<IsPDEModel<T>::value, int> = 0>
    auto zeroModel(const T& t, const std::string& name, F closure = F{})
    {
      return closure(ZeroModel<typename T::DomainFunctionSpaceType, typename T::RangeFunctionSpaceType>(name));
    }

    /**@copydoc zeroModel()
     *
     * Variant ommitting the name parameter.
     */
    template<class T, class F = Expressions::Closure, std::enable_if_t<IsPDEModel<T>::value, int> = 0>
    auto zeroModel(const T&, F closure = F{})
    {
      return closure(ZeroModel<typename T::DomainFunctionSpaceType, typename T::RangeFunctionSpaceType>());
    }

    /**@copydoc zeroModel()
     *
     * Variant for use without an instance of T, and without a name.
     */
    template<class T, class F = Expressions::Closure, std::enable_if_t<IsPDEModel<T>::value, int> = 0>
    auto zeroModel(F closure = F{})
    {
      return closure(ZeroModel<typename T::DomainFunctionSpaceType, typename T::RangeFunctionSpaceType>());
    }

    //@} ModelGenerators

    //@} BasicModels

    //@} PDE-Models

  } // namespace ACFem::PDEModel

  namespace ACFem {

    using PDEModel::zeroModel;

  }

}  //Namespace Dune


#endif // __DUNE_ACFEM_MODELS_ZEROMODEL_HH__
