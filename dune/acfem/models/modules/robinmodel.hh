#ifndef __DUNE_ACFEM_MODELS_MODULES_ROBINMODEL_HH__
#define __DUNE_ACFEM_MODELS_MODULES_ROBINMODEL_HH__

#include "../indicators/boundaryindicator.hh"
#include "../modelbase.hh"
#include "../expressiontraits.hh"

namespace Dune {

  namespace ACFem::PDEModel {

    /**@addtogroup PDE-Models
     * @{
     */

    /**@addtogroup BasicModels
     * @{
     */

    /**@addtogroup BoundaryValueModel
     * @{
     */

    /**A (homogeneous) Robin-boundary model. (linear) Robin
     * boundary-conditions are in general boundary conditions of the
     * following form
     *
     * @f[
     * (A(x)\nabla u(x))\cdot\nu(x) = c(x)\,(u^{\text{ext}}(x) - u(x))\text{ on }\Gamma_R
     * @f]
     *
     * where @f$A(x)@f$ is the coefficient matrix of the principle
     * part of an associated PDE of 2nd order, @f$c(x)@f$ is a given
     * wnon-negative cofficient function and @f$u^{\text{ext}}(x)@f$ is
     * some given function.
     *
     * This models the scenario that the flux of the qunatity u over
     * the boundary is proportional to the difference to some
     * prescribed external temperature distribution with transition
     * coefficient @f$c(x)@f$.
     *
     * This particular model implements the following homogeneous
     * special case:
     *
     * @f[
     * (A(x)\nabla u(x))\cdot\nu(x) = - u(x))\text{ on }\Gamma_R
     * @f]
     *
     * The general non-homogeneous case with arbitrary coefficient
     * function can be obtained by subtracting the inhomogeneity from
     * the model and multiplying the resulting non-homogeneous
     * Robin-model with transition coefficent 1 by the desired
     * transition coefficent, which may be a constant, parameter of
     * function.
     *
     * @param[in] Indicator The boundary indicator function which
     * decides on which part of the boundary the Robin boundary
     * conditions is imposed.
     *
     * \note Note also that the  inhomogeneity uExt is internally
     * incorporated into the Neumann boundary data.
     */
    template<class FunctionSpace, class Indicator = EntireBoundaryIndicator>
    class RobinBoundaryModel
      : public ModelBase<FunctionSpace>
      , public Expressions::SelfExpression<RobinBoundaryModel<FunctionSpace, Indicator> >
      , public MPL::UniqueTags<ConditionalType<ExpressionTraits<Indicator>::isVolatile, VolatileExpression, void>,
                               ConditionalType<IsConstantExprArg<Indicator>::value, ConstantExpression, void>,
                               ConditionalType<IsTypedValue<Indicator>::value, TypedValueExpression, void>,
                               PositiveExpression,
                               SymmetricModel>
    {
      using ThisType = RobinBoundaryModel;
      using BaseType = ModelBase<FunctionSpace>;
     public:
      typedef Indicator IndicatorType;

      using typename BaseType::DomainType;
      using typename BaseType::RangeType;
      using typename BaseType::BoundaryConditionsType;
      using BaseType::dimRange;

      template<class IndicatorArg,
               std::enable_if_t<(std::is_constructible<Indicator, IndicatorArg>::value
                                 && std::is_default_constructible<IndicatorArg>::value
        ), int> = 0>
      RobinBoundaryModel(IndicatorArg&& indicator = IndicatorArg(), const std::string& name = "")
        : indicator_(std::forward<IndicatorArg>(indicator)),
          supported_(false, std::bitset<dimRange>()),
          name_(name == "" ? "Robin(1)" : name)
      {}

      template<class IndicatorArg,
               std::enable_if_t<(std::is_constructible<Indicator, IndicatorArg>::value
                                 && !std::is_default_constructible<IndicatorArg>::value
        ), int> = 0>
      RobinBoundaryModel(IndicatorArg&& indicator, const std::string& name = "")
        : indicator_(std::forward<IndicatorArg>(indicator)),
          supported_(false, std::bitset<dimRange>()),
          name_(name == "" ? "Robin(1)" : name)
      {}

      std::string name() const
      {
        return name_;
      }

     protected:
      enum {
        emptySupport = IndicatorTraits<IndicatorType>::emptySupport,
        globalSupport = IndicatorTraits<IndicatorType>::globalSupport
      };

     public:
      //!@copydoc ModelInterface::classifyBoundary
      template<class Intersection>
      auto classifyBoundary(const Intersection& intersection)
      {
        supported_.first = IndicatorTraits<IndicatorType>::globalSupport || indicator_.applies(intersection);
        return supported_;
      }

      //!@copydoc ModelInterface::linearizedRobinFlux()
      auto linearizedRobinFlux(const DomainType& unitOuterNormal,
                               const RangeType& value) const
      {
        if constexpr (globalSupport) {
          return value;
        } else if constexpr (emptySupport) {
          return zero(value);
        } else {
          return supported_.first ? value : RangeType(0.);
        }
      }

     protected:
      IndicatorType indicator_;
      BoundaryConditionsType supported_;
      std::string name_;
    };

    //!@} BoundaryValueModel

    /**@addtogroup ModelGenerators
     * @{
     */

    /**\brief Generate homogeneous Robin boundary conditions
     *fitting the specified object.
     *
     * In order for this to work the data-type of the given object
     * must have the two sub-types Object::FunctionSpaceType and
     * Object::GridPartType and a method object.gridPart().
     *
     * @param[in] object Super-object, the RobinBoundaryModel
     * generated will be compatible to this object.
     *
     * @param[in] where Indicator which decides where the Robin
     * b.c. apply.
     *
     * Examples of suitable objects are something that satisfies the
     *
     * - ModelInterface (another model)
     *
     * - Fem::DiscreteFunctionSpaceInterface
     *
     * - Fem::DiscreteFunctionInterface (including any wrapped or
     *   adapted non-discrete function)
     *
     * \see CompatibleModel.
     */
    template<class Object, class Indicator = EntireBoundaryIndicator>
    constexpr  auto
    robinZeroModel(const Object& object,
                   Indicator&& where = Indicator{},
                   const std::string& name = "")
    {
      typedef RobinBoundaryModel<typename Object::FunctionSpaceType, Indicator> ModelType;
      return expressionClosure(ModelType(std::forward<Indicator>(where), name));
    }

    template<class Object, class Indicator, std::enable_if_t<ExpressionTraits<Indicator>::isZero, int> = 0>
    constexpr  auto
    robinZeroModel(const Object& object, Indicator&& where, const std::string& name = "")
    {
      return zeroModel(object);
    }

    /**\brief Generate a RobinBoundaryModel from given grid-function and
     *  boundary indicator.
     *
     * @param[in] values The Robin boundary values.
     *
     * @param[in] where Indicator which decides which part of the
     *                  boundary is affected
     */
    template<class GridFunction, class Indicator = EntireBoundaryIndicator>
    constexpr auto
    robinBoundaryModel(GridFunction&& values,
                       Indicator&& where = Indicator(),
                       const std::string& name = "")
    {
      return
        robinZeroModel(
          std::forward<GridFunction>(values), std::forward<Indicator>(where), name)
        +
        neumannBoundaryModel(
          std::forward<GridFunction>(values), std::forward<Indicator>(where), name);
    }

    //@} ModelGenerators

    //@} BasicModels

    //@} PDE-Models

  } // namespace ACFem::PDEModel::

  namespace ACFem {

    using PDEModel::robinBoundaryModel;
    using PDEModel::robinZeroModel;

  }

}  //Namespace Dune


#endif // __DUNE_ACFEM_MODELS_MODULES_ROBINMODEL_HH__
