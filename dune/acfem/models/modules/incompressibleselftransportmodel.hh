#ifndef __DUNE_ACFEM_MODELS_MODULES_INCOMPRESSIBLESELFTRANSPORTMODEL_HH__
#define __DUNE_ACFEM_MODELS_MODULES_INCOMPRESSIBLESELFTRANSPORTMODEL_HH__

#include "../../expressions/terminal.hh"

#include "../modelbase.hh"

namespace Dune {

  namespace ACFem::PDEModel {

    /**@addtogroup PDE-Models
     * @{
     */

    /**@addtogroup BasicModels
     * @{
     */

    /**@addtogroup BulkModel
     * @{
     */

    /**\brief Define a model for the "Navier-Stokes" non-lineariry.
     *
     * This fragment models the change in momentum caused by the
     * transport via the velocity field of the fluid on the fluid
     * itself. This forms half of the so called "material derivative"
     * for time-dependent flows and the entire material derivative for
     * stationary flow problems. The "material derivative" of the
     * velocity field of the fluid is the change of the velocity
     * measured by an observier moving with the fluid.
     *
     * In formulas: this fragment implements the non-linear "source" term
     *
     * @f[
     * \int_\Omega U_i\,\partial_i U_j\,\phi_j
     * @f]
     *
     * Of course, the formula uses sum-convention. There is also
     * another variant available which move the derivative away from U
     * to the test-function at the cost of introducing a boundary
     * integral.
     *
     * \see FluidSelfTransportModel, DeformationTensorModel, \ref TransportModelPage
     */
    template<class FunctionSpace>
    class IncompressibleSelfTransportModel
      : public ModelBase<FunctionSpace>
      , public Expressions::SelfExpression<IncompressibleSelfTransportModel<FunctionSpace> >
      , public TypedValueExpression
    {
      using ThisType = IncompressibleSelfTransportModel;
      using BaseType = ModelBase<FunctionSpace>;
     public:
      using typename BaseType::RangeType;
      using typename BaseType::JacobianRangeType;
      using BaseType::dimDomain;
      using BaseType::dimRange;

      static constexpr int dimWorld = dimDomain;

      static_assert(dimDomain == dimRange && dimDomain == dimWorld,
                    "This is meant for dimensionworld vector-fields, "
                    "like fluids, deformations etc.");

      // Interface methods that need to be reimplemented

      IncompressibleSelfTransportModel(const std::string& name = "")
        : name_(name == "" ? "U_iD_iU_j" : name)
      {}

      std::string name() const
      {
        return name_;
      }

      /**@copydoc ModelFacade::source() */
      auto source(const RangeType& value,
                  const JacobianRangeType& jacobian) const
      {
        RangeType result;
        jacobian.mv(value, result);
        return result;
      }

      /**@copydoc ModelFacade::linearizedSource() */
      auto linearizedSource(const RangeType& uBar,
                            const JacobianRangeType& DuBar,
                            const RangeType& value,
                            const JacobianRangeType& jacobian) const
      {
        RangeType result;
        DuBar.mv(value, result);
        jacobian.umv(uBar, result);
        return result;
      }

     protected:
      std::string name_;
    };

    //@} BulkModel

    /**@addtogroup ModelGenerators
     * @{
     */

    /**\brief Generate a Navier-Stokes non-linearity fitting the given
     * object.
     *
     * This variant moves the derivative to the test function at the
     * cost of introducing a boundary integral.
     *
     * \copydetails massModel()
     */
    template<class Object>
    static inline auto
    incompressibleSelfTransportModel(const Object& object, const std::string& name = "")
    {
      return expressionClosure(IncompressibleSelfTransportModel<typename Object::FunctionSpaceType>(name));
    }

    //@} ModelGenerators

    //@} BasicModels

    //@} PDE-Models

  } // namespace ACFem::PDEModel

  namespace ACFem {

    using PDEModel::incompressibleSelfTransportModel;

  }

}  //Namespace Dune


#endif // __DUNE_ACFEM_MODELS_MODULES_INCOMPRESSIBLESELFTRANSPORTMODEL_HH__
