#ifndef __DUNE_ACFEM_MODELS_MODEL_HH__
#define __DUNE_ACFEM_MODELS_MODEL_HH__

#include "basicmodels.hh"
#include "definiteness.hh"
#include "expressions.hh"
#include "modeltraits.hh"
#include "ostream.hh"

#endif // __DUNE_ACFEM_MODELS_MODEL_HH__
