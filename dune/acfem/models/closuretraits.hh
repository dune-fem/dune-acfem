#ifndef __DUNE_ACFEM_MODELS_CLOSURETRAITS_HH__
#define __DUNE_ACFEM_MODELS_CLOSURETRAITS_HH__

#include "../expressions/interface.hh"
#include "operationtraits.hh"
#include "operations/closure.hh"

namespace Dune
{
  namespace ACFem
  {

    namespace PDEModel
    {

      /**Generate the actual return type for model expressions. The
       * "closure" adds "missing" methods and call-signatures.
       */
      template<class T, std::enable_if_t<(IsPDEModel<T>::value && !Expressions::IsClosure<T>::value), int> = 0>
      auto expressionClosure(T&& t)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        return operate(
          Expressions::DontOptimize{}, OperationTraits<ClosureOperation<> >{},
          std::forward<T>(t)
          );
      }

    }

    namespace Expressions
    {

      /**Identify the expression closure.*/
      template<class T>
      struct IsClosure<
        T,
        std::enable_if_t<(IsDecay<T>::value
                          && IsPDEModel<T>::value
                          && IsUnaryExpression<T>::value
                          && !IsSelfExpression<T>::value
        )> >
      : BoolConstant<(std::is_same<typename ModelIntrospection::Methods<T>, ModelAdmissibleMethods>::value
                      &&
                      std::is_same<typename ModelIntrospection::MethodCallSignatures<T>, ModelMethodClosureSignatures>::value)>
      {};

    } // NS Expressions

  } // NS ACFem

} // NS Dune

#endif //  __DUNE_ACFEM_MODELS_CLOSURETRAITS_HH__
