#ifndef __DUNE_ACFEM_MODELS_OSTREAM_HH__
#define __DUNE_ACFEM_MODELS_OSTREAM_HH__

#include <ostream>

#include "modeltraits.hh"
#include "expressiontraits.hh"

namespace Dune {

  namespace ACFem {

    namespace PDEModel {

      using namespace ModelIntrospection;

      template<class Model>
      std::ostream& printModelStructure(std::ostream& out, Model&& model)
      {
        using ModelType = std::decay_t<Model>;
        using TraitsType = ModelTraits<ModelType>;

        out << "Model: " << model.name() << std::endl;

        //out << "source sigs: " << std::endl << typename TraitsType::template FilteredSignatures<source>{} << std::endl;

        //Traits<Model>::Methods,
        out << "Methods:" << std::endl << typename TraitsType::Methods{} << std::endl;
        out << "MethodsType: " << typeString(typename TraitsType::Methods{}) << std::endl;

        //Traits<Model>::MethodCallSignatures
        out << "Signatures:" << std::endl << typename TraitsType::MethodCallSignatures{} << std::endl;
        out << "SignaturesType: " << typeString(typename TraitsType::MethodCallSignatures{}) << std::endl;

        out << "****** linear or non-linearly implemented methods ******" << std::endl;
        out << std::endl;
        out << "exists flux:         " << TraitsType::template Exists<flux>::value << std::endl;
        out << "exists source:       " << TraitsType::template Exists<source>::value << std::endl;
        out << "exists robinFlux:    " << TraitsType::template Exists<robinFlux>::value << std::endl;
        out << "exists singularFlux: " << TraitsType::template Exists<singularFlux>::value << std::endl;
        out << "exists dirichlet:    " << TraitsType::template Exists<dirichlet>::value << std::endl;
        out << std::endl;

        out << "****** particular methods implemented ******" << std::endl;
        out << std::endl;
        out << "has flux():                   " << TraitsType::template HasMethod<flux>::value << std::endl;
        out << "has linearizedFlux:           " << TraitsType::template HasMethod<linearizedFlux>::value << std::endl;
        out << "has fluxDivergence:           " << TraitsType::template HasMethod<fluxDivergence>::value << std::endl;
        out << "has AffineLinearFlux:         " << TraitsType::template IsAffineLinear<flux>::value << std::endl;
        out << "has LinearFlux:               " << TraitsType::template IsLinear<linearizedFlux>::value << std::endl;
        out << "flux is load:                 " << TraitsType::template IsLoad<flux>::value << std::endl;
        out << "flux p.w. const.              " << TraitsType::template IsPiecewiseConstant<flux>::value << std::endl;
        out << "lin. flux p.w. const.         " << TraitsType::template IsPiecewiseConstant<linearizedFlux>::value << std::endl;
        out << std::endl;
        out << "has source:                   " << TraitsType::template HasMethod<source>::value << std::endl;
        out << "has linearizedSource:         " << TraitsType::template HasMethod<linearizedSource>::value << std::endl;
        out << "has AffineLinearSource:       " << TraitsType::template IsAffineLinear<source>::value << std::endl;
        out << "has LinearSource:             " << TraitsType::template IsLinear<linearizedSource>::value << std::endl;
        out << "source is load:               " << TraitsType::template IsLoad<source>::value << std::endl;
        out << "source p.w. const.            " << TraitsType::template IsPiecewiseConstant<source>::value << std::endl;
        out << "lin. source p.w. const.       " << TraitsType::template IsPiecewiseConstant<linearizedSource>::value << std::endl;
        out << std::endl;
        out << "has robinFlux:                " << TraitsType::template HasMethod<robinFlux>::value << std::endl;
        out << "has linearizedRobinFlux:      " << TraitsType::template HasMethod<linearizedRobinFlux>::value << std::endl;
        out << "has AffineLinearRobinFlux:    " << TraitsType::template IsAffineLinear<robinFlux>::value << std::endl;
        out << "has LinearRobinFlux:          " << TraitsType::template IsLinear<linearizedRobinFlux>::value << std::endl;
        out << "robinFlux is load:            " << TraitsType::template IsLoad<robinFlux>::value << std::endl;
        out << "robinFlux p.w. const.         " << TraitsType::template IsPiecewiseConstant<robinFlux>::value << std::endl;
        out << "lin. robinFlux p.w. const.    " << TraitsType::template IsPiecewiseConstant<linearizedRobinFlux>::value << std::endl;
        out << std::endl;
        out << "has singularFlux:             " << TraitsType::template HasMethod<singularFlux>::value << std::endl;
        out << "has linearizedSingularFlux:   " << TraitsType::template HasMethod<linearizedSingularFlux>::value << std::endl;
        out << "has AffineLinearSingularFlux: " << TraitsType::template IsAffineLinear<singularFlux>::value << std::endl;
        out << "has LinearSingularFlux:       " << TraitsType::template IsLinear<linearizedSingularFlux>::value << std::endl;
        out << "singularFlux is load:         " << TraitsType::template IsLoad<singularFlux>::value << std::endl;
        out << "singularFlux p.w. const.      " << TraitsType::template IsPiecewiseConstant<singularFlux>::value << std::endl;
        out << "lin. singularFlux p.w. const. " << TraitsType::template IsPiecewiseConstant<linearizedSingularFlux>::value << std::endl;
        out << std::endl;
        out << "has dirichlet:                " << TraitsType::template HasMethod<dirichlet>::value << std::endl;
        out << "has linearizedDirichlet:      " << TraitsType::template HasMethod<linearizedDirichlet>::value << std::endl;
        out << "has AffineLinearDirichlet:    " << TraitsType::template IsAffineLinear<dirichlet>::value << std::endl;
        out << "has LinearDirichlet:          " << TraitsType::template IsLinear<linearizedDirichlet>::value << std::endl;
        out << "dirichlet is load:            " << TraitsType::template IsLoad<dirichlet>::value << std::endl;
        out << "dirichlet p.w. const.         " << TraitsType::template IsPiecewiseConstant<dirichlet>::value << std::endl;
        out << "lin. dirichlet p.w. const.    " << TraitsType::template IsPiecewiseConstant<linearizedDirichlet>::value << std::endl;
        out << std::endl;

        out << "****** global structure flags ******" << std::endl;
        out << "isAffineLinear:               " << TraitsType::isAffineLinear << std::endl;
        out << "isLinear:                     " << TraitsType::isLinear << std::endl;
        out << "isZero:                       " << TraitsType::isZero << std::endl;
        out << "isLoad:                       " << TraitsType::isLoad << std::endl;
        out << "isPiecewiseConstant           " << TraitsType::isPiecewiseConstant << std::endl;
        out << "isSymmetric:                  " << ExpressionTraits<ModelType>::isSymmetric << std::endl;

        using Sign = typename ExpressionTraits<ModelType>::Definiteness;

        out << "isNonSingular:                " << Sign::isNonSingular << std::endl;
        out << "isSemiPositive:               " << Sign::isSemiPositive << std::endl;
        out << "isPositive:                   " << Sign::isPositive << std::endl;
        out << "isSemiNegative:               " << Sign::isSemiNegative << std::endl;
        out << "isNegative:                   " << Sign::isNegative << std::endl;
        return out;
      }

    } // PDEModel::

    template<class Model, std::enable_if_t<IsPDEModel<Model>::value, int> = 0>
    std::ostream& operator<<(std::ostream& out, const Model& model)
    {
      return PDEModel::printModelStructure(out, model);
    }

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_MODELS_OSTREAM_HH__
