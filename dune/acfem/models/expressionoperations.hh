#ifndef __DUNE_ACFEM_MODELS_EXPRESSIONOPERATIONS_HH__
#define __DUNE_ACFEM_MODELS_EXPRESSIONOPERATIONS_HH__

/**@file Add-ons for expressions operations which do not make sense without tensors.*/

#include "../expressions/expressionoperations.hh"
#include "modeltraits.hh"

namespace Dune {

  namespace ACFem {

    namespace PDEModel {

      using namespace ModelIntrospection;

      /**@addtogroup ExpressionTemplates
       *
       * @{
       *
       */

      /**@addtogroup ExpressionOperations
       *
       * "Tag"-structures for each supported algebraic operation, used
       * to distinguish the various expression template classes from
       * each other.
       *
       * @{
       */

      template<class T>
      struct IsDedicatedOperation
        : FalseType
      {};

      ///////////////////////////////////////////////

      /**Remove all methods which are not listed in @a MethodTags. @a
       * MethodTags must be an ACFem::IndexSequence.
       */
      template<class MethodTags>
      struct CropOperation
      {};

      template<class MethodTags>
      struct IsDedicatedOperation<CropOperation<MethodTags> >
        : TrueType
      {};

      template<class T>
      struct IsCropOperation
        : FalseType
      {};

      template<class MethodTags>
      struct IsCropOperation<CropOperation<MethodTags> >
        : TrueType
      {};

      template<class T>
      using IsCropExpression =
        BoolConstant<(Expressions::IsExpression<T>::value && IsCropOperation<Expressions::Operation<T> >::value)>;

      ///////////////////////////////////////////////

      /**Augment the call signature of the methods listed in @a
       * MethodTags by the call-signature indicated by the bit-masks
       * in @a ClosureCallSignatures.
       */
      template<class MethodTags = ModelAdmissibleMethods,
               class ClosureCallSignatures = ModelMethodClosureSignatures>
      struct ClosureOperation
      {
        static_assert(MethodTags::size() == ClosureCallSignatures::size(),
                      "Each methods needs a designated closure signature.");
      };

      template<class MethodTags, class ClosureCallSignatures>
      struct IsDedicatedOperation<ClosureOperation<MethodTags, ClosureCallSignatures> >
        : TrueType
      {};

      template<class T>
      struct IsClosureOperation
        : FalseType
      {};

      template<class MethodTags, class ClosureSignatures>
      struct IsClosureOperation<ClosureOperation<MethodTags, ClosureSignatures> >
        : TrueType
      {};

      template<class T>
      using IsClosureExpression =
        BoolConstant<(Expressions::IsExpression<T>::value && IsClosureOperation<Expressions::Operation<T> >::value)>;

      //////////////////////////////////////////////////////

      struct RitzLoadFlavour
      {};

      struct L2LoadFlavour
      {};

      //!Apply a model to a function
      template<class Flavour>
      struct ApplyOperation
      {};

      template<class Flavour>
      struct IsDedicatedOperation<ApplyOperation<Flavour> >
        : TrueType
      {};

      //! @} ExpressionOperations

      //! @} ExpressionTemplates

    } // NS PDEModel

    template<class T>
    using IsModelOperation = PDEModel::IsDedicatedOperation<T>;

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_MODELS_EXPRESSIONOPERATIONS_HH__
