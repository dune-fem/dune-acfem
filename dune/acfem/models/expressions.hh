#ifndef __DUNE_ACFEM_MODELS_EXPRESSIONS_HH__
#define __DUNE_ACFEM_MODELS_EXPRESSIONS_HH__

#include "../expressions/operandpromotion.hh"
#include "../functions/localfunctiontraits.hh"

#include "expressiontraits.hh"
#include "operationtraits.hh"
#include "unaryexpression.hh"
#include "binaryexpression.hh"
#include "closuretraits.hh"
#include "modules/zeromodel.hh"
#include "modules/bulkloadmodel.hh"
#include "operations/crop.hh"
#include "operations/closure.hh"
#include "operations/apply.hh"
#include "operations/assume.hh"
#include "operandpromotion.hh"

namespace Dune
{

  namespace ACFem
  {

    namespace PDEModel
    {

// Suck in using declarations. This is needed as we defined operator
// functions of the same name in our own namespace.
#include "../expressions/usingpromotion.hh"
#include "../mpl/usingcomparisons.hh"

      using Expressions::finalize;

      /**@addtogroup ExpressionTemplates
       *
       * @{
       */

      /**@addtogroup ModelExpressions
       *
       * All implemented arithmetic operations with models.
       *
       * @note We use the std::enable_if_t SFINAE
       * techiques. Unfortunately, operator functions cannot have
       * default arguments. Hence we have to use std::enable_if_t with
       * the return type which means that we cannot use "auto" for the
       * return type. This provides some boiler-plate code. Don't think
       * that this can be helped easily.
       *
       * @note the basic expressions are duplicated in the ModelFacade
       * class for technical reasons. See the respective comments on
       * "friend" functions there.
       *
       *@{
       */

      /**Provide the canonical zero object.*/
      template<class F, class T, std::enable_if_t<IsPDEModel<T>::value, int> = 0>
      auto zero(F&&, T&&)
      {
        return zeroModel<T, Expressions::Disclosure>();
      }

      template<class T0, class T1, std::enable_if_t<IsPDEModel<T0>::value || IsPDEModel<T1>::value, int> = 0>
      auto zero(OperationTraits<SMultiplyOperation>, T0&&, T1&&)
      {
        if constexpr (IsPDEModel<T0>::value) {
          return zeroModel<T0, Expressions::Disclosure>();
        } else {
          return zeroModel<T1, Expressions::Disclosure>();
        }
      }

      ///////////////////////////////////////////////////////////////////

      /**Unary minus for models.*/
      template<class Model,
               std::enable_if_t<IsProperPDEModel<Model>::value, int> = 0>
      auto operator-(Model&& m)
      {
        return Expressions::finalize<MinusOperation>(std::forward<Model>(m));
      }

      /**Binary plus for models.*/
      template<class Left, class Right,
               std::enable_if_t<(IsProperPDEModel<Left>::value && IsProperPDEModel<Right>::value), int> = 0>
      auto operator+(Left&& left, Right&& right)
      {
        return Expressions::finalize<PlusOperation>(std::forward<Left>(left), std::forward<Right>(right));
      }

      /**Binary minus for models.*/
      template<class Left, class Right,
               std::enable_if_t<(IsProperPDEModel<Left>::value && IsProperPDEModel<Right>::value), int> = 0>
      auto operator-(Left&& left, Right&& right)
      {
        return Expressions::finalize<MinusOperation>(std::forward<Left>(left), std::forward<Right>(right));
      }

      /**@name BulkLoadFunction
       *
       * Add a function to a model in order to implement a load
       * contribution. The operations generate a BulkLoadFunctionModel.
       *
       * Possible optimizations: move constants out of the load-function
       * in front of the resulting model.
       *
       * @{
       */

      /**Add a function as right-hand-side in order to solve A - f = 0.*/
      template<class Left, class Right,
               std::enable_if_t<(IsProperPDEModel<Left>::value && IsWrappableByConstLocalFunction<Right>::value), int> = 0>
      auto operator-(Left&& left, Right&& right)
      {
        return Expressions::finalize<MinusOperation>(
          std::forward<Left>(left),
          asExpression(bulkLoadFunctionModel(std::forward<Right>(right)))
          );
      }

      /**Add a function with the "wrong sign" as right-hand-side. The
       * resulting model will solve A + f = 0.
       */
      template<class Left, class Right,
               std::enable_if_t<(IsProperPDEModel<Left>::value && IsWrappableByConstLocalFunction<Right>::value), int> = 0>
      auto operator+(Left&& left, Right&& right)
      {
        return Expressions::finalize<PlusOperation>(
          std::forward<Left>(left),
          asExpression(bulkLoadFunctionModel(std::forward<Right>(right)))
          );
      }

      /**Add a function as right-hand-side. The resulting model will
       * solve f - A = 0.
       */
      template<class Left, class Right,
               std::enable_if_t<(IsProperPDEModel<Right>::value && IsWrappableByConstLocalFunction<Left>::value), int> = 0>
      auto operator-(Left&& left, Right&& right)
      {
        return Expressions::finalize<MinusOperation>(
          asExpression(bulkLoadFunctionModel(std::forward<Left>(left))),
          std::forward<Right>(right)
          );
      }

      /**Add a function with the "wrong sign" as right-hand-side. The
       * resulting model will solve A + f = 0.
       */
      template<class Left, class Right,
               std::enable_if_t<(IsProperPDEModel<Right>::value && IsWrappableByConstLocalFunction<Left>::value), int> = 0>
      auto operator+(Left&& left, Right&& right)
      {
        return Expressions::finalize<MinusOperation>(
          std::forward<Right>(right),
          asExpression(bulkLoadFunctionModel(std::forward<Left>(left)))
          );
      }

      //!@} BulkLoadFunction

      /**@name Multiplications
       *
       * Multiplication by scalar quantities -- scalar functions and
       * parameters.
       *
       * @{
       */

      //!Multiplication by scalars from the left
      template<class T, class M,
               std::enable_if_t<(IsProperMultiplicationOperand<T>::value
                                 && IsProperPDEModel<M>::value
        ), int> = 0>
      auto operator*(T&& t, M&& m)
      {
        return Expressions::finalize<SMultiplyOperation>(std::forward<T>(t), std::forward<M>(m));
      }

      //!Multiplication by scalars from the right.
      template<class T, class M,
               std::enable_if_t<(IsProperMultiplicationOperand<T>::value
                                 && IsProperPDEModel<M>::value
        ), int> = 0>
      auto operator*(M&& m, T&& t)
      {
        return Expressions::finalize<SMultiplyOperation>(std::forward<T>(t), std::forward<M>(m));
      }

      //!@} Multiplications

      /**@name Divisions
       *
       * Division by scalar quantities, i.e. scalar functions and
       * parameters.
       *
       * @{
       */

      //!Division by something scalar
      template<class M, class T,
               std::enable_if_t<(IsProperMultiplicationOperand<T>::value
                                 && IsProperPDEModel<M>::value
        ), int> = 0>
      decltype(auto) operator/(M&& m, T&& t)
      {
        return Expressions::finalize<SMultiplyOperation>(
          Expressions::operate<ReciprocalOperation>(
            std::forward<T>(t)
            ),
          std::forward<M>(m)
          );
      }

      //!@} Divisions

      //!@} ModelOperations

      //!@} ExpressionTemplates

    } // NS PDEModel

    using PDEModel::EffectiveModelTraits;
    using PDEModel::EffectiveModel;

  } // namespace ACFem

} // namespace Dune

#endif // __DUNE_ACFEM_MODELS_EXPRESSIONS_HH__
