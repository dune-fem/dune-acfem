#ifndef __DUNE_ACFEM_MODELS_MODELBASE_HH__
#define __DUNE_ACFEM_MODELS_MODELBASE_HH__

#include <bitset>
#include <dune/fem/space/common/functionspace.hh>
#include <dune/fem/common/explicitfieldvector.hh>

#include "../expressions/constantoperations.hh"
#include "../expressions/expressionoperations.hh"
#include "../common/quadraturepoint.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup PDE-Models
     *
     * Classes which define some diffusion dominated PDE models. A
     * model in this sense provides "germs" for the integrals which
     * finally form the weak formulation. That is, the methods a model
     * defines form one factor of the bilinear forms, the
     * multiplication by test-functions and their jacobians is then
     * handled by Fem (see EllipticOperator).
     *
     * @{
     */

    /**@addtogroup ModelInterface
     *
     * Interface definition for a model for a non-linear diffusion
     * dominated model in the context of continuous FEM.
     *
     *@{
     */

    /**A structure defining some basic default types and methods. All
     * models "must" inherit from this class.
     */
    template<class DomainFunctionSpace, class RangeFunctionSpace = DomainFunctionSpace>
    struct ModelBase
    {
      /**@name FunctionSpaceTypes
       *
       * Forward some basic type from the supplied function-spaces to
       * the model class. Note that FunctionSpace is defined as a
       * shortcut for RangeFunctionSpace in order to simplify the
       * common case DomainFunctionSpace == RangeFunctionSpace.
       *
       * @{
       */
      template<class FunctionSpace>
      using HessianRangeSelector = typename std::conditional<
        std::is_convertible<typename FunctionSpace::RangeType, typename FunctionSpace::HessianRangeType>::value,
        Fem::ExplicitFieldVector<typename FunctionSpace::HessianRangeType::value_type, FunctionSpace::dimRange>,
        typename FunctionSpace::HessianRangeType>::type;

      using RangeFunctionSpaceType = RangeFunctionSpace; //!
      using DomainFunctionSpaceType = DomainFunctionSpace; //!
      using FunctionSpaceType = RangeFunctionSpaceType; //!

      using DomainType = typename FunctionSpaceType::DomainType; //!
      using RangeType = typename FunctionSpaceType::RangeType; //!
      using JacobianRangeType = typename FunctionSpaceType::JacobianRangeType; //!
      //using HessianRangeType = typename FunctionSpaceType::HessianRangeType; //!
      using HessianRangeType = HessianRangeSelector<FunctionSpaceType>;
      using DomainFieldType = typename FunctionSpaceType::DomainFieldType; //!
      using RangeFieldType = typename FunctionSpaceType::RangeFieldType; //!

      using DomainRangeType = typename DomainFunctionSpaceType::RangeType; //!
      using DomainDomainType = typename DomainFunctionSpaceType::DomainType; //!
      using DomainJacobianRangeType = typename DomainFunctionSpaceType::JacobianRangeType; //!
      //using DomainHessianRangeType = typename DomainFunctionSpaceType::HessianRangeType; //!
      using DomainHessianRangeType = HessianRangeSelector<DomainFunctionSpaceType>;
      using DomainDomainFieldType = typename DomainFunctionSpaceType::DomainFieldType; //!
      using DomainRangeFieldType = typename DomainFunctionSpaceType::RangeFieldType; //!

      using RangeRangeType = typename RangeFunctionSpaceType::RangeType; //!
      using RangeDomainType = typename RangeFunctionSpaceType::DomainType; //!
      using RangeJacobianRangeType = typename RangeFunctionSpaceType::JacobianRangeType; //!
      //using RangeHessianRangeType = typename RangeFunctionSpaceType::HessianRangeType; //!
      using RangeHessianRangeType = HessianRangeSelector<RangeFunctionSpaceType>;
      using RangeDomainFieldType = typename RangeFunctionSpaceType::DomainFieldType; //!
      using RangeRangeFieldType = typename RangeFunctionSpaceType::RangeFieldType; //!

      static constexpr int dimDomain = FunctionSpaceType::dimDomain; //!
      static constexpr int dimRange = FunctionSpaceType::dimRange; //!
      static constexpr int domainDimDomain = DomainFunctionSpaceType::dimDomain; //!@internal
      static constexpr int domainDimRange = DomainFunctionSpaceType::dimRange; //!
      static constexpr int rangeDimDomain = RangeFunctionSpaceType::dimDomain; //!@internal
      static constexpr int rangeDimRange = RangeFunctionSpaceType::dimRange; //!

      static_assert(domainDimDomain == rangeDimDomain,
                    "Function-space need to live on the same domain");

      /*@} FunctionSpaceType */

      /**The type returned by classifyBoundary(). */
      using BoundaryConditionsType = std::pair<bool, std::bitset<dimRange> >;

      //!Print a descriptive name for debugging and output
      std::string name() const
      {
        return "Hi! I am the base class of all models!";
      }

      /**Bind to the given entity.
       *
       * @param[in] entity The entity to bind to.
       *
       * @warning Calling any other method without first binding the model
       * results in undefined behaviour.
       *
       * @warning Models needing this method need to reimplement
       * it. This is just here to obey the "bindable" interface of
       * Dune::Fem.
       */
      template<class Entity>
      void bind(const Entity& entity)
      {}

      /**Unbind from the previously bound entity.
       *
       * @warning Calling this method on an unbound model may cause
       * undefined behaviour.
       *
       * @warning Models needing this method need to reimplement
       * it. This is just here to obey the "bindable" interface of
       * Dune::Fem.
       */
      void unbind()
      {}

      /**Bind to the given intersection and classify the components
       * w.r.t. to the kind of applicable boundary conditions.
       *
       * @warning Note that prior to calling this function the model
       * has to be bound to the inside entity of the given
       * intersection. Failing to do so generates undefined behaviour.
       *
       * @warning The result of calling the other boundary related
       * methods without binding to an intersection is undefined.
       *
       * @warning If RESULT.first is @c false, then the result of
       * calling any of the other boundary related functions is @b
       * undefined. Philosophically, they should return 0 in this
       * case, but in order to have decent performance they give a
       * damn and just don't care.
       *
       * @warning If RESULT.first is @c true, then still you cannot
       * rely on user-friendly behaviour:
       *
       * - only if the respective bit of RESULT.second is set to 1,
       * then the Dirichlet value in this compoment is well-defined.
       *
       * - only if the respective bit of RESULT.second is set to 0,
       * then the Robin value in this component is well defined.
       *
       * @param[in] intersection The intersection to bind to.
       *
       * @return A tuple. First component is a @c bool which is @c
       * true iff any of the boundary related data functions would
       * result in non trivial results. Second component is a bitset
       * of size @c dimRange which is @c true if the given component
       * of the system is subject to Dirichlet boundary conditions and
       * @c false if it is subject to Robin or Neumann boundary
       * conditions. If @c first is @c false then the contents of the
       * bitset is undefined.
       */
      template<class Intersection>
      auto classifyBoundary(const Intersection& intersection)
      {
        return BoundaryConditionsType();
      }
    };

    //!@} Model

    //!@} PDE-Models

  } // namespace ACFem

}  //Namespace Dune

#endif  // __DUNE_ACFEM_MODELS_MODELBASE_HH__
