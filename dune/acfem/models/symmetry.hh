#ifndef __DUNE_ACFEM_MODELS_SYMMETRY_HH__
#define __DUNE_ACFEM_MODELS_SYMMETRY_HH__

#include "../expressions/storage.hh"
#include "modeltraits.hh"

namespace Dune {

  namespace ACFem {

    namespace PDEModel {

      //!Tag structure identifying symmetric models.
      struct SymmetricModel
      {};

      template<class T, std::enable_if_t<!IsPDEModel<T>::value, int> = 0>
      constexpr auto isSymmetric(T&&)
      {
        return TrueType{};
      }

      template<class T, std::enable_if_t<(IsPDEModel<T>::value
                                          && (!IsExpression<T>::value || IsSelfExpression<T>::value)
        ), int> = 0>
      constexpr auto isSymmetric(T&&)
      {
        return BoolConstant<HasTag<T, SymmetricModel>::value || ModelTraits<T>::isLoad>{};
      }

      namespace {

        template<class T, std::size_t... Idx>
        constexpr auto isSymmetricExpander(T&& t, IndexSequence<Idx...>)
        {
          return BoolConstant<(... && decltype(isSymmetric(std::forward<T>(t).template operand<Idx>()))::value)>{};
        }

      }

      template<class T, std::enable_if_t<(IsPDEModel<T>::value
                                          && IsExpression<T>::value
                                          && !IsSelfExpression<T>::value
        ), int> = 0>
      constexpr auto isSymmetric(T&& t)
      {
        if constexpr (HasTag<T, SymmetricModel>::value || ModelTraits<T>::isLoad) {
          return TrueType{};
        } else if constexpr (!IsExpression<T>::value || IsSelfExpression<T>::value) {
          return FalseType{};
        } else {
          return isSymmetricExpander(std::forward<T>(t), MakeIndexSequence<Expressions::Arity<T>::value>{});
        }
      }

    } // PDEModel::

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_MODELS_SYMMETRY_HH__
