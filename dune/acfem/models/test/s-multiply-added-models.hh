#include "../../functions/gridfunction.hh"

#include "test-template.hh"

namespace Dune::ACFem::PDEModel::Testing
{
  template<class Space, class ScalarSpace>
  void sMultiplyAddedModels(const Space& space, const ScalarSpace& scalarSpace)
  {
    auto Ph = discreteFunction(scalarSpace, "Ph");
    auto sdend(Ph.dend());
    for (auto it = Ph.dbegin(); it != sdend; ++it) {
      *it = 1; // simply generate a constant function
    }

    auto DU_DPhi = laplacianModel(scalarSpace, "DU_DPhi");
    std::clog << "Expression: Ph * Ph * (DU_DPhi + ritzLoadModel(DU_DPhi, Ph)" << std::endl << std::endl;
    auto expression = Ph * Ph * (DU_DPhi + ritzLoadModel(DU_DPhi, Ph));
    gridWalkTest(expression, scalarSpace);
  }
}
