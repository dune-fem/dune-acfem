#include <config.h>

#include <dune/common/exceptions.hh>
#include <dune/fem/io/parameter.hh>
#include <dune/fem/gridpart/adaptiveleafgridpart.hh>

#include "../../common/scopedredirect.hh"
#include "../../common/ostream.hh"
#include "../../common/discretefunctionselector.hh"

// All includes for grid-functions.
#include "../../functions/basicfunctions.hh"

// All includes for parameters
//#include "../../parameters/basicparameters.hh"

// All includes for models
#include "../model.hh"

/**@addtogroup PDE-Models
 * @{
 */

/**@addtogroup ModelTests
 * @{
 */

//!Embedded grid-function test DGF-"file"
const std::string dgfData(
  "DGF\n"
  "\n"
  "Vertex\n"
  " 0   0\n"
  " 1   0\n"
  " 1   1\n"
  " 0   1\n"
  " 0.5 0.5\n"
  "#\n"
  "\n"
  "SIMPLEX\n"
  "0 1 4\n"
  "1 2 4\n"
  "2 3 4\n"
  "3 0 4\n"
  "#\n"
  "\n"
  "BoundaryDomain\n"
  "default 1\n"
  "1   1 0   1 1 : blah1  % right boundary\n"
  "2   0 1   1 1 : blah2  % upper boundary\n"
  "3   0 0   0 1 : blah3  % left boundary\n"
  "4   0 0   1 0 : blah4  % lower boundary\n"
  "#\n"
  "\n"
  "GridParameter\n"
  "% longest or arbitrary (see DGF docu)\n"
  "refinementedge longest\n"
  "% there is zarro information on this ... :(\n"
  "overlap 0\n"
  "% whatever this may be ...\n"
  "tolerance 1e-12\n"
  "% silence?\n"
  "verbose 0\n"
  "#\n");

using namespace Dune;
using namespace Dune::ACFem;
using namespace Dune::ACFem::Literals;
using namespace Dune::ACFem::PDEModel;

using Expressions::asExpression;

template<class Model, class GridPart, std::enable_if_t<Expressions::IsClosure<Model>::value, int> = 0>
void instantiateFacade(Model&& model, GridPart& gridPart)
{
  typedef Model ModelType;
  typedef ModelIntrospection::Traits<ModelType> TraitsType;
  typedef typename TraitsType::DomainType DomainType;

  typedef typename TraitsType::DomainRangeType DomainRangeType;
  typedef typename TraitsType::DomainJacobianRangeType DomainJacobianRangeType;
  typedef typename TraitsType::DomainHessianRangeType DomainHessianRangeType;

  for (const auto& element : elements(gridPart)) {

    model.bind(element);

    model.flux(*DomainType(0), DomainRangeType(0), DomainJacobianRangeType(0));
    model.linearizedFlux(DomainRangeType(0), DomainJacobianRangeType(0), *DomainType(0), DomainRangeType(0), DomainJacobianRangeType(0));
    model.fluxDivergence(*DomainType(0), DomainRangeType(0), DomainJacobianRangeType(0), DomainHessianRangeType(0));

    model.source(*DomainType(0), DomainRangeType(0), DomainJacobianRangeType(0));
    model.linearizedSource(DomainRangeType(0), DomainJacobianRangeType(0), *DomainType(0), DomainRangeType(0), DomainJacobianRangeType(0));

    for (const auto& intersection : intersections(gridPart, element)) {

      if (model.classifyBoundary(intersection).first) {

        model.robinFlux(*DomainType(0), DomainType(0), DomainRangeType(0), DomainJacobianRangeType(0));
        model.linearizedRobinFlux(DomainRangeType(0), DomainJacobianRangeType(0), *DomainType(0), DomainType(0), DomainRangeType(0), DomainJacobianRangeType(0));

        model.singularFlux(*DomainType(0), DomainType(0), DomainRangeType(0), DomainJacobianRangeType(0));
        model.linearizedSingularFlux(DomainRangeType(0), DomainJacobianRangeType(0), *DomainType(0), DomainType(0), DomainRangeType(0), DomainJacobianRangeType(0));

        model.dirichlet(*DomainType(0), DomainRangeType(0));
        model.linearizedDirichlet(DomainRangeType(0), *DomainType(0), DomainRangeType(0));
      }
    }

    model.unbind();
  }
}

template<class Model, class GridPart, std::enable_if_t<!Expressions::IsClosure<Model>::value, int> = 0>
void instantiateFacade(Model&& model, GridPart& gridPart)
{
  instantiateFacade(expressionClosure(std::forward<Model>(model)), gridPart);
}

/** Test-template main program. Instantiate a single expression
 * template and evaluate it on a simple grid.
 */
int main(int argc, char *argv[])
  try {
    // General setup
    Fem::MPIManager::initialize(argc, argv);

    // append parameter
    Fem::Parameter::append(argc, argv);

    // append default parameter file
    Fem::Parameter::append(SRCDIR "/parameter");

    // redirect the stream cerr to cout
    // so we only get the output of clog on stderr
    ScopedRedirect redirect(std::cerr, std::cout);

    // reduce precision and use normalized FP output
    std::clog << std::scientific << std::setprecision(4);

    // type of hierarchical grid
    typedef GridSelector::GridType HGridType;

    // the method rank and size from MPIManager are static
    if (Fem::MPIManager::rank() == 0) {
      std::clog << "Loading embedded macro grid: "
                << std::endl
                << "=================================" << std::endl
                << dgfData
                << "=================================" << std::endl
                << std::endl;
    }

    // we try to be self-contained an simply load the embedded simple dgf-file
    std::istringstream dgfStream(dgfData);

    // construct macro using the DGF Parser
    GridPtr<HGridType> gridPtr(dgfStream);
    HGridType& grid = *gridPtr;

    // do initial load balance
    grid.loadBalance();

    auto gridPart = leafGridPart(grid);
    auto space = discreteFunctionSpace<HGridType::dimensionworld>(gridPart, lagrange<POLORDER>);

    typedef
      Fem::FunctionSpace<typename HGridType::ctype,
                         typename HGridType::ctype,
                         HGridType::dimensionworld,
                         HGridType::dimensionworld>
      FunctionSpaceType;
    typedef typename FunctionSpaceType::ScalarFunctionSpaceType ScalarFunctionSpaceType;

    auto zero = GridFunction::zeros<FunctionSpaceType>(gridPart);
    auto scalarZero = GridFunction::zeros<ScalarFunctionSpaceType>(gridPart);
    auto one = GridFunction::one(zero);
    auto X = GridFunction::identityGridFunction(gridPart);
    auto X0 = X[0_c];
    auto X1 = X[1_c];

    // boundary values
//    auto bdryFct = BoundaryIdIndicator(3) * X;
    auto Dbc0    = dirichletZeroModel(space);
    auto Dbc     = dirichletBoundaryModel(X, BoundaryIdIndicator(3));

    // some basic models ...
    auto DU_DPhi = laplacianModel(space, "DU_DPhi");
    auto U_Phi = massModel(space, "U_Phi");
    auto F_Phi = bulkLoadFunctionModel(X);
    //auto Z_Phi = bulkLoadFunctionModel(zero);
    auto nothing = zeroModel(U_Phi);

    ModelFacade<decltype(DU_DPhi)> t = DU_DPhi;
    DU_DPhi = t;

    //std::clog << "sign: " << BoolConstant<(DU_DPhi >= 0_c)>::value << std::endl;

    std::clog << "****************************************" << std::endl;
    std::clog << asExpression(DU_DPhi) << std::endl;
    instantiateFacade(DU_DPhi, gridPart);

    std::clog << "****************************************" << std::endl;
    std::clog << asExpression(-DU_DPhi) << std::endl;
    instantiateFacade(-DU_DPhi, gridPart);

    std::clog << "****************************************" << std::endl;
    std::clog << asExpression(U_Phi) << std::endl;
    instantiateFacade(U_Phi, gridPart);

    std::clog << "****************************************" << std::endl;
    std::clog << asExpression(F_Phi) << std::endl;
    instantiateFacade(F_Phi, gridPart);

    std::clog << "****************************************" << std::endl;
    std::clog << asExpression(Dbc0) << std::endl;
    instantiateFacade(Dbc0, gridPart);

    std::clog << "****************************************" << std::endl;
    std::clog << -DU_DPhi << std::endl;
    std::clog << TypeString<decltype(-DU_DPhi)>{} << std::endl;
    instantiateFacade(-DU_DPhi, gridPart);

    std::clog << "****************************************" << std::endl;
    std::clog << -(-DU_DPhi) << std::endl;
    std::clog << TypeString<decltype(-(-DU_DPhi))>{} << std::endl;
    instantiateFacade(-(-DU_DPhi), gridPart);

    std::clog << "****************************************" << std::endl;
    std::clog << asExpression(DU_DPhi + U_Phi + F_Phi + Dbc) << std::endl;
    instantiateFacade(DU_DPhi + U_Phi + F_Phi + Dbc, gridPart);

    std::clog << "****************************************" << std::endl;
    std::clog << asExpression(DU_DPhi + F_Phi + Dbc) << std::endl;
    instantiateFacade(DU_DPhi + F_Phi + Dbc, gridPart);

    std::clog << "****************************************" << std::endl;
    std::clog << asExpression(DU_DPhi + Dbc) << std::endl;
    instantiateFacade(DU_DPhi + Dbc, gridPart);

    std::clog << "****************************************" << std::endl;
    std::clog << asExpression(3.*(DU_DPhi - X + Dbc)) << std::endl;
    instantiateFacade(3.*(DU_DPhi -X + Dbc), gridPart);

    std::clog << "****************************************" << std::endl;
    std::clog << asExpression(X0*(DU_DPhi - X) + Dbc) << std::endl;
    instantiateFacade(X0*(DU_DPhi - X) + Dbc, gridPart);

    std::clog << "****************************************" << std::endl;
    std::clog << asExpression((DU_DPhi - X)/X0 + Dbc) << std::endl;
    instantiateFacade((DU_DPhi - X)/X0 + Dbc, gridPart);

    std::clog << "****************************************" << std::endl;
    std::clog << "L2 LOAD" << std::endl;
    auto nonLoadModel = (DU_DPhi - X)/X0 + Dbc;
    auto loadedModel = nonLoadModel - loadModel(nonLoadModel, X);
    std::clog << asExpression(loadedModel) << std::endl;
    instantiateFacade(loadedModel, gridPart);

    std::clog << "****************************************" << std::endl;
    std::clog << "RITZ LOAD" << std::endl;
    auto nonRitzLoadModel = (DU_DPhi - X)/X0 + Dbc;
    auto ritzLoadedModel = nonRitzLoadModel - ritzLoadModel(nonLoadModel, X);
    std::clog << asExpression(ritzLoadedModel) << std::endl;
    instantiateFacade(ritzLoadedModel, gridPart);

    std::clog << "****************************************" << std::endl;
    std::clog << "CROP" << std::endl;
    auto unstripped = (DU_DPhi - X)/X0 + Dbc;
    auto stripped = crop(unstripped, IndexSequence<PDEModel::dirichlet, PDEModel::linearizedDirichlet>{});
    std::clog << asExpression(stripped) << std::endl;
    instantiateFacade(stripped, gridPart);

    std::clog << "****************************************" << std::endl;
    std::clog << "DOUBLE CROP" << std::endl;
    auto doubleStripped = crop(stripped, IndexSequence<PDEModel::dirichlet, PDEModel::linearizedDirichlet>{});
    std::clog << asExpression(doubleStripped) << std::endl;
    instantiateFacade(doubleStripped, gridPart);

    std::clog << "****************************************" << std::endl;
    std::clog << "CLOSURE" << std::endl;
    auto nonClosure = asExpression((DU_DPhi - X)/X0 + Dbc);
    auto closureModel = closure(nonClosure, IndexSequence<PDEModel::flux, PDEModel::source>{});
    std::clog << closureModel << std::endl;
    instantiateFacade(closureModel, gridPart);

    std::clog << "****************************************" << std::endl;
    std::clog << "SELECTIVE CLOSURE" << std::endl;
    auto selectiveClosure = closure(nonClosure,
                                    IndexSequence<PDEModel::flux, PDEModel::source>{},
                                    IndexSequence<SequenceMask<PDEModel::pointIndex>::value, ~0UL>{});
    std::clog << asExpression(selectiveClosure) << std::endl;
    instantiateFacade(selectiveClosure, gridPart);

    std::clog << "****************************************" << std::endl;
    std::clog << "DOUBLE CLOSURE" << std::endl;
    auto doubleClosure = closure(selectiveClosure,
                                 IndexSequence<PDEModel::flux, PDEModel::source>{},
                                 IndexSequence<SequenceMask<PDEModel::pointIndex>::value, ~0UL>{});
    std::clog << "same: "
              << std::is_same<decltype(selectiveClosure), decltype(doubleClosure)>::value
              << std::endl;
    std::clog << asExpression(doubleClosure) << std::endl;
    instantiateFacade(doubleClosure, gridPart);

    std::clog << "****************************************" << std::endl;
    std::clog << "STRIPPED CLOSURE" << std::endl;
    auto strippedClosure = closure(stripped,
                                   IndexSequence<PDEModel::flux, PDEModel::source>{},
                                   IndexSequence<SequenceMask<PDEModel::pointIndex>::value, ~0UL>{});
    std::clog << asExpression(strippedClosure) << std::endl;
    instantiateFacade(strippedClosure, gridPart);

    std::clog << "****************************************" << std::endl;
    std::clog << "NITSCHE DIRICHLET" << std::endl;
    auto nonNitsche = DU_DPhi + Dbc; //X0*(DU_DPhi - X) + Dbc;
    //std::clog << asExpression(nonNitsche) << std::endl;
    auto nitscheDirichlet = nitscheDirichletModel(nonNitsche, gridPart);
    std::clog << asExpression(nitscheDirichlet) << std::endl;
    instantiateFacade(nitscheDirichlet, gridPart);

    std::clog << "****************************************" << std::endl;

  } catch(const Exception &exception) {
    std::clog << "Error: " << exception << std::endl;
    return 1;
  }

//!@} ModelTests

//!@} PDE-Models
