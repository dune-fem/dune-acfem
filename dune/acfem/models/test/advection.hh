#include "../../functions/gridfunction.hh"

#include "test-template.hh"

namespace Dune::ACFem::PDEModel::Testing
{
  template<class Space, class ScalarSpace>
  void advection(const Space& space, const ScalarSpace& scalarSpace)
  {
    auto Uh = discreteFunction(space, "Uh");
    auto dend(Uh.dend());
    for (auto it = Uh.dbegin(); it != dend; ++it) {
      *it = 1; // simply generate a constant function
    }

    auto X = gridFunction(space.gridPart(),[](auto&& x) { return std::forward<decltype(x)>(x); });

    // first order bulk models
    auto X_nabla_u = incompressibleTransportModel(space, X, "X_nabla u");
    auto div_X_u = transportModel(space, X, "div_X_u");
    auto Uh_nabla_u = incompressibleTransportModel(space, Uh, "Uh_nabla u");
    auto div_Uh_u = transportModel(space, Uh, "div_Uh_u");
    auto u_nabla_u = incompressibleSelfTransportModel(space, "u_nabla u");
    auto div_uu = fluidSelfTransportModel(space, "div_uu");

    std::clog << "Expression: Uh_nabla_u + X_nabla_u + div_Uh_u + div_X_u + u_nabla_u + div_uu" << std::endl << std::endl;
    auto expression = Uh_nabla_u + X_nabla_u + div_Uh_u + div_X_u + u_nabla_u + div_uu;
    gridWalkTest(expression, space);
  }
}
