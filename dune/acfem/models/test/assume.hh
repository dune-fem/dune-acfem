#include "../../functions/gridfunction.hh"

#include "test-template.hh"

namespace Dune::ACFem::PDEModel::Testing
{
  template<class Space, class ScalarSpace>
  void assume(const Space& space, const ScalarSpace& scalarSpace)
  {
    auto m = -laplacianModel(space);
    auto mPlus = assume<PositiveExpression>(m);

    static_assert(mPlus > 0_c, "assume(Expr > 0) failed");
    static_assert(!(m > 0_c), "!(Expr > 0) failed");
  }
}
