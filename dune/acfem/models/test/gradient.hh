#include "../../functions/gridfunction.hh"

#include "test-template.hh"

namespace Dune::ACFem::PDEModel::Testing
{
  template<class Space, class ScalarSpace>
  void gradient(const Space& space, const ScalarSpace& scalarSpace)
  {
    auto Ph = discreteFunction(scalarSpace, "Ph");
    auto sdend(Ph.dend());
    for (auto it = Ph.dbegin(); it != sdend; ++it) {
      *it = 1; // simply generate a constant function
    }

    auto X0 = gridFunction(space.gridPart(),[](auto&& x) { return std::forward<decltype(x)>(x); })[0_c];

    auto Delta_u = -laplacianModel(space, "-Delta_u");
    auto grad_X0 = gradientLoadModel(X0);
    auto grad_Ph = gradientLoadModel(Ph);

    std::clog << "Expression: -Delta_u + grad_X0 + grad_Ph" << std::endl << std::endl;
    auto expression = -Delta_u + grad_X0 + grad_Ph;
    gridWalkTest(expression, space);
  }
}
