#include "../../functions/gridfunction.hh"

#include "test-template.hh"

namespace Dune::ACFem::PDEModel::Testing
{
  template<class Space, class ScalarSpace>
  void divergence(const Space& space, const ScalarSpace& scalarSpace)
  {
    auto Uh = discreteFunction(space, "Uh");
    auto dend(Uh.dend());
    for (auto it = Uh.dbegin(); it != dend; ++it) {
      *it = 1; // simply generate a constant function
    }

    auto X = gridFunction(space.gridPart(),[](auto&& x) { return std::forward<decltype(x)>(x); });

    auto div_X = divergenceLoadModel(X);
    auto div_Uh = divergenceLoadModel(Uh);
    auto p =  massModel(scalarSpace, "p");

    std::clog << "Expression: p + div_X + div_Uh" << std::endl << std::endl;
    auto expression = p + div_X + div_Uh;
    gridWalkTest(expression, space);
  }
}
