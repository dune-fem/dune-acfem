#include "../../functions/gridfunction.hh"

#include "test-template.hh"

namespace Dune::ACFem::PDEModel::Testing
{
  template<class Space, class ScalarSpace>
  void weakDivergence(const Space& space, const ScalarSpace& scalarSpace)
  {
    auto Uh = discreteFunction(space, "Uh");
    auto dend(Uh.dend());
    for (auto it = Uh.dbegin(); it != dend; ++it) {
      *it = 1; // simply generate a constant function
    }

    auto X = gridFunction(space.gridPart(),[](auto&& x) { return std::forward<decltype(x)>(x); });

    auto p =  massModel(scalarSpace, "p");
    auto weakdiv_X = weakDivergenceModel(X);
    auto weakdiv_Uh = weakDivergenceModel(Uh);

    std::clog << "Expression: p + weakdiv_X + weakdiv_Uh" << std::endl << std::endl;
    auto expression = p + weakdiv_X + weakdiv_Uh;
    gridWalkTest(expression, space);
  }
}
