#include "../../functions/gridfunction.hh"

#include "test-template.hh"

namespace Dune::ACFem::PDEModel::Testing
{
  template<class Space, class ScalarSpace>
  void meanCurvature(const Space& space, const ScalarSpace& scalarSpace)
  {
    auto X = gridFunction(space.gridPart(),[](auto&& x) { return std::forward<decltype(x)>(x); });
    auto X0 = X[0_c];

    auto H = meanCurvatureModel(1_f, X0 /* scalarSpace, only typedefs needed. */);

    std::clog << "Expression: H - X0" << std::endl << std::endl;
    auto expression = H - X0;
    gridWalkTest(expression, space);
  }
}
