#include "../../functions/gridfunction.hh"

#include "test-template.hh"

namespace Dune::ACFem::PDEModel::Testing
{
  template<class Space, class ScalarSpace>
  void pLaplacian(const Space& space, const ScalarSpace& scalarSpace)
  {
    // p-Laplace
    auto Delta_u_p = p_LaplacianModel(1.5, space);
    auto u_p = p_MassModel(1.5, space);

    std::clog << "Expression: -Delta_u_p + u_p" << std::endl << std::endl;
    auto expression = -Delta_u_p + u_p;
    gridWalkTest(expression, space);
  }
}
