#include "../../functions/gridfunction.hh"

#include "test-template.hh"

namespace Dune::ACFem::PDEModel::Testing
{
  template<class Space, class ScalarSpace>
  void factorInScalars(const Space& space, const ScalarSpace& scalarSpace)
  {
    auto DU_DPhi = laplacianModel(scalarSpace, "DU_DPhi");
    auto U_Phi = massModel(scalarSpace, "U_Phi");
    std::clog << "Expression: 2 * (0.5 * DU_DPhi + 1.5 * U_PHI)" << std::endl << std::endl;
    auto expression = 2_f * (1_f / 2_f * DU_DPhi + 1_f / 3_f * U_Phi);
    gridWalkTest(expression, scalarSpace);
  }
}
