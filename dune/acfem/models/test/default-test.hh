#include "../../functions/gridfunction.hh"

#include "test-template.hh"

namespace Dune::ACFem::PDEModel::Testing
{
  template<class Space, class ScalarSpace>
  void defaultTest(const Space& space, const ScalarSpace& scalarSpace)
  {
    // some basic models
    auto u = massModel(space, "u");
    auto Delta_u = -laplacianModel(space, "-Delta_u");

    // some functions
    auto X = gridFunction(space.gridPart(),[](auto&& x) { return std::forward<decltype(x)>(x); });
    auto X0 = X[0_c];

    // inhomogeneous boundary models
    auto Dbc = dirichletBoundaryModel(X, BoundaryIdIndicator(1));
    auto Rbc = robinBoundaryModel(X, BoundaryIdIndicator(2));
    auto Nbc = neumannBoundaryModel(X, BoundaryIdIndicator(3));

    std::clog << "Expression: -Delta_u + u - X - (exp(10.*X*X)*X) + Dbc + 4.0*X0*Rbc + Nbc" << std::endl << std::endl;
    auto expression = -Delta_u + u - X - (exp(10.*X*X)*X) + Dbc + 4.0*X0*Rbc + Nbc;
    gridWalkTest(expression, space);
  }
}
