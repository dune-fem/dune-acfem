#ifndef __DUNE_ACFEM_MODELS_TEST_TEST_TEMPLATE_HH__
#define __DUNE_ACFEM_MODELS_TEST_TEST_TEMPLATE_HH__

#include "../../common/typestring.hh"
#include "../../common/literals.hh"
#include "../../common/quadraturepoint.hh"
#include "../../expressions/interface.hh"

#include "../model.hh"
#include "../modelboundaryindicators.hh"

namespace Dune {
  template<class T, int N>
  bool operator<(const FieldVector<T, N>& a, const FieldVector<T, N>& b)
  {
    for(unsigned int i = 0; i < a.size(); ++i)
    {
      if(std::abs(a[i]-b[i]) > 1e-8)
      {
        return a[i] < b[i];
      }
    }
    return a.two_norm2() < b.two_norm2();
  }
}

namespace Dune::ACFem::PDEModel::Testing
{
  using namespace Literals;
  using ACFem::operator*;

  /**Test routine which is called with the GridFunctionExpression from
   * the main-program. We simply walk over the grid and evaluate components
   * of the model.
   *
   * @bug Currently the flux- and source-methods are not evaluated and
   * hence not instantiated.
   */
  template<class Model, class DiscreteSpace>
  void gridWalkTest(Model&& model, const DiscreteSpace& dfSpace)
  {
    using ModelType = std::decay_t<Model>;

    using RangeType = typename ModelType::RangeType;
    using JacobianRangeType = typename ModelType::JacobianRangeType;
    using HessianRangeType = typename ModelType::HessianRangeType;

    //std::clog << TypeString<Expressions::EnclosedType<Model> >{} << std::endl;
    std::clog << Expressions::asExpression(model) << std::endl;

    const auto& gridPart(dfSpace.gridPart());

    // only check compilation for now
    auto dirichletIndicator = modelDirichletIndicator(model);
    (void)dirichletIndicator;
    auto robinIndicator = modelRobinIndicator(model);
    (void)robinIndicator;
    auto dirichletValues = modelDirichletValues(model, gridPart);
    (void)dirichletValues;
    auto dirichletLocal = constLocalFunction(dirichletValues);

    std::map<typename ModelType::DomainType, typename DiscreteSpace::EntityType> leafElements;
    const auto end = dfSpace.end();
    for (auto it = dfSpace.begin(); it != end; ++it) {
      const auto entity(*it);
      leafElements[entity.geometry().center()] = entity;
    }

    for (auto mapEl : leafElements) {
      const auto& entity = mapEl.second;

      const auto& geometry(entity.geometry());

      // This cannot be handled with "auto" as these are no return types.
      RangeType value(1./2.);
      JacobianRangeType jacobian(1./3.);
      HessianRangeType hessian(1./5.);

      std::clog << "Values:   " << std::endl << value << std::endl;
      std::clog << "Jacobian: " << std::endl << jacobian << std::endl;
      std::clog << "Hessian:  " << std::endl << hessian << std::endl;

      std::clog << "***** Start Entity *****"
                << std::endl
                << std::endl;

      // initialize the model on this entity, if required
      model.bind(entity);

      auto localCenter = geometry.local(geometry.center());

      std::clog << "Local   : " << localCenter << std::endl;
      std::clog << "Global  : " << geometry.center() << std::endl;

      JacobianRangeType flux = model.flux(*localCenter, value, jacobian);

      std::clog << "Non-linear Flux Value   : "
                << std::endl
                << flux
                << std::endl;

      flux = model.linearizedFlux(value, jacobian, *localCenter, value, jacobian);
      std::clog << "Linearized Flux Value   : "
                << std::endl
                << flux
                << std::endl;

      RangeType source = model.source(*localCenter, value, jacobian);
      std::clog << "Non-linear Sources Value: "
                << source
                << std::endl;

      source = model.linearizedSource(value, jacobian, *localCenter, value, jacobian);
      std::clog << "Linearized Sources Value: "
                << source
                << std::endl;

      RangeType divFlux = model.fluxDivergence(*localCenter, value, jacobian, hessian);
      std::clog << "Flux Divergence         : "
                << divFlux
                << std::endl;

      std::clog << std::endl;

      auto iend(gridPart.iend(entity));
      for (auto iit = gridPart.ibegin(entity); iit != iend; ++iit) {
        const auto& intersection(*iit);

        if (intersection.neighbor() || !intersection.boundary()) {
          continue;
        }

        auto active = model.classifyBoundary(intersection);

        std::clog << "Boundary Id: " << intersection.impl().boundaryId() << std::endl
                  << "Active:      " << active.first << std::endl
                  << "Components:  " << active.second << std::endl
                  << std::endl;

        std::clog << "Dirichlet Indicator: " << dirichletIndicator.applies(intersection)
                  << std::endl
                  << std::endl;

        const auto& faceCenter = geometry.local(intersection.geometry().center());

        std::clog << "Local center   : " << faceCenter << std::endl;
        std::clog << "Global center  : " << intersection.geometry().center() << std::endl;

        // subject to Dirichlet if "something" is to do and some bits are 1
        bool dirichletActive = active.first && active.second.any();

        std::clog << "Dirichlet active   : " << dirichletActive << std::endl;

        if (dirichletActive) {
          typename ModelType::RangeType values;
          dirichletLocal.evaluate(*faceCenter, values);
          std::clog << "Dirichlet Function  : " << values << std::endl;
        }

        auto dirichlet = model.dirichlet(*faceCenter, value);
        std::clog << "Dirichlet data     : " << dirichlet << std::endl;
        // NB: if active.first is false then the model is not required
        // to produce well defined results.
        if (!dirichletActive && (dirichlet.two_norm2() != 0.0)) {
          // although not an error this should be visible in the output!
          std::clog << "Dirichlet data non-zero on inactive face. This is not an error!" << std::endl;
        }

        dirichlet = model.linearizedDirichlet(value, *faceCenter, value);
        std::clog << "Lin. Dirichlet data: " << dirichlet << std::endl;
        if (!dirichletActive && active.first && (dirichlet.two_norm2() != 0.0)) {
          // although not an error this should be visible in the output!
          std::clog << "Linearized Dirichlet data non-zero on inactive face. This is not an error!" << std::endl;
        }

        // subject to Robin if "something" is to do and not all components are Dirichlet
        bool robinActive = active.first && !active.second.all();

        std::clog << "Robin active       : " << robinActive << std::endl;

        RangeType robinFlux = model.robinFlux(*faceCenter, intersection.centerUnitOuterNormal(), value, jacobian);
        std::clog << "Robin flux         : " << robinFlux << std::endl;
        if (!robinActive && active.first && (robinFlux.two_norm2() != 0.0)) {
          // although not an error this should be visible in the output!
          std::clog << "Robin flux non-zero on inactive face. This is not an error!" << std::endl;
        }

        robinFlux = model.linearizedRobinFlux(value, jacobian, *faceCenter, intersection.centerUnitOuterNormal(), value, jacobian);
        std::clog << "Lin. Robin flux    : " << robinFlux << std::endl;
        if (!robinActive && active.first && (robinFlux.two_norm2() != 0.0)) {
          // although not an error this should be visible in the output!
          std::clog << "Linearized Robin flux non-zero on inactive face. This is not an error!" << std::endl;
        }

        std::clog << std::endl;
      }

      model.unbind();

      std::clog << "***** End Entity *****"
                << std::endl
                << std::endl;
    }
  }

}

#endif // __DUNE_ACFEM_MODELS_TEST_TEST_TEMPLATE_HH__
