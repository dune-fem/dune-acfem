#include <config.h>

#include <libgen.h>

#include <dune/fem/io/parameter.hh>

#include "../../common/scopedredirect.hh"
#include "../../common/discretefunctionselector.hh"

#if DEFAULT_TEST
# include "default-test.hh"
#endif
#if ASSUME
# include "assume.hh"
#endif
#if ADVECTION
# include "advection.hh"
#endif
#if P_LAPLACIAN
# include "p-laplacian.hh"
#endif
#if MEAN_CURVATURE
# include "mean-curvature.hh"
#endif
#if GRADIENT
# include "gradient.hh"
#endif
#if DIVERGENCE
# include "divergence.hh"
#endif
#if WEAK_DIVERGENCE
# include "weak-divergence.hh"
#endif
#if S_MULTIPLY_ADDED_MODELS
# include "s-multiply-added-models.hh"
#endif
#if FACTOR_IN_SCALARS
# include "factor-in-scalars.hh"
#endif

using namespace Dune;
using namespace ACFem;
using namespace PDEModel;
using namespace Testing;

//!Embedded grid-function test DGF-"file"
const std::string dgfData(R"DGF(DGF

Vertex
 0   0
 1   0
 1   1
 0   1
 0.5 0.5
#

SIMPLEX
0 1 4
1 2 4
2 3 4
3 0 4
#

BoundaryDomain
default 1
1   1 0   1 1 : blah1  % right boundary
2   0 1   1 1 : blah2  % upper boundary
3   0 0   0 1 : blah3  % left boundary
4   0 0   1 0 : blah4  % lower boundary
#

GridParameter
% longest or arbitrary (see DGF docu)
refinementedge longest
% there is zarro information on this ... :(
overlap 0
% whatever this may be ...
tolerance 1e-12
% silence?
verbose 0
#
)DGF");

/** Test-template main program. Instantiate a single expression
 * template and evaluate it on a simple grid.
 */
int main(int argc, char *argv[])
  try {
    // General setup
    Fem::MPIManager::initialize(argc, argv);

    // append parameter
    Fem::Parameter::append(argc, argv);

    // append default parameter file
    Fem::Parameter::append(SRCDIR "/parameter");

    //redirect the stream cerr to cout
    //so we only get the output of clog on stderr
    ScopedRedirect redirect(std::cerr, std::cout);

    // reduce precision and use normalized FP output
    std::clog << std::scientific << std::setprecision(4);

    // type of hierarchical grid
    typedef GridSelector::GridType HGridType;

    // the method rank and size from MPIManager are static
    if (Fem::MPIManager::rank() == 0) {
      std::clog << "Loading embedded macro grid: "
                << std::endl
                << "=================================" << std::endl
                << dgfData
                << "=================================" << std::endl
                << std::endl;
    }

    // we try to be self-contained an simply load the embedded simple dgf-file
    std::istringstream dgfStream(dgfData);

    // construct macro using the DGF Parser
    GridPtr<HGridType> gridPtr(dgfStream);
    HGridType& grid = *gridPtr;

    // do initial load balance
    grid.loadBalance();

    auto gridPart = leafGridPart<InteriorBorder_Partition>(grid);

    auto space = discreteFunctionSpace<HGridType::dimensionworld>(gridPart, lagrange<POLORDER>);
    auto scalarSpace = discreteFunctionSpace(gridPart, lagrange<POLORDER>);

#if SEPARATETESTS
    const std::string testName = Fem::Parameter::getValue<std::string>(
      "testName",
      std::string(basename(argv[0])).substr(std::string("model-").size())
      );
#else
    const std::string testName = Fem::Parameter::getValue<std::string>("testName", "default-test");
#endif

    if (testName == "default-test") {
#if DEFAULT_TEST
      defaultTest(space, scalarSpace);
#endif
    } else if (testName == "assume") {
#if ASSUME
      assume(space, scalarSpace);
#endif
    } else if (testName == "advection") {
#if ADVECTION
      advection(space, scalarSpace);
#endif
    } else if (testName == "p-laplacian") {
#if P_LAPLACIAN
      pLaplacian(space, scalarSpace);
#endif
    } else if (testName == "mean-curvature") {
#if MEAN_CURVATURE
      meanCurvature(space, scalarSpace);
#endif
    } else if (testName == "gradient") {
#if GRADIENT
      gradient(space, scalarSpace);
#endif
    } else if (testName == "divergence") {
#if DIVERGENCE
      divergence(space, scalarSpace);
#endif
    } else if (testName == "weak-divergence") {
#if WEAK_DIVERGENCE
      weakDivergence(space, scalarSpace);
#endif
    } else if (testName == "s-multiply-added-models") {
#if S_MULTIPLY_ADDED_MODELS
      sMultiplyAddedModels(space, scalarSpace);
#endif
    } else if (testName == "factor-in-scalars") {
#if FACTOR_IN_SCALARS
      factorInScalars(space, scalarSpace);
#endif
    } else {
      DUNE_THROW(Dune::NotImplemented, "Test \""+testName+"\" not implemented");
    }
    return EXIT_SUCCESS;
  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
    return EXIT_FAILURE;
  }


//!@} TensorTests

//!@} Tensors

//!@} LinearAlgebra
