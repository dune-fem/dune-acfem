#ifndef __DUNE_ACFEM_MODELS_OPERANDPROMOTION_HH__
#define __DUNE_ACFEM_MODELS_OPERANDPROMOTION_HH__

#include "../common/literals.hh"
#include "../mpl/compare.hh"
#include "../expressions/interface.hh"
#include "../tensors/tensorbase.hh"
#include "../tensors/expressiontraits.hh"

#include "operations/crop.hh"
#include "modeltraits.hh"

namespace Dune
{
  namespace ACFem
  {
    namespace PDEModel
    {
      using namespace Literals;
      using Expressions::IsPromotedTopLevel;

      /**@internal Allow promotion of any tensor-like operands if any
       * of the other operands already is a PDEModel
       */
      template<class... T>
      using PromoteTensorOperands =
        BoolConstant<(// Handled top-level
                      !AnyIs<IsPromotedTopLevel, T...>::value
                      // Something to do?
                      && AnyIs<Tensor::IsNonTensorTensorOperand, T...>::value
                      // One is already a PDE model
                      && AnyIs<IsPDEModel, T...>::value)>;

      /**We promote only operands of binary expressions is the
       * respective other operand is a PDE-model.
       */
      template<std::size_t N, class... T,
               std::enable_if_t<(PromoteTensorOperands<T...>::value
                                 && !NthIs<N, Tensor::IsNonTensorTensorOperand, T...>::value
                 ), int> = 0>
      constexpr decltype(auto) operandPromotion(T&&... t)
      {
        return std::forward<TupleElement<N, std::tuple<T...> > >(
          get<N>(std::forward_as_tuple(t...))
          );
      }

      /**@internal Only instantiate if any of the operands is allowed
       * to and can be promoted and in this case promote anything
       * which can be converted into a tensor into
       * BindableTensorFunction.
       */
      template<std::size_t N, class... T,
               std::enable_if_t<(PromoteTensorOperands<T...>::value
                                 && NthIs<N, Tensor::IsNonTensorTensorOperand, T...>::value
                 ), int> = 0>
      constexpr auto operandPromotion(T&&... t)
      {
        return tensor(std::forward<TupleElement<N, std::tuple<T...> > >(
                        get<N>(std::forward_as_tuple(t...))
                        ));
      }

      /////////////////////////////////////////////////////////////////////////

      //!Promote operands to model crop operation
      template<std::size_t... MethodTag, class T,
               std::enable_if_t<(sizeof(
                                   crop<MethodTag...>(operandPromotion<0>(std::declval<T>()))
                                   ) >= 0
        ), int> = 0>
      constexpr decltype(auto) crop(T&& t, IndexSequence<MethodTag...> = IndexSequence<MethodTag...>{})
      {
        return crop<MethodTag...>(operandPromotion<0>(std::forward<T>(t)));
      }

      /////////////////////////////////////////////////////////////////////////

      //!Promote operands to model closure operation
      template<std::size_t... MethodTag, class T,
               class ClosureCallSignatures = IndexSequence<MethodSignatureClosure<MethodTag>::value...>,
               std::enable_if_t<(sizeof(
                                   closure(operandPromotion<0>(std::declval<T>()), IndexSequence<MethodTag...>{}, ClosureCallSignatures{})
                                   ) >= 0
        ), int> = 0>
      constexpr decltype(auto) closure(T&& t, IndexSequence<MethodTag...> = IndexSequence<MethodTag...>{}, ClosureCallSignatures = ClosureCallSignatures{})
      {
        return closure(operandPromotion<0>(std::forward<T>(t)), IndexSequence<MethodTag...>{}, ClosureCallSignatures{});
      }

      /////////////////////////////////////////////////////////////////////////

      //!Promote operands of "apply" operation
      template<class Flavour, class Model, class Function,
               std::enable_if_t<(IsPDEModel<Model>::value
                                 && IsWrappableByConstLocalFunction<Function>::value
                                 && sizeof(apply(
                                             operandPromotion<0>(std::declval<Model>(), std::declval<Function>()),
                                             operandPromotion<1>(std::declval<Model>(), std::declval<Function>()),
                                             Flavour{}
                                             )) >= 0
        ), int> = 0>
      constexpr decltype(auto) apply(Model&& m, Function&& f, Flavour = Flavour{})
      {
        return apply<Flavour>(
          operandPromotion<0>(std::forward<Model>(m), std::forward<Function>(f)),
          operandPromotion<1>(std::forward<Model>(m), std::forward<Function>(f))
          );
      }

      /////////////////////////////////////////////////////////////////////////

      //!Promote operands of nitscheDirichletModel
      template<class Model,
               class PenaltyFunction,
               class Symmetrize = SkeletonSymmetrizeDefault<Model>,
               std::enable_if_t<(
                 true
                 && IsPDEModel<Model>::value
                 && IsWrappableByConstLocalFunction<PenaltyFunction>::value
                 && IsSign<Symmetrize>::value
                 && sizeof(nitscheDirichletModel(
                             operandPromotion<0>(std::declval<Model>(), std::declval<PenaltyFunction>()),
                             operandPromotion<1>(std::declval<Model>(), std::declval<PenaltyFunction>()),
                             Symmetrize{}
                             )) >= 0
                 ), int> = 0>
      constexpr decltype(auto) nitscheDirichletModel(Model&& m, PenaltyFunction&& p, Symmetrize = Symmetrize{})
      {
        return nitscheDirichletModel(
          operandPromotion<0>(std::forward<Model>(m), std::forward<PenaltyFunction>(p)),
          operandPromotion<1>(std::forward<Model>(m), std::forward<PenaltyFunction>(p)),
          Symmetrize{});
      }

      /////////////////////////////////////////////////////////////////////////

    } // NS PDEModel

  } // NS ACFem

  namespace Fem {

    using ACFem::PDEModel::operandPromotion;

  }

} // NS Dune

#endif //  __DUNE_ACFEM_MODELS_OPERANDPROMOTION_HH__
