#ifndef __DUNE_ACFEM_MODELES_APPLYEXPRESSION_HH__
#define __DUNE_ACFEM_MODELES_APPLYEXPRESSION_HH__

#include <dune/fem/function/localfunction/const.hh>

#include "modeltraits.hh"
#include "binaryexpression.hh"

namespace Dune
{

  namespace ACFem
  {

    namespace PDEModel
    {

      namespace
      {

        /**Parametrized method-generater which implements the method
         * designated by tag.
         *
         * @param[in] Model The respective model.
         *
         * @param[in] Function The function which provides the values
         * the model is applied to.
         *
         * @param[in] tag The "method tag" which designates the method
         * to implement, e.g. ModelIntrospection::flux.
         *
         * @param[in] ArgSeq The index sequence of indices into
         * ModelIntrospection::AllArguments which designates the
         * call-signature of the respective method implemented by the
         * model.
         *
         * The class will implement one of the flux, source, robinFlux,
         * singularFlux, fluxDivergence methods as needed by the
         * model. The LoadModel template will then inherit these classes
         * using parameter-pack expansion of the pack of method tags.
         *
         * The proper method is called via the TaggedModelMethod defined
         * in unarymodelexpression.hh which also takes care of calling
         * the linearized method if the non-linear method is not
         * implemented.
         *
         * Unneccessary function evaluations are avoided automatically,
         * as the get<Idx>(forward_as_tuple()) construct will
         * (hopefully) through away unneeded values. TODO: check this.
         */
        template<class Model,
                 class Function,
                 std::size_t tag,
                 class Flavour = L2LoadFlavour,
                 class ArgSeq = MaskSequence<ModelMethodSignatureClosure<Model, tag>::value>,
                 class FluxSeq = MaskSequence<ModelMethodSignatureClosure<Model, ModelIntrospection::flux>::value>,
                 class DivSeq = MaskSequence<ModelMethodSignatureClosure<Model, ModelIntrospection::fluxDivergence>::value> >
        class LoadModelMethod;

        template<class Model, class Function, std::size_t tag, class Flavour>
        using TaggedLoadModelMethod = LoadModelMethod<
          Model, Function, tag, Flavour,
          MaskSequence<ModelMethodSignatureClosure<Model, tag>::value>,
          MaskSequence<ModelMethodSignatureClosure<Model, ModelIntrospection::flux>::value>,
          MaskSequence<ModelMethodSignatureClosure<Model, ModelIntrospection::fluxDivergence>::value> >;

        /**@copydoc LoadModelMethod*/
        template<class Model, class Function, std::size_t... ArgsIdx>
        class LoadModelMethod<Model, Function, flux, RitzLoadFlavour, IndexSequence<ArgsIdx...> >
        {
         public:
          static constexpr MethodTag tag = ModelIntrospection::flux;

          LoadModelMethod(const Model& model, const Function& f)
            : method_(model), f_(f)
          {}

          /**Flux method implemented by the LoadModel. This is needed
           * for the implementation of load-model for the
           * Ritz-projection.
           *
           * @param[in] x The quadrature point.
           *
           * @return Whatever the Model::flux() generates when it is
           * applied to its proper arguments.
           */
          template<class Quadrature>
          auto flux(const QuadraturePoint<Quadrature>& x) const
          {
            return method_(std::get<ArgsIdx>(std::forward_as_tuple(0, 0, x, 0, f_.evaluate(x), f_.jacobian(x), 0))...);
          }

         private:
          const TaggedModelMethod<Model, tag, SequenceMask<ArgsIdx...>::value> method_;
          const Function& f_;
        };

        /**@copydoc LoadModelMethod*/
        template<class Model, class Function, std::size_t... ArgsIdx>
        class LoadModelMethod<Model, Function, source, RitzLoadFlavour, IndexSequence<ArgsIdx...> >
        {
         public:
          static constexpr MethodTag tag = ModelIntrospection::source;

          LoadModelMethod(const Model& model, const Function& f)
            : method_(model), f_(f)
          {}

          /**Source method implemented by the LoadModel.
           *
           * @param[in] x The quadrature point.
           *
           * @return Whatever the Model::source() generates when it is
           * applied to its proper arguments.
           */
          template<class Quadrature>
          auto source(const QuadraturePoint<Quadrature>& x) const
          {
            return method_(std::get<ArgsIdx>(std::forward_as_tuple(0, 0, x, 0, f_.evaluate(x), f_.jacobian(x), 0))...);
          }

         private:
          const TaggedModelMethod<Model, tag, SequenceMask<ArgsIdx...>::value> method_;
          const Function& f_;
        };

        /**@copydoc LoadModelMethod*/
        template<class Model, class Function,
                 std::size_t... ArgsIdx, std::size_t... FluxArgsIdx, std::size_t... DivArgsIdx>
        class LoadModelMethod<Model, Function, source, L2LoadFlavour,
                              IndexSequence<ArgsIdx...>,
                              IndexSequence<FluxArgsIdx...>,
                              IndexSequence<DivArgsIdx...> >
        {
         public:
          static constexpr MethodTag tag = ModelIntrospection::source;

          LoadModelMethod(const Model& model, const Function& f)
            : method_(model), divMethod_(model), f_(f)
          {}

          /**Source method implemented by the LoadModel.
           *
           * @param[in] x The quadrature point.
           *
           * @return Whatever the Model::source() generates when it is
           * applied to its proper arguments.
           */
          template<class Quadrature>
          auto source(const QuadraturePoint<Quadrature>& x) const
          {
            constexpr std::size_t closureArgMask = (ModelMethodSignatureClosure<Model, tag>::value
                                                    |ModelMethodSignatureClosure<Model, ModelIntrospection::fluxDivergence>::value);
            constexpr bool needValues = hasBit(closureArgMask, valueIndex); (void)needValues;
            constexpr bool needJacobian = hasBit(closureArgMask, jacobianIndex); (void)needJacobian;
            constexpr bool needHessian = hasBit(closureArgMask, hessianIndex); (void)needHessian;

            auto values = needValues ? f_.evaluate(x) : typename Model::RangeType{};
            auto jacobian = needJacobian ? f_.jacobian(x) : typename Model::JacobianRangeType{};
            auto hessian = needHessian ? f_.hessian(x) : typename Model::HessianRangeType{};

            return (method_(std::get<ArgsIdx>(std::forward_as_tuple(0, 0, x, 0, values, jacobian, 0))...)
                    +
                    divMethod_(std::get<DivArgsIdx>(std::forward_as_tuple(0, 0, x, 0, values, jacobian, hessian))...));
          }

         private:
          const TaggedModelMethod<Model, tag, SequenceMask<ArgsIdx...>::value> method_;
          const TaggedModelMethod<Model, ModelIntrospection::fluxDivergence, SequenceMask<DivArgsIdx...>::value> divMethod_;
          const Function& f_;
        };

        /**@copydoc LoadModelMethod*/
        template<class Model, class Function, std::size_t... ArgsIdx>
        class LoadModelMethod<Model, Function, robinFlux, RitzLoadFlavour, IndexSequence<ArgsIdx...> >
        {
         public:
          static constexpr MethodTag tag = ModelIntrospection::robinFlux;
          using DomainDomainType = typename Model::DomainDomainType;

          LoadModelMethod(const Model& model, const Function& f)
            : method_(model), f_(f)
          {}

          /**Source method implemented by the LoadModel.
           *
           * @param[in] x The quadrature point.
           *
           * @return Whatever the Model::source() generates when it is
           * applied to its proper arguments.
           */
          template<class Quadrature>
          auto robinFlux(const QuadraturePoint<Quadrature>& x, const DomainDomainType& unitOuterNormal) const
          {
            return method_(std::get<ArgsIdx>(std::forward_as_tuple(0, 0, x, unitOuterNormal, f_.evaluate(x), 0, 0))...);
          }

         private:
          const TaggedModelMethod<Model, tag, SequenceMask<ArgsIdx...>::value> method_;
          const Function& f_;
        };

        /**@copydoc LoadModelMethod
         *
         * @note This in a sense computes the wrong right-hand-side:
         * quadrature point and normal are discrete quantities. The
         * correct non-discrete load-contribution would first have to
         * project the given point to the non-discrete boundary.
         *
         * This means that an adaptive method using the usual error
         * estimators will produce wrong estimates at the boundary
         * because the geometry error wrongly is not taken into
         * account.
         */
        template<class Model, class Function, std::size_t... ArgsIdx, std::size_t... FluxArgsIdx>
        class LoadModelMethod<Model, Function, robinFlux, L2LoadFlavour,
                              IndexSequence<ArgsIdx...>,
                              IndexSequence<FluxArgsIdx...> >
        {
         public:
          static constexpr MethodTag tag = ModelIntrospection::robinFlux;
          using DomainDomainType = typename Model::DomainDomainType;

          LoadModelMethod(const Model& model, const Function& f)
            : method_(model), fluxMethod_(model), f_(f)
          {}

          /**Source method implemented by the LoadModel.
           *
           * @param[in] x The quadrature point.
           *
           * @return Whatever the Model::source() generates when it is
           * applied to its proper arguments.
           */
          template<class Quadrature>
          auto robinFlux(const QuadraturePoint<Quadrature>& x, const DomainDomainType& unitOuterNormal) const
          {
            constexpr std::size_t closureArgMask = (ModelMethodSignatureClosure<Model, tag>::value
                                                    |ModelMethodSignatureClosure<Model, ModelIntrospection::flux>::value);
            constexpr bool needValues = hasBit(closureArgMask, valueIndex); (void)needValues;
            constexpr bool needJacobian = hasBit(closureArgMask, jacobianIndex); (void)needJacobian;

            auto values = needValues ? f_.evaluate(x) : typename Model::RangeType{};
            auto jacobian = needJacobian ? f_.jacobian(x) : typename Model::JacobianRangeType{};

            auto result = method_(std::get<ArgsIdx>(std::forward_as_tuple(0, 0, x, unitOuterNormal, values, jacobian, 0))...);
            auto flux = fluxMethod_(std::get<FluxArgsIdx>(std::forward_as_tuple(0, 0, x, 0, values, jacobian, 0))...);
            //flux.umv(unitOuterNormal, result);
            //return result;
            return result += contractInner(flux, unitOuterNormal);
          }

         private:
          const TaggedModelMethod<Model, tag, SequenceMask<ArgsIdx...>::value> method_;
          const TaggedModelMethod<Model, ModelIntrospection::flux, SequenceMask<FluxArgsIdx...>::value> fluxMethod_;
          const Function& f_;
        };

        /**@copydoc LoadModelMethod*/
        template<class Model, class Function, class Flavour, std::size_t... ArgsIdx>
        class LoadModelMethod<Model, Function, singularFlux, Flavour, IndexSequence<ArgsIdx...> >
        {
         public:
          static constexpr MethodTag tag = ModelIntrospection::singularFlux;
          using DomainDomainType = typename Model::DomainDomainType;

          LoadModelMethod(const Model& model, const Function& f)
            : method_(model), f_(f)
          {}

          /**Source method implemented by the LoadModel.
           *
           * @param[in] x The quadrature point.
           *
           * @return Whatever the Model::source() generates when it is
           * applied to its proper arguments.
           */
          template<class Quadrature>
          auto singularFlux(const QuadraturePoint<Quadrature>& x, const DomainDomainType& unitOuterNormal) const
          {
            return method_(std::get<ArgsIdx>(std::forward_as_tuple(0, 0, x, unitOuterNormal, f_.evaluate(x), f_.jacobian(x), 0))...);
          }

         private:
          const TaggedModelMethod<Model, tag, SequenceMask<ArgsIdx...>::value> method_;
          const Function& f_;
        };

        /**@copydoc LoadModelMethod*/
        template<class Model, class Function, class Flavour, std::size_t... ArgsIdx>
        class LoadModelMethod<Model, Function, dirichlet, Flavour, IndexSequence<ArgsIdx...> >
        {
         public:
          static constexpr MethodTag tag = ModelIntrospection::dirichlet;

          LoadModelMethod(const Model& model, const Function& f)
            : method_(model), f_(f)
          {}

          /**Source method implemented by the LoadModel.
           *
           * @param[in] x The quadrature point.
           *
           * @return Whatever the Model::source() generates when it is
           * applied to its proper arguments.
           */
          template<class Quadrature>
          auto dirichlet(const QuadraturePoint<Quadrature>& x) const
          {
            return method_(std::get<ArgsIdx>(std::forward_as_tuple(0, 0, x, 0, f_.evaluate(x), 0, 0))...);
          }

         private:
          const TaggedModelMethod<Model, tag, SequenceMask<ArgsIdx...>::value> method_;
          const Function& f_;
        };

        /**@copydoc LoadModelMethod*/
        template<class Model, class Function, std::size_t... ArgsIdx>
        class LoadModelMethod<Model, Function, fluxDivergence, RitzLoadFlavour, IndexSequence<ArgsIdx...> >
        {
         public:
          static constexpr MethodTag tag = ModelIntrospection::fluxDivergence;

          LoadModelMethod(const Model& model, const Function& f)
            : method_(model), f_(f)
          {}

          /**Source method implemented by the LoadModel.
           *
           * @param[in] x The quadrature point.
           *
           * @return Whatever the Model::source() generates when it is
           * applied to its proper arguments.
           */
          template<class Quadrature>
          auto fluxDivergence(const QuadraturePoint<Quadrature>& x) const
          {
            return method_(std::get<ArgsIdx>(std::forward_as_tuple(0, 0, x, 0, f_.evaluate(x), f_.jacobian(x), f_.hessian(x)))...);
          }

         private:
          const TaggedModelMethod<Model, tag, SequenceMask<ArgsIdx...>::value> method_;
          const Function& f_;
        };

      }

      /**Just provide all methods implemented by the model. */
      template<class Model, class Function>
      struct BinaryModelMethods<ApplyOperation<RitzLoadFlavour>, Model, Function>
      {
        using Type = MaskSequence<
          NonLinearMethodsClosure<ModelTraits<Model>::methodsMask>::value>;
      };

      /**Call the fluxDivergence inside the source() method and do
       * not provide a flux() or fluxDivergence() method.*/
      template<class Model, class Function>
      struct BinaryModelMethods<ApplyOperation<L2LoadFlavour>, Model, Function>
      {
        using Traits = ModelTraits<Model>;
        using Type = MaskSequence<
          NonLinearMethodsClosure<(Traits::methodsMask
                                   |
                                   (Traits::template HasMethod<fluxDivergence>::value ? SequenceMask<source>::value : 0)
                                   |
                                   (Traits::template Exists<flux>::value ? SequenceMask<robinFlux>::value : 0)
          )>::value
          &
          ~SequenceMask<flux, fluxDivergence>::value>;
      };

      //!
      template<class Flavour, class Model, class GridFunction, std::size_t... tag>
      class BinaryModelExpression<ApplyOperation<Flavour>, Model, GridFunction, IndexSequence<tag...> >
        : public ModelBase<typename std::decay_t<Model>::DomainFunctionSpaceType,
                           typename std::decay_t<Model>::RangeFunctionSpaceType>
        , public Expressions::Storage<OperationTraits<ApplyOperation<Flavour> >, Model, GridFunction>
        , public TaggedLoadModelMethod<std::decay_t<Model>, LocalFunctionStorage<GridFunction>, tag, Flavour>...
      {
        using ThisType = BinaryModelExpression;
        using ModelDecay = std::decay_t<Model>;
        using BaseType = ModelBase<typename ModelDecay::DomainFunctionSpaceType, typename ModelDecay::RangeFunctionSpaceType>;
        using StorageType = Expressions::Storage<OperationTraits<ApplyOperation<Flavour> >, Model, GridFunction>;
        using TraitsType = ModelTraits<Model>;
        using FunctionDecay = std::decay_t<GridFunction>;
        using LocalFunctionType = LocalFunctionStorage<GridFunction>;
       public:
        using StorageType::operand;
        using StorageType::operation;
        using typename StorageType::OperationType;
        using typename StorageType::FunctorType;
        using LeftType = Model;
        using RightType = GridFunction;

        template<class ModelArg, class FunctionArg, std::enable_if_t<std::is_constructible<StorageType, FunctorType, ModelArg, FunctionArg>::value, int> = 0>
        BinaryModelExpression(ModelArg&& model, FunctionArg&& f)
          : StorageType(std::forward<ModelArg>(model), std::forward<FunctionArg>(f))
          , TaggedLoadModelMethod<ModelDecay, LocalFunctionType, tag, Flavour>(left(), localFunction_)...
          , localFunction_(right())
        {}

        BinaryModelExpression(const ThisType& other)
          : StorageType(other)
          , TaggedLoadModelMethod<ModelDecay, LocalFunctionType, tag, Flavour>(left(), localFunction_)...
          , localFunction_(right())
        {}

        BinaryModelExpression(ThisType&& other)
          : StorageType(std::move(other))
          , TaggedLoadModelMethod<ModelDecay, LocalFunctionType, tag, Flavour>(left(), localFunction_)...
          , localFunction_(right())
        {}

        std::string name() const
        {
          return "<(" + left().name() + ") << (" + right().name() + "), . >";
        }

        template<class Entity>
        void bind(const Entity& entity)
        {
          localFunction_.bind(entity);
          left().bind(entity);
        }

        void unbind()
        {
          left().unbind();
          localFunction_.unbind();
        }

        template<class Intersection>
        auto classifyBoundary(const Intersection& intersection)
        {
          if (std::is_same<Flavour, RitzLoadFlavour>::value) {
            if (!TraitsType::template Exists<robinFlux>::value
                && !TraitsType::template Exists<singularFlux>::value
                && !TraitsType::template Exists<dirichlet>::value) {
              return typename BaseType::BoundaryConditionsType{};
            }
            return left().classifyBoundary(intersection);
          }
          // now L2LoadFlavour
          if (!TraitsType::template Exists<robinFlux>::value
              && !TraitsType::template Exists<singularFlux>::value
              && !TraitsType::template Exists<dirichlet>::value
              && !TraitsType::template Exists<flux>::value) {
            return typename BaseType::BoundaryConditionsType{};
          }
          auto result = left().classifyBoundary(intersection);
          if (!result.first) {
            result.first = TraitsType::template Exists<flux>::value;
            result.second = 0;
          }
          return result;
        }

       protected:
        using StorageType::t0_;
        using StorageType::t1_;
        constexpr auto&& left() && { return t0_; }
        constexpr auto& left() & { return t0_; }
        constexpr const auto& left() const& { return t0_; }
        constexpr auto&& right() && { return t1_; }
        constexpr auto& right() & { return t1_; }
        constexpr const auto& right() const& { return t1_; }

        LocalFunctionType localFunction_;
      };

    } // PDEModel

    /**This will become a "load"-model which applies "Model" to
     * "Function" and generates the necessary load-vectors
     * s.t. Function will be approximated by the respective
     * FEM-discretization.
     *
     * There will be two flavours:
     *
     * - L2-style load-vector model for rapid proto-typing which
     *   adds the fluxDivergence() to the source() contribution closes
     *   the model with the appropriate boundary values.
     *
     * - Ritz-projection model which simply applies the model to the
     *   function as is. This can be used to compute a Ritz-projection
     *   in order to obtain better initial values for time-dependent
     *   problems.
     *
     * @note You have to substract the load-model from a given model
     * in order to form the "equation" which leads to a
     * FEM-approximation of the given function. The LoadModel simply
     * applies the model to the given function, but does @b not "move
     * it to the right hand side".
     */
    template<class Model, class GridFunction>
    using LoadModel = PDEModel::BinaryModelExpression<PDEModel::ApplyOperation<PDEModel::L2LoadFlavour>, Model, GridFunction>;

    /**copydoc LoadModel*/
    template<class Model, class GridFunction>
    using RitzLoadModel = PDEModel::BinaryModelExpression<PDEModel::ApplyOperation<PDEModel::RitzLoadFlavour>, Model, GridFunction>;

  } // namespace ACFem

} // namespace Dune


#endif //  __DUNE_ACFEM_MODELES_APPLYEXPRESSION_HH__
