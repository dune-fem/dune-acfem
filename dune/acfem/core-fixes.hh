#ifndef __DUNE_ACFEM_COMMON_CORE_FIXES_HH__
#define __DUNE_ACFEM_COMMON_CORE_FIXES_HH__

// This file is intentially empty but can be used to counter balance
// bugs in the core-modules. It is include by config.h in an early
// stage of the compilation.

#endif // __DUNE_ACFEM_COMMON_CORE_FIXES_HH__
