#ifndef __DUNE_ACFEM_MPL_TYPETUPLESORT_HH__
#define __DUNE_ACFEM_MPL_TYPETUPLESORT_HH__

#include "typetuple.hh"

namespace Dune::ACFem::MPL {

  using ::Dune::ACFem::TypePackElement;

  // merge two sorted tuples

  namespace {

    template<class Output, class In0, class In1, template<CXX17_P0522R0(class A, class B)> class Swap, class SFINAE = void>
    struct MergeTypeTupleHelper;

    template<class... Out, template<CXX17_P0522R0(class A, class B)> class Swap>
    struct MergeTypeTupleHelper<TypeTuple<Out...>, TypeTuple<>, TypeTuple<>, Swap>
    {
      using Type = TypeTuple<Out...>;
    };

    template<class... Out, class... In0, template<CXX17_P0522R0(class A, class B)> class Swap>
    struct MergeTypeTupleHelper<TypeTuple<Out...>, TypeTuple<In0...>, TypeTuple<>, Swap>
    {
      using Type = TypeTuple<Out..., In0...>;
    };

    template<class... Out, class... In1, template<CXX17_P0522R0(class A, class B)> class Swap>
    struct MergeTypeTupleHelper<TypeTuple<Out...>, TypeTuple<>, TypeTuple<In1...>, Swap>
    {
      using Type = TypeTuple<Out..., In1...>;
    };

    template<class... Out, class In00, class... In0Rest, class In10, class... In1Rest, template<CXX17_P0522R0(class A, class B)> class Swap>
    struct MergeTypeTupleHelper<TypeTuple<Out...>, TypeTuple<In00, In0Rest...>, TypeTuple<In10, In1Rest...>, Swap,
                                std::enable_if_t<Swap<In00, In10>::value > >
    {
      using Type = typename MergeTypeTupleHelper<TypeTuple<Out..., In10>, TypeTuple<In00, In0Rest...>, TypeTuple<In1Rest...>, Swap>::Type;
    };

    template<class... Out, class In00, class... In0Rest, class In10, class... In1Rest, template<CXX17_P0522R0(class A, class B)> class Swap>
    struct MergeTypeTupleHelper<TypeTuple<Out...>, TypeTuple<In00, In0Rest...>, TypeTuple<In10, In1Rest...>, Swap,
                                std::enable_if_t<!Swap<In00, In10>::value > >
    {
      using Type = typename MergeTypeTupleHelper<TypeTuple<Out..., In00>, TypeTuple<In0Rest...>, TypeTuple<In10, In1Rest...>, Swap>::Type;
    };

  }

  /**Merge two sorted type-tuples into one sorted type-tuple.*/
  template<class In0, class In1, template<CXX17_P0522R0(class A, class B)> class Swap>
  using TypeTupleMerge = typename MergeTypeTupleHelper<TypeTuple<>, In0, In1, Swap>::Type;

  /**Merge one element into an already sorted type-tuple.*/
  template<class One, class Many, template<CXX17_P0522R0(class A, class B)> class Swap>
  using TypeTupleMergeOneLeft = typename MergeTypeTupleHelper<TypeTuple<>, TypeTuple<One>, Many, Swap>::Type;

  /**Merge one element into an already sorted type-tuple.*/
  template<class Many, class One, template<CXX17_P0522R0(class A, class B)> class Swap>
  using TypeTupleMergeOneRight = typename MergeTypeTupleHelper<TypeTuple<>, Many, TypeTuple<One>, Swap>::Type;

  namespace {

    /**@internal Split a type tuple into sorted type-tuples.*/
    template<class Input, template<CXX17_P0522R0(class A, class B)> class Swap,
             class Done = TypeTuple<>, class Cur = TypeTuple<>, class CurLast = TypeTuple<>,
             class SFINAE = void>
    struct SplitTypeTuple;

    //!@internal recursion end-point
    template<class... Done, class... Cur, class CurLast, template<CXX17_P0522R0(class A, class B)> class Swap>
    struct SplitTypeTuple<TypeTuple<>, Swap, TypeTuple<Done...>, TypeTuple<Cur...>, TypeTuple<CurLast> >
    {
      using Type = TypeTuple<Done..., TypeTuple<Cur..., CurLast> >;
    };

    template<class... Done, class... Cur, class CurLast, class In0, class... InRest, template<CXX17_P0522R0(class A, class B)> class Swap>
    struct SplitTypeTuple<TypeTuple<In0, InRest...>, Swap,
                          TypeTuple<Done...>, TypeTuple<Cur...>, TypeTuple<CurLast>,
                          std::enable_if_t<!Swap<CurLast, In0>::value> >
    {
      using Type = typename SplitTypeTuple<TypeTuple<InRest...>, Swap, TypeTuple<Done...>, TypeTuple<Cur..., CurLast>, TypeTuple<In0> >::Type;
    };

    template<class... Done, class... Cur, class CurLast, class In0, class... InRest, template<CXX17_P0522R0(class A, class B)> class Swap>
    struct SplitTypeTuple<TypeTuple<In0, InRest...>, Swap,
                          TypeTuple<Done...>, TypeTuple<Cur...>, TypeTuple<CurLast>,
                          std::enable_if_t<Swap<CurLast, In0>::value> >
    {
      using Type = typename SplitTypeTuple<TypeTuple<InRest...>, Swap, TypeTuple<Done..., TypeTuple<Cur..., CurLast> >, TypeTuple<>, TypeTuple<In0> >::Type;
    };

    /**@internal Start splitting the input tuple into sorted sub-tuples.*/
    template<class In0, class... InRest, template<CXX17_P0522R0(class A, class B)> class Swap>
    struct SplitTypeTuple<TypeTuple<In0, InRest...>, Swap, TypeTuple<>, TypeTuple<>, TypeTuple<> >
    {
      using Type = typename SplitTypeTuple<TypeTuple<InRest...>, Swap, TypeTuple<>, TypeTuple<>, TypeTuple<In0> >::Type;
    };

    /**@internal Special case for sorting an empty tuple.*/
    template<template<CXX17_P0522R0(class A, class B)> class Swap>
    struct SplitTypeTuple<TypeTuple<>, Swap, TypeTuple<>, TypeTuple<>, TypeTuple<> >
    {
      using Type = TypeTuple<TypeTuple<> >;
    };

    template<class Output, class Input, template<CXX17_P0522R0(class A, class B)> class Swap>
    struct MergeRunLists;

    //Empty input case.
    template<template<CXX17_P0522R0(class A, class B)> class Swap>
    struct MergeRunLists<TypeTuple<>, TypeTuple<>, Swap>
    {
      using Type = TypeTuple<>;
    };

    //Recursion end-point: the only remaining sorted input list is the result.
    template<class Input,  template<CXX17_P0522R0(class A, class B)> class Swap>
    struct MergeRunLists<TypeTuple<>, TypeTuple<Input>, Swap>
    {
      using Type = Input;
    };

    //Merge every two input tuples
    template<class... Output, class In0, class In1, class... InRest,  template<CXX17_P0522R0(class A, class B)> class Swap>
    struct MergeRunLists<TypeTuple<Output...>, TypeTuple<In0, In1, InRest...>, Swap>
    {
      using Type = typename MergeRunLists<TypeTuple<Output..., TypeTupleMerge<In0, In1, Swap> >, TypeTuple<InRest...>, Swap>::Type;
    };

    //If only one remains, then just add it and recurse to the next merge round
    template<class... Output, class InLast,  template<CXX17_P0522R0(class A, class B)> class Swap>
    struct MergeRunLists<TypeTuple<Output...>, TypeTuple<InLast>, Swap>
    {
      using Type = typename MergeRunLists<TypeTuple<>, TypeTuple<Output..., InLast>, Swap>::Type;
    };

    //Up one level: run again over the list of sorted tuples.
    template<class Out0, class... OutRest,  template<CXX17_P0522R0(class A, class B)> class Swap>
    struct MergeRunLists<TypeTuple<Out0, OutRest...>, TypeTuple<>, Swap>
    {
      using Type = typename MergeRunLists<TypeTuple<>, TypeTuple<Out0, OutRest...>, Swap>::Type;
    };

  }

  template<class Tuple, template<CXX17_P0522R0(class A, class B)> class Swap>
  using MergeSort = typename MergeRunLists<TypeTuple<>, typename SplitTypeTuple<Tuple, Swap>::Type, Swap>::Type;

  template<class TupleTuple, template<CXX17_P0522R0(class A, class B)> class Swap>
  using MergeSorted = typename MergeRunLists<TypeTuple<>, TupleTuple, Swap>::Type;

  template<class Tuple, template<CXX17_P0522R0(class A, class B)> class Swap>
  constexpr inline bool IsSortedV = (size<typename SplitTypeTuple<Tuple, Swap>::Type>() == 1);

}

#endif // __DUNE_ACFEM_MPL_TYPETUPLESORT_HH__
