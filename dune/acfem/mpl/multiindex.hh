#ifndef __DUNE_ACFEM_MPL_MULTIINDEX_HH__
#define __DUNE_ACFEM_MPL_MULTIINDEX_HH__

#include "../common/types.hh"

#include "accumulate.hh"
#include "compare.hh"
#include "foreach.hh"
#include "generators.hh"
#include "sequenceslice.hh"

namespace Dune
{

  namespace ACFem
  {

    namespace {
      template<std::size_t I>
      constexpr auto multiIndexHelper(IndexSequence<>)
      {
        return IndexSequence<>{};
      }

      template<std::size_t I, std::size_t FrontDim,  std::size_t... RestDims>
      constexpr auto multiIndexHelper(IndexSequence<FrontDim, RestDims...>)
      {
        return PushBack<I % FrontDim, decltype(multiIndexHelper<I / FrontDim>(IndexSequence<RestDims...>{}))>{};
      }

      template<std::size_t I, class DimSeq>
      struct MultiIndexHelper;

      template<std::size_t I>
      struct MultiIndexHelper<I, IndexSequence<> >
      {
        using Type = IndexSequence<>;
        static constexpr std::size_t index = I;
      };

      template<std::size_t I, std::size_t FrontDim, std::size_t... RestDims>
      struct MultiIndexHelper<I, IndexSequence<FrontDim, RestDims...> >
      {
        using ChildType = MultiIndexHelper<I, IndexSequence<RestDims...> >;
        using Type = PushFront<ChildType::index % FrontDim, typename ChildType::Type>;
        static constexpr std::size_t index = ChildType::index / FrontDim;
      };

    } // NS anonymous

    /**Generate the multi-index corresponding to the flattened index I
     * where the multiindex varies between the 0 and the given
     * dimensions. The enumeration is such that the last index is
     * incremented first as if the generated index would correspond to
     * a digit representation of some number. E.g. for Dims... = 2,2,2:
     *
     * <0,0,0> <0,0,1> <0,1,0> <0,1,1> etc.
     */
    template<std::size_t I, std::size_t... Dims>
    constexpr auto multiIndex(IndexSequence<Dims...>, IndexConstant<I> = IndexConstant<I>{})
    {
      return multiIndexHelper<I>(ReverseSequence<IndexSequence<Dims...> >{});
      //return typename MultiIndexHelper<I, IndexSequence<Dims...> >::Type{};
    }

    /**@copydoc multiIndex*/
    template<std::size_t I, class DimSeq>
    using MultiIndex = std::decay_t<decltype(multiIndex<I>(DimSeq{}))>;
    //using MultiIndex = typename MultiIndexHelper<I, DimSeq>::Type;

    /**@copydoc multiIndex. Runtime-dynamic version.*/
    template<std::size_t... Dims>
    constexpr auto multiIndex(std::size_t index, IndexSequence<Dims...>)
    {
      using Signature = IndexSequence<Dims...>;
      constexpr std::size_t numDims = sizeof...(Dims);
      std::array<std::size_t, numDims> result({{}});
      forEach(ReverseSequence<MakeIndexSequence<numDims> >{}, [&](auto i) {
          using I = decltype(i);
          result[I::value] = index % Get<I::value, Signature>::value;
          index /= Get<I::value, Signature>::value;
        });
      return result;
    }

    /**************************************************************************
     *
     * flattenMultiIndex() for std::array
     *
     */

    namespace {

      template<std::size_t N>
      constexpr std::size_t flattenMultiIndexHelper(IndexSequence<>, const std::array<std::size_t, N>&)
      {
        return 0;
      }

      template<std::size_t D0, std::size_t... DRest, std::size_t N,
               std::enable_if_t<(sizeof...(DRest) < N), int> = 0>
      constexpr std::size_t flattenMultiIndexHelper(IndexSequence<D0, DRest...>, const std::array<std::size_t, N>& indices)
      {
        constexpr std::size_t pos = sizeof...(DRest);
        return indices[pos] + D0 * flattenMultiIndexHelper(IndexSequence<DRest...>{}, indices);
      }
    }

    /**Flatten run-time variable std::array.*/
    template<class Dims, std::size_t N, std::enable_if_t<Dims::size() == N, int> = 0>
    constexpr std::size_t flattenMultiIndex(Dims, const std::array<std::size_t, N>& indices)
    {
      return flattenMultiIndexHelper(ReverseSequence<Dims>{}, indices);
    }

    /**************************************************************************
     *
     * flattenMultiIndex() for integral packs (runtime variable)
     *
     */
    template<class Dims, class... I,
             std::enable_if_t<IsIntegralPack<I...>::value, int> = 0>
    constexpr std::size_t flattenMultiIndex(Dims, I... i)
    {
      return flattenMultiIndex(Dims{}, std::array<std::size_t, sizeof...(I)>({{(std::size_t)i...}}));
    }

    /**************************************************************************
     *
     * flattenMultiIndex() for integer_sequence
     *
     */

    namespace {

      constexpr auto flattenMultiIndexHelper(IndexSequence<>, IndexSequence<>)
      {
        return 0;
      }

      template<std::size_t I0, std::size_t... Indices, std::size_t D0, std::size_t... Dims>
      constexpr auto flattenMultiIndexHelper(IndexSequence<D0, Dims...>, IndexSequence<I0, Indices...> = IndexSequence<I0, Indices...>{})
      {
        static_assert(sizeof...(Indices) == sizeof...(Dims), "Number of indices in multiindex must equal to number of dimensions.");
        return I0 + D0 * flattenMultiIndexHelper(IndexSequence<Dims...>{}, IndexSequence<Indices...>{});
      }
    }

    /**Compile-time constant flatten routine.*/
    template<std::size_t... Indices, std::size_t... Dims>
    constexpr auto flattenMultiIndex(IndexSequence<Dims...>, IndexSequence<Indices...> = IndexSequence<Indices...>{})
    {
      return flattenMultiIndexHelper(ReverseSequence<IndexSequence<Dims...> >{}, ReverseSequence<IndexSequence<Indices...> >{});
    }

    /**Compute the index of the given multi-index in the tuple of all indices.*/
    template<class Indices, class Dims>
    using FlattenMultiIndex = IndexConstant<flattenMultiIndex(Dims{}, Indices{})>;

    /**************************************************************************
     *
     * total number of DoF of a given multi-index signature.
     *
     */

    /**Compute the "dimension" corresponding to the given signature,
     * i.e. to total number of the multi-indices for the given
     * dimensions.
     */
    template<std::size_t... IndexPositions, std::size_t... Dimensions>
    static std::size_t constexpr multiDim(IndexSequence<Dimensions...>, IndexSequence<IndexPositions...> = IndexSequence<IndexPositions...>{})
    {
      static_assert(sizeof...(IndexPositions) <= sizeof...(Dimensions), "Too many index positions.");
      using Signature = IndexSequence<Dimensions...>;
      using SubSignature = ConditionalType<sizeof...(IndexPositions) == 0, Signature, SubSequence<Signature, IndexPositions...> >;
      return Prod<SubSignature>::value;
    }

    template<class IndexPositions, class Dimensions>
    using MultiDim = IndexConstant<multiDim(Dimensions{}, IndexPositions{})>;

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_MPL_MULTIINDEX_HH__
