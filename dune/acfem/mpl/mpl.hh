#ifndef __DUNE_ACFEM_MPL_MPL_HH__
#define __DUNE_ACFEM_MPL_MPL_HH__

#include "access.hh"
#include "accumulate.hh"
#include "assign.hh"
#include "conditionaljoin.hh"
#include "conditional.hh"
#include "filter.hh"
#include "generators.hh"
#include "insertat.hh"
#include "mask.hh"
#include "permutation.hh"
#include "sequenceslice.hh"
#include "submask.hh"
#include "subtuple.hh"
#include "toarray.hh"
#include "tostring.hh"
#include "transform.hh"
#include "transformtuple.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup MetaProgrammingLibrary
     *
     * @{
     */

    // Kept here or documentation purposes.

    //!@} MetaProgrammingLibrary

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_MPL_MPL_HH__
