#ifndef __DUNE_ACFEM_MPL_TYPEPACKELEMENT_HH__
#define __DUNE_ACFEM_MPL_TYPEPACKELEMENT_HH__

#include <cstddef>
#include "../common/compiler.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup C++-Templates
     *
     * @{
     */

    /**@addtogroup TupleUtilities
     *
     * Some additional utitliy functions for tuple-likes and
     * integer-sequences.
     *
     * @{
     */

    /**@addtogroup SequenceElementAccess
     *
     * Random access to sequence elements.
     *
     * @{
     */

#if DUNE_ACFEM_IS_CLANG(5,99)
    static_assert(__has_builtin(__type_pack_element), "");

    template<std::size_t N, class... T>
    using TypePackElement = __type_pack_element<N, T...>;
#else
    namespace {
      template<std::size_t N, class... T>
      struct TypePackElementHelper
      {};

      template<std::size_t N, class T0, class... TRest>
      struct TypePackElementHelper<N, T0, TRest...>
        : TypePackElementHelper<N-1, TRest...>
      {};

      template<class T0, class... TRest>
      struct TypePackElementHelper<0, T0, TRest...>
      {
        using Type = T0;
      };
#if 0
      template<class T0, class T1, class... TRest>
      struct TypePackElementHelper<1, T0, T1, TRest...>
      {
        using Type = T1;
      };

      template<class T0, class T1, class T2, class... TRest>
      struct TypePackElementHelper<2, T0, T1, T2, TRest...>
      {
        using Type = T2;
      };

      template<class T0, class T1, class T2, class T3, class... TRest>
      struct TypePackElementHelper<3, T0, T1, T2, T3, TRest...>
      {
        using Type = T3;
      };

      template<class T0, class T1, class T2, class T3, class T4, class... TRest>
      struct TypePackElementHelper<4, T0, T1, T2, T3, T4, TRest...>
      {
        using Type = T4;
      };

      template<class T0, class T1, class T2, class T3, class T4, class T5, class... TRest>
      struct TypePackElementHelper<5, T0, T1, T2, T3, T4, T5, TRest...>
      {
        using Type = T5;
      };

      template<class T0, class T1, class T2, class T3, class T4, class T5, class T6, class... TRest>
      struct TypePackElementHelper<6, T0, T1, T2, T3, T4, T5, T6, TRest...>
      {
        using Type = T6;
      };

      template<class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class... TRest>
      struct TypePackElementHelper<7, T0, T1, T2, T3, T4, T5, T6, T7, TRest...>
      {
        using Type = T7;
      };

      template<class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, class... TRest>
      struct TypePackElementHelper<8, T0, T1, T2, T3, T4, T5, T6, T7, T8, TRest...>
      {
        using Type = T8;
      };

      template<class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, class T9, class... TRest>
      struct TypePackElementHelper<9, T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, TRest...>
      {
        using Type = T9;
      };
#endif
    }

    template<std::size_t N, class... T>
    using TypePackElement = typename TypePackElementHelper<N, T...>::Type;
#endif

    //!@} TupleUtilities

    //!@} C++-Templates

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_MPL_TYPEPACKELEMENT_HH__
