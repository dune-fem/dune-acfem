#ifndef __DUNE_ACFEM_MPL_TOSTRING_HH__
#define __DUNE_ACFEM_MPL_TOSTRING_HH__

#include <string>
#include "generators.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup C++-Templates
     *
     * @{
     */

    /**@addtogroup TupleUtilities
     *
     * Some additional utitliy functions for tuple-likes and
     * integer-sequences.
     *
     * @{
     */

    /**Generate a compact string representation as a comma separated
     * list of the values, without spaces.
     */
    template<class T, T I0>
    auto toString(Sequence<T, I0>)
    {
      return std::to_string(I0);
    }

    /**Generate a compact string representation as a comma separated
     * list of the values, without spaces.
     */
    template<class T, T I0, T I1, T... Rest>
    auto toString(Sequence<T, I0, I1, Rest...>)
    {
      return std::to_string(I0) + "," + toString(Sequence<T, I1, Rest...>{});
    }

    /**@internal Special case: empty sequence yields an empty string.*/
    template<class T>
    auto toString(Sequence<T>)
    {
      return std::string();
    }

    namespace {
      template<class T, std::size_t I0>
      std::string toStringHelper(const T& t, IndexSequence<I0>)
      {
        return std::to_string(std::get<I0>(t));
      }

      template<class T, std::size_t I0, std::size_t I1, std::size_t... Rest>
      auto toStringHelper(const T& t, IndexSequence<I0, I1, Rest...>)
      {
        return std::to_string(std::get<I0>(t)) + "," + toStringHelper(t, IndexSequence<I1, Rest...>{});
      }
    }

    template<class T, std::enable_if_t<IsTupleLike<T>{}, int> = 0>
    auto toString(const T& t)
    {
      return toStringHelper(t, MakeSequenceFor<T>{});
    }

    template<class T, T V, std::enable_if_t<!std::is_same<T, bool>{}, int> = 0>
    auto toString(Constant<T, V>)
    {
      return std::to_string(V)+"_c";
    }

    template<bool V>
    std::string toString(BoolConstant<V>)
    {
      return V ? "true" : "false";
    }

    //!@} TupleUtilities

    //!@} C++-Templates

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_MPL_TOSTRING_HH__
