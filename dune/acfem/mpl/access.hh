#ifndef __DUNE_ACFEM_MPL_ACCESS_HH__
#define __DUNE_ACFEM_MPL_ACCESS_HH__

#include <tuple>
#include <array>

#include <dune/common/hybridutilities.hh>
#include "../common/types.hh"
#include "generators.hh"
#include "size.hh"
#include "typepackelement.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup C++-Templates
     *
     * @{
     */

    /**@addtogroup TupleUtilities
     *
     * Some additional utitliy functions for tuple-likes and
     * integer-sequences.
     *
     * @{
     */

    /**@addtogroup SequenceElementAccess
     *
     * Random access to sequence elements.
     *
     * @{
     */

    /**Gets the type of the n-th element of a tuple-like or the
     * std::integral_constant corresponding to the n-th element of a
     * sequence.
     */
    template<std::size_t N, class Seq>
    struct Get;

#if DUNE_ACFEM_IS_CLANG(5,99)
# define DUNE_ACFEM_GET_IS_FAST true
    static_assert(__has_builtin(__type_pack_element), "");

    template<std::size_t N, class I, I... Is>
    struct Get<N, Sequence<I, Is...> >
      : __type_pack_element<N, Constant<I, Is>...>
    {};
#else
    template<std::size_t N, class I, I I0, I... IRest>
    struct Get<N, Sequence<I, I0, IRest...> >
      : Get<N-1, Sequence<I, IRest...> >
    {};

    template<class I, I I0, I... IRest>
    struct Get<0, Sequence<I, I0, IRest...> >
      : Constant<I, I0>
    {};

#if 0
    template<class I, I I0, I I1, I... IRest>
    struct Get<1, Sequence<I, I0, I1, IRest...> >
      : Constant<I, I1>
    {};

    template<class I, I I0, I I1, I I2, I... IRest>
    struct Get<2, Sequence<I, I0, I1, I2, IRest...> >
      : Constant<I, I2>
    {};

    template<class I, I I0, I I1, I I2, I I3, I... IRest>
    struct Get<3, Sequence<I, I0, I1, I2, I3, IRest...> >
      : Constant<I, I3>
    {};

    template<class I, I I0, I I1, I I2, I I3, I I4, I... IRest>
    struct Get<4, Sequence<I, I0, I1, I2, I3, I4, IRest...> >
      : Constant<I, I4>
    {};

    template<class I, I I0, I I1, I I2, I I3, I I4, I I5, I... IRest>
    struct Get<5, Sequence<I, I0, I1, I2, I3, I4, I5, IRest...> >
      : Constant<I, I5>
    {};

    template<class I, I I0, I I1, I I2, I I3, I I4, I I5, I I6, I... IRest>
    struct Get<6, Sequence<I, I0, I1, I2, I3, I4, I5, I6, IRest...> >
      : Constant<I, I6>
    {};

    template<class I, I I0, I I1, I I2, I I3, I I4, I I5, I I6, I I7, I... IRest>
    struct Get<7, Sequence<I, I0, I1, I2, I3, I4, I5, I6, I7, IRest...> >
      : Constant<I, I7>
    {};

    template<class I, I I0, I I1, I I2, I I3, I I4, I I5, I I6, I I7, I I8, I... IRest>
    struct Get<8, Sequence<I, I0, I1, I2, I3, I4, I5, I6, I7, I8, IRest...> >
      : Constant<I, I8>
    {};

    template<class I, I I0, I I1, I I2, I I3, I I4, I I5, I I6, I I7, I I8, I I9, I... IRest>
    struct Get<9, Sequence<I, I0, I1, I2, I3, I4, I5, I6, I7, I8, I9, IRest...> >
      : Constant<I, I9>
    {};
#endif

#endif

    template<class I>
    struct Get<0, Sequence<I> >
    {
      // To static_assert or not to static_assert ...
    };

    template<class I, I I0>
    struct Get<0, Constant<I, I0> >
      : Constant<I, I0>
    {};

    /**Forward to std::tuple_element<N, std::decay_t<T> >*/
    template<std::size_t N, class TupleLike>
    using TupleElement = std::tuple_element_t<N, std::decay_t<TupleLike> >;

    /**Access to the i-the element.*/
    template<std::size_t I, class T, std::enable_if_t<IsTupleLike<T>::value, int> = 0>
    constexpr decltype(auto) get(T&& t, IndexConstant<I> = IndexConstant<I>{})
    {
      return std::get<I>(std::forward<T>(t));
    }

    /**Access to the i-the element.*/
    template<std::size_t I, class T, std::size_t N, std::enable_if_t<std::is_integral<T>::value, int> = 0>
    constexpr decltype(auto) get(const T (&t)[N], IndexConstant<I> = IndexConstant<I>{})
    {
      return t[I];
    }

    /**Access to the i-the element.*/
    template<std::size_t I, class T, T... V>
    constexpr auto get(Sequence<T, V...>, IndexConstant<I> = IndexConstant<I>{})
    {
      return Get<I, Sequence<T, V...> >::value;
    }

    /**Access to the i-the element.*/
    template<std::size_t I, class T, T V>
    constexpr auto get(Constant<T, V>, IndexConstant<I> = IndexConstant<I>{})
    {
      static_assert(I == 0, "Integral constants only have one value.");
      return V;
    }

    template<class T, class SFINAE = void>
    struct AllowsGet
      : FalseType
    {};

    template<class T>
    struct AllowsGet<T, std::enable_if_t<(size<T>() != size<void>())> >
      : TrueType
    {};

    /*Obtain type of first tuple element, define to void if empty tuple. */
    template<class SequenceLike>
    using Head = Get<0, SequenceLike>;

    /*Obtain type of a last tuple element.*/
    template<class SequenceLike>
    using Tail = Get<SequenceLike::size()-1, SequenceLike>;

    namespace {

      // helpers for GetPart template which extracts a consecutive
      // part of an integer_sequence starting at a given position.

      template<std::size_t Skip, class Sequence, class SFINAE = void>
      struct GetTailPartHelper;

      template<class Seq>
      struct GetTailPartHelper<0, Seq>
      {
        using Type = Seq;
      };

      template<std::size_t Skip, class T, T I0, T... I>
      struct GetTailPartHelper<Skip, Sequence<T, I0, I...>, std::enable_if_t<(Skip > 0)> >
      {
        using Type = typename GetTailPartHelper<Skip-1, Sequence<T, I...> >::Type;
      };

      template<std::size_t Cnt, class Input, class Output = Sequence<typename Input::value_type>, class SFINAE = void>
      struct GetHeadPartHelper;

      template<class Input, class Output>
      struct GetHeadPartHelper<0, Input, Output>
      {
        using Type = Output;
      };

      template<std::size_t Cnt, class T, T I0, T... I, T... O>
      struct GetHeadPartHelper<Cnt, Sequence<T, I0, I...>, Sequence<T, O...>, std::enable_if_t<(Cnt > 0)> >
      {
        using Type = typename GetHeadPartHelper<Cnt-1, Sequence<T, I...>, Sequence<T, O..., I0> >::Type;
      };

    }

    /**Extract Cnt many consecutive elements from Seq starting at position N.*/
    template<std::size_t Skip, std::size_t Cnt, class Seq>
    using GetPart = typename GetHeadPartHelper<Cnt, typename GetTailPartHelper<Skip, Seq>::Type>::Type;

    /**Extract Cnt many consecutive elements from the front of Seq.*/
    template<std::size_t Cnt, class Seq>
    using HeadPart = typename GetHeadPartHelper<Cnt, Seq>::Type;

    /**Extract Cnt many consecutive elements from the end of Seq.*/
    template<std::size_t Cnt, class Seq>
    using TailPart = typename GetTailPartHelper<Seq::size()-Cnt, Seq>::Type;

    namespace {
      // helper templates for IndexIn template.

      // default is "infinity".
      template<std::size_t HeadSize, class T, T V, class Seq>
      struct IndexInHelper
      {
        static constexpr std::size_t value = std::numeric_limits<std::size_t>::max();
      };

      // "diagonal" case: first element is equal to V
      template<std::size_t HeadSize, class T, T V, T... Ints>
      struct IndexInHelper<HeadSize, T, V, Sequence<T, V, Ints...> >
      {
        static constexpr std::size_t value = HeadSize;
      };

      // Recurse until
      template<std::size_t HeadSize, class T, T V, T I, T... Ints>
      struct IndexInHelper<HeadSize, T, V, Sequence<T, I, Ints...> >
      {
        static constexpr std::size_t value =
          IndexInHelper<HeadSize+1, T, V, Sequence<T, Ints...> >::value;
      };

    }

    template<class T, T... I, T V>
    constexpr auto multiplicity(Sequence<T, I...>, Constant<T, V>)
    {
      return IndexConstant<((std::size_t)(I == V) + ... + 0)>::value;
    }

    template<class T, T... I>
    constexpr bool containsValue(Sequence<T, I...>, T V)
    {
      return (... || (I == V));
    }

#if 0
    template<class T, T... I, T V, std::size_t... Idx>
    constexpr auto indexInHelper(Sequence<T, I...>, Constant<T, V>, IndexSequence<Idx...>)
    {
      return IndexConstant<((std::size_t)((I == V)*Idx) + ... + 0)>::value;
    }

    template<class T, T... I, T V>
    constexpr auto indexIn(Sequence<T, I...>, Constant<T, V>)
    {
      if constexpr (
      return IndexConstant<((std::size_t)((I == V)*Idx) + ... + 0)>::value;
    }
#endif

    /**Find the index of a given value in a sequence.*/
    template<class Seq, typename Seq::value_type V>
    using IndexIn = IndexConstant<IndexInHelper<0, typename Seq::value_type, V, Seq>::value>;

#if 0
    /**Check for presence of given value in sequence.*/
    template<class Seq, typename Seq::value_type V>
    using ContainsValue = BoolConstant<IndexIn<Seq, V>::value < std::numeric_limits<std::size_t>::max()>;
#else
    template<class Seq, typename Seq::value_type V>
    constexpr inline bool ContainsValueV = containsValue(Seq{}, V);

    template<class Seq, typename Seq::value_type V>
    using ContainsValue = BoolConstant<containsValue(Seq{}, V)>;
#endif

    /**Add V at the front of the sequence.*/
    template<std::ptrdiff_t V, class T, T... I>
    constexpr auto pushFront(Sequence<T, I...>, IntConstant<V> = IntConstant<V>{})
    {
      return Sequence<T, V, I...>{};
    }

    /**Add V at the end of the sequece.*/
    template<std::ptrdiff_t V, class T, T... I>
    constexpr auto pushBack(Sequence<T, I...>, IntConstant<V> = IntConstant<V>{})
    {
      return Sequence<T, I..., V>{};
    }

    /**Add value to start of integer-sequence. */
    template<std::ptrdiff_t V, class Seq>
    using PushFront = decltype(pushFront<V>(Seq{}));

    /**Add value to end of integer-sequence. */
    template<std::ptrdiff_t V, class Seq>
    using PushBack = decltype(pushBack<V>(Seq{}));

    namespace {
      template<std::size_t V, std::size_t N, class Head, class Tail>
      struct PutHelper;

      template<std::size_t V, std::size_t N, std::size_t... Head, std::size_t Tail0, std::size_t... Tail>
      struct PutHelper<V, N, IndexSequence<Head...>, IndexSequence<Tail0, Tail...> >
      {
        using Type = typename PutHelper<V, N-1, IndexSequence<Head..., Tail0>, IndexSequence<Tail...> >::Type;
      };

      template<std::size_t V, std::size_t... Head, std::size_t Tail0, std::size_t... Tail>
      struct PutHelper<V, 0, IndexSequence<Head...>, IndexSequence<Tail0, Tail...> >
      {
        using Type = IndexSequence<Head..., V, Tail...>;
      };
    }

    /**Replace value at position @a N with @a V.*/
    template<std::size_t V, std::size_t N, class Input>
    using Put = typename PutHelper<V, N, IndexSequence<>, Input>::Type;

    //!@} SequenceElementAccess

    //!@} TupleUtilities

    //!@} C++-Templates

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_MPL_ACCESS_HH__
