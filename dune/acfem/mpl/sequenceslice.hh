#ifndef __DUNE_ACFEM_MPL_SEQUENCESLICE_HH__
#define __DUNE_ACFEM_MPL_SEQUENCESLICE_HH__

#include "../common/types.hh"
#include "access.hh"

namespace Dune::ACFem {

  namespace MPL {

    /**Create a subsequence containing all values designated by the
     * positions given by @a Indices. This is cheap if Get is cheap
     * ...
     */
    template<class Seq, std::size_t... Indices>
    using SubSequence = Sequence<typename Seq::value_type, Get<Indices, Seq>::value...>;

    namespace {
      template<class Seq, class Slice>
      struct SliceExpander;

      template<class Seq, std::size_t... Indices>
      struct SliceExpander<Seq, IndexSequence<Indices...> >
      {
        using Type = Sequence<typename Seq::value_type, Get<Indices, Seq>::value...>;
      };
    }

    /**Like SubSequence, but take the indices from another sequence.
     */
    template<class Seq, class SliceIndices>
    using SequenceSlice = typename SliceExpander<Seq, SliceIndices>::Type;

    /**Form a sub-sequence with the values at the positions NOT given
     * in @a Indices.
     */
    namespace {

      template<class Input, class Indices, class Output = Sequence<typename Input::value_type>, std::size_t N = 0, class SFINAE = void>
      struct SequenceSliceComplementHelper;

      // no match recursion
      template<class T, T In0, T... In, class Output, std::size_t N, std::size_t... Indices>
      struct SequenceSliceComplementHelper<
        Sequence<T, In0, In...>,
        IndexSequence<Indices...>,
        Output, N,
        std::enable_if_t<(... || (N == Indices))> >
      {
        using Type = typename SequenceSliceComplementHelper<
          Sequence<T, In...>,
          IndexSequence<Indices...>,
          Output, N+1 >::Type;
      };

      // have match recursion
      template<class T, T In0, T... In, T... Out, std::size_t N, std::size_t... Indices>
      struct SequenceSliceComplementHelper<
        Sequence<T, In0, In...>,
        IndexSequence<Indices...>,
        Sequence<T, Out...>, N,
        std::enable_if_t<(... && (N != Indices))> >
      {
        using Type = typename SequenceSliceComplementHelper<
          Sequence<T, In...>,
          IndexSequence<Indices...>,
          Sequence<T, Out..., In0>, N+1>::Type;
      };

      // recursion end-point
      template<class T, class Indices, class Output, std::size_t N>
      struct SequenceSliceComplementHelper<Sequence<T>, Indices, Output, N>
      {
        using Type = Output;
      };

    }

    template<class Seq, class SliceIndices>
    using SequenceSliceComplement = typename SequenceSliceComplementHelper<Seq, SliceIndices>::Type;

    template<class Seq, std::size_t... Indices>
    using SubSequenceComplement = typename SequenceSliceComplementHelper<Seq, IndexSequence<Indices...> >::Type;

    namespace {

      template<class Input, class Output = Sequence<typename Input::value_type> >
      struct ReverseSequenceHelper;

      template<class T, T I0, T... I, T... Out>
      struct ReverseSequenceHelper<Sequence<T, I0, I...>, Sequence<T, Out...> >
        : ReverseSequenceHelper<Sequence<T, I...>, Sequence<T, I0, Out...> >
      {};

      template<class T, class Output>
      struct ReverseSequenceHelper<Sequence<T>, Output>
      {
        using Type = Output;
      };

    }

    /**Generate the reverse sequence.*/
    template<class Seq>
    using ReverseSequence = typename ReverseSequenceHelper<Seq>::Type;

  } // MPL::

  using MPL::SequenceSlice;
  using MPL::SequenceSliceComplement;
  using MPL::SubSequence;
  using MPL::SubSequenceComplement;
  using MPL::ReverseSequence;

} // Dune::ACFem::

#endif // __DUNE_ACFEM_MPL_SEQUENCESLICE_HH__
