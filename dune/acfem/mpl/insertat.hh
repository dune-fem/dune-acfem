#ifndef __DUNE_ACFEM_MPL_INSERTAT_HH__
#define __DUNE_ACFEM_MPL_INSERTAT_HH__

#include "sort.hh"
#include "permutation.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup C++-Templates
     *
     * @{
     */

    /**@addtogroup TupleUtilities
     *
     * Some additional utitliy functions for tuple-likes and
     * integer-sequences.
     *
     * @{
     */

    /**@addtogroup SequenceTransform
     *
     * Apply transformation functors to given integer sequences.
     *
     * @{
     */

    namespace {

      // Helper for sequence insertAt() alike
      template<class Out, class Input, class Inject, class Pos, bool AssumeSorted = true, class SFINAE = void>
      struct InsertAtHelper;

      // Inject data at current position
      template<class T, class Input, class Inject, class Pos>
      struct InsertAtHelper<Sequence<T>, Input, Inject, Pos, false>
      {
        using Sort = SortSequence<Pos>;
        using SortedInject = PermuteSequence<Inject, typename Sort::Permutation>;
        using SortedPos = typename Sort::Result;
        using Type = typename InsertAtHelper<Sequence<T>, Input, SortedInject, SortedPos>::Type;
      };

      // Inject data at current position
      template<class T, T... Out,
               class Input,
               T FrontInject, T... RestInject,
               std::size_t FrontPos, std::size_t... RestPos>
      struct InsertAtHelper<Sequence<T, Out...>, Input, Sequence<T, FrontInject, RestInject...>, IndexSequence<FrontPos, RestPos...>,
                            true,
                            std::enable_if_t<sizeof...(Out) == FrontPos> >
      {
        using Type = typename InsertAtHelper<Sequence<T, Out..., FrontInject>, Input, Sequence<T, RestInject...>, IndexSequence<RestPos...> >::Type;
      };

      // Still data to inject, but not at this position
      template<class T, T... Out,
               T FrontInput, T... RestInput,
               class Inject,
               std::size_t FrontPos, std::size_t... RestPos>
      struct InsertAtHelper<Sequence<T, Out...>,
                            Sequence<T, FrontInput, RestInput...>,
                            Inject,
                            IndexSequence<FrontPos, RestPos...>,
                            true,
                            std::enable_if_t<(sizeof...(Out) < FrontPos)> >
      {
        using Type = typename InsertAtHelper<Sequence<T, Out..., FrontInput>, Sequence<T, RestInput...>, Inject, IndexSequence<FrontPos, RestPos...> >::Type;
      };

      // Recursion end-point: nothing more to inject, just copy over old data.
      template<class Out, class Input, class Pos>
      struct InsertAtHelper<Out, Input, Sequence<typename Input::value_type>, Pos>
      {
        using Type = SequenceCat<Out, Input>;
      };
    }

    /**Insert @a Inject into the sequence @a Input at the position specified by @a Pos.
     *
     * The tuple look-alike insertAt<Pos>(input, inject).
     */
    template<class Input, class Inject, class Pos, bool AssumeSorted = true>
    using InsertAt = typename InsertAtHelper<Sequence<typename Input::value_type>, Input, Inject, Pos, AssumeSorted>::Type;

    //!@} SequenceTransform

    //!@} TupleUtilities

    //!@} C++-Templates

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_MPL_INSERTAT_HH__
