#ifndef __DUNE_ACFEM_MPL_PERMUATION_HH__
#define __DUNE_ACFEM_MPL_PERMUATION_HH__

#include "filter.hh"
#include "transform.hh"
#include "compare.hh"
#include "generators.hh"
#include "sort.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup C++-Templates
     *
     * @{
     */

    /**@addtogroup TupleUtilities
     *
     * Some additional utitliy functions for tuple-likes and
     * integer-sequences.
     *
     * @{
     */

#if 0
    namespace {
      // doesn't seem to make much difference whether we use the
      // permutation from the sorting algorithm or doing in "by hand".

      template<class Input, std::size_t N = 0, class Result = MakeIndexSequence<size<Input>()> >
      struct InversePermutationHelper;

      template<std::size_t I0, std::size_t... I, std::size_t N, class Result>
      struct InversePermutationHelper<IndexSequence<I0, I...>, N, Result>
      {
        using Type = typename InversePermutationHelper<IndexSequence<I...>, N+1, Put<N, I0, Result> >::Type;
      };

      template<std::size_t N, class Result>
      struct InversePermutationHelper<IndexSequence<>, N, Result>
      {
        using Type = Result;
      };
    }

    /**Compute the inverse of the given permutation.*/
    template<class Permutation>
    using InversePermutation = typename InversePermutationHelper<Permutation>::Type;
#else

    /**Compute the inverse of the given permutation.*/
    template<class Permutation>
    using InversePermutation = typename SortSequence<Permutation>::Permutation;
#endif

    namespace {
      template<class Permutation, std::size_t N, class SFINAE = void>
      struct ResizedPermutationHelper;

      template<class Permutation, std::size_t N>
      struct ResizedPermutationHelper<Permutation, N, std::enable_if_t<(N > Permutation::size())> >
      {
        using Type = SequenceCat<Permutation, MakeIndexSequence<(N - Permutation::size()), Permutation::size()> >;
      };

      template<class Permutation, std::size_t N>
      struct ResizedPermutationHelper<Permutation, N, std::enable_if_t<N == Permutation::size()> >
      {
        using Type = Permutation;
      };

      template<class Permutation, std::size_t N>
      struct ResizedPermutationHelper<Permutation, N, std::enable_if_t<(N < Permutation::size())> >
      {
        using Type = HeadPart<N, Permutation>;
      };
    }

    /**Pad or truncate the permuation at the given size.*/
    template<class Permutation, std::size_t N>
    using ResizedPermutation = typename ResizedPermutationHelper<Permutation, N>::Type;

#if 1
    namespace {

      template<class Perm, class Seq>
      struct PermuteSequenceValuesHelper;

      template<class Perm, class T, T... I>
      struct PermuteSequenceValuesHelper<Perm, Sequence<T, I...> >
      {
        using Type = Sequence<T, Get<I, Perm>::value...>;
      };

    }

    /**Apply the given permutation to the values of the given sequence.*/
    template<class Sequence, class Perm>
    using PermuteSequenceValues = typename PermuteSequenceValuesHelper<Perm, Sequence>::Type;
#else
    /**Apply the given permutation to the values of the given sequence.*/
    template<class Sequence, class Perm>
    using PermuteSequenceValues = TransformedSequence<MapSequenceFunctor<Perm>, Sequence>;
#endif

    /**Evaluate to @c true if applying the permutation @a Perm to the
     * values of @a Seq leaves the values invariant.
     */
    template<class Seq, class Perm>
    constexpr inline bool HasInvariantValuesV = std::is_same<Seq, PermuteSequenceValues<Seq, Perm> >::value;

    /**Permute the given sequence.*/
    template<std::size_t...Ps, class Seq, std::enable_if_t<IsSequence<Seq>::value, int> = 0>
    constexpr auto permute(Seq, IndexSequence<Ps...> = IndexSequence<Ps...>{})
    {
      return Sequence<typename Seq::value_type, Get<Ps, Seq>::value...>{};
    }

    /**Permute the given tuple like.*/
    template<std::size_t...Ps, class T, std::enable_if_t<IsTupleLike<T>::value && !isSimple(IndexSequence<Ps...>{}), int> = 0>
    constexpr auto permute(T&& t, IndexSequence<Ps...> = IndexSequence<Ps...>{})
    {
      return std::make_tuple(get<Ps>(std::forward<T>(t))...);
    }

    /**Permute the given tuple like.*/
    template<std::size_t...Ps, class T, std::enable_if_t<IsTupleLike<T>::value && isSimple(IndexSequence<Ps...>{}), int> = 0>
    constexpr decltype(auto) permute(T&& t, IndexSequence<Ps...> = IndexSequence<Ps...>{})
    {
      return std::forward<T>(t);
    }

    /**Apply the given permutation to the positions of the given sequence.*/
    template<class Sequence, class Perm>
    using PermuteSequence = std::decay_t<decltype(permute(Sequence{}, Perm{}))>;

    /**Return @c true if applying the given permutation to the value
     * positions leaves the sequence invariant.
     */
    template<class T, T... I, std::size_t... P, std::enable_if_t<sizeof...(I) == sizeof...(P), int> = 0>
    constexpr auto isInvariant(Sequence<T, I...>, IndexSequence<P...>)
    {
      return (... && (I == Get<P, Sequence<T, I...> >::value));
    }

    template<class T, T... I, class Permutation, std::enable_if_t<(sizeof...(I) > Permutation::size()), int> = 0>
    constexpr auto isInvariant(Sequence<T, I...>, Permutation)
    {
      return isInvariant(Sequence<T, I...>{}, ResizedPermutation<Permutation, sizeof...(I)>{});
    }

    template<class T, T... I, class Permutation, std::enable_if_t<(sizeof...(I) < Permutation::size()), int> = 0>
    constexpr auto isInvariant(Sequence<T, I...>, Permutation)
    {
      return false;
    }

    namespace {
      template<std::size_t I0, std::size_t I1, std::size_t N>
      struct TranspositionHelper
      {
        using Type = SequenceCat<
          MakeIndexSequence<I0>,
          IndexSequence<I1>,
          MakeIndexSequence<I1-I0-1, I0+1>,
          IndexSequence<I0>,
          MakeIndexSequence<N-I1-1, I1+1>
          >;
      };

      template<std::size_t I0, std::size_t N>
      struct TranspositionHelper<I0, I0, N>
      {
        using Type = MakeIndexSequence<N>;
      };

      template<class Seq0, class Seq1, std::size_t N>
      struct BlockTranspositionHelper
      {
        static constexpr std::size_t Head0 = Head<Seq0>::value;
        static constexpr std::size_t Head1 = Head<Seq1>::value;
        static constexpr std::size_t BlockSize = Seq0::size();
        using Type = SequenceCat<
          MakeIndexSequence<Head0>,
          Seq1,
          MakeIndexSequence<Head1-Head0-BlockSize, Head0+BlockSize>,
          Seq0,
          MakeIndexSequence<N-Head1-BlockSize, Head1+BlockSize>
          >;
      };

      template<class Seq, std::size_t N>
      struct BlockTranspositionHelper<Seq, Seq, N>
      {
        using Type = MakeIndexSequence<N>;
      };

    }

    /**Generate the transposition of I0 and I1 as permutation. Passing
     * I0 > I1 will result in undefined behaviour.
     */
    template<std::size_t I0, std::size_t I1, std::size_t N = I1 + 1>
    using Transposition = typename TranspositionHelper<I0, I1, N>::Type;

    /**Generate the block-transposition of the I0 and the I1-block as permutation. Passing
     * I0 > I1 will result in undefined behaviour.
     */
    template<class Seq0, class Seq1, std::size_t N = Head<Seq1>::value+Seq1::size()>
    using BlockTransposition = typename BlockTranspositionHelper<Seq0, Seq1, N>::Type;

    //!@} TupleUtilities

    //!@} C++-Templates

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_MPL_PERMUATION_HH__
