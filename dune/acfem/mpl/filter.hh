#ifndef __DUNE_ACFEM_MPL_FILTER_HH__
#define __DUNE_ACFEM_MPL_FILTER_HH__

#include "transform.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup C++-Templates
     *
     * @{
     */

    /**@addtogroup TupleUtilities
     *
     * Some additional utitliy functions for tuple-likes and
     * integer-sequences.
     *
     * @{
     */

    /**@addtogroup SequenceFilter
     *
     * Create sub-sequences by filtering out elements of a given sequence.
     *
     * @{
     */

    /**Filter-proxy for TransformSequence template alias. Wraps a given
     * uuary functor which provides a template method
     *
     * @code
     * template<class T, T V, std::size_t N>
     * using Apply = SOMETHING
     * @endcode
     *
     * where
     *
     * @code
     * Apply<T, V, N>::value
     * @endcode
     *
     * must be something convertible to @c bool. The wrapped functor
     * can be passed to TransformSequene. This is used by the
     * FilterSequence alias template which forms a new sequence out of
     * a given sequence by filtering out values. If N is not needed an
     * implementation may simply omit the parameter N.
     */
    template<class F, class Input>
    struct UnaryFilterFunctor
    {
      template<class Tin, Tin V, class Tout, Tout FV, std::size_t N>
      struct Apply
        : BoolConstant<F::template Apply<Tin, V, N>::value>
      {};
    };

    /**@internal concat 2 sequences.*/
    template<typename Int, Int... I0, Int... I1>
    constexpr auto sequenceCat(Sequence<Int, I0...>, Sequence<Int, I1...>)
    {
      return Sequence<Int, I0..., I1...>{};
    }

    /**@internal concat 3 and more sequences, parameter Seq is
     * needed to distinguish this from the 2 sequence casee as
     * parameter packs may be empty.
     */
    template<typename Int, Int... I0, Int... I1, class Seq, class... RestSeq>
    constexpr auto sequenceCat(Sequence<Int, I0...>, Sequence<Int, I1...>, Seq, RestSeq...)
    {
      return sequenceCat(Sequence<Int, I0..., I1...>{}, Seq{}, RestSeq{}...);
    }

    /**@internal concat 1 sequence with nothing.*/
    template<class Seq>
    constexpr auto sequenceCat(Seq)
    {
      return Seq{};
    }

    /**Create a new sequence by filtering out certain elements. The
     * functor must provide a template alias or a structure
     *
     * @code
     * struct MyFunctor
     * {
     *   template<class T, T mask, std::size_t N>
     *   using Apply = SOMETHING
     * };
     * @endcode
     *
     * where
     *
     * @code
     * Apply<T, mask, N>::value
     * @endcode
     *
     * must be a value convertible to @c bool. The resulting sequence
     * is formed from the elements where the functor template
     * evaluates to @c true.
     *
     * The parameter N may be omitted, it's also ok in this way:
     *
     * @code
     * struct MyFunctor
     * {
     *   template<class T, T mask, std::size_t N>
     *   using Apply = SOMETHING
     * };
     * @endcode
     */
    template<class F, class Seq>
    using FilteredSequence = TransformSequence<Seq, Sequence<typename Seq::value_type>, IdentityFunctor, UnaryFilterFunctor<F, typename Seq::value_type> >;

    /**@addtogroup SequenceFilterFunctors
     *
     * Pre-defined functors as argument to FilteredSequence template.
     *
     * @{
     */
    struct UnaryAcceptAllFunctor
    {
      template<class T, T>
      using Apply = TrueType;
    };

    /**Negate a given unary functor. See UnaryFilterFunctor for the
     * requirements on @c F.
     */
    template<class F>
    struct UnaryNotFunctor
    {
      template<class T, T V, std::size_t N>
      struct Apply
        : BoolConstant<!F::template Apply<T, V, N>::value>
      {};
    };

    /**Alternative: avoid bit-masks and use ContainsValue<> template.*/
    template<std::size_t... Indices>
    struct UnaryKeepAtFunctor
    {
      template<class T, T V, std::size_t N>
      struct Apply
        : ContainsValue<IndexSequence<Indices...>, N>
      {};
    };

    //!@} SequenceFilterFunctors

    namespace {
      template<std::size_t N, class S1, class S2>
      struct CommonHeadHelper
      {
        static constexpr std::size_t value = N;
      };

      template<std::size_t N,
               class T, T Head, T... Rest1, T... Rest2>
      struct CommonHeadHelper<N, Sequence<T, Head, Rest1...>, Sequence<T, Head, Rest2...> >
      {
        static constexpr std::size_t value =
          CommonHeadHelper<N+1, Sequence<T, Rest1...>, Sequence<T, Rest2...> >::value;
      };
    }

    /**Compute the number of identical indices at the head of the
     * sequence.
     */
    template<class Seq1, class Seq2>
    using CommonHead = IndexConstant<CommonHeadHelper<0UL, Seq1, Seq2>::value>;

    //!@} SequenceFilter

    //!@} TupleUtilities

    //!@} C++-Templates

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_MPL_FILTER_HH__
