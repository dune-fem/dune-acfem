#ifndef __DUNE_ACFEM_MPL_FOREACH_HH__
#define __DUNE_ACFEM_MPL_FOREACH_HH__

#include <dune/common/hybridutilities.hh>
#include "../common/types.hh"
#include "typetuple.hh"
#include "access.hh"
#include "filter.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup C++-Templates
     *
     * @{
     */

    /**@addtogroup TupleUtilities
     *
     * @{
     */

    /**@addtogroup Loops
     *
     * @{
     */

    template<std::size_t... I, class F>
    constexpr void forEach(F&& f, IndexSequence<I...> = IndexSequence<I...>{})
    {
      (... , std::forward<F>(f)(IndexConstant<I>{}));
    }

    template<std::size_t... I, class F>
    constexpr void forEach(IndexSequence<I...>, F&& f)
    {
      (... , std::forward<F>(f)(IndexConstant<I>{}));
    }

    template<class... T, class F>
    constexpr void forEach(MPL::TypeTuple<T...>, F&& f)
    {
      (... , std::forward<F>(f)(MPL::TypeWrapper<T>{}));
    }

    template<class T, class F, std::enable_if_t<IsTupleLike<T>::value, int> = 0>
    constexpr void forEach(T&& t, F&& f)
    {
      forEach(MakeSequenceFor<T>{}, [&](auto i) {
          using I = decltype(i);
          std::forward<F>(f)(std::forward<TupleElement<I::value, T> >(get<I::value>(std::forward<T>(t))));
        });
    }

    /***/
    template<std::size_t N, class F>
    constexpr void forLoop(F&& f)
    {
      forEach(MakeIndexSequence<N>{}, std::forward<F>(f));
    }

    /**Repeat until F returns @c false. Obviously, this requires that
     * F returns a boolean value.
     *
     * @param[in] s Integer sequence.
     *
     * @param[in] f Functor. @a f has to return a boolean, the loop
     * stops if f returns @c false. @a f is called in turn with the
     * values of the integer sequence @a s. @a f is passed the loop
     * index as st::integral_constant.
     *
     * @return @c false if the loop as been terminated before reaching
     * the end of @a s, otherwise @c true.
     */
    template<std::size_t... I, class F>
    constexpr bool forEachWhile(F&& f, IndexSequence<I...> = IndexSequence<I...>{})
    {
      return (... && (std::forward<F>(f)(IndexConstant<I>{})));
    }

    //!@copydoc forEachWhile()
    template<std::size_t... I, class F>
    constexpr bool forEachWhile(IndexSequence<I...>, F&& f)
    {
      return forEachWhile<I...>(std::forward<F>(f));
    }

    /**Invoke the given functor sizeof...(I) times and sum up the
     * result, where @a init gives the initial value of the sum.
     */
    template<std::size_t... I, class F, class T>
    constexpr auto addEach(T&& init, F&& f, IndexSequence<I...> = IndexSequence<I...>{})
    {
      return (std::forward<T>(init) + ... + (std::forward<F>(f)(IndexConstant<I>{})));
    }

    /**Invoke the given functor sizeof...(I) times and sum up the
     * result, where @a init gives the initial value of the sum.
     */
    template<std::size_t... I, class F, class T>
    constexpr auto
    addEach(IndexSequence<I...>, T&& init, F&& f)
    {
      return addEach<I...>(std::forward<F>(f), std::forward<T>(init));
    }

    /**Version with just a plain number as argument.*/
    template<std::size_t N, class F, class T>
    constexpr auto addLoop(F&& f, T&& init, IndexConstant<N> = IndexConstant<N>{})
    {
      return addEach(MakeIndexSequence<N>{}, std::forward<F>(f), std::forward<T>(init));
    }

    /**Invoke the given functor sizeof...(I) times and multiply the
     * result, where @a init gives the initial value of the sum.
     */
    template<std::size_t... I, class F, class T>
    constexpr auto multiplyEach(T&& init, F&& f, IndexSequence<I...> = IndexSequence<I...>{})
    {
      return (std::forward<T>(init) * ... * (std::forward<F>(f)(IndexConstant<I>{})));
    }

    /**Invoke the given functor sizeof...(I) times and multiply the
     * result, where @a init gives the initial value of the sum.
     */
    template<std::size_t... I, class F, class T>
    constexpr auto multiplyEach(IndexSequence<I...>, T&& init, F&& f)
    {
      return multiplyEach<I...>(std::forward<F>(f), std::forward<T>(init));
    }

    /**Version with just a plain number as argument.*/
    template<std::size_t N, class F, class T>
    constexpr auto multiplyLoop(T&& init, F&& f, IndexConstant<N> = IndexConstant<N>{})
    {
      return multiplyEach(MakeIndexSequence<N>{}, std::forward<F>(f), std::forward<T>(init));
    }

    //!@} Loops

    //!@} TupleUtilities

    //!@} C++-Templates

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_MPL_FOREACH_HH__
