#ifndef __DUNE_ACFEM_MPL_TRANSFORMTUPLE_HH__
#define __DUNE_ACFEM_MPL_TRANSFORMTUPLE_HH__

#include "../common/types.hh"
#include "access.hh"
#include "generators.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup C++-Templates
     *
     * @{
     */

    /**@addtogroup TupleUtilities
     *
     * @{
     */

    /**@addtogroup Loops
     *
     * @{
     */

    namespace {

      template<class Tuple, class F, std::size_t... I>
      constexpr auto transformTupleExpander(Tuple&& tuple, F&& f, IndexSequence<I...>)
      {
        return std::tuple<decltype(f(std::declval<TupleElement<I, Tuple> >()))...>(
          std::forward<F>(f)(std::forward<TupleElement<I, Tuple> >(get<I>(std::forward<Tuple>(tuple))))...
          );
      }

    }

    template<class Tuple, class F, std::enable_if_t<IsTupleLike<Tuple>::value, int> = 0>
    constexpr auto transformTuple(Tuple&& tuple, F&& f)
    {
      return transformTupleExpander(std::forward<Tuple>(tuple), std::forward<F>(f), MakeSequenceFor<Tuple>{});
    }

    //!@} Loops

    //!@} TupleUtilities

    //!@} C++-Templates

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_MPL_TRANSFORMTUPLE_HH__
