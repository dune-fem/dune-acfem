#ifndef __DUNE_ACFEM_MPL_CONDITIONAL_HH__
#define __DUNE_ACFEM_MPL_CONDITIONAL_HH__

namespace Dune {

  namespace ACFem {

    namespace MPL {

      template<bool>
      struct Conditional;

      template<>
      struct Conditional<true> {
        template<class A, class B>
        using Type = A;
      };

      template <>
      struct Conditional<false> {
        template<class A, class B>
        using Type = B;
      };

    }

#if 1
    template<bool Cond, class A, class B>
    using ConditionalType = typename MPL::Conditional<Cond>::template Type<A, B>;
#else
    template<bool Cond, class A, class B>
    using ConditionalType = std::conditional_t<Cond, A, B>;
#endif

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_MPL_CONDITIONAL_HH__
