#ifndef __DUNE_ACFEM_MPL_TYPETUPLEUTIL_HH__
#define __DUNE_ACFEM_MPL_TYPETUPLEUTIL_HH__

#include "typetuple.hh"

namespace Dune::ACFem::MPL {

  template<class... T>
  constexpr auto toTypeTuple(const std::tuple<T...>&)
  {
    return TypeTuple<T...>{};
  }

  template<class T>
  using TypeTupleType = decltype(toTypeTuple(std::declval<T>()));

  template<class Tuple, template<class...> class Predicate, class... Rest>
  struct PredicateMatch;

  template<class T0, class... T, template<class...> class Predicate, class... Rest>
  struct PredicateMatch<TypeTuple<T0, T...>, Predicate, Rest...>
    : ConditionalType<Predicate<T0, Rest...>::value,
                      TrueType,
                      PredicateMatch<TypeTuple<T...>, Predicate, Rest...> >
  {};

  template<template<class...> class Predicate, class... Rest>
  struct PredicateMatch<TypeTuple<>, Predicate, Rest...>
    : FalseType
  {};

  template<template<class...> class F, class Tuple, class T>
  using AddFrontIf =
    ConditionalType<PredicateMatch<Tuple, F, T>::value, AddFrontType<T, Tuple>, Tuple>;

  template<template<class...> class F, class Tuple, class T>
  using AddFrontUnless =
    ConditionalType<!PredicateMatch<Tuple, F, T>::value, AddFrontType<T, Tuple>, Tuple>;

  template<template<class...> class F, class Tuple, class T>
  using PushBackIf =
    ConditionalType<PredicateMatch<Tuple, F, T>::value, PushBackType<T, Tuple>, Tuple>;

  template<template<class...> class F, class Tuple, class T>
  using PushBackUnless =
    ConditionalType<!PredicateMatch<Tuple, F, T>::value, PushBackType<T, Tuple>, Tuple>;

  namespace {

    template<template<class...> class AddOne, class... Tuple>
    struct ConditionalJoinHelper;

    template<template<class...> class AddOne>
    struct ConditionalJoinHelper<AddOne>
    {
      using Type = MPL::TypeTuple<>;
    };

    template<template<class...> class AddOne, class Tuple>
    struct ConditionalJoinHelper<AddOne, Tuple>
    {
      using Type = Tuple;
    };

    template<template<class...> class AddOne, class Tuple, class... Rest>
    struct ConditionalJoinHelper<AddOne, Tuple, MPL::TypeTuple<>, Rest...>
    {
      using Type = typename ConditionalJoinHelper<AddOne, Tuple, Rest...>::Type;
    };

    template<template<class...> class AddOne, class Tuple, class T10, class... T1, class... Rest>
    struct ConditionalJoinHelper<AddOne, Tuple, MPL::TypeTuple<T10, T1...>, Rest...>
    {
      using Type = typename ConditionalJoinHelper<AddOne, AddOne<Tuple, T10>, MPL::TypeTuple<T1...>, Rest...>::Type;
    };

    template<template<class...> class F>
    struct ConditionalAddOne
    {
      template<class Tuple, class T>
      using If = PushBackIf<F, Tuple, T>;

      template<class Tuple, class T>
      using Unless = PushBackUnless<F, Tuple, T>;
    };


  }

  template<template<class...> class F, class... Tuples>
  using JoinIf = typename ConditionalJoinHelper<ConditionalAddOne<F>:: template If, Tuples...>::Type;

  template<template<class...> class F, class... Tuples>
  using JoinUnless = typename ConditionalJoinHelper<ConditionalAddOne<F>::template Unless, Tuples...>::Type;

  template<class Input, class Output>
  struct FlattenTypeTupleHelper;

  template<class Tuple>
  using FlattenTypeTuple = typename FlattenTypeTupleHelper<Tuple, TypeTuple<> >::Type;

  template<class... In0, class... InRest, class... Out>
  struct FlattenTypeTupleHelper<TypeTuple<TypeTuple<In0...>, InRest...>, TypeTuple<Out...> >
  {
    using Type = typename FlattenTypeTupleHelper<TypeTuple<In0..., InRest...>, TypeTuple<Out...> >::Type;
  };

  template<class In0, class... InRest, class... Out>
  struct FlattenTypeTupleHelper<TypeTuple<In0, InRest...>, TypeTuple<Out...> >
  {
    using Type = typename FlattenTypeTupleHelper<TypeTuple<InRest...>, TypeTuple<Out..., In0> >::Type;
  };

  template<class Out>
  struct FlattenTypeTupleHelper<TypeTuple<>, Out>
  {
    using Type = Out;
  };

}

#endif // __DUNE_ACFEM_MPL_TYPETUPLEUTIL_HH__
