#ifndef __DUNE_ACFEM_MPL_SIZE_HH__
#define __DUNE_ACFEM_MPL_SIZE_HH__

#include <tuple>
#include <array>

#include "../common/types.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup C++-Templates
     *
     * @{
     */

    /**@addtogroup TupleUtilities
     *
     * Some additional utitliy functions for tuple-likes and
     * integer-sequences.
     *
     * @{
     */

    /**@addtogroup SequenceElementAccess
     *
     * Random access to sequence elements.
     *
     * @{
     */

    /**@name ListSize
     *
     * Getting the size of sequences and tuple-likes.
     *
     * @{
     */
    template<class T>
    constexpr inline std::size_t SizeV = std::numeric_limits<std::size_t>::max();

    template<class T, T V>
    constexpr inline std::size_t SizeV<Constant<T, V> > = 1UL;

    template<class T, T... V>
    constexpr inline std::size_t SizeV<Sequence<T, V...> > = sizeof...(V);

    template<class... T>
    constexpr inline std::size_t SizeV<std::tuple<T...> > = sizeof...(T);

    template<class T0, class T1>
    constexpr inline std::size_t SizeV<std::pair<T0, T1> > = 2UL;

    template<class T, std::size_t N>
    constexpr inline std::size_t SizeV<std::array<T, N> > = N;

    template<class T>
    constexpr inline std::size_t SizeV<T&&> = SizeV<std::decay_t<T> >;

    template<class T>
    constexpr inline std::size_t SizeV<T&> = SizeV<std::decay_t<T> >;

    /**Gives the number of elements in tuple-likes and std::integer_sequence.*/
    template<class T>
    using Size = IndexConstant<SizeV<std::decay_t<T> > >;

#if DUNE_ACFEM_IS_CLANG(1,5)
    // clang < 6 does not like variable templates
# error Please use clang-6 or later.
#endif

    template<class T>
    constexpr std::size_t size()
    {
      return SizeV<T>;
    }

    /**@copydoc size().*/
    template<class T, std::enable_if_t<(SizeV<T> != SizeV<void>), int> = 0>
    constexpr std::size_t size(T&& t)
    {
      return size<T>();
    }

    //!@}

    //!@} SequenceElementAccess

    //!@} TupleUtilities

    //!@} C++-Templates

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_MPL_SIZE_HH__
