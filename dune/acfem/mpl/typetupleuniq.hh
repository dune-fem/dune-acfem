#ifndef __DUNE_ACFEM_MPL_TYPETUPLEUNIQ_HH__
#define __DUNE_ACFEM_MPL_TYPETUPLEUNIQ_HH__

#include "typetuplesort.hh"

namespace Dune::ACFem::MPL {

  namespace {

    template<class In, class Out, template<CXX17_P0522R0(class A, class B)> class Swap, class SFINAE = void>
    struct UniqueHelper;

    template<class T0, class T1, class... T, class... Out, template<CXX17_P0522R0(class A, class B)> class Swap>
    struct UniqueHelper<TypeTuple<T0, T1, T...>, TypeTuple<Out...>, Swap, std::enable_if_t<!Swap<T1, T0>::value> >
    {
      using Type = typename UniqueHelper<TypeTuple<T1, T...>, TypeTuple<Out...>, Swap>::Type;
    };

    template<class T0, class T1, class... T, class... Out, template<CXX17_P0522R0(class A, class B)> class Swap>
    struct UniqueHelper<TypeTuple<T0, T1, T...>, TypeTuple<Out...>, Swap, std::enable_if_t<Swap<T1, T0>::value> >
    {
      using Type = typename UniqueHelper<TypeTuple<T1, T...>, TypeTuple<Out..., T0>, Swap>::Type;
    };

    template<class T, class... Out, template<CXX17_P0522R0(class A, class B)> class Swap>
    struct UniqueHelper<TypeTuple<T>, TypeTuple<Out...>, Swap>
    {
      using Type = TypeTuple<Out..., T>;
    };

    template<template<CXX17_P0522R0(class A, class B)> class Swap>
    struct UniqueHelper<TypeTuple<>, TypeTuple<>, Swap>
    {
      using Type = TypeTuple<>;
    };

  }

  template<class Tuple, template<CXX17_P0522R0(class A, class B)> class Swap>
  using Unique = typename UniqueHelper<Tuple, TypeTuple<>, Swap>::Type;

  namespace {

    template<std::size_t Cnt, class In, class Out, template<CXX17_P0522R0(class A, class B)> class Unequal, template<std::size_t, class, class> class Process, class SFINAE = void>
    struct MultiplicityHelper;

    template<std::size_t Cnt, class T0, class T1, class... T, class Out, template<CXX17_P0522R0(class A, class B)> class Unequal, template<std::size_t, class, class> class Process>
    struct MultiplicityHelper<Cnt, TypeTuple<T0, T1, T...>, Out, Unequal, Process, std::enable_if_t<!Unequal<T1, T0>::value> >
    {
      using Type = typename MultiplicityHelper<Cnt+1, TypeTuple<T1, T...>, Out, Unequal, Process>::Type;
    };

    template<std::size_t Cnt, class T0, class T1, class... T, class Out, template<CXX17_P0522R0(class A, class B)> class Unequal, template<std::size_t, class, class> class Process>
    struct MultiplicityHelper<Cnt, TypeTuple<T0, T1, T...>, Out, Unequal, Process, std::enable_if_t<Unequal<T1, T0>::value> >
    {
      using Type = typename MultiplicityHelper<
        1,
        TypeTuple<T1, T...>,
        Process<Cnt, T0, Out>,
        Unequal,
        Process>::Type;
    };

    template<std::size_t Cnt, class T, class Out, template<CXX17_P0522R0(class A, class B)> class Unequal, template<std::size_t, class, class> class Process>
    struct MultiplicityHelper<Cnt, TypeTuple<T>, Out, Unequal, Process>
    {
      using Type = Process<Cnt, T, Out>;
    };

    template<class Out, template<CXX17_P0522R0(class A, class B)> class Unequal, template<std::size_t, class, class> class Process>
    struct MultiplicityHelper<1, TypeTuple<>, Out, Unequal, Process>
    {
      using Type = TypeTuple<>;
    };

  }

  template<class A, class B>
  using MultiplicityLT = BoolConstant<(A::First::value < B::First::value)>;

  template<class A, class B>
  using MultiplicityGT = BoolConstant<(A::First::value > B::First::value)>;

  template<std::size_t Cnt, class T, class Out>
  using MultiplicityTuple = TypeTupleMergeOneLeft<TypePair<IndexConstant<Cnt>, T>, Out, MultiplicityLT>;

  template<class Tuple, template<CXX17_P0522R0(class A, class B)> class Unequal, template<std::size_t Cnt, class T, class Out> class Process = MultiplicityTuple>
  using Multiplicities = typename MultiplicityHelper<1, Tuple, TypeTuple<>, Unequal, Process>::Type;

  template<std::size_t Cnt, class T, class Out>
  using MultiplicityMax = ConditionalType<
    (Cnt > Out::First::value),
    TypePair<IndexConstant<Cnt>, T>,
    Out>;

  template<class Tuple, template<CXX17_P0522R0(class A, class B)> class Unequal, template<std::size_t Cnt, class T, class Out> class Process = MultiplicityMax>
  using MultiplicitySelect = typename MultiplicityHelper<1, Tuple, TypePair<IndexConstant<0>, void>, Unequal, Process>::Type;

  namespace {

#if 0
    // variant ignoring sorted tuples.

    template<class T, class Head, class Tail, template<CXX17_P0522R0(class A, class B)> class Unequal, class SFINAE = void>
    struct RemoveMatchingHelper;

    template<class T, class... Head, class Tail0, class... TailRest, template<CXX17_P0522R0(class A, class B)> class Unequal>
    struct RemoveMatchingHelper<T, TypeTuple<Head...>, TypeTuple<Tail0, TailRest...>, Unequal, std::enable_if_t<!Unequal<T, Tail0>::value> >
    {
      using Type = TypeTuple<Head..., TailRest...>;
    };

    //!@internal Continue searching
    template<class T, class... Head, class Tail0, class... TailRest, template<CXX17_P0522R0(class A, class B)> class Unequal>
    struct RemoveMatchingHelper<
      T, TypeTuple<Head...>, TypeTuple<Tail0, TailRest...>, Unequal,
      std::enable_if_t<Unequal<T, Tail0>::value> >
    {
      using Type = typename RemoveMatchingHelper<T, TypeTuple<Head..., Tail0>, TypeTuple<TailRest...>, Unequal>::Type;
    };

    //!@internal Endpoint, input exhausted.
    template<class T, class Head, template<CXX17_P0522R0(class A, class B)> class Unequal>
    struct RemoveMatchingHelper<T, Head, TypeTuple<>, Unequal>
    {
      using Type = Head;
    };

#else
    // variant requiring sorted tuples.

    template<class T, class Head, class Tail, template<CXX17_P0522R0(class A, class B)> class Unequal, template<CXX17_P0522R0(class A, class B)> class Swap, class SFINAE = void>
    struct RemoveMatchingHelper;

    template<class T, class... Head, class Tail0, class... TailRest, template<CXX17_P0522R0(class A, class B)> class Unequal, template<CXX17_P0522R0(class A, class B)> class Swap>
    struct RemoveMatchingHelper<T, TypeTuple<Head...>, TypeTuple<Tail0, TailRest...>, Unequal, Swap, std::enable_if_t<!Unequal<T, Tail0>::value> >
    {
      using Type = TypeTuple<Head..., TailRest...>;
    };

    //!@internal Terminate when Tail0 is "larger" than T, assuming the list is sorted.
    template<class T, class... Head, class Tail0, class... TailRest, template<CXX17_P0522R0(class A, class B)> class Unequal, template<CXX17_P0522R0(class A, class B)> class Swap>
    struct RemoveMatchingHelper<T, TypeTuple<Head...>, TypeTuple<Tail0, TailRest...>, Unequal, Swap,
                                std::enable_if_t<Unequal<T, Tail0>::value && Swap<Tail0, T>::value> >
    {
      using Type = TypeTuple<Head..., Tail0, TailRest...>;
    };

    //!@internal Continue searching
    template<class T, class... Head, class Tail0, class... TailRest, template<CXX17_P0522R0(class A, class B)> class Unequal, template<CXX17_P0522R0(class A, class B)> class Swap>
    struct RemoveMatchingHelper<T, TypeTuple<Head...>, TypeTuple<Tail0, TailRest...>, Unequal, Swap,
                                std::enable_if_t<Unequal<T, Tail0>::value && !Swap<Tail0, T>::value> >
    {
      using Type = typename RemoveMatchingHelper<T, TypeTuple<Head..., Tail0>, TypeTuple<TailRest...>, Unequal, Swap>::Type;
    };

    //!@internal Endpoint, input exhausted.
    template<class T, class Head, template<CXX17_P0522R0(class A, class B)> class Unequal, template<CXX17_P0522R0(class A, class B)> class Swap>
    struct RemoveMatchingHelper<T, Head, TypeTuple<>, Unequal, Swap>
    {
      using Type = Head;
    };
#endif

    template<class T, class Tuple, template<CXX17_P0522R0(class A, class B)> class Unequal>
    struct HasMatchingTypeHelper;

    template<class T, class... Ts, template<CXX17_P0522R0(class A, class B)> class Unequal>
    struct HasMatchingTypeHelper<T, TypeTuple<Ts...>, Unequal>
      : BoolConstant<(... || !Unequal<T, Ts>::value)>
    {};

  }

  /**Generate a new TypeTuple from @a Tuple by removing the first
   * instance of @a T. The search optionally stops when @a T becomes
   * "smaller" than the current element according to the predicate @a
   * Swap which could speed-up the search for sorted lists.
   */
  template<class T, class Tuple, template<CXX17_P0522R0(class A, class B)> class Unequal, template<CXX17_P0522R0(class A, class B)> class Swap = AlwaysFalse>
  using RemoveFirstMatching = typename RemoveMatchingHelper<T, TypeTuple<>, Tuple, Unequal, Swap>::Type;

  template<class T, class Tuple, template<CXX17_P0522R0(class A, class B)> class Unequal>
  constexpr inline bool HasMatchingTypeV = HasMatchingTypeHelper<T, Tuple, Unequal>::value;

}

#endif // __DUNE_ACFEM_MPL_TYPETUPLEUNIQ_HH__
