/**@file
 *
 * By intention there is no header guard. Use this file to suck in the
 * declarations into a namespace if necessary.
 */

using ::Dune::ACFem::operator==;
using ::Dune::ACFem::operator!=;
using ::Dune::ACFem::operator<;
using ::Dune::ACFem::operator<=;
using ::Dune::ACFem::operator>;
using ::Dune::ACFem::operator>=;
