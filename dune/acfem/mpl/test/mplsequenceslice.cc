#include <iostream>
#include <tuple>

#include "../generators.hh"
#include "../transform.hh"
#include "../compare.hh"
#include "../sequenceslice.hh"
#include "../../common/ostream.hh" // for typePrint()

using namespace Dune::ACFem;

template<std::size_t... I>
using Seq = IndexSequence<I...>;

int main(int argc, char *argv[])
{
  {
    using InputSequence = MakeIndexSequence<10>;
    using SliceSequence = IndexSequence<0, 3, 4, 1, 7, 6>;

    std::clog << "Input: " << InputSequence{} << std::endl;
    std::clog << "Slice indices: " << SliceSequence{} << std::endl;
    std::clog << "Slice:" << MPL::SequenceSlice<InputSequence, SliceSequence>{} << std::endl;
    std::clog << "Complement:" << MPL::SequenceSliceComplement<InputSequence, SliceSequence>{} << std::endl;
    std::clog << std::endl;
  }

  {
    using InputSequence = IndexSequence<1, 2>;
    using SliceSequence = IndexSequence<1, 0>;

    std::clog << "Input: " << InputSequence{} << std::endl;
    std::clog << "Slice indices: " << SliceSequence{} << std::endl;
    std::clog << "Slice:" << MPL::SequenceSlice<InputSequence, SliceSequence>{} << std::endl;
    std::clog << "Complement:" << MPL::SequenceSliceComplement<InputSequence, SliceSequence>{} << std::endl;
    std::clog << std::endl;
  }
  return 0;
}
