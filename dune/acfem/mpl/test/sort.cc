/** INTENTIONALLY, THIS FILE IS UNDOCUMENTED. FEEL FREE TO PLAY AROUND. */

#include <iostream>
#include <tuple>

#include "../generators.hh"
#include "../transform.hh"
#include "../compare.hh"
#include "../../common/ostream.hh" // for typePrint()

using namespace Dune::ACFem;

template<std::size_t I0, std::size_t I1>
using Decreasing = BoolConstant<(I0 < I1)>;

int main(int argc, char *argv[])
{
  using Seq = IndexSequence<3, 2, 4, 0, 1>;

  std::clog << "Ordinary sort of " << Seq{} << std::endl;
  std::clog << "  " << sort(Seq{}) << std::endl;

  std::clog << "Iverted sort of " << Seq{} << std::endl;
  std::clog << "  " << sort<Decreasing>(Seq{}) << std::endl;

  std::clog << "IsSorted for empty sequence" << std::endl;
  std::clog << "  " << isSorted<Decreasing>(IndexSequence<>{}) << std::endl;

  return 0;
}
