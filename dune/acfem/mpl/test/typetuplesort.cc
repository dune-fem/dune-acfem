#include <iostream>
#include <tuple>
#include <array>

#include "../../common/types.hh"
#include "../../common/ostream.hh"
#include "../typetuplesort.hh"

using namespace Dune::ACFem;
using namespace Dune::ACFem::MPL;

template<class A, class B>
using Swap = BoolConstant<(A::value > B::value)>;

int main(int argc, char *argv[])
{
  using InputList = TypeTuple<IntConstant<3>, IntConstant<-5>, IntConstant<34>, IntConstant<42>, IntConstant<100> >;

  std::clog << typeString((InputList *)nullptr) << std::endl;

  std::clog << typeString((MergeSort<InputList, Swap> *)nullptr) << std::endl;

  return EXIT_SUCCESS;
}
