#include <iostream>
#include <tuple>
#include <array>

#include "../generators.hh"
#include "../access.hh"
#include "../../common/ostream.hh" // for typePrint()

using namespace Dune::ACFem;

template<std::size_t... I>
using Seq = IndexSequence<I...>;

int main(int argc, char *argv[])
{
  std::clog << "SizeV variable template" << std::endl;
  std::clog << "void:                " << SizeV<void> << std::endl;
  std::clog << "10 element array:    " << SizeV<std::array<int, 10> > << std::endl;
  std::clog << " 3 element tuple:    " << SizeV<std::tuple<int, int, int> > << std::endl;
  std::clog << "pair:                " << SizeV<std::pair<int, int> > << std::endl;
  std::clog << "integral constant:   " << SizeV<Constant<int, 10> > << std::endl;
  std::clog << "10 element sequence: " << SizeV<MakeIndexSequence<10> > << std::endl;

  std::clog << "size<T>() template" << std::endl;
//  std::clog << "void:                " << size<void>() << std::endl;
  std::clog << "10 element array:    " << size<std::array<int, 10> >() << std::endl;
  std::clog << " 3 element tuple:    " << size<std::tuple<int, int, int> >() << std::endl;
  std::clog << "pair:                " << size<std::pair<int, int> >() << std::endl;
  std::clog << "integral constant:   " << size<Constant<int, 10> >() << std::endl;
  std::clog << "10 element sequence: " << size<MakeIndexSequence<10> >() << std::endl;

  return 0;
}
