#include <iostream>
#include <tuple>
#include <array>

#include "../../common/types.hh"
#include "../../common/ostream.hh"
#include "../uniquetags.hh"

using namespace Dune::ACFem;

struct Tag1
{};

struct Tag2
  : Tag1
{};

struct Tag3
  : Tag1
{};

struct Tag4
  : MPL::UniqueTags<Tag1, Tag2, Tag3>
{};

#if 0
  : MPL::UniqueTags<void, Tag1, Tag2, Tag3>
{};
#endif

template<std::size_t... I>
using Seq = IndexSequence<I...>;

int main(int argc, char *argv[])
{
  std::clog << typeString(MPL::RemoveImplied<Tag1, Tag2, Tag3, Tag4>{}) << std::endl;

  std::clog << "T4 is a T1: " << std::is_base_of<Tag1, Tag4>::value << std::endl;
  std::clog << "T4 is a T2: " << std::is_base_of<Tag2, Tag4>::value << std::endl;
  std::clog << "T4 is a T3: " << std::is_base_of<Tag3, Tag4>::value << std::endl;

//  auto blah = static_cast<Tag1>(Tag4{});

  return EXIT_SUCCESS;
}
