#include <config.h>

#include <iostream>
#include <functional>
#include <bitset>
#include <dune/common/fvector.hh>

#include "../mpl.hh"
#include "../../common/ostream.hh"

using namespace Dune::ACFem;

namespace detail {
  template <class F, class Tuple, std::size_t... I>
  constexpr decltype(auto) method_impl(F&& f, Tuple&& t, std::index_sequence<I...>)
  {
    //return std::invoke(std::forward<F>(f), std::get<I>(std::forward<Tuple>(t))...);
    return f.method(std::get<I>(std::forward<Tuple>(t))...);
  }
}  // namespace detail

template <class F, class Tuple>
constexpr decltype(auto) method(F&& f, Tuple&& t)
{
  return detail::method_impl(
    std::forward<F>(f), std::forward<Tuple>(t),
    std::make_index_sequence<std::tuple_size<std::decay_t<Tuple> >::value>{});
}

typedef Dune::FieldVector<double, 3> DomainType;
typedef Dune::FieldVector<double, 4> RangeType;

class A
{
 public:
  template<class Point>
  inline __attribute__((__always_inline__)) int method(const double&, const Point&) const
  {
#ifdef __GNUC__
    std::cout << __PRETTY_FUNCTION__ << std::endl;
#endif
    return 0;
  }
};

int subFunction(const double& a0, const DomainType& a1)
{
  return 0;
}

template<class A, class Point>
inline int superFunction(const A& a, const size_t& a0, const double& a1, const Point& a2, const RangeType& a3)
{
    return method(a, maskTuple<0x6>(std::forward_as_tuple(a0, a1, a2, a3)));
    //return std::apply(&A::template method<Point>, maskTuple<0xD>(std::forward_as_tuple(a, a0, a1, a2, a3)));
//  return std::apply(subFunction, std::forward_as_tuple(a2, a3));
//  prettyPrint(maskTuple<0x6>(std::forward_as_tuple(a0, a1, a2, a3)));
//  return 0;
}

enum ArgumentPositions {
  linearizedValueBit, linearizedJacobianBit,
  pointBit, normalBit, valueBit, jacobianBit, hessianBit
};

template<class TupleLike, class Seq>
class MethodDefiner;

template<class TupleLike, std::size_t... I>
class MethodDefiner<TupleLike, std::index_sequence<I...> >
{
 protected:
  auto method(decltype(std::get<I>(std::declval<TupleLike>()))&&...)
  {
    return std::string("Hello World!");
  }
};

class MethodUser
  : protected MethodDefiner<std::tuple<DomainType, DomainType, RangeType, RangeType>, std::index_sequence<3, 0> >
{
  typedef MethodDefiner<std::tuple<DomainType, DomainType, RangeType, RangeType>, std::index_sequence<3, 0> > BaseType;
 public:
  using BaseType::method;
};

template<std::size_t tag>
class Enumerated
{};

template<>
class Enumerated<0>
{};

template<>
class Enumerated<1>
{};

template<class Seq>
class MultiChild;

template<std::size_t... Ints>
class MultiChild<std::index_sequence<Ints...> >
  : public Enumerated<Ints>...
{};

namespace Fancy {

  namespace {

    struct Test
    {
      /**Transform to bool sequence of even indices.*/
      template<bool result>
      struct MyIsOddEvenFunctor
      {
        template<class T, T V, std::size_t>
        using Apply = BoolConstant<((V & 1) == 0) == result>;
      };

      template<class Seq, bool result>
      using EvenSeq = FilteredSequence<MyIsOddEvenFunctor<result>, Seq>;
    };

  }

  template<class Seq, bool result>
  using EvenSeq = Test::EvenSeq<Seq, result>;
}

template<class Seq, bool result>
using EvenSeq = Fancy::EvenSeq<Seq, result>;

struct TransformAtIndexTestFunctor
{
  template<class T, T V, std::size_t N>
  using Apply = IndexConstant<N>;
};

int main(int argc, char *argvp[])
{
  std::clog << "index: " << IndexIn<IndexSequence<2, 3, 4, 5, 6>, 5>::value << std::endl;
  std::clog << "index: " << IndexIn<IndexSequence<2, 3, 4, 5, 6>, 10>::value << std::endl;
  std::clog << "index: " << TypeString<IndexSequence<2, 3, 4, 5, 6> >{} << std::endl;
  std::clog << "index: " << TypeString<TransformOnly<TransformAtIndexTestFunctor,
                                                     IndexSequence<2, 3, 4, 5, 6>,
                                                     2, 4, 5> >{} << std::endl;

  typedef std::tuple<int, float, double> TupleType;

  std::clog << "Get<2, " << TypeString<TupleType>{} << " >: " << TypeString<TupleElement<2, TupleType> >{} << std::endl;

  MethodUser methodUser;

  MultiChild<std::index_sequence<0, 1> > multiChild;
  std::clog << typeString<decltype(multiChild)>() << std::endl;

  std::clog << methodUser.method(RangeType{}, DomainType{}) << std::endl;

  superFunction(A(), 1, 3., DomainType(), RangeType(1.0));

  auto testTuple = std::make_tuple(0, 1, 2, 3, 4);

  {
    const std::size_t mask = 0xFF00UL;
    std::clog << "Mask:  " << makeBitset(mask) << std::endl;
    std::clog << "Most:  " << FindMostSignificantBit<mask>::value << std::endl;
    std::clog << "Check: " << ((1UL << FindMostSignificantBit<mask>::value) <= mask && (1UL << (1+FindMostSignificantBit<mask>::value)) > mask) << std::endl;
  }

  {
    const std::size_t mask = 0xFF00UL;
    std::clog << "Mask:  " << makeBitset(mask) << std::endl;
    std::clog << "Least: " << FindLeastSignificantBit<mask>::value << std::endl;
    std::clog << "Check: " << ((1UL << FindLeastSignificantBit<mask>::value) <= mask && (1UL << (FindLeastSignificantBit<mask>::value-1)) < mask) << std::endl;
  }

  {
    const std::size_t mask = 0xFFUL;
    std::clog << "Mask:  " << makeBitset(mask) << std::endl;
    std::clog << "FFZ:   " << FindFirstZeroBit<mask>::value << std::endl;
  }

  {
    std::clog << "Bool sequence \"if even\": "
              << TypeString<TransformedSequence<IsEvenFunctor, std::make_index_sequence<13> > >{}
              << std::endl;
    std::clog << "Sequence of evens: "
              << TypeString<EvenSeq<std::make_index_sequence<13>, true> >{}
              << std::endl;
    std::clog << "Evens AND: "
              << AccumulateSequence<LogicalAndFunctor, TransformedSequence<IsEvenFunctor, std::make_index_sequence<13> > >::value
              << std::endl;
    std::clog << "Evens OR: "
              << AccumulateSequence<LogicalOrFunctor, TransformedSequence<IsEvenFunctor, std::make_index_sequence<13> > >::value
              << std::endl;
  }

  {
    typedef IndexSequence<SequenceMask<0, 7>::value, SequenceMask<0, 4>::value, SequenceMask<5, 6, 7>::value> TestSeq;

    std::clog << "Bitwise OR Sequence: " << TypeString<TestSeq>{} << std::endl;
    std::clog << "Bitwise OR: "
              << makeBitset(AccumulateSequence<BitwiseOrFunctor, TestSeq>::value)
              << std::endl;
    std::clog << "Bitwise AND: "
              << makeBitset(AccumulateSequence<BitwiseAndFunctor, TestSeq>::value)
              << std::endl;
  }

  const auto super = SequenceMask<pointBit, jacobianBit, hessianBit, linearizedValueBit>::value;
  //const auto sub   = SequenceMask<jacobianBit, valueBit>::value; should fail
  const auto sub   = SequenceMask<jacobianBit, pointBit>::value;

  std::clog << "Super:     " << makeBitset(super) << std::endl;
  std::clog << "Sub:       " << makeBitset(sub) << std::endl;
  std::clog << "Condensed: " << makeBitset(CondensedMask<super, sub>::value) << std::endl;
  std::clog << "Condensed: " << makeBitset(CondensedMask<super, super>::value) << std::endl;

  std::clog << testTuple << std::endl;
  std::clog << sequenceMask(std::index_sequence<1,12>{}) << std::endl;

  std::clog << makeBitset(0x6UL) << std::endl;
  std::clog << makeBitset(SequenceMask<pointBit, valueBit>::value) << std::endl;

  std::clog << "SequenceMask: "
            << TypeString<SequenceMask<pointBit, valueBit, jacobianBit> >()
            << std::endl;
  std::clog << "SubMaskSequence: "
            << TypeString<SubMaskSequence<SequenceMask<pointBit, valueBit, jacobianBit>::value> >()
            << std::endl;
  typedef std::index_sequence<0, 32, 16, 48, 4, 36, 20, 52> TestSeq;
  std::clog << "TestSeq: " << TypeString<TestSeq>{} << std::endl;


  std::clog << "TransformedSequence<AddBitShiftRight...:   "
            << TypeString<TransformedSequence<AddBitShiftRightFunctor<5, 2>, TestSeq> >()
            << std::endl;
  std::clog << "CatTransformedSequence<AddBitShiftRight...:"
            << TypeString<CatTransformedSequence<AddBitShiftRightFunctor<5, 2>, TestSeq> >()
            << std::endl;
  std::clog << "TransformUnique<AddBitShiftRight...:     "
            << TypeString<TransformUnique<AddBitShiftRightFunctor<5, 2>, TestSeq> >()
            << std::endl;
  std::clog << "SequenceCat<...:                         "
            << TypeString<CatSequence<TestSeq, std::index_sequence<42, 42> > >()
            << std::endl;

  typedef SubMaskSequence<SequenceMask<2, 3, 4, 5>::value> Test2Seq;

  typedef CatTransformUnique<AddBitShiftRightFunctor<4, 4>,
                             CatTransformUnique<AddBitShiftRightFunctor<5, 4>,
                                                Test2Seq> > TransSeq;

  std::clog << "CatTransformUnique<AddBitShiftRight...:  "
            << TypeString<TransSeq>()
            << std::endl;

  std::clog << "Orig Sequence bits: "
            << std::endl
            << std::hex
            << std::showbase
            << Test2Seq()
            << std::endl;

  std::clog << "Trans Sequence bits: "
            << std::endl
            << TransSeq()
            << std::endl;

  {

  }

  return 0;
}
