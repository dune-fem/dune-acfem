
#include <iostream>
#include <tuple>

#include "../generators.hh"
#include "../transform.hh"
#include "../compare.hh"
#include "../filter.hh"
#include "../../common/ostream.hh" // for typePrint()

using namespace Dune::ACFem;

template<std::size_t... I>
using Seq = IndexSequence<I...>;

int main(int argc, char *argv[])
{
  // (<2,2,2> *[1][2] <2,2,2>) *[1][2] <2,2,2?
  //using Signature = Seq<2,2,2>;
  using InnerSignature = SequenceCat<SequenceSliceComplement<Seq<2,2,2>, Seq<1> >,
                                     SequenceSliceComplement<Seq<2,2,2>, Seq<2> > >;
  using OuterSignature = SequenceCat<SequenceSliceComplement<Seq<2,2,2,2>, Seq<0> >,
                                     SequenceSliceComplement<Seq<2,2,2>, Seq<2> > >;

  std::clog << "Inner *[0][2]: " << InnerSignature{} << std::endl;
  std::clog << "Outer *[1][2]: " << OuterSignature{} << std::endl;

  using Empty = SequenceSliceComplement<Seq<>, Seq<> >;

  std::clog << "Empty: " << Empty{} << std::endl;
  std::clog << "Empty2: " << SequenceSliceComplement<Seq<2,2,2>, Seq<> >{} << std::endl;

  std::clog << "Wrong order: " << SequenceSliceComplement<Seq<2,2,2>, Seq<1, 0> >{} << std::endl;

  return 0;
}
