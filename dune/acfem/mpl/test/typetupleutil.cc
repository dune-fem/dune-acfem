#include <iostream>
#include <tuple>
#include <array>

#include "../../common/types.hh"
#include "../../common/ostream.hh"
#include "../typetupleutil.hh"

using namespace Dune::ACFem;
using namespace Dune::ACFem::MPL;

int main(int argc, char *argv[])
{
  std::clog << "Should yield TypeTuple<int>" << std::endl;
  std::clog << typeString(AddFrontIf<std::is_same, TypeTuple<int>, char>{}) << std::endl;

  std::clog << "Should yield TypeTuple<int, int>" << std::endl;
  std::clog << typeString(AddFrontIf<std::is_same, TypeTuple<int>, int>{}) << std::endl;

  std::clog << "Should yield TypeTuple<char, int>" << std::endl;
  std::clog << typeString(AddFrontUnless<std::is_same, TypeTuple<int>, char>{}) << std::endl;

  std::clog << "Should yield TypeTuple<int>" << std::endl;
  std::clog << typeString(AddFrontUnless<std::is_same, TypeTuple<int>, int>{}) << std::endl;

  std::clog << "Should yield TypeTuple<int, int, long, int>" << std::endl;
  std::clog << typeString(JoinIf<std::is_same, TypeTuple<int, int, long>, TypeTuple<int, char> >{}) << std::endl;

  std::clog << "Should yield TypeTuple<int, int, long, char>" << std::endl;
  std::clog << typeString(JoinUnless<std::is_same, TypeTuple<int, int, long>, TypeTuple<int, char> >{}) << std::endl;

  std::clog << "Should yield TypeTuple<int...> with 10 ints." << std::endl;
  std::clog << typeString(FlattenTypeTuple<TypeTuple<int, TypeTuple<int, int, TypeTuple<int, int>, int>, TypeTuple<int, int, int>, TypeTuple<>, int> >{}) << std::endl;

  return EXIT_SUCCESS;
}
