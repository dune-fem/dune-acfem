/** INTENTIONALLY, THIS FILE IS UNDOCUMENTED. FEEL FREE TO PLAY AROUND. */

#include <iostream>
#include <tuple>

#include "../generators.hh"
#include "../../common/ostream.hh" // for typePrint()

using namespace Dune::ACFem;

int main(int argc, char *argv[])
{
  std::clog << "Index Sequence with 10 elements." << std::endl;
  std::clog << typeString<MakeIndexSequence<10> >() << std::endl;

  std::clog << "10 odd numbers starting at 3." << std::endl;
  std::clog << typeString<MakeIndexSequence<15, 3, 2> >() << std::endl;

  std::clog << "Sequence starting at 10." << std::endl;
  std::clog << typeString<MakeIndexSequence<10, 10> >() << std::endl;

  std::clog << "Constant sequence." << std::endl;
  std::clog << typeString<MakeConstantSequence<10, 42> >() << std::endl;

  std::clog << "Reverse Sequence with 10 elements." << std::endl;
  std::clog << typeString<MakeReverseSequence<10> >() << std::endl;

  return 0;
}
