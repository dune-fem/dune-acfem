#include "../../common/types.hh"
#include "../../common/ostream.hh"
#include "../multiindex.hh"

using namespace Dune::ACFem;

template<class Dims, std::size_t N, std::size_t... I>
constexpr auto testExpander(Dims, const std::array<std::size_t, N>& indices, IndexSequence<I...>)
{
  return flattenMultiIndex(Dims{}, get<I>(indices)...);
}

int main(int argc, char *argv[])
{
  using Signature = IndexSequence<3,4,5,6>;
  constexpr auto signature = Signature{};

  // Compile time constant
  forLoop<multiDim(signature)>([&](auto i) {
      using I = decltype(i);
      constexpr auto index = multiIndex<I::value>(signature);
      constexpr std::size_t flatIndex = flattenMultiIndex(signature, index);
      std::clog << "Index:       " << I::value << std::endl;
      std::clog << "Multi-Index: " << index << std::endl;
      std::clog << "Flat-Index:  " << flatIndex << std::endl;
      std::clog << std::endl;
      assert(flatIndex == I::value);
    });

  // Run-time dynamic
  forLoop<multiDim(signature)>([&](auto i) {
      using I = decltype(i);
      auto index = multiIndex(I::value, signature);
      std::size_t flatIndex = flattenMultiIndex(signature, index);
      std::clog << "Index:       " << I::value << std::endl;
      std::clog << "Multi-Index: " << index << std::endl;
      std::clog << "Flat-Index:  " << flatIndex << std::endl;
      std::clog << std::endl;
      assert(flatIndex == I::value);
    });

  // Run-time dynamic
  forLoop<multiDim(signature)>([&](auto i) {
      using I = decltype(i);
      auto index = multiIndex(I::value, signature);
      std::size_t flatIndex = testExpander(signature, index, MakeSequenceFor<Signature>{});
      std::clog << "Index:       " << I::value << std::endl;
      std::clog << "Multi-Index: " << index << std::endl;
      std::clog << MakeSequenceFor<Signature>{} << std::endl;
      std::clog << "Flat-Index:  " << flatIndex << std::endl;
      std::clog << std::endl;
      assert(flatIndex == I::value);
    });

  return EXIT_SUCCESS;
}
