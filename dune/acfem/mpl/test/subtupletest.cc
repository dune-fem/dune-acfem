/** INTENTIONALLY, THIS FILE IS UNDOCUMENTED. FEEL FREE TO PLAY AROUND. */

#include <iostream>
#include <tuple>

#include "../mpl.hh"
#include "../../common/ostream.hh" // for typePrint()

using Dune::ACFem::operator<<;
using Dune::ACFem::subTuple;
using Dune::ACFem::insertAt;

int main(int argc, char *argv[])
{
  // src-tuple
  auto myTuple = std::make_tuple(100UL, 101UL, 102UL, 104UL);

  // try to insert 99 at position 0
  std::clog << "Insert 99 at position 0 into " << myTuple << std::endl;
  std::clog << insertAt<0>(myTuple, std::make_tuple(99)) << std::endl;

  // try to insert 103 at position 3
  std::clog << "Insert l03 at position 3 into " << myTuple << std::endl;
  std::clog << insertAt<3>(myTuple, 103) << std::endl;

  // Multiple insertions
  std::clog << "Insert (200, 201) at positions 2 and 4 into " << myTuple << std::endl;
  std::clog << insertAt<2, 4>(myTuple, std::array<int, 2>({{200, 201}})) << std::endl;

  // Form a sub-tuple
  std::clog << "Subtuple at pos. 1 and 3 from " << myTuple << std::endl;
  std::clog << subTuple<1, 3>(myTuple) << std::endl;

  // Form a sub-tuple
  std::clog << "Subtuple at pos. seq(1, 3) from " << myTuple << std::endl;
  std::clog << subTuple(myTuple, std::index_sequence<1, 3>{}) << std::endl;

  return 0;
}
