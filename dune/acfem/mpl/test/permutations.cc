/** INTENTIONALLY, THIS FILE IS UNDOCUMENTED. FEEL FREE TO PLAY AROUND. */

#include <iostream>
#include <tuple>

#include "../generators.hh"
#include "../transform.hh"
#include "../compare.hh"
#include "../../common/ostream.hh" // for typePrint()

using namespace Dune::ACFem;

template<std::size_t... I>
using S = IndexSequence<I...>;

int main(int argc, char *argv[])
{
  using Perm = IndexSequence<3, 2, 4, 0, 1>;
  using InvPerm = InversePermutation<Perm>;

  std::clog << "Index Sequence with 5 elements." << std::endl;
  std::clog << typeString<Perm>() << std::endl;
  std::clog << "Is a permuation: " << isPermutation(Perm{}) << std::endl;
  std::clog << "Its inverse: " << std::endl;
  std::clog << typeString<InvPerm>() << std::endl;
  std::clog << "Is a permuation: " << isPermutation(InvPerm{}) << std::endl;

  std::clog << "Transposition<0,1,7>: " << typeString<Transposition<0, 1, 7> >() << std::endl;
  std::clog << "Transposition<1,3,7>: " << typeString<Transposition<1, 3, 7> >() << std::endl;
  std::clog << "Transposition<2,6,7>: " << typeString<Transposition<2, 6, 7> >() << std::endl;
  std::clog << "Transposition<6,6,7>: " << typeString<Transposition<6, 6, 7> >() << std::endl;

  std::clog << "BlockTransposition<0,1,3,7>: " << typeString<BlockTransposition<S<0,1,2>, S<3,4,5>, 21> >() << std::endl;
  std::clog << "BlockTransposition<1,3,3,7>: " << typeString<BlockTransposition<S<3,4,5>, S<9,10,12>, 21> >() << std::endl;
  std::clog << "BlockTransposition<2,6,3,7>: " << typeString<BlockTransposition<S<6,7,8>, S<18,19,20>, 21> >() << std::endl;
  std::clog << "BlockTransposition<6,6,3,7>: " << typeString<BlockTransposition<S<18,19,20>, S<18,19,20>, 21> >() << std::endl;

  return 0;
}
