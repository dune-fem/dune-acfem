#ifndef __DUNE_ACFEM_MPL_SEQUENCESETOPERATIONS_HH__
#define __DUNE_ACFEM_MPL_SEQUENCESETOPERATIONS_HH__

#include "../common/types.hh"
#include "access.hh"
#include "transform.hh"

namespace Dune::ACFem {

  namespace MPL {

    namespace impl {

      template<class Input, class Other, class Output = Sequence<typename Input::value_type>, class SFINAE = void>
      struct SequenceSetMinus
      {
        using Type = Output;
      };

      // have match
      template<class T, T In0, T... In, class Other, class Output>
      struct SequenceSetMinus<Sequence<T, In0, In...>, Other, Output,
                              std::enable_if_t<ContainsValueV<Other, In0> > >
      {
        using Type = typename SequenceSetMinus<Sequence<T, In...>, Other, Output>::Type;
      };

      // no match
      template<class T, T In0, T... In, class Other, T... Output>
      struct SequenceSetMinus<Sequence<T, In0, In...>, Other, Sequence<T, Output...>,
                              std::enable_if_t<!ContainsValueV<Other, In0> > >
      {
        using Type = typename SequenceSetMinus<Sequence<T, In...>, Other, Sequence<T, Output..., In0> >::Type;
      };

      template<class Input, class Other, class Output = Sequence<typename Input::value_type>, class SFINAE = void>
      struct SequenceSetIntersection
      {
        using Type = Output;
      };

      // no match
      template<class T, T In0, T... In, class Other, class Output>
      struct SequenceSetIntersection<Sequence<T, In0, In...>, Other, Output,
                                     std::enable_if_t<!ContainsValueV<Other, In0> > >
      {
        using Type = typename SequenceSetIntersection<Sequence<T, In...>, Other, Output>::Type;
      };

      // have match
      template<class T, T In0, T... In, class Other, T... Output>
      struct SequenceSetIntersection<Sequence<T, In0, In...>, Other, Sequence<T, Output...>,
                              std::enable_if_t<ContainsValueV<Other, In0> > >
      {
        using Type = typename SequenceSetIntersection<Sequence<T, In...>, Other, Sequence<T, Output..., In0> >::Type;
      };

    }

    /**Generate the sequence with the indices from @a Seq not
     * contained in @a Other. The resulting sequence may contain
     * duplicate indices if @a Seq contains matching duplicates.
     */
    template<class Seq, class Other>
    using SequenceSetMinus = typename impl::SequenceSetMinus<Seq, Other>::Type;

    /**Generate the sequence with the indices from @a Seq also
     * contained in @a Other. The resulting sequence may contain
     * duplicate indices if @a Seq contains matching duplicates.
     */
    template<class Seq, class Other>
    using SequenceSetIntersection = typename impl::SequenceSetIntersection<Seq, Other>::Type;

    /**Generate the sequence with the elements from @a Seq0 and the
     * elements from @a Seq1 which are not already contained in @a
     * Seq0. Note that the resulting sequence will contain duplicate
     * elements iff @a Seq0 already contained duplicate indices.*/
    template<class Seq0, class Seq1>
    using SequenceSetUnion = SequenceCat<Seq0, SequenceSetMinus<Seq1, Seq0> >;

  } // MPL::

} // Dune::ACFem::

#endif // __DUNE_ACFEM_MPL_SEQUENCESETOPERATIONS_HH__
