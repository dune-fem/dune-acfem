#ifndef __DUNE_ACFEM_MPL_SUBMASK_HH__
#define __DUNE_ACFEM_MPL_SUBMASK_HH__

#include "transform.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup C++-Templates
     *
     * @{
     */

    /**@addtogroup TupleUtilities
     *
     * Some additional utitliy functions for tuple-likes and
     * integer-sequences.
     *
     * @{
     */

    /**@addtogroup SequenceMask
     *
     * @{
     */

    namespace {
      // Helpers for SubMaskSequence

      template<class Seq1, class Seq2, class Required>
      struct SubMaskSequenceHelper;

      template<std::size_t... I1, std::size_t Bit, std::size_t... I2, std::size_t... Required>
      struct SubMaskSequenceHelper<IndexSequence<I1...>,
                                   IndexSequence<Bit, I2...>,
                                   IndexSequence<Required...> >
      {
        typedef CatTransformedSequence<
          AddBitFunctor<Bit>,
          typename SubMaskSequenceHelper<IndexSequence<I1..., Bit>,
                                         IndexSequence<I2...>,
                                         IndexSequence<Required...> >::type>
        type;
      };

      template<std::size_t... I, std::size_t... Required>
      struct SubMaskSequenceHelper<IndexSequence<I...>,
                                   IndexSequence<>,
                                   IndexSequence<Required...> >
      {
        typedef IndexSequence<SequenceMask<Required...>::value> type;
      };
    }

    /**@copydoc SubMaskSequence.*/
    template<std::size_t mask>
    constexpr auto submaskSequence(IndexConstant<mask>&&)
    {
      return typename SubMaskSequenceHelper<IndexSequence<>, MaskSequence<mask>, IndexSequence<> >::type{};
    }

    /**@copydoc SubMaskSequence.*/
    template<std::size_t... I>
    constexpr auto submaskSequence(IndexSequence<I...>&&)
    {
      return typename SubMaskSequenceHelper<IndexSequence<>,
                                            IndexSequence<I...>,
                                            IndexSequence<> >::type{};
    }

    /**Generate a sequence of pair-wise distinct sub-bitmasks starting
     * from a given bit-mask. The generated sequence of bitmask is
     * "ORed" with the value specified by RequiredMask.
     */
    template<std::size_t Mask, std::size_t RequiredMask = 0UL>
    using SubMaskSequence = typename SubMaskSequenceHelper<IndexSequence<>,
                                                           MaskSequence<Mask>,
                                                           MaskSequence<RequiredMask> >::type;

    //!@} SequenceMask

    //!@} TupleUtilities

    //!@} C++-Templates

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_MPL_SUBMASK_HH__
