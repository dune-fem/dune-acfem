#ifndef __DUNE_ACFEM_MPL_UNIQUETAGS_HH__
#define __DUNE_ACFEM_MPL_UNIQUETAGS_HH__

#include "typetuple.hh"

namespace Dune::ACFem::MPL {

  template<class... Tags>
  struct TagContainer
    : TypeTuple<Tags...>
    , Tags...
  {
    using TagTuple = TagContainer;
  };

  template<class Input, class Output>
  struct RemoveImpliedType;

  template<class... In0, class... In, class... Out>
  struct RemoveImpliedType<TypeTuple<TagContainer<In0...>, In...>, TypeTuple<Out...> >
    : RemoveImpliedType<TypeTuple<In0..., In...>, TypeTuple<Out...> >
  {};

  template<class In0, class... In, class... Out>
  struct RemoveImpliedType<TypeTuple<In0, In...>, TypeTuple<Out...> >
    : RemoveImpliedType<TypeTuple<In...>, ConditionalType<(std::is_same<In0, void>::value
                                                           || (... || (std::is_base_of<In0, Out>::value))
                                                           || (... || (std::is_base_of<In0, In>::value))
                                                          ),
                                                          TypeTuple<Out...>,
                                                          TypeTuple<Out..., In0> > >
  {};

  template<class... Out>
  struct RemoveImpliedType<TypeTuple<>, TypeTuple<Out...> >
  {
    using Type = TypeTuple<Out...>;
  };

  /**Remove types which are base classes of other types in the list.*/
  template<class... Tags>
  using RemoveImplied = typename RemoveImpliedType<TypeTuple<Tags...>, TypeTuple<> >::Type;

  template<class Tuple>
  struct MakeTagContainer;

  template<class... Tags>
  struct MakeTagContainer<TypeTuple<Tags...>  >
  {
    using Type = TagContainer<Tags...>;
  };

  template<class... Tags>
  using UniqueTags = typename MakeTagContainer<RemoveImplied<Tags...> >::Type;

}

#endif // __DUNE_ACFEM_MPL_UNIQUETAGS_HH__
