#ifndef __DUNE_ACFEM_MPL_MASK_HH__
#define __DUNE_ACFEM_MPL_MASK_HH__

#include "access.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup C++-Templates
     *
     * @{
     */

    /**@addtogroup TupleUtilities
     *
     * Some additional utitliy functions for tuple-likes and
     * integer-sequences.
     *
     * @{
     */

    /**@addtogroup SequenceMask
     *
     * Bit-mask and strictly monotone sequences and conversion utilities.
     *
     * @{
     */

    namespace {
      // helper templates in order to convert a bit-mask in into the
      // sequence of set-bit positions.

      template<std::size_t mask, std::size_t N, class = void, std::size_t... I>
      struct MaskSequenceHelper;

      template<std::size_t mask, std::size_t N, std::size_t... I>
      struct MaskSequenceHelper<mask, N, std::enable_if_t<mask & 1>, I...>
      {
        typedef typename MaskSequenceHelper<(mask/2), N+1, void, I..., N>::type type;
      };

      template<std::size_t mask, std::size_t N, std::size_t... I>
      struct MaskSequenceHelper<mask, N, std::enable_if_t<mask != 0 && !(mask & 1)>, I...>
      {
        typedef typename MaskSequenceHelper<(mask/2), N+1, void, I...>::type type;
      };

      // recursion end-point
      template<std::size_t mask, std::size_t N, std::size_t... I>
      struct MaskSequenceHelper<mask, N, std::enable_if_t<mask == 0>, I...>
      {
        typedef std::index_sequence<I...> type;
      };

      template<std::size_t... I>
      struct SequenceMaskHelper;

      template<std::size_t I0, std::size_t... I>
      struct SequenceMaskHelper<I0, I...>
      {
        static constexpr std::size_t value = (1UL << I0)|SequenceMaskHelper<I...>::value;
      };

      template<>
      struct SequenceMaskHelper<>
      {
        static constexpr std::size_t value = 0UL;
      };
    }

    /**Generate the index-sequence type of set bits from a bit-mask. */
    template<std::size_t mask>
    using MaskSequence = typename MaskSequenceHelper<mask, 0>::type;

    /**Generate a bit-mask from the given index-sequence.*/
    template<std::size_t... I>
    using SequenceMask = IndexConstant<SequenceMaskHelper<I...>::value>;

    /**Generate a bit-mask from the given index-sequence.*/
    template<std::size_t... I>
    constexpr auto sequenceMask(const IndexSequence<I...>&)
    {
      return SequenceMask<I...>::value;
    }

    /**std::true_type if Mask has Bit set.*/
    template<std::size_t Mask, std::size_t Bit>
    using HasBit = BoolConstant<(Mask & SequenceMask<Bit>::value) != 0UL>;

    /**@copydoc HasBit*/
    constexpr bool hasBit(const std::size_t& mask, const std::size_t& bit)
    {
      return mask & (1 << bit);
    }

    namespace {
      template<class Sup, class Sub, class = void>
      struct CondenseMaskHelper;

      template<class T, T Sup, T Sub>
      struct CondenseMaskHelper<Constant<T, Sup>, Constant<T, Sub>, std::enable_if_t<Sup & 1> >
      {
        static constexpr T value = (Sub & 1) | CondenseMaskHelper<Constant<T, (Sup >> 1)>, Constant<T, (Sub >> 1)> >::value << 1;
      };

      template<class T, T Sup, T Sub>
      struct CondenseMaskHelper<Constant<T, Sup>, Constant<T, Sub>, std::enable_if_t<(Sup > 0) && !(Sup & 1)> >
      {
        static_assert(!(Sub & 1), "Sub-mask not sub-ordinate to super-mask");
        static constexpr T value = CondenseMaskHelper<Constant<T, (Sup >> 1)>, Constant<T, (Sub >> 1)> >::value;
      };

      template<class T, T Sub>
      struct CondenseMaskHelper<Constant<T, 0>, Constant<T, Sub> >
      {
        static_assert(Sub == 0, "Sub-mask not sub-ordinate to super-mask");
        static constexpr T value = (T)0;
      };
    }

    /**Transform the bit-mask given in @a Sub be removing all
     * bit-positions of zeros in @a Super. @a Sub is required to also
     * only contain zeros at the removed positions.
     *
     * This is, e.g., used in the model-code in order to compute
     * argument lists for methods with deliberately ommitted
     * parameters.
     */
    template<std::size_t Super, std::size_t Sub>
    using CondensedMask = IndexConstant<CondenseMaskHelper<IndexConstant<Super>, IndexConstant<Sub> >::value>;

    //!@} SequenceMask

    //!@} TupleUtilities

    //!@} C++-Templates

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_MPL_MASK_HH__
