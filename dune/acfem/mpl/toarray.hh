#ifndef __DUNE_ACFEM_MPL_TOARRAY_HH__
#define __DUNE_ACFEM_MPL_TOARRAY_HH__

#include "generators.hh"
#include "access.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup C++-Templates
     *
     * @{
     */

    /**@addtogroup TupleUtilities
     *
     * Some additional utitliy functions for tuple-likes and
     * integer-sequences.
     *
     * @{
     */

    namespace {
      template<class T, class Tuple, std::size_t... I>
      auto toArrayHelper(Tuple&& tuple, IndexSequence<I...>)
      {
        return std::array<T, sizeof...(I)>({{ (T)std::get<I>(std::forward<Tuple>(tuple))... }});
      }

      template<class Tuple, class Idx, class SFINAE = void>
      struct ScalarTupleTypePromotionHelper;

      template<class Tuple, std::size_t... Idx>
      struct ScalarTupleTypePromotionHelper<
        Tuple, IndexSequence<Idx...>,
        std::enable_if_t<IsTupleLike<Tuple>::value && sizeof...(Idx) != 0> >
      {
        using Type = std::decay_t<decltype((std::declval<TupleElement<0, Tuple> >() + ... + std::declval<TupleElement<Idx, Tuple> >()))>;
      };

      template<class T, T... Ts, std::size_t... Idx>
      struct ScalarTupleTypePromotionHelper<
        Sequence<T, Ts...>, IndexSequence<Idx...>,
        std::enable_if_t<sizeof...(Idx) != 0> >
      {
        using Type = T;
      };

    }

    /**Generate the type resulting from summing up all tuple elements.*/
    template<class Tuple>
    using TupleScalarPromotionType =
      typename ScalarTupleTypePromotionHelper<Tuple, MakeSequenceFor<Tuple> >::Type;

    /**@internal FalseType by default.*/
    template<class T, class SFINAE = void>
    struct CanPromoteTupleTypes
      : FalseType
    {};

    /**TrueType if the type of the tuple T can be promoted to another
     * type. Used by toArray() in order to automatically deduce a
     * suitable array type.
     */
    template<class T>
    struct CanPromoteTupleTypes<T, std::enable_if_t<sizeof(TupleScalarPromotionType<T>) >= 0> >
      : TrueType
    {};

    /**Convert a compile-time constant integer sequence to a
     * rutime-object with the same values.
     */
    template<class T, T... Ind>
    auto toArray(Sequence<T, Ind...>)
    {
      return std::array<T, sizeof...(Ind)>({{ Ind... }});
    }

    /**Convert tuples and pairs to arrays.*/
    template<class T, class Tuple, std::enable_if_t<IsTupleLike<Tuple>::value, int> = 0>
    auto toArray(Tuple&& t)
    {
      return toArrayHelper<T>(std::forward<Tuple>(t), MakeSequenceFor<Tuple>{});
    }

    /**Convert tuples and pairs to arrays.*/
    template<
      class Tuple,
      std::enable_if_t<(IsTupleLike<Tuple>::value
                        && CanPromoteTupleTypes<Tuple>::value
      ), int> = 0>
    auto toArray(Tuple&& t)
    {
      return toArray<TupleScalarPromotionType<Tuple> >(std::forward<Tuple>(t));
    }

    template<class T, std::enable_if_t<IsArray<T>::value, int> = 0>
    constexpr decltype(auto) toArray(T&& t)
    {
      return std::forward<T>(t);
    }

    //!@} TupleUtilities

    //!@} C++-Templates

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_MPL_TOARRAY_HH__
