#ifndef __DUNE_ACFEM_MPL_CONDITIONALJOIN_HH__
#define __DUNE_ACFEM_MPL_CONDITIONALJOIN_HH__

#include "compare.hh"

namespace Dune {

  namespace ACFem {

    namespace TupleUtilities {

      /**@addtogroup C++-Templates
       *
       * @{
       */

      /**@addtogroup TupleUtilities
       *
       * @{
       */

      /**Enforce kind of uniqueness. The effort, however, is linear in
       * the tuple size for adding a single element. F is instantiated
       * with the types of the elements of t as first argument and the
       * type E as second argument.
       */
      template<template<class...> class F, class Tuple, class E,
               std::enable_if_t<(IsTupleLike<Tuple>::value
                                 && PredicateMatch<Tuple, PredicateProxy<F, E>::template ForwardFirst>::value
                 ), int> = 0>
      constexpr auto pushBackIf(Tuple&& t, E&& e, PredicateWrapper<F> = PredicateWrapper<F>{})
      {
        return std::tuple_cat(std::forward<Tuple>(t), std::tuple<E>(e));
      }

      /**If we do not have a match then just forward the given tuple.*/
      template<template<class...> class F, class Tuple, class E,
               std::enable_if_t<(IsTupleLike<Tuple>::value
                                 && !PredicateMatch<Tuple, PredicateProxy<F, E>::template ForwardFirst>::value
                 ), int> = 0>
      constexpr auto pushBackIf(Tuple&& t, E&& e, PredicateWrapper<F> = PredicateWrapper<F>{})
      {
        return std::forward<Tuple>(t);
      }

      /**Enforce kind of uniqueness. The effort, however, is linear in
       * the tuple size for adding a single element. F is instantiated
       * with the types of the elements of t as first argument and the
       * type E as second argument.
       */
      template<template<class...> class F, class Tuple, class E,
               std::enable_if_t<(IsTupleLike<Tuple>::value
                                 && !PredicateMatch<Tuple, PredicateProxy<F, E>::template ForwardFirst>::value
                 ), int> = 0>
      constexpr auto pushBackUnless(Tuple&& t, E&& e, PredicateWrapper<F> = PredicateWrapper<F>{})
      {
        return std::tuple_cat(std::forward<Tuple>(t), std::tuple<E>(e));
      }

      /**If we do not have a match then just forward the given tuple.*/
      template<template<class...> class F, class Tuple, class E,
               std::enable_if_t<(IsTupleLike<Tuple>::value
                                 && PredicateMatch<Tuple, PredicateProxy<F, E>::template ForwardFirst>::value
                 ), int> = 0>
      constexpr auto pushBackUnless(Tuple&& t, E&& e, PredicateWrapper<F> = PredicateWrapper<F>{})
      {
        return std::forward<Tuple>(t);
      }

      /**Enforce kind of uniqueness. The effort, however, is linear in
       * the tuple size for adding a single element.
       */
      template<template<class...> class F, class Tuple, class E,
               std::enable_if_t<(IsTupleLike<Tuple>::value
                                 && PredicateMatch<Tuple, PredicateProxy<F, E>::template ForwardFirst>::value
                 ), int> = 0>
      constexpr auto addFrontIf(Tuple&& t, E&& e, PredicateWrapper<F> = PredicateWrapper<F>{})
      {
        return std::tuple_cat(std::tuple<E>(e), std::forward<Tuple>(t));
      }

      /**If we do not have a match then just forward the given tuple.*/
      template<template<class...> class F, class Tuple, class E,
               std::enable_if_t<(IsTupleLike<Tuple>::value
                                 && !PredicateMatch<Tuple, PredicateProxy<F, E>::template ForwardFirst>::value
                 ), int> = 0>
      constexpr auto addFrontIf(Tuple&& t, E&& e, PredicateWrapper<F> = PredicateWrapper<F>{})
      {
        return std::forward<Tuple>(t);
      }

      /**Enforce kind of uniqueness. The effort, however, is linear in
       * the tuple size for adding a single element.
       */
      template<template<class...> class F, class Tuple, class E,
               std::enable_if_t<(IsTupleLike<Tuple>::value
                                 && !PredicateMatch<Tuple, PredicateProxy<F, E>::template ForwardFirst>::value
                 ), int> = 0>
      constexpr auto addFrontUnless(Tuple&& t, E&& e, PredicateWrapper<F> = PredicateWrapper<F>{})
      {
        return std::tuple_cat(std::tuple<E>(e), std::forward<Tuple>(t));
      }

      /**If we do not have a match then just forward the given tuple.*/
      template<template<class...> class F, class Tuple, class E,
               std::enable_if_t<(IsTupleLike<Tuple>::value
                                 && PredicateMatch<Tuple, PredicateProxy<F, E>::template ForwardFirst>::value
                 ), int> = 0>
      constexpr auto addFrontUnless(Tuple&& t, E&& e, PredicateWrapper<F> = PredicateWrapper<F>{})
      {
        return std::forward<Tuple>(t);
      }

      namespace {
        template<std::size_t N, template<class...> class F, class T1, class T2,
                 std::enable_if_t<(N >= size<T2>()), int> = 0>
        constexpr T1 joinIfHelper(T1&& t1, T2&& t2, IndexConstant<N> = IndexConstant<N>{})
        {
          return t1;
        }

        template<std::size_t N, template<class, class, class...> class F, class T1, class T2,
                 std::enable_if_t<(N < size<T2>()), int> = 0>
        constexpr auto joinIfHelper(T1&& t1, T2&& t2, IndexConstant<N> = IndexConstant<N>{})
        {
          return joinIfHelper<N+1, F>(pushBackIf<F>(std::forward<T1>(t1), get<N>(std::forward<T2>(t2))),
                                      std::forward<T2>(t2));
        }

        template<std::size_t N, template<class...> class F, class T1, class T2,
                 std::enable_if_t<(N >= size<T2>()), int> = 0>
        constexpr T1 joinUnlessHelper(T1&& t1, T2&& t2, IndexConstant<N> = IndexConstant<N>{})
        {
          return std::forward<T1>(t1);
        }

        template<std::size_t N, template<class...> class F, class T1, class T2,
                 std::enable_if_t<(N < size<T2>()), int> = 0>
        constexpr auto joinUnlessHelper(T1&& t1, T2&& t2, IndexConstant<N> = IndexConstant<N>{})
        {
          return joinUnlessHelper<N+1, F>(pushBackUnless<F>(std::forward<T1>(t1), get<N>(std::forward<T2>(t2))),
                                          std::forward<T2>(t2));
        }

      } // NS anonymous

      /**Add the elements of Arg... in turn to T if the predicate F matches.*/
      template<template<class...> class F, class T, class... Arg,
               std::enable_if_t<(!IsAlwaysTrue<F>::value
                                 && IsTupleLike<T>::value
                                 && sizeof...(Arg) == 1
                                 && size<decltype(joinIfHelper<0, F>(std::declval<T>(), std::declval<Arg>()...))>() > size<T>()
                 ), int> = 0>
      constexpr auto joinIf(T&& t, Arg&&... arg)
      {
        return joinIfHelper<0, F>(std::forward<T>(t), std::forward<Arg>(arg)...);
      }

      /**Version of joinIf() which avoids runtime computations if it
       * is clear that nothing is added.
       */
      template<template<class...> class F, class T, class... Arg,
               std::enable_if_t<(!IsAlwaysTrue<F>::value
                                 && IsTupleLike<T>::value
                                 && sizeof...(Arg) == 1
                                 && size<decltype(joinIfHelper<0, F>(std::declval<T>(), std::declval<Arg>()...))>() == size<T>()
                 ), int> = 0>
      constexpr auto joinIf(T&& t, Arg&&...)
      {
        return std::forward<T>(t);
      }

      template<template<class...> class F, class T0, class T1, class... Arg,
               std::enable_if_t<(!IsAlwaysTrue<F>::value
                                 && IsTupleLike<T0>::value
                                 && IsTupleLike<T1>::value
                                 && sizeof...(Arg) >= 1), int> = 0>
      constexpr auto joinIf(T0&& t0, T1&& t1, Arg&&... arg)
      {
        return joinIf<F>(joinIf<F>(std::forward<T0>(t0), std::forward<T1>(t1)), std::forward<Arg>(arg)...);
      }

      template<template<class...> class F, class T, class... Arg,
               std::enable_if_t<(IsAlwaysTrue<F>::value
                                 && IsTupleLike<T>::value
                 ), int> = 0>
      constexpr auto joinIf(T&& t, Arg&&... arg)
      {
        return std::tuple_cat(std::forward<T>(t), std::forward<Arg>(arg)...);
      }

      ////////////////////////// Unless ...

      /**Add the elements of Arg... in turn to T if the predicate F matches.*/
      template<template<class...> class F, class T, class... Arg,
               std::enable_if_t<(!IsAlwaysFalse<F>::value
                                 && IsTupleLike<T>::value
                                 && sizeof...(Arg) == 1
                                 && size<decltype(joinUnlessHelper<0, F>(std::declval<T>(), std::declval<Arg>()...))>() > size<T>()
                 ), int> = 0>
      constexpr auto joinUnless(T&& t, Arg&&... arg)
      {
        return joinUnlessHelper<0, F>(std::forward<T>(t), std::forward<Arg>(arg)...);
      }

      /**Version of joinUnless() which avoids runtime computations if it
       * is clear that nothing is added.
       */
      template<template<class...> class F, class T, class... Arg,
               std::enable_if_t<(!IsAlwaysFalse<F>::value
                                 && IsTupleLike<T>::value
                                 && sizeof...(Arg) == 1
                                 && size<decltype(joinUnlessHelper<0, F>(std::declval<T>(), std::declval<Arg>()...))>() == size<T>()
                 ), int> = 0>
      constexpr auto joinUnless(T&& t, Arg&&...)
      {
        return std::forward<T>(t);
      }

      template<template<class...> class F, class T0, class T1, class... Arg,
               std::enable_if_t<(!IsAlwaysFalse<F>::value
                                 && IsTupleLike<T0>::value
                                 && IsTupleLike<T1>::value
                                 && sizeof...(Arg) >= 1), int> = 0>
      constexpr auto joinUnless(T0&& t0, T1&& t1, Arg&&... arg)
      {
        return joinUnless<F>(joinUnless<F>(std::forward<T0>(t0), std::forward<T1>(t1)), std::forward<Arg>(arg)...);
      }

      template<template<class...> class F, class T, class... Arg,
               std::enable_if_t<(IsAlwaysFalse<F>::value
                                 && IsTupleLike<T>::value
                 ), int> = 0>
      constexpr auto joinUnless(T&& t, Arg&&... arg)
      {
        return std::tuple_cat(std::forward<T>(t), std::forward<Arg>(arg)...);
      }

      //!@} TupleUtilities

      //!@} C++-Templates

    } // TupleUtilities

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_MPL_CONDITIONALJOIN_HH__
