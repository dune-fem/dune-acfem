#ifndef __DUNE_ACFEM_MPL_SUBTUPLE_HH__
#define __DUNE_ACFEM_MPL_SUBTUPLE_HH__

#include "access.hh"
#include "mask.hh"
#include "sort.hh"
#include "filter.hh"
#include "generators.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup C++-Templates
     *
     * @{
     */

    /**@addtogroup TupleUtilities
     *
     * Some additional utitliy functions for tuple-likes and
     * integer-sequences.
     *
     * @{
     */

    /**@addtogroup SubTuple
     *
     * Generating sub- and super-tuples and related stuff.
     *
     * @{
     */

    namespace {
      // Helper namespace for insertAt()

      /**@internal Insert one element at given position.*/
      template<class T, class Tuple, std::size_t... Front, std::size_t... Back>
      auto insertAtInsertOne(T t, Tuple&& tuple, std::index_sequence<Front...>, std::index_sequence<Back...>)
      {
        return std::make_tuple(get<Front>(tuple)..., t, get<Back+sizeof...(Front)>(tuple)...);
      }

      /**@internal Recursion end-point: just return @a tuple.*/
      template<std::size_t N, class PosSeq, class DataTuple, class SrcTuple,
               std::enable_if_t<(N == PosSeq::size()), int> = 0>
      auto insertAtHelper(PosSeq pos, const DataTuple& data, const SrcTuple& tuple, TrueType)
      {
        return tuple;
      }

      /**@internal Recursion: insert each element in turn.
       *
       * @param[in] N The index into PosSeq and DataTuple in order to
       * fetch index and data to insert.
       *
       * @param[in] pos The sequence of the positions to insert the
       * data contained in @a DataTuple at.
       *
       * @param[in] data The tuple of data items to insert.
       *
       * @param[in] tuple The original tuple.
       *
       * @return A new tuple with resulting from the argument @a tuple
       * and one additional data-item inserted at a given position.
       */
      template<std::size_t N, class PosSeq, class DataTuple, class SrcTuple,
               std::enable_if_t<(N < PosSeq::size()), int> = 0>
      auto insertAtHelper(PosSeq pos, const DataTuple& data, const SrcTuple& tuple, TrueType)
      {
        constexpr std::size_t size = std::tuple_size<SrcTuple>::value;
        constexpr std::size_t index = Get<N, PosSeq>::value;
        using Front = MakeIndexSequence<index>;
        using Back = MakeIndexSequence<size - index>;

        return insertAtHelper<N+1>(pos, data, insertAtInsertOne(get<N>(data), tuple, Front{}, Back{}), trueType());
      }

      template<std::size_t N, class PosSeq, class DataTuple, class SrcTuple>
      auto insertAtHelper(PosSeq pos, const DataTuple& data, const SrcTuple& tuple, FalseType)
      {
        using Sort = SortSequence<PosSeq>;
        using SortedPos = typename Sort::Result;
        using Perm = typename Sort::Permutation;

        return insertAtHelper<N>(SortedPos{}, permute(Perm{}, data), tuple, trueType());
      }

    }

    /**Insert the elements of @a data at positions given by @a pos into @a
     * src in turn. In turn means that @a pos gives the index in the final
     * tuple after inserting each element one after another. E.g.:
     *
     * data: make_tuple(a, b)
     * pos: index_sequence<2, 4>
     * src: make_tuple(0, 1, 2, 3)
     *
     * yields:
     *
     * make_tuple(0, 1, a, 2, b, 3).
     */
    template<std::size_t... Pos, class SrcTuple, class DataTuple, bool AssumeSorted = true,
             // Enable for "things" like tuples. We need std::get
             std::enable_if_t<AllowsGet<SrcTuple>::value && AllowsGet<DataTuple>::value, int> = 0>
    auto insertAt(SrcTuple&& src, DataTuple&& data, BoolConstant<AssumeSorted> = BoolConstant<AssumeSorted>{})
    {
      return insertAtHelper<0>(std::index_sequence<Pos...>{}, std::forward<DataTuple>(data), std::forward<SrcTuple>(src), BoolConstant<AssumeSorted>{});
    }

    /**Like the tuple variant, but with single non-tuple value.*/
    template<std::size_t Where, class SrcTuple, class Data, bool AssumeSorted = true,
             // Enable for "things" like tuples. We need std::get
             std::enable_if_t<AllowsGet<SrcTuple>::value && !AllowsGet<Data>::value, int> = 0>
    auto insertAt(SrcTuple&& src, Data&& data, BoolConstant<AssumeSorted> = BoolConstant<AssumeSorted>{})
    {
      return insertAtHelper<0>(IndexSequence<Where>{}, std::forward_as_tuple(data), std::forward<SrcTuple>(src), BoolConstant<AssumeSorted>{});
    }

    /**Insert-at variant with indices given by index-sequence argument.*/
    template<std::size_t... Pos, class SrcTuple, class DataTuple, bool AssumeSorted = true,
             // Enable for "things" like tuples. We need std::get
             std::enable_if_t<AllowsGet<SrcTuple>::value && AllowsGet<DataTuple>::value, int> = 0>
    auto insertAt(SrcTuple&& src, DataTuple&& data, IndexSequence<Pos...>, BoolConstant<AssumeSorted> = BoolConstant<AssumeSorted>{})
    {
      return insertAt<Pos...>(std::forward<SrcTuple>(src), std::forward<DataTuple>(data), BoolConstant<AssumeSorted>{});
    }

    /**Insert-at variant with indices given by index-constant argument.*/
    template<std::size_t Where, class SrcTuple, class Data, bool AssumeSorted = true,
             // Enable for "things" like tuples. We need std::get
             std::enable_if_t<AllowsGet<SrcTuple>::value && !AllowsGet<Data>::value, int> = 0>
    auto insertAt(SrcTuple&& src, Data&& data, IndexConstant<Where>, BoolConstant<AssumeSorted> = BoolConstant<AssumeSorted>{})
    {
      return insertAt<Where>(std::forward<SrcTuple>(src), std::forward<Data>(data), BoolConstant<AssumeSorted>{});
    }

    /**Extract a sub-tuple by explicit indexing.
     *
     * @param[in] t Some tuple.
     *
     * @return a std::tuple containing only the elements as specified
     * by the index-sequence.
     *
     * Example which extract the first and third element:
     * @code
     * auto sub = subTuple<0, 2>(std::make_tuple(0, 1, 2, 3, 4);
     * @endcode
     * The resulting tuple @c sub will contain the numbers 0 and 2.
     */
    template<std::size_t... I, class TupleLike,
             std::enable_if_t<IsTupleLike<TupleLike>::value, int> = 0>
    auto subTuple(const TupleLike& t, IndexSequence<I...> = IndexSequence<I...>{}) {
      return std::make_tuple(std::get<I>(t)...);
    }

    /**Like subTuble() but forward the arguments, possibly in order to
     * be expanded as parameters to another function.
     */
    template<std::size_t... I, class TupleLike,
             std::enable_if_t<IsTupleLike<TupleLike>::value, int> = 0>
    auto forwardSubTuple(const TupleLike& t, IndexSequence<I...> = IndexSequence<I...>{}) {
      return std::forward_as_tuple(std::get<I>(t)...);
    }

    /**Define the type of a sub-tuple given by an index-pack. */
    template<class T, std::size_t... I>
    using SubTuple = decltype(subTuple<I...>(std::declval<T>()));

    /**Generate a sub-tuple with the elements at the positions where a
     * bit in a bit-mask is set. The bit mask has to be passed as
     * template-parameter.
     *
     * @param[in] t Some tuple.
     *
     * @return a std::tuple containing only the elements as specified
     * by the bit-mask
     *
     * Example which extract the first and third element:
     * @code
     * auto sub = maskTuple<0x5>(std::make_tuple(0, 1, 2, 3, 4));
     * @endcode
     * The resulting tuple @c sub will contain the numbers 0 and 2.
     */
    template<std::size_t mask, typename... T>
    auto maskTuple(const std::tuple<T...>& t, IndexConstant<mask> = IndexConstant<mask>{})
    {
      return subTuple(t, MaskSequence<mask>());
    }

    /**Define the type a sub-tuple generated by the give bit-mask. */
    template<std::size_t mask, class T>
    using MaskTuple = decltype(maskTuple<mask>(std::declval<T>()));

    /**@internal Helper for filteredSubTuple(). F should be a functor
     * which derives from std::true_type or std::false_type. During
     * the filter procedure it will be instantiated with the type of
     * each tuple element as template argument.
     */
    template<class Tuple, template<class...> class F>
    struct HasPropertyFunctor
    {
      template<class T, T, std::size_t N>
      using Apply = F<TupleElement<N, Tuple> >;
    };

    /**Generate a sub-tuple of the elements where
     * F<TupleElement<N, Tuple> > is derived from
     * std::true_type, where N is the index of the respective type in
     * the tuple.
     *
     * @param[in] t A tuple-like (tuple, pair, array)
     *
     * @param[in] f A functor class. F<T> should inspect T and
     * specialize to std::true_type or std::false_type depending on
     * T. F is instantiated with the type of each tuple-element in
     * turn. The fancy variadic argument list for @a F allows for
     * additional default arguments for @a F, for instance in order to
     * exploit SFINAE techniques.
     *
     * @return A std::tuple object with the matched elements.
     */
    template<template<class T, class...> class F, class Tuple>
    auto filteredTuple(Tuple&& t)
    {
      using ComponentSequence = MakeSequenceFor<Tuple>;
      using SliceSequence = FilteredSequence<HasPropertyFunctor<Tuple, F>, ComponentSequence>;
      return forwardSubTuple(std::forward<Tuple>(t), SliceSequence{});
    }

    /**Generate the type of the return value of filteredTuple().*/
    template<template<class T, class...> class F, class Tuple>
    using FilteredTuple = std::decay_t<decltype(filteredTuple<F>(std::declval<Tuple>()))>;

    /**Add an element to the front.*/
    template<class T, class Tuple, std::enable_if_t<IsTupleLike<Tuple>::value, int> = 0>
    constexpr auto addFront(T&& t, Tuple&& tuple)
    {
      return std::tuple_cat(std::tuple<T>(std::forward<T>(t)), std::forward<Tuple>(tuple));
    }

    /**Add an element to the end.*/
    template<class T, class Tuple, std::enable_if_t<IsTupleLike<Tuple>::value, int> = 0>
    constexpr auto pushBack(T&& t, Tuple&& tuple)
    {
      return std::tuple_cat(std::forward<Tuple>(tuple), std::tuple<T>(std::forward<T>(t)));
    }

    /**Type of the tuple where a T is added at the front.*/
    template<class T, class Tuple>
    using AddFrontTuple = std::decay_t<decltype(addFront(std::declval<T>(), std::declval<Tuple>()))>;

    /**Type of the tuple where a T is added to the end.*/
    template<class T, class Tuple>
    using PushBackTuple = std::decay_t<decltype(pushBack(std::declval<T>(), std::declval<Tuple>()))>;

    template<class... Tuple>
    using TupleCat = std::decay_t<decltype(std::tuple_cat(std::declval<Tuple>()...))>;

    //!@} SubTuple

    //!@} TupleUtilities

    //!@} C++-Templates

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_MPL_SUBTUPLE_HH__
