#ifndef __DUNE_ACFEM_MPL_MAKESEQUENCE_HH__
#define __DUNE_ACFEM_MPL_MAKESEQUENCE_HH__

#include "../common/compiler.hh"
#include "../common/types.hh"
#include "size.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup C++-Templates
     *
     * @{
     */

    /**@addtogroup TupleUtilities
     *
     * Some additional utitliy functions for tuple-likes and
     * integer-sequences.
     *
     * @{
     */

    /**@addtogroup SequenceGenerators
     *
     * Generate integer sequences.
     *
     * @{
     */

#if false && DUNE_ACFEM_IS_GCC(8,99)
// the builtin __integer_pack is completely unstable ATM
#  define __DUNE_ACFEM_USE_INTEGER_PACK 1
#elif defined(__has_builtin)
# if __has_builtin(__make_integer_seq)
#  define __DUNE_ACFEM_USE_MAKE_INTEGER_SEQ 1
# endif
#endif

#if __DUNE_ACFEM_USE_MAKE_INTEGER_SEQ || __DUNE_ACFEM_USE_MAKE_INTEGER_SEQUENCE

    namespace {

      template<class Seq, std::ptrdiff_t Offset = 0, std::ptrdiff_t Stride = 1L, std::size_t Repeat = 1>
      struct MakeSequenceExpander;

      template<class T, T... I, std::ptrdiff_t Offset, std::ptrdiff_t Stride, std::size_t Repeat>
      struct MakeSequenceExpander<Sequence<T, I...>, Offset, Stride, Repeat>
      {
        using Type = Sequence<T, (Offset + I/Repeat*Stride)...>;
      };

    }

    /**Make an index sequence with an optional offset.
     *
     * @param T The type of the elements.
     *
     * @param N The number of "different" sequence entries.
     *
     * @param Offset An offset added to all sequence entries. Note:
     * the offset is allowed to be negative.
     *
     * @param Stride A The stride size between different sequence
     * entries. In particular, if Stride == 0 then a constant sequence
     * is generated.
     *
     * @param Repeat The number of repetitions of each individual
     * sequence values. The actual size of the generated sequence is
     * N*Repeat.
     */
#if __DUNE_ACFEM_USE_MAKE_INTEGER_SEQUENCE
    template<class T, T N, std::ptrdiff_t Offset = 0, std::ptrdiff_t Stride = 1L, std::size_t Repeat = 1>
    using MakeSequence = typename MakeSequenceExpander<std::make_integer_sequence<T, N*Repeat>, Offset, Stride, Repeat>::Type;
#else
    template<class T, T N, std::ptrdiff_t Offset = 0, std::ptrdiff_t Stride = 1L, std::size_t Repeat = 1>
    using MakeSequence = typename MakeSequenceExpander<__make_integer_seq<Sequence, T, N*Repeat>, Offset, Stride, Repeat>::Type;
#endif

#elif __DUNE_ACFEM_USE_INTEGER_PACK

    /**Make an index sequence with an optional offset.
     *
     * @param T The type of the elements.
     *
     * @param N The number of "different" sequence entries.
     *
     * @param Offset An offset added to all sequence entries. Note:
     * the offset is allowed to be negative.
     *
     * @param Stride A The stride size between different sequence
     * entries. In particular, if Stride == 0 then a constant sequence
     * is generated.
     *
     * @param Repeat The number of repetitions of each individual
     * sequence values. The actual size of the generated sequence is
     * N*Repeat.
     */
    template<class T, T N, std::ptrdiff_t Offset = 0, std::ptrdiff_t Stride = 1L, std::size_t Repeat = 1>
    using MakeSequence = Sequence<T, (Offset + __integer_pack(N*Repeat)/Repeat*Stride)...>;

#else

    namespace {

      // sequence generation workhorse with optional repeat and stride
      // and offset. The implementation follows the one from gcc where
      // a bisection technique is used in order to reduce the
      // computational effort.

      template<class T, std::ptrdiff_t Stride, std::size_t Repeat,
               class Seq1, class Seq2>
      struct GenSeqCat;

      template<class T, std::ptrdiff_t Stride, std::size_t Repeat,
               T... I1, T... I2>
      struct GenSeqCat<T, Stride, Repeat,
                       Sequence<T, I1...>, Sequence<T, I2...> >
      {
        using Type = Sequence<T, I1..., (I2 + sizeof...(I1)/Repeat*Stride)...>;
      };

      template<class T, std::ptrdiff_t Stride,
               T... I1, T... I2>
      struct GenSeqCat<T, Stride, 1,
                       Sequence<T, I1...>, Sequence<T, I2...> >
      {
        using Type = Sequence<T, I1..., (I2 + sizeof...(I1)*Stride)...>;
      };

      template<class T,
               T... I1, T... I2>
      struct GenSeqCat<T, 1, 1,
                       Sequence<T, I1...>, Sequence<T, I2...> >
      {
        using Type = Sequence<T, I1..., (I2 + sizeof...(I1))...>;
      };

      template<class T,
               T... I1, T... I2>
      struct GenSeqCat<T, -1, 1,
                       Sequence<T, I1...>, Sequence<T, I2...> >
      {
        using Type = Sequence<T, I1..., (I2 - sizeof...(I1))...>;
      };

      template<class T, std::size_t N, std::ptrdiff_t Offset, std::ptrdiff_t Stride, std::size_t Repeat>
      struct BuildSequence
        : GenSeqCat<T, Stride, Repeat,
                    typename BuildSequence<T, N / 2, Offset, Stride, Repeat>::Type,
                    typename BuildSequence<T, N - N/2, Offset, Stride, Repeat>::Type>
      {};

      /**@internal Recursion endpoint Repeat many equal
       * elements. Recurse into Stride = 0, but N > 0.
       */
      template<class T, std::ptrdiff_t Offset, std::ptrdiff_t Stride, std::size_t Repeat>
      struct BuildSequence<T, 1, Offset, Stride, Repeat>
      {
        using Type = typename BuildSequence<T, Repeat, Offset, 0, 1>::Type;
      };

      /**@internal Recursion endpoint for one element sequence, Repeat = 1.*/
      template<class T, std::ptrdiff_t Offset, std::ptrdiff_t Stride>
      struct BuildSequence<T, 1, Offset, Stride, 1>
      {
        using Type = Sequence<T, Offset>;
      };

      /**@internal Recursion endpoint, N = 0, empty sequence.*/
      template<class T, std::ptrdiff_t Offset, std::ptrdiff_t Stride, std::size_t Repeat>
      struct BuildSequence<T, 0, Offset, Stride, Repeat>
      {
        using Type = Sequence<T>;
      };
    }

    /**Make an index sequence with an optional offset.
     *
     * @param T The type of the elements.
     *
     * @param N The number of "different" sequence entries.
     *
     * @param Offset An offset added to all sequence entries. Note:
     * the offset is allowed to be negative.
     *
     * @param Stride A The stride size between different sequence
     * entries. In particular, if Stride == 0 then a constant sequence
     * is generated.
     *
     * @param Repeat The number of repetitions of each individual
     * sequence values. The actual size of the generated sequence is
     * N*Repeat.
     */
    template<class T, T N, std::ptrdiff_t Offset = 0, std::ptrdiff_t Stride = 1L, std::size_t Repeat = 1>
    using MakeSequence = typename BuildSequence<T, N, Offset, Stride, Repeat>::Type;
#endif

    //!@} SequenceGenerators

    //!@} TupleUtilities

    //!@} C++-Templates

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_MPL_MAKESEQUENCE_HH__
