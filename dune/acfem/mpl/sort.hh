#ifndef __DUNE_ACFE_MPL_SORT_HH__
#define __DUNE_ACFE_MPL_SORT_HH__

#include "generators.hh"

#include "sequencesort.hh"

namespace Dune {

  namespace ACFem {

    template<std::size_t I0, std::size_t I1>
    using DefaultDoSwap = BoolConstant<(I0 > I1)>;

    template<class Seq, template<std::size_t, std::size_t> class DoSwap = DefaultDoSwap>
    using SortSequence = MPL::SortSequence<Seq, DoSwap>;

    template<class Seq, template<std::size_t, std::size_t> class DoSwap = DefaultDoSwap>
    using IsSorted = MPL::IsSorted<Seq, DoSwap>;

    template<class Seq, template<std::size_t, std::size_t> class DoSwap = DefaultDoSwap>
    constexpr inline bool IsSortedV = IsSorted<Seq, DoSwap>::value;

    template<class Seq, std::enable_if_t<IsSequence<Seq>::value, int> = 0>
    static constexpr auto sort(Seq = Seq{})
    {
      return typename SortSequence<Seq, DefaultDoSwap>::Result{};
    }

    template<template<std::size_t, std::size_t> class DoSwap,
             class Seq, std::enable_if_t<IsSequence<Seq>::value, int> = 0>
    static constexpr auto sort(Seq = Seq{})
    {
      return typename SortSequence<Seq, DoSwap>::Result{};
    }

    template<template<std::size_t, std::size_t> class DoSwap, class Seq,
             std::enable_if_t<IsSequence<Seq>::value, int> = 0>
    static constexpr auto sortPermutation(Seq = Seq{})
    {
      return typename SortSequence<Seq, DoSwap>::Permutation{};
    }

    template<class Seq, std::enable_if_t<IsSequence<Seq>::value, int> = 0>
    static constexpr auto sortPermutation(Seq = Seq{})
    {
      return sortPermutation<DefaultDoSwap>(Seq{});
    }

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFE_MPL_SORT_HH__
