#ifndef __DUNE_ACFEM_MPL_LEXCOMPARE_HH__
#define __DUNE_ACFEM_MPL_LEXCOMPARE_HH__

#include "../common/types.hh"

namespace Dune::ACFem::MPL {

  template<class A, class B, std::ptrdiff_t Cmp = static_cast<ptrdiff_t>(A::size()) - static_cast<ptrdiff_t>(B::size())>
  constexpr inline std::ptrdiff_t LexCompareV = Cmp;

  template<std::size_t A0, std::size_t... A, std::size_t B0, std::size_t... B>
  constexpr inline std::ptrdiff_t LexCompareV<IndexSequence<A0, A...>, IndexSequence<B0, B...>, 0L> = LexCompareV<IndexSequence<A...>, IndexSequence<B...>, static_cast<ptrdiff_t>(A0) - static_cast<ptrdiff_t>(B0)>;

  template<std::size_t A0, std::size_t... A>
  constexpr inline std::ptrdiff_t LexCompareV<IndexSequence<A0, A...>, IndexSequence<A0, A...>, 0L> = 0L;

} // Dune::ACFem::MPL::

#endif // __DUNE_ACFEM_MPL_LEXCOMPARE_HH__
