#ifndef __DUNE_ACFEM_MPL_TYPETUPLE_HH__
#define __DUNE_ACFEM_MPL_TYPETUPLE_HH__

#include "typepackelement.hh"
#include "size.hh"

namespace Dune::ACFem::MPL {

  using ::Dune::ACFem::TypePackElement;

  template<class... T>
  struct TypeTuple
  {
    static constexpr std::size_t size()
    {
      return sizeof...(T);
    }

    template<std::size_t I>
    using Get = TypePackElement<I, T...>;
  };

  template<class T>
  struct TypeTuple<T>
  {
    using Type = T;

    static constexpr std::size_t size()
    {
      return 1;
    }

    template<std::size_t I>
    using Get = T;
  };

  template<class T>
  using TypeWrapper = TypeTuple<T>;

  template<class T0, class T1>
  struct TypeTuple<T0, T1>
  {
    using First = T0;
    using Second = T1;

    static constexpr std::size_t size()
    {
      return 2;
    }

    template<std::size_t I>
    using Get = ConditionalType<I == 0, T0, T1>;
  };

  template<class T0, class T1>
  using TypePair = TypeTuple<T0, T1>;

  template<class T>
  constexpr inline bool IsTypeTupleV = false;

  template<class T>
  constexpr inline bool IsTypeTupleV<T&> = IsTypeTupleV<T>;

  template<class T>
  constexpr inline bool IsTypeTupleV<T&&> = IsTypeTupleV<T>;

  template<class... T>
  constexpr inline bool IsTypeTupleV<TypeTuple<T...> > = true;

  template<std::size_t N, class Tuple>
  using TypeTupleElement = typename Tuple::template Get<N>;

  template<std::size_t N, class Tuple>
  using GetType = typename Tuple::template Get<N>;

  template<class Tuple>
  struct FrontTypeExpander;

  template<class T, class... Rest>
  struct FrontTypeExpander<TypeTuple<T, Rest...> >
  {
    using Type = T;
    using Tail = TypeTuple<Rest...>;
  };

  template<class Tuple>
  using FrontType = typename FrontTypeExpander<Tuple>::Type;

  template<class Tuple>
  using TailPart = typename FrontTypeExpander<Tuple>::Tail;

  template<class T, class... Rest>
  constexpr auto addFrontType(TypeTuple<Rest...>)
  {
    return MPL::TypeTuple<T, Rest...>{};
  }

  template<class T, class TypeList>
  using AddFrontType = decltype(addFrontType<T>(TypeList{}));

  template<class T, class... Types>
  constexpr auto removeFrontType(TypeTuple<T, Types...>)
  {
    return TypeTuple<Types...>{};
  };

  template<class TypeList>
  using RemoveFrontType = decltype(removeFrontType(TypeList{}));

  template<class T, class... Types>
  constexpr auto pushBackType(TypeTuple<Types...>)
  {
    return TypeTuple<Types..., T>{};
  }

  template<class T, class TypeList>
  using PushBackType = decltype(pushBackType<T>(TypeList{}));

  template<class... Types, class T>
  constexpr auto popBackType(TypeTuple<Types..., T>)
  {
    return TypeTuple<Types...>{};
  }

  template<class TypeList>
  using PopBackType = decltype(popBackType(TypeList{}));

  template<class... Tuples>
  struct TypeTupleCatHelper;

  template<>
  struct TypeTupleCatHelper<>
  {
    using Type = TypeTuple<>;
  };

  template<class Tuple>
  struct TypeTupleCatHelper<Tuple>
  {
    using Type = Tuple;
  };

  template<class... T0, class... T1>
  struct TypeTupleCatHelper<TypeTuple<T0...>, TypeTuple<T1...> >
  {
    using Type = TypeTuple<T0..., T1...>;
  };

  template<class... T0, class... T1, class... T2>
  struct TypeTupleCatHelper<TypeTuple<T0...>, TypeTuple<T1...>, TypeTuple<T2...> >
  {
    using Type = TypeTuple<T0..., T1..., T2...>;
  };

  template<class... T0, class... T1, class... T2, class... T3>
  struct TypeTupleCatHelper<TypeTuple<T0...>, TypeTuple<T1...>, TypeTuple<T2...>, TypeTuple<T3...> >
  {
    using Type = TypeTuple<T0..., T1..., T2..., T3...>;
  };

  template<class... T0, class... T1, class... T2, class... T3, class... T4>
  struct TypeTupleCatHelper<TypeTuple<T0...>, TypeTuple<T1...>, TypeTuple<T2...>, TypeTuple<T3...>, TypeTuple<T4...> >
  {
    using Type = TypeTuple<T0..., T1..., T2..., T3..., T4...>;
  };

  template<class... T0, class... T1, class... T2, class... T3, class... T4,
           class... T5>
  struct TypeTupleCatHelper<TypeTuple<T0...>, TypeTuple<T1...>, TypeTuple<T2...>, TypeTuple<T3...>, TypeTuple<T4...>,
                            TypeTuple<T5...> >
  {
    using Type = TypeTuple<T0..., T1..., T2..., T3..., T4...,
                           T5...>;
  };


  template<class... T0, class... T1, class... T2, class... T3, class... T4,
           class... T5, class... T6>
  struct TypeTupleCatHelper<TypeTuple<T0...>, TypeTuple<T1...>, TypeTuple<T2...>, TypeTuple<T3...>, TypeTuple<T4...>,
                            TypeTuple<T5...>, TypeTuple<T6...> >
  {
    using Type = TypeTuple<T0..., T1..., T2..., T3..., T4...,
                           T5..., T6...>;
  };

  template<class... T0, class... T1, class... T2, class... T3, class... T4,
           class... T5, class... T6, class... T7>
  struct TypeTupleCatHelper<TypeTuple<T0...>, TypeTuple<T1...>, TypeTuple<T2...>, TypeTuple<T3...>, TypeTuple<T4...>,
                            TypeTuple<T5...>, TypeTuple<T6...>, TypeTuple<T7...> >
  {
    using Type = TypeTuple<T0..., T1..., T2..., T3..., T4...,
                           T5..., T6..., T7...>;
  };

  template<class... T0, class... T1, class... T2, class... T3, class... T4,
           class... T5, class... T6, class... T7, class... T8>
  struct TypeTupleCatHelper<TypeTuple<T0...>, TypeTuple<T1...>, TypeTuple<T2...>, TypeTuple<T3...>, TypeTuple<T4...>,
                            TypeTuple<T5...>, TypeTuple<T6...>, TypeTuple<T7...>, TypeTuple<T8...> >
  {
    using Type = TypeTuple<T0..., T1..., T2..., T3..., T4...,
                           T5..., T6..., T7..., T8...>;
  };

  template<class... T0, class... T1, class... T2, class... T3, class... T4,
           class... T5, class... T6, class... T7, class... T8, class... T9>
  struct TypeTupleCatHelper<TypeTuple<T0...>, TypeTuple<T1...>, TypeTuple<T2...>, TypeTuple<T3...>, TypeTuple<T4...>,
                            TypeTuple<T5...>, TypeTuple<T6...>, TypeTuple<T7...>, TypeTuple<T8...>, TypeTuple<T9...> >
  {
    using Type = TypeTuple<T0..., T1..., T2..., T3..., T4...,
                           T5..., T6..., T7..., T8..., T9...>;
  };

  template<class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, class T9, class Other, class... Rest>
  struct TypeTupleCatHelper<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, Other, Rest...>
  {
    using Type = typename TypeTupleCatHelper<
      typename TypeTupleCatHelper<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>::Type, Other, Rest...
      >::Type;
  };

  template<class... Tuples>
  using TypeTupleCat = typename TypeTupleCatHelper<Tuples...>::Type;

  namespace {

    template<class Tuple, template<CXX17_P0522R0(class T)> class Transform>
    struct TypeTupleTransformHelper;

    template<class... T,  template<CXX17_P0522R0(class)> class Transform>
    struct TypeTupleTransformHelper<TypeTuple<T...>, Transform>
    {
      using Type = TypeTuple<Transform<T>...>;
    };

  }

  template<class Tuple, template<CXX17_P0522R0(class T)> class Transform>
  using TypeTupleTransform = typename TypeTupleTransformHelper<Tuple, Transform>::Type;

}

#include "size.hh"

namespace Dune::ACFem {

  template<class... T>
  constexpr inline std::size_t SizeV<MPL::TypeTuple<T...> > = sizeof...(T);

}

#endif // __DUNE_ACFEM_MPL_TYPETUPLE_HH__
