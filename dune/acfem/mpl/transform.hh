#ifndef __DUNE_ACFEM_MPL_TRANSFORM_HH__
#define __DUNE_ACFEM_MPL_TRANSFORM_HH__

#include "mask.hh"
#include "generators.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup C++-Templates
     *
     * @{
     */

    /**@addtogroup TupleUtilities
     *
     * Some additional utitliy functions for tuple-likes and
     * integer-sequences.
     *
     * @{
     */

    /**@addtogroup SequenceTransform
     *
     * Apply transformation functors to given integer sequences.
     *
     * @{
     */

    // forward
    struct IdentityFunctor;

    // forward
    struct AcceptAllFunctor;

    // forward
    template<class F, std::size_t... Index>
    struct TransformAtIndexFunctor;

    // forward
    template<bool acceptEqual>
    struct AcceptEqualFunctor;

    namespace {
      // helpers for TransformSequence template.

      // Forward, specialized below.
      template<class SeqIn,
               class SeqOut,
               class TransformFunctor,
               class AcceptFunctor,
               std::size_t N,
               class SFINAE = void>
      struct TransformSequenceRecursion;

      /**@internal Recursion with positive accept functor @a A,
       * passing an index to the functors.
       */
      template<class Tin, Tin I0, Tin... RestIn, class Tout, Tout... Out,
               class F, class A, std::size_t N>
      struct TransformSequenceRecursion<
        Sequence<Tin, I0, RestIn...>,
        Sequence<Tout, Out...>,
        F, A, N,
        std::enable_if_t<A::template Apply<Tin, I0, Tout, F::template Apply<Tin, I0, N>::value, N>::value> >
      {
        static const Tout V = F::template Apply<Tin, I0, N>::value;
        using Type = typename TransformSequenceRecursion<Sequence<Tin, RestIn...>, Sequence<Tout, Out..., V>, F, A, N+1>::Type;
      };

      /**@internal Recursion with negative accept functor @a A.*/
      template<class Tin, Tin I0, Tin... RestIn, class Tout, Tout... Out,
               class F, class A, std::size_t N>
      struct TransformSequenceRecursion<
        Sequence<Tin, I0, RestIn...>,
        Sequence<Tout, Out...>,
        F, A, N,
        std::enable_if_t<!A::template Apply<Tin, I0, Tout, F::template Apply<Tin, I0, N>::value, N>::value> >
      {
        using Type = typename TransformSequenceRecursion<Sequence<Tin, RestIn...>, Sequence<Tout, Out...>, F, A, N+1>::Type;
      };

      /**@internal Recursion end-point with empty input sequence.*/
      template<class Tin, class Tout, Tout... Out, class F, class A, std::size_t N>
      struct TransformSequenceRecursion<Sequence<Tin>, Sequence<Tout, Out...>, F, A, N>
      {
        using Type = Sequence<Tout, Out...>;
      };

      /**@internal Recursion start where AcceptFunctor is not AcceptAll.*/
      template<class SeqIn,
               class SeqOut,
               class TransformFunctor,
               class AcceptFunctor,
               class SFINAE = void>
      struct TransformSequenceHelper
      {
        using Type = typename TransformSequenceRecursion<SeqIn, SeqOut, TransformFunctor, AcceptFunctor, 0UL>::Type;
      };

      /**@internal Use potentially faster pack expansion if all elements are accepted.*/
      template<class SeqIn, class SeqOutt, class TransformFunctor, class IndexSeq>
      struct TransformSequenceExpander;

      /**@internal Use potentially faster pack expansion if all elements are accepted.*/
      template<class Tin, Tin... In, class Tout, Tout... Out, class TransformFunctor, std::size_t... N>
      struct TransformSequenceExpander<
        Sequence<Tin, In...>, Sequence<Tout, Out...>, TransformFunctor, IndexSequence<N...> >
      {
        using Type = Sequence<Tout, Out..., TransformFunctor::template Apply<Tin, In, N>::value...>;
      };

      /**@internal Simplified variant for AcceptAllFunctor.*/
      template<class Tin, Tin... In,
               class SeqOut,
               class TransformFunctor>
      struct TransformSequenceHelper<
        Sequence<Tin, In...>, SeqOut, TransformFunctor, AcceptAllFunctor>
      {
        using Type = typename TransformSequenceExpander<Sequence<Tin, In...>, SeqOut, TransformFunctor, MakeIndexSequence<sizeof...(In)> >::Type;
      };

      /**@internal Simplified variant for AcceptAllFunctor and empty input sequence.
       */
      template<class Tin, class SeqOut, class TransformFunctor>
      struct TransformSequenceHelper<Sequence<Tin>, SeqOut, TransformFunctor, AcceptAllFunctor>
      {
        using Type = SeqOut;
      };

    }

    /**General sequence transformation alias.
     *
     * @param SeqIn Input integer sequence.
     *
     * @param SeqOut Initial output sequence. Result of transformation
     * will we prepended to SeqOut.
     *
     * @param TransformFunctor A type proving an "Apply" template like
     * follows, where N is the index into the input sequence and Tin its
     * value type, V the current untransformed value:
     *
     * @code
     * struct TransformFunctor
     * {
     *   template<class Tin, Tin V, std::size_t N>
     *   using Apply = Constant<Tout, RESULT>;
     * };
     * @endcode
     *
     * @param AcceptFunctor A type providing an "Apply" template like
     * follows, where Tin, Tout are the value-types of the input and
     * output sequence, V the current untransformed value and FV the
     * transformed value after applying the TransformFunctor. The
     * output sequence will only contain elements which lead to
     * std::true_type.
     *
     * @code
     * struct AcceptFunctor
     * {
     *   template<class Tin, Tin V, class Tout, Tout FV>
     *   using Apply = BoolConstant<RESULT>;
     * }
     * @endcode
     */
    template<class SeqIn,
             class SeqOut = Sequence<typename SeqIn::value_type>,
             class TransformFunctor = IdentityFunctor,
             class AcceptFunctor = AcceptAllFunctor>
    using TransformSequence = typename TransformSequenceHelper<SeqIn, SeqOut, TransformFunctor, AcceptFunctor>::Type;

    /**Concatenate the given sequences to <Seq2, Seq1> */
    template<class Seq1, class Seq2>
    using CatSequence = TransformSequence<Seq1, Seq2>;

    namespace {
      template<class... S>
      struct SequenceCatHelper;

      template<class S>
      struct SequenceCatHelper<S>
      {
        using Type = S;
      };

      template<class S1, class S2, class... Rest>
      struct SequenceCatHelper<S1, S2, Rest...>
      {
        using Type = TransformSequence<typename SequenceCatHelper<S2, Rest...>::Type, S1>;
      };

      template<class... S>
      struct SequenceCatHelper2;

      template<>
      struct SequenceCatHelper2<>
      {
        using Type = IndexSequence<>;
      };

      template<class S>
      struct SequenceCatHelper2<S>
      {
        using Type = S;
      };

      template<class I1, I1... Heads, class I2, I2... Tails, class... S>
      struct SequenceCatHelper2<Sequence<I1, Heads...>, Sequence<I2, Tails...>, S...>
      {
        using IType = std::decay_t<decltype(std::declval<I1>()*std::declval<I2>())>;
        using Type = typename SequenceCatHelper2<Sequence<IType, Heads..., Tails...>, S...>::Type;
      };
    }

    /**Concatenate the given sequences, in order, to <S0, S1, ... >.*/
    template<class... S>
    //    using SequenceCat = typename SequenceCatHelper<S...>::Type;
    using SequenceCat = typename SequenceCatHelper2<S...>::Type;

    /**Simple offset operation without the overhead of TransformSequence.*/
    template<std::ptrdiff_t Offset, class T, T... Ts>
    constexpr auto offsetSequence(Sequence<T, Ts...> = Sequence<T, Ts...>{}, IntConstant<Offset> = IntConstant<Offset>{})
    {
      return Sequence<T, (Ts+Offset)...>{};
    }

    template<std::ptrdiff_t Offset, class Seq>
    using OffsetSequence = std::decay_t<decltype(offsetSequence<Offset>(Seq{}))>;

    namespace {
      template<std::size_t N, class Seq, class Result>
      struct SequenceProdHelper;

      template<std::size_t N, class T, T... I, T... Out>
      struct SequenceProdHelper<N, Sequence<T, I...>, Sequence<T, Out...> >
      {
        using Type = typename SequenceProdHelper<N-1, Sequence<T, I...>, Sequence<T, Out..., I...> >::Type;
      };

      template<class T, T... I, T... Out>
      struct SequenceProdHelper<0, Sequence<T, I...>, Sequence<T, Out...> >
      {
        using Type = Sequence<T, Out...>;
      };

    }

    /**Compute the "product" multi index of N identical copies.*/
    template<std::size_t N, class Seq>
    using SequenceProd = typename SequenceProdHelper<N, Seq, Sequence<typename Seq::value_type> >::Type;

    namespace {
      template<std::size_t, class>
      struct SequenceSpliceHelper;

      template<std::size_t N, class T, T... V>
      struct SequenceSpliceHelper<N, Sequence<T, V...> >
      {
        using Type = SequenceCat<MakeSequence<T, N, V, 0>...>;
      };
    }

    /**Splice the sequence by repeating each value the given amount of times.*/
    template<std::size_t count, class Seq>
    using SequenceSplice = typename SequenceSpliceHelper<count, Seq>::Type;

    /**Transform given sequence. Shortcut for TransformSeuence
     * template with less complicated arguments.
     */
    template<class F, class Seq, class Tout = typename F::template Apply<typename Seq::value_type, 0, 0>::value_type>
    using TransformedSequence = TransformSequence<Seq, Sequence<Tout>, F>;

    /**Transform only the element at the given position of the given
     * sequence.
     */
    //template<std::size_t Index, class F, class Seq, class Tout = typename F::template Apply<Index, typename Seq::value_type, Index>::value_type>

    /**Cat transformed sequence with original sequence.*/
    template<class F, class Seq>
    using CatTransformedSequence = TransformSequence<Seq, Seq, F>;

    /**Apply the functor F::template value_type<I>::value to each
     * element in Seq; add to the output/sequence if the result
     * differs from I.
     */
    template<class F, class Seq>
    using TransformUnique = TransformSequence<Seq, Sequence<typename Seq::value_type>, F, AcceptEqualFunctor<false> >;

    /**Concat the result of TransformUnique with the original
     * sequence.
     */
    template<class F, class Seq>
    using CatTransformUnique = TransformSequence<Seq, Seq, F, AcceptEqualFunctor<false> >;

    /**Transform only those elements specified by the parameter pack
     * Index. The given transform functors Apply template is not
     * instantiated for the other indices.
     */
    template<class F, class Seq, std::size_t... Index>
    using TransformOnly = TransformSequence<Seq, Sequence<typename Seq::value_type>, TransformAtIndexFunctor<F, Index...>, AcceptAllFunctor>;

    /**@addtogroup SequenceTransformFunctors
     *
     * Predefined functors as arguments for TransformFunctor for the
     * TransformSequence template.
     *
     * @{
     */

    /**Identity functor. Used by TransformSequence type template as
     * transform-functor.
     */
    struct IdentityFunctor
    {
      template<class T, T V, std::size_t... N>
      struct Apply
        : Constant<T, V>
      {};
    };

    /**Multiply-offset functor.*/
    template<std::size_t Stride, std::ptrdiff_t Offset = 0>
    struct MultiplyOffsetFunctor
    {
      template<class T, T V, std::size_t... N>
      struct Apply
        : Constant<T, V*Stride+Offset>
      {};
    };

    /**Offset functor. Add an offset.*/
    template<std::ptrdiff_t Offset>
    using OffsetFunctor = MultiplyOffsetFunctor<1, Offset>;

    /**Multiply functor. Multiply by a factor.*/
    template<std::size_t Stride>
    using MultiplyFunctor = MultiplyOffsetFunctor<Stride, 0>;

    /**Index functor. Transform to the index of mask in the given
     * sequence. In combination with the AcceptValue functor this can
     * be used to find the index of a given value in a sequence.
     */
    struct IndexFunctor
    {
      template<class T, T mask, std::size_t N>
      using Apply = Constant<T, N>;
    };

    /**Applies another functor only to the elements at the specified
     * positions given by the Index parameter pack. The given functor
     * is not instantiated for the other indices.
     */
    template<class F, std::size_t... Index>
    struct TransformAtIndexFunctor
    {
      template<std::size_t N, class SFINAE = void>
      struct ApplyHelper
      {
        template<class T, T V>
        using Apply = typename IdentityFunctor::template Apply<T, V>;
      };

      template<std::size_t N>
      struct ApplyHelper<N, std::enable_if_t<ContainsValue<IndexSequence<Index...>, N>::value> >
      {
        template<class T, T V>
        using Apply = typename F::template Apply<T, V, IndexIn<IndexSequence<Index...>, N>::value>;
      };

      template<class T, T V, std::size_t N>
      using Apply = typename ApplyHelper<N>::template Apply<T, V>;
    };

    /**Transform to bool sequence of even indices.*/
    struct IsEvenFunctor
    {
      template<class T, T V, std::size_t... N>
      struct Apply
        : BoolConstant<(V & 1) == 0>
      {};
    };

    /**Transform to bool sequence of even indices.*/
    struct IsOddFunctor
    {
      template<class T, T V, std::size_t... N>
      struct Apply
        : BoolConstant<(V & 1) != 0>
      {};
    };

    /**Add given bit to all masks. */
    template<std::size_t Bit>
    struct AddBitFunctor
    {
      template<class T, T mask, std::size_t... N>
      struct Apply
        : Constant<T, (mask | (1UL << Bit))>
      {};
    };

    /**Add a set bit at right-shifted position. */
    template<std::size_t Bit, std::size_t Shift>
    struct AddBitShiftRightFunctor
    {
      template<class T, T mask, std::size_t... N>
      struct Apply
        : Constant<T, mask | ((mask & SequenceMask<Bit>::value) >> Shift)>
      {};
    };

    /**Add a set bit at left-shifted position. */
    template<std::size_t Bit, std::size_t Shift>
    struct AddBitShiftLeftFunctor
    {
      template<class T, T mask, std::size_t... N>
      struct Apply
        : Constant<T, (mask | (mask & SequenceMask<Bit>::value) << Shift)>
      {};
    };

    /**Map sequence values to values specified by other sequence,
     * e.g. in order to apply a permutation.
     */
    template<class Values>
    struct MapSequenceFunctor
    {
      template<class T, T V, std::size_t... N>
      struct Apply
        : Constant<T, Get<V, Values>::value>
      {};
    };

    /**Per convention just use the identiy for the empty value sequence.*/
    template<class I>
    struct MapSequenceFunctor<Sequence<I> >
      : IdentityFunctor
    {};

    /**Map a value to the index of the value in Values*/
    template<class Values>
    struct InverseMapSequenceFunctor
    {
      template<class T, T V, std::size_t... N>
      struct Apply
        : Constant<T, IndexIn<Values, V>::value>
      {};
    };

    /**@} SequenceTransformFunctors */

    /**@addtogroup SequenceAcceptFunctors
     *
     * Predefined functors as arguments for AcceptFunctor for the
     * TransformSequence template.
     *
     * @{
     */

    /**Functor for TransformSequence: unconditionally accept.*/
    struct AcceptAllFunctor
    {
      template<class Tin, Tin, class Tout, Tout, std::size_t... N>
      struct Apply
        : TrueType
      {};
    };

    /**Functor for TransformSequence: accept if result is either equal
     * or different from original value.
     */
    template<bool acceptEqual>
    struct AcceptEqualFunctor
    {
      template<class Tin, Tin V, class Tout, Tout FV, std::size_t... N>
      struct Apply
        : BoolConstant<(V == FV) == acceptEqual>
      {};
    };

    /**Accept if input-value is either equal or unequal to given
     * value.
     */
    template<class T, T GivenValue, bool acceptEqual = true>
    struct AcceptInputValueFunctor
    {
      template<class Tin, Tin V, class Tout, Tout FV, std::size_t... N>
      struct Apply
        : BoolConstant<(V == GivenValue) == acceptEqual>
      {};
    };

    /**Accept if input-value is in range of given bounds. The check is
     * (V >= Begin && V < End), i.e. V is accepted if it is strictly
     * less than End.
     */
    template<class T, T Begin, T End>
    struct AcceptInputInRangeFunctor
    {
      template<class Tin, Tin V, class Tout, Tout FV, std::size_t... N>
      struct Apply
        : BoolConstant<(V >= Begin && V < End)>
      {};
    };

    /**Accept if input-value MOD P = R. */
    template<class T, T P, T R>
    struct AcceptModuloFunctor
    {
      template<class Tin, Tin V, class Tout, Tout FV, std::size_t... N>
      struct Apply
        : BoolConstant<(V % P == R)>
      {};
    };

    /**@} SequenceAcceptFunctors */

    //!@} SequenceTransform

    //!@} TupleUtilities

    //!@} C++-Templates

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_MPL_MASK_HH__
