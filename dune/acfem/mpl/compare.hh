#ifndef __DUNE_ACFEM_MPL_COMPARE_HH__
#define __DUNE_ACFEM_MPL_COMPARE_HH__

#include <tuple>
#include <array>

#include "../common/types.hh"
#include "accumulate.hh"
#include "sort.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup C++-Templates
     *
     * @{
     */

    /**@addtogroup TupleUtilities
     *
     * Some additional utitliy functions for tuple-likes and
     * integer-sequences.
     *
     * @{
     */

    template<class T0, T0 t0, class T1, T1 t1>
    constexpr bool operator==(Constant<T0, t0>, Constant<T1, t1>)
    {
      return t0 == t1;
    }

    template<class T0, T0 t0, class T1, T1 t1>
    constexpr bool operator!=(Constant<T0, t0>, Constant<T1, t1>)
    {
      return t0 != t1;
    }

    template<class T0, T0 t0, class T1, T1 t1>
    constexpr bool operator<=(Constant<T0, t0>, Constant<T1, t1>)
    {
      return t0 <= t1;
    }

    template<class T0, T0 t0, class T1, T1 t1>
    constexpr bool operator<(Constant<T0, t0>, Constant<T1, t1>)
    {
      return t0 < t1;
    }

    template<class T0, T0 t0, class T1, T1 t1>
    constexpr bool operator>(Constant<T0, t0>, Constant<T1, t1>)
    {
      return t0 > t1;
    }

    template<class T0, T0 t0, class T1, T1 t1>
    constexpr bool operator>=(Constant<T0, t0>, Constant<T1, t1>)
    {
      return t0 >= t1;
    }

    template<class T0, T0 V0, class T1, T1... V1>
    constexpr bool operator==(Constant<T0, V0>, Sequence<T1, V1...>)
    {
      return (... && (V0 == V1));
    }

    template<class T0, T0 V0, class T1, T1... V1>
    constexpr bool operator!=(Constant<T0, V0>, Sequence<T1, V1...>)
    {
      return (... || (V0 != V1));
    }

    template<class T0, T0 V0, class T1, T1... V1>
    constexpr bool operator<=(Constant<T0, V0>, Sequence<T1, V1...>)
    {
      return (... && (V0 <= V1));
    }

    template<class T0, T0 V0, class T1, T1... V1>
    constexpr bool operator<(Constant<T0, V0>, Sequence<T1, V1...>)
    {
      return (... && (V0 < V1));
    }

    template<class T0, T0 V0, class T1, T1... V1>
    constexpr bool operator>=(Constant<T0, V0>, Sequence<T1, V1...>)
    {
      return (... && (V0 >= V1));
    }

    template<class T0, T0 V0, class T1, T1... V1>
    constexpr bool operator>(Constant<T0, V0>, Sequence<T1, V1...>)
    {
      return (... && (V0 > V1));
    }

    template<class T0, T0... V0, class T1, T1 V1>
    constexpr bool operator==(Sequence<T0, V0...>, Constant<T1, V1>)
    {
      return (... && (V0 == V1));
    }

    template<class T0, T0... V0, class T1, T1 V1>
    constexpr bool operator!=(Sequence<T0, V0...>, Constant<T1, V1>)
    {
      return (... || (V0 != V1));
    }

    template<class T0, T0... V0, class T1, T1 V1>
    constexpr bool operator<=(Sequence<T0, V0...>, Constant<T1, V1>)
    {
      return (... && (V0 <= V1));
    }

    template<class T0, T0... V0, class T1, T1 V1>
    constexpr bool operator<(Sequence<T0, V0...>, Constant<T1, V1>)
    {
      return (... && (V0 < V1));
    }

    template<class T0, T0... V0, class T1>
    constexpr bool operator<(Sequence<T0, V0...>, T1 t1)
    {
      return (... && (V0 < t1));
    }

    template<class T0, T0... V0, class T1, T1 V1>
    constexpr bool operator>=(Sequence<T0, V0...>, Constant<T1, V1>)
    {
      return (... && (V0 >= V1));
    }

    template<class T0, T0... V0, class T1, T1 V1>
    constexpr bool operator>(Sequence<T0, V0...>, Constant<T1, V1>)
    {
      return (... && (V0 > V1));
    }

    template<class T0, T0... V0, class T1, T1... V1,
             std::enable_if_t<sizeof...(V0) == sizeof...(V1), int> = 0>
    constexpr bool operator==(Sequence<T0, V0...>, Sequence<T1, V1...>)
    {
      return (... && (V0 == V1));
    }

    template<class T0, T0... V0, class T1, T1... V1,
             std::enable_if_t<sizeof...(V0) == sizeof...(V1), int> = 0>
    constexpr bool operator!=(Sequence<T0, V0...>, Sequence<T1, V1...>)
    {
      return (... || (V0 != V1));
    }

    template<class T0, T0... V0, class T1, T1... V1,
             std::enable_if_t<sizeof...(V0) == sizeof...(V1), int> = 0>
    constexpr bool operator<=(Sequence<T0, V0...>, Sequence<T1, V1...>)
    {
      return (... && (V0 <= V1));
    }

    template<class T0, T0... V0, class T1, T1... V1,
             std::enable_if_t<sizeof...(V0) == sizeof...(V1), int> = 0>
    constexpr bool operator<(Sequence<T0, V0...>, Sequence<T1, V1...>)
    {
      return (... && (V0 < V1));
    }

    template<class T0, T0... V0, class T1, T1... V1,
             std::enable_if_t<sizeof...(V0) == sizeof...(V1), int> = 0>
    constexpr bool operator>=(Sequence<T0, V0...>, Sequence<T1, V1...>)
    {
      return (... && (V0 >= V1));
    }

    template<class T0, T0... V0, class T1, T1... V1,
             std::enable_if_t<sizeof...(V0) == sizeof...(V1), int> = 0>
    constexpr bool operator>(Sequence<T0, V0...>, Sequence<T1, V1...>)
    {
      return (... && (V0 > V1));
    }

    template<class T0, T0... V0, class T1, T1... V1,
             std::enable_if_t<sizeof...(V0) != sizeof...(V1), int> = 0>
    constexpr bool operator==(Sequence<T0, V0...>, Sequence<T1, V1...>)
    {
      return false;
    }

    template<class T0, T0... V0, class T1, T1... V1,
             std::enable_if_t<sizeof...(V0) != sizeof...(V1), int> = 0>
    constexpr bool operator!=(Sequence<T0, V0...>, Sequence<T1, V1...>)
    {
      return true;
    }

    template<class T0, T0... V0, class T1, T1... V1,
             std::enable_if_t<sizeof...(V0) != sizeof...(V1), int> = 0>
    constexpr bool operator<=(Sequence<T0, V0...>, Sequence<T1, V1...>)
    {
      return false;
    }

    template<class T0, T0... V0, class T1, T1... V1,
             std::enable_if_t<sizeof...(V0) != sizeof...(V1), int> = 0>
    constexpr bool operator<(Sequence<T0, V0...>, Sequence<T1, V1...>)
    {
      return false;
    }

    template<class T0, T0... V0, class T1, T1... V1,
             std::enable_if_t<sizeof...(V0) != sizeof...(V1), int> = 0>
    constexpr bool operator>=(Sequence<T0, V0...>, Sequence<T1, V1...>)
    {
      return false;
    }

    template<class T0, T0... V0, class T1, T1... V1,
             std::enable_if_t<sizeof...(V0) != sizeof...(V1), int> = 0>
    constexpr bool operator>(Sequence<T0, V0...>, Sequence<T1, V1...>)
    {
      return false;
    }

    namespace {

      /**Things of different size always compare false.*/
      template<class T1, class T2, class F, class S,
               std::enable_if_t<size<T1>() != size<T2>(), int> = 0>
      constexpr bool tupleCompareHelper(T1&&, T2&&, S, F&& f)
      {
        return false;
      }

      /**Recursion work horse.*/
      template<class T1, class T2, class F, std::size_t... I>
      constexpr bool tupleCompareHelper(T1&& t1, T2&& t2, IndexSequence<I...>, F&& f)
      {
        return (... && f(get<I>(std::forward<T1>(t1)), get<I>(std::forward<T2>(t2))));
      }

    } // NS anonymous

    template<class T1, class T2, std::enable_if_t<AllowsGet<T1>::value && AllowsGet<T2>::value, int> = 0>
    constexpr bool operator==(T1&& t1, T2&& t2)
    {
      return tupleCompareHelper(std::forward<T1>(t1), std::forward<T2>(t2), MakeSequenceFor<T1>{}, [](auto&& a, auto&& b) { return a == b; });
    }

    template<class T1, class T2, std::enable_if_t<AllowsGet<T1>::value && AllowsGet<T2>::value, int> = 0>
    constexpr bool operator!=(T1&& t1, T2&& t2)
    {
      return !(t1 == t2);
    }

    template<class T1, class T2, std::enable_if_t<AllowsGet<T1>::value && AllowsGet<T2>::value, int> = 0>
    constexpr bool operator<=(T1&& t1, T2&& t2)
    {
      return tupleCompareHelper(std::forward<T1>(t1), std::forward<T2>(t2), MakeSequenceFor<T1>{}, [](auto&& a, auto&& b) { return a <= b; });
    }

    template<class T1, class T2, std::enable_if_t<AllowsGet<T1>::value && AllowsGet<T2>::value, int> = 0>
    constexpr bool operator<(T1&& t1, T2&& t2)
    {
      return tupleCompareHelper(std::forward<T1>(t1), std::forward<T2>(t2), MakeSequenceFor<T1>{}, [](auto&& a, auto&& b) { return a < b; });
    }

    template<class T1, class T2, std::enable_if_t<AllowsGet<T1>::value && AllowsGet<T2>::value, int> = 0>
    constexpr bool operator>=(T1&& t1, T2&& t2)
    {
      return tupleCompareHelper(std::forward<T1>(t1), std::forward<T2>(t2), MakeSequenceFor<T1>{}, [](auto&& a, auto&& b) { return a >= b; });
    }

    template<class T1, class T2, std::enable_if_t<AllowsGet<T1>::value && AllowsGet<T2>::value, int> = 0>
    constexpr bool operator>(T1&& t1, T2&& t2)
    {
      return tupleCompareHelper(std::forward<T1>(t1), std::forward<T2>(t2), MakeSequenceFor<T1>{}, [](auto&& a, auto&& b) { return a > b; });
    }

    /**@return @c true if given non-empty sequence is constant,
     * i.e. all value ares equal to each other.
     */
    template<class T, T T0, T... Ts>
    constexpr bool isConstant(Sequence<T, T0, Ts...>)
    {
      return (... && (T0 == Ts));
    }

    /**Mark one element and empty sequences as pairwise equal.
     */
    template<class T>
    constexpr bool isConstant(Sequence<T>)
    {
      return true;
    }

    /**@return @c true if the sequence is a consecutive sequence
     * without holes.
     */
    template<class T, T I, T... Rest>
    constexpr bool isConsecutive(Sequence<T, I, Rest...> seq)
    {
      return seq == MakeSequence<T, sizeof...(Rest)+1, I>{};
    }

    /**@return @c true if the sequence is a consecutive sequence
     * without holes.
     */
    template<class T>
    constexpr bool isConsecutive(Sequence<T>)
    {
      return true;
    }

    /**@return @c true if the sequence is sorted.*/
    template<template<std::size_t, std::size_t> class DoSwap, class T, T... Ts>
    constexpr bool isSorted(Sequence<T, Ts...>)
    {
      return IsSortedV<Sequence<T, Ts...>, DoSwap>;
    }

    /**@return @c true if the sequence is sorted.*/
    template<class T, T... Ts>
    constexpr bool isSorted(Sequence<T, Ts...> seq)
    {
      return isSorted<DefaultDoSwap>(seq);
    }

    /**@return @c true for a simple consecutive sequence starting at
     * 0.
     */
    template<class T, T... V>
    constexpr bool isSimple(Sequence<T, V...> seq)
    {
      return seq == MakeSequence<T, sizeof...(V)>{};
    }

    /**@return @c true if the given sequence is consecutive and start
     * aligned to @a Align.
     */
    template<std::size_t Align, class T, T V0, T... V>
    constexpr bool isAlignedBlock(Sequence<T, V0, V...>, IndexConstant<Align> = IndexConstant<Align>{})
    {
      constexpr std::size_t start = V0/Align*Align;
      constexpr std::size_t size = (1+sizeof...(V))/Align*Align;

      return std::is_same<MakeSequence<T, size, start>, Sequence<T, V0, V...> >::value;
    }

    template<std::size_t Align, class T>
    constexpr bool isAlignedBlock(Sequence<T>, IndexConstant<Align> = IndexConstant<Align>{})
    {
      return true;
    }


    /**@return @c true if the sequence is a permutation of the
     * sequence <0, 1, 2, ...>.
     */
    template<std::size_t... Ind>
    constexpr bool isPermutation(IndexSequence<Ind...> = IndexSequence<Ind...>{})
    {
      constexpr std::size_t size = sizeof...(Ind);
      return (... &&(Ind >= 0)) && (... && (Ind < size)) && ((0 + ... + Ind) == (size-1)*size/2);
    }

    /**Decide whether the given parameter pack contains only integral types.*/
    template<class... T>
    constexpr auto isIntegralPack(T&&...)
    {
      return BoolConstant<(... && std::is_integral<std::decay_t<T> >{})>{};
    }

    /**Decide whether the given parameter pack contains only integral types.*/
    template<class... T>
    using IsIntegralPack = decltype(isIntegralPack(std::declval<T>()...));

    /**Decide whether the given tuple contains only integral types.*/
    template<class T, std::size_t N>
    constexpr auto isIntegralTuple(const std::array<T, N>&)
    {
      return std::is_integral<T>{};
    }

    /**Decide whether the given tuple contains only integral types.*/
    template<class T1, class T2>
    constexpr auto isIntegralTuple(const std::pair<T1, T2>&)
    {
      return IsIntegralPack<T1, T2>{};
    }

    /**Decide whether the given tuple contains only integral types.*/
    template<class... T>
    constexpr auto isIntegralTuple(const std::tuple<T...>&)
    {
      return IsIntegralPack<T...>{};
    }

    /**Decide whether the given tuple contains only integral types.*/
    template<class T>
    using IsIntegralTuple = decltype(isIntegralTuple(std::declval<T>()));

    namespace {
      template<class Tuple>
      constexpr bool isConstantTupleHelper(Tuple&&, IndexSequence<>)
      {
        return true;
      }

      template<class Tuple, std::size_t First, std::size_t... Rest>
      constexpr bool isConstantTupleHelper(Tuple&& t, IndexSequence<First, Rest...>)
      {
        return (... && (0 == get<First>(std::forward<Tuple>(t)) - get<Rest>(std::forward<Tuple>(t))));
      }

    }

    /**@c true if all elements of the tuple compare equal.*/
    template<class Tuple, std::enable_if_t<IsTupleLike<Tuple>::value, int> = 0>
    constexpr bool isConstant(Tuple&& t)
    {
      return isConstantTupleHelper(std::forward<Tuple>(t), MakeIndexSequence<size<Tuple>()>{});
    }

    namespace {

      template<class Tuple, template<class...> class Predicate,
               std::size_t start = 0,
               std::size_t size = size<Tuple>()>
      struct PredicateMatchHelper
        : public ConditionalType<Predicate<TupleElement<start, Tuple> >::value,
                                 TrueType,
                                 PredicateMatchHelper<Tuple, Predicate, start+1> >
      {
        static constexpr std::ptrdiff_t index_ =
          Predicate<TupleElement<start, Tuple> >::value ? start : PredicateMatchHelper<Tuple, Predicate, start+1>::index_;
      };

      template<class Tuple, template<class...> class Predicate, std::size_t size>
      struct PredicateMatchHelper<Tuple, Predicate, size, size>
        : FalseType
      {
        static constexpr std::ptrdiff_t index_ = -1;
      };

    }

    /**Apply the predicate to each tuple type in turn. Derive from
     * TrueType if any type matches, otherwise derive from
     * FalseType.
     */
    template<class Tuple, template<class...> class Predicate, class... Rest>
    using PredicateMatch = PredicateMatchHelper<std::decay_t<Tuple>, PredicateProxy<Predicate, Rest...>::template ForwardFirst>;

    /**Obtain the index of the first matching tuple type.*/
    template<template<class...> class Predicate, class... T>
    using IndexOfMatching = IndexConstant<PredicateMatch<std::tuple<T...>, Predicate>::index_>;

    /**TrueType if any type matches the predicate.*/
    template<template<class...> class Predicate, class... T>
    using AnyIs = BoolConstant<(... || Predicate<T>::value)>;

    /**TrueType if all types match the predicate.*/
    template<template<class...> class Predicate, class... T>
    using AllAre = BoolConstant<(... && Predicate<T>::value)>;

    /**Instantiates to Predicate applied to the N-th element of T...*/
    template<std::size_t N, template<class...> class Predicate, class... T>
    using NthIs = Predicate<TypePackElement<N, T...> >;

    //!@} TupleUtilities

    //!@} C++-Templates

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_MPL_COMPARE_HH__
