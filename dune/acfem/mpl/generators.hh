#ifndef __DUNE_ACFEM_MPL_GENERATORS_HH__
#define __DUNE_ACFEM_MPL_GENERATORS_HH__

#include "../common/types.hh"
#include "size.hh"
#include "makesequence.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup C++-Templates
     *
     * @{
     */

    /**@addtogroup TupleUtilities
     *
     * Some additional utitliy functions for tuple-likes and
     * integer-sequences.
     *
     * @{
     */

    /**@addtogroup SequenceGenerators
     *
     * Generate integer sequences.
     *
     * @{
     */

    /**Make a sequence of std::size_t elements. See MakeSequence.*/
    template<std::size_t N, std::ptrdiff_t Offset = 0, std::ptrdiff_t Stride = 1L, std::size_t Repeat = 1>
    using MakeIndexSequence = MakeSequence<std::size_t, N, Offset, Stride, Repeat>;

    /**Make a simple index sequence of the size of the argument, which
     * may be anything for which size<T>() evaluates to a constant
     * non-negative integer.
     */
    template<class T>
    using MakeSequenceFor = MakeIndexSequence<size<T>()>;

    /**Generate a constant index sequence of the given size.*/
    template<std::size_t N, std::size_t Value>
    using MakeConstantSequence = MakeIndexSequence<N, Value, 0>;

    /**Generate a simple reverse sequence.*/
    template<std::size_t N, std::ptrdiff_t Offset = 0, std::size_t Repeat = 1>
    using MakeReverseSequence = MakeIndexSequence<N, (ptrdiff_t)N-1 + Offset, -1, Repeat>;

    //!@} SequenceGenerators

    //!@} TupleUtilities

    //!@} C++-Templates

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_MPL_GENERATORS_HH__
