#ifndef __DUNE_ACFEM_MPL_SEQUENCESORT_HH__
#define __DUNE_ACFEM_MPL_SEQUENCESORT_HH__

#include "generators.hh"
#include "typetuplesort.hh"

namespace Dune::ACFem::MPL {

  namespace {

    template<std::size_t I0, std::size_t I1>
    struct IndexPair
    {
      static constexpr std::size_t i0 = I0;
      static constexpr std::size_t i1 = I1;
    };

    template<class Tuple>
    struct UnpackSortData;

    template<std::size_t... I, std::size_t... P>
    struct UnpackSortData<TypeTuple<IndexPair<I, P>...> >
    {
      using Result = IndexSequence<I...>;
      using Permutation = IndexSequence<P...>;
    };

  }

  template<class Seq, template<std::size_t, std::size_t> class DoSwap, class Permutation = MakeSequenceFor<Seq> >
  struct SortSequence;

  template<std::size_t... I,  std::size_t... P, template<std::size_t, std::size_t> class DoSwap>
  struct SortSequence<IndexSequence<I...>, DoSwap, IndexSequence<P...> >
  {
    template<class A, class B>
    using Swap = DoSwap<A::i0, B::i0>;

    using SortData = TypeTuple<IndexPair<I, P>...>;
    using SortInfo = MergeSort<SortData, Swap>;
    using Result = typename UnpackSortData<SortInfo>::Result;
    using Permutation = typename UnpackSortData<SortInfo>::Permutation;
  };

  template<class Seq, template<std::size_t, std::size_t> class DoSwap>
  struct IsSorted;

  template<std::size_t... I,  template<std::size_t, std::size_t> class DoSwap>
  struct IsSorted<IndexSequence<I...>, DoSwap>
  {
    template<class A, class B>
    using Swap = DoSwap<A::value, B::value>;

    static constexpr bool value = IsSortedV<TypeTuple<IndexConstant<I>...>, Swap>;
  };

} // Dune::ACFem::MPL

#endif // __DUNE_ACFEM_MPL_SEQUENCESORT_HH__
