#ifndef __DUNE_ACFEM_MPL_ACCUMULATE_HH__
#define __DUNE_ACFEM_MPL_ACCUMULATE_HH__

#include "transform.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup C++-Templates
     *
     * @{
     */

    /**@addtogroup TupleUtilities
     *
     * Some additional utitliy functions for tuple-likes and
     * integer-sequences.
     *
     * @{
     */

    /**@addtogroup SequenceAccumulate
     *
     * Accumulate over all elements of a sequence using functors.
     *
     * @{
     */

    namespace {
      template<class Sequence, class Functor>
      struct AccumulateSequenceHelper;

      template<class T, T I, T... Rest, class F>
      struct AccumulateSequenceHelper<Sequence<T, I, Rest...>, F>
      {
        static const T value =
          F::template Apply<T, I, AccumulateSequenceHelper<Sequence<T, Rest...>, F>::value>::value;
      };

      template<class T, class F>
      struct AccumulateSequenceHelper<Sequence<T>, F>
      {
        static constexpr T value = F::template NeutralValue<T>::value;
      };
    }

    /**Accumulate the values of the sequence according to the supplied
     * functor. The functor must define an "Apply"-template of the
     * following type, where the @c neutralValue is the result value
     * for the empty sequence and the value the last element of the
     * sequence will be combined with. @c Front is the current
     * element, @c Tail the resulting value obtained by accumulating
     * the rest of the sequence. T will be TYPE when instantiating the
     * Apply alias.
     *
     * @code
     * struct Functor
     * {
     *   template<class T>
     *   using NeutralValue = Constant<T, SOMETHING>;
     *
     *   template<class T, T Front, T Tail>
     *   using Apply = IntegerConstant<TYPE, EXPRESSION(Front, Tail)>;
     * };
     * @endcode
     *
     * @note AccumulateSequence is rather general. For simple things
     * it is probably better to use the builtin fold expressions of
     * the compiler.
     */
    template<class F, class Seq>
    using AccumulateSequence = Constant<typename Seq::value_type, AccumulateSequenceHelper<Seq, F>::value>;

    /**@addtogroup SequenceAccumulateFunctors
     *
     * Pre-defined functors as argument to the AccumulateSequence template.
     *
     * @{
     */
    struct LogicalAndFunctor
    {
      template<class T>
      using NeutralValue = Constant<T, true>;

      template<class T, T I1, T I2>
      using Apply = BoolConstant<I1 && I2>;
    };

    struct LogicalOrFunctor
    {
      template<class T>
      using NeutralValue = Constant<T, false>;

      template<class T, T I1, T I2>
      using Apply = BoolConstant<I1 || I2>;
    };

    struct BitwiseAndFunctor
    {
      //!Empty sequence will AND-up full bit-mask.
      template<class T>
      using NeutralValue = Constant<T, T(~0UL)>;

      template<class T, T I1, T I2>
      using Apply = Constant<T, I1 & I2>;
    };

    struct BitwiseOrFunctor
    {
      //!Empty sequence will OR-up to zero bit-mask.
      template<class T>
      using NeutralValue = Constant<T, T(0UL)>;

      template<class T, T I1, T I2>
      using Apply = Constant<T, I1 | I2>;
    };

    struct MinFunctor
    {
      template<class T>
      using NeutralValue = Constant<T, std::numeric_limits<T>::max()>;

      template<class T, T I1, T I2>
      using Apply = Constant<T, std::min(I1, I2)>;
    };

    struct MaxFunctor
    {
      template<class T>
      using NeutralValue = Constant<T, std::numeric_limits<T>::min()>;

      template<class T, T I1, T I2>
      using Apply = Constant<T, std::max(I1, I2)>;
    };

    struct AddFunctor
    {
      template<class T>
      using NeutralValue = Constant<T, T(0)>;

      template<class T, T I1, T I2>
      using Apply = Constant<T, I1 + I2>;
    };

    struct ProdFunctor
    {
      template<class T>
      using NeutralValue = Constant<T, T(1)>;

      template<class T, T I1, T I2>
      using Apply = Constant<T, I1 * I2>;
    };

    template<class F, F V>
    struct MultiplyAddFunctor
    {
      template<class T>
      using NeutralValue = Constant<T, T(0)>;

      template<class T, T Front, T Rest>
      using Apply = Constant<T, Front + V * Rest>;
    };

    /**@name AccumulateFoldExpressions
     *
     * Accumulate Sequences with fold expressions.
     *
     * @{
     */

    template<class T, T... Ts>
    constexpr bool logicalAnd(Sequence<T, Ts...>)
    {
      return (... && ((bool)Ts));
    }

    template<class T, T... Ts>
    constexpr bool logicalOr(Sequence<T, Ts...>)
    {
      return (... || ((bool)Ts));
    }

    template<class T, T... Ts>
    constexpr T bitwiseAnd(Sequence<T, Ts...>)
    {
      return (T(~0UL) & ... & ((bool)Ts));
    }

    template<class T, T... Ts>
    constexpr T bitwiseOr(Sequence<T, Ts...>)
    {
      return (T(0) | ... | ((bool)Ts));
    }

    template<class T, T... Ts>
    constexpr T sum(Sequence<T, Ts...>)
    {
      return (T(0) + ... + Ts);
    }

    template<class T, T... Ts>
    constexpr T prod(Sequence<T, Ts...>)
    {
      return (T(1) * ... * Ts);
    }

    template<class T, T... Ts>
    constexpr T max(Sequence<T, Ts...>)
    {
      return AccumulateSequence<MaxFunctor, Sequence<T, Ts...> >::value;
    }

    template<class T, T... Ts>
    constexpr T min(Sequence<T, Ts...>)
    {
      return AccumulateSequence<MinFunctor, Sequence<T, Ts...> >::value;
    }

    template<class Seq>
    using LogicalAnd = BoolConstant<logicalAnd(Seq{})>;

    template<class Seq>
    using LogicalOr = BoolConstant<logicalOr(Seq{})>;

    template<class Seq>
    using BitwiseAnd = Constant<typename Seq::value_type, bitwiseAnd(Seq{})>;

    template<class Seq>
    using BitwiseOr = Constant<typename Seq::value_type, bitwiseOr(Seq{})>;

    template<class Seq>
    using Sum = Constant<typename Seq::value_type, sum(Seq{})>;

    template<class Seq>
    using Prod = Constant<typename Seq::value_type, prod(Seq{})>;

    template<class Seq>
    using Min = Constant<typename Seq::value_type, min(Seq{})>;

    template<class Seq>
    using Max = Constant<typename Seq::value_type, max(Seq{})>;

    //!@} AccumulateFoldExpressions

    //!@} SequenceAccumulateFunctors

    //!@} SequenceAccumulate

    //!@} TupleUtilities

    //!@} C++-Templates

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_MPL_ACCUMULATE_HH__
