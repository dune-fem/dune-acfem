#ifndef __DUNE_ACFEM_MPL_ASSIGN_HH__
#define __DUNE_ACFEM_MPL_ASSIGN_HH__

#include <tuple>
#include <array>

#include "access.hh"
#include "foreach.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup C++-Templates
     *
     * @{
     */

    /**@addtogroup TupleUtilities
     *
     * Some additional utitliy functions for tuple-likes and
     * integer-sequences.
     *
     * @{
     */

    /**@addtogroup SequenceElementAccess
     *
     * Random access to sequence elements.
     *
     * @{
     */

    /**Assign one tuple-alike to another by looping over the elements.*/
    template<class T1, class T2,
             std::enable_if_t<(IsTupleLike<T1>::value
                               && IsTupleLike<T2>::value
                               && size<T1>() == size<T2>())
                              , int> = 0>
    auto& assign(T1& t1, T2&& t2)
    {
      forLoop<size<T1>()>([&] (auto i) {
          std::get<i>(t1) = std::get<i>(t2);
        });
      return t1;
    }

    //!@} SequenceElementAccess

    //!@} TupleUtilities

    //!@} C++-Templates

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_MPL_ASSIGN_HH__
