#ifndef MARKING_HH
#define MARKING_HH

#include "../common/entitystorage.hh"

namespace Dune {

namespace ACFem {

template<class GridPart>
class MarkingStrategy
{
 protected:
  using EntityStorageType = EntityStorage<typename GridPart::GridPartType, double>;
 public:
  MarkingStrategy(GridPart& gridPart, const EntityStorageType& estimates, const std::string& name = "adaptation")
    : name_(name), gridPart_(gridPart), estimates_(estimates)
  {}

  //! mark all elements due to given tolerance
  bool mark (const double tolerance) const
  {
    // possible strategies
    enum Strategy { none = 0, maximum = 1, equitol = 2, equiv = 3, uniform = 4 };
    static const std::string strategyNames []
      = { "none", "maximum", "equitolerance", "equidistribution", "uniform" };
    //default maximum
    Strategy strategy = (Strategy) Dune::Fem::Parameter::getEnum(name_ + "." + "markingStrategy", strategyNames, 1);

    double localTol2 = 0;
    double coarseTol2 = std::numeric_limits<double>::max();

    int refineBisections = 1;
    int coarseBisections = 1;

    switch(strategy) {
    case maximum: {
      double maxError2 = 0;
      localTol2 = Dune::Fem::Parameter::getValue<double>(name_ + ".maximum.refinetol",0.9);
      coarseTol2 = Dune::Fem::Parameter::getValue<double>(name_ + ".maximum.coarsetol",0.1);

      localTol2 *= localTol2;
      coarseTol2 *= coarseTol2;

      // maximum of local estimates
      for (auto x : estimates_) {
        maxError2 = std::max(x, maxError2);
      }

      // get global maxError
      maxError2 = estimates_.grid().comm().max(maxError2);
      localTol2 *= maxError2;
      coarseTol2 *= maxError2;

      if (Dune::Fem::Parameter::verbose()) {
        std::cerr << "max: " << maxError2 << std::endl;
        std::cerr << "local: " << localTol2 << std::endl;
        std::cerr << "coarse: " << coarseTol2 << std::endl;
      }
      break;
    }

    case equitol: { // try to equidistribute the given tolerance
      // sum up local estimates
      size_t indSize = estimates_.size();

      // get global number of elements
      estimates_.grid().comm().sum(&indSize, 1);

      double avg2 = tolerance * tolerance / indSize;

      localTol2 = Dune::Fem::Parameter::getValue<double>(name_ + ".equitol.refinetol",0.9);
      coarseTol2 = Dune::Fem::Parameter::getValue<double>(name_ + ".equitol.coarsetol",0.1);

      localTol2 *= localTol2 * avg2;
      coarseTol2 *= localTol2 * avg2;

      if (Dune::Fem::Parameter::verbose()) {
        std::cerr << "squared mean tolerance: " << avg2 << std::endl;
        std::cerr << "local: " << localTol2 << std::endl;
        std::cerr << "coarse: " << coarseTol2 << std::endl;
      }
      break;
    }

    case equiv: { //independent form given tolerance
      double sumError2 = 0;
      // sum up local estimates
      size_t indSize = estimates_.size();
      for (size_t i = 0; i < indSize; ++i)  {
       sumError2 += estimates_[i];
      }
      // get global sum of number of elements and local error sum
      double buffer[2] = { (double)indSize , sumError2 };
      estimates_.grid().comm().sum(buffer, 2);

      sumError2 = buffer[1];
      indSize   = buffer[0];

      double avg2 = sumError2 / indSize;

      localTol2 = Dune::Fem::Parameter::getValue<double>(name_ + ".equiv.refinetol",0.9);
      coarseTol2 = Dune::Fem::Parameter::getValue<double>(name_ + ".equiv.coarsetol",0.1);

      localTol2 *= localTol2 * avg2;
      coarseTol2 *= coarseTol2 * avg2;

      if (Dune::Fem::Parameter::verbose()) {
        std::cerr << "estimate: " << std::sqrt(sumError2) << std::endl;
        std::cerr << "squared mean: " << avg2 << std::endl;
        std::cerr << "local: " << localTol2 << std::endl;
        std::cerr << "coarse: " << coarseTol2 << std::endl;
      }
      break;
    }

    case uniform: {
      double sumError2 = 0;
      // sum up local estimates
      size_t indSize = estimates_.size();
      for (size_t i=0; i < indSize; ++i)  {
       sumError2 += estimates_[i];
      }
      // get global sum of number of elements and local error sum
      double buffer[2] = { (double)indSize , sumError2 };
      estimates_.grid().comm().sum(buffer, 2);

      sumError2 = buffer[1];
      indSize   = buffer[0];

      double coarseTolUnif = Dune::Fem::Parameter::getValue<double>(name_ + "uniform.coarsetol",0.05);
      coarseTolUnif *= coarseTolUnif;

      refineBisections = coarseBisections = Dune::DGFGridInfo<typename GridPart::GridType>::refineStepsForHalf();

      // if global sum > tol, then refine all
      if (sumError2 > tolerance*tolerance ) {
        localTol2 = -1;
        coarseTol2 = -1;
      }
      // if global sum < tol*coarsenToleranceUniform (default 0.05), then coarse all
      else if (sumError2 < tolerance*tolerance*coarseTolUnif ) {
        localTol2 = std::numeric_limits<double>::max();
        coarseTol2 = std::numeric_limits<double>::max();
      }
      // else do nothing
      else {
        localTol2 = std::numeric_limits<double>::max();
        coarseTol2 = -1;
      }
      break;
    }

    case none: {
      // settings to do nothing
      localTol2 = std::numeric_limits<double>::max();
      coarseTol2 = -1;
      break;
    }
    default:
      break;
    }

    int marked = 0;
    int notmarked = 0;
    int coarseMarked = 0;
    // loop over all elements
    auto end = gridPart_.template end<0>();
    for (auto it = gridPart_.template begin<0>(); it != end; ++it) {
      auto& entity = *it;
      // check local error indicator
      if (estimates_[entity] > localTol2) {
        // mark entity for refinement
        gridPart_.grid().mark(refineBisections, entity);
        //std::cerr << "refineBisections: " << refineBisections << std::endl;
        // grid was marked
        ++marked;
      } else if (estimates_[entity] < coarseTol2) {
        gridPart_.grid().mark(-coarseBisections, entity);
        ++coarseMarked;
      } else {
        ++notmarked;
      }
    }

    // get globally marked
    int comarray[] = { marked, coarseMarked, notmarked };
    estimates_.grid().comm().sum(comarray, 3);
    marked = comarray[0];
    coarseMarked = comarray[1];
    notmarked = comarray[2];

    if (Dune::Fem::Parameter::verbose()) {
      std::cout << "Marked   Elements:   " << marked << std::endl;
      std::cout << "Coarsen Marked:      " << coarseMarked << std::endl;
      std::cout << "Elements not marked: " << notmarked << std::endl;
    }

    return bool(marked + coarseMarked);
  }

 protected:
  const std::string name_;
  GridPart& gridPart_;
  const EntityStorageType& estimates_;
};

} // ACFem
} // Dune

#endif
