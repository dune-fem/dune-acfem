/**@file
 * Abstract adaptive scheme. Mmmh. Rather empty ...
 *
 */
#ifndef __FEMSCHEME_INTERFACE_HH__
#define __FEMSCHEME_INTERFACE_HH__

/*********************************************************/


#include <cstddef>
#include <string>

namespace Dune {

  namespace ACFem {

    /**@addtogroup Algorithms
     *
     * Abstract FEM algorithms and schemes.
     *
     * @{
     */

    //! Abstract non-adaptive basic FEM scheme. Used as solver
    //! black-box, so to say.
    class BasicFemScheme
    {
     public:
      //! initialize the solution
      virtual void initialize() = 0;

      //! name of the Fem Scheme
      virtual std::string name() const = 0;

      /** Solve the system.
       *
       * @param[in] forceMatrixAssembling If the solve step requires
       * assembling of matrices, then by default the application may
       * give a hint to the FemScheme that this might not be
       * necessary. The actual implementation can then decide if it
       * caches the assembled matrix from the last solve() invocation
       * and reuses it. If @a forceAssemble is @c true then the
       * implementation has to discard and cached assembled matrices
       * and recompute them.
       */
      virtual void solve(bool forceMatrixAssembling = true) = 0;

      //! data I/O, return -1 if no data has been written otherwise the
      //! sequence number of the file
      virtual int output() = 0;

      //! check whether solver has converged
      virtual bool converged() const = 0;

      //! calculate residual (in small l^2)
      virtual double residual() const = 0;

      //! calculate L2/H1 error
      virtual double error() const = 0;

      //! return some measure about the number of DOFs in use
      virtual size_t size() const = 0;
    };

    //! Abstract space adaptative FEM scheme. Also used as building-block
    //! for transient adaptive schemes.
    class AdaptiveFemScheme
      : public BasicFemScheme
    {
     public:
      //! mark elements for adaptation
      virtual bool mark(const double tolerance) = 0;

      //! calculate error estimator
      virtual double estimate() = 0;

      //! do the adaptation for a given marking
      virtual void adapt() = 0;
    };

    class TransientAdaptiveFemScheme
      : public AdaptiveFemScheme
    {
     public:
      //! restart from a check-point
      virtual void restart(const std::string& name = "") = 0;

      //! close the current time-step
      virtual void next() = 0;

      //! return the most recent time error estimate
      virtual double timeEstimate() = 0;

      //! initial estimate. If this fails to fulfil a given tolerance,
      //! then initialize() may be called again after adapting the mesh.
      virtual double initialEstimate() = 0;

      //! initial marking strategy
      virtual bool initialMarking(const double tolerance) = 0;

    };

    //!@} Algorithms

  } // ACFem::

} // Dune::

#endif // __FEMSCHEME_INTERFACE_HH__
