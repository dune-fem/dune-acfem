#ifndef __DUNE_ACFEM_ALGORITHMS_STATIONARY_ADAPTIVE_ALGORITHM_HH__
#define __DUNE_ACFEM_ALGORITHMS_STATIONARY_ADAPTIVE_ALGORITHM_HH__

#include <iomanip>
#include <dune/fem/io/parameter.hh>

#include "../common/flops.hh"
#include "../algorithms/femschemeinterface.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup Algorithms
     * @{
     */

    /**Stationary adaptive algorithm like adapt_method_stat() from
     * poor old ALBERTA. This could be moved into a pre-compiled
     * library.
     *
     * @param[in,out] scheme The transient fem-scheme used for the
     * solve-estimate-mark cycles.
     *
     * @param[in] prefix_ A prefix like "poisson" to tag the varous
     * parameters fetched from the parameter file. In particalar the
     * tolerance is fetched from the Fem::Parameter class with
     * "prefix_.adaptation.tolerance" key. Of course the "." is not
     * added if prefix_ is empty.
     *
     * @return A std::pair with pair.first being the final estimator
     * value and pair.second intentionally being the real error for
     * convergence tests. If no exact solution is known (general case)
     * then the distance to some initial guess of the solution. See
     * EllipticFemScheme.
     *
     * @todo Document the relevant parameters fetched from the
     * parameter file.
     */
    static inline
    std::pair<double, double>
    adaptiveAlgorithm(AdaptiveFemScheme& scheme, const std::string& prefix_ = "")
    {
      std::string prefix = prefix_ == std::string("") ? scheme.name() + "." : prefix_ + ".";

      const bool verbose = Fem::Parameter::getValue<int>("fem.verboserank", 0) != -1;
      const bool doFlops = FlopCounter::present() && Fem::Parameter::getValue<bool>("acfem.performanceCounter", false);
      const int prec = std::cout.precision();

      if (verbose) {
        std::cout << std::scientific << std::setprecision(std::min(4, prec));
      }
                // initialize discrete solution
      scheme.initialize();

      // solve once
      scheme.solve();

      // write initial solve
      scheme.output();

      // calculate error
      double error = -1., oldError;
      double estimate = 1., oldEstimate;
      size_t size = 0, oldSize;

      // get tolerance for adaptive algorithm
      const double tolerance = Dune::Fem::Parameter::getValue<double>(prefix+"adaptation.tolerance", 0.1);

      // get error estimate
      estimate = scheme.estimate();

      // TODO: maybe trigger computation of errors by a dedicated
      // flag parameter (also second error computation below)
      if (verbose) {
        error = scheme.error();
        size = scheme.size(); // attention: communication, NOT inside verbose()!!!
        if (Fem::Parameter::verbose()) {
          std::cout << "Estimate/Error: " << estimate << ", " << error << std::endl;
          std::cout << "#DoFs:          " << size << std::endl;
        }
      }

      // until estimator is below tolerance for *THE LOOP*
      while (estimate > tolerance) {
        if (verbose && doFlops) {
          FlopCounter::start();
        }

        // mark element for adaptation
        scheme.mark(tolerance);

        // adapt grid
        scheme.adapt();

        // solve again
        scheme.solve();

        if (verbose && doFlops) {
          FlopCounter::stop();
          std::cout << "FLOPS (mark/adapt/solve)" << std::endl;
          FlopCounter::print(std::cout);
        }

        // data I/O
        scheme.output();

        // save old estimate
        oldEstimate = estimate;

        if (verbose && doFlops) {
          FlopCounter::start();
        }

        // calculate new error
        estimate = scheme.estimate();

        if (verbose && doFlops) {
          FlopCounter::stop();
          std::cout << "FLOPS (estimate)" << std::endl;
          FlopCounter::print(std::cout);
        }

        // TODO: maybe trigger computation of errors by a dedicated
        // flag parameter (also second error computation below)
        if (verbose) {
          oldError = error;
          error = scheme.error();
          oldSize = size;
          size = scheme.size(); // attention: communication, NOT inside Parameter::verbose()!!!
          if (Fem::Parameter::verbose()) {
            std::cout << "Estimate/Error: " << estimate << ", " << error << std::endl;
            std::cout << "#DoFs:          " << size << std::endl;
            if (oldEstimate > 0) {
              double estimatorRatio = oldEstimate/estimate;
              double errorRatio = oldError/error;
              double sizeRatio = (double)size/(double)oldSize;

              std::cout << "DoF EOC:     "
                        << std::log(estimatorRatio)/std::log(sizeRatio)
                        << ", "
                        << std::log(errorRatio)/std::log(sizeRatio)
                        << std::endl;
            }
          }
        }
      }

      if (!verbose) {
        error = scheme.error();
      }

      if (verbose) {
        std::cout << std::scientific << std::setprecision(prec);
      }

      return std::pair<double,double>(estimate, error);
    }

    //!@} Algorithms

  } // ACFem

} // Dune

#endif //  __DUNE_ACFEM_ALGORITHMS_STATIONARY_ADAPTIVE_ALGORITHM_HH__
