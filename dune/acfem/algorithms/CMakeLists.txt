set(acfemalgorithmsdir  ${CMAKE_INSTALL_INCLUDEDIR}/dune/acfem/algorithms)
set(acfemalgorithms_HEADERS
 femschemeinterface.hh
 ellipticfemscheme.hh
 parabolicfemscheme.hh
 marking.hh
 transientadaptivealgorithm.hh)
set(SUBDIRS test)
# include not needed for CMake
# include $(top_srcdir)/am/global-rules
install(FILES ${acfemalgorithms_HEADERS} DESTINATION ${acfemalgorithmsdir})
foreach(i ${SUBDIRS})
  if(${i} STREQUAL "test")
    set(opt EXCLUDE_FROM_ALL)
  endif(${i} STREQUAL "test")
  add_subdirectory(${i} ${opt})
  unset(opt)
endforeach(i ${SUBDIRS})
