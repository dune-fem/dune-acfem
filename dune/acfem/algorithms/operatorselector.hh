#ifndef __ACFEM_OPERATORSELECTOR_HH__
#define __ACFEM_OPERATORSELECTOR_HH__

#include <dune/fem/space/common/capabilities.hh>

#include "modelparameters.hh"
#include "../common/literals.hh"
#include "../models/modules/nitschedirichletmodel.hh"

namespace Dune {

  namespace ACFem {

    using namespace Literals;

    /**@addtogroup Algorithms
     *
     * @{
     */

    /**@internal Choice DG methods
     *
     */
    enum DGFlavour { sip, iip, nip };

    /**@internal
     *
     */
    template<class DiscreteFunctionSpace, class Model, DGFlavour flavour = sip>
    struct OperatorTypeSelector
    {
     private:
      static const bool isContinuous = Dune::Fem::Capabilities::isContinuous<DiscreteFunctionSpace>::v;
      static const bool hasFlux = ModelMethodExists<Model, PDEModel::flux>::value;
      static const bool isSymmetric = ExpressionTraits<Model>::isSymmetric && flavour == sip;
     public:
      static const bool useDG = hasFlux && !(isContinuous);
      static const DGFlavour dgFlavour = flavour;
      static const int sFormDG = (dgFlavour == sip) ? 1 : (dgFlavour == nip ? -1 : 0);
      static auto dgPenalty()
      {
        if constexpr (useDG) {
          return ModelParameters::dgPenalty();
        } else {
          return 0_f;
        }
      }
    };

    template<class Model, class DiscreteSpace, class Symmetrize = PDEModel::SkeletonSymmetrizeDefault<Model>,
             std::enable_if_t<(IsPDEModel<Model>::value
                               && OperatorTypeSelector<DiscreteSpace, Model>::useDG
                               && IsSign<Symmetrize>::value
      ), int> = 0>
    auto discreteModel(Model&& m, const DiscreteSpace& space, Symmetrize&& = Symmetrize{})
    {
      return nitscheDirichletModel(std::forward<Model>(m), space.gridPart(),
                                   OperatorTypeSelector<DiscreteSpace, Model>::dgPenalty(), Symmetrize{});
    }

    template<class Model, class DiscreteSpace, class Symmetrize = PDEModel::SkeletonSymmetrizeDefault<Model>,
             std::enable_if_t<(IsPDEModel<Model>::value
                               && !OperatorTypeSelector<DiscreteSpace, Model>::useDG
                               && IsSign<Symmetrize>::value
      ), int> = 0>
    constexpr decltype(auto) discreteModel(Model&& m, const DiscreteSpace& space, Symmetrize&& = Symmetrize{})
    {
      return std::forward<Model>(m);
    }

    template<class Model, class DiscreteSpace, class Symmetrize = PDEModel::SkeletonSymmetrizeDefault<Model> >
    using DiscreteModel = std::decay_t<
      decltype(
        asExpression(
          discreteModel(std::declval<Model>(), std::declval<DiscreteSpace>(), Symmetrize{})))>;

    /**@}  */

  }//namespace ACFem

}//namespace Dune

#endif //__ACFEM_OPERATORSELECTOR_HH__
