#! /bin/sh

env
set -x
# Environment:
# buildir
# srcdir

MODE="${1}"
TEST="${2}"
EXPECTED="${srcdir}/${TEST}.expected"
GENERATED="${builddir}/${TEST}.generated"
DIFFERENCE="${builddir}/${TEST}.diff"

do_exit='rm -f ${GENERATED} ${DIFFERENCE}; (exit $st); exit $st'
trap "st=129; $do_exit" 1
trap "st=130; $do_exit" 2
trap "st=141; $do_exit" 13
trap "st=143; $do_exit" 15

if [ "${MODE}" = check ]; then
    test -f ${EXPECTED} || exit 77
fi
${builddir}/${TEST} 2>&1 | grep -E "Error\\s+:" | sed -e 's/-nan/nan/g' -e 's/-nan/nan/g'  > ${GENERATED}
TESTERROR=$?
if [ "${MODE}" = generate ]; then
    if [ ${TESTERROR} -ne 0 ]; then
	echo "Test program failed to run, not generating ${EXPECTED}" 1>&2
	exit 1
    fi
    mv ${GENERATED} ${EXPECTED}
    exit 0
fi
numdiff -a 1e-10 ${EXPECTED} ${GENERATED} > ${DIFFERENCE}
DIFFERROR=$?

DIFF=$(cat ${DIFFERENCE})

# remove all mess in success and exit with success
if [ ${TESTERROR} -eq 0 ] && [ ${DIFFERROR} -eq 0 ]; then
    rm -f ${GENERATED} ${DIFFERENCE}
    exit 0 # hurray!
fi

# program exited with error
if [ ${TESTERROR} -ne 0 ]; then
    rm -f ${DIFFERENCE} # not of much use
    exit 99 # hard error
fi

# program exited with error
if [ ${DIFFERROR} -ne 0 ]; then
    exit 1 # failed
fi

# otherwise ????? we do not reach this point!
