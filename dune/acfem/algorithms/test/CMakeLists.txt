# set default grid type
#set(GRIDTYPE YASPGRID)
#set(GRIDTYPE PARALLELGRID_ALUGRID_SIMPLEX)
#set(GRIDTYPE ALUGRID_SIMPLEX)
set(GRIDTYPE ALUGRID_CONFORM)
#set(GRIDTYPE ALUGRID_CUBE)
#set(GRIDTYPE ALBERTAGRID)
set(GRIDDIM 2)
set(POLORDER 2)
set(WANT_ISTL 1 CACHE BOOL "use ISTL")
set(WANT_PETSC 0 CACHE BOOL "use PETSC")

add_definitions(
 -D${GRIDTYPE}
 -DPOLORDER=${POLORDER}
 -DGRIDDIM=${GRIDDIM}
 -DWANT_ISTL=${WANT_ISTL}
 -DWANT_PETSC=${WANT_PETSC}
 -DSRCDIR=\"${CMAKE_CURRENT_SOURCE_DIR}\"
)

set(TESTPROGRAMS
  poisson
  poisson-cg
  poisson-dg
  poisson-nitsche-dirichlet
  poisson-l2load
  poisson-ritzload
  )
#  heat not yet

#set(poisson-dg-TIMEOUT 30)
set(poisson-dg-DEFINITIONS "-UPOLORDER -DPOLORDER=3")
set(poisson-cg-DEFINITIONS "-UPOLORDER -DPOLORDER=3 -DNO_DG=1")

set(TESTDRIVER ${CMAKE_CURRENT_SOURCE_DIR}/nompicheck.sh)
set(HEATDRIVER ${CMAKE_CURRENT_SOURCE_DIR}/heatcheck.sh)

add_custom_target(algorithm-expectations)
add_custom_target(algorithm-tests)

# for-loop for the case of multiple test over EOC tests
set(TESTEXPECTS "")
foreach(i ${TESTPROGRAMS})
  if(${i} STREQUAL poisson-cg)
    add_executable(${i} poisson-dg.cc)
  else()
    add_executable(${i} ${i}.cc)
  endif()
  add_dune_acfem_papi_flags(${i})
  dune_target_enable_all_packages(${i})
  if(${GRIDTYPE} STREQUAL ALBERTAGRID)
    add_dune_alberta_flags(${i} WORLDDIM ${GRIDDIM})
  endif()
  if(NOT ${i}-TIMEOUT)
    set(${i}-TIMEOUT 300)
  endif()
  if(${i} STREQUAL heat)
    set(COMMAND ${HEATDRIVER})
  else()
    set(COMMAND ${TESTDRIVER})
  endif()
  dune_add_test(NAME ${i}
    TARGET ${i}
    TIMEOUT ${${i}-TIMEOUT}
    COMMAND ${COMMAND}
    CMD_ARGS check ${i})
  set_tests_properties(${i}
    PROPERTIES ENVIRONMENT "srcdir=${CMAKE_CURRENT_SOURCE_DIR};builddir=$<TARGET_FILE_DIR:${i}>;")
  if(${i}-DEFINITIONS)
    set_property(TARGET ${i}
      APPEND_STRING
      PROPERTY COMPILE_FLAGS
      " ${${i}-DEFINITIONS}")
  endif()
  set(TESTEXPECTS ${TESTEXPECTS} ${CMAKE_CURRENT_SOURCE_DIR}/${i}.expected)
  add_custom_target(${i}-expectations
    DEPENDS ${i}
    COMMAND env srcdir=${CMAKE_CURRENT_SOURCE_DIR} builddir=${CMAKE_CURRENT_BINARY_DIR} ${TESTDRIVER} generate ${i})
  add_dependencies(algorithm-tests ${i})
  add_dependencies(algorithm-expectations ${i}-expectations)
endforeach()

SET_DIRECTORY_PROPERTIES(PROPERTIES
  ADDITIONAL_MAKE_CLEAN_FILES "acfem-expression-profile.dump")
