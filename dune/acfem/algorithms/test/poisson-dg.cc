#include <config.h>

// iostream includes
#include <iostream>

#include <dune/acfem/common/scopedredirect.hh>

// include header of adaptive scheme
#include <dune/acfem/common/discretefunctionselector.hh>
#include <dune/acfem/functions/basicfunctions.hh>
#include <dune/acfem/models/basicmodels.hh>
#include <dune/acfem/algorithms/ellipticfemscheme.hh>
#include <dune/acfem/algorithms/stationaryadaptivealgorithm.hh>

using namespace Dune::ACFem;

/**Implement the most simple elliptic toy problem for a right hand
 * side constructed from an Gaussian "bell" function, in the sense that
 * the "exact solution" @f$u(x) = e^{-C\,|x|^2}@f$ is used in order
 * to construct all data. The discretization will reproduce the
 * "solution" on the unit-square with consecutive boundary ids, like
 * defined by the following DGF-file:
 *
@code
DGF

Interval
 0   0
 1   1
 1   1
#

GridParameter
% longest or arbitrary (see DGF docu)
refinementedge longest
overlap 0
#

#

#
@endcode
 *
 * The code imposes Dirichlet boundary condition on all
 * boundaries. The bulk-equations reads:
 *
 * \f[
 * -\Delta\,u = 2\,C\,(d-2\,C\,|x|^2)\,e^{-C\,|x|^2}
 * \f]
 *
 * After defining auxiliary variables the discrete model is finally constructed in symbolic notation
 *
 * @code
auto pdeModel = -Delta_U - F + (Dbc0 - gD);
@endcode
 *
 * and then passed via an EllipticFemScheme to an adaptiveAlgorithm():
 *
 * @code
SchemeType scheme(solution, pdeModel, exactSolution);

return adaptiveAlgorithm(scheme);
@endcode
 *
 * The constant @f$C@f$ is chosen as .5 in this example in order to
 * have "numerically non-zero" normal derivatives at the boundary.
 *
 */
template<class HGridType>
std::pair<double, double> algorithm(HGridType &grid)
{
#if !NO_DG
  auto gridPart = leafGridPart<Dune::All_Partition>(grid);
  auto discreteSpace = discreteFunctionSpace(gridPart, dG<POLORDER>);
#else
  auto gridPart = leafGridPart<Dune::InteriorBorder_Partition>(grid);
  auto discreteSpace = discreteFunctionSpace(gridPart, lagrange<POLORDER>);
#endif
  auto solution = discreteFunction(discreteSpace, "solution");

  // Define some basic ingredients
  auto X  = identityGridFunction(gridPart);
  auto X0 = X[0_c];
  auto X1 = X[1_c];

  // auto exactSolution = exp(-C*sqr(X));
  //auto exactSolution = cos(2_f*M::pi*X0)*cos(2_f*M::pi*X1);
  auto exactSolution = cos(2*M_PI*X0)*cos(2*M_PI*X1);
  //auto exactSolution = sin(2*M_PI*X0)*sin(2*M_PI*X1);

  // Dirichlet Boundary Conditions Every Where
  auto gD = exactSolution;

  // homogeneous boundary models
  auto Dbc = dirichletBoundaryModel(gD, EntireBoundaryIndicator{});

  // bulk contributions
  // const unsigned dimDomain = FunctionSpaceType::dimDomain;
  // const double C = 0.5;
  // auto F = 2.0 * C * (dimDomain - 2.0 * C * sqr(X)) * exactSolution;
  //auto F1 = (8_f*M::pi*M::pi + 1_f)*exactSolution;
  auto F = (8*M_PI*M_PI +1)*exactSolution;
  auto DUDPhi = laplacianModel(discreteSpace);
  auto Mass = massModel(DUDPhi);

  // now define the discrete model ...
  auto pdeModel = DUDPhi + Mass - F + Dbc ;

  // create adaptive scheme, with exact solution for testing
  // purposes. The exact solution defaults to the ZeroGridFunction if
  // not specified. The scheme has to be passed as mutable reference
  // to the algorithm
  auto scheme = ellipticFemScheme(solution, pdeModel, exactSolution);

  return adaptiveAlgorithm(scheme);
}

// main
// ----

#ifndef SRCDIR
# define SRCDIR "."
#endif

int main (int argc, char **argv)
try
{
  // initialize MPI, if necessary
  Dune::Fem::MPIManager::initialize(argc, argv);

  //redirect the stream cerr to cout
  //so we only get the output of clog on stderr
  Dune::ACFem::ScopedRedirect redirect(std::cerr, std::cout);

  // reduce precision and use normalized FP output
  std::cout << std::scientific << std::setprecision(3);
  std::cerr << std::scientific << std::setprecision(3);
  std::clog << std::scientific << std::setprecision(3);

  std::cerr << "MPI rank " << Dune::Fem::MPIManager::rank() << std::endl;

  // append overloaded parameters from the command line
  Dune::Fem::Parameter::append(argc, argv);

  // append possible given parameter files
  for (int i = 1; i < argc; ++i)
    Dune::Fem::Parameter::append(argv[ i ]);

  // append default parameter file
  Dune::Fem::Parameter::append(SRCDIR "/parameter");

  // type of hierarchical grid
  //typedef Dune::AlbertaGrid< 2 , 2 > GridType;
  typedef Dune::GridSelector::GridType  HGridType ;

  // create grid from DGF file
  const std::string gridkey = Dune::Fem::IOInterface::defaultGridKey(HGridType::dimension);
  const std::string gridfile = SRCDIR "/" + Dune::Fem::Parameter::getValue<std::string>(gridkey);

  // the method rank and size from MPIManager are static
  if (Dune::Fem::MPIManager::rank() == 0)
    std::clog << "Loading macro grid: " << gridfile << std::endl;

  // construct macro using the DGF Parser
  Dune::GridPtr< HGridType > gridPtr(gridfile);
  HGridType& grid = *gridPtr ;

  // do initial load balance
  grid.loadBalance();

  // initial grid refinement
  const int level = Dune::Fem::Parameter::getValue< int >("ellipt.initialRefinements");

  // number of global refinements to bisect grid width
  const int refineStepsForHalf = Dune::DGFGridInfo< HGridType >::refineStepsForHalf();

  // refine grid
  grid.globalRefine(level * refineStepsForHalf);

  // let it go ... quasi adapt_method_stat()
  std::pair<double,double> estimateError = algorithm(grid);

  std::clog << "Estimated Global Error: " << estimateError.first << std::endl;
  std::clog << "Real Global Error: " << estimateError.second << std::endl;

  return 0;
}
catch(const Dune::Exception &exception)
{
  std::cerr << "Error: " << exception << std::endl;
  return 1;
}
