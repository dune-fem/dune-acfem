#ifndef __DUNE_ACFEM_ALGORITHMS_MODELPARAMETERS_HH__
#define __DUNE_ACFEM_ALGORITHMS_MODELPARAMETERS_HH__

#include <limits>
#include "../parameters/generic.hh"

namespace Dune::ACFem::ModelParameters {

  constexpr inline std::size_t DGPenaltyId = std::numeric_limits<std::size_t>::max() - 0;
  constexpr inline std::size_t NitscheDirichletPenaltyId = std::numeric_limits<std::size_t>::max() - 1;
  constexpr inline std::size_t TimeStepId = std::numeric_limits<std::size_t>::max() - 2;
  // constexpr inline std::size_t RobinFactorId = std::numeric_limits<std::size_t>::max() - 3;

  auto dgPenalty()
  {
    return positiveParameter<DGPenaltyId, double>("acfem.dgpenalty");
  }

  auto nitscheDirichletPenalty()
  {
    return positiveParameter<DGPenaltyId, double>("acfem.nitschedirichletpenalty");
  }

  auto timeStep()
  {
    return positiveParameter<TimeStepId, double>("acfem.timestep");
  }

  // auto robinFactor()
  // {
  //   return positiveParameter<RobinFactorId, double>("acfem.robinfactor");
  // }

}

#endif // __DUNE_ACFEM_ALGORITHMS_MODELPARAMETERS_HH__
