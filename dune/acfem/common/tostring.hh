#ifndef __DUNE_ACFEM_COMMON_TOSTRING_HH__
#define __DUNE_ACFEM_COMMON_TOSTRING_HH__

#include <string>
#include <sstream>

#include <dune/common/streamoperators.hh>
#include "types.hh"
#include "typestring.hh"

namespace Dune
{
  namespace ACFem
  {
    using Dune::operator<<;

    template<class T, class SFINAE = void>
    struct HasStdToString
      : FalseType
    {};

    template<class T>
    struct HasStdToString<T, VoidType<decltype(std::to_string(std::declval<T>()))> >
      : TrueType
    {};

    template<class T, class SFINAE = void>
    struct IsPrintable
      : FalseType
    {};

    template<class T>
    struct IsPrintable<T, VoidType<decltype(std::declval<std::ostream&>() << std::declval<T>())> >
      : TrueType
    {};

    template<class T, class SFINAE = void>
    struct HasNameMethod
      : FalseType
    {};

    template<class T>
    struct HasNameMethod<T, VoidType<decltype(std::declval<T>().name())> >
      : TrueType
    {};

    namespace {

      //!@internal If there is not better way, output the mangled name of the type.
      template<class T, std::enable_if_t<!IsPrintable<T>::value, int> = 0>
      std::string toString(T&& t, PriorityTag<0>)
      {
        return typeString(std::forward<T>(t));
      }

      //!@internal If a T can be printed, use a stringstream.
      template<class T, std::enable_if_t<IsPrintable<T>::value, int> = 0>
      std::string toString(T&& t, PriorityTag<8>)
      {
        std::ostringstream ss;
        ss << std::forward<T>(t);
        return ss.str();
      }

      template<class T, std::enable_if_t<HasStdToString<T>::value, int> = 0>
      std::string toString(T&& t, PriorityTag<9>)
      {
#if 1
        // assume that anything with std::to_string() can also be printed ...
        std::ostringstream ss;
        ss << std::forward<T>(t);
        return ss.str();
#else
        // just too unflexible
        return std::to_string(std::forward<T>(t));
#endif
      }

      //!@internal If something has a name() method, use that as string representation.
      template<class T, std::enable_if_t<(sizeof(std::declval<T>().name()) > 0), int> = 0>
      std::string toString(T&& t, PriorityTag<10>)
      {
        return std::forward<T>(t).name();
      }
    }

    template<class T>
    std::string toString(T&& t)
    {
      return toString(std::forward<T>(t), PriorityTag<42>{});
    }

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_COMMON_TOSTRING_HH__
