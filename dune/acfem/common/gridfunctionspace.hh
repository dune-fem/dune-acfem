#ifndef __DUNE_ACFEM_COMMON_GRIDFUNCTIONSPACE_HH__
#define __DUNE_ACFEM_COMMON_GRIDFUNCTIONSPACE_HH__

#include <dune/fem/space/common/functionspace.hh>

#include "types.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup TypeTraits
     * @{
     */

    template<class>
    struct IsFunctionSpace
      : FalseType
    {};


    template<class DomainField, class RangeField, int dimD, int dimR>
    class IsFunctionSpace<Fem::FunctionSpace<DomainField, RangeField, dimD, dimR> >
      : TrueType
    {};

    //!@} TypeTraits

    /**@addtogroup Convenience
     * @{
     */

    namespace {
      template<class GridPart, class...>
      struct GridFunctionSpaceHelper;

      template<class GridPart, class T>
      struct GridFunctionSpaceHelper<GridPart, T>
      {
        using Type = Fem::GridFunctionSpace<GridPart, T>;
      };

      template<class GridPart>
      struct GridFunctionSpaceHelper<GridPart>
      {
        using RangeType = FieldVector<typename GridPart::ctype, GridPart::dimensionworld>;
        using Type = Fem::GridFunctionSpace<GridPart, RangeType>;
      };

      template<class GridPart, std::size_t dimRange>
      struct GridFunctionSpaceHelper<GridPart, IndexConstant<dimRange> >
      {
        using RangeType = FieldVector<typename GridPart::ctype, dimRange>;
        using Type = Fem::GridFunctionSpace<GridPart, RangeType>;
      };
    }

    template<class GridPart, class... T>
    using GridFunctionSpace = typename GridFunctionSpaceHelper<GridPart, T...>::Type;

    template<class GridPart, std::size_t dimRange = GridPart::dimensionworld>
    using VectorGridFunctionSpace = typename GridFunctionSpaceHelper<GridPart, IndexConstant<dimRange> >::Type;

    template<class GridPart>
    using ScalarGridFunctionSpace = VectorGridFunctionSpace<GridPart, 1>;

    //!@} Convenience

  } // NS ACFem::

} // NS Dune::

#endif // __DUNE_ACFEM_COMMON_GRIDFUNCTIONSPACE_HH__
