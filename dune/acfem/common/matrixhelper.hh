#ifndef __DUNE_ACFEM_COMMON_MATRIXHELPER_HH__
#define __DUNE_ACFEM_COMMON_MATRIXHELPER_HH__

#include <dune/fem/operator/linear/petscoperator.hh>
#include <dune/fem/operator/linear/istloperator.hh>
#include <dune/fem/operator/linear/spoperator.hh>

namespace Dune::ACFem {

// TODO: make sure, the matrix is assembled

template<class T>
class MatrixHelper {
 public:
  using LinearOperator = T;

  MatrixHelper(const LinearOperator& linOp)
      : linOp_(linOp) {}

 protected:
  const LinearOperator& linOp_;
};

template<class DomainFunction, class RangeFunction>
class MatrixHelper<Dune::Fem::SparseRowLinearOperator<DomainFunction, RangeFunction>> {
 public:
  using LinearOperator = Dune::Fem::SparseRowLinearOperator<DomainFunction, RangeFunction>;
  using ThisType = MatrixHelper<LinearOperator>;

  MatrixHelper(const LinearOperator& linOp)
      : linOp_(linOp) {}

  void printSparse(std::ostream& out = std::cout) const {
    const auto& matrix = linOp_.exportMatrix();

    for (std::size_t row = 0; row < matrix.rows(); ++row) {
      auto startOfRow = matrix.startRow(row);
      for (std::size_t colpos = 0; colpos < matrix.numNonZeros(row); ++colpos) {
        const auto valcol = matrix.realValue(startOfRow + colpos);
        out << row << " " << valcol.second << " " << valcol.first << std::endl;
      }
    }
  }

  void printPretty(std::ostream& out = std::cout) const {
    const auto& matrix = linOp_.exportMatrix();

    out << "Matrix Size: " << matrix.rows() << " x " << matrix.cols() << std::endl;
    if(matrix.cols() > prettyPrintColLimit_)
    {
      out << "Too many Columns for Pretty Print" << std::endl;
      return;
    }
    if(matrix.rows() > prettyPrintRowLimit_)
    {
      out << "Too many Rows for Pretty Print" << std::endl;
      return;
    }
    DynamicMatrix<double> outMatrix( matrix.rows(), matrix.cols(), 0 );
    for (std::size_t row = 0; row < matrix.rows(); ++row) {
      auto startOfRow = matrix.startRow(row);
      for (std::size_t colpos = 0; colpos < matrix.numNonZeros(row); ++colpos) {
        const auto valcol = matrix.realValue(startOfRow + colpos);
        outMatrix[row][valcol.second] = valcol.first;
      }
    }
    for (unsigned row = 0; row < matrix.rows(); ++row) {
      for (unsigned col = 0; col < matrix.cols(); ++col) {
        out << " " << std::setw(10) << outMatrix[row][col];
      }
      out << std::endl;
    }
  }

  friend std::ostream& operator<<(std::ostream& out, const ThisType& mh) {
    mh.printPretty(out);
    return out;
  }

 protected:
  const LinearOperator& linOp_;
  const unsigned int prettyPrintColLimit_ = 20;
  const unsigned int prettyPrintRowLimit_ = 20;
};

#if HAVE_DUNE_ISTL

template<class DomainFunction, class RangeFunction>
class MatrixHelper<Dune::Fem::ISTLLinearOperator<DomainFunction, RangeFunction>> {
 public:
  using LinearOperator = typename Dune::Fem::ISTLLinearOperator<DomainFunction, RangeFunction>;
  using ThisType = MatrixHelper<LinearOperator>;

  MatrixHelper(const LinearOperator& linOp)
      : linOp_(linOp) {}

  void printSparse(std::ostream& out = std::cout) const {
    const auto& matrix = linOp_.exportMatrix();

    matrix.print(out);

    // FIXME: output is blockwise because of ImprovedBCRSMatrix
    std::cerr << "WARNING: printed block matrix in inconsistent format" << std::endl;
  }

  void printPretty(std::ostream& out = std::cout) const {
    const auto& matrix = linOp_.exportMatrix();

    matrix.print(out);

    // FIXME: output is blockwise because of ImprovedBCRSMatrix
    std::cerr << "WARNING: printed block matrix in inconsistent sparse format" << std::endl;
  }

  friend std::ostream& operator<<(std::ostream& out, const ThisType& mh) {
    mh.printPretty(out);
    return out;
  }

 protected:
  const LinearOperator& linOp_;
};

#endif // HAVE_DUNE_ISTL

#if HAVE_PETSC

// TODO: output to out ostream and use consistent format
//       manual looping is difficult because of parallel matrices
// http://www.mcs.anl.gov/petsc/petsc-3.9/docs/manualpages/Mat/MatView.html
template<class DomainFunction, class RangeFunction>
class MatrixHelper<Dune::Fem::PetscLinearOperator<DomainFunction, RangeFunction>> {
 public:
  using LinearOperator = Dune::Fem::PetscLinearOperator<DomainFunction, RangeFunction>;
  using ThisType = MatrixHelper<LinearOperator>;

  MatrixHelper(const LinearOperator& linOp)
      : linOp_(linOp) {}

  void printSparse(std::ostream& out = std::cout) const {
    const auto& matrix = linOp_.exportMatrix();

    Dune::Petsc::PetscViewerSetFormat(PETSC_VIEWER_STDOUT_WORLD, PETSC_VIEWER_ASCII_MATLAB);
    Dune::Petsc::MatView(matrix, PETSC_VIEWER_STDOUT_WORLD);

    std::cerr << "WARNING: printed matrix in inconsistent format" << std::endl;
  }

  void printPretty(std::ostream& out = std::cout) const {
    const auto& matrix = linOp_.exportMatrix();

    Dune::Petsc::PetscViewerSetFormat(PETSC_VIEWER_STDOUT_WORLD, PETSC_VIEWER_ASCII_DENSE);
    Dune::Petsc::MatView(matrix, PETSC_VIEWER_STDOUT_WORLD);

    std::cerr << "WARNING: printed matrix in inconsistent format" << std::endl;
  }

  friend std::ostream& operator<<(std::ostream& out, const ThisType& mh) {
    mh.printPretty(out);
    return out;
  }

 protected:
  const LinearOperator& linOp_;
};

#endif // HAVE_PETSC

} // namespace Dune::ACFem

#endif // __DUNE_ACFEM_COMMON_MATRIXHELPER_HH__
