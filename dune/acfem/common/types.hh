#ifndef __DUNE_ACFEM_COMMON_TYPES_HH__
#define __DUNE_ACFEM_COMMON_TYPES_HH__

#include <limits>
#include <utility>
#include <cstdint>
#include <cstddef>
#include <tuple>
#include <array>

#include "compiler.hh"
#include "../mpl/conditional.hh"

#include "../core-fixes.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup C++-Templates
     *
     * @{
     */

    /**@addtogroup TypeAbbreviations.
     *
     * @{
     */

    template<class T, T V>
    struct integral_constant
    {
      using value_type = T;
      static constexpr T value = V;
      constexpr value_type operator()() const { return value; }
    };

    /**Short-cut for any integral constant.*/
    template<class T, T V>
    using Constant = integral_constant<T, V>;

    /**Short-cut for integral constant of type std::size_t.*/
    template<std::size_t V>
    using IndexConstant = Constant<std::size_t, V>;

    /**Short-cut for integral constant of type bool.*/
    template<bool V>
    using BoolConstant = Constant<bool, V>;

    /**Short-for integral constant of type std::p0trdiff_t.*/
    template<std::ptrdiff_t V>
    using IntConstant = Constant<std::ptrdiff_t, V>;

    /**Sequence of any type of integer values.*/
    template<class T, T... V>
    using Sequence = std::integer_sequence<T, V...>;

    /**Sequence of boolean values. */
    template<bool... Decisions>
    using BoolSequence = Sequence<bool, Decisions...>;

    /**Sequence of std::size_t values. */
    template<std::size_t... V>
    using IndexSequence = Sequence<std::size_t, V...>;

    /**Sequence of std::ptrdiff_t values. */
    template<std::ptrdiff_t... V>
    using IntSequence = Sequence<std::ptrdiff_t, V...>;

    //!@} TypeAbbreviations

    /**@addtogroup TemplateAlgorithms
     *
     * @{
     */

    /**Increment an integral_constant.*/
    template<class Const>
    using Incr = Constant<typename Const::value_type, Const::value + 1>;

    /**Decrement an integral_constant.*/
    template<class Const>
    using Decr = Constant<typename Const::value_type, Const::value - 1>;

    //!@} TemplateAlgorithms

    /**@addtogroup TypeTraits
     *
     * @{
     */

    /**@c true if T is its own decay type.*/
    template<class T>
    using IsDecay = std::is_same<T, std::decay_t<T> >;

    /**@c true if the decay types match.*/
    template<class T1, class T2>
    using HasSameDecay = std::is_same<std::decay_t<T1>, std::decay_t<T2> >;

    /**@c TrueType if the decay type of Base is a base of the
     * DecayType of Derived.
     */
    template<class Base, class Derived> using
    IsBaseOfDecay = std::is_base_of<std::decay_t<Base>, std::decay_t<Derived> >;

    /**Alias for std::true_type.*/
    using TrueType = BoolConstant<true>;

    /**Alias for std::false_type.*/
    using FalseType = BoolConstant<false>;

    /**Generate a TrueType.*/
    constexpr auto trueType() { return TrueType{}; }

    /**Generate a FalseType.*/
    constexpr auto falseType() { return FalseType{}; }

    /**Generate a BoolConstant.*/
    template<bool Cond>
    constexpr auto boolType(BoolConstant<Cond> arg = BoolConstant<Cond>{}) { return arg; }

    /**Forward as pair capturing lvalue references.*/
    template<class T0, class T1>
    constexpr auto forwardAsPair(T0&& t0, T1&& t1)
    {
      return std::pair<T0, T1>(std::forward<T0>(t0), std::forward<T1>(t1));
    }

    /**TrueType if const or a reference to a const.*/
    template<class T>
    using RefersConst = BoolConstant<(std::is_const<T>::value
                                      ||
                                      std::is_const<std::remove_reference_t<T> >::value)>;

    /**@internal Helper struct for FirstType, VoidType, AlwaysFalse
     * and AlwaysTrue. Some compilers fail to treat type alias
     * substitution failures as substitution failures.
     */
    template<class T, class... Other>
    struct MakeType
    {
      using Type = T;
    };

    /**Generate the first type of the template argument list.*/
    template<class... T>
    using FirstType = typename MakeType<T...>::Type;

    /**Generate void regardless of the template argument list.*/
    template<class... Other>
    using VoidType = typename MakeType<void, Other...>::Type;

    /**Generate FalseType regardless of the template argument list.*/
    template<class... Other>
    using AlwaysFalse = typename MakeType<FalseType, Other...>::Type;

    /**Generate TrueType regardless of the template argument list.*/
    template<class... Other>
    using AlwaysTrue = typename MakeType<TrueType, Other...>::Type;

    /**Assume a predicate is a traits-class and would never evaluate
     * to true unless it is equivalent to AlwaysTrue.
     */
    template<template<class...> class Predicate, class SFINAE = void>
    struct IsAlwaysTrue
      : FalseType
    {};

    /**Assume a predicate is a traits-class and would never evaluate
     * to true unless it is equivalent to AlwaysTrue.
     */
    template<template<class...> class Predicate>
    struct IsAlwaysTrue<Predicate, std::enable_if_t<Predicate<>::value> >
      : TrueType
    {};

    /**Assume a predicate is a traits-class and would never evaluate
     * to false unless it is equivalent to AlwaysFalse.
     */
    template<template<class...> class Predicate, class SFINAE = void>
    struct IsAlwaysFalse
      : FalseType
    {};

    /**Assume a predicate is a traits-class and would never evaluate
     * to false unless it is equivalent to AlwaysFalse.
     */
    template<template<class...> class Predicate>
    struct IsAlwaysFalse<Predicate, std::enable_if_t<!Predicate<>::value> >
      : TrueType
    {};

    /**Logical "and" for two perdicates.*/
    template<template<class...> class P0, template<class...> class P1>
    struct AndPredicate
    {
      template<class... Arg>
      struct Predicate
        : BoolConstant<P0<Arg...>::value && P1<Arg...>::value>
      {};
    };

    /**Logical "or" for two perdicates.*/
    template<template<class...> class P0, template<class...> class P1>
    struct OrPredicate
    {
      template<class... Arg>
      struct Predicate
        : BoolConstant<P0<Arg...>::value || P1<Arg...>::value>
      {};
    };

    /**Wrap the given predicate class F into another one with spcial
     * requirements.
     */
    template<template<class...> class F, class... Rest>
    struct PredicateProxy
    {
      /**Discard any but the first template parameter, instantiate F
       * with T and Rest. This can be used for template-template
       * arguments with more than one parameter where it is know that
       * only the first matters.
       */
      template<class T, class...>
      struct ForwardFirst
        : F<T, Rest...>
      {};

      template<class T, class...>
      struct FirstOfPair
        : F<typename T::first_type, Rest...>
      {};

      template<class T1, class T2, class...>
      struct BinaryFirstOfPair
        : F<typename T1::first_type, typename T2::first_type, Rest...>
      {};

      template<class T, class...>
      struct SecondOfPair
        : F<typename T::second_type, Rest...>
      {};

      template<class T1, class T2, class...>
      struct BinarySecondOfPair
        : F<typename T1::second_type, typename T2::second_type, Rest...>
      {};

      /**Generate a one-parameter template to support
       * template-template arguments with a single argument.
       */
      template<class T>
      struct Unary
        : F<T, Rest...>
      {};

    };

    /**Wrap a predicate into a class which can be passed on as
     * argument.
     */
    template<template<class...> class F>
    struct PredicateWrapper
    {
      template<class... Arg>
      using Predicate = F<Arg...>;

      template<class... Arg>
      constexpr bool operator()(Arg&&...) const
      {
        return F<Arg...>::value;
      }
    };

    /**@internal FalseType by default.*/
    template<class T, class SFINAE = void>
    struct IsSequence
      : FalseType
    {};

    template<class T>
    struct IsSequence<T, std::enable_if_t<!IsDecay<T>::value> >
      : IsSequence<std::decay_t<T> >
    {};

    /**Evaluate to TrueType of integer sequences.*/
    template<class T, T... Ints>
    struct IsSequence<Sequence<T, Ints...> >
      : TrueType
    {};

    /**@internal FalseType by default.*/
    template<class T, class SFINAE = void>
    struct IsIndexSequence
      : FalseType
    {};

    template<class T>
    struct IsIndexSequence<T, std::enable_if_t<!IsDecay<T>::value> >
      : IsIndexSequence<std::decay_t<T> >
    {};

    /**TrueType for integer sequences with value_type std::size_t.*/
    template<std::size_t... Ints>
    struct IsIndexSequence<IndexSequence<Ints...> >
      : TrueType
    {};

    /**FalseType by default.*/
    template<class T>
    struct IsIntegralConstant
      : FalseType
    {};

    template<class T>
    struct IsIntegralConstant<T&>
      : IsIntegralConstant<std::decay_t<T> >
    {};

    template<class T>
    struct IsIntegralConstant<T&&>
      : IsIntegralConstant<std::decay_t<T> >
    {};

    /**TrueType for integral_constant.*/
    template<class T, T V>
    struct IsIntegralConstant<Constant<T, V> >
      : TrueType
    {};

    /**@internal FalseType by default.*/
    template<class T>
    struct IsBoolConstant
      : FalseType
    {};

    /**Forward to decay traits class for non decay types.*/
    template<class T>
    struct IsBoolConstant<T&>
      : IsBoolConstant<std::decay_t<T> >
    {};

    /**Forward to decay traits class for non decay types.*/
    template<class T>
    struct IsBoolConstant<T&&>
      : IsBoolConstant<std::decay_t<T> >
    {};

    /**TrueType for bool constant.*/
    template<bool V>
    struct IsBoolConstant<BoolConstant<V> >
      : TrueType
    {};

    /**@internal FalseType by default.*/
    template<class, class SFINAE = void>
    struct IsDefined
      : FalseType
    {};

    /**TrueType if T is an object which is not a pointer.*/
    template<class T>
    struct IsDefined<T,
                     std::enable_if_t<(std::is_object<T>::value &&
                                      !std::is_pointer<T>::value &&
                                       (sizeof(T) >= 0))>
      > : TrueType
    {};

    /**@internal FalseType by default.*/
    template<class T, class SFINAE = void>
    struct IsTupleLike
      : FalseType
    {};

    /**Evaluate to @c true if @a T behaves more or less like a
     * tuple. This includes std::tuple, std::pair and std::array, at
     * least. We examine whether std::tupe_size is defined.
     *
     * @param[in] T The type to examine.
     *
     * @param[in] SFINAE The SFINAE dummy parameter in order to
     * perform the specialization.
     */
    template<class T>
    struct IsTupleLike<T, std::enable_if_t<(std::tuple_size<std::decay_t<T> >::value >= 0)> >
      : TrueType
    {};

    template<class T>
    struct IsTuple
      : FalseType
    {};

    template<class... T>
    struct IsTuple<std::tuple<T...> >
      : TrueType
    {};

    /**@internal FalseType by default.*/
    template<class T, class SFINAE = void>
    struct IsArray
      : FalseType
    {};

    /**Forward to decay if T is not a decay type.*/
    template<class T>
    struct IsArray<T, std::enable_if_t<!IsDecay<T>::value> >
      : IsArray<std::decay_t<T> >
    {};

    /**TrueType if T is a std::array.*/
    template<class T, std::size_t N>
    struct IsArray<std::array<T, N> >
      : TrueType
    {};

    /**@internal Default false.*/
    template<class T, class SFINAE = void>
    struct HasResize
      : FalseType
    {};

    template<class T>
    struct HasResize<T, std::enable_if_t<!IsDecay<T>::value> >
      : HasResize<std::decay_t<T> >
    {};

    template<class T>
    struct HasResize<T, VoidType<decltype(std::declval<T>().resize(0UL))> >
      : TrueType
    {};

    /**TrueType if T1 and T2 have the same decay types, otherwise
     * FalseType.
     */
    template<class T1, class T2>
    struct SameDecay
      : std::is_same<std::decay_t<T1>, std::decay_t<T2> >
    {};

    /**@internal FalseType by default.*/
    template<class T, class SFINAE = void>
    struct HasUnaryMinus
      : FalseType
    {};

    /**TrueType if a T allows for the unary minus operator.*/
    template<class T>
    struct HasUnaryMinus<T, std::enable_if_t<(sizeof(decltype(-std::declval<T>())) >= 0)> >
      : TrueType
    {};

    /**Case-structure for multi-conditional switch.
     *
     * @param C A boolean value which is made available trough the
     * data member Case::value.
     *
     * @param T A Type which is made available through the type alias
     * Case::type.
     */
    template<bool C, class T>
    struct Case
      : BoolConstant<C>  //!< export @a C.
    {
      using Type = T; //!< export @a T.
    };

    /**Generate the negation of the given Case, i.e. CaseNot::value ==
     * !CaseType::value.
     */
    template<class CaseType>
    using CaseNot = Case<!CaseType::value, typename CaseType::Type>;

    /**If A is void then CaseVoid::value is @c true and CaseVoid::Type is B.
     *
     * @param A The type to examine.
     *
     * @param B The type to export, defaults to A.
     */
    template<class A, class B = A>
    using CaseVoid = Case<std::is_same<A, void>::value, B>;

    /**If A is not void then CaseNotVoid::value is @c true and
     * CaseNotVoid::Type is B.
     *
     * @param A The type to examine.
     *
     * @param B The type to export, defaults to A.
     */
    template<class A, class B = A>
    using CaseNotVoid = CaseNot<CaseVoid<A, B> >;

    /**If A equals B export C as CaseSame::Type. C defaults to A (@b
     * not to B).
     */
    template<class A, class B, class C = A>
    using CaseSame = Case<std::is_same<A, B>::value, C>;

    /**Negation of CaseSame, if A is not B then export C as
     * CaseNotSame::Type. C default to A.
     */
    template<class A, class B, class C = A>
    using CaseNotSame = CaseNot<CaseSame<A, B, C> >;

    /**If A is a base of B then export C as CaseBase::Type. C defaults
     * to A.
     */
    template<class A, class B, class C = A>
    using CaseBase = Case<std::is_base_of<A, B>::value, C>;

    /**Negation of CaseBase, if A is not a base of B, then export C as
     * CaseNotBase::Type. C defaults to A.
     */
    template<class A, class B, class C = A>
    using CaseNotBase = CaseNot<CaseBase<A, B, C> >;

    /**Recursion for a multi-conditional switch. Export Head::Type as
     * type alias Switch::Type and recurses to Switch<Tail...>
     * otherwise.
     *
     * @param Head Must be Case type, i.e. Head::value must be a
     * boolean and Head::Type must be a type.
     */
    template<class Head, class... Tail>
    struct Switch
      : ConditionalType<Head::value, Head, Switch<Tail...> >
    {};

    /**Recursion endpoint: If the terminating template parameter to
     * SwitchType is not a Case template, then export T as
     * Switch<T>::Type.
     */
    template<class T>
    struct Switch<T>
    {
      using Type = T;
    };

    /**Recursion end-point: if the last template parameter to
     * SwitchType is a case then export T as SwitchCase<C,T>::Type if
     * C is @c true. Otherwise issue a static assert.
     */
    template<bool C, class T>
    struct Switch<Case<C, T> >
    {
      static_assert(C, "Type-switch without terminating \"true\" case.");
      using Type = T;
    };

    /**Multiple conditions. Last element of Cases is the default
     * type. Cases must be of type Case<Condition, Type>, except for
     * the last template parameter which can be used to define a
     * default type.
     *
     * SwitchType exports Cases::Type as SwitchType::Type for the
     * first template parameter with Cases::value == true.
     *
     * If no cases match, export the last template paramter as
     * SwitchType::Type if it is not a Case. Otherwise generate a
     * static_assert().
     */
    template<class... Cases>
    using SwitchType = typename Switch<Cases...>::Type;

    /**Type Yes if C is true, otherwise type No which default to
     * void.
     */
    template<bool C, class Yes, class No = void>
    using IfElseType = SwitchType<Case<C, Yes>, No>;

    /**First non-void type in list. */
    template<class... A>
    using OrType = SwitchType<CaseNotVoid<A>...>;

    /**A if A and B have same type, otherwise Default.*/
    template<class A, class B, class Default = void>
    using AndType = SwitchType<CaseSame<A, B>, Default>;

    /**Export a type copy_cv_reference::type which has the same
     * qualifiers as T and decays to the decay of U.
     */
    template<class T, class U>
    struct copy_cv_reference
    {
     private:
      using R = std::remove_reference_t<T>;
      using U0 = std::decay_t<U>; // = U?
      using U1 = ConditionalType<std::is_const<R>::value, std::add_const_t<U0>, U0>;
      using U2 = ConditionalType<std::is_volatile<R>::value, std::add_volatile_t<U1>, U1>;
      using U3 = ConditionalType<std::is_lvalue_reference<T>::value, std::add_lvalue_reference_t<U2>, U2>;
      using U4 = ConditionalType<std::is_rvalue_reference<T>::value, std::add_rvalue_reference_t<U3>, U3>;
     public:
      using type = U4;
    };

    /**@copydoc copy_cv_reference.*/
    template<class From, class To>
    using copy_cv_reference_t = typename copy_cv_reference<From, To>::type;

    /**Export a type trough remove_const::type as follows:
     *
     * + If T is not a reference then remove any const attribute.
     *
     * + If T is an lvalue reference type then generate a mutable
     *   lvalue reference type.
     */
    template<class T>
    struct remove_const
    {
      using type = ConditionalType<std::is_lvalue_reference_v<T>,
                                   std::add_lvalue_reference_t<std::remove_const<std::remove_reference_t<T> > >,
                                   std::remove_const<std::remove_reference_t<T> > >;
    };

    /**@copydoc remove_const.*/
    template<class T>
    using remove_const_t = typename remove_const<T>::type;

    /**Forward lvalue-references as references.*/
    template<class T>
    constexpr T forwardReturnValue(std::remove_reference_t<T>& t)
    {
      return static_cast<T>(t);
    }

    /**Force copy for rvalue-references in order to guard against
     * leaking references to local variables in return statements.
     */
    template<class T>
    constexpr T forwardReturnValue(std::remove_reference_t<T>&& t)
    {
      return static_cast<T&&>(t);
    }

    //!@} TypeTraits

    //!@} C++-Templates

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_COMMON_TYPES_HH__
