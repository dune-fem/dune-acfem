#include <config.h>

// iostream includes
#include <iostream>
#include <sstream>

#include <dune/acfem/common/scopedredirect.hh>

// include header of adaptive scheme
#include "../discretefunctionselector.hh"
#include "../dofmappertupledatahandle.hh"

using namespace Dune::ACFem;

template<class HGridType>
void algorithm(HGridType &grid)
{
  const size_t dimRange = 32;

  auto gridPart = leafGridPart(grid);
  auto discreteSpace = discreteFunctionSpace<dimRange>(gridPart, lagrange<POLORDER>);

  const size_t blockSize = decltype(discreteSpace)::localBlockSize;
  static_assert(blockSize == dimRange, "Bogus inconsistent dimRange");

  auto storage3 = discreteFunction<SolverFamily::ISTL>(discreteSpace, "fv");
  auto storage4 = discreteFunction<SolverFamily::FEM>(discreteSpace, "sv");

  typedef std::bitset<blockSize> DataItem1Type;
  typedef int DataItem2Type;
  typedef std::vector<DataItem1Type> Storage1Type;
  typedef std::vector<DataItem2Type> Storage2Type;

  const std::string bitBlock = "1010101001010101"; // 0xAA55
  std::string bitsetInitializer = "";
  for (size_t i = 0; i < (blockSize + bitBlock.size() - 1) / bitBlock.size(); ++i) {
    bitsetInitializer += bitBlock;
  }

  Storage1Type storage1(discreteSpace.size() / blockSize);
  Storage2Type storage2(discreteSpace.size() / blockSize);

  // Example for Dirichlet-flags: Dirichlet wins, hence | operator.
  auto dataHandle =
    dofMapperTupleDataHandle(
      discreteSpace,
#if DUNE_ACFEM_IS_GCC(7, 7)
      [blockSize](const auto& a, auto& b) {
#else
      [](const auto& a, auto& b) {
#endif
        for (size_t i = 0; i < blockSize; ++i) {
          if (std::get<0>(a)[i]) {
            std::get<1>(b) = std::get<1>(a);
            std::get<2>(b) = std::get<2>(a);
            for (size_t i = 0; i< blockSize; ++i) {
              // Here b is a sub-vector, a is a FieldVector, there is
              // no compound assign, so we have to loop.
              std::get<3>(b)[i] = std::get<3>(a)[i];
            }
          }
        }
        std::get<0>(b) |= std::get<0>(a);
      },
      storage1, storage2, storage3.dofVector(), storage4.dofVector());

  if (Dune::Fem::MPIManager::rank() == 0) {
    // just fill the entire vector. Result should then be visible on
    // the other nodes after communication
    for (auto& bits : storage1) {
      bits = DataItem1Type(bitsetInitializer);
    }
    for (auto& ints : storage2) {
      ints = 1234567;
    }
    for (auto& dof : dofs(storage3)) {
      dof = -13.;
    }
    for (auto& dof : dofs(storage4)) {
      dof = -17.;
    }
  } else {
    storage3.clear();
    storage4.clear();
  }

  gridPart.communicate(dataHandle,
                       decltype(gridPart)::indexSetInterfaceType,
                       Dune::ForwardCommunication);

  const auto rank = Dune::Fem::MPIManager::rank();
  std::ostringstream ss;
  ss << " *********** output from rank " << rank << " *********** " << std::endl;
  for (size_t i = 0; i < storage1.size(); ++i) {
    if (storage1[i].any()) {
      ss << "bits[" << i << "]:   " << storage1[i] << std::endl;
      ss << "ints[" << i << "]:   " << storage2[i] << std::endl;
      ss << "bvector[" << i << "]: " << storage3.dofVector()[i] << std::endl;
      ss << "svector[" << i << "]: " << storage4.dofVector()[i] << std::endl;
    }
  }

  int len = ss.str().size()+1;
  int recvlen[256] = { 0, };
  gridPart.comm().gather(&len, recvlen, 1, 0);

  int displ[256];
  if (rank == 0) {
    displ[0] = 0;
    for (int rank = 1; rank < gridPart.comm().size(); ++rank) {
      displ[rank] = displ[rank-1] + recvlen[rank-1];
    }
  }

  // See Dune::CollectiveCommunication docs
  char data[1024*1024];
  gridPart.comm().gatherv(ss.str().c_str(), ss.str().size()+1,
                          data, recvlen, displ, 0);

  if (rank == 0) {
    for (int rank = 0; rank < gridPart.comm().size(); ++rank) {
      std::clog << &data[displ[rank]];
    }
  }
}

// main
// ----

#ifndef SRCDIR
# define SRCDIR "."
#endif

int main (int argc, char **argv)
try
{
  // initialize MPI, if necessary
  Dune::Fem::MPIManager::initialize(argc, argv);

  //redirect the stream cerr to cout
  //so we only get the output of clog on stderr
  Dune::ACFem::ScopedRedirect redirect(std::cerr, std::cout);

  std::cerr << "MPI rank " << Dune::Fem::MPIManager::rank() << std::endl;

  // append overloaded parameters from the command line
  Dune::Fem::Parameter::append(argc, argv);

  // append possible given parameter files
  for (int i = 1; i < argc; ++i)
    Dune::Fem::Parameter::append(argv[ i ]);

  // append default parameter file
  Dune::Fem::Parameter::append(SRCDIR "/parameter");

  // type of hierarchical grid
  //typedef Dune::AlbertaGrid< 2 , 2 > GridType;
  typedef Dune::GridSelector::GridType  HGridType ;

  // create grid from DGF file
  const std::string gridkey = Dune::Fem::IOInterface::defaultGridKey(HGridType::dimension);
  const std::string gridfile = SRCDIR "/" + Dune::Fem::Parameter::getValue<std::string>(gridkey);

  // the method rank and size from MPIManager are static
  if (Dune::Fem::MPIManager::rank() == 0)
    std::cerr << "Loading macro grid: " << gridfile << std::endl;

  // construct macro using the DGF Parser
  Dune::GridPtr< HGridType > gridPtr(gridfile);
  HGridType& grid = *gridPtr ;

  // do initial load balance
  grid.loadBalance();

  // initial grid refinement
  const int level = Dune::Fem::Parameter::getValue< int >("fem.globalRefinements");

  // number of global refinements to bisect grid width
  const int refineStepsForHalf = Dune::DGFGridInfo< HGridType >::refineStepsForHalf();

  // refine grid
  grid.globalRefine(level * refineStepsForHalf);

  // let it go ... quasi adapt_method_stat()
  algorithm(grid);

  //std::clog << "Estimated Global Error: " << estimateError.first << std::endl;
  //std::clog << "Real Global Error: " << estimateError.second << std::endl;

  return 0;
}
catch(const Dune::Exception &exception)
{
  std::cerr << "Error: " << exception << std::endl;
  return 1;
}
