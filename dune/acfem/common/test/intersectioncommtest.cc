#include <config.h>

// iostream includes
#include <iostream>

#include <dune/acfem/common/scopedredirect.hh>

// include grid part
#include <dune/fem/io/parameter.hh>
#include <dune/fem/misc/mpimanager.hh>
#include <dune/fem/gridpart/adaptiveleafgridpart.hh>
#include <dune/fem/space/common/commoperations.hh>

#include "../intersectiondatahandle.hh"
#include "../intersectionclassification.hh"

using namespace Dune;

#ifndef SRCDIR
# define SRCDIR "."
#endif

int main ( int argc, char **argv )
  try
  {
    // initialize MPI, if necessary
    Fem::MPIManager::initialize( argc, argv );

    //redirect the stream cerr to cout
    //so we only get the output of clog on stderr
    ACFem::ScopedRedirect redirect(std::cerr, std::cout);

    // append overloaded parameters from the command line
    Fem::Parameter::append( argc, argv );

    // append possible given parameter files
    for( int i = 1; i < argc; ++i )
      Fem::Parameter::append( argv[ i ] );

    // append default parameter file
    Fem::Parameter::append( SRCDIR "/parameter" );

    // type of hierarchical grid
    //typedef AlbertaGrid< 2 , 2 > GridType;
    typedef GridSelector::GridType  HGridType ;

    // create grid from DGF file
    const std::string gridkey = Fem::IOInterface::defaultGridKey( HGridType::dimension );
    const std::string gridfile = SRCDIR "/" + Fem::Parameter::getValue< std::string >( gridkey );

    // the method rank and size from MPIManager are static
    if( Fem::MPIManager::rank() == 0 )
      std::cout << "Loading macro grid: " << gridfile << std::endl;

    // construct macro using the DGF Parser
    DGFGridFactory<HGridType> gridFactory( gridfile );
    //GridPtr< HGridType > gridPtr( gridfile );
    HGridType& grid = *gridFactory.grid(); //*gridPtr ;

    // do initial load balance
    grid.loadBalance();

    // initial grid refinement
    const int level = Fem::Parameter::getValue< int >("fem.globalRefinements");

    // number of global refinements to bisect grid width
    const int refineStepsForHalf = DGFGridInfo< HGridType >::refineStepsForHalf();

    // refine grid
    grid.globalRefine( level * refineStepsForHalf );

    typedef Fem::AdaptiveLeafGridPart<HGridType, InteriorBorder_Partition> GridPartType;

    GridPartType gridPart(grid);

    typedef GridPartType::IndexSetType IndexSetType;
    typedef IndexSetType::IndexType IndexType;
    typedef FieldVector<double, 3> DataItemType;
    typedef std::vector<DataItemType> IntersectionStorageType;
    typedef std::map<IndexSetType::IndexType, IntersectionStorageType> StorageType;
    typedef Fem::DFCommunicationOperation::Add OperationType;
    typedef ACFem::IntersectionDataHandle<IndexSetType, StorageType, OperationType> DataHandleType;

    const IndexSetType& indexSet = gridPart.indexSet();
    typedef DataHandleType::CommunicationStorageType StorageType;
    StorageType storage;

    const size_t numItems = 42; // number of data itmes per
                                // intersection, e.g. quad-points in a
                                // real application.

    auto end = gridPart.template end<0>();
    for(auto it = gridPart.template begin<0>(); it != end; ++it) {
      const auto& entity = *it;

      auto faceEnd = gridPart.iend(entity);
      for (auto faceIt = gridPart.ibegin(entity); faceIt != faceEnd; ++faceIt) {
        const auto &intersection = *faceIt;

        // let's get hold of the codim 1 index. This is a mess ...
        IndexType faceIndex = indexSet.index(entity.template subEntity<1>(intersection.indexInInside()));

        if (ACFem::isProcessorBoundary(intersection)) {
          auto& faceStorage(storage[faceIndex]); // std::map
          const size_t startSize = faceStorage.size();
          faceStorage.resize(startSize + numItems);
          for (size_t i = startSize; i < startSize + numItems; ++i) {
            faceStorage[i] = DataItemType(1.0); // FieldVector
          }
        }
      }
    }

    DataHandleType dataHandle(indexSet, storage);

    gridPart.communicate(dataHandle,
			 InteriorBorder_InteriorBorder_Interface,
			 ForwardCommunication);

    // Loop over the mesh and check each intersection. On the boundary
    // between computing nodes the storage array should consist of
    // "constant 2" matrices, everything else should be "constant 1"
    //
    // N.b.: this check is only correct for conforming meshes. As the
    // data is attached to the faces a non-conforming triangulation
    // can lead to larger values (number of intersections of a face).

    for(auto it = gridPart.template begin<0>(); it != end; ++it) {
      const auto& entity = *it;

      auto faceEnd = gridPart.iend(entity);
      for (auto faceIt = gridPart.ibegin(entity); faceIt != faceEnd; ++faceIt) {
        const auto &intersection = *faceIt;

        // let's get hold of the codim 1 index. This is a mess ...
        IndexType faceIndex = indexSet.index(entity.template subEntity<1>(intersection.indexInInside()));

        bool interProcessBoundary = false;
        double expectedValue = 1.0;

        if (intersection.neighbor()) {
          if (intersection.outside().partitionType() == GhostEntity) {
            // case with ghosts
            if (Fem::MPIManager::rank() == 0) {
              std::clog << "Process boundary with ghost at face " << faceIndex << std::endl;
            }
            interProcessBoundary = true;
            expectedValue = 2.0;
          }
        } else if (!intersection.boundary()) {
          // case without ghosts
          if (Fem::MPIManager::rank() == 0) {
            // Don't, output depends on timing and is not constant between different test runs
            //std::clog << "Process boundary without ghost at face " << faceIndex << std::endl;
          }
          interProcessBoundary = true;
          expectedValue = 2.0;
        }

        if (!interProcessBoundary && storage.count(faceIndex) > 0) {
          // Don't, output depends on timing and is not constant between different test runs
          //std::clog << "Found data-items attached to non-process-boundary faces: " << faceIndex << std::endl;
        }

        if (!interProcessBoundary) {
          continue;
        }

        const auto& faceStorage(storage[faceIndex]);
        const auto end = faceStorage.end();
        for (auto it = faceStorage.begin(); it != end; ++it) {
          for (int i = 0; i < DataItemType::dimension; ++i) {
            if ((*it)[i] != expectedValue) {
              std::clog
                << "Unexpected Value" << std::endl
                << "  MPI rank : " << Fem::MPIManager::rank() << std::endl
                << "  expected : " << expectedValue << std::endl
                << "  got      : " << (*it)[i] << std::endl
                << "  faceIndex: " << faceIndex << std::endl
                << "  procBndry: " << (interProcessBoundary ? "true" : "false") << std::endl;
              exit(1);
            }
          }
        }
      }
    }
    if (Fem::MPIManager::rank() == 0) {
      std::clog << "Test completed successfully!" << std::endl;
    }
    return 0;
  }
  catch( const Exception &exception )
  {
    std::cerr << "Error: " << exception << std::endl;
    return 1;
  }
