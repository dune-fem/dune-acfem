#include <config.h>

// iostream includes
#include <iostream>
#include <vector>

#include <dune/acfem/common/scopedredirect.hh>

#include <dune/acfem/common/discretefunctionselector.hh>
#include <dune/acfem/common/subentitymapper.hh>

using namespace Dune;
using namespace Dune::ACFem;

int main ( int argc, char **argv )
  try
  {
    // initialize MPI, if necessary
    Fem::MPIManager::initialize( argc, argv );

    //redirect the stream cerr to cout
    //so we only get the output of clog on stderr
    ACFem::ScopedRedirect redirect(std::cerr, std::cout);

    // append overloaded parameters from the command line
    Fem::Parameter::append( argc, argv );

    // append possible given parameter files
    for( int i = 1; i < argc; ++i )
      Fem::Parameter::append( argv[ i ] );

    // append default parameter file
    Fem::Parameter::append(SRCDIR "/"  "./parameter" );

    // type of hierarchical grid
    //typedef AlbertaGrid< 2 , 2 > GridType;
    typedef GridSelector::GridType  HGridType ;

    // create grid from DGF file
    const std::string gridkey = Fem::IOInterface::defaultGridKey( HGridType::dimension );
    const std::string gridfile = SRCDIR "/" + Fem::Parameter::getValue< std::string >( gridkey );

    // the method rank and size from MPIManager are static
    if( Fem::MPIManager::rank() == 0 )
      std::cout << "Loading macro grid: " << gridfile << std::endl;

    // construct macro using the DGF Parser
    DGFGridFactory<HGridType> gridFactory( gridfile );
    //GridPtr< HGridType > gridPtr( gridfile );
    HGridType& grid = *gridFactory.grid(); //*gridPtr ;

    // do initial load balance
    grid.loadBalance();

    // initial grid refinement
    //const int level = Fem::Parameter::getValue< int >("fem.globalRefinements");

    // number of global refinements to bisect grid width
    // const int refineStepsForHalf = DGFGridInfo< HGridType >::refineStepsForHalf();

    // refine grid
    // grid.globalRefine( level * refineStepsForHalf );

    auto gridPart = leafGridPart<InteriorBorder_Partition>(grid);
    auto space = discreteFunctionSpace(gridPart, lagrange<POLORDER>);

    using GlobalKeyType = decltype(space)::BlockMapperType::GlobalKeyType;

    const auto& mapper = space.blockMapper();
    std::vector<GlobalKeyType> globalBulkIndices(mapper.maxNumDofs());
    std::vector<GlobalKeyType> globalFaceIndices(mapper.maxNumDofs());
    std::vector<unsigned> localFaceIndices(mapper.maxNumDofs());

    auto end = gridPart.template end<0>();
    for(auto it = gridPart.template begin<0>(); it != end; ++it) {
      const auto& entity = *it;

      unsigned numBulkDofs;
      mapper.map(entity, globalBulkIndices);
      numBulkDofs = mapper.numDofs(entity);

      std::clog << "Global Bulk DoFs"
                << std::endl
                << std::endl;
      for (unsigned i = 0; i < numBulkDofs; ++i) {
        std::clog << "global[" << i << "] = " << globalBulkIndices[i] << std::endl;
      }
      std::clog << std::endl;

      auto faceEnd = gridPart.iend(entity);
      for (auto faceIt = gridPart.ibegin(entity); faceIt != faceEnd; ++faceIt) {
        const auto &intersection = *faceIt;
        unsigned numFaceIndices;

        numFaceIndices = ACFem::mapFaceDofs(mapper, intersection, localFaceIndices, globalFaceIndices);

        std::clog << "Global Face DoFs for face " << intersection.indexInInside()
                  << std::endl
                  << std::endl;

        for (unsigned i = 0; i < numFaceIndices; ++i) {
          std::clog << "global[" << localFaceIndices[i] << "] = " << globalFaceIndices[i] << std::endl;
        }
        std::clog << std::endl;

      }
    }

    return 0;
  }
  catch( const Exception &exception )
  {
    std::cerr << "Error: " << exception << std::endl;
    return 1;
  }
