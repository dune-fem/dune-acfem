#include <config.h>

#include "../../expressions/constantoperations.hh"
#include "../../expressions/signutil.hh"
#include "../fractionconstant.hh"
#include "../ostream.hh"

using namespace Dune;
using namespace Dune::ACFem;

template<class T>
void printInfo(std::ostream& out, T&& t)
{
  std::clog << "  Value:  " << t << std::endl;
  std::clog << "  Type:   " << typeString(t) << std::endl;
  std::clog << "  scalar: " << IsScalar<T>::value << std::endl;
  std::clog << "  typed:  " << IsTypedValue<T>::value << std::endl;
  std::clog << "  zero:   " << expressionTraits(t).isZero << std::endl;
  std::clog << "  one:    " << expressionTraits(t).isOne << std::endl;
  std::clog << "  -one:   " << expressionTraits(t).isMinusOne << std::endl;
  std::clog << "  sign:   " << expressionSign(t) << std::endl;
  std::clog << "  int:    " << std::ptrdiff_t(t) << std::endl;
  std::clog << "  floor:  " << floor(t) << std::endl;
  std::clog << "  ceil:   " << ceil(t) << std::endl;
  std::clog << "  round:  " << round(t) << std::endl;
  std::clog << "    eq:   " << (std::ptrdiff_t(t) == t) << std::endl;
  std::clog << "  double: " << double(t) << std::endl;
  std::clog << "    eq:   " << (double(t) == t) << std::endl;
}

int main(int argc, char *argvp[])
{
  std::clog << "min(1, 2): " << min(intFraction<1>(), intFraction<2>()) << std::endl;
  std::clog << "max(1, 2): " << max(intFraction<1>(), intFraction<2>()) << std::endl;

  std::clog << "1/2:      " << std::endl;
  printInfo(std::clog, intFraction<1,2>());

  std::clog << "42:      " << std::endl;
  printInfo(std::clog, intFraction<42>());

  std::clog << "3/4 + 7/-6:   " << std::endl;
  printInfo(std::clog, intFraction<3,4>() + intFraction<7,-6>());

  std::clog << "3/4 / (7/-6): " << std::endl;
  printInfo(std::clog, intFraction<3,4>() / intFraction<7,-6>());

  std::clog << "1 * 3.0: " << std::endl;
  printInfo(std::clog, intFraction<1>() * 3.0);

  std::clog << "0 * 3.0: " << std::endl;
  printInfo(std::clog, intFraction<0>() * 3.0);

  std::clog << "0 + 3.0: " << std::endl;
  printInfo(std::clog, intFraction<0>() + 3.0);

  std::clog << "1 - 3.0: " << std::endl;
  printInfo(std::clog, intFraction<1>() - 3.0);

  std::clog << "4 - 3.0: " << std::endl;
  printInfo(std::clog, intFraction<4>() - 3.0);

  std::clog << "4/5 - 3.0: " << std::endl;
  printInfo(std::clog, intFraction<4,5>() - 3.0);

  //(with operand types 'Dune::ACFem::TypedValue::FractionConstant<long, 1, 1, double>' and 'double')
  std::clog << "1 * 3.0: " << std::endl;
  printInfo(std::clog, intFraction<1,1>() * 3.0);

  std::clog << BoolConstant<(intFraction<1,1>() == 1)>::value << std::endl;

  return 0;
}
