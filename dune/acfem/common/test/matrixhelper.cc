#include <config.h>

// iostream includes
#include <iostream>

#include <dune/acfem/common/scopedredirect.hh>

// include header of adaptive scheme
#include <dune/acfem/common/discretefunctionselector.hh>
#include <dune/acfem/functions/basicfunctions.hh>
#include <dune/acfem/models/basicmodels.hh>
#include <dune/acfem/algorithms/ellipticfemscheme.hh>
#include <dune/acfem/algorithms/stationaryadaptivealgorithm.hh>


// TODO: make a simpler test without depending on EllipticFemScheme

using namespace Dune::ACFem;

template<class HGridType>
void operatorMatrix(HGridType &grid)
{
  auto gridPart = leafGridPart(grid);
  auto discreteSpace = discreteFunctionSpace(gridPart, lagrange<1>);
  auto solution = discreteFunction(discreteSpace, "solution");

  // Define some basic ingredients
  auto X  = gridFunction(gridPart, [](auto&& x) { return x; });
  auto X0 = X[0_c];
  auto X1 = X[1_c];

  // auto exactSolution = exp(-C*sqr(X));
  auto exactSolution = X0+X1;
  //auto exactSolution = sin(2*M_PI*X0)*sin(2*M_PI*X1);

  // Dirichlet boundary conditions everywhere.
  auto Dbc = dirichletBoundaryModel(exactSolution);

  // bulk contributions
  auto DUDPhi = laplacianModel(discreteSpace);
  auto Mass = massModel(DUDPhi);

  std::clog << "------Mass Operator------" << std::endl;
  // now define the discrete model ...
  auto pdeModel = Mass;
  auto scheme = ellipticFemScheme(solution, pdeModel, exactSolution);
  scheme.printSystemMatrix(false, std::clog);
  scheme.printSystemMatrix(true, std::clog);

  std::clog << "------Laplace Operator------" << std::endl;
  auto pdeModel2 = DUDPhi;
  auto scheme2 = ellipticFemScheme(solution, pdeModel2, exactSolution);
  scheme2.printSystemMatrix(false, std::clog);
  scheme2.printSystemMatrix(true, std::clog);

  //std::clog << "------Laplace + Nitsche Dirichlet Values------" << std::endl;
  //auto pdeModel3 = DUDPhi + Dbc;
  //auto scheme3 = ellipticFemScheme(solution, pdeModel3, exactSolution);
  // FIXME: petsc has problems with printing this
  //scheme3.printSystemMatrix(std::clog);

  std::clog << "Writing a final line, so I dont have empty line at end of file for gitlab..." << std::endl;
}

// main
// ----

#ifndef SRCDIR
# define SRCDIR "."
#endif

int main (int argc, char **argv)
try
{
  // initialize MPI, if necessary
  Dune::Fem::MPIManager::initialize(argc, argv);

  //redirect the stream cerr to cout
  //so we only get the output of clog on stderr
  Dune::ACFem::ScopedRedirect redirect(std::cerr, std::cout);

  std::cerr << "MPI rank " << Dune::Fem::MPIManager::rank() << std::endl;

  // append overloaded parameters from the command line
  Dune::Fem::Parameter::append(argc, argv);

  // append possible given parameter files
  for (int i = 1; i < argc; ++i)
    Dune::Fem::Parameter::append(argv[ i ]);

  // append default parameter file
  Dune::Fem::Parameter::append(SRCDIR "/parameter");

  // type of hierarchical grid
  //typedef Dune::AlbertaGrid< 2 , 2 > GridType;
  typedef Dune::GridSelector::GridType  HGridType ;

  // create grid from DGF file
  const std::string gridkey = Dune::Fem::IOInterface::defaultGridKey(HGridType::dimension);
  const std::string gridfile = SRCDIR "/" + Dune::Fem::Parameter::getValue<std::string>(gridkey);

  // the method rank and size from MPIManager are static
  if (Dune::Fem::MPIManager::rank() == 0)
    std::cout << "Loading macro grid: " << gridfile << std::endl;

  // construct macro using the DGF Parser
  Dune::GridPtr< HGridType > gridPtr(gridfile);
  HGridType& grid = *gridPtr ;

  // let it go ... quasi adapt_method_stat()
  operatorMatrix(grid);

  return 0;
}
catch(const Dune::Exception &exception)
{
  std::cerr << "Error: " << exception << std::endl;
  return 1;
}
