#include <config.h>

#include "../../mpl/typetuple.hh"
#include "../typetraits.hh"
#include "../ostream.hh"

using namespace Dune;
using namespace Dune::ACFem;

struct Functor
{
  template<class... T>
  constexpr auto operator()(T&&...) const
  {
    return 0;
  }
};

struct PairFunctor
{
  template<class T0, class T1>
  constexpr auto operator()(const std::pair<T0, T1>&) const
  {
    return 0;
  }
};

struct TupleFunctor
{
  template<class... T>
  constexpr auto operator()(const MPL::TypeTuple<T...>&) const
  {
    return 0;
  }
};

int main(int argc, char *argvp[])
{
  std::clog << "Applicable to int:    " << IsApplicableTo<int, Functor>::value << std::endl;
  std::clog << "Applicable to pair:   " << IsApplicableTo<std::pair<int, int>, PairFunctor>::value << std::endl;
  std::clog << "Applicable to ttuple: " << IsApplicableTo<MPL::TypeTuple<int, int>, TupleFunctor>::value << std::endl;
  std::clog << "Applicable to multi:  " << IsApplicableTo<MPL::TypeTuple<int, int>, Functor>::value << std::endl;

  return 0;
}
