#include <config.h>

#include "../../expressions/constantoperations.hh"
#include "../../expressions/signutil.hh"
#include "../namedconstant.hh"
#include "../literals.hh"
#include "../ostream.hh"

using namespace Dune;
using namespace Dune::ACFem;
using namespace Dune::ACFem::Literals;

template<class T>
void printInfo(std::ostream& out, T&& t)
{
  std::clog << "  Value:  " << t << std::endl;
  std::clog << "  Type:   " << typeString(t) << std::endl;
  std::clog << "  scalar: " << IsScalar<T>::value << std::endl;
  std::clog << "  typed:  " << IsTypedValue<T>::value << std::endl;
  std::clog << "  zero:   " << expressionTraits(t).isZero << std::endl;
  std::clog << "  one:    " << expressionTraits(t).isOne << std::endl;
  std::clog << "  -one:   " << expressionTraits(t).isMinusOne << std::endl;
  std::clog << "  sign:   " << expressionSign(t) << std::endl;
  std::clog << "  int:    " << std::ptrdiff_t(t) << std::endl;
  std::clog << "    eq:   " << (std::ptrdiff_t(t) == t) << std::endl;
  std::clog << "  double: " << double(t) << std::endl;
  std::clog << "    eq:   " << (double(t) == t) << std::endl;
}

int main(int argc, char *argvp[])
{
  std::clog << TypedValue::NamedConstant<double, 'E'>{} << std::endl;
  printInfo(std::clog, TypedValue::NamedConstant<double, 'E'>{});

  return 0;
}
