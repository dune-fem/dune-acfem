#include <config.h>
#include <iostream>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/fem/space/common/functionspace.hh>
#include <dune/fem/quadrature/quadrature.hh>
#include <dune/fem/common/explicitfieldvector.hh>

#include "../types.hh"
#include "../ostream.hh"
#include "../quadraturepoint.hh"

using namespace Dune;
using namespace Dune::ACFem;

typedef Dune::Fem::FunctionSpace<double, double, 3, 3> FunctionSpaceType;
typedef FunctionSpaceType::DomainType DomainType;
typedef FunctionSpaceType::RangeType RangeType;
typedef FunctionSpaceType::JacobianRangeType JacobianRangeType;
//typedef FunctionSpaceType::HessianRangeType HessianRangeType;
typedef Fem::ExplicitFieldVector<FieldMatrix<double, 3, 3>, 3> HessianRangeType;
typedef FieldVector<FieldMatrix<double, 3, 3>, 3> OrigHessianType;

class EmptyQuadrature
{
 public:
  typedef DomainType CoordinateType;
  typedef DomainType LocalCoordinateType;
  typedef real_t<CoordinateType> RealType;
  const CoordinateType point(unsigned int) const { return CoordinateType(); }
  const RealType weight(unsigned int) const { return RealType(); }
  const LocalCoordinateType localPoint(unsigned int) const { return LocalCoordinateType(); }
  unsigned int nop() const { return 0; }
};

typedef FieldMatrix<double, 5, 5> MatrixType;

IndexConstant<0> conversion(...);
IndexConstant<1> conversion(const JacobianRangeType& arg);
IndexConstant<2> conversion(const HessianRangeType& arg);
IndexConstant<3> matrixConversion(const MatrixType& arg);

IndexConstant<0> quadPointConversion(...);

template<class Quadrature>
IndexConstant<4> quadPointConversion(const Fem::QuadraturePointWrapper<Quadrature> &x);


const RangeType& bar(const RangeType& arg)
{
  return arg;
}

template<class Quadrature>
auto foo(const QuadraturePoint<Quadrature>& arg)
{
  return arg.position();
}

class B
{};

template<class T>
class A
{
 public:
  A(const T&) {}
  operator T&() { return b_; }
  operator const T&() const { return b_; }
 protected:
  T b_;
};

template<class T>
void foobar(const A<T>& arg, std::ostream& out)
{
  out << "Type of arg: " << TypeString<decltype(arg)>{} << std::endl;
}

int main(int argc, char *argvp[])
{
  RangeType v;
  HessianRangeType a(v); // ok
  //HessianRangeType b = v; // must fail, implicit conversion
  OrigHessianType c = v;
  (void)c;
  HessianRangeType d(0);

  std::clog << "range: " << decltype(conversion(std::declval<RangeType>()))::value << std::endl;
  std::clog << "scalar: " << decltype(conversion(1.))::value << std::endl;
//  std::clog << "scalar: " << decltype(matrixConversion(1.))::value << std::endl;

  std::clog << "range: " << TypeString<RangeType>() << std::endl;
  std::clog << "quad: " << TypeString<decltype(quadPointConversion(std::declval<RangeType>()))>{} << std::endl;
  std::clog << "quad: " << TypeString<decltype(quadPointConversion(std::declval<JacobianRangeType>()))>{} << std::endl;
  std::clog << "quad: " << TypeString<decltype(quadPointConversion(std::declval<HessianRangeType>()))>{} << std::endl;
  std::clog << "quad: " << TypeString<decltype(foo(*RangeType()))>{} << std::endl;

  std::clog << "type of foo: " << TypeString<decltype(foo(*RangeType(0)))>{} << std::endl;

  std::clog << "foo: " << foo(*RangeType(0)) << std::endl;

  B b;
  foobar<B>(b, std::clog);

  return 0;
}
