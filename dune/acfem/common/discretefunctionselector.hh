#ifndef __DUNE_ACFEM_DISCRETE_FUNCTION_SELECTOR_HH__
#define __DUNE_ACFEM_DISCRETE_FUNCTION_SELECTOR_HH__

#if HAVE_CONFIG_H
# include <config.h>
#endif

#include <dune/fem/gridpart/adaptiveleafgridpart.hh>
#include <dune/fem/space/lagrange.hh>
#include <dune/fem/space/discontinuousgalerkin.hh>
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/function/blockvectorfunction.hh>

#include "types.hh"

#ifndef HAVE_PETSC
# define HAVE_PETSC false
#endif
#ifndef WANT_PETSC
# define WANT_PETSC false
#endif
#ifndef HAVE_DUNE_ISTL
# define HAVE_DUNE_ISTL false
#endif
#ifndef WANT_ISTL
# define WANT_ISTL false
#endif

namespace Dune {

  namespace ACFem {

    template<class T, class SFINAE = void>
    struct IsGridPart
      : FalseType
    {};

    template<class T>
    struct IsGridPart<T, std::enable_if_t<
        std::is_same<T, typename T::GridPartType>::value> >
      : TrueType
    {};

    enum class SolverFamily {
      PETSC,
      ISTL,
      FEM,
    };

    static constexpr auto defaultSolverFamily =
        (WANT_ISTL ? SolverFamily::ISTL :
          (WANT_PETSC ? SolverFamily::PETSC : SolverFamily::FEM));

    enum class FEType {
      Lagrange,
      DG,
      LagrangeDG,
    };

    template<FEType type, int degree>
    struct FE {};

    // allows use of e.g. lagrange<1> for linear lagrange elements
    template<int degree>
    const auto lagrange = FE<FEType::Lagrange, degree>{};

    template<int degree>
    const auto dG = FE<FEType::DG, degree>{};

    template<int degree>
    const auto lagrangeDG = FE<FEType::LagrangeDG, degree>{};

    namespace SelectorTraits {
      // FEType => DiscreteFunctionSpace

      template<class FunctionSpace, class GridPart, FEType elType, int elDegree>
      struct DiscreteFunctionSpace;

      template<class FunctionSpace, class GridPart, int elDegree>
      struct DiscreteFunctionSpace<FunctionSpace, GridPart, FEType::Lagrange, elDegree> {
        using type = Fem::LagrangeDiscreteFunctionSpace<FunctionSpace, GridPart, elDegree>;
      };

      template<class FunctionSpace, class GridPart, int elDegree>
      struct DiscreteFunctionSpace<FunctionSpace, GridPart, FEType::DG, elDegree> {
        using type = Fem::DiscontinuousGalerkinSpace<FunctionSpace, GridPart, elDegree>;
      };

      template<class FunctionSpace, class GridPart, int elDegree>
      struct DiscreteFunctionSpace<FunctionSpace, GridPart, FEType::LagrangeDG, elDegree> {
        using type = Fem::LagrangeDiscontinuousGalerkinSpace<FunctionSpace, GridPart, elDegree>;
      };

      template<class FunctionSpace, class GridPart, FEType elType, int elDegree>
      using DiscreteFunctionSpaceType = typename DiscreteFunctionSpace<FunctionSpace, GridPart, elType, elDegree>::type;

      // SolverFamily => DiscreteFunction

      template<class DiscreteFunctionSpace, SolverFamily solverFamily>
      struct DiscreteFunction;

      template<class DiscreteFunctionSpace>
      struct DiscreteFunction<DiscreteFunctionSpace, SolverFamily::PETSC> {
        using type = Fem::PetscDiscreteFunction<DiscreteFunctionSpace>;
      };

      template<class DiscreteFunctionSpace>
      struct DiscreteFunction<DiscreteFunctionSpace, SolverFamily::ISTL> {
        using type = Fem::ISTLBlockVectorDiscreteFunction<DiscreteFunctionSpace>;
      };

      template<class DiscreteFunctionSpace>
      struct DiscreteFunction<DiscreteFunctionSpace, SolverFamily::FEM> {
        using type = Fem::AdaptiveDiscreteFunction<DiscreteFunctionSpace>;
      };

      template<class DiscreteFunctionSpace, SolverFamily solverFamily>
      using DiscreteFunctionType = typename DiscreteFunction<DiscreteFunctionSpace, solverFamily>::type;
    }


    // Factory Functions


    // TODO: maybe make Partition default to InteriorBorder?
    template<PartitionIteratorType Partition = All_Partition, class Grid>
    auto leafGridPart(Grid& grid) {
      return Dune::Fem::AdaptiveLeafGridPart<Grid, Partition>{grid};
    }

    template<int dimRange = 1, class GridPart, FEType elType, int elDegree,
             std::enable_if_t<IsGridPart<GridPart>::value, int> = 0>
    constexpr auto discreteFunctionSpace(GridPart& gridPart, const FE<elType, elDegree>&) {
      using Grid = typename GridPart::GridType;
      using FunctionSpace = Fem::FunctionSpace<double, double, Grid::dimensionworld, dimRange>;

      return SelectorTraits::DiscreteFunctionSpaceType<FunctionSpace, GridPart, elType, elDegree>{gridPart};
    }

    template<PartitionIteratorType Partition, class Grid>
    auto& staticLeafGridPart(Grid& grid)
    {
      static auto gridPart = leafGridPart<Partition>(grid);

      return gridPart;
    }

    template<int dimRange = 1, class Grid, FEType elType, int elDegree,
             std::enable_if_t<!IsGridPart<Grid>::value, int> = 0>
    auto discreteFunctionSpace(Grid& grid, const FE<elType, elDegree>&) {
      constexpr PartitionIteratorType partition =
          (elType == FEType::Lagrange)? InteriorBorder_Partition : All_Partition;
      // this outlives the function call and is only assigned once, i.e. leafGridPart() is only called once.
      auto& gridPart = staticLeafGridPart<partition>(grid);
      using FunctionSpace = Fem::FunctionSpace<double, double, Grid::dimensionworld, dimRange>;
      using GridPartType = std::decay_t<decltype(gridPart)>;

      return SelectorTraits::DiscreteFunctionSpaceType<FunctionSpace, GridPartType, elType, elDegree>{gridPart};
    }

    template<SolverFamily solverFamily = defaultSolverFamily, class DiscreteFunctionSpace>
    auto discreteFunction(const DiscreteFunctionSpace& dfspace, std::string&& name = "f") {
      return SelectorTraits::DiscreteFunctionType<DiscreteFunctionSpace, solverFamily>{std::forward<std::string>(name), dfspace};
    }

    //!@} Convenience

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_DISCRETE_FUNCTION_SELECTOR_HH__
