#ifndef __DUNE_ACFEM_ENTITYSTORAGE_HH__
#define __DUNE_ACFEM_ENTITYSTORAGE_HH__

#include <vector>
#include <dune/fem/io/parameter.hh>

namespace Dune {

  namespace ACFem {

    template<class GridPart,
             class T,
             int codim = 0>
    class EntityStorage
    {
     public:
      using GridPartType = typename GridPart::GridPartType;
      using IndexSetType = typename GridPartType::IndexSetType;
      using Entity = typename GridPartType::GridType::template Codim<codim>::Entity;

      EntityStorage(const GridPartType& gridPart)
        : gridPart_(gridPart), indexSet_(gridPart_.indexSet()) {
        clear();
      }

      void clear() {
        storage_.resize(size());
        std::fill(storage_.begin(), storage_.end(), 0);
      }

      size_t size() const {
        return indexSet_.size(codim);
      }

      const IndexSetType& indexSet() const {
        return indexSet_;
      }

      auto begin() const {
        return storage_.begin();
      }

      auto end() const {
        return storage_.end();
      }

      auto begin() {
        return storage_.begin();
      }

      auto end() {
        return storage_.end();
      }

      const T& operator[](const size_t idx) const {
        return storage_[idx];
      }

      T& operator[](const size_t idx) {
        return storage_[idx];
      }

      const T& operator[](const Entity& entity) const {
        return (*this)[indexSet_.index(entity)];
      }

      T& operator[](const Entity& entity) {
        return (*this)[indexSet_.index(entity)];
      }

      auto& grid() const {
        return gridPart_.grid();
      }

      auto& gridPart() const {
        return gridPart_;
      }

     protected:
      const GridPartType& gridPart_;
      const IndexSetType& indexSet_;
      std::vector<T> storage_;
    };

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_COMMON_ENTITYSTORAGE_HH__
