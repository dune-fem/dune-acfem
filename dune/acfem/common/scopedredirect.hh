#ifndef __ACFEM_SCOPED_REDIRECT_HH_
#define __ACFEM_SCOPED_REDIRECT_HH_

#include <iostream>

namespace Dune {

  namespace ACFem {

    /**@addtogroup Convenience
      * @{
      */

    /**
      * A class to redirect streams.
      * Streams are redirected until the class goes out of scope.
      **/
    class ScopedRedirect
    {
    public:
      ScopedRedirect(std::ostream & inOriginal, std::ostream & inRedirect) :
        mOriginal(inOriginal),
        mOldBuffer(inOriginal.rdbuf(inRedirect.rdbuf()))
      { }

      ~ScopedRedirect()
      {
        mOriginal.rdbuf(mOldBuffer);
      }

    private:
      ScopedRedirect(const ScopedRedirect&);
      ScopedRedirect& operator=(const ScopedRedirect&);

      std::ostream & mOriginal;
      std::streambuf * mOldBuffer;
    };

    /** @} */
  } //end namespace ACFem
} // end namespace Dune

#endif  //__ACFEM_SCOPED_REDIRECT_HH_
