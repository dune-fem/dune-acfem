#ifndef __DUNE_ACFEM_STRINGCONVERSION_HH__
#define __DUNE_ACFEM_STRINGCONVERSION_HH__

#include <string>
#include <iostream>
#include <sstream>

#include "ostream.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup Convenience
     * @{
     */

    /** Convert any object which has an associated output stream operator
     * "<<" to a string, return @c true on success and @c false on failure.
     */
    template<typename T>
    std::string objectToString(const T& something)
    {
      std::ostringstream ss;

      if (ss << something) {
        return ss.str();
      } else {
        DUNE_THROW(NotImplemented, "Don't know how to convert from " << TypeString<T>{} << " to std::string");
        return ""; // not reached, avoid potential compiler warning
      }
    }

    /** Convert a string to any object with an associated input stream
     * operator ">>", return @c true on success and @c false on failure.
     */
    template<typename T>
    bool stringToObject(const std::string &text, T& something)
    {
      std::istringstream ss(text);

      return ss >> something;
    }

    //!@} Convenience

  } // ACFem::

} // Dune::

#endif //  __DUNE_ACFEM_STRINGCONVERSION_HH__
