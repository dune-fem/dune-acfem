#ifndef __DUNE_ACFEM_COMMON_TYPETRAITS_HH__
#define __DUNE_ACFEM_COMMON_TYPETRAITS_HH__

#include "typetraits/isapplicableto.hh"
#include "typetraits/removervaluereference.hh"

#endif //  __DUNE_ACFEM_COMMON_TYPETRAITS_HH__
