#ifndef __DUNE_ACFEM_STRINGHELPER_HH__
#define __DUNE_ACFEM_STRINGHELPER_HH__

#include <string>
#include <iostream>
#include <sstream>

namespace Dune {

  namespace ACFem {

    /**@addtogroup Convenience
     * @{
     */

    /**Check name for correct parenthesis. We simply do +-
     * book-keeping for parenthesis, if the count drops below 0 the
     * syntax is not correct, if the count is larger than 0 at the end
     * then the expression it is also not correct. This is only a
     * helper in order to remove the outer-most parenthesis. Provided
     * that the original syntax was correct, it is ok to remove the
     * outer most enclosing parenthesis if this function returns true.
     *
     *
     */
    static inline
    bool checkParenthesis(const std::string& name, unsigned start, unsigned end)
    {
      int parenCount = 0;
      for (auto i = start; i < end; ++i) {
        parenCount += name[i] == '(';
        parenCount -= name[i] == ')';
        if (parenCount < 0) {
          return false;
        }
      }
      return parenCount == 0;
    }

    //! Remove any outer redundant parenthesis.
    static inline
    void trimParenthesis(std::string& name)
    {
      while (!name.empty() &&
             name.front() == '(' &&
             name.back() == ')' &&
             checkParenthesis(name, 1, name.size()-1)) {
        name.erase(0, 1);
        name.erase(name.size()-1, 1);
      }
    }

    //!@} Convenience

  } // ACFem::

} // Dune::

#endif //  __DUNE_ACFEM_STRINGHELPER_HH__
