#ifndef __DUNE_ACFEM_COMMON_FRACTIONCONSTANT_HH__
#define __DUNE_ACFEM_COMMON_FRACTIONCONSTANT_HH__

#include <string>
#include <numeric>
#include <ratio>

#include <dune/acfem/common/types.hh>
namespace Dune {

  namespace ACFem {

    namespace TypedValue {

      /**A class implementing compile-time constant fractions of
       * integers. The class expects D to be positive and the fraction
       * to be normalized, i.e. the GCD of N and D must be 1.
       *
       * @paeam Int The underlying integer type.
       *
       * @param N The numerator value
       *
       * @param D The denominator value
       */
      template<class Int, Int N, Int D>
      struct FractionConstant
      {
        using IntegerType = Int;
        static constexpr IntegerType numerator = N;
        static constexpr IntegerType denominator = D;

        static_assert(D > 0 && (std::gcd(N, D) == 1 || std::gcd(N, D) == -1),
                      "GCD should be 1 here ... oops");

        constexpr FractionConstant()
        {}

        template<class Int2, Int2 N2, Int2 D2>
        constexpr FractionConstant(Constant<Int2, N2>, Constant<Int2, D2>)
        {
          static_assert((N == (Int)N2 / std::gcd((Int)N2, (Int)D2))
                        &&
                        (D == (Int)D2 / std::gcd((Int)N2, (Int)D2)),
                        "Assignment from integral constants only possible if their quotient matches the value of the fraction");
        }

        template<class Int2, Int2 N2>
        constexpr FractionConstant(Constant<Int2, N2>)
          : FractionConstant(Constant<Int, (Int)N2>{}, Constant<Int, (Int)1>{})
        {}

        constexpr FractionConstant(const FractionConstant&)
        {}

        constexpr FractionConstant(FractionConstant&&)
        {}

        constexpr FractionConstant& operator=(const FractionConstant&)
        {
          return *this;
        }

        template<class T, std::enable_if_t<std::numeric_limits<std::decay_t<T> >::is_specialized, int> = 0>
        constexpr operator T() const
        {
          return static_cast<T>(N) / static_cast<T>(D);
        }

      };

      /**@internal Default FalseType.*/
      template<class T, bool RequireDecay = false, class SFINAE = void>
      struct IsFractionConstant
        : FalseType
      {};

      template<class T>
      struct IsFractionConstant<T, false, std::enable_if_t<!IsDecay<T>::value> >
        : IsFractionConstant<std::decay_t<T> >
      {};

      template<class I, I N, I D, bool RequireDecay>
      struct IsFractionConstant<FractionConstant<I, N, D>, RequireDecay>
        : TrueType
      {};

      template<class T>
      constexpr T sign(T val) {
        return (T(0) < val) - (val < T(0));
      }

      template<class T>
      constexpr T abs(T val) {
        return sign(val) * val;
      }

      /**A normalized fraction of integeres with positive
       * denominator.
       */
      template<class I, I N, I D = 1>
      using Fraction =
        FractionConstant<I,
                         sign(D) * N / std::gcd(N, D),
                         abs(D) / std::gcd(N, D)>;

      /**Fraction of std::ptrdiff_t numbers.*/
      template<std::ptrdiff_t N, std::ptrdiff_t D = 1>
      using IntFraction = Fraction<std::ptrdiff_t, N, D>;

      /**Generate a fraction object.
       *
       * @param N Numerator value.
       *
       * @parma D Denominator value.
       *
       * @code
       * auto two = intFraction<2>();
       * auto half = intFraction<1,2>();
       * @endcode
       *
       * @return A normalize fraction with positive denominator.
       */
      template<std::ptrdiff_t N, std::ptrdiff_t D = 1>
      constexpr auto intFraction(IntConstant<N> = IntConstant<N>{}, IntConstant<D> = IntConstant<D>{})
      {
        return IntFraction<N, D>{};
      }

      template<std::size_t N, std::size_t D = 1>
      constexpr auto intFraction(IndexConstant<N>, IndexConstant<D> = IndexConstant<D>{})
      {
        return IntFraction<(std::ptrdiff_t)N, (std::ptrdiff_t)D>{};
      }

      template<class T>
      constexpr std::ptrdiff_t numerator(T&& t = T{})
      {
        static_assert(IsFractionConstant<T>::value, "T must fulfil IsFractionConstant<T>");
        return std::decay_t<T>::numerator;
      }

      template<class T>
      constexpr std::ptrdiff_t denominator(T&& t = T{})
      {
        static_assert(IsFractionConstant<T>::value, "T must fulfil IsFractionConstant<T>");
        return std::decay_t<T>::denominator;
      }

      /**Generate a sign as compile time constant.*/
      template<std::ptrdiff_t S>
      using Sign = IntFraction<(S > 0 ? 1 : (S < 0 ? -1 : 0))>;

      /**Generate a sign as constexpr function.*/
      template<std::ptrdiff_t S>
      constexpr auto sign(IntConstant<S> = IntConstant<S>{})
      {
        return Sign<S>{};
      }

      /**Sign of a non-integer type.*/
      template<class T>
      using SignOf = Sign<T{}>;

      /**Sign of a non-integer type as constexpr function.*/
      template<class T>
      constexpr auto signOf(T&&)
      {
        return SignOf<T>{};
      }

      /**@internal @c FalseType by default.*/
      template<class T, class SFINAE = void>
      struct IsSign;

      /**Only fraction constants can be signs.*/
      template<class T>
      struct IsSign<T, std::enable_if_t<!IsFractionConstant<T>::value> >
        : FalseType
      {};

      /**@c TrueType is T models a sign which means it is a
       * FractionConstant<I, N, D>, D == 1 and N*N <= 1.
       */
      template<class T>
      struct IsSign<T, std::enable_if_t<IsFractionConstant<T>::value> >
        : BoolConstant<(numerator<T>()*numerator<T>() <= 1 && denominator<T>() == 1)>
      {};

      /**@internal Promote given types to a "closure" by asking the compiler.*/
      template<class T1, class T2>
      using FieldPromotionType = std::decay_t<decltype(std::declval<T1>() * std::declval<T2>())>;

      template<class I, I N, I D>
      constexpr auto operator-(FractionConstant<I, N, D>)
      {
        return Fraction<I, -N, D>{};
      }

      template<class I, I N, I D>
      constexpr auto reciprocal(FractionConstant<I, N, D>)
      {
        static_assert(N != 0, "Reciprocal of zero attempted.");
        return Fraction<I, D, N>{};
      }

      template<class I1, I1 N1, I1 D1,
               class I2, I2 N2, I2 D2>
      constexpr auto operator*(FractionConstant<I1, N1, D1>, FractionConstant<I2, N2, D2>)
      {
        // normalize
        using T1 = Fraction<I1, N1, D1>;
        using T2 = Fraction<I2, N2, D2>;

        // cross normalize to reduce the chance of overflow
        using C1 = Fraction<FieldPromotionType<I1, I2>, T1::numerator, T2::denominator>;
        using C2 = Fraction<FieldPromotionType<I1, I2>, T2::numerator, T1::denominator>;

        // calculate
        return Fraction<FieldPromotionType<I1, I2>,
                        C1::numerator * C2::numerator,
                        C1::denominator * C2::denominator>{};
      }

      template<class I1, I1 N1, I1 D1,
               class I2, I2 N2, I2 D2>
      constexpr auto operator+(FractionConstant<I1, N1, D1>, FractionConstant<I2, N2, D2>)
      {
        // normalize
        using Int = FieldPromotionType<I1, I2>;
        using T1 = Fraction<Int, N1, D1>;
        using T2 = Fraction<Int, N2, D2>;

        // calculate

        // reduce the chance of overflow
        constexpr Int denomGcd = std::gcd(T1::denominator, T2::denominator);
        constexpr Int denom1 = T1::denominator/denomGcd;
        constexpr Int denom2 = T2::denominator/denomGcd;

        return Fraction<Int,
                        T1::numerator*denom2 + T2::numerator*denom1,
                        denom1*denom2*denomGcd>{};
      }

      template<class I1, I1 N1, I1 D1,
               class I2, I2 N2, I2 D2>
      constexpr auto operator-(FractionConstant<I1, N1, D1>, FractionConstant<I2, N2, D2>)
      {
        // normalize
        using Int = FieldPromotionType<I1, I2>;
        using T1 = Fraction<Int, N1, D1>;
        using T2 = Fraction<Int, N2, D2>;

        // calculate

        // reduce the chance of overflow
        constexpr Int denomGcd = std::gcd(T1::denominator, T2::denominator);
        constexpr Int denom1 = T1::denominator/denomGcd;
        constexpr Int denom2 = T2::denominator/denomGcd;

        return Fraction<Int,
                        T1::numerator*denom2 - T2::numerator*denom1,
                        denom1*denom2*denomGcd>{};
      }

      template<class I1, I1 N1, I1 D1,
               class I2, I2 N2, I2 D2>
      constexpr auto operator/(FractionConstant<I1, N1, D1> f1, FractionConstant<I2, N2, D2> f2)
      {
        static_assert(N2 != 0, "Division by Zero.");

        return f1 * reciprocal(f2);
      }

      template<class I1, I1 N1, class I2, I2 N2>
      constexpr auto operator%(FractionConstant<I1, N1, 1>, FractionConstant<I2, N2, 1>)
      {
        return Fraction<FieldPromotionType<I1, I2>, (N1 % N2), 1>{};
      }

      template<class I1, I1 N1, class I2, I2 N2>
      constexpr auto div(FractionConstant<I1, N1, 1>, FractionConstant<I2, N2, 1>)
      {
        return Fraction<FieldPromotionType<I1, I2>, N1 / N2, 1>{};
      }

      /**Provide pow for integral exponents.*/
      template<class I1, I1 N1, I1 D1, class I2>
      constexpr auto pow(FractionConstant<I1, N1, D1>, FractionConstant<I2, 0, 1>)
      {
        return FractionConstant<I1, 1, 1>{};
      }

      /**Provide pow for integral exponents.*/
      template<class I1, I1 N1, I1 D1, class I2>
      constexpr auto pow(FractionConstant<I1, N1, D1>, FractionConstant<I2, 1, 1>)
      {
        return FractionConstant<I1, N1, D1>{};
      }

      /**Provide pow for integral exponents.*/
      template<class I1, I1 N1, I1 D1, class I2, I2 N2, std::enable_if_t<(N2 > 1), int> = 0>
      constexpr auto pow(FractionConstant<I1, N1, D1>, FractionConstant<I2, N2, 1>)
      {
        return FractionConstant<I1, N1, D1>{}*pow(FractionConstant<I1, N1, D1>{}, FractionConstant<I2, N2-1, 1>{});
      }

      template<class I, I N, I D>
      constexpr auto sqr(FractionConstant<I, N, D>)
      {
        return pow(FractionConstant<I, N, D>{}, FractionConstant<I, 2, 1>{});
      }

      /**Provide pow for integral exponents.*/
      template<class I1, I1 N1, I1 D1, class I2, I2 N2, std::enable_if_t<(N2 < 0 && N1 != 0), int> = 0>
      constexpr auto pow(FractionConstant<I1, N1, D1>, FractionConstant<I2, N2, 1>)
      {
        return pow(Fraction<I1, D1, N1>{}, FractionConstant<I2, N2, 1>{});
      }

      template<class I, I N, I D>
      constexpr auto floor(FractionConstant<I, N, D>)
      {
        if constexpr (D == 1) {
          return FractionConstant<I, N, 1>{};
        } else if constexpr (N < 0) {
          // -1.5 = -3/2 -> -2
          // -0.5 = -1/2 -> -1
          return FractionConstant<I, N/D-1, 1>{};
        } else {
          // 1.5 = 3/2 -> 1
          return FractionConstant<I, N/D, 1>{};
        }
      }

      template<class I, I N, I D>
      constexpr auto ceil(FractionConstant<I, N, D>)
      {
        if constexpr (D == 1) {
          return FractionConstant<I, N, 1>{};
        } else {
          // -1.5 = -3/2 -> (-3+1)/2 = -1
          // 1.5 = 3/2 -> (3+1)/2 = 2
          return FractionConstant<I, (N+1)/D, 1>{};
        }
      }

      //! Round to nearest
      template<class I, I N, I D>
      constexpr auto round(FractionConstant<I, N, D>)
      {
        // -9/14, (-9+14)/14 = 5/14 = 0
        return FractionConstant<I, (N < 0 ? (N-D/2)/D : (N-D/2)/D) , 1>{};
      }

      template<class I, I N, I D>
      constexpr auto abs(FractionConstant<I, N, D>)
      {
        if constexpr (N < 0) {
          return FractionConstant<I, -N, D>{};
        } else {
          return FractionConstant<I, N, D>{};
        }
      }

      template<class I1, I1 N1, I1 D1,
               class I2, I2 N2, I2 D2>
      constexpr bool operator==(FractionConstant<I1, N1, D1> f1, FractionConstant<I2, N2, D2> f2)
      {
        // closure type
        using Int = FieldPromotionType<I1, I2>;

        // normalize
        using T1 = Fraction<Int, N1, D1>;
        using T2 = Fraction<Int, N2, D2>;

        // reduce the chance of overflow
        constexpr Int denomGcd = std::gcd(T1::denominator, T2::denominator);
        constexpr Int denom1 = T1::denominator/denomGcd;
        constexpr Int denom2 = T2::denominator/denomGcd;

        return T1::numerator*denom2 == T2::numerator*denom1;
      }

      template<class I1, I1 N1, I1 D1,
               class I2, I2 N2, I2 D2>
      constexpr bool operator!=(FractionConstant<I1, N1, D1> f1, FractionConstant<I2, N2, D2> f2)
      {
        return !(f1 == f2);
      }

      template<class I1, I1 N1, I1 D1,
               class I2, I2 N2, I2 D2>
      constexpr bool operator<(FractionConstant<I1, N1, D1> f1, FractionConstant<I2, N2, D2> f2)
      {
        // closure type
        using Int = FieldPromotionType<I1, I2>;

        // normalize
        using T1 = Fraction<Int, N1, D1>;
        using T2 = Fraction<Int, N2, D2>;

        // reduce the chance of overflow
        constexpr Int denomGcd = std::gcd(T1::denominator, T2::denominator);
        constexpr Int denom1 = T1::denominator/denomGcd;
        constexpr Int denom2 = T2::denominator/denomGcd;

        return T1::numerator*denom2 < T2::numerator*denom1;
      }

      template<class I1, I1 N1, I1 D1,
               class I2, I2 N2, I2 D2>
      constexpr bool operator<=(FractionConstant<I1, N1, D1> f1, FractionConstant<I2, N2, D2> f2)
      {
        return f1 < f2 || f1 == f2;
      }

      template<class I1, I1 N1, I1 D1,
               class I2, I2 N2, I2 D2>
      constexpr bool operator>(FractionConstant<I1, N1, D1> f1, FractionConstant<I2, N2, D2> f2)
      {
        return f2 < f1;
      }

      template<class I1, I1 N1, I1 D1,
               class I2, I2 N2, I2 D2>
      constexpr bool operator>=(FractionConstant<I1, N1, D1> f1, FractionConstant<I2, N2, D2> f2)
      {
        return f2 <= f1;
      }

      using std::min;

      template<class I1, I1 N1, I1 D1,
               class I2, I2 N2, I2 D2,
               std::enable_if_t<!std::is_same<FractionConstant<I1, N1, D1>,
                                              FractionConstant<I2, N2, D2> >::value, int> = 0>
      constexpr auto min(FractionConstant<I1, N1, D1> f1, FractionConstant<I2, N2, D2> f2)
      {
        if constexpr (f1 <= f2) {
          return f1;
        } else {
          return f2;
        }
      }

      using std::max;

      template<class I1, I1 N1, I1 D1,
               class I2, I2 N2, I2 D2,
               std::enable_if_t<!std::is_same<FractionConstant<I1, N1, D1>,
                                              FractionConstant<I2, N2, D2> >::value, int> = 0>
      constexpr auto max(FractionConstant<I1, N1, D1> f1, FractionConstant<I2, N2, D2> f2)
      {
        if constexpr (f1 >= f2) {
          return f1;
        } else {
          return f2;
        }
      }

      template<class I, I N, I D>
      std::string toString(FractionConstant<I, N, D>)
      {
        // normalize
        using T = Fraction<I, N, D>;
        if constexpr (T::denominator == 1) {
          return std::to_string(T::numerator) + "_c";
        } else {
          return "(" + std::to_string(T::numerator) + "/" + std::to_string(T::denominator) + ")_c";
        }
      }

      /**Print FractionConstant.*/
      template<class I, I N, I D>
      std::ostream& operator<<(std::ostream& out, FractionConstant<I, N, D> frac)
      {
        return out << toString(frac);
      }

    } // NS TypedValue

    template<class T>
    using IsFractionConstant = TypedValue::IsFractionConstant<T>;

  } // namespace ACFem

} // Namespace Dune

#endif // __DUNE_ACFEM_COMMON_FRACTIONCONSTANT_HH__
