#ifndef __DUNE_ACFEM_INTERSECTIONDATAHANDLE_HH__
#define __DUNE_ACFEM_INTERSECTIONDATAHANDLE_HH__

#include <map>
#include <dune/common/version.hh>
#include <dune/grid/common/datahandleif.hh>
#include <dune/common/version.hh>

namespace Dune  {

  namespace ACFem {

    template<class Storage>
    struct IntersectionDataHandleTraits
    {
      typedef Storage StorageType;
      typedef typename StorageType::value_type IntersectionStorageType;
      typedef typename IntersectionStorageType::value_type DataItemType;
    };

    template<class Key, class IntersectionStorage, class... Further>
    struct IntersectionDataHandleTraits<std::map<Key, IntersectionStorage, Further...> >
    {
      typedef std::map<Key, IntersectionStorage, Further...> StorageType;
      typedef IntersectionStorage IntersectionStorageType;
      typedef typename IntersectionStorageType::value_type DataItemType;
    };

    /**General intersection - intersection communication which
     * communicates for each intersection a potentially variable
     * number of data-items.
     *
     * @param IndexSet The index set for the entities of the
     * underlying GridView.
     *
     * @param Storage A random access container class which allows for
     * indexing with the face index-set. Each element of the container
     * is itself a container with elements which have to be compatible
     * with the Operation functor (i.e. Operation::apply(src, dst)
     * must be possible with the elements of the per-face-entity
     * containers).
     *
     * @param Operation A functor which is called in the scatter()
     * method.
     */
    template<class IndexSet, class Storage, class Operation>
    class IntersectionDataHandle
      : public CommDataHandleIF<IntersectionDataHandle<IndexSet, Storage, Operation>,
                                typename IntersectionDataHandleTraits<Storage>::DataItemType>
    {
    public:
      typedef IndexSet IndexSetType;
      typedef Storage CommunicationStorageType;
     private:
      typedef IntersectionDataHandleTraits<Storage> TraitsType;
      typedef typename TraitsType::IntersectionStorageType IntersectionStorageType;
      typedef typename TraitsType::DataItemType DataItemType;
      typedef Operation OperationType;
     public:

      IntersectionDataHandle(const IndexSetType& idxSet, CommunicationStorageType& storage)
        : indexSet_(idxSet), storage_(storage)
      {}

      IntersectionDataHandle(const IntersectionDataHandle& other)
        : indexSet_(other.indexSet_), storage_(other.storage_)
      {}

      //! returns true if data for this codim should be communicated
      bool contains(int dim, int codim) const
      {
        return codim == 1;
      }

      /**This entire beast is meant for communication like arrays of
       * values at each quadrature point, hence we cannot be fixedsize
       * in the most general case (multiple intersections, different
       * kinds of reference elements).
       */
      bool fixedSize(int dim, int codim) const
      {
        return false;
      }

      /** how many objects of type DataItemType have to be sent for a given entity.
       * Note: Only the sender side needs to know this size.
       */
      template<class Entity>
      size_t size(const Entity& e) const
      {
        return storage_[indexSet_.index(e)].size();
      }

      //! pack data from user to message buffer
      template<class MessageBuffer, class Entity>
      void gather(MessageBuffer& buff, const Entity& e) const
      {
        for (const auto data : storage_[indexSet_.index(e)]) {
          buff.write(data);
        }
      }

      //! unpack data from message to user buffer
      template<class MessageBuffer, class Entity>
      void scatter(MessageBuffer& buff, const Entity& e, size_t n)
      {
        assert(n == size(e));
        for (auto& data : storage_[indexSet_.index(e)]) {
          typename std::decay<decltype(data)>::type tmp;
          buff.read(tmp);
#if DUNE_VERSION_NEWER(DUNE_FEM, 2, 5)
          OperationType()(tmp, data);
#else
          OperationType::apply(tmp, data);
#endif
        }
      }

    private:
      const IndexSetType& indexSet_;
      CommunicationStorageType& storage_;
    };

  } // namespace ACFem

} // namespace Dune


#endif //  __DUNE_ACFEM_INTERSECTIONDATAHANDLE_HH__
