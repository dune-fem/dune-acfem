#ifndef __DUNE_ACFEM_COMMON_GEOMETRY_HH__
#define __DUNE_ACFEM_COMMON_GEOMETRY_HH__

#include <cmath>

#include <dune/geometry/referenceelements.hh>

namespace Dune {

  namespace ACFem {

    /**@addtogroup Convenience
     * @{
     */

    /**Compute a rough estimate of the square of the diameter of the
     * element's Geometry.
     */
    template<class Geometry>
    typename Geometry::ctype h2Estimate(const Geometry& geometry)
    {
      const int dimension = Geometry::mydimension;
      typename Geometry::ctype volume = geometry.volume();

      switch(dimension) {
      case 1:
        return volume*volume;
      case 2:
        return volume;
      case 3:
        return std::pow(volume, 2.0/(double)dimension);
      default:
        return -1.0;
      }
    }

    /**Compute a rough estimate of the diameter of the
     * element's Geometry.
     */
    template<class Geometry>
    typename Geometry::ctype hEstimate(const Geometry& geometry)
    {
      typedef typename Geometry::ctype ctype;
      const int dimension = Geometry::mydimension;
      const ctype volume = geometry.volume();

      switch(dimension) {
      case 1:
        return volume;
      case 2:
        return std::sqrt(volume);
      case 3:
        return std::pow(volume, 1.0/(double)dimension);
      default:
        return -1.0;
      }
    }

    /**Export some hard-to-get-at things for geometries. This is like
     * Fem::GeometryInformation, but with static methods.
     */
    template<class GridPart, int codim = 0>
    class GeometryInformation
    {
      using ThisType = GeometryInformation<GridPart, codim>;
    public:
      //! grid type
      using GridPartType = GridPart;

      //! dimension
      static constexpr int dim = GridPartType::dimension - codim;

      //! coordinate type
      using ctype = typename GridPartType::ctype;

     protected:
      using ReferenceElementsType = Dune::ReferenceElements<ctype, dim>;

     public:
      //! type of reference element
      using ReferenceElementType = std::decay_t<decltype(ReferenceElementsType::general(std::declval<const GeometryType&>()))>;

      //! type of domain vector, local coordinates.
      using DomainType = FieldVector<ctype, dim>;

    public:
      //! return local bary center for geometry of type type
      static const DomainType localCenter(const GeometryType &type)
      {
        return type.isNone() ? DomainType(0) : referenceElement(type).position(0, 0);
      }

      //! return volume of reference element for geometry of type type
      static ctype referenceVolume (const GeometryType &type)
      {
        return type.isNone() ? ctype(1.0) : referenceElement(type).volume();
      }

      //! return reference element for type
      static auto referenceElement(const GeometryType &type)
      {
        assert(!type.isNone());
        return ReferenceElementsType::general( type );
      }
    };

    /**@}*/

  } // namespace ACFem

} // namespace Dune

#endif // __DUNE_ACFEM_COMMON_GEOMETRY_HH__
