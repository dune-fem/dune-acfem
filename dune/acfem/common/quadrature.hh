#ifndef __DUNE_ACFEM_QUADRATURE_HH__
#define __DUNE_ACFEM_QUADRATURE_HH__

#include <dune/fem/quadrature/cachingquadrature.hh>
#include <dune/fem/quadrature/intersectionquadrature.hh>
#include <dune/fem/quadrature/lumpingquadrature.hh>

#include "types.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup Quadrature
     *
     * Quadrature related traits.
     */

    /**Helper traits-class, defining likely quadrature types.
     */
    template<class GridPart>
    struct DefaultQuadratureTraits
    {
      typedef GridPart GridPartType; //!< Exported template argument.

      /**The quadrature to use for integrating over bulk elements. */
      typedef Fem::CachingQuadrature<GridPartType, 0> BulkQuadratureType;

      /**The quadrature to use for integrating over faces. */
      typedef Fem::CachingQuadrature<GridPartType, 1> FaceQuadratureType;

      /**The quadrature to use for integrating mass
       * contributions. The quadrature potentially differes from
       * BulkQuadratureType in order to allow for mass-lumping. In
       * this case hasMassQuadrature is set to true.
       */
      typedef BulkQuadratureType BulkMassQuadratureType;

      /** The quadrature to use for integrating mass
       * contributions over faces. The quadrature potentially differes from
       * BulkQuadratureType in order to allow for mass-lumping. In
       * this case hasFaceMassQuadrature is set to true.
       */
      typedef FaceQuadratureType FaceMassQuadratureType;

      /**Template type for conforming/non-conforming intersections. */
      template<bool conforming>
      using IntersectionQuadrature = Fem::IntersectionQuadrature<FaceQuadratureType, conforming>;

      /**Template type for conforming/non-conforming intersections for mass contributions. */
      template<bool conforming>
      using IntersectionMassQuadrature = Fem::IntersectionQuadrature<FaceMassQuadratureType, conforming>;

      /** Set to true if BulkMassQuadratureType differs from BulkQuadratureType. */
      static const bool hasMassQuadrature = false;

      /** Set to true if FaceMassQuadrature differs from FaceQuadratureType. */
      static const bool hasFaceMassQuadrature = false;
    };

    /**Helper traits-class, defining likely quadrature types for
     * mass-lumping. The default is not to lump face quadratures.
     *
     * @param[in] GridPartType Type of the underlying grid.
     *
     * @param[in] faceLumping Really want also to use mass-lumping on faces?
     */
    template<class GridPart, bool bulkLumping, bool faceLumping>
    struct MaybeLumpingQuadratureTraits
      : public DefaultQuadratureTraits<GridPart>
    {
     protected:
      typedef DefaultQuadratureTraits<GridPart> BaseType;
     public:
      /**\copydoc DefaultQuadratureTraits::GridPartType */
      typedef typename BaseType::GridPartType GridPartType;
      /**\copydoc DefaultQuadratureTraits::BulkQuadratureType */
      typedef typename BaseType::BulkQuadratureType BulkQuadratureType;
      /**\copydoc DefaultQuadratureTraits::FaceQuadratureType */
      typedef typename BaseType::FaceQuadratureType FaceQuadratureType;

      /**\copydoc DefaultQuadratureTraits::IntersectionQuadrature */
      using BaseType::IntersectionQuadrature;

      /**Provides a lumping quadrature type in order to do mass-lumping. */
      using BulkMassQuadratureType =
        ConditionalType<bulkLumping,
                        Fem::CachingLumpingQuadrature<GridPartType, 0>,
                        typename BaseType::BulkMassQuadratureType>;

      /**Potentially provides a lumping quadrature for faces, if the
       * template argument faceLumpingh is set to @c true. In this
       * case hasFaceMassQuadratureType is set to @c true.
       */
      using FaceMassQuadratureType =
        ConditionalType<faceLumping,
                        Fem::CachingLumpingQuadrature<GridPartType, 1>,
                        typename BaseType::FaceMassQuadratureType>;

      /**\copydoc DefaultQuadratureTraits::IntersectionMassQuadrature */
      template<bool conforming>
      using IntersectionMassQuadrature = Fem::IntersectionQuadrature<FaceMassQuadratureType, conforming>;

      /**Indicates that BulkMassQudratureType provides a lumping quadrature. */
      static const bool hasMassQuadrature = bulkLumping;

      /** Set to true if FaceMassQuadrature differs from
       * FaceQuadratureType and provides a lumping quadrature type.
       */
      static const bool hasFaceMassQuadrature = faceLumping;
    };

    /**Traits class with lumping quadratures in the bulk and on the skeleton.
     *
     * @note Defining this as a using-template-alias somehow does not
     * work; gcc complains about the template-parameter list when
     * using quadrature traits as template-template parameters.
     */
    template<class GridPart>
    using LumpingQuadratureTraits = MaybeLumpingQuadratureTraits<GridPart, true, true>;

    /**Traits class with lumping quadratures in the bulk but not on the skeleton. */
    template<class GridPart>
    using BulkLumpingQuadratureTraits = MaybeLumpingQuadratureTraits<GridPart, true, false>;

    /**Traits class with lumping quadratures only on the skeleton. */
    template<class GridPart>
    using FaceLumpingQuadratureTraits = MaybeLumpingQuadratureTraits<GridPart, false, true>;

    /**Traits class with ordinary quadratures in the bulk and on the skeleton. */
    template<class GridPart>
    using QuadratureTraits = DefaultQuadratureTraits<GridPart>;

    //!@} Quadrature

  } // namespace ACFem

} // namespace Dune

#endif // __DUNE_ACFEM_QUADRATURE_HH__
