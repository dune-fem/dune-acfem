#ifndef __DUNE_ACFEM_DOFMAPPERTUPLEDATAHANDLE_HH__
#define __DUNE_ACFEM_DOFMAPPERTUPLEDATAHANDLE_HH__

#include <dune/grid/common/datahandleif.hh>

#include "containertuple.hh"

namespace Dune  {

  namespace ACFem {

    /**Traits for just something indexable. */
    template<class... Storages>
    struct DofMapperTupleDataHandleTraits
    {
      typedef ContainerTuple<Storages...> StorageType;
      typedef typename StorageType::ConstValueType DataItemType;
      typedef typename StorageType::ValueType DataItemScatterType;
    };

    /**Potential all - all communication for mapped data. Depends on
     * the mapper. Used e.g. to communicate Dirichlet
     * constraints. Stolen from an equivalent class from
     * Dune::FemPy. The most convenient way to generate your
     * DofMapperTupleDataHandle instance is provided by the "generator
     * function" dofMapperTupleDataHandle().
     *
     * @param Mapper The type of the mapper to use. Probably from a
     * discrete space.
     *
     * @param Storage A random access container class which allows for
     * indexing with the indices provided by the mapper. In the
     * simplest case this could be a std::vector<> which just has
     * enough space for the size of the mapper.
     *
     * @param Operation A functor which is called in the scatter()
     * method. The items stored in Storage have to support the
     * operation, of course.
     */
    template<class Mapper, class Operation, class... Storages>
    class DofMapperTupleDataHandle
      : public CommDataHandleIF<DofMapperTupleDataHandle<Mapper, Operation, Storages...>,
                                typename DofMapperTupleDataHandleTraits<Storages...>::DataItemType>
    {
    public:
      typedef Mapper MapperType;
      typedef Operation OperationType;
     private:
      typedef DofMapperTupleDataHandleTraits<Storages...> TraitsType;
      typedef typename TraitsType::DataItemType DataItemType;
      typedef typename TraitsType::DataItemScatterType DataItemScatterType;
      typedef ContainerTuple<Storages...> StorageTupleType;
     public:
      /**Construct the data handle from the given mapper and storage
       * container.
       *
       * @param[in] mapper The dof-mapper. Probably discreteSpace.blockMapper().
       *
       * @param[in] operator An instance of Operation. Operation must
       * supply an Operation::operator()(dest, src) where @c dest and
       * @c src must accepts arguments of the type generated
       * by ContainerTuple::operator[](). Lambdas are allowed, e.g.
       * @code
       * [](const auto& a, auto& b) { ... }
       * @endcode
       *
       *
       *
       * @param[in] storages Storage container argument pack. The @a
       * storages are stuffed into a ContainerTuple. In order for this
       * to work they have to supply a random access operator[]().
       */
      DofMapperTupleDataHandle(const MapperType& mapper,
                               const Operation& operation,
                               Storages&... storages)
        : mapper_(mapper), operation_(operation), storages_(storages...)
      {}

      DofMapperTupleDataHandle(const DofMapperTupleDataHandle& other)
        : mapper_(other.mapper_), operation_(other.operation_), storages_(other.storages_)
      {}

      //! returns true if data for this codim should be communicated
      bool contains(int dim, int codim) const
      {
        return mapper_.contains(codim);
      }

      /**We don't have a fixed-size. Depends on the mapper and the
       * number of DoFs on sub-entities.
       */
      bool fixedSize(int dim, int codim) const
      {
        return false;
      }

      /** how many objects of type DataItemType have to be sent for a given entity.
       * Note: Only the sender side needs to know this size.
       */
      template<class Entity>
      size_t size(const Entity& entity) const
      {
        return mapper_.numEntityDofs(entity);
      }

      //! pack data from user to message buffer
      template<class MessageBuffer, class Entity>
      void gather(MessageBuffer& buff, const Entity& entity) const
      {
        size_t localBlocks = mapper_.numEntityDofs(entity);
        std::vector<std::size_t> globalBlockDofs(localBlocks);
        mapper_.mapEntityDofs(entity, globalBlockDofs);
        assert(size(entity) == globalBlockDofs.size());
        for (size_t localBlock = 0; localBlock < globalBlockDofs.size(); ++ localBlock) {
          buff.write(storages_[globalBlockDofs[localBlock]]);
        }
      }

      //! unpack data from message to user buffer
      template<class MessageBuffer, class Entity>
      void scatter(MessageBuffer& buff, const Entity& entity, size_t n)
      {
        size_t localBlocks = mapper_.numEntityDofs(entity);
        std::vector<std::size_t> globalBlockDofs(localBlocks);
        mapper_.mapEntityDofs(entity, globalBlockDofs);
        assert(size(entity) == n);
        assert(size(entity) == globalBlockDofs.size());
        for (size_t localBlock = 0; localBlock < globalBlockDofs.size(); ++ localBlock) {
          DataItemType src;
          buff.read(src);
          // We need the variable dest, because if one of the
          // tuple-components uses Fem::SubVector as blocks then dest
          // must contain a non-const copy of the block. Hence we
          // cannot pass dest as a const argument to operation_(), but
          // then without dest we would pass a non-const rvalue
          // reference as an lvalue reference to operation_() which is
          // not allowed.
          auto dest = storages_[globalBlockDofs[localBlock]];
          operation_(src, dest);
        }
      }

     private:
      const MapperType& mapper_;
      const Operation operation_;
      StorageTupleType storages_;
    };

    /**Generate a communication object which will synchronize DoF data
     * across all affected (sub-)entities.
     *
     * @param[in] space The discrete space which provides the
     * respective DoF-mapper.
     *
     * @param[in] operation A function, or lambda or std::function
     * which manipulates the data after communication. The most
     * probable sensible action is to add the data from the other
     * computing node to _this_ computing node. The calling convention
     * for operations is:
     *
     * [](const
     */
    template<class Space, class Operation, class... Storages,
             std::enable_if_t<sizeof(typename Space::BlockMapperType) >= 0, int> = 0>
    auto dofMapperTupleDataHandle(const Space& space,
                                  const Operation& operation,
                                  Storages&... storages)
    {
      typedef DofMapperTupleDataHandle<typename Space::BlockMapperType, Operation, Storages...> ResultType;
      return ResultType(space.blockMapper(), operation, storages...);
    }

  } // namespace ACFem

} // namespace Dune


#endif // __DUNE_ACFEM_DOFMAPPERTUPLEDATAHANDLE_HH__
