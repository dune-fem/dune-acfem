#ifndef __DUNE_ACFEM_LOCALOBJECTSTORAGE_HH__
#define __DUNE_ACFEM_LOCALOBJECTSTORAGE_HH__

#include <dune/fem/storage/objectstack.hh>

namespace Dune {

  namespace ACFem {

    /**@addtogroup Convenience
     * @{
     */

    /**Define a factory for a "local object" which has a constructor
     * which accepts a single argument, a "global object". Like local
     * functions or local functionals or local operators which can be
     * constructed from their global representations.
     */
    template<class GlobalObject, class LocalObject>
    class LocalObjectFactory
    {
     public:
      typedef GlobalObject GlobalObjectType;
      typedef LocalObject ObjectType;

     public:
      explicit LocalObjectFactory(GlobalObjectType& global)
        : globalObject_(global)
      {}

      ObjectType *newObject() const
      {
        return new ObjectType(globalObject_);
      }

     protected:
      GlobalObjectType& globalObject_;
    };

    /**Local object stack which (hopefully) efficiently caches local
     * objects. This is only useful if the construction of the local
     * object is rather complicated and implies, e.g., the allocation
     * of additional storage. A local object storing for example only
     * a reference to its global parent object should not make use of
     * such a stack.
     */
    template<class Factory, template<class> class Wrapper>
    class LocalObjectStack
      : public Fem::ObjectStack<Factory>
    {
      typedef LocalObjectStack ThisType;
      typedef Fem::ObjectStack<Factory> BaseType;
     public:
      typedef Factory LocalObjectFactoryType;
      typedef typename LocalObjectFactoryType::ObjectType LocalObjectType;
      typedef typename LocalObjectFactoryType::GlobalObjectType GlobalObjectType;
      typedef Wrapper<ThisType> LocalWrapperType;

     public:
      explicit LocalObjectStack(const LocalObjectFactoryType& factory)
        : BaseType(factory)
      {}

     private:
      LocalObjectStack(const ThisType&);
      ThisType& operator=(const ThisType&);

     public:
      LocalWrapperType localObject()
      {
        return LocalWrapperType(*this);
      }

      template<class Entity>
      LocalWrapperType localObject(Entity& entity)
      {
        return LocalWrapperType(entity, *this);
      }

      template<class Entity>
      const LocalWrapperType localObject(Entity& entity) const
      {
        return LocalWrappertType(entity, *this);
      }
    };

    //!Provide a writable object stack for technical reasons.
    template<class Storage>
    struct MutableLocalObjectStorageProvider
    {
      typedef Storage StorageType;
      typedef typename StorageType::LocalObjectFactoryType FactoryType;
      typedef typename StorageType::GlobalObjectType GlobalObjectType;

      MutableLocalObjectStorageProvider(GlobalObjectType& global)
        : factory_(global),
          storage_(factory_)
      {}
     private:
      MutableLocalObjectStorageProvider(const MutableLocalObjectStorageProvider&) = delete;
      MutableLocalObjectStorageProvider(MutableLocalObjectStorageProvider&&) = delete;
     public:

      StorageType& storage() const
      {
        return storage_;
      }

     protected:
      FactoryType factory_;
      mutable StorageType storage_;
    };

    //!Provide a writable pseudo object stack which is only a
    //!reference to the global object. This is a utility class which
    //!can be used to handle dynamic allocation of local objects and
    //!allocation as automatic variable with the same code
    //!constructs. See DiscreteLinearFunctional for an example.
    template<class Global>
    struct MutableNoStorageProvider
    {
      typedef Global StorageType;
      typedef Global GlobalObjectType;

      MutableNoStorageProvider(GlobalObjectType& global)
        : global_(global)
      {}

      GlobalObjectType& storage() const
      {
        return global_;
      }

     protected:
      GlobalObjectType& global_;
    };

    //! @} Convenience

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_LOCALOBJECTSTORAGE_HH__
