#ifndef __DUNE_ACFEM_COMMON_FLOPS_HH__
#define __DUNE_ACFEM_COMMON_FLOPS_HH__

#include <dune/fem/misc/flops.hh>

namespace Dune {

  namespace ACFem {

    class FlopCounter
      : public Fem::FlopCounter
    {
      using BaseType = Fem::FlopCounter;
     public:
      static bool present()
      {
#if HAVE_PAPI
        return true;
#else
        return false;
#endif
      }
    };

  }
}

#endif // __DUNE_ACFEM_COMMON_FLOPS_HH__
