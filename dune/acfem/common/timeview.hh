#ifndef TIMEVIEW_HH
#define TIMEVIEW_HH

#include <dune/fem/solver/timeprovider.hh>

namespace Dune {

  namespace ACFem {

    /**@addtogroup PDE-Models
     * @{
     */

    /**\brief Generate a view on the current time-step.
     *
     * Provide a fixed relative point in time within the current time
     * interval.
     *
     * @param[in] TimeProvider
     */
    template<class TimeProvider>
    class TimeView
    {
     public:
      typedef TimeProvider TimeProviderType;

      //!Constructor, default is a view at the start of the time interval.
      TimeView(const TimeProviderType& origin, double theta = 0.0)
        : origin_(origin), theta_(theta)
      {}

      //!Copy constructor.
      TimeView(const TimeView& other)
        : origin_(other.origin_), theta_(other.theta_)
      {}

      //!Return the relative point in time, withe respect to the current time step size.
      double theta() const     { return theta_; }

      //!Return the absolute point in time.
      double time() const      { return origin_.time() + theta() * deltaT(); }

      //!Return the current time step size.
      double deltaT() const    { return origin_.deltaT(); }

      //!Return the absolute start point of the current time interval.
      double startTime() const { return origin_.time(); }

      //!Return the absolute end point of the current time interval.
      double endTime() const   { return startTime() + deltaT(); }

      //!Redefine the current view to look at another point in the current time interval.
      void setTheta(const double theta) {
        assert(theta >= 0.0 && theta <= 1.0);
        theta_ = theta;
      }

      //!Return the time-provider we are linked to
      const TimeProviderType& timeProvider() const
      {
        return origin_;
      }

      //!Return the time-provider we are linked to
      const TimeProviderType& timeProvider()
      {
        return origin_;
      }

     private:
      const TimeProviderType& origin_;
      double theta_;
    };

    //@} PDE-Models

  } // namespace ACFem

} // namespace Dune

#endif
