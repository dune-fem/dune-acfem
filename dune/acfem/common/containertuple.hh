#ifndef __DUNE_ACFEM_COMMON_TUPLETHINGS_HH__
#define __DUNE_ACFEM_COMMON_TUPLETHINGS_HH__

#include <tuple>
#include <dune/common/fvector.hh>
#include <dune/fem/storage/subvector.hh>

namespace Dune {

  namespace ACFem {

    /**@addtogroup C++-Templates
     *
     * Additional C++ template stuff needed in some places.
     *
     * @warning Chances are very high that things defined here are not
     * needed at all or already have been solved in a much more
     * elegant fashion elsewhere. PLEASE FIXME!!!
     *
     * @{
     */

    /**Create a tuple of values from a tuple of containers. This is
     * very simplistic, we just implement access.
     *
     * Things get complicated when trying to support Dune::Fem
     * specialities like their "SubVector" magic which is very
     * flexible but makes some things a little bit complicated.
     *
     * @param[in] Containers Random access containers. They need to
     * have operator[].
     */
    template<class... Containers>
    class ContainerTuple
    {
      using ContainerTupleType = std::tuple<Containers&...>;
     public:
      enum {
        tupleSize = std::tuple_size<ContainerTupleType>::value
      };
      ContainerTuple(Containers&... containers)
        : containers_(containers...)
      {}

      ContainerTuple(const ContainerTuple& other)
        : containers_(other.containers_)
      {}

      // Just get the type of the const random access operator
      template<class Data, class = void>
      struct ConstValueTraits
      {
        typedef Data type;
      };

      // Just get the type of the random access operator
      template<class Data, class = void>
      struct ValueTraits
      {
        typedef Data& type;
      };

      // For one-level Fem::SubVector stuff the following might do it
      template<class Base, class Mapper>
      struct ConstValueTraits<Fem::SubVector<Base, Mapper>, void>
      {
       private:
        typedef Fem::SubVector<Base, Mapper> DataType;
        typedef typename DenseMatVecTraits<DataType>::value_type ValueType;
        static const size_t size = Mapper::size();
       public:
        typedef FieldVector<ValueType, size> type;
      };

      // For one-level Fem::SubVector stuff the following might do
      // it. The subvector already stores references, so we must not
      // store a reference to a temporary containing references, but a
      // copy of the sub-vector.
      template<class Base, class Mapper>
      struct ValueTraits<Fem::SubVector<Base, Mapper>, void>
      {
        typedef Fem::SubVector<Base, Mapper> type;
      };

      typedef
      std::tuple<
        typename ValueTraits<
          std::decay_t<
            decltype(std::declval<Containers>()[0])> >::type...> ValueType;

      typedef
      std::tuple<
        typename ConstValueTraits<
          std::decay_t<
            decltype(std::declval<const Containers>()[0])> >::type...> ConstValueType;

     private:
      // helper dingsbums. This is just here to support Dune::Fem
      // SubVector for ApaptiveDiscreteFunction. Oh well. Why?
      template<class Tuple, size_t... Is>
      auto mutableValueTuple(size_t idx, const Tuple& tuple, std::index_sequence<Is...>)
      {
        return ValueType(std::get<Is>(tuple)[idx]...);
      }
      template<class Tuple, size_t... Is>
      auto valueTuple(size_t idx, const Tuple& tuple, std::index_sequence<Is...>) const
      {
        return std::make_tuple(std::get<Is>(tuple)[idx]...);
      }
     public:
      ValueType operator[](size_t idx)
      {
        return mutableValueTuple(idx, containers_, std::make_index_sequence<tupleSize>{});
      }
      // give a damn ...
      const ConstValueType operator[](size_t idx) const
      {
        return valueTuple(idx, containers_, std::make_index_sequence<tupleSize>{});
      }
     private:
      ContainerTupleType containers_;
    };

    //!@} C++-Templates

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_COMMON_TUPLETHINGS_HH__
