#ifndef __DUNE_ACFEM_COMMON_LITERALS_HH__
#define __DUNE_ACFEM_COMMON_LITERALS_HH__

#include "types.hh"
#include "../mpl/accumulate.hh"
#include "../mpl/sequenceslice.hh"
#include "../mpl/compare.hh"
#include "fractionconstant.hh"

namespace Dune {

  namespace ACFem {

    namespace Literals {

      /**@addtogroup C++-Templates
       *
       * @{
       */

      namespace {

        template<class Seq, class SFINAE = void>
        struct IntParserHelper;

        /**@internal Parse decimal numbers. A number is decimal if it
         * starts with one numerical digit which is either not 0 or
         * the only digit in the string.
         */
        template<char c0, char... c>
        struct IntParserHelper<Sequence<char, c0, c...>,
                               std::enable_if_t<c0 != '0' || sizeof...(c) == 0> >
        {
          using Digits = IndexSequence<(c0 - '0'), (c-'0')...>;
          static constexpr std::size_t base = 10;
          static_assert(Digits{} <= MakeConstantSequence<sizeof...(c)+1, 9>{},
                        "Expecting a sequence of non-negative decimal digits");
        };

        /**@internal Parse hex numbers starting with 0x or 0X.*/
        template<char c2, char... c>
        struct IntParserHelper<Sequence<char, '0', c2, c...>, std::enable_if_t<c2 == 'x' || c2 == 'X'> >
        {
          using Digits = IndexSequence<((c >= '0' && c <= '9')
                                        ? (c - '0')
                                        : ((c >= 'a' && c <= 'f')
                                           ? (10 + c - 'a')
                                           : ((c >= 'A' && c <= 'F')
                                              ? (10 + c - 'A')
                                              : ~0UL)))...>;
          static constexpr std::size_t base = 16;
          static_assert(Digits{} <= MakeConstantSequence<sizeof...(c), 15>{},
                        "Expecting a sequence of non-negative binary digits");
        };

        /**@internal Parse binary numbers starting with 0b or 0B.*/
        template<char c2, char... c>
        struct IntParserHelper<Sequence<char, '0', c2, c...>, std::enable_if_t<c2 == 'b' || c2 == 'B'> >
        {
          using Digits = IndexSequence<(c-'0')...>;
          static constexpr std::size_t base = 2;
          static_assert(Digits{} <= MakeConstantSequence<sizeof...(c), 1>{},
                        "Expecting a sequence of non-negative binary digits");
        };

        /**@internal Parse octal numbers starting with 0.*/
        template<char c2, char... c>
        struct IntParserHelper<
          Sequence<char, '0', c2, c...>,
          std::enable_if_t<c2 != 'x' && c2 != 'X' && c2 != 'b' && c2 != 'B'>
          >
        {
          using Digits = IndexSequence<(c2-'0'), (c-'0')...>;
          static constexpr std::size_t base = 8;
          static_assert(Digits{} <= MakeConstantSequence<sizeof...(c)+1, 7>{},
                        "Expecting a sequence of non-negative octal digits");
        };
      }

      namespace {
        template<char... c>
        constexpr auto integerLiteral()
        {
          using Traits = IntParserHelper<Sequence<char, c...> >;
          using Digits = typename Traits::Digits;
          constexpr std::size_t base = Traits::base;
          return AccumulateSequence<MultiplyAddFunctor<std::size_t, base>, ReverseSequence<Digits> >{};
        }
      }

      /**Parse a string literal into an integral constant.*/
      template<char... c>
      constexpr auto operator"" _c ()
      {
        return integerLiteral<c...>();
      }

      /**Parse a string literal into a FractionConstant with
       * denominator 1.
       */
      template<char... c>
      constexpr auto operator"" _f ()
      {
        return TypedValue::intFraction(integerLiteral<c...>());
      }

      //@} C++-Templates

    } // NS Literals

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_COMMON_LITERALS_HH__
