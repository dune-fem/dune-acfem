#ifndef __DUNE_ACFEM_COMMON_FUNCTIONSPACENORM_HH__
#define __DUNE_ACFEM_COMMON_FUNCTIONSPACENORM_HH__

namespace Dune::ACFem {

  enum FunctionSpaceNorm {
    L2Norm, H1Norm
  };

}

#endif // __DUNE_ACFEM_COMMON_FUNCTIONSPACENORM_HH__
