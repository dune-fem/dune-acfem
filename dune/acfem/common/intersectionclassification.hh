#ifndef __DUNE_ACFEM_COMMON_INTERSECTIONCLASSIFICATION_HH__
#define __DUNE_ACFEM_COMMON_INTERSECTIONCLASSIFICATION_HH__

#include <dune/grid/common/gridenums.hh>

namespace Dune
{
  namespace ACFem
  {

    /**@addtogroup Convenience
     * @{
     */

    //!Return true if at the global domain boundary
    template<class Intersection>
    bool isDomainBoundary(const Intersection& intersection)
    {
      return !intersection.neighbor() && intersection.boundary();
    }

    /**Return true if at a periodic boundary. Note that isInterior()
     * will then also return true.
     */
    template<class Intersection>
    bool isPeriodicBoundary(const Intersection& intersection)
    {
      return intersection.neighbor() && intersection.boundary();
    }

    /**Retrun true if at the border to another computing note. Note
     * that isInterior() may or may not return true int his case.
     */
    template<class Intersection>
    bool isProcessorBoundary(const Intersection& intersection)
    {
      if (intersection.neighbor()) {
        return intersection.outside().partitionType() == GhostEntity;
      } else {
        return !intersection.boundary();
      }
    }

    /**Return true if there is a neighbor. Note that this function
     * will return false if there is a neighbor on another computing
     * node, but ghost-cells are unavailable.
     */
    template<class Intersection>
    bool isInterior(const Intersection& intersection)
    {
      return intersection.neighbor();
    }

    //@} Convenience

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_COMMON_INTERSECTIONCLASSIFICATION_HH__
