#ifndef __DUNE_ACFEM_COMMON_CSVWRITER_HH__
#define __DUNE_ACFEM_COMMON_CSVWRITER_HH__

#include <iostream>
#include <iomanip>

namespace Dune::ACFem {

  template<typename... T>
  class CSVWriter {
   public:

    // simple constructor
    CSVWriter(std::ostream& stream = std::cout) : stream_(stream) {}

    // constructor writing header line
    template<typename... F>
    CSVWriter(std::ostream& stream, F&&... fieldname) : stream_(stream) {
      static_assert(sizeof...(F) == sizeof...(T), "number of fieldnames doesn't match number of fieldtype arguments");
      static_assert((std::is_constructible_v<std::string, F> && ...), "fieldnames need to be strings");

      stream_ << std::left;
      write_(fieldname...);
    }

    // write row
    template<typename... V>
    void write(V&&... fieldvalue) {
      static_assert((std::is_constructible_v<T, V> && ...), "fieldvalues don't match declared fieldtypes");

      stream_ << std::setprecision(fpPrecision) << std::scientific << std::right;
      write_(fieldvalue...);
    }

    void setprecision(int n) { fpPrecision = n; }
    void setw(int n) { fieldWidth = n; }

   protected:

    template<typename... V>
    void write_(V&&... fieldvalue) {
      ((stream_ << std::setw(fieldWidth) << fieldvalue << ","), ...);
      stream_ << std::endl;
    }

   private:
    std::ostream& stream_;
    int fpPrecision = 5;
    int fieldWidth = 12;
  };

} // Dune::ACFem

#endif // __DUNE_ACFEM_COMMON_CSVWRITER_HH__
