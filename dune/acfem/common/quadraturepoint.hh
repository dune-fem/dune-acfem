#ifndef __DUNE_ACFEM_COMMON_QUADRATUREPOINT_HH__
#define __DUNE_ACFEM_COMMON_QUADRATUREPOINT_HH__

#include <dune/fem/quadrature/quadrature.hh>

#ifndef __DUNE_ACFEM_MAKE_CHECK__
# define __DUNE_ACFEM_MAKE_CHECK__ 0
#endif

#include "types.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup Quadrature
     *
     * Quadrature related traits.
     */

    /**Shortcut.*/
    template<class Quadrature>
    using QuadraturePoint = Fem::QuadraturePointWrapper<Quadrature>;

    /**@internal Default FalseType.*/
    template<class T, class SFINAE = void>
    struct IsQuadraturePoint
      : FalseType
    {};

    /**@internal Forward to decay type.*/
    template<class T>
    struct IsQuadraturePoint<T, std::enable_if_t<!IsDecay<T>{}> >
      : IsQuadraturePoint<std::decay_t<T> >
    {};

    /**Identify quadrature points.*/
    template<class Quadrature>
    struct IsQuadraturePoint<Fem::QuadraturePointWrapper<Quadrature> >
      : TrueType
    {};

    /**A wrapper class which allows to "simulate" a
     * Dune::Fem::QuadraturePointWrapper. The idea is to get rid of
     * "catch-all" template arguments for the quadrature points and
     * instead require a template-template parameter.
     *
     * FIXME: to get this right one probably needs local-global
     * conversion at some point.
     */
    template<class T>
    class PointWrapperQuadrature
      : public T
    {
      typedef T DomainType;
      typedef T BaseType;
     public:
      PointWrapperQuadrature(const DomainType& p) : BaseType(p) {}
      typedef DomainType CoordinateType;
      typedef DomainType LocalCoordinateType;
      typedef real_t<CoordinateType> RealType;
      const CoordinateType& point(unsigned int index) const { assert(index == 0); return *this; }
      const RealType weight(unsigned int) const { return RealType(1.); }
      const LocalCoordinateType& localPoint(unsigned int index) const { assert(index == 0); return *this; }
      unsigned int nop() const { return 1; }
      operator const T&() const { return *this; }
      operator T&() { return *this; }
    };

    template<class T>
    using PlaceholderQuadraturePoint = QuadraturePoint<PointWrapperQuadrature<T> >;

    template<class T, int N>
    auto quadraturePoint(const FieldVector<T, N>& t)
    {
      typedef PointWrapperQuadrature<FieldVector<T, N> > QuadratureType;
      return QuadraturePoint<QuadratureType>(static_cast<const QuadratureType&>(t), 0);
    }

    template<class Quadrature>
    decltype(auto) quadraturePoint(const QuadraturePoint<Quadrature>& qp)
    {
      return qp;
    }

    template<class T, int N>
    auto operator*(const FieldVector<T, N>& t)
    {
      return quadraturePoint(t);
    }

    /**Forward Fem::QuadraturePointWrapper as is. */
    template<class Quadrature>
    decltype(auto) operator*(const QuadraturePoint<Quadrature>& qp)
    {
      return qp;
    }

    template<class T>
    std::ostream& operator<<(std::ostream& out, const QuadraturePoint<PointWrapperQuadrature<T> >& arg)
    {
      return out << (const T&)arg;
    };

    //!@} Quadrature

  } // NS ACFem::

} // NS Dune::

#endif // __DUNE_ACFEM_COMMON_QUADRATUREPOINT_HH__
