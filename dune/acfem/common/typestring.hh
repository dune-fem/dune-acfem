#ifndef __DUNE_ACFEM_COMMON_TYPESTRING_HH__
#define __DUNE_ACFEM_COMMON_TYPESTRING_HH__

#if HAVE_CONFIG_H
# include <config.h>
#endif

#include <limits>
#include <regex>
#include <iostream>
#if HAVE_CXA_DEMANGLE
# include <typeinfo>
# include <cxxabi.h>
# include <cassert>
#else
# warning Pretty-printing of class-names will not work, C++ demangler not available.
#endif

#include "types.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup Convenience
     * @{
     */

    template<class T>
    std::string cvString(T&& t)
    {
      std::string result;
      if (std::is_lvalue_reference<T>::value) {
        result += "&";
      }
      if (std::is_rvalue_reference<T>::value) {
        result += "&&";
      }
      if (std::is_pointer<T>::value) {
        result += "*";
      }
      if (std::is_const<T>::value) {
        result += "const";
      }
      return result;
    }

    template<class T>
    std::string demangle(T&& t)
    {
#if HAVE_CXA_DEMANGLE
      auto *cname = typeid(std::forward<T>(t)).name();
      int status;
      auto *demangled = abi::__cxa_demangle(cname, 0, 0, &status);
      assert(status == 0);
      auto name = std::string(demangled);
      delete demangled;
#else
      name = std::string("Don't know how to generate pretty type-names with this compiler.");
#endif
      return name;
    }

    /**A class constructing the name of another type. */
    template<class T>
    class TypeString
    {
     public:
      TypeString(const std::decay_t<T>* arg = nullptr, bool isRValueRef = false)
      {
#if HAVE_CXA_DEMANGLE
        auto *name = typeid(arg).name();
        int status;
        auto *demangled = abi::__cxa_demangle(name, 0, 0, &status);
        if (status != 0) {
          std::clog << "*** Unable to demangle symbol(" << status << ")" << std::endl
                    << name << std::endl
                    << "***" << std::endl;
        }
        assert(status == 0);
        name_ = std::string(demangled);
        delete demangled;
        name_ = name_.substr(0, name_.size() - strlen(" const*"));
        if (std::is_rvalue_reference<T>::value) {
          name_ += "&&";
        } else if (std::is_lvalue_reference<T>::value) {
          name_ += "&";
        }
        if (std::is_const<std::remove_reference_t<T> >::value) {
          name_ += " const";
        }
#else
        name_ = std::string("Don't know how to generate pretty type-names with this compiler.");
#endif
      }
      TypeString(T&& arg)
        : TypeString(&arg, std::is_rvalue_reference<decltype(arg)>::value)
      {}

      /**If implemented for the current compiler return the name of
       * the given type.
       *
       * @param[in] stripNameSpaces If @a true strip Dune::, ACFem::
       * and Fem:: name-spaces from the type-name.
       */
      std::string name(bool postProcess = true) const
      {
        if (postProcess) {
          auto result = name_;

          // strip name space and SFINAE dummy parameters
          result = std::regex_replace(result, std::regex("ACFem::|Dune::|Fem::|Expressions::|MPL::|FunctionExpressions::|GridFunction::|std::|Tensor::|TypedValue::|PDEModel::|ModelInstrospection::|[(]anonymous namespace[)]::|impl::|__[0-9]+::|(>)(,( )void)+(>)|, void"), "$1$3$4");

          // also strip suffixes indicating the integer type.
          result = std::regex_replace(result, std::regex("([0-9]+)[uU]?[lL]?"), "$1");

          // short cut for canonical ALU leaf grid
          result = std::regex_replace(result, std::regex("AdaptiveLeafGridPart<(ALUGrid)<([0-9]), ([0-9]), [(]ALUGridElementType[)]0, [(]ALUGridRefinementType[)]0, ALUGridMPIComm>, [(]PartitionIteratorType[)][0-9]+, (false|true)>"), "$1<$2,$3>");

          // Short cut for  lagrange space. Omit grid and storage and just keep degree and dimensions.
          result = std::regex_replace(result,
                                      std::regex("LagrangeDiscreteFunctionSpace<FunctionSpace<double, double, ([0-9]+), ([0-9]+)>,.+?, ([0-9]+), CachingStorage>"),
                                      "LagrangeSpace<$3, <$1,$2> >");

          // return the entire mess
          return result;
        } else {
          return name_;
        }
      }
     protected:
      std::string name_;
    };

    /**Generator for TypeString.
     *
     * @param[in] t An rvalue-reference of Type T. Only its type is
     * used.
     *
     * @param[in] stripNameSpaces Strip Dune::, ACFem:: and Fem::
     * namespace prefixes.
     */
    template<class T>
    auto typeString(T&& t, bool stripNameSpaces = true)
    {
      return TypeString<T>(std::forward<T>(t)).name(stripNameSpaces);
    }

    template<class T>
    auto typeString(bool stripNameSpaces = true, const std::remove_reference_t<T>* arg = nullptr)
    {
      return TypeString<T>{}.name(stripNameSpaces);
    }

    //!@} Convenience

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_COMMON_TYPESTRING_HH__
