#ifndef __DUNE_ACFEM_COMMON_COMPILER_HH__
#define __DUNE_ACFEM_COMMON_COMPILER_HH__

#if !defined(__clang__) && defined(__GNUC__)
# define DUNE_ACFEM_IS_GCC(MIN_MAJOR, MAX_MAJOR) (__GNUC__ >= MIN_MAJOR && __GNUC__ <= MAX_MAJOR)
#else
# define DUNE_ACFEM_IS_GCC(one, two) false
#endif

#if __clang__
# define DUNE_ACFEM_IS_CLANG(MIN_MAJOR, MAX_MAJOR) (__clang_major__ >= MIN_MAJOR && __clang_major__ <= MAX_MAJOR)
#else
# define DUNE_ACFEM_IS_CLANG(one, two) false
#endif

#if DUNE_ACFEM_IS_CLANG(0, 10)
# define CXX17_P0522R0(...) class...
#else
# if DUNE_ACFEM_IS_CLANG(0, 99)
#  warning This construct was known to fail for clang <= V10
# endif
# define CXX17_P0522R0(...) __VA_ARGS__
#endif

#endif // __DUNE_ACFEM_COMMON_COMOILER_HH__
