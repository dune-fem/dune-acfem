#ifndef __DUNE_ACFEM_COMMON_FUNCTIONAL_HH__
#define __DUNE_ACFEM_COMMON_FUNCTIONAL_HH__

#include <tuple>

namespace Dune {

  namespace ACFem {

    /**@addtogroup C++-Templates
     *
     * @{
     */

    /**Apply a method through a member function pointer. This will
     * unpack the arguments in t according to I and pass them to
     * (o.*f)().
     */
    template <class F, class O, class Tuple, std::size_t... I>
    constexpr decltype(auto) applyMethod(F&& f, O&& o, Tuple&& t, std::index_sequence<I...>)
    {
      return (o.*f)(std::get<I>(std::forward<Tuple>(t))...);
    }

    /**Apply a method through a member function pointer. This will
     * unpack the arguments in t and pass them to (o.*f)().
     */
    template <class F, class O, class Tuple>
    constexpr decltype(auto) applyMethod(F&& f, O&& o, Tuple&& t)
    {
      return applyMethod(
        std::forward<F>(f),
        std::forward<O>(o),
        std::forward<Tuple>(t),
        std::make_index_sequence<std::tuple_size<std::decay_t<Tuple> >::value>{});
    }

    //!@} C++-Templates

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_COMMON_FUNCTIONAL_HH__
