#ifndef _DUNE_ACFEM_NAMESPACE_HH_
#define _DUNE_ACFEM_NAMESPACE_HH_

namespace Dune {

  /**A namespace encapsulating everything defined in our dune-acfem
   * project. dune-acfem extracts and refines the fem-poisson and
   * fem-heat examples from the dune-fem-school-generator and factors
   * out general functionality. It serves as backend for
   * single-purpose standalone dune-modules. Examples are the
   * ellipt-dot-c and heat-dot-c modules which more or less do the
   * same thing as the ALBERTA examples with the same name.
   *
   * The major difference to the school code is the idea to factor out
   * commonly needed functionality into a Dune::Fem addon-library
   * instead of copying files around.
   */
  namespace ACFem {

    // empty, this files exists in order to define a description of ACFem for Doxygen.

  } // ACFem::

} // Dune::

#endif // _DUNE_ACFEM_NAMESPACE_HH_

