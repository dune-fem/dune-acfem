#ifndef __DUNE_ACFEM_MODELES_TIMEPROVIDERTRAITS_HH__
#define __DUNE_ACFEM_MODELES_TIMEPROVIDERTRAITS_HH__

#include <type_traits>

namespace Dune {

  namespace ACFem {

    /**@addtogroup PDE-Models
     * @{
     */

    /**Type of time and time-step values.*/
    template<class TimeProvider>
    struct TimeProviderTraits
    {
      using TimeStepType = std::decay_t<decltype(std::declval<TimeProvider>().deltaT())>;
      using TimeType = std::decay_t<decltype(std::declval<TimeProvider>().time())>;
    };

    //! @} PDE-Models

  } // ACFem

} // Dune

#endif // __DUNE_ACFEM_MODELES_TIMEPROVIDERTRAITS_HH__
