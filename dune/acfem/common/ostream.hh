#ifndef __DUNE_ACFEM_COMMON_OSTREAM_HH__
#define __DUNE_ACFEM_COMMON_OSTREAM_HH__

#if HAVE_CONFIG_H
# include <config.h>
#endif

#include <limits>
#include <iostream>
#include <iomanip>
#include <bitset>
#include <utility>
#include <dune/common/hybridutilities.hh>
#include <dune/common/streamoperators.hh> // << for tuples etc.

#include "types.hh"
#include "tostring.hh"
#include "typestring.hh"
#include "../mpl/mpl.hh"
#include "findbit.hh"
#include "fractionconstant.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup Convenience
     * @{
     */

    template<class T, std::size_t Size = std::numeric_limits<T>::digits>
    auto makeBitset(const T& t, const IndexConstant<Size>& = IndexConstant<Size>{})
    {
      return std::bitset<Size>(t);
    }

    /**Output operator for TypePrint tag-structure.*/
    template<class T>
    std::ostream& operator<<(std::ostream& out, TypeString<T>&& t)
    {
      return out << t.name(true);
    }

    /**Print something which shows the type that was passed to the
     * function.
     *
     * @param arg Dummy, unused.
     *
     * @param[in] out ostream object
     *
     * @param[in] newLine Print a new line after printing the type.
     */
    template<class T>
    void prettyPrint(T&& arg, std::ostream& out = std::cout, const bool newLine = true)
    {
      out << typeString(arg);
      if (newLine) {
        out << std::endl;
      }
    }

    // provided in dune/common/streamoperators.hh
    /* *Output operator for tuple-like objects. Should probably support
     * everthing which is accepted by std::get.
     *
     * @param[in,out] out stream to print to.
     *
     * @param[in] tuple Tuple-like object.
     */
    template<class T, std::enable_if_t<(IsTupleLike<T>::value
                                        && !IsTuple<T>::value // dune-common
                                        && !IsArray<T>::value // dune-common
      ), int> = 0>
    std::ostream& operator<<(std::ostream& out, const T& t)
    {
      out << (IsArray<T>::value ? "[" : "(");
      forLoop<size<T>()>([&] (auto i) {
          using I = decltype(i);
          out << std::get<I::value>(t);
          if (I::value < size<T>()-1) {
            out << ", ";
          }
        });
      out << (IsArray<T>::value ? "]" : ")");
      return out;
    }

    /**Print sequence.*/
    template<class Int, Int I0, Int... I>
    std::ostream& operator<<(std::ostream& out, Sequence<Int, I0, I...>)
    {
      return ((out << "<" << I0) << ... << ("," + toString(I))) << ">";
    }

    template<class Int>
    std::ostream& operator<<(std::ostream& out, Sequence<Int>)
    {
      return out << "<>";
    }

    /**Print bool-sequence.*/
    template<bool I0, bool... I>
    std::ostream& operator<<(std::ostream& out, BoolSequence<I0, I...>)
    {
      return ((out << (I0 ? "true" : "false"))  << ... << (I ? " true" : " false"));
    }

    static inline std::ostream& operator<<(std::ostream& out, BoolSequence<>)
    {
      return out;
    }

    using Dune::operator<<;
    using Dune::ACFem::operator<<;

    //!@} Convenience

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_COMMON_OSTREAM_HH__
