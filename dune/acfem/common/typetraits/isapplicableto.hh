#ifndef __DUNE_ACFEM_COMMON_ISAPPLICABLETO_HH__
#define __DUNE_ACFEM_COMMON_ISAPPLICABLETO_HH__

#include "../types.hh"
#include "../../mpl/typetuple.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup C++-Templates
     *
     * @{
     */

    /**@addtogroup TypeTraits
     *
     * @{
     */

    /**Is TrueType if an F can be invoked with an E. FIXME: there
     * might be std functionality for this.
     */
    template<class E, class F>
    struct IsApplicableTo
      : std::is_invocable<F, E>
    {};

    template<class... E, class F>
    struct IsApplicableTo<MPL::TypeTuple<E...>, F>
      : std::is_invocable<F, E...>
    {};

    //!@} TypeTraits

    //!@} C++-Templates

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_COMMON_ISAPPLICABLETO_HH__
