#ifndef __DUNE_ACFEM_COMMON_REMOVERVALUEREFERENCE_HH__
#define __DUNE_ACFEM_COMMON_REMOVERVALUEREFERENCE_HH__

#include "../types.hh"
#include "../../mpl/typetuple.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup C++-Templates
     *
     * @{
     */

    /**@addtogroup TypeTraits
     *
     * @{
     */

    template<class T>
    struct RemoveRValueReference
    {
      using Type = T;
    };

    template<class T>
    struct RemoveRValueReference<T&&>
    {
      using Type = T;
    };

    /**If T is an rvalue reference then yield the dereferenced
     * type. If T is not an rvalue-reference then simply yield
     * T. lvalue references are kept as is.
     */
    template<class T>
    using RemoveRValueReferenceType = typename RemoveRValueReference<T>::Type;

    //!@} TypeTraits

    //!@} C++-Templates

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_COMMON_REMOVERVALUEREFERENCE_HH__
