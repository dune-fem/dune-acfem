#ifndef __DUNE_ACFEM_COMMON_NAMED_CONSTANT_HH__
#define __DUNE_ACFEM_COMMON_NAMED_CONSTANT_HH__

#include "types.hh"
#include "tostring.hh"

namespace Dune
{

  namespace ACFem
  {

    namespace TypedValue
    {

      /**A named constant wraps a constant of the given type T tagging
       * it with the given character sequence. Specializations should
       * provide a type-cast operator to a constant value of type T.
       */
      template<class T, char... Name>
      struct NamedConstant;

      template<class T>
      struct IsNamedConstant
        : FalseType
      {};

      template<class T>
      struct IsNamedConstant<T&>
        : IsNamedConstant<std::decay_t<T> >
      {};

      template<class T>
      struct IsNamedConstant<T&&>
        : IsNamedConstant<std::decay_t<T> >
      {};

      template<class T, char... Name>
      struct IsNamedConstant<NamedConstant<T, Name...> >
        : TrueType
      {};

      template<class T, char... Name>
      std::string toString(NamedConstant<T, Name...>)
      {
        using Type = NamedConstant<T, Name...>;
        const std::string name({ Name... });
        return name+":"+ACFem::toString(Type::value);
      }

      /** e form glibc implementation. */
      template<class T>
      struct NamedConstant<T, 'E'>
      {
        static_assert(sizeof(T) < sizeof(0.0L), "Unsupported floating point precision.");
        static constexpr T value = (T)2.718281828459045235360287471352662498L;
        constexpr T operator()() const
        {
          return value;
        }
        constexpr operator T() const
        {
          return value;
        }
      };

      /** log_2 e form glibc implementation. */
      template<class T>
      struct NamedConstant<T, 'L', 'O', 'G', '2', 'E'>
      {
        static_assert(sizeof(T) < sizeof(0.0L), "Unsupported floating point precision.");
        static constexpr T value = (T)1.442695040888963407359924681001892137L;
        constexpr T operator()() const
        {
          return value;
        }
        constexpr operator T() const
        {
          return value;
        }
      };

      /** log_10 e form glibc implementation. */
      template<class T>
      struct NamedConstant<T, 'L', 'O', 'G', '1', '0', 'E'>
      {
        static_assert(sizeof(T) < sizeof(0.0L), "Unsupported floating point precision.");
        static constexpr T value = (T)0.434294481903251827651128918916605082L;
        constexpr T operator()() const
        {
          return value;
        }
        constexpr operator T() const
        {
          return value;
        }
      };

      /** log_e 2 form glibc implementation. */
      template<class T>
      struct NamedConstant<T, 'L', 'N', '2'>
      {
        static_assert(sizeof(T) < sizeof(0.0L), "Unsupported floating point precision.");
        static constexpr T value = (T)0.693147180559945309417232121458176568L;
        constexpr T operator()() const
        {
          return value;
        }
        constexpr operator T() const
        {
          return value;
        }
      };

      /** log_e 10 form glibc implementation. */
      template<class T>
      struct NamedConstant<T, 'L', 'N', '1', '0'>
      {
        static_assert(sizeof(T) < sizeof(0.0L), "Unsupported floating point precision.");
        static constexpr T value = (T)2.302585092994045684017991454684364208L;
        constexpr T operator()() const
        {
          return value;
        }
        constexpr operator T() const
        {
          return value;
        }
      };

      /** pi form glibc implementation. */
      template<class T>
      struct NamedConstant<T, 'P', 'I'>
      {
        static_assert(sizeof(T) < sizeof(0.0L), "Unsupported floating point precision.");
        static constexpr T value = (T)3.141592653589793238462643383279502884L;
        constexpr T operator()() const
        {
          return value;
        }
        constexpr operator T() const
        {
          return value;
        }
      };

      /** pi/2 form glibc implementation. */
      template<class T>
      struct NamedConstant<T, 'P', 'I', '_', '2'>
      {
        static_assert(sizeof(T) < sizeof(0.0L), "Unsupported floating point precision.");
        static constexpr T value = (T)1.570796326794896619231321691639751442L;
        constexpr T operator()() const
        {
          return value;
        }
        constexpr operator T() const
        {
          return value;
        }
      };

      /** pi/4 form glibc implementation. */
      template<class T>
      struct NamedConstant<T, 'P', 'I', '_', '4'>
      {
        static_assert(sizeof(T) < sizeof(0.0L), "Unsupported floating point precision.");
        static constexpr T value = (T)0.785398163397448309615660845819875721L;
        constexpr T operator()() const
        {
          return value;
        }
        constexpr operator T() const
        {
          return value;
        }
      };

      /** 1/pi form glibc implementation. */
      template<class T>
      struct NamedConstant<T, '1', '_', 'P', 'I'>
      {
        static_assert(sizeof(T) < sizeof(0.0L), "Unsupported floating point precision.");
        static constexpr T value = (T)0.318309886183790671537767526745028724L;
        constexpr T operator()() const
        {
          return value;
        }
        constexpr operator T() const
        {
          return value;
        }
      };

      /** 2/pi form glibc implementation. */
      template<class T>
      struct NamedConstant<T, '2', '_', 'P', 'I'>
      {
        static_assert(sizeof(T) < sizeof(0.0L), "Unsupported floating point precision.");
        static constexpr T value = (T)0.636619772367581343075535053490057448L;
        constexpr T operator()() const
        {
          return value;
        }
        constexpr operator T() const
        {
          return value;
        }
      };

      /** 2/sqrt(pi) form glibc implementation. */
      template<class T>
      struct NamedConstant<T, '2', '_', 'S', 'Q', 'R', 'T', 'P', 'I'>
      {
        static_assert(sizeof(T) < sizeof(0.0L), "Unsupported floating point precision.");
        static constexpr T value = (T)1.128379167095512573896158903121545172L;
        constexpr T operator()() const
        {
          return value;
        }
        constexpr operator T() const
        {
          return value;
        }
      };

      /** sqrt(2) form glibc implementation. */
      template<class T>
      struct NamedConstant<T, 'S', 'Q', 'R', 'T', '2'>
      {
        static_assert(sizeof(T) < sizeof(0.0L), "Unsupported floating point precision.");
        static constexpr T value = (T)1.414213562373095048801688724209698079L;
        constexpr T operator()() const
        {
          return value;
        }
        constexpr operator T() const
        {
          return value;
        }
      };

      /** 1/sqrt(2) form glibc implementation. */
      template<class T>
      struct NamedConstant<T, 'S', 'Q', 'R', 'T', '1', '_', '2'>
      {
        static_assert(sizeof(T) < sizeof(0.0L), "Unsupported floating point precision.");
        static constexpr T value = (T)0.707106781186547524400844362104849039L;
        constexpr T operator()() const
        {
          return value;
        }
        constexpr operator T() const
        {
          return value;
        }
      };

    } // NS TypedValue

    using TypedValue::IsNamedConstant;

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_COMMON_NAMED_CONSTANT_HH__
