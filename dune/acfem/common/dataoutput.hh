#ifndef __DUNE_ACFEM_DATAOUTPUT_HH__
#define __DUNE_ACFEM_DATAOUTPUT_HH__

// iostream includes
#include <iostream>

/*********************************************************/

// include parameter handling
#include <dune/fem/io/parameter.hh>

// include output
#include <dune/fem/io/file/dataoutput.hh>
#include <dune/fem/io/file/datawriter.hh>

namespace Dune {

  namespace ACFem {

    // DataOutputParameters
    // --------------------

    /**@addtogroup Convenience
     *
     * Utility helper classes.
     */

    /**Potentially overwrite some parameters. We keep this as empty
     * example because of the somewhat fancy inheritance structure.
     *
     * BTW, the funny looking LocalParamter<> template is there to
     * implement a move-constructor idiom. Second parameter needs to
     * be the "implementation".
     */
    class DataOutputParameters
      : public Dune::Fem::LocalParameter<Dune::Fem::DataOutputParameters, DataOutputParameters>
    {
     public:
    };

    template<class GridImp, class DataImp>
    class DataOutput
      : public Dune::Fem::DataOutput<GridImp, DataImp>
    {
      typedef  Dune::Fem::DataOutput<GridImp, DataImp> BaseType;
     public:
      DataOutput(const typename BaseType::GridType &grid,
                 typename BaseType::OutPutDataType &data,
                 const Dune::Fem::DataOutputParameters &parameter = Dune::Fem::DataOutputParameters())
        : BaseType(grid, data, parameter)
      {}

      DataOutput (const typename BaseType::GridType &grid,
                  typename BaseType::OutPutDataType &data,
                  const Dune::Fem::TimeProviderBase &tp,
                  const Dune::Fem::DataOutputParameters &parameter = Dune::Fem::DataOutputParameters())
        : BaseType(grid, data, tp, parameter)
      {}

      using BaseType::willWrite;
      using BaseType::write;
      using BaseType::writeData;
      using BaseType::myClassName;
      using BaseType::path;
      using BaseType::consistentSaveStep;

      //! Return the sequence number for the files that would be written if willWrite returns true.
      int writeStep() const {
        return BaseType::writeStep_;
      }


    };

    //!@} Convenience

  } // ACFem::

} // Dune::

#endif // __DATAOUTPUT_PARAMETERS_HH__
