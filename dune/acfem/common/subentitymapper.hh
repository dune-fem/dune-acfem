/**@file
 *
 * This is proof of concept. The code quality is probably poor ...
 */
#ifndef __DUNE_ACFEM_SUBENTITYMAPPER_HH__
#define __DUNE_ACFEM_SUBENTITYMAPPER_HH__

#include <dune/geometry/referenceelements.hh>
#include <dune/grid/common/intersection.hh>
#include <dune/common/version.hh> // DUNE_VERSION_NEWER
#include <array>
#include <unordered_map>
#include "../mpl/foreach.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup Convenience
     * @{
     */

    namespace {

      /**@internal
       *
       * This is a messy helper structure, partly motivated by the
       * fact that (naturally) a function may not defined local
       * template structures.
       */
      template<class DofMapper, int coDimension>
      struct MapEntityHierarchyHelper
      {
        typedef DofMapper DofMapperType;
        typedef typename DofMapperType::GridPartType GridPartType;
        typedef typename GridPartType::ctype FieldType;
        typedef typename DofMapperType::ElementType ElementType;
        typedef typename DofMapperType::GlobalKeyType GlobalKeyType;
        typedef ReferenceElements<FieldType, ElementType::dimension> ReferenceElementsType;
        typedef typename ReferenceElementsType::ReferenceElement ReferenceElementType;

        // Recurse into the entity hierarchy.
        template<int codim>
        struct SubSubEntityFunctor
        {
          enum {
            subCoDimension = codim,
            gridDimension = GridPartType::dimension,
            subSubEntityDimension = gridDimension-subCoDimension
          };
          template<class RefElem, class Functor>
          static void apply(const DofMapperType& mapper, const ElementType& element, const RefElem& refElem, int subIndex, Functor f,
                            const std::unordered_map<GlobalKeyType, unsigned>& globalToLocal)
          {
            const int numSubSubEntities = refElem.size(subIndex, coDimension, subCoDimension);

            for (int i = 0; i < numSubSubEntities; ++i) {
              const unsigned subSubEntityIndex = refElem.subEntity(subIndex, coDimension, i, subCoDimension);
              const auto subSubEntity = element.template subEntity<subCoDimension>(subSubEntityIndex);

              mapper.mapEachEntityDof(subSubEntity, [f, globalToLocal] (unsigned localIndex, const GlobalKeyType& globalIndex) {
                  f(globalToLocal.at(globalIndex), globalIndex);
                });
            }
          }
        };

      };

    }

    /**Iterate over the entire sub-entity hierarchy below one given
     * sub-entity (including the given one) and call the dof-mapper on
     * each sub-entity in turn.
     *
     * @param[in] mapper The block-mapper in use.
     *
     * @param[in] element The bulk (i.e. co-dimension 0) element.
     *
     * @param[in] subIndex The index of the sub-element where the
     * mapping shall occur.
     *
     * @param[in] dummy Co-dimension dummy in order to enable
     * arguemnt-dependent lookup (C++ compiler).
     *
     * @param[in] f Functor or generally: callable object. Lambdas
     * will do. The calling convention is not fixed in the type, but
     * we expect a callable with signature f(localDof, globalDof).
     *
     * @note Side-effects as implemented by @a f.
     */
    template<class DofMapper, int coDimension, class Functor>
    void mapEntityHierarchy(const DofMapper& mapper,
                            const typename DofMapper::ElementType& element,
                            int subIndex,
                            const std::integral_constant<int, coDimension>& dummy,
                            Functor f)
    {
      typedef DofMapper DofMapperType;
      typedef MapEntityHierarchyHelper<DofMapperType, coDimension> HelperType;
      typedef typename HelperType::GridPartType GridPartType;
      typedef typename HelperType::GlobalKeyType GlobalKeyType;
      typedef typename HelperType::ReferenceElementsType ReferenceElementsType;

      enum { dimension = GridPartType::dimension };

      // first construct a reverse lookup for the global keys
      std::unordered_map<GlobalKeyType, unsigned> globalToLocal;
      mapper.mapEach(element, [&globalToLocal] (unsigned local, const GlobalKeyType& global) {
          globalToLocal[global] = local;
        });

      // get codim-0 reference element
      const auto& refElem = ReferenceElementsType::general(element.type());

      // Use ForLoop in order to generate a constant co-dimension as
      // we need element.template subEntity<cdim>
      forLoop<dimension-coDimension+1>([&](auto i){
          HelperType::template SubSubEntityFunctor<i.value+coDimension>::apply(mapper, element, refElem, subIndex, f, globalToLocal);
        });
    }

    /**Iterate over the codim-1 sub-entity hierarchy linked to the
     * given intersection and call the dof-mapper on each sub-entity
     * in turn.
     *
     * @param[in] mapper The DoF-mapper to use, for example space_.blockMapper().
     *
     * @param[in] intersection The intersection carrying the
     * sub-entity information. The mapper will all DoFs attached to
     * intersection.inside() (and all its sub-entities).
     *
     * @param[in] f A Functor, generally any callable object with
     * operator() will do (lambda will do).
     *
     * @note Also in the non-conforming case all DoFs on the
     * sub-entity are mapped. The point is that the intersection knows
     * the index of the codim-1 sub-entity in the bulk entity and
     * therefore acts as a convenient parameter-transport structure
     * here.
     */
    template<class DofMapper, class Intersection, class Functor>
    void mapEachFaceDof(const DofMapper& mapper,
                        const Intersection& intersection,
                        Functor f)
    {
      mapEntityHierarchy(mapper, intersection.inside(), intersection.indexInInside(),
                         std::integral_constant<int, 1>(), f);
    }

    /**Fetch all global DoFs of the codim-1 entity @a intersection
     * belongs to. This function will construct an array with the
     * global DoFs as well as the local mapping to the local DoFs of
     * the bulk-entity.
     *
     * @param[in] mapper The DoF-mapper to use.
     *
     * @param[in] intersection The intersection carrying the
     * sub-entity information. The mapper will all DoFs attached to
     * intersection.inside() (and all its sub-entities).
     *
     * @param[out] localIndices localIndices[i] is the local
     * DoF-number with respect to the bulk-entities basis-function
     * set.
     *
     * @parma[out] globalIndices The global index corresponding to
     * localIndices[i].
     *
     * @return The number of DoFs attached to the given face.
     *
     */
    template<class DofMapper, class Intersection, class LocalIndices, class GlobalIndices>
    unsigned mapFaceDofs(const DofMapper& mapper, const Intersection& intersection,
                         LocalIndices& localIndices, GlobalIndices& globalIndices)
    {
      typedef typename DofMapper::GlobalKeyType GlobalKeyType;

      unsigned count = 0;
      mapEachFaceDof(mapper, intersection,
                     [&localIndices, &globalIndices, &count] (unsigned localIndex, GlobalKeyType globalIndex) {
                       localIndices[count] = localIndex;
                       globalIndices[count] = globalIndex;
                       ++count;
                     });

      return count;
    }

    /**@}*/

  }

}

#endif  // __DUNE_ACFEM_SUBENTITYMAPPER_HH__
