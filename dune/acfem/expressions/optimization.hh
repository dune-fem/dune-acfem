#ifndef __DUNE_ACFEM_EXPRESSIONS_OPTIMIZATION_HH__
#define __DUNE_ACFEM_EXPRESSIONS_OPTIMIZATION_HH__

#include "storage.hh"
#include "interface.hh"
#include "runtimeequal.hh"
#include "terminal.hh"
#include "transform.hh"
#include "examine.hh"
#include "complexity.hh"
#include "optimizationbase.hh"
#include "optimizesums.hh"
#include "../common/literals.hh"

namespace Dune
{

  namespace ACFem
  {

    namespace Expressions
    {

      /**@addtogroup ExpressionTemplates
       *
       * @{
       *
       */

      /**@addtogroup ExpressionOptimization
       *
       * General optimization patterns.
       *
       * @{
       */

      ///////////////////////////////////////////

      template<class F, class T0, class T1>
      constexpr inline std::size_t IsBinaryZeroV<F, T0, T1, std::enable_if_t<FunctorHas<IsProductOperation, F>::value> > = ZerosSignatureV<T0, T1> != 0;

      template<class F, class T0, class T1>
      constexpr inline std::size_t ReturnMinusFirstV<F, T0, T1, std::enable_if_t<MultiplicationAdmitsScalarsV<F> > > = MinusOnesSignatureV<T0, T1> & 2;

      template<class F, class T0, class T1>
      constexpr inline std::size_t ReturnMinusSecondV<F, T0, T1, std::enable_if_t<MultiplicationAdmitsScalarsV<F> > > = MinusOnesSignatureV<T0, T1> == 1;

      template<class F, class T0, class T1>
      constexpr inline bool ReturnFirstV<F, T0, T1, std::enable_if_t<MultiplicationAdmitsScalarsV<F> > > =
        OnesSignatureV<T0, T1> & 2;

      template<class F, class T0, class T1>
      constexpr inline bool ReturnSecondV<F, T0, T1, std::enable_if_t<MultiplicationAdmitsScalarsV<F> > > =
        OnesSignatureV<T0, T1> == 1;

      template<class F, class T0, class T1, class SFINAE = void>
      constexpr inline std::size_t FactorOutSignsV = 0;

      template<class F, class T0, class T1>
      constexpr inline std::size_t FactorOutSignsV<F, T0, T1, std::enable_if_t<FunctorHas<IsProductOperation, F>::value> > = SignSignatureV<T0, T1>;

      template<class F, class T0, class T1, std::enable_if_t<FactorOutSignsV<F, T0, T1> == 1, int> = 0>
      constexpr decltype(auto) operate(OptimizeNormalize0, F&& f, T0&& t0, T1&& t1)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        DUNE_ACFEM_EXPRESSION_RESULT(
          operate<MinusOperation>(operate(std::forward<F>(f), std::forward<T0>(t0).operand(0_c), std::forward<T1>(t1)))
          , "factor out left sign"
          );
      }

      template<class F, class T0, class T1, std::enable_if_t<FactorOutSignsV<F, T0, T1> == 2, int> = 0>
      constexpr decltype(auto) operate(OptimizeNormalize tag, F&& f, T0&& t0, T1&& t1)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        DUNE_ACFEM_EXPRESSION_RESULT(
          operate<MinusOperation>(operate(std::forward<F>(f), std::forward<T0>(t0), std::forward<T1>(t1).operand(0_c)))
          , "factor out right sign"
          );
      }

      template<class F, class T0, class T1, std::enable_if_t<FactorOutSignsV<F, T0, T1> == 3, int> = 0>
      constexpr decltype(auto) operate(OptimizeNormalize tag, F&& f, T0&& t0, T1&& t1)
      {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          DUNE_ACFEM_EXPRESSION_RESULT(
            operate(std::forward<F>(f), std::forward<T0>(t0).operand(0_c), std::forward<T1>(t1).operand(0_c))
            , "factor out both signs"
            );
      }

      template<
        class F, class T0, class T1,
        std::enable_if_t<(FunctorHas<IsSMultiplyOperation, F>::value
                          && IsSMultiplyExpression<T1>::value
        ), int> = 0>
      constexpr decltype(auto) operate(OptimizeGeneric, F&& f, T0&& t0, T1&& t1)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        DUNE_ACFEM_EXPRESSION_RESULT(
          operate<SMultiplyOperation>(
            multiplyScalars(
              std::forward<T0>(t0),
              std::forward<T1>(t1).operand(0_c)
              ),
            std::forward<T1>(t1).operand(1_c)
            )
          , "merge s-multiplies"
          );
      }

      /**Identify composition with inverse.*/
      template<class F, class T, class SFINAE = void>
      constexpr inline bool IsCompositionWithInverseV = std::is_same<typename std::decay_t<F>::InvertibleOperation, Operation<T> >::value;

#if DUNE_ACFEM_IS_GCC(9, 99) || DUNE_ACFEM_IS_CLANG(0, 10)
      template<class F, class T>
      constexpr inline bool IsCompositionWithInverseV<
        F, T,
        std::enable_if_t<(!IsFunctor<F>::value
        )> > = false;

      template<class F, class T>
      constexpr inline bool IsCompositionWithInverseV<
        F, T,
        std::enable_if_t<IsFunctor<F>::value &&
#else
# if DUNE_ACFEM_IS_CLANG(0, 9)
#  warning This construct was known to fail for clang <= V10
# endif
# if DUNE_ACFEM_IS_GCC(10, 99)
#  warning This construct is known to fail for gcc >= V9
# endif
      template<class F, class T>
      constexpr inline bool IsCompositionWithInverseV<
        F, T,
        std::enable_if_t<
#endif
                         (Arity<T>::value != 1
                          // ???? guard against "self expression"?
                          || FunctorHas<IsIdentityOperation, F>::value
                          || FunctorHas<IsPlusOperation, F>::value
        )> > = false;

      /**optimize @f$f \circ f^{-1}@f$ to no-op.*/
      template<class F, class T, std::enable_if_t<IsCompositionWithInverseV<F, T>, int> = 0>
      constexpr decltype(auto) operate(OptimizeTerminal3, F&& f, T&& t)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        DUNE_ACFEM_EXPRESSION_RESULT(
          (forwardReturnValue<Operand<0, T> >(std::forward<T>(t).operand(0_c)))
          , "composition with inverse"
          );
      }

      /** !1 -> 0. */
      template<class T>
      constexpr inline bool IsUnaryZeroV<F<LogicalNotOperation>, T> = ExpressionTraits<T>::isOne;

      /** !0 -> 1 */
      template<class T>
      constexpr inline bool IsUnaryOneV<F<LogicalNotOperation>, T> = ExpressionTraits<T>::isZero;

      /** (X==1) || Y, X || 0 -> X */
      template<class T0, class T1>
      constexpr inline bool ReturnFirstV<F<LogicalOrOperation>, T0, T1> =
        ZerosSignatureV<T0, T1> & 2 || OnesSignatureV<T0, T1> == 1;

      /** X || (Y==1), 0 || Y -> Y */
      template<class T0, class T1>
      constexpr inline bool ReturnSecondV<F<LogicalOrOperation>, T0, T1> =
        OnesSignatureV<T0, T1> & 2 || ZerosSignatureV<T0, T1> == 1;

      template<class T0, class T1>
      constexpr inline bool ReturnFirstV<F<MinOperation>, T0, T1> = AreRuntimeEqual<T0, T1>::value;

      template<class T0, class T1>
      constexpr inline bool ReturnFirstV<F<MaxOperation>, T0, T1> = AreRuntimeEqual<T0, T1>::value;

      ///////////////////////////////////////////

      //! @} BooleanOperationsOptimization

      //! @} ExpressionOptimization

      //! @} ExpressionOptimizations

    } // Expressions

  } // ACFem

} // Dune

#endif // __DUNE_ACFEM_EXPRESSIONS_OPTIMIZATION_HH__
