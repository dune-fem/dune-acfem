#ifndef __DUNE_ACFEM_EXPRESSIONS_OPERATIONPAIR_HH__
#define __DUNE_ACFEM_EXPRESSIONS_OPERATIONPAIR_HH__

#include "treeoperand.hh"
#include "complexity.hh"

namespace Dune::ACFem::Expressions {

  /**Collect type, sign and the position in an expression of one
   * operand.
   */
  template<class S, class E, class Pos>
  struct TreeData
  {
    using Sign = S;
    using Operand = E;
    using Position = Pos;
  };

  /**Collect sign, operation and data-types of the operands for a
   * binary operation.
   */
  template<class S, class F, class TreeData0, class TreeData1, class OptimizeTag = OptimizeTop>
  struct OperationPair
  {
    using Sign = S;
    using FunctorType = F;
    using TreeNode0 = TreeData0;
    using TreeNode1 = TreeData1;
    using Tag = OptimizeTag;
  };

  template<class T, class SFINAE = void>
  constexpr inline bool IsTreeDataV = false;

  template<class Sign, class E, class Pos>
  constexpr inline bool IsTreeDataV<TreeData<Sign, E, Pos> > = true;

  /**An operations pair for collecting tuples of operands subject to
   * the same operation as left operand with a single right operand.
   */
  template<class S, class F, class... TreeData0, class TreeData1, class OptimizeTag>
  struct OperationPair<S, F, MPL::TypeTuple<TreeData0...>, TreeData1, OptimizeTag>
  {
    using Sign = S;
    using FunctorType = F;
    using TreeNode0 = MPL::TypeTuple<TreeData0...>;
    using TreeNode1 = TreeData1;
    using Tag = OptimizeTag;
  };

  /**An operations pair for collecting tuples of operands subject to
   * the same operation as right operand with a single left operand.
   */
  template<class S, class F, class TreeData0, class... TreeData1, class OptimizeTag>
  struct OperationPair<S, F, TreeData0, MPL::TypeTuple<TreeData1...>, OptimizeTag>
  {
    using Sign = S;
    using FunctorType = F;
    using TreeNode0 = TreeData0;
    using TreeNode1 = MPL::TypeTuple<TreeData1...>;
    using Tag = OptimizeTag;
  };

  template<class T, class SFINAE = void>
  constexpr inline bool IsOperationPairV = false;

  template<class Sign, class InnerF, class TreeData0, class TreeData1, class OptimizeTag>
  constexpr inline bool IsOperationPairV<OperationPair<Sign, InnerF, TreeData0, TreeData1, OptimizeTag> > = true;

  template<class F, class Node>
  struct MergeTreeNodeSignHelper;

  template<class OuterSign, class Sign, class E, class Pos>
  struct MergeTreeNodeSignHelper<OuterSign, TreeData<Sign, E, Pos> >
  {
    using Type = TreeData<NestedSumFunctor<OuterSign, Sign>, E, Pos>;
  };

  template<class OuterSign, class Sign, class F, class TreeData0, class TreeData1, class OptimizeTag>
  struct MergeTreeNodeSignHelper<OuterSign, OperationPair<Sign, F, TreeData0, TreeData1, OptimizeTag> >
  {
    using Type = OperationPair<NestedSumFunctor<OuterSign, Sign>, F, TreeData0, TreeData1, OptimizeTag>;
  };

  template<class Sign, class... Node>
  struct MergeTreeNodeSignHelper<Sign, MPL::TypeTuple<Node...> >
  {
    using Type = MPL::TypeTuple<typename MergeTreeNodeSignHelper<Sign, Node>::Type...>;
  };

  /**Combine @a Sign with the sign of @a Node which must be either a
   * TreeData or an OperationPair or an MPL::TypeTuple of these
   * types.
   */
  template<class Sign, class Node>
  using MergeTreeNodeSign = typename MergeTreeNodeSignHelper<Sign, Node>::Type;

  template<class F, class Node>
  struct SetTreeNodeSignHelper;

  template<class OuterSign, class Sign, class E, class Pos>
  struct SetTreeNodeSignHelper<OuterSign, TreeData<Sign, E, Pos> >
  {
    using Type = TreeData<OuterSign, E, Pos>;
  };

  template<class OuterSign, class Sign, class F, class TreeData0, class TreeData1, class OptimizeTag>
  struct SetTreeNodeSignHelper<OuterSign, OperationPair<Sign, F, TreeData0, TreeData1, OptimizeTag> >
  {
    using Type = OperationPair<OuterSign, F, TreeData0, TreeData1, OptimizeTag>;
  };

  template<class Sign, class... Node>
  struct SetTreeNodeSignHelper<Sign, MPL::TypeTuple<Node...> >
  {
    using Type = MPL::TypeTuple<typename SetTreeNodeSignHelper<Sign, Node>::Type...>;
  };

  /**Inject @a Sign into @a Node which must be either a TreeData or an
   * OperationPair or an MPL::TypeTuple of these.
   */
  template<class Sign, class Node>
  using SetTreeNodeSign = typename SetTreeNodeSignHelper<Sign, Node>::Type;

  template<class T>
  struct PositiveTreeNodeHelper;

  template<class Sign, class E, class Pos>
  struct PositiveTreeNodeHelper<TreeData<Sign, E, Pos> >
  {
    using Type = TreeData<PlusFunctor, E, Pos>;
  };

  template<class Sign, class F, class TreeData0, class TreeData1, class OptimizeTag>
  struct PositiveTreeNodeHelper<OperationPair<Sign, F, TreeData0, TreeData1, OptimizeTag> >
  {
    using Type = OperationPair<PlusFunctor, F, TreeData0, TreeData1, OptimizeTag>;
  };

  template<class... Node>
  struct PositiveTreeNodeHelper<MPL::TypeTuple<Node...> >
  {
    using Type = MPL::TypeTuple<typename PositiveTreeNodeHelper<Node>::Type...>;
  };

  template<class Node>
  using PositiveTreeNode = typename PositiveTreeNodeHelper<Node>::Type;

  template<class T>
  struct NegativeTreeNodeHelper;

  template<class Sign, class E, class Pos>
  struct NegativeTreeNodeHelper<TreeData<Sign, E, Pos> >
  {
    using Type = TreeData<MinusFunctor, E, Pos>;
  };

  template<class Sign, class F, class TreeData0, class TreeData1, class OptimizeTag>
  struct NegativeTreeNodeHelper<OperationPair<Sign, F, TreeData0, TreeData1, OptimizeTag> >
  {
    using Type = OperationPair<MinusFunctor, F, TreeData0, TreeData1, OptimizeTag>;
  };

  template<class Node>
  using NegativeTreeNode = typename NegativeTreeNodeHelper<Node>::Type;

  template<class Node>
  constexpr inline bool IsPositiveTreeNodeV = std::is_same<typename Node::Sign, PlusFunctor>::value;

  template<class T>
  constexpr inline bool IsSimpleTreeNodeV = false;

  template<class S, class E, class Pos>
  constexpr inline bool IsSimpleTreeNodeV<TreeData<S, E, Pos> > = true;

  template<class S, class F, class TreeData0, class TreeData1, class OptimizeTag>
  constexpr inline bool IsSimpleTreeNodeV<OperationPair<S, F, TreeData0, TreeData1, OptimizeTag> > =
    IsSimpleTreeNodeV<TreeData0> && IsSimpleTreeNodeV<TreeData1>;

  template<class T, class SFINAE = void>
  struct TreeExpressionHelper
  {};

  template<class T>
  using TreeExpression = typename TreeExpressionHelper<T>::Type;

  template<class S, class E, class Pos>
  struct TreeExpressionHelper<TreeData<S, E, Pos> >
  {
    using Type = E;
  };

  template<class S, class F, class TreeData0, class TreeData1, class OptimizeTag>
  struct TreeExpressionHelper<
    OperationPair<S, F, TreeData0, TreeData1, OptimizeTag>,
    std::enable_if_t<IsSimpleTreeNodeV<TreeData0> && IsSimpleTreeNodeV<TreeData1> >
    >
  {
    using Type = ExpressionType<OptimizeTag, F, TreeExpression<TreeData0>, TreeExpression<TreeData1> >;
  };

  /////////

  template<class TreeNode>
  constexpr inline std::size_t ComplexityV<TreeNode, std::enable_if_t<IsSimpleTreeNodeV<TreeNode> > > =
    ComplexityV<TreeExpression<TreeNode> >;

  template<class S, class F, class TreeData0, class TreeData1, class OptimizeTag>
  constexpr inline std::size_t ComplexityV<
    OperationPair<S, F, TreeData0, TreeData1, OptimizeTag>,
    std::enable_if_t<!IsSimpleTreeNodeV<TreeData0> || !IsSimpleTreeNodeV<TreeData1> > > =
    1 + ComplexityV<TreeData0> + ComplexityV<TreeData1>;

  /**Get the complexity of a tuple of expression types under the
   * assumption that we need (N-1) operations (new tree nodes) to
   * combine the N arguments.
   */
  template<class Arg0, class... Arg>
  constexpr inline std::size_t ComplexityV<MPL::TypeTuple<Arg0, Arg...> > =
    (ComplexityV<Arg0> + ... + ComplexityV<Arg>) + sizeof...(Arg) + 1UL - IsPositiveTreeNodeV<Arg0>;

  ////////

  template<class TreeNode>
  constexpr inline std::size_t ExpressionWeightV<TreeNode, std::enable_if_t<IsSimpleTreeNodeV<TreeNode> > > =
    ExpressionWeightV<TreeExpression<TreeNode> >;

  template<class S, class F, class TreeData0, class TreeData1, class OptimizeTag>
  constexpr inline std::size_t ExpressionWeightV<
    OperationPair<S, F, TreeData0, TreeData1, OptimizeTag>,
    std::enable_if_t<!IsSimpleTreeNodeV<TreeData0> || !IsSimpleTreeNodeV<TreeData1> > > =
    Policy::WeightDefaultV + ExpressionWeightV<TreeData0> + ExpressionWeightV<TreeData1>;

  /**Get the complexity of a tuple of expression types under the
   * assumption that we need (N-1) operations (new tree nodes) to
   * combine the N arguments.
   */
  template<class Arg0, class... Arg>
  constexpr inline std::size_t ExpressionWeightV<MPL::TypeTuple<Arg0, Arg...> > =
    (ExpressionWeightV<Arg0> + ... + ExpressionWeightV<Arg>) + Policy::WeightDefaultV * (sizeof...(Arg) + 1UL - IsPositiveTreeNodeV<Arg0>);

  ////////

  namespace impl {

    template<class F, class E, class Pos>
    struct ExpressionFunctor<TreeData<F, E, Pos> >
    {
      using Type = Functor<E>;
    };

    template<class Sign, class Functor, class TreeData0, class TreeData1, class OptimizeTag>
    struct ExpressionFunctor<OperationPair<Sign, Functor, TreeData0, TreeData1, OptimizeTag> >
    {
      using Type = Functor;
    };

  }

} // Dune::ACFem::Expressions::

#endif // __DUNE_ACFEM_EXPRESSIONS_OPERATIONPAIR_HH__
