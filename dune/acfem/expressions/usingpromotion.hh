/*@file Suck all type-promotion operators into the surrounding
 * namespace. Note that this is different from a "using
 * Expressions::Promotion" namespace in that a using NS:Thing is a
 * declaration while "using NS" is just a directive. It makes a
 * difference during argument dependent lookup.
 */

/**@addtogroup ExpressionTemplates
 *
 * @{
 *
 */

/**@addtogroup ExpressionInterface
 *
 * @{
 */

using ::Dune::ACFem::Expressions::operator+;
using ::Dune::ACFem::Expressions::operator-;
using ::Dune::ACFem::Expressions::operator*;
using ::Dune::ACFem::Expressions::operator/;
using ::Dune::ACFem::Expressions::operator%;
using ::Dune::ACFem::Expressions::operator~;
using ::Dune::ACFem::Expressions::operator&;
using ::Dune::ACFem::Expressions::operator|;
using ::Dune::ACFem::Expressions::operator^;
using ::Dune::ACFem::Expressions::operator==;
using ::Dune::ACFem::Expressions::operator!=;
using ::Dune::ACFem::Expressions::operator<=;
using ::Dune::ACFem::Expressions::operator>=;
using ::Dune::ACFem::Expressions::operator<;
using ::Dune::ACFem::Expressions::operator>;

using ::Dune::ACFem::Expressions::assume;
using ::Dune::ACFem::Expressions::indeterminate;
using ::Dune::ACFem::Expressions::placeholder;
using ::Dune::ACFem::Expressions::subExpression;

using ::Dune::ACFem::Expressions::reciprocal;
using ::Dune::ACFem::Expressions::sqrt;
using ::Dune::ACFem::Expressions::sqr;

using ::Dune::ACFem::Expressions::cos;
using ::Dune::ACFem::Expressions::sin;
using ::Dune::ACFem::Expressions::tan;

using ::Dune::ACFem::Expressions::acos;
using ::Dune::ACFem::Expressions::asin;
using ::Dune::ACFem::Expressions::atan;

using ::Dune::ACFem::Expressions::exp;
using ::Dune::ACFem::Expressions::log;

using ::Dune::ACFem::Expressions::cosh;
using ::Dune::ACFem::Expressions::sinh;
using ::Dune::ACFem::Expressions::tanh;

using ::Dune::ACFem::Expressions::acosh;
using ::Dune::ACFem::Expressions::asinh;
using ::Dune::ACFem::Expressions::atanh;

using ::Dune::ACFem::Expressions::erf;
using ::Dune::ACFem::Expressions::lgamma;
using ::Dune::ACFem::Expressions::tgamma;

using ::Dune::ACFem::Expressions::ceil;
using ::Dune::ACFem::Expressions::floor;
using ::Dune::ACFem::Expressions::round;
using ::Dune::ACFem::Expressions::abs;

using ::Dune::ACFem::Expressions::atan2;
using ::Dune::ACFem::Expressions::pow;
using ::Dune::ACFem::Expressions::min;
using ::Dune::ACFem::Expressions::max;

//!@}

//!@}
