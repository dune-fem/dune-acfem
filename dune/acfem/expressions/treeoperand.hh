#ifndef __DUNE_ACFEM_EXPRESSIONS__TREEOPERAND_HH__
#define __DUNE_ACFEM_EXPRESSIONS__TREEOPERAND_HH__

#include "storage.hh"

namespace Dune::ACFem::Expressions {

  /**Position of a sub-expression in a given expression (tree) where
   * the d-th number ArgPos in the integer-sequence designates operand
   * number ArgPos at depth d in the expression tree.
   */
  template<std::size_t... ArgPos>
  using TreePosition = IndexSequence<ArgPos...>;

  template<class E>
  constexpr decltype(auto) treeOperand(E&& e, TreePosition<> = TreePosition<>{})
  {
    return std::forward<E>(e);
  }

  template<std::size_t I0, class E>
  constexpr decltype(auto) treeOperand(E&& e, TreePosition<I0> = TreePosition<I0>{})
  {
    return std::forward<E>(e).template operand<I0>();
  }

  template<std::size_t I0, std::size_t I1, std::size_t... I, class E>
  constexpr decltype(auto) treeOperand(E&& e, TreePosition<I0, I1, I...> = TreePosition<I0, I1, I...>{})
  {
    return treeOperand(std::forward<E>(e).template operand<I0>(), TreePosition<I1, I...>{});
  }

  namespace {

    template<class TreePos, class E>
    struct TreeOperandHelper;

    template<std::size_t I0, class E>
    struct TreeOperandHelper<TreePosition<I0>, E>
    {
      using Type = Operand<I0, E>;
    };

    template<std::size_t I0, std::size_t I1, std::size_t... I, class E>
    struct TreeOperandHelper<TreePosition<I0, I1, I...>, E>
    {
      using Type = typename TreeOperandHelper<TreePosition<I1, I...>, Operand<I0, E> >::Type;
    };

  }

  /**Get the type of the operand at tree-position TreePos in E.*/
  template<class TreePos, class E>
  using TreeOperand = typename TreeOperandHelper<TreePos, E>::Type;

}

#endif // __DUNE_ACFEM_EXPRESSIONS__TREEOPERAND_HH__
