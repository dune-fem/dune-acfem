#ifndef __DUNE_ACFEM_EXPRESSIONS_EXPRESSIONOPERATIONS_HH__
#define __DUNE_ACFEM_EXPRESSIONS_EXPRESSIONOPERATIONS_HH__

/**@file
 *
 * A list of supported expression operations.
 */

#include "../common/types.hh"
#include "../mpl/uniquetags.hh"

namespace Dune
{

  namespace ACFem
  {

    /**@addtogroup ExpressionTemplates
     *
     * @{
     *
     */

    /**@addtogroup ExpressionOperations
     *
     * "Tag"-structures for each supported algebraic operation, used
     * to distinguish the various expression template classes from
     * each other.
     *
     * @{
     */

    //! Addition of two objects.
    struct PlusOperation
    {};

    //! A += B
    struct PlusEqOperation
    {};

    //! Subtraction of two objects and unary minus.
    struct MinusOperation
    {};

    //! A -= B
    struct MinusEqOperation
    {};

    //! Logical Not
    struct LogicalNotOperation
    {};

    //! Logical And
    struct LogicalAndOperation
    {};

    //! Logical Or
    struct LogicalOrOperation
    {};

    //! ==
    struct EqOperation
    {};

    //! !=
    struct NeOperation
    {};

    //! >
    struct GtOperation
    {};

    //! >=
    struct GeOperation
    {};

    //! <
    struct LtOperation
    {};

    //! <=
    struct LeOperation
    {};

    //! Multiplication by scalars from the left.
    struct SMultiplyOperation
    {};

    //! A *= s
    struct SMultiplyEqOperation
    {};

    //! Identity, i.e. just wrap the object
    struct IdentityOperation
    {};

    /**Assume operations attach a property to the wrapped
     * expression.
     */
    template<class... Property>
    struct AssumeOperation
    {
      using Properties = MPL::TagContainer<Property...>;
    };

    /**Indeterminate operation, wrap another object and attach an
     * id. All wrapped objects with the same id are considered
     * equal. During auto-differentiation "indeterminate expressions"
     * act as indeterminate variables.
     */
    template<std::size_t Id>
    struct IndeterminateOperation
    {};

    /**Placeholder operation wraps another object and attaches an id
     * to it. Placeholder operations are used in order to identify
     * placeholders in an expression chain. The difference to
     * IndeterminateOperation is that during auto-differentiation the
     * derivative of an "indetermiante expression" is the constant 1
     * (what "1" may ever mean in the given context) and the
     * derivative of a "placeholder expression" is not known unless
     * explicitly implemented.
     */
    template<class Placeholder>
    struct PlaceholderOperation
    {};

    /**Kind of placeholder. Intended to inject evaluation checkpoints
     * into the lazy evaluation chain.
     *
     * @param TreePos An index sequence marking the position in a
     * given expression tree. TreePos holds the operand position while
     * descending the expression tree.
     *
     */
    template<class TreePos = IndexSequence<> >
    struct SubExpressionOperation
    {};

    //! Inversion of a scalar object.
    struct ReciprocalOperation
    {};

    //! Taking the square root of an object.
    struct SqrtOperation
    {};

    //! Taking the square of an object.
    struct SquareOperation
    {};

    //! Taking something to the power of something
    struct PowOperation
    {};

    //! Exponentiation of an object.
    struct ExpOperation
    {};

    //! Taking the logarithm of an object.
    struct LogOperation
    {};

    //! Hyperbolic cosine
    struct CoshOperation
    {};

    //! Hyperbolic sine
    struct SinhOperation
    {};

    //! Hyperbolic tangens
    struct TanhOperation
    {};

    //! Inverse hyperbolic cosine
    struct AcoshOperation
    {};

    //! Inverse hyperbolic sine
    struct AsinhOperation
    {};

    //! Inverse hyperbolic tangens
    struct AtanhOperation
    {};

    //! Taking the sine of an object.
    struct SinOperation
    {};

    //! Taking the cosine of an object.
    struct CosOperation
    {};

    //! Taking the tangens of an object.
    struct TanOperation
    {};

    //! Taking the arc sine of an object.
    struct AsinOperation
    {};

    //! Taking the arc cosine of an object.
    struct AcosOperation
    {};

    //! Taking the arctan of an object.
    struct AtanOperation
    {};

    //! Taking the arctan of an object.
    struct Atan2Operation
    {};

    //! erf
    struct ErfOperation
    {};

    //! tgamma
    struct TGammaOperation
    {};

    //! ;gamma
    struct LGammaOperation
    {};

    //! ceil
    struct CeilOperation
    {};

    //! floor
    struct FloorOperation
    {};

    //! round
    struct RoundOperation
    {};

    //! abs
    struct AbsOperation
    {};

    //! min
    struct MinOperation
    {};

    //! max
    struct MaxOperation
    {};

    //! IfOperation
    struct TernaryOperation
    {};

    //! @} ExpressionOperations

    //! @} ExpressionTemplates

  } // ACFem

} // Dune

#endif // __DUNE_ACFEM_EXPRESSIONS_EXPRESSIONOPERATIONS_HH__
