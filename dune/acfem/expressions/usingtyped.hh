/*@file Inject using declarations into the surrounding namespace.
 */

/**@addtogroup ExpressionTemplates
 *
 * @{
 *
 */

/**@addtogroup ExpressionInterface
 *
 * @{
 */

using ::Dune::ACFem::TypedValue::min;
using ::Dune::ACFem::TypedValue::max;

using ::Dune::ACFem::TypedValue::operator*;
using ::Dune::ACFem::TypedValue::operator/;
using ::Dune::ACFem::TypedValue::operator%;
using ::Dune::ACFem::TypedValue::operator+;
using ::Dune::ACFem::TypedValue::operator-;

using ::Dune::ACFem::TypedValue::operator==;
using ::Dune::ACFem::TypedValue::operator!=;
using ::Dune::ACFem::TypedValue::operator>=;
using ::Dune::ACFem::TypedValue::operator<=;
using ::Dune::ACFem::TypedValue::operator>;
using ::Dune::ACFem::TypedValue::operator<;

using ::Dune::ACFem::TypedValue::operator+=;
using ::Dune::ACFem::TypedValue::operator-=;
using ::Dune::ACFem::TypedValue::operator*=;
using ::Dune::ACFem::TypedValue::operator/=;

//!@}

//!@}
