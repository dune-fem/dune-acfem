#ifndef __DUNE_ACFEM_EXPRESSIONS_TRAITSDEFAULT_HH__
#define __DUNE_ACFEM_EXPRESSIONS_TRAITSDEFAULT_HH__

#include <ostream>

#include "expressiontraits.hh"
#include "signutil.hh"
#include "storage.hh"
#include "terminal.hh"

namespace Dune {

  namespace ACFem {

    namespace Expressions {

      /**Deduce ExpressionTraits from tag-class overrides.*/
      template<class T>
      struct TraitsOfTags
      {
        using ExpressionType = std::decay_t<T>;

        static constexpr bool isNonZero = std::is_base_of<NonZeroExpression, ExpressionType>::value;
        static constexpr bool isSemiPositive = std::is_base_of<SemiPositiveExpression, ExpressionType>::value;
        static constexpr bool isSemiNegative = std::is_base_of<SemiNegativeExpression, ExpressionType>::value;

        static constexpr bool isZero = !isNonZero && isSemiPositive && isSemiNegative;
        static constexpr bool isPositive = isSemiPositive && isNonZero;
        static constexpr bool isNegative = isSemiNegative && isNonZero;

        static constexpr bool isOne = std::is_base_of<OneExpression, ExpressionType>::value;
        static constexpr bool isMinusOne = std::is_base_of<MinusOneExpression, ExpressionType>::value;

        static constexpr bool isVolatile = HasTag<ExpressionType, VolatileExpression>::value;
        static constexpr bool isTypedValue = !isVolatile && HasTag<ExpressionType, TypedValueExpression>::value;
        static constexpr bool isConstant = isTypedValue || HasTag<ExpressionType, ConstantExpression>::value;
        static constexpr bool isIndependent = isConstant || HasTag<ExpressionType, IndependentExpression>::value;

        using Sign = ExpressionSign<isNonZero, isSemiPositive, isSemiNegative>;
      };

      template<class Functor, class... T>
      struct TraitsOfOperation
        : ExpressionSignAt<0, decltype(Functor::signPropagation(typename ExpressionTraits<T>::Sign{}...))>
      {
        using SignOfOperation = decltype(Functor::signPropagation(typename ExpressionTraits<T>::Sign{}...));

        static constexpr bool isOne = SignOfOperation::reference == 1 && SignOfOperation::isZero;
        static constexpr bool isMinusOne = SignOfOperation::reference == -1 && SignOfOperation::isZero;

        static constexpr bool isVolatile =
          FunctorHas<IsVolatileOperation, Functor>::value || (... || ExpressionTraits<T>::isVolatile);

        static constexpr bool isTypedValue =
          !isVolatile &&
          !FunctorHas<IsDynamicOperation, Functor>::value &&
          (... && ExpressionTraits<T>::isTypedValue);

        static constexpr bool isConstant =
          isTypedValue || (!FunctorHas<IsDynamicOperation, Functor>::value && (... && IsConstantExprArg<T>::value));

        // Operations with referenced values can never be independent,
        // unless overidden by inheriting from a tag-class.
        static constexpr bool isIndependent =
          isConstant || (!FunctorHas<IsDynamicOperation, Functor>::value
                         && !(... || (std::is_reference<T>::value))
                         && (... && ExpressionTraits<T>::isIndependent)
            );
      };

      namespace {

        template<class T, std::size_t... I>
        constexpr auto traitsOfOperationExpander(IndexSequence<I...>)
        {
          return TraitsOfOperation<Functor<T>, Operand<I, T>...>{};
        }

      }

      template<class T, std::enable_if_t<IsExpression<T>::value, int> = 0>
      constexpr auto traitsOfOperation(T&&)
      {
        return traitsOfOperationExpander<T>(MakeIndexSequence<Arity<T>::value>{});
      }

      template<class T>
      struct TraitsOfOperation<T>
        : decltype(traitsOfOperation(std::declval<T>()))
      {};

    } // Expressions::

    /**Default Expression-traits for any non-terminal expression use
     * the induced traits of the operation while allowing overrides from
     * attached tag-base-classes.
     */
    template<class T>
    struct ExpressionTraits<
      T,
      std::enable_if_t<(IsExpression<T>::value
                        && !IsSelfExpression<T>::value
                        && !Expressions::IsClosure<T>::value
      )>,
      BaseTraitsLevel>
    {
      using ExpressionType = T;

      using TraitsOfTags = Expressions::TraitsOfTags<T>;
      using TraitsOfOperation = Expressions::TraitsOfOperation<T>;

      // If TraitsOfTags is false, then the tag is not there and we
      // use the flag-value induced by the operation.
      static constexpr bool isNonZero = TraitsOfTags::isNonZero || TraitsOfOperation::isNonZero;
      static constexpr bool isSemiPositive = TraitsOfTags::isSemiPositive || TraitsOfOperation::isSemiPositive;
      static constexpr bool isSemiNegative = TraitsOfTags::isSemiNegative || TraitsOfOperation::isSemiNegative;

      static constexpr bool isZero = isSemiPositive && isSemiNegative;
      static constexpr bool isPositive = isSemiPositive && isNonZero;
      static constexpr bool isNegative = isSemiNegative && isNonZero;

      static constexpr bool isOne = TraitsOfTags::isOne || TraitsOfOperation::isOne;
      static constexpr bool isMinusOne = TraitsOfTags::isMinusOne || TraitsOfOperation::isMinusOne;

      static constexpr bool isVolatile =
        TraitsOfTags::isVolatile || TraitsOfOperation::isVolatile;

      static constexpr bool isIndependent =
        TraitsOfTags::isIndependent
        ||
        (!isVolatile && TraitsOfOperation::isIndependent);

      static constexpr bool isConstant =
        TraitsOfTags::isConstant
        ||
        (!isVolatile && TraitsOfOperation::isConstant);

      static constexpr bool isTypedValue =
        TraitsOfTags::isTypedValue
        ||
        (!isVolatile && TraitsOfOperation::isTypedValue);

      static_assert(!isVolatile || (isVolatile && !isIndependent && !isConstant && !isTypedValue),
                    "Volatile expression cannot be non-volatile.");
      static_assert(!isTypedValue || (isTypedValue && isConstant && isIndependent),
                    "TypedValue expressions must also qualify as constant and independent.");
      static_assert(!isConstant || (isConstant && isIndependent),
                    "Constant expressions must also qualify as independet ones.");

      using Sign = ExpressionSign<isNonZero, isSemiPositive, isSemiNegative>;
    };

    /**Use traits of enclosed type for closure expressions.*/
    template<class T>
    struct ExpressionTraits<
      T,
      std::enable_if_t<Expressions::IsClosure<T>::value>,
      BaseTraitsLevel>
      : ExpressionTraits<Expressions::EnclosedType<T> >
    {
      using ExpressionType = T;
    };

    /**Default expression traits for terminal expressions or
     * non-expressions just look at the tag-classes.
     */
    template<class T>
    struct ExpressionTraits<
      T,
      std::enable_if_t<!IsExpression<T>::value || IsSelfExpression<T>::value>,
      BaseTraitsLevel>
      : Expressions::TraitsOfTags<T>
    {};

    /**This is the default implementation if not overriden at higher
     * level. Higher level may also opt to inherit from this one and
     * specialize as appropriate.
     */
    template<class T>
    using BaseExpressionTraits = ExpressionTraits<T, void, BaseTraitsLevel>;

    /**Explicit expression traits for integer constants. FIXME. Maybe no longer needed.*/
    template<class I, I V>
    struct ExpressionTraits<Constant<I, V> >
    {
      static constexpr bool isNonZero = V != 0;
      static constexpr bool isNonSingular = V != 0;
      static constexpr bool isZero = V == 0;
      static constexpr bool isOne = V == 1;
      static constexpr bool isMinusOne = V == -1; // limit!
      static constexpr bool isSemiPositive = !std::numeric_limits<I>::is_signed || V >= 0;
      static constexpr bool isSemiNegative = (std::numeric_limits<I>::is_signed && V <= 0) || V == 0;
      static constexpr bool isPostive = V > 0;
      static constexpr bool isNegative = (std::numeric_limits<I>::is_signed && V < 0);

      static constexpr bool isVolatile = false;
      static constexpr bool isIndependent = true;
      static constexpr bool isConstant = true;
      static constexpr bool isTypedValue = true;

      using Sign = ExpressionSign<isNonZero, isSemiPositive, isSemiNegative, 0>;
    };

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_EXPRESSIONS_TRAITSDEFAULT_HH__
