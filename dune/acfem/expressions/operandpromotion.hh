#ifndef __DUNE_ACFEM_EXPRESSIONS_OPERANDPROMOTION_HH__
#define __DUNE_ACFEM_EXPRESSIONS_OPERANDPROMOTION_HH__

#include <ostream>
#include "expressionoperations.hh"
#include "interface.hh"

// OK. Preprocessor macros. But here it seems to be appropriate
#define DUNE_ACFEM_UNARY_OPERAND_PROMOTION(OP)                          \
template<class T, std::enable_if_t<sizeof(OP(operandPromotion<0>(std::declval<T>()))) >= 0, int> = 0> \
constexpr decltype(auto) OP(T&& t)                                      \
{                                                                       \
  return OP(operandPromotion<0>(std::forward<T>(t)));                   \
}                                                                       \
struct __dune_acfem_allow_semicolon

#define DUNE_ACFEM_BINARY_OPERAND_PROMOTION(OP)                         \
template<class T0, class T1,                                            \
         std::enable_if_t<(sizeof(OP(operandPromotion<0>(std::declval<T0>(), std::declval<T1>()), \
                                     operandPromotion<1>(std::declval<T0>(), std::declval<T1>())) \
                             ) >= 0                                     \
        ), int> = 0>                                                    \
constexpr decltype(auto) OP(T0&& t0, T1&& t1)                           \
{                                                                       \
  return OP(operandPromotion<0>(std::forward<T0>(t0), std::forward<T1>(t1)), \
            operandPromotion<1>(std::forward<T0>(t0), std::forward<T1>(t1))); \
}                                                                       \
struct __dune_acfem_allow_semicolon

namespace Dune
{

  namespace ACFem
  {

    namespace Expressions
    {

// inject math-functions into Expressions::
#include "usingstd.hh"
#include "usingtyped.hh"

      using namespace Literals;

      /**@addtogroup ExpressionTemplates
       *
       * @{
       *
       */

      /**@addtogroup ExpressionInterface
       *
       * @{
       */

      /************************************************************************
       *
       * Standard arithmetic.
       *
       */

      template<class T, std::enable_if_t<sizeof(-operandPromotion<0>(std::declval<T>())) >= 0, int> = 0>
      constexpr decltype(auto) operator-(T&& t)
      {
        return -operandPromotion<0>(std::forward<T>(t));
      }

      template<class T0, class T1,
               std::enable_if_t<(sizeof(
                                   operandPromotion<0>(std::declval<T0>(), std::declval<T1>())
                                   -
                                   operandPromotion<1>(std::declval<T0>(), std::declval<T1>())
                                   ) >= 0
        ), int> = 0>
      constexpr decltype(auto) operator-(T0&& t0, T1&& t1)
      {
        return
          (operandPromotion<0>(std::forward<T0>(t0), std::forward<T1>(t1))
          -
          operandPromotion<1>(std::forward<T0>(t0), std::forward<T1>(t1)));
      }

      template<class T0, class T1,
               std::enable_if_t<(sizeof(
                                   operandPromotion<0>(std::declval<T0>(), std::declval<T1>())
                                   +
                                   operandPromotion<1>(std::declval<T0>(), std::declval<T1>())
                                   ) >= 0
        ), int> = 0>
      constexpr decltype(auto) operator+(T0&& t0, T1&& t1)
      {
        return (
          operandPromotion<0>(std::forward<T0>(t0), std::forward<T1>(t1))
          +
          operandPromotion<1>(std::forward<T0>(t0), std::forward<T1>(t1))
          );
      }

      template<class T0, class T1,
               std::enable_if_t<(sizeof(
                                   operandPromotion<0>(std::declval<T0>(), std::declval<T1>())
                                   *
                                   operandPromotion<1>(std::declval<T0>(), std::declval<T1>())
                                   ) >= 0
        ), int> = 0>
      constexpr decltype(auto) operator*(T0&& t0, T1&& t1)
      {
        return (
          operandPromotion<0>(std::forward<T0>(t0), std::forward<T1>(t1))
          *
          operandPromotion<1>(std::forward<T0>(t0), std::forward<T1>(t1))
          );
      }

      template<class T0, class T1,
               std::enable_if_t<(sizeof(
                                   operandPromotion<0>(std::declval<T0>(), std::declval<T1>())
                                   /
                                   operandPromotion<1>(std::declval<T0>(), std::declval<T1>())
                                   ) >= 0
        ), int> = 0>
      constexpr decltype(auto) operator/(T0&& t0, T1&& t1)
      {
        return (
          operandPromotion<0>(std::forward<T0>(t0), std::forward<T1>(t1))
          /
          operandPromotion<1>(std::forward<T0>(t0), std::forward<T1>(t1))
          );
      }

      template<class T0, class T1,
               std::enable_if_t<(sizeof(
                                   operandPromotion<0>(std::declval<T0>(), std::declval<T1>())
                                   %
                                   operandPromotion<1>(std::declval<T0>(), std::declval<T1>())
                                   ) >= 0
        ), int> = 0>
      constexpr decltype(auto) operator%(T0&& t0, T1&& t1)
      {
        return (
          operandPromotion<0>(std::forward<T0>(t0), std::forward<T1>(t1))
          %
          operandPromotion<1>(std::forward<T0>(t0), std::forward<T1>(t1))
          );
      }

      /************************************************************************
       *
       * Bitwise arithmetic
       *
       */

      template<class T, std::enable_if_t<sizeof(~operandPromotion<0>(std::declval<T>())) >= 0, int> = 0>
      constexpr decltype(auto) operator~(T&& t)
      {
        return ~operandPromotion<0>(std::forward<T>(t));
      }

      template<class T0, class T1,
               std::enable_if_t<(sizeof(
                                   operandPromotion<0>(std::declval<T0>(), std::declval<T1>())
                                   &
                                   operandPromotion<1>(std::declval<T0>(), std::declval<T1>())
                                   ) >= 0
        ), int> = 0>
      constexpr decltype(auto) operator&(T0&& t0, T1&& t1)
      {
        return (operandPromotion<0>(std::forward<T0>(t0), std::forward<T1>(t1))
                &
                operandPromotion<1>(std::forward<T0>(t0), std::forward<T1>(t1)));
      }

      template<class T0, class T1,
               std::enable_if_t<(sizeof(
                                   operandPromotion<0>(std::declval<T0>(), std::declval<T1>())
                                   |
                                   operandPromotion<1>(std::declval<T0>(), std::declval<T1>())
                                   ) >= 0
        ), int> = 0>
      constexpr decltype(auto) operator|(T0&& t0, T1&& t1)
      {
        return (operandPromotion<0>(std::forward<T0>(t0), std::forward<T1>(t1))
                |
                operandPromotion<1>(std::forward<T0>(t0), std::forward<T1>(t1)));
      }

      template<class T0, class T1,
               std::enable_if_t<(sizeof(
                                   operandPromotion<0>(std::declval<T0>(), std::declval<T1>())
                                   ^
                                   operandPromotion<1>(std::declval<T0>(), std::declval<T1>())
                                   ) >= 0
        ), int> = 0>
      constexpr decltype(auto) operator^(T0&& t0, T1&& t1)
      {
        return (operandPromotion<0>(std::forward<T0>(t0), std::forward<T1>(t1))
                ^
                operandPromotion<1>(std::forward<T0>(t0), std::forward<T1>(t1)));
      }

      /************************************************************************
       *
       * Comparisons
       *
       */

      template<class T0, class T1,
               std::enable_if_t<(sizeof(
                                   operandPromotion<0>(std::declval<T0>(), std::declval<T1>())
                                   ==
                                   operandPromotion<1>(std::declval<T0>(), std::declval<T1>())
                                   ) >= 0
        ), int> = 0>
      constexpr auto operator==(T0&& t0, T1&& t1)
      {
        return (operandPromotion<0>(std::forward<T0>(t0), std::forward<T1>(t1))
                ==
                operandPromotion<1>(std::forward<T0>(t0), std::forward<T1>(t1)));
      }

      template<class T0, class T1,
               std::enable_if_t<(sizeof(
                                   operandPromotion<0>(std::declval<T0>(), std::declval<T1>())
                                   !=
                                   operandPromotion<1>(std::declval<T0>(), std::declval<T1>())
                                   ) >= 0
        ), int> = 0>
      constexpr auto operator!=(T0&& t0, T1&& t1)
      {
        return (operandPromotion<0>(std::forward<T0>(t0), std::forward<T1>(t1))
                !=
                operandPromotion<1>(std::forward<T0>(t0), std::forward<T1>(t1)));
      }

      template<class T0, class T1,
               std::enable_if_t<(sizeof(
                                   operandPromotion<0>(std::declval<T0>(), std::declval<T1>())
                                   <=
                                   operandPromotion<1>(std::declval<T0>(), std::declval<T1>())
                                   ) >= 0
        ), int> = 0>
      constexpr auto operator<=(T0&& t0, T1&& t1)
      {
        return (operandPromotion<0>(std::forward<T0>(t0), std::forward<T1>(t1))
                <=
                operandPromotion<1>(std::forward<T0>(t0), std::forward<T1>(t1)));
      }

      template<class T0, class T1,
               std::enable_if_t<(sizeof(
                                   operandPromotion<0>(std::declval<T0>(), std::declval<T1>())
                                   <
                                   operandPromotion<1>(std::declval<T0>(), std::declval<T1>())
                                   ) >= 0
        ), int> = 0>
      constexpr auto operator<(T0&& t0, T1&& t1)
      {
        return (operandPromotion<0>(std::forward<T0>(t0), std::forward<T1>(t1))
                <
                operandPromotion<1>(std::forward<T0>(t0), std::forward<T1>(t1)));
      }

      template<class T0, class T1,
               std::enable_if_t<(sizeof(
                                   operandPromotion<0>(std::declval<T0>(), std::declval<T1>())
                                   >=
                                   operandPromotion<1>(std::declval<T0>(), std::declval<T1>())
                                   ) >= 0
        ), int> = 0>
      constexpr auto operator>=(T0&& t0, T1&& t1)
      {
        return (operandPromotion<0>(std::forward<T0>(t0), std::forward<T1>(t1))
                >=
                operandPromotion<1>(std::forward<T0>(t0), std::forward<T1>(t1)));
      }

      template<class T0, class T1,
               std::enable_if_t<(sizeof(
                                   operandPromotion<0>(std::declval<T0>(), std::declval<T1>())
                                   >
                                   operandPromotion<1>(std::declval<T0>(), std::declval<T1>())
                                   ) >= 0
        ), int> = 0>
      constexpr auto operator>(T0&& t0, T1&& t1)
      {
        return (operandPromotion<0>(std::forward<T0>(t0), std::forward<T1>(t1))
                >
                operandPromotion<1>(std::forward<T0>(t0), std::forward<T1>(t1)));
      }

      /************************************************************************
       *
       * IO (rather O ATM)
       *
       */

      template<class T, std::enable_if_t<sizeof(-operandPromotion<0>(std::declval<T>())) >= 0, int> = 0>
      std::ostream& operator<<(std::ostream& out, T&& t)
      {
        return out << operandPromotion<0>(std::forward<T>(t));
      }

      /************************************************************************
       *
       * Special operations
       *
       */

      template<
        class... Properties, class T,
        std::enable_if_t<(sizeof(assume(operandPromotion<0>(std::declval<T>()), MPL::TagContainer<Properties...>{})) >= 0), int> = 0>
      constexpr decltype(auto) assume(T&& t, MPL::TagContainer<Properties...> = MPL::TagContainer<Properties...>{})
      {
        return assume<Properties...>(operandPromotion<0>(std::forward<T>(t)));
      }

      template<
        std::size_t Id, class T,
        std::enable_if_t<(sizeof(indeterminate(operandPromotion<0>(std::declval<T>()), IndexConstant<Id>{})) >= 0), int> = 0>
      constexpr decltype(auto) indeterminate(T&& t, IndexConstant<Id> = IndexConstant<Id>{})
      {
        return indeterminate<Id>(operandPromotion<0>(std::forward<T>(t)));
      }

      template<
        class T, class Placeholder = EnclosedType<T>,
        std::enable_if_t<(sizeof(
                            placeholder(
                              operandPromotion<0>(std::declval<T>()),
                              PlaceholderOperation<Placeholder>{}
                              )
                            ) >= 0), int> = 0>
      constexpr decltype(auto) placeholder(T&& t, PlaceholderOperation<Placeholder> = PlaceholderOperation<Placeholder>{})
      {
        return placeholder(operandPromotion<0>(std::forward<T>(t)), PlaceholderOperation<Placeholder>{});
      }

      template<
        std::size_t... OperandPos, class T0, class T1,
        std::enable_if_t<(sizeof(
                            subExpression(operandPromotion<0>(std::declval<T0>(), std::declval<T1>()),
                                          operandPromotion<1>(std::declval<T0>(), std::declval<T1>()),
                                          IndexSequence<OperandPos...>{})
                            ) >= 0
        ), int> = 0>
      constexpr decltype(auto) subExpression(T0&& t0, T1&& t1, IndexSequence<OperandPos...> = IndexSequence<OperandPos...>{})
      {
        return subExpression(
          operandPromotion<0>(std::forward<T0>(t0), std::forward<T1>(t1)),
          operandPromotion<1>(std::forward<T0>(t0), std::forward<T1>(t1)),
          IndexSequence<OperandPos...>{});
      }

      /************************************************************************
       *
       * Math operations
       *
       */

      DUNE_ACFEM_UNARY_OPERAND_PROMOTION(reciprocal);

      DUNE_ACFEM_UNARY_OPERAND_PROMOTION(sqrt);
      DUNE_ACFEM_UNARY_OPERAND_PROMOTION(sqr);

      DUNE_ACFEM_UNARY_OPERAND_PROMOTION(cos);
      DUNE_ACFEM_UNARY_OPERAND_PROMOTION(sin);
      DUNE_ACFEM_UNARY_OPERAND_PROMOTION(tan);

      DUNE_ACFEM_UNARY_OPERAND_PROMOTION(acos);
      DUNE_ACFEM_UNARY_OPERAND_PROMOTION(asin);
      DUNE_ACFEM_UNARY_OPERAND_PROMOTION(atan);

      DUNE_ACFEM_UNARY_OPERAND_PROMOTION(exp);
      DUNE_ACFEM_UNARY_OPERAND_PROMOTION(log);

      DUNE_ACFEM_UNARY_OPERAND_PROMOTION(cosh);
      DUNE_ACFEM_UNARY_OPERAND_PROMOTION(sinh);
      DUNE_ACFEM_UNARY_OPERAND_PROMOTION(tanh);

      DUNE_ACFEM_UNARY_OPERAND_PROMOTION(acosh);
      DUNE_ACFEM_UNARY_OPERAND_PROMOTION(asinh);
      DUNE_ACFEM_UNARY_OPERAND_PROMOTION(atanh);

      DUNE_ACFEM_UNARY_OPERAND_PROMOTION(erf);
      DUNE_ACFEM_UNARY_OPERAND_PROMOTION(lgamma);
      DUNE_ACFEM_UNARY_OPERAND_PROMOTION(tgamma);

      DUNE_ACFEM_UNARY_OPERAND_PROMOTION(ceil);
      DUNE_ACFEM_UNARY_OPERAND_PROMOTION(floor);
      DUNE_ACFEM_UNARY_OPERAND_PROMOTION(round);
      DUNE_ACFEM_UNARY_OPERAND_PROMOTION(abs);

      DUNE_ACFEM_BINARY_OPERAND_PROMOTION(atan2);
      DUNE_ACFEM_BINARY_OPERAND_PROMOTION(pow);
      DUNE_ACFEM_BINARY_OPERAND_PROMOTION(min);
      DUNE_ACFEM_BINARY_OPERAND_PROMOTION(max);

      //! @} ExpressionInterface

      //! @} ExpressionTemplates

    } // Expressions

// The following include sucks all operator declarations into the
// ACFem namespace.
#include "usingpromotion.hh"

  } // ACFem

} // Dune

#endif // __DUNE_ACFEM_EXPRESSIONS_OPERANDPROMOTION_HH__
