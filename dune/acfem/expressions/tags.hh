#ifndef __DUNE_ACFEM_EXPRESSIONS_TAGS_HH__
#define __DUNE_ACFEM_EXPRESSIONS_TAGS_HH__

/**@file
 *
 * Tag structures defining properties of expressions.
 */

#include "../mpl/uniquetags.hh"

namespace Dune
{

  namespace ACFem
  {

    /**@addtogroup ExpressionTemplates
     *
     * ACFem provides expression templates in order to form algebraic
     * expression for models, grid-functions and parameters.
     *
     * @{
     *
     */

    /**@addtogroup ExpressionTraits
     *
     * "Tag"-structures for each supported algebraic operation, used
     * to distinguish the various expression template classes from
     * each other.
     *
     * @{
     */

    /**@name ExpressionTags
     *
     * Empty tag structures in order to indicate certain
     * properties. See also ExpressionSign.
     *
     *@{
     */

    /**A terminal expression is an "expression end-point", i.e. a
     * "data provider", like a real vector storing the data in a
     * vector expression.
     */
    struct TerminalExpression
    {};

    /**A volatile expression will not be replaced by its value except
     * for RuntimeEqual optimizations.
     */
    struct VolatileExpression
    {};

    /**A tag structure signalling that objects of this type can be
     * considered to yield the same values at runtime, if encountered
     * in the same expression. Non-trivial examples are indeterminate
     * and placeholder expressions.
     */
    struct RuntimeEqualExpression
    {};

    /**A tag for use with assume() to generate a unique type which is
     * assumed to be "runtime equal" by inheriting from
     * RuntimeEqualExpression.
     */
    template<std::size_t id>
    struct UniqueExpression
      : RuntimeEqualExpression
    {};

    /**An independent expression is an expression which does not
     * depend on external state. For example a scalar-product
     * expression storing the data of the two vector operands. An
     * IndependentExpression is in some sense constant, although its
     * evaluation may need a considerable amount of work and its
     * actual value can only be determined at runtime.
     */
    struct IndependentExpression
    {};

    /**Something constant on a given entity.*/
    struct PieceWiseConstantExpression
    {};

    /**A constant. Something known to be constant during the run-time
     * of the program.
     */
    struct ConstantExpression
      : PieceWiseConstantExpression
      , IndependentExpression
    {};

    /**A tag structure signalling that this expression carries its
     * value in its type.
     */
    struct TypedValueExpression
      : ConstantExpression
    {};

    /**Something which may be change between different evaluations of
     * an expression.
     */
    struct MutableExpression
    {};

    /**Greater equal 0 expression.*/
    struct SemiPositiveExpression
    {};

    /**Less equal 0 expression.*/
    struct SemiNegativeExpression
    {};

    /**Complementary to ZeroExpression for use in std::conditional,
     * for example, otherwise unused. This is not the same as
     * !ZeroExpression, because this struct expresses knowledge that
     * the expression indeed is non zero at run time.
     */
    struct NonZeroExpression
    {};

    /**Non-singular, but somehow zero, whatever that means.*/
    struct RegularZeroExpression
      : NonZeroExpression
      , SemiPositiveExpression
      , SemiNegativeExpression
    {};

    /**A tag structure which can be attached as base class to
     * zero-expressions like the ZeroGridFunction, the ZeroModel, the
     * ZeroFunctional. We implement the fact that >= 0 and <= 0 means
     * 0.
     */
    struct ZeroExpression
      : MPL::UniqueTags<SemiPositiveExpression, SemiNegativeExpression, ConstantExpression, TypedValueExpression>
    {};

    /**An expression which is known to be positive.*/
    struct PositiveExpression
      : NonZeroExpression
      , SemiPositiveExpression
    {};

    /**An expression which is known to be negative.*/
    struct NegativeExpression
      : NonZeroExpression
      , SemiNegativeExpression
    {};

    /**A tag structure which can be attached as base-class to
     * expressions modelling a 1 (in a field, e.g., or the
     * constant-one function)
     */
    struct OneExpression
      : MPL::UniqueTags<PositiveExpression, ConstantExpression, TypedValueExpression>
    {};

    /**A tag structure which can be attached as base-class to
     * expressions modelling a 1 (in a field, e.g., or the
     * constant-one function)
     */
    struct MinusOneExpression
      : MPL::UniqueTags<NegativeExpression, ConstantExpression, TypedValueExpression>
    {};

    /**@name TagUtilities
     * @
     */

    /**Evaluate to std::true_type if std::decay_t<A> is derived from
     * Tag, otherwise to std::false_type.
     */
    template<class A, class Tag>
    using HasTag = std::is_base_of<Tag, std::decay_t<A> >;

    //!@} TagUtilities

    //!@} ExpressionTags

    //!@} ExpressionTraits

    //!@} ExpressionTemplates

  } // ACFem

} // Dune

#endif // __DUNE_ACFEM_EXPRESSIONS_TAGS_HH__
