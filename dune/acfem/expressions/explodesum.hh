#ifndef __DUNE_ACFEM_EXPRESSIONS_EXPLODESUM_HH_
#define __DUNE_ACFEM_EXPRESSIONS_EXPLODESUM_HH_

#include "optimizesums.hh"

namespace Dune::ACFem::Expressions
{

  namespace Sums
  {

    using ExplodeTag = DontOptimize;//OptimizeTag<9>;//OptimizeTag<7>;

    template<class F, class E, class TreePos, class Tuple0, class Tuple1>
    struct ExplodeProduct;

    template<class F, class E, class TreePos>
    struct ExplodeProduct<F, E, TreePos, MPL::TypeTuple<>, MPL::TypeTuple<> >
    {
      using Type = MPL::TypeTuple<>;
    };

    template<class F, class E, std::size_t... TreePos, class T10, class... T1>
    struct ExplodeProduct<F, E, TreePosition<TreePos...>, MPL::TypeTuple<>, MPL::TypeTuple<T10, T1...> >
    {
      using Data0 = TreeData<PlusFunctor, Operand<0, E>, TreePosition<TreePos..., 0> >;
      using Type = MPL::TypeTuple<
        OperationPair<typename T10::Sign, F, Data0, T10, ExplodeTag>,
        OperationPair<typename T1::Sign, F, Data0, T1, ExplodeTag>...
        >;
    };

    template<class F, class E, std::size_t... TreePos, class T00, class... T0>
    struct ExplodeProduct<F, E, TreePosition<TreePos...>, MPL::TypeTuple<T00, T0...>, MPL::TypeTuple<> >
    {
      using Data1 = TreeData<PlusFunctor, Operand<1, E>, TreePosition<TreePos..., 1> >;
      using Type = MPL::TypeTuple<
        OperationPair<typename T00::Sign, F, T00, Data1, ExplodeTag>,
        OperationPair<typename T0::Sign, F, T0, Data1, ExplodeTag>...
        >;
    };

    template<class F, class E, class TreePos, class T00, class... T1>
    struct ExplodeProduct<F, E, TreePos, MPL::TypeTuple<T00>, MPL::TypeTuple<T1...> >
    {
      using Type = MPL::TypeTuple<
        OperationPair<NestedSumFunctor<typename T00::Sign, typename T1::Sign>, F, T00, T1, ExplodeTag>...
        >;
    };

    template<class F, class E, class TreePos, class T00, class T01, class... T0, class... T1>
    struct ExplodeProduct<F, E, TreePos, MPL::TypeTuple<T00, T01, T0...>, MPL::TypeTuple<T1...> >
    {
      using Type = MPL::TypeTupleCat<
        typename ExplodeProduct<F, E, TreePos, MPL::TypeTuple<T01, T0...>, MPL::TypeTuple<T1...> >::Type,
        MPL::TypeTuple<
          OperationPair<NestedSumFunctor<typename T00::Sign, typename T1::Sign>, F, T00, T1, ExplodeTag>...
          >
        >;
    };

    //!@internal
    template<class F, class E, class...>
    struct ProcessExplodeHelper;

    template<class F, class E, class TreePos, class Tuple0, class Tuple1>
    struct ProcessExplodeHelper<F, E, TreePos, Tuple0, Tuple1>
    {
      static_assert(FunctorHas<IsProductOperation, F>::value, "");

      using Type = typename ExplodeProduct<F, E, TreePos, Tuple0, Tuple1>::Type;
    };

    template<class E, class TreePos, class T0, class... TRest>
    struct ProcessExplodeHelper<SquareFunctor, E, TreePos, MPL::TypeTuple<T0, TRest...> >
    {
      using Type = typename ExplodeProduct<typename SquareTraits<T0>::ExplodeFunctor, E, TreePos, MPL::TypeTuple<T0, TRest...>, MPL::TypeTuple<T0, TRest...> >::Type;
    };

    template<class E, class TreePos, class... T0, class... T1>
    struct ProcessExplodeHelper<PlusFunctor, E, TreePos, MPL::TypeTuple<T0...>, MPL::TypeTuple<T1...> >
    {
      using Type = MPL::TypeTuple<T0..., T1...>;
    };

    template<class E, class TreePos, class... T0, class... T1>
    struct ProcessExplodeHelper<MinusFunctor, E, TreePos, MPL::TypeTuple<T0...>, MPL::TypeTuple<T1...> >
    {
      using Type = MPL::TypeTuple<
        T0...,
        MergeTreeNodeSign<MinusFunctor, T1>...
        >;
    };

    template<class E, class TreePos, class... T0>
    struct ProcessExplodeHelper<MinusFunctor, E, TreePos, MPL::TypeTuple<T0...> >
    {
      using Type = MPL::TypeTuple<MergeTreeNodeSign<MinusFunctor, T0>...>;
    };

    //!@internal Process the results found at lower level
    template<class E, class TreePos, class... T>
    using ProcessExplode = typename ProcessExplodeHelper<Functor<E>, E, TreePos, T...>::Type;

    //!@internal Ignore irrelevant expressions, only recurse into sums and products.
    template<class E>
    using ExplodeIgnore = BoolConstant<!(IsPlusOrMinusOperation<Operation<E> >::value || IsProductExpression<E>::value || IsSquareExpression<E>::value)>;

    //!@internal Identify a terminal operand (recursion stops)
    template<class E, class Parent>
    struct IsExplodeOperand
      : BoolConstant<!ExplodeIgnore<Parent>::value && ExplodeIgnore<E>::value>
    {};

    template<class E>
    struct IsExplodeOperand<E, void>
      : BoolConstant<ExplodeIgnore<E>::value>
    {};

    //!@internal Pack tree-position, type and resulting type into a "result node".
    template<class T, class TreePos, class Parent>
    using ExplodePackIt = TreeData<PlusFunctor, T, TreePos>;

    //!Explode a factorized sum.
    template<class Expr, class TreePos = TreePosition<>, template<class E, class Pos, class...> class Process = ProcessExplode>
    using Explode = Expressions::TreeExtract<IsExplodeOperand, Expr, ExplodePackIt, TreePos, ExplodeIgnore, Process>;

  } // Sums::

} // Dune::ACFem::Expressions::

#endif //  __DUNE_ACFEM_EXPRESSIONS_EXPLODESUM_HH_
