#ifndef __DUNE_ACFEM_EXPRESSION_WEIGHT_HH__
#define __DUNE_ACFEM_EXPRESSION_WEIGHT_HH__

#include "../common/types.hh"
#include "policy.hh"

namespace Dune::ACFem::Expressions
{

  /**Provide an overridable weight for expressions which defaults to
   * their depth. See ExpressionWeight in examineutil.hh.
   */
  template<class T, class SFINAE = void>
  constexpr inline std::size_t WeightV = Policy::WeightDefaultV;

}

#endif // __DUNE_ACFEM_EXPRESSION_WEIGHT_HH__
