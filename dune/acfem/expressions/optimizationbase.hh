#ifndef __DUNE_ACFEM_EXPRESSIONS_OPTIMIZATIONBASE_HH__
#define __DUNE_ACFEM_EXPRESSIONS_OPTIMIZATIONBASE_HH__

#include "policy.hh"
#include "storage.hh"
#include "operationtraits.hh"
#include "optimizationprofile.hh"

namespace Dune
{

  namespace ACFem
  {

    namespace Expressions
    {

      /**@addtogroup ExpressionTemplates
       *
       * @{
       *
       */

      /**@addtogroup ExpressionOptimization
       *
       * General optimization patterns.
       *
       * @{
       */

      /**Optimization pattern disambiguation struct. The first
       * parameter to an operate() optimization pattern is an
       * OptimizeTag. Higher "level" OptimizeTag's inherit from
       * lower-level OptimizeTag's such that finally the
       * optimization-pattern with the highest OptimizeTag will win;
       * if a high level method is not implemented, the compiler will
       * recurse to lower-level optimization patterns.
       */
      template<std::size_t N>
      struct OptimizeTag
        : OptimizeTag<N-1>
      {
        static constexpr std::size_t level_ = N;
      };

      template<>
      struct OptimizeTag<0>
      {
        static constexpr std::size_t level_ = 0;
      };

      template<class Tag, std::size_t Incr = 1>
      using OptimizeNext = OptimizeTag<Tag::level_ - Incr>;

      template<class Tag, std::size_t Incr = 1>
      using OptimizePrior = OptimizeTag<Tag::level_ + Incr>;

      /**The top-level optmization tag.*/
      using OptimizeTop = OptimizeTag<Policy::OptimizationLevelMax::value>;
      using Optimize2nd = OptimizeTag<Policy::OptimizationLevelMax::value-1>;
      using Optimize3rd = OptimizeTag<Policy::OptimizationLevelMax::value-2>;

      /**Using this tag at the top-level is reserved in order to avoid
       * infinite recursions. optimize(T&&, OptimizeTop) can be
       * specialized to do nothing for known optimization blockers
       * like sub-expression expressions.
       */
      //using OptimizeBlock = OptimizeTop;

      /**Last optimization level.*/
      using OptimizeLast = OptimizeTag<1>;

      /**Bottom level is overloaded to do nothing.*/
      using DontOptimize = OptimizeTag<0>;

/*

Terminal
Einsum
Distributivity
Factorize
AllAll

1->2, Terminal
2->1  ScalarEinsum, *
  Optimizations are tried with OptimizeTop
3->5  DistributiveOperation *
  f() is called with OptimizeTop

AllAll
Explode


4->5  Factorize. +
  Does a full explode and computes a "good" factorization.

  Final step of sumUp: should avoid factorize.
5->6  SumAllAll, +

  Must avoid to recurse into Factorize. ScalarEinsum and
  Distributivity optimization do not play a role

  Final step of sumUp: Could recurse into Factorize, if so:
  Explode-Tag must come afterwards

6->7  Terminal again?
7->8  generic
8...1 Other
0     Just operate

*/

      using OptimizeNormalize = OptimizeTop;
      using OptimizeNormalize1 = OptimizeNormalize;
      using OptimizeNormalize0 = OptimizeNext<OptimizeNormalize1>;
      using OptimizeGap1 = OptimizeNext<OptimizeNormalize>;
      using OptimizeExpensive1 = OptimizeNext<OptimizeGap1>;
      using OptimizeGap2 = OptimizeNext<OptimizeExpensive1>;
      using OptimizeOrdinary = OptimizeNext<OptimizeGap2>;
      ////
      using OptimizeTerminal  = OptimizeOrdinary;
      using OptimizeTerminal3 = OptimizeTerminal;
      using OptimizeTerminal2 = OptimizeNext<OptimizeTerminal3>;
      using OptimizeTerminal1 = OptimizeNext<OptimizeTerminal2>;
      using OptimizeTerminal0 = OptimizeNext<OptimizeTerminal1>;
      ///
      using OptimizeGeneric = OptimizeNext<OptimizeTerminal0>;
      using OptimizeFurther = OptimizeNext<OptimizeGeneric>;

      template<std::size_t N>
      constexpr auto optimizeNext(OptimizeTag<N> = OptimizeTag<N>{})
      {
        return OptimizeTag<N-1>{};
      }

      /**Default is not to optimize. Optimization functions must be
       * implemented with a higher OptimizeTag to ease disambiguation.
       * Note that lower optimization levels always match, so
       * optimizability cannot be checked for by testing the presence
       * of the optimize() function.
       */
#if 0
      template<class F, class... T, std::enable_if_t<IsFunctor<F>::value, int> = 0>
      constexpr decltype(auto) operate(DontOptimize, F&& f, T&&... t)
      {
        static_assert(IsFunctor<F>::value, "F is not an expression functor.");
        return std::forward<F>(f)(std::forward<T>(t)...);
      }

      template<class Operation, class... T>
      constexpr decltype(auto) operate(DontOptimize, T&&... t)
      {
        return F<Operation>{}(std::forward<T>(t)...);
      }
#endif

      template<class T>
      constexpr decltype(auto) operate(DontOptimize, OperationTraits<IdentityOperation>, T&& t)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        return forwardReturnValue<T>(t);
      }

      template<class T>
      constexpr decltype(auto) operate(DontOptimize, OperationTraits<PlusOperation>, T&& t)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        return forwardReturnValue<T>(t);
      }

      /**Possibly optimize the given expression template by recursing
       * into optimize(T, Tag) with the highest supported level.
       */
      template<class F, class... T, std::enable_if_t<IsFunctor<F>::value, int> = 0>
      constexpr decltype(auto) operate(F&& f, T&&... t)
      {
#if DUNE_ACFEM_TRACE_OPTIMIZATION
        std::clog << "<<<<<<<< OptmizeTop <<<<<<<<<" << std::endl;
#endif
        return operate(OptimizeTop{}, std::forward<F>(f), std::forward<T>(t)...);
      }

      template<class Operation, class... T>
      constexpr decltype(auto) operate(T&&... t)
      {
#if DUNE_ACFEM_TRACE_OPTIMIZATION
        std::clog << "<<<<<<<< OptmizeTop <<<<<<<<<" << std::endl;
#endif
        return operate(OptimizeTop{}, F<Operation>{}, std::forward<T>(t)...);
      }

      namespace {

        template<class Optimize, class T, std::size_t... I>
        constexpr decltype(auto) evaluateExpander(Optimize, T&& t, IndexSequence<I...>)
        {
          return operate(Optimize{}, std::forward<T>(t).operation(), std::forward<T>(t).template operand<I>()...);
        }

      } // anonymous::

      /**Evaluate an expression or expression Expressions::Storage container.*/
      template<class Optimize, class T>
      constexpr decltype(auto) evaluate(Optimize, T&& t)
      {
        return evaluateExpander(Optimize{}, std::forward<T>(t), MakeIndexSequence<arity<T>()>{});
      }

      /**Evaluate an expression or expression Expressions::Storage container.*/
      template<class T>
      constexpr decltype(auto) evaluate(T&& t)
      {
        return evaluate(OptimizeTop{}, std::forward<T>(t));
      }

      template<std::size_t N, class F, class... T>
      constexpr decltype(auto) finalize(OptimizeTag<N>, F&& f, T&&... t)
      {
        return expressionClosure(
          operate(
            OptimizeTag<N>{}, std::forward<F>(f), std::forward<T>(t)...
            )
          );
      }

      template<class F, class... T,
               std::enable_if_t<IsFunctor<F>::value, int> = 0>
      constexpr decltype(auto) finalize(F&& f, T&&... t)
      {
        return expressionClosure(
          operate(
            std::forward<F>(f), std::forward<T>(t)...
            )
          );
      }

      template<class Operation, class... T>
      constexpr decltype(auto) finalize(T&&... t)
      {
        return expressionClosure(
          operate<Operation>(std::forward<T>(t)...)
          );
      }

      /**Generate the type of an expression by calling operate().
       *
       * @param OptOrF Either an operation functor or the optimization level.
       *
       * @param Rest Remaining parameters. If OptOrF is an optimization level the pack must
       * start with an expression functor.
       */
      template<class OptOrF, class... Rest>
      using ExpressionType =
        decltype(operate(std::declval<OptOrF>(), std::declval<Rest>()...));

      /**(Re-)evaluate a given expression or Expressions::Storage.*/
      template<class T>
      using EvaluatedType =
        decltype(evaluate(std::declval<T>()));

      //! @} ExpressionOptimization

      //! @} ExpressionTemplates

    } // Expressions

    using Expressions::ExpressionType;

  } // ACFem

} // Dune

#endif // __DUNE_ACFEM_EXPRESSIONS_OPTIMIZATIONBASE_HH__
