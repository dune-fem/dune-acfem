#ifndef __DUNE_ACFEM_EXPRESSIONS_COMPLEXITY_HH__
#define __DUNE_ACFEM_EXPRESSIONS_COMPLEXITY_HH__

#include "../mpl/foreach.hh"
#include "examineutil.hh"
#include "optimizationbase.hh"

namespace Dune
{
  namespace ACFem
  {
    namespace Expressions
    {

      template<class T, class SFINAE = void>
      constexpr inline std::size_t ComplexityV =
        (!IsExpression<T>::value || IsSelfExpression<T>::value
         ? 1UL
         : addLoop<Arity<T>::value>([](auto i) { return ComplexityV<Operand<decltype(i)::value, T> >; }, 1UL));

      /**The complexity of an expression is simply the number of nodes
       * in the expression tree, where a node is either a terminal or
       * an operation on other nodes.
       */
      template<class E>
      constexpr std::size_t complexity()
      {
        return ComplexityV<E>;
      }

      //!@copydoc complexitiy().
      template<class E>
      constexpr std::size_t complexity(E&& e)
      {
        return complexity<E>();
      }


      template<class F, class... T, std::enable_if_t<IsFunctor<F>::value, int> = 0>
      constexpr std::size_t complexity()
      {
        return (1UL + ... + complexity<T>());
      }

      template<class F, class... T, std::enable_if_t<IsFunctor<F>::value, int> = 0>
      constexpr std::size_t complexity(F&& f, T&&...)
      {
        return complexity<F, T...>();
      }

      template<class F, class... T>
      constexpr bool hasReducedComplexity()
      {
        return complexity<F, T...>() > complexity<ExpressionType<F, T...> >();
      }

      template<class F, class... T>
      constexpr bool hasReducedComplexity(F&&, T&&...)
      {
        return hasReducedComplexity<F, T...>();
      }

    } // NS Expressions

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_EXPRESSIONS_COMPLEXITY_HH__
