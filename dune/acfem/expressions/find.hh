#ifndef __DUNE_ACFEM_EXPRESSIONS_FIND_HH__
#define __DUNE_ACFEM_EXPRESSIONS_FIND_HH__

#include "treeextract.hh"

namespace Dune::ACFem {

  namespace Expressions {

    namespace impl {

      template<template<class...> class Predicate,
               class Expr, class TreePos, template<class...> class Ignore,
               class SFINAE = void>
      struct TreeFindHelper;

      template<template<class...> class Predicate,
               class Expr, class TreePos, template<class...> class Ignore,
               std::size_t ArgIdx, std::size_t NArgs, class SFINAE = void>
      struct TreeFindIterator;

      template<template<class...> class Predicate,
               class Expr, std::size_t... TreePos, template<class...> class Ignore,
               std::size_t ArgIdx, std::size_t NArgs>
      struct TreeFindIterator<Predicate, Expr, TreePosition<TreePos...>, Ignore, ArgIdx, NArgs>
      {
        using Type = typename TreeFindHelper<Predicate, Operand<ArgIdx, Expr>, TreePosition<TreePos..., ArgIdx>, Ignore>::Type;
      };

      // Recursion end-point, didn't find anything.
      template<template<class...> class Predicate,
               class T, class TreePos, template<class...> class Ignore,
               std::size_t NArgs>
      struct TreeFindIterator<Predicate, T, TreePos, Ignore, NArgs, NArgs>
      {
        using Type = FalseType;
      };

      template<template<class...> class Predicate,
               class Expr, class TreePos, template<class...> class Ignore,
               std::size_t ArgIdx, std::size_t NArgs>
      struct TreeFindIterator<Predicate, Expr, TreePos, Ignore, ArgIdx, NArgs,
                              std::enable_if_t<std::is_same<FalseType, typename TreeFindHelper<Predicate, Expr, TreePos, Ignore>::Type>::value> >
      {
        using Type = typename TreeFindIterator<Predicate, Expr, TreePos, Ignore, ArgIdx+1, NArgs>::Type;
      };

      template<template<class...> class Predicate, class Expr, class TreePos, template<class...> class Ignore>
      struct TreeFindHelper<Predicate, Expr, TreePos, Ignore, std::enable_if_t<Predicate<Expr>::value> >
      {
        using Type = MPL::TypePair<Expr, TreePos>;
      };

      template<template<class...> class Predicate, class Expr, class TreePos, template<class...> class Ignore>
      struct TreeFindHelper<Predicate, Expr, TreePos, Ignore, std::enable_if_t<!Predicate<Expr>::value && !Ignore<Expr>::value> >
      {
        using Type = typename TreeFindIterator<Predicate, Expr, TreePos, Ignore, 0, Arity<Expr>::value>::Type;
      };

      template<template<class...> class Predicate, class Expr, class TreePos, template<class...> class Ignore>
      struct TreeFindHelper<Predicate, Expr, TreePos, Ignore, std::enable_if_t<!Predicate<Expr>::value && Ignore<Expr>::value> >
      {
        using Type = FalseType;
      };

    }

    template<template<class...> class Predicate, class Expr, class TreePos = TreePosition<>, template<class...> class Ignore = IsSelfExpression>
    using TreeFind = typename impl::TreeFindHelper<Predicate, Expr, TreePos, Ignore>::Type;

  } // Expressions::

} // Dune::ACFem::

#endif // __DUNE_ACFEM_EXPRESSIONS_FIND_HH__
