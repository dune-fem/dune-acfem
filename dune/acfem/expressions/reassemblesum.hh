#ifndef __DUNE_ACFEM_EXPRESSIONS_REASSEMBLESUM_HH__
#define __DUNE_ACFEM_EXPRESSIONS_REASSEMBLESUM_HH__

#include "explodesum.hh"

namespace Dune::ACFem::Expressions {

  namespace Sums {

    //!@internal@copydoc AddNodes.
    template<class OptimizeTag, class Left, class Right, class SFINAE = void>
    struct AddNodesHelper
    {
      using LF = typename Left::Sign;
      using RF = typename Right::Sign;
      using Functor = NestedSumFunctor<LF, RF>;
      using Type = OperationPair<LF, Functor, Left, Right, OptimizeTag>;
    };

    //!@internal@copydoc AddNodes.
    template<class OptimizeTag, class Left, class Right>
    struct AddNodesHelper<OptimizeTag, Left, Right,
                          std::enable_if_t<FunctorHas<IsMinusOperation, typename Left::Sign>::value> >
    {
      using LF = typename Left::Sign;
      using RF = typename Right::Sign;
      using Functor = NestedSumFunctor<LF, RF>;
      using Type = OperationPair<RF, Functor, Right, Left, OptimizeTag>;
    };

    /**@internal Combine two nodes with a sign, minimizing the number
     * of unary minus operations.
     */
    template<class OptimizeTag, class Left, class Right>
    using AddNodes = typename AddNodesHelper<OptimizeTag, Left, Right>::Type;

    template<class OptimizeTag, class Pos, class Min, class TryNode, class Candidates, class SFINAE = void>
    struct MinimizeOneHelper;

    // Start the search
    template<class OptimizeTag, class TryNode, class Cand0, class... CandRest>
    struct MinimizeOneHelper<OptimizeTag, TreePosition<0, 1>, void, TryNode, MPL::TypeTuple<Cand0, CandRest...> >
    {
      using Type = typename MinimizeOneHelper<OptimizeTag, TreePosition<0, 2>, MPL::TypePair<TreePosition<0, 1>, AddNodes<OptimizeTag, TryNode, Cand0> >, TryNode, MPL::TypeTuple<CandRest...> >::Type;
    };

    // replace current minimum
    template<class OptimizeTag, std::size_t Row, std::size_t Col, class ArgMin, class MinNode, class TryNode, class Cand0, class... CandRest>
    struct MinimizeOneHelper<OptimizeTag, TreePosition<Row, Col>, MPL::TypePair<ArgMin, MinNode>, TryNode, MPL::TypeTuple<Cand0, CandRest...>,
                             std::enable_if_t<(MinNode::complexity > AddNodes<OptimizeTag, TryNode, Cand0>::complexity)> >
    {
      using Type = typename MinimizeOneHelper<OptimizeTag, TreePosition<Row, Col+1>, MPL::TypePair<TreePosition<Row, Col>, AddNodes<OptimizeTag, TryNode, Cand0> >, TryNode, MPL::TypeTuple<CandRest...> >::Type;
    };

    // Keep current minimum
    template<class OptimizeTag, std::size_t Row, std::size_t Col, class Min, class TryNode, class Cand0, class... CandRest>
    struct MinimizeOneHelper<OptimizeTag, TreePosition<Row, Col>, Min, TryNode, MPL::TypeTuple<Cand0, CandRest...>,
                             std::enable_if_t<(Min::Second::complexity <= AddNodes<OptimizeTag, TryNode, Cand0>::complexity)> >
    {
      using Type = typename MinimizeOneHelper<OptimizeTag, TreePosition<Row, Col+1>, Min, TryNode, MPL::TypeTuple<CandRest...> >::Type;
    };

    // Recursion end-point.
    template<class OptimizeTag, class Pos, class Min, class TryNode>
    struct MinimizeOneHelper<OptimizeTag, Pos, Min, TryNode, MPL::TypeTuple<> >
    {
      using Type = Min;
    };

    //!@internal Find the least complexity candidate of one operand with all the others.
    template<class OptimizeTag, std::size_t N, class Min, class Try, class Rest>
    using MinimizeOne = typename MinimizeOneHelper<OptimizeTag, TreePosition<N, N+1>, Min, Try, Rest>::Type;

    template<class OptimizeTag, std::size_t N, class Min, class Candidates>
    struct MinimizeAllHelper;

    template<class OptimizeTag, std::size_t N, class Min, class Front, class... Rest>
    struct MinimizeAllHelper<OptimizeTag, N, Min, MPL::TypeTuple<Front, Rest...> >
    {
      using Type = typename MinimizeAllHelper<OptimizeTag, N+1, MinimizeOne<OptimizeTag, N, Min, Front, MPL::TypeTuple<Rest...> >, MPL::TypeTuple<Rest...> >::Type;
    };

    template<class OptimizeTag, std::size_t N, class Min>
    struct MinimizeAllHelper<OptimizeTag, N, Min, MPL::TypeTuple<> >
    {
      using Type = Min;
    };

    //!@internal Find the least-complex sum of two distinct operands
    template<class OptimizeTag, class Explode>
    using MinimizeAll = typename MinimizeAllHelper<OptimizeTag, 0, void, Explode>::Type;

    /**@internal After finding one "global minimizer" replace remove
     * the corresponding operands from the explode-tuple, add the
     * minimum at end and start over until only one element -- the
     * final sum -- remains.
     */
    template<class Min, class Explode, class Out = MPL::TypeTuple<>, std::size_t = 0, class SFINAE = void>
    struct ReplaceByMinimum;

    template<std::size_t N, std::size_t LeftPos, std::size_t RightPos, class MinNode, class Head, class... Rest, class... Out>
    struct ReplaceByMinimum<MPL::TypePair<TreePosition<LeftPos, RightPos>, MinNode>, MPL::TypeTuple<Head, Rest...>, MPL::TypeTuple<Out...>, N, std::enable_if_t<(LeftPos != N && RightPos != N)> >
    {
      using Type = typename ReplaceByMinimum<MPL::TypePair<TreePosition<LeftPos, RightPos>, MinNode>, MPL::TypeTuple<Rest...>, MPL::TypeTuple<Out..., Head>, N+1>::Type;
    };

    template<std::size_t N, std::size_t RightPos, class MinNode, class Head, class... Rest, class Out>
    struct ReplaceByMinimum<MPL::TypePair<TreePosition<N, RightPos>, MinNode>, MPL::TypeTuple<Head, Rest...>, Out, N>
    {
      using Type = typename ReplaceByMinimum<MPL::TypePair<TreePosition<N, RightPos>, MinNode>, MPL::TypeTuple<Rest...>, Out, N+1>::Type;
    };

    template<std::size_t N, std::size_t LeftPos, class MinNode, class Head, class... Rest, class Out>
    struct ReplaceByMinimum<MPL::TypePair<TreePosition<LeftPos, N>, MinNode>, MPL::TypeTuple<Head, Rest...>, Out, N>
    {
      using Type = typename ReplaceByMinimum<MPL::TypePair<TreePosition<LeftPos, N>, MinNode>, MPL::TypeTuple<Rest...>, Out, N+1>::Type;
    };

    template<std::size_t N, class MinPos, class MinNode, class... Out>
    struct ReplaceByMinimum<MPL::TypePair<MinPos, MinNode>, MPL::TypeTuple<>, MPL::TypeTuple<Out...>, N>
    {
      using Type = MPL::TypeTuple<Out..., MinNode>;
    };

    template<class OptimizeTag, class Explode>
    struct ReassembleHelper
    {
      using Type = typename ReassembleHelper<OptimizeTag, typename ReplaceByMinimum<MinimizeAll<OptimizeTag, Explode>, Explode>::Type>::Type;
    };

    template<class OptimizeTag, class Sum>
    struct ReassembleHelper<OptimizeTag, MPL::TypeTuple<Sum> >
    {
      using Type = Sum;
    };

    /**Reassemble an exploded sum as generated by Explode by pair-wise
     * operations of least complexity.
     */
    template<class Explode, class OptimizeTag = OptimizeTop>
    using Reassemble = typename ReassembleHelper<OptimizeTag, Explode>::Type;

  } // Sums::

} // Dune::ACFem::Expressions::

#endif // __DUNE_ACFEM_EXPRESSIONS_REASSEMBLESUM_HH__
