#ifndef __DUNE_ACFEM_EXPRESSIONS_EXAMINEUTIL_HH__
#define __DUNE_ACFEM_EXPRESSIONS_EXAMINEUTIL_HH__

#include "../mpl/accumulate.hh"
#include "examine.hh"
#include "runtimeequal.hh"
#include "weight.hh"

namespace Dune
{
  namespace ACFem
  {
    namespace Expressions
    {

      /**Check whether the expression contains the given type.
       *
       * @param E The expression to examine (NOTA BENE, this is not T)
       *
       * @param T The type to check for.
       *
       * @return If @a T is contained in @a E then instantiate to
       * ACFem::TrueType, otherwise to ACFem::FalseType.
       */
      template<class E, class T>
      using ContainsType = ExamineOr<E, std::is_same, typename ClosureTraits<T>::ExpressionType>;

      /**Check whether the expression contains the given type up to decay
       * of both types.
       *
       * @param E The expression to examine (NOTA BENE, this is not T)
       *
       * @param T The type to check for.
       *
       * @return If std::decay_t<T> is equal to std::decay_t of any of the
       * operands contained in E or E itself evaluate to ACFem::TrueType,
       * otherwise to ACFem::FalseType.
       */
      template<class E, class T>
      using ContainsDecayed = ExamineOr<E, SameDecay, typename ClosureTraits<T>::ExpressionType>;

      template<class E>
      using NumberOfTerminals = ExamineAdd<E, IsSelfExpression>;

      template<class E>
      constexpr inline std::size_t numberOfTerminals()
      {
        return NumberOfTerminals<E>::value;
      }

      template<class E>
      constexpr inline std::size_t numberOfTerminals(E&&)
      {
        return numberOfTerminals<E>();
      }

      /**Obtain the number of times T is contained in E, up to the
       * ACFem::Expressions::AreRuntimeEqual predicate.
       *
       * @param E The expression to examine.
       *
       * @param T The type to compare to.
       *
       * @return An integral constant wrapping the number of times T is
       * contained in E provided it qualifies as
       * ACFem::Expressions::IsRuntimeEqual. If @a T does not qualify
       * for IsRuntimeEqual, then the generated multiplicity is 0,
       * regardless whether any flavours (decayed or not) of T are
       * contained in E.
       */
      template<class E, class T>
      using SubExpressionMultiplicity = ExamineAdd<EnclosedType<E>, AreRuntimeEqual, EnclosedType<T> >;

      /**Obtain the sub-expression multiplicity as constexpr function.*/
      template<class E, class T>
      constexpr auto subExpressionMultiplicity(E&& e = E{}, T&& t = T{})
      {
        return SubExpressionMultiplicity<E, T>::value;
      }

      namespace ExamineImpl
      {
        /**@internal Helper in order to determine depth of expression.*/
        template<class Dummy, class ArgDepths>
        struct DepthFunctor
          : IndexConstant<1+ArgDepths::value>
        {};
      }

      /**Obtain the depth of E as integral constant.*/
      template<class E>
      using Depth = ExamineRecurse<E, std::size_t, Max, ExamineImpl::DepthFunctor>;

      /**Obtain the depth of E as constexpr.*/
      template<class E>
      constexpr std::size_t depth()
      {
        return Depth<E>::value;
      }

      /**Obtain the depth of E as constexpr function.*/
      template<class E>
      constexpr auto depth(E&& e)
      {
        return depth<E>();
      }

      template<class T, class SFINAE = void>
      constexpr inline std::size_t ExpressionWeightV = addLoop<Arity<T>::value>([](auto i) { return ExpressionWeightV<Operand<decltype(i)::value, T> >; }, WeightV<T>);

      template<class T>
      constexpr inline std::size_t ExpressionWeightV<T, std::enable_if_t<IsSelfExpression<T>::value> > = WeightV<T>;

      /**Obtain the weight of E as constexpr function.*/
      template<class E>
      constexpr auto weight()
      {
        return ExpressionWeightV<E>;
      }

      /**Obtain the weight of E as constexpr function.*/
      template<class E>
      constexpr auto weight(E&&)
      {
        return weight<E>();
      }

      /**Yields std::true_type if the expression @a E explicitly depends
       * on an indeterminate with id @a Id.
       */
      template<std::size_t Id, class E>
      using ExplicitlyDependsOn = ExamineOr<E, IndeterminateMatch, IndexConstant<Id> >;

    } // NS Expressions

    using Expressions::ExpressionWeightV;

  } // NS ACFem
} // NS Dune

#endif // __DUNE_ACFEM_EXPRESSIONS_EXAMINEUTIL_HH__
