#ifndef __DUNE_ACFEM_EXPRESSIONS_EXTRACT_HH__
#define __DUNE_ACFEM_EXPRESSIONS_EXTRACT_HH__

#include "transform.hh"
#include "examine.hh"
#include "../common/typetraits.hh"
#include "../common/literals.hh"
#include "../mpl/conditionaljoin.hh"

namespace Dune::ACFem::Expressions {

  using namespace Literals;

  /**Default pack-data functor.*/
  struct ExtractDataDefaultFunctor
  {
    template<class E,  class Parent>
    constexpr decltype(auto) operator()(E&& e, NoTreePos, Parent&&) const
    {
      return std::forward<E>(e);
    }

    template<class E, class TreePos, class Parent>
    constexpr decltype(auto) operator()(E&& e, TreePos, Parent&&) const
    {
      return std::pair<E, TreePos>(std::forward<E>(e), TreePos{});
    }
  };

  using Recursive = TrueType;
  using NonRecursive = FalseType;

  namespace {


    /**@internal Add matching element to result tuple, non-recursive version.*/
    template<class E, class Extracts, class ArgIndices, class F, class Recursive, class Unique, class TreePos, class Parent, class PackData,
             std::enable_if_t<(IsTransformInvocable<F, E, TreePos, Parent>::value
                               && (!Recursive::value || IsSelfExpression<E>::value)
      ), int> = 0>
    constexpr auto extractTupleHelper(E&& e, const Extracts& t, ArgIndices, F&&, Recursive, Unique, TreePos, Parent&& parent, PackData&& packData)
    {
//      std::clog << "helper invocable non recursive" << std::endl;
//      std::clog << e.name() << std::endl;
      return TupleUtilities::addFrontUnless(
        t,
        packData(std::forward<E>(e), TreePos{}, std::forward<Parent>(parent)),
        Unique{});
    }

    /**@internal Add matching element to result tuple, recursive version.*/
    template<class E, class Extracts, std::size_t... ArgIndex, class F, class Recursive, class Unique, class TreePos, class Parent, class PackData,
             std::enable_if_t<(IsTransformInvocable<F, E, TreePos, Parent>::value
                               && !IsSelfExpression<E>::value
                               && Recursive::value
      ), int> = 0>
    constexpr auto extractTupleHelper(E&& e, const Extracts& t, IndexSequence<ArgIndex...>, F&& f, Recursive, Unique, TreePos, Parent&& parent, PackData&& packData)
    {
      if constexpr (!std::is_same<Parent, FalseType>::value) {
        return TupleUtilities::joinUnless<Unique::template Predicate>(
          TupleUtilities::addFrontUnless(t, packData(std::forward<E>(e), TreePos{}, std::forward<Parent>(parent)), Unique{}),
          extract(
            std::forward<E>(e).template operand<ArgIndex>(),
            std::forward<F>(f), Recursive{}, Unique{}, AppendTreePos<TreePos, ArgIndex>{},
            std::forward<E>(e)
            )...);
      } else {
        return TupleUtilities::joinUnless<Unique::template Predicate>(
          TupleUtilities::addFrontUnless(t, packData(std::forward<E>(e), TreePos{}, std::forward<Parent>(parent)), Unique{}),
          extract(
            std::forward<E>(e).template operand<ArgIndex>(),
            std::forward<F>(f), Recursive{}, Unique{}, AppendTreePos<TreePos, ArgIndex>{},
            FalseType{}
            )...);
      }
    }

    // ignore terminals
    template<class E, class Extracts, class ArgIndices, class F, class Recursive, class Unique, class TreePos, class Parent, class PackData,
             std::enable_if_t<(!IsTransformInvocable<F, E, TreePos, Parent>::value
                               && IsSelfExpression<E>::value
      ), int> = 0>
    constexpr auto extractTupleHelper(E&& e, const Extracts& t, ArgIndices, F&&, Recursive, Unique, TreePos, Parent&&, PackData&&)
    {
      //std::clog << "helper !invocable terminal" << std::endl;
      //std::clog << e.name() << std::endl;
      return t;
    }

    // recurse into subexpressions
    template<class E, class... P, std::size_t... ArgIndex, class F, class Recursive, class Unique,
             class TreePos, class Parent, class PackData,
             std::enable_if_t<(!IsTransformInvocable<F, E, TreePos, Parent>::value
                               && !IsSelfExpression<E>::value
               ), int> = 0>
    constexpr auto extractTupleHelper(E&& e, const std::tuple<P...>& t, IndexSequence<ArgIndex...>, F&& f, Recursive, Unique, TreePos, Parent&&, PackData&& packData)
    {
      //std::clog << "helper !invocable !terminal" << std::endl;
      //std::clog << e.name() << std::endl;
      //std::clog << typeString(f) << std::endl;
      if constexpr (!std::is_same<Parent, FalseType>::value) {
        return TupleUtilities::joinUnless<Unique::template Predicate>(
          t,
          extract(
            std::forward<E>(e).template operand<ArgIndex>(),
            std::forward<F>(f), Recursive{}, Unique{}, AppendTreePos<TreePos, ArgIndex>{},
            std::forward<E>(e),
            std::forward<PackData>(packData)
            )...
          );
      } else {
        return TupleUtilities::joinUnless<Unique::template Predicate>(
          t,
          extract(
            std::forward<E>(e).template operand<ArgIndex>(),
            std::forward<F>(f), Recursive{}, Unique{}, AppendTreePos<TreePos, ArgIndex>{},
            FalseType{},
            std::forward<PackData>(packData)
            )...
          );
      }
    }

  }

  /**Extract a tuple with all elements matching F. F is a functor
   * class which has to provide an operator() method. If the
   * F::operator()(std::declval<T>()) exists then then corresponding T
   * object is added to the tuple. Recursion stops at this point,
   * unless Recursive is TrueType in which case also sub-expressions
   * of T are in turn taken into account.
   *
   * @param e The expression to examine.
   *
   * @param f The functor with potential operator()-methods defining
   * the match. The functor may maintain state.
   *
   * @param Recursive TrueType or FalseType.
   *
   * @param Unique A binary predicate template Unique<T1,T2> which can
   * be used to filter out duplicates, e.g.
   */
  template<class E,
           class F,
           class... P,
           bool Recursive = false,
           class Unique = PredicateProxy<AlwaysFalse>,
           class TreePos = NoTreePos,
           class Parent = NoParent,
           class PackData = ExtractDataDefaultFunctor,
           std::enable_if_t<IsExpression<E>::value, int> = 0>
  constexpr auto extract(E&& e, F&& f,
                         const std::tuple<P...>& initial,
                         BoolConstant<Recursive> r = BoolConstant<Recursive>{},
                         Unique = Unique{},
                         TreePos tp = TreePos{},
                         Parent&& parent = Parent{},
                         PackData&& packData = PackData{})
  {
    if constexpr (std::is_same<TreePos, TrueType>::value) {
      using UniqueWrapper =
        ConditionalType<std::is_same<PackData, ExtractDataDefaultFunctor>::value,
                        PredicateWrapper<PredicateProxy<Unique::template Predicate>::template BinaryFirstOfPair>,
                        Unique>;
      return extractTupleHelper(std::forward<E>(e),
                                initial, MakeIndexSequence<Arity<E>::value>{}, std::forward<F>(f),
                                r, UniqueWrapper{}, IndexSequence<>{},
                                std::forward<Parent>(parent),
                                std::forward<PackData>(packData));
    } else {
      return extractTupleHelper(std::forward<E>(e),
                                initial, MakeIndexSequence<Arity<E>::value>{}, std::forward<F>(f),
                                r, Unique{}, tp,
                                std::forward<Parent>(parent),
                                std::forward<PackData>(packData));
    }
  }

  /**Variant with empty initial tuple of matches.*/
  template<class E,
           class F,
           class Recursive = FalseType,
           class Unique = PredicateWrapper<AlwaysFalse>,
           class TreePos = NoTreePos,
           class Parent = NoParent,
           class PackData = ExtractDataDefaultFunctor,
           std::enable_if_t<IsExpression<E>::value, int> = 0>
  constexpr auto extract(E&& e, F&& f,
                         Recursive r = Recursive{},
                         Unique u = Unique{},
                         TreePos tp = TreePos{},
                         Parent&& parent = Parent{},
                         PackData&& packData = PackData{})
  {
    if constexpr (std::is_same<TreePos, TrueType>::value) {
      using UniqueWrapper =
        ConditionalType<std::is_same<PackData, ExtractDataDefaultFunctor>::value,
                        PredicateWrapper<PredicateProxy<Unique::template Predicate>::template BinaryFirstOfPair>,
                        Unique>;
      return extract(std::forward<E>(e), std::forward<F>(f), std::tuple<>{},
                     r, UniqueWrapper{},
                     IndexSequence<>{},
                     std::forward<Parent>(parent),
                     std::forward<PackData>(packData));
    } else {
      return extract(std::forward<E>(e), std::forward<F>(f), std::tuple<>{},
                     r, u, tp,
                     std::forward<Parent>(parent),
                     std::forward<PackData>(packData));
    }
  }

  /**Variant of extract() where the functor is constructed from a
   * simple traits class F. If is instantiated with the type of each
   * contained expression in turn, if it a TrueType, the correpsonging
   * sub-expression is added to the tuple. The extra template argument
   * to F are just for convenience in order to allow for additional
   * default arguments.
   */
  template<template<class T, class...> class F,
           class E,
           class... P,
           bool Recursive = false,
           class Unique = PredicateWrapper<AlwaysFalse>,
           class TreePos = NoTreePos,
           class Parent = NoParent,
           class PackData = ExtractDataDefaultFunctor,
           std::enable_if_t<IsExpression<E>::value && ExamineTreeOr<E, TreePos, Parent, F>::value, int> = 0>
  constexpr auto extract(E&& e,
                         const std::tuple<P...>& initial,
                         BoolConstant<Recursive> r = BoolConstant<Recursive>{},
                         Unique u = Unique{},
                         TreePos tp = TreePos{},
                         Parent&& parent = Parent{},
                         PackData&& packData = PackData{})
  {
    // AlwaysFalse is the ignore-functor for the transform machinery
    // which would cause a match if left to the default.
    using FunctorType = SimpleReplaceFunctor<int, PredicateProxy<F>::template ForwardFirst, AlwaysFalse>;

    if constexpr (std::is_same<TreePos, TrueType>::value) {
      using UniqueWrapper =
        ConditionalType<std::is_same<PackData, ExtractDataDefaultFunctor>::value,
                        PredicateWrapper<PredicateProxy<Unique::template Predicate>::template BinaryFirstOfPair>,
                        Unique>;
      return extract(std::forward<E>(e), FunctorType{}, initial,
                     r, UniqueWrapper{},
                     IndexSequence<>{},
                     std::forward<Parent>(parent),
                     std::forward<PackData>(packData));
    } else {
      return extract(std::forward<E>(e), FunctorType{}, initial,
                     r, u, tp,
                     std::forward<Parent>(parent),
                     std::forward<PackData>(packData));
    }
  }

  /**Dummy if it is known that no subexpression of E matches F.*/
  template<template<class T, class...> class F,
           class E,
           class... P,
           bool Recursive = false,
           class Unique = PredicateWrapper<AlwaysFalse>,
           class TreePos = NoTreePos,
           class Parent = NoParent,
           class PackData = ExtractDataDefaultFunctor,
           std::enable_if_t<(IsExpression<E>::value
                             && !ExamineTreeOr<E, TreePos, Parent, F>::value
             ), int> = 0>
  constexpr auto extract(E&& e,
                         const std::tuple<P...>& initial,
                         BoolConstant<Recursive> = BoolConstant<Recursive>{},
                         Unique = Unique{},
                         TreePos = TreePos{},
                         Parent&& parent = Parent{},
                         PackData&& packData = PackData{})
  {
    //std::clog << "!examine" << std::endl;
    //std::clog << e.name() << std::endl;
    //std::clog << "exa: " << ExamineTreeOr<Operand<0, E>, TreePos, E, F>::value << std::endl;

    return initial;
  }

  /**Variant with empty initial tuple of matches.*/
  template<template<class T, class...> class F,
           class E,
           bool Recursive = false,
           class Unique = PredicateWrapper<AlwaysFalse>,
           class TreePos = NoTreePos,
           class Parent = NoParent,
           class PackData = ExtractDataDefaultFunctor,
           std::enable_if_t<IsExpression<E>::value, int> = 0>
  constexpr auto extract(E&& e,
                         BoolConstant<Recursive> r = BoolConstant<Recursive>{},
                         Unique u = Unique{},
                         TreePos tp = TreePos{},
                         Parent&& parent = Parent{},
                         PackData&& packData = PackData{})
  {
    if constexpr (std::is_same<TreePos, TrueType>::value) {
      using UniqueWrapper =
        ConditionalType<std::is_same<PackData, ExtractDataDefaultFunctor>::value,
                        PredicateWrapper<PredicateProxy<Unique::template Predicate>::template BinaryFirstOfPair>,
                        Unique>;
      return extract<F>(std::forward<E>(e), std::tuple<>{},
                        r, UniqueWrapper{},
                        IndexSequence<>{},
                        std::forward<Parent>(parent),
                        std::forward<PackData>(packData));
    } else {
      return extract<F>(std::forward<E>(e), std::tuple<>{},
                        r, u, tp,
                        std::forward<Parent>(parent),
                        std::forward<PackData>(packData));
    }
  }

  /**Extract only the left-most operand.*/
  template<template<class...> class Predicate, class E, template<class...> class DescendUntil = AlwaysFalse>
  constexpr decltype(auto) extractLeftMost(E&& e, PredicateWrapper<DescendUntil> = PredicateWrapper<DescendUntil>{})
  {
    if constexpr (IsSelfExpression<E>::value || DescendUntil<E>::value) {
      if constexpr (Predicate<E>::value) {
        return std::forward<E>(e);
      } else {
        return FalseType{};
      }
    } else {
      return extractLeftMost<Predicate>(std::forward<E>(e).operand(0_c), PredicateWrapper<DescendUntil>{});
    }
  }

  template<class E, template<class...> class Predicate = AlwaysTrue, template<class...> class DescendUntil = AlwaysFalse>
  using LeftMostOperand = decltype(extractLeftMost<Predicate>(std::declval<E>(), PredicateWrapper<DescendUntil>
    {}));

  /**Extract a tuple with references to all contained placeholders.*/
  template<class E, class Initial = std::tuple<>, template<class...> class Unique = AlwaysFalse>
  constexpr auto extractPlaceholders(E&& e,
                                     Initial&& initial = Initial{},
                                     PredicateWrapper<Unique> u = PredicateWrapper<Unique>{})
  {
    return extract<IsPlaceholderExpression>(std::forward<E>(e), std::forward<Initial>(initial), NoTreePos{}, u);
  }

  /**Extract a tuple with references to all indeterminates matching
   * the given Id. See also replaceIndeterminate().
   */
  template<std::size_t Id, class E>
  constexpr auto extractIndeterminates(E&& e)
  {
    // AlwaysFalse is the ignore-functor for the transform machinery
    // which would cause a match if left to the default.
    using FunctorType = ReplaceFunctor<IndexConstant<Id>, int, IndeterminateMatch, AlwaysFalse>;
    return extract(std::forward<E>(e), FunctorType{});
  }

  /**Extract the leaf-nodes of an expression.*/
  template<class E>
  constexpr auto extractTerminals(E&& e)
  {
    return extract<IsTerminal>(std::forward<E>(e));
  }

  template<class E>
  using TerminalOperands = decltype(extractTerminals(std::declval<E>()));

  template<class F, class... T>
  using OperationTerminals = TerminalOperands<ExpressionType<F, T...> >;

  /**Type of the return value of the simple boolean extractions.*/
  template<template<class T, class...> class F, class E, class Recursive = FalseType>
  using MatchingExpressionTuple = std::decay_t<decltype(extract<F>(std::declval<E>(), Recursive{}))>;

  /**Type of the return value of extractPlaceholders().*/
  template<class E>
  using PlaceholderTuple = std::decay_t<decltype(extractPlaceholders(std::declval<E>()))>;

  /**Type of the return value of extractIndeterminates().*/
  template<std::size_t Id, class E>
  using IndeterminateTuple = std::decay_t<decltype(extractIndeterminates<Id>(std::declval<E>()))>;

  /**Yields std::true_type if the expression @a E contains
   * placeholders.
   */
  template<class E>
  struct ContainsPlaceholders
    : BoolConstant<(size<PlaceholderTuple<E>() > > 0)>
  {};

} // NS Expressions

#endif // __DUNE_ACFEM_EXPRESSIONS_EXTRACT_HH__
