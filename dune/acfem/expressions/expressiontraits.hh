#ifndef __DUNE_ACFEM_EXPRESSIONS_EXPRESSIONTRAITS_HH__
#define __DUNE_ACFEM_EXPRESSIONS_EXPRESSIONTRAITS_HH__

/**@file
 *
 * Traits-class in order to deduce properties from expressions.
 */

#include "../common/types.hh"
#include "policy.hh"
#include "sign.hh"

namespace Dune
{

  namespace ACFem
  {

    /**@addtogroup ExpressionTemplates
     *
     * ACFem provides expression templates in order to form algebraic
     * expression for models, grid-functions and parameters.
     *
     * @{
     *
     */

    /**@addtogroup ExpressionTraits
     *
     * "Tag"-structures for each supported algebraic operation, used
     * to distinguish the various expression template classes from
     * each other.
     *
     * @{
     */

    enum TraitsPriority {
      DefaultTraitsLevel = 0
      , BaseTraitsLevel
      , DecayTraitsLevel
      , TopTraitsLevel = Expressions::Policy::TraitsLevelMax::value
      , Primary = TopTraitsLevel
      , Secondary = (int)TopTraitsLevel - 1
    };

    /**Default expression traits definition is a recursion in order to
     * ease disambiguation.
     */
    template<class T,
             class SFINAE = void,
             std::size_t Priority = TopTraitsLevel>
    struct ExpressionTraits
      : ExpressionTraits<T, void, Priority-1>
    {};

    /**A traits class in order to collect properties of
     * expressions. This is the recursion end-point if the traits class
     * has not been implemented at any "priority" level.
     *
     * @param Expression Any class.
     */
    template<class T>
    struct ExpressionTraits<T, void, DefaultTraitsLevel>
      : public UnknownExpressionSign
    {
      using ExpressionType = void;

      static constexpr bool isOne = false;
      static constexpr bool isMinusOne = false;

      static constexpr bool isVolatile = false;
      static constexpr bool isIndependent = false;
      static constexpr bool isConstant = false;
      static constexpr bool isTypedValue = false;
    };

    /**Forward to the traits class for the decay type.
     *
     * @param T Any type which is not its decay type.
     */
    template<class T>
    struct ExpressionTraits<T,
      std::enable_if_t<!IsDecay<T>::value>,
      DecayTraitsLevel>
      : public ExpressionTraits<std::decay_t<T> >
    {};

    //!Compile-time true if T is a "typed value", e.g. a std::integral_constant.
    template<class T>
    using IsTypedValue = BoolConstant<ExpressionTraits<T>::isTypedValue>;

    //!Compile-time true if T is a reference to a "typed value".
    template<class T>
    using IsTypedValueReference = BoolConstant<std::is_reference<T>::value && IsTypedValue<T>::value>;

    //!ExpressionTraits for any zero-expression.
    template<class T>
    struct ZeroExpressionTraits
    {
      using ExpressionType = std::decay<T>;
      static constexpr bool isNonZero = false;
      static constexpr bool isNonSingular = false;
      static constexpr bool isZero = true;
      static constexpr bool isOne = false;
      static constexpr bool isMinusOne = false;
      static constexpr bool isSemiPositive = true;
      static constexpr bool isSemiNegative = true;
      static constexpr bool isPostive = false;
      static constexpr bool isNegative = false;

      static constexpr bool isVolatile = false;
      static constexpr bool isIndependent = true;
      static constexpr bool isConstant = true;
      static constexpr bool isTypedValue = true;

      using Sign = ExpressionSign<isNonSingular, isSemiPositive, isSemiNegative, 0>;
    };

    /**@name SignComparisons
     *
     * Some constexpr operators to support symbolic notations.
     *
     * @{
     */

    template<class T, class SFINAE = void>
    struct OwnsExpressionTraits
      : FalseType
    {};

    template<class T>
    struct OwnsExpressionTraits<
      T,
      std::enable_if_t<(std::is_class<std::decay_t<T> >::value &&
                        SameDecay<T, typename ExpressionTraits<T>::ExpressionType>::value)> >
      : TrueType
    {};

    template<class Expression, class Int, Int I>
    constexpr std::enable_if_t<OwnsExpressionTraits<Expression>::value, bool>
    operator<=(const Expression&, Constant<Int, I>)
    {
      return ExpressionTraits<Expression>::isSemiNegative && I >= 0;
    }

    template<class Expression, class Int, Int I>
    constexpr std::enable_if_t<OwnsExpressionTraits<Expression>::value, bool>
    operator>=(const Expression&, Constant<Int, I>)
    {
      return ExpressionTraits<Expression>::isSemiPositive && I <= 0;
    }

    template<class Expression, class Int, Int I>
    constexpr std::enable_if_t<OwnsExpressionTraits<Expression>::value, bool>
    operator<(const Expression&, Constant<Int, I>)
    {
      return ExpressionTraits<Expression>::isNegative && I >= 0;
    }

    template<class Expression, class Int, Int I>
    constexpr std::enable_if_t<OwnsExpressionTraits<Expression>::value, bool>
    operator>(const Expression&, Constant<Int, I>)
    {
      return ExpressionTraits<Expression>::isPositive && I <= 0;
    }

    template<class Expression, class Int, Int I>
    constexpr std::enable_if_t<OwnsExpressionTraits<Expression>::value, bool>
    operator==(const Expression&, Constant<Int, I>)
    {
      return ExpressionTraits<Expression>::isZero && I == 0;
    }

    template<class Expression, class Int, Int I>
    constexpr std::enable_if_t<OwnsExpressionTraits<Expression>::value, bool>
    operator!=(const Expression&, Constant<Int, I>)
    {
      return ExpressionTraits<Expression>::isZero && I != 0;
    }

    template<class T>
    constexpr auto expressionTraits()
    {
      return ExpressionTraits<T>{};
    }

    template<class T>
    constexpr auto expressionTraits(T&&)
    {
      return expressionTraits<T>();
    }

    //!@} SignComparisons

    //!@} ExpressionTraits

    //! @} ExpressionTemplates

  } // ACFem

} // Dune

#endif // __DUNE_ACFEM_EXPRESSIONS_EXPRESSIONTRAITS_HH__
