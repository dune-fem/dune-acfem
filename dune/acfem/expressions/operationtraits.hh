#ifndef __DUNE_ACFEM_EXPRESSIONS_OPERATIONTRAITS_HH__
#define __DUNE_ACFEM_EXPRESSIONS_OPERATIONTRAITS_HH__

/**@file
 *
 * A list of supported expression operations.
 */

#include "../common/ostream.hh"
#include "../common/literals.hh"
#include "../mpl/tostring.hh"
#include "../mpl/foreach.hh"

#include "expressiontraits.hh"
#include "constantoperations.hh"
#include "expressionoperations.hh"

namespace Dune
{

  namespace ACFem
  {

    // inject math-functions into ACFem::
#include "usingstd.hh"

    /**@addtogroup ExpressionTemplates
     *
     * @{
     *
     */

    /**@addtogroup ExpressionResults
     *
     * Helper structures for expression templates which aid in forming
     * an (optimized) result types. While forming expression templates
     * there are several challenges:
     *
     * - perform the usual no-op optimizations (-(-arg) == arg), avoid
     *   double wrapping into IdentityOperations). Optimize for no-ops
     *   resulting from adding zeros are multiplying with zeros (see
     *   ZeroExpression, ExpressionTraits).
     *
     * - deduce the point-wise return type, keeping sclar product and
     *   the like into account (see UnaryExpressionTraits,
     *   BinaryExpressionTraits)
     *
     * - provide means to make use of optimized operator-functions in
     *   the definition of expression templates for complicated
     *   compound-object expressions (see ModelExpression and
     *   ExpressionResult).
     *
     * - actually form those complicated-optimized expressions by
     *   simply calling the operator-functions, but in a consistent
     *   manner, see ExpressionResult.
     *
     *
     * @{
     */

    // forward
    template<class Operation>
    struct OperationTraits;

    //////////////////////////////////////////////////////////////////////

    template<template<class...> class Predicate, class T>
    struct FunctorHas
      : FalseType
    {};

    template<template<class...> class Predicate, class T>
    struct FunctorHas<Predicate, T&>
      : FunctorHas<Predicate, std::decay_t<T> >
    {};

    template<template<class...> class Predicate, class T>
    struct FunctorHas<Predicate, T&&>
      : FunctorHas<Predicate, std::decay_t<T> >
    {};

    template<template<class...> class Predicate, class T>
    struct FunctorHas<Predicate, OperationTraits<T> >
      : Predicate<T>
    {};

    /**************************************************************************
     *
     * Identity stuff
     *
     */

    template<class F>
    using IsIdentityOperation = std::is_same<IdentityOperation, F>;

    template<class T>
    using IsIdentityExpression = IsIdentityOperation<Expressions::Operation<T> >;

    /**************************************************************************
     *
     * Product stuff
     *
     */

    /**Evaluate to std::true_type if the operation is
     * product-like. This enables certain optimizations, like
     * extraction of scalar factors where we assume that a scalar
     * commutes with all product expressions.
     */
    template<class T>
    struct IsProductOperation
      : FalseType
    {};

    /**std::true_type for product like expressions. Checks arity,
     * expression-ness and the applied operation.
     */
    template<class T>
    using IsProductExpression = IsProductOperation<Expressions::Operation<T> >;

    template<>
    struct IsProductOperation<LogicalAndOperation>
      : TrueType
    {};

    template<>
    struct IsProductOperation<SMultiplyOperation>
      : TrueType
    {};

    template<class F>
    using IsSMultiplyOperation = std::is_same<SMultiplyOperation, F>;

    template<class T>
    using IsSMultiplyExpression = IsSMultiplyOperation<Expressions::Operation<T> >;

    ////// TODO: rework later to use variable templates.

    template<class T>
    constexpr inline bool MultiplicationAdmitsScalarsV = false;

    template<class T>
    constexpr inline bool MultiplicationAdmitsScalarsV<T&> = MultiplicationAdmitsScalarsV<std::decay_t<T> >;

    template<class T>
    constexpr inline bool MultiplicationAdmitsScalarsV<OperationTraits<T> > = MultiplicationAdmitsScalarsV<T>;

    template<>
    constexpr inline bool MultiplicationAdmitsScalarsV<SMultiplyOperation> = true;

    template<>
    constexpr inline bool MultiplicationAdmitsScalarsV<LogicalAndOperation> = true;

    /**************************************************************************
     *
     * !, &&, ||
     *
     */

    template<class T>
    using IsLogicalOrExpression = std::is_same<Expressions::Operation<T>, LogicalOrOperation>;

    template<class T>
    using IsLogicalAndExpression = std::is_same<Expressions::Operation<T>, LogicalAndOperation>;;

    template<class T>
    using IsLogicalNotExpression = std::is_same<Expressions::Operation<T>, LogicalNotOperation>;

    /**************************************************************************
     *
     * +/-
     *
     */

    template<class F>
    using IsPlusOperation = std::is_same<PlusOperation, F>;

    template<class F>
    using IsMinusOperation = std::is_same<MinusOperation, F>;

    template<class F>
    using IsPlusOrMinusOperation = BoolConstant<IsPlusOperation<F>::value || IsMinusOperation<F>::value>;

    template<class T>
    using IsBinaryPlusExpression =
      BoolConstant<(IsExpressionOfArity<2, T>::value
                    && std::is_same<Expressions::Operation<T>, PlusOperation>::value)>;

    template<class T>
    using IsBinaryMinusExpression =
      BoolConstant<(IsExpressionOfArity<2, T>::value
                    && std::is_same<Expressions::Operation<T>, MinusOperation>::value)>;

    template<class T>
    using IsUnaryMinusExpression =
      BoolConstant<(IsExpressionOfArity<1, T>::value
                    && std::is_same<Expressions::Operation<T>, MinusOperation>::value)>;

    template<class T>
    using IsPlusOrMinusExpression = BoolConstant<IsBinaryPlusExpression<T>::value || IsBinaryMinusExpression<T>::value>;

    using PlusFunctor = OperationTraits<PlusOperation>;
    using MinusFunctor = OperationTraits<MinusOperation>;

    namespace {

      template<class F0, class F1>
      struct NestedSumFunctorHelper
      {
        using Type = MinusFunctor;
      };

      template<class F>
      struct NestedSumFunctorHelper<F, F>
      {
        using Type = PlusFunctor;
      };
    }

    template<class F0, class F1>
    using NestedSumFunctor = typename NestedSumFunctorHelper<F0, F1>::Type;

    /**True for expressions which can be moved inside or outside of sums.*/
    template<class Operation, class SFINAE = void>
    struct IsDistributiveOperation
      : IsProductOperation<Operation>
    {};

    /**************************************************************************
     *
     * Pow, Sqrt, Square, 1/ support
     *
     */

    template<class T>
    struct IsReciprocalExpression
      : std::is_same<Expressions::Operation<T>, ReciprocalOperation>
    {};

    template<class T>
    struct IsSqrtExpression
      : std::is_same<Expressions::Operation<T>, SqrtOperation>
    {};

    template<class T>
    struct IsSquareExpression
      : std::is_same<Expressions::Operation<T>, SquareOperation>
    {};

    template<class T>
    struct IsPowExpression
      : std::is_same<Expressions::Operation<T>, PowOperation>
    {};

    template<class T>
    struct IsExponentiationOperation
      : BoolConstant<(std::is_same<T, ReciprocalOperation>::value
                      || std::is_same<T, SqrtOperation>::value
                      || std::is_same<T, SquareOperation>::value
                      || std::is_same<T, PowOperation>::value
        )>
    {};

    template<class T>
    struct IsExponentiationExpression
      : BoolConstant<(IsPowExpression<T>::value
                      || IsSqrtExpression<T>::value
                      || IsSquareExpression<T>::value
                      || IsReciprocalExpression<T>::value)>
    {};

    template<class T, std::enable_if_t<IsPowExpression<T>::value, int> = 0>
    constexpr decltype(auto) exponent(T&& t)
    {
      return std::forward<T>(t).template operand<1>();
    }

    template<class T, std::enable_if_t<IsSqrtExpression<T>::value, int> = 0>
    constexpr auto exponent(T&& t)
    {
      return IntFraction<1, 2>{};
    }

    template<class T, std::enable_if_t<IsSquareExpression<T>::value, int> = 0>
    constexpr auto exponent(T&& t)
    {
      return IntFraction<2>{};
    }

    template<class T, std::enable_if_t<IsReciprocalExpression<T>::value, int> = 0>
    constexpr auto exponent(T&& t)
    {
      return IntFraction<-1>{};
    }

    constexpr auto exponent(SqrtOperation)
    {
      return IntFraction<1, 2>{};
    }

    constexpr auto exponent(SquareOperation)
    {
      return IntFraction<2>{};
    }

    constexpr auto exponent(ReciprocalOperation)
    {
      return IntFraction<-1>{};
    }

    template<class Operation>
    constexpr auto exponent(OperationTraits<Operation>)
    {
      return exponent(Operation{});
    }

    template<class WhatEver>
    constexpr auto exponent()
    {
      return exponent(WhatEver{});
    }

    template<class T, class SFINAE = void>
    struct ExponentiationTraits
    {};

    template<class T>
    struct ExponentiationTraits<T, std::enable_if_t<IsExponentiationExpression<T>::value> >
    {
      using BaseType = Expressions::Operand<0, T>;
      using ExponentType = std::decay_t<decltype(exponent(std::declval<T>()))>;
    };

    template<class T>
    using ExponentOfPower = typename ExponentiationTraits<T>::ExponentType;

    template<class T>
    using BaseOfPower = typename ExponentiationTraits<T>::BaseType;

    using SquareFunctor = OperationTraits<SquareOperation>;

    template<class T, class SFINAE = void>
    struct SquareTraits;

    /**************************************************************************
     *
     * AssumeOperation
     *
     */

    template<class T>
    struct IsAssumeOperation
      : FalseType
    {};

    template<class... Properties>
    struct IsAssumeOperation<AssumeOperation<Properties...> >
      : TrueType
    {};

    template<class T>
    using IsAssumeExpression = IsAssumeOperation<Expressions::Operation<T> >;

    template<class T>
    struct AssumeTraits
    {
      using Properties = void;
    };

    template<class... Property>
    struct AssumeTraits<AssumeOperation<Property...> >
    {
      using Properties = MPL::TagContainer<Property...>;
    };

    template<class... Properties>
    struct AssumeTraits<OperationTraits<AssumeOperation<Properties...> > >
      : AssumeTraits<AssumeOperation<Properties...> >
    {};

    /**************************************************************************
     *
     * IndeterminateOperation
     *
     */

    template<class T>
    struct IsIndeterminateOperation
      : FalseType
    {};

    template<std::size_t Id>
    struct IsIndeterminateOperation<IndeterminateOperation<Id> >
      : TrueType
    {};

    template<class T>
    using IsIndeterminateExpression = IsIndeterminateOperation<Expressions::Operation<T> >;

    template<class T, class SFINAE = void>
    struct IndeterminateTraits;

    template<std::size_t Id>
    struct IndeterminateTraits<IndeterminateOperation<Id> >
    {
      static constexpr std::size_t id_ = Id;
      using IdType = IndexConstant<Id>;
    };

    template<std::size_t Id>
    struct IndeterminateTraits<OperationTraits<IndeterminateOperation<Id> > >
      : IndeterminateTraits<IndeterminateOperation<Id> >
    {};

    template<class T>
    struct IndeterminateTraits<
      T,
      std::enable_if_t<IsIndeterminateOperation<Expressions::Operation<T> >::value> >
      : IndeterminateTraits<Expressions::Operation<T> >
    {};

    template<class Candidate, class Id, class SFINAE = void>
    struct IndeterminateMatch
      : FalseType
    {};

    template<class Candidate, class Id>
    struct IndeterminateMatch<
      Candidate, Id,
      std::enable_if_t<IndeterminateTraits<Candidate>::id_ == Id::value> >
      : TrueType
    {};

    template<std::size_t Id, class T>
    constexpr bool indeterminateMatch(T&& t, IndexConstant<Id> = IndexConstant<Id>::value)
    {
      return IndeterminateMatch<T, IndexConstant<Id> >::value;
    }

    /**************************************************************************
     *
     * PlaceholderOperation
     *
     */

    template<class T>
    struct IsPlaceholderOperation
      : FalseType
    {};

    template<class Placeholder>
    struct IsPlaceholderOperation<PlaceholderOperation<Placeholder> >
      : TrueType
    {};

    template<class T>
    using IsPlaceholderExpression = IsPlaceholderOperation<Expressions::Operation<T> >;

    template<class T, class SFINAE = void>
    struct PlaceholderTraits;

    template<class Placeholder>
    struct PlaceholderTraits<PlaceholderOperation<Placeholder> >
    {
      using PlaceholderType = Placeholder;
    };

    template<class Placeholder>
    struct PlaceholderTraits<OperationTraits<PlaceholderOperation<Placeholder> > >
      : PlaceholderTraits<PlaceholderOperation<Placeholder> >
    {};

    template<class T>
    struct PlaceholderTraits<
      T,
      std::enable_if_t<IsPlaceholderOperation<Expressions::Operation<T> >::value> >
      : PlaceholderTraits<Expressions::Operation<T> >
    {};

    template<class Candidate, class Placeholder, class SFINAE = void>
    struct PlaceholderMatch
      : FalseType
    {};

    template<class Candidate, class Placeholder>
    struct PlaceholderMatch<
      Candidate, Placeholder,
      std::enable_if_t<std::is_same<typename PlaceholderTraits<Candidate>::PlaceholderType, Placeholder>::value> >
      : TrueType
    {};

    template<class Placeholder, class T>
    constexpr bool placeholderMatch(T&& t, Placeholder&&)
    {
      return PlaceholderMatch<T, Placeholder>::value;
    }

    template<class Placeholder, class T>
    constexpr bool placeholderMatch(T&& t)
    {
      return PlaceholderMatch<T, Placeholder>::value;
    }

    /**************************************************************************
     *
     * SubExpressionOperation
     *
     */

    template<class T>
    struct IsSubExpressionOperation
      : FalseType
    {};

    template<class Id>
    struct IsSubExpressionOperation<SubExpressionOperation<Id> >
      : TrueType
    {};

    template<class T>
    using IsSubExpressionExpression = IsSubExpressionOperation<Expressions::Operation<T> >;

    template<class T, class SFINAE = void>
    struct SubExpressionTraits;

    template<std::size_t... OperandPos>
    struct SubExpressionTraits<SubExpressionOperation<IndexSequence<OperandPos...> > >
    {
      using TreePos = IndexSequence<OperandPos...>;
    };

    template<std::size_t... OperandPos>
    struct SubExpressionTraits<OperationTraits<SubExpressionOperation<IndexSequence<OperandPos...> > > >
      : SubExpressionTraits<SubExpressionOperation<IndexSequence<OperandPos...> > >
    {};

    template<class T>
    struct SubExpressionTraits<
      T,
      std::enable_if_t<IsSubExpressionOperation<Expressions::Operation<T> >::value> >
      : SubExpressionTraits<Expressions::Operation<T> >
    {};

    /**************************************************************************
     *
     * Volatile operations render the resulting expression as "must
     * not be optimized away", except for "runtime-equal"
     * optimizations.
     *
     */

    template<class T>
    struct IsVolatileOperation
      : FalseType
    {};

    /**Placeholder expressions are volatile by design, unless
     * overridden by a RuntimEqualExpression tag.
     */
    template<class Placeholder>
    struct IsVolatileOperation<PlaceholderOperation<Placeholder> >
      : TrueType
    {};

    /**Indeterminates are also volatile by design.*/
    template<std::size_t Id>
    struct IsVolatileOperation<IndeterminateOperation<Id> >
      : TrueType
    {};

    /**************************************************************************
     *
     * Dynamic operations on constant objects render the result
     * dynamic, see IsRuntimeEqual, AreRuntimeEqual.
     *
     */

    /**Should be overloaded to std::true_type for operations which
     * introduce runtime variable data into the expression as part of
     * the operation. Example is the dynamic tensor restriction or the
     * dynamic kronecker tensor.
     *
     * Dynamic operations on constant objects render the result
     * "independent".
     */
    template<class T, class SFINAE = void>
    struct IsDynamicOperation
      : IsVolatileOperation<T>
    {};

    /////////////////////////////////////////////////////////////////////

    namespace Expressions
    {
      template<class Operation>
      using F = OperationTraits<Operation>;
    }

    //! Verbose print of an operation, helper function to produce noise.
    template<class F>
    std::string operationName(F&& f, const std::string& arg)
    {
      return std::forward<F>(f).name() + "(" + arg + ")";
    }

    //! Verbose print of a binary operation, helper function to produce noise.
    template<class F>
    std::string operationName(F&& f, const std::string& left, const std::string& right)
    {
      return "(" + left + " " + std::forward<F>(f).name() + " " + right + ")";
    }

    namespace {

      template<class String>
      std::string operationNameHelper(String&& last)
      {
        return std::forward<String>(last);
      }

      template<class... Strings>
      std::string operationNameHelper(const std::string& s0, const std::string& s1, Strings&&... rest)
      {
        return s0 + ", " + operationNameHelper(s1, std::forward<Strings>(rest)...);
      }
    }

    //! Verbose print of an n-ary operation.
    template<class F, class... Strings, std::enable_if_t<(sizeof...(Strings) > 2), int> = 0>
    std::string operationName(F&& f, Strings&&... strings)
    {
      return (std::forward<F>(f).name()
              +
              "(" + operationNameHelper(std::forward<Strings>(strings)...) + ")"
        );
    }

    template<>
    struct OperationTraits<IdentityOperation>
    {
      using InvertibleOperation = IdentityOperation;
      using OperationType = IdentityOperation;

      template<class Value>
      using ResultType = Value;

      template<class Order>
      static constexpr auto polynomialOrder(Order oldOrder)
      {
        return oldOrder;
      }

      template<class Sign>
      static constexpr auto signPropagation(Sign)
      {
        return Sign{};
      }

      static std::string name()
      {
        return "Id";
      }

      template<class T>
      constexpr decltype(auto) operator()(T&& arg) const
      {
        return std::forward<T>(arg);
      }
    };

    /**@name Special Operations
     *
     * @}
     */

    template<class... Property>
    struct OperationTraits<AssumeOperation<Property...> >
    {
      using InvertibleOperation = void;
      using OperationType = AssumeOperation<Property...>;
      using Properties = typename AssumeOperation<Property...>::Properties;

      template<class T, class SFINAE = void>
      struct HasOperation
        : FalseType
      {
        // assume elementary type if no indeterminate operation is defined.
        using ResultType = FloatingPointClosure<T>;
      };

      template<class T>
      struct HasOperation<T, std::enable_if_t<(sizeof(decltype(assume(std::declval<T>(), Properties{}))) >= 0)> >
        : TrueType
      {
        using ResultType = std::decay_t<decltype(assume(std::declval<T>(), Properties{}))>;
      };

      template<class T>
      using ResultType = typename HasOperation<T>::ResultType;

      //TODO: may depend on the property
      template<class Order>
      static constexpr auto polynomialOrder(Order oldOrder)
      {
        return oldOrder;
      }

      //TODO: may depend on the propery
      template<class Sign>
      static constexpr auto signPropagation(Sign)
      {
        return UnknownExpressionSign{};
      }

      static std::string name()
      {
        // indeterminates are always called "x", unless they are called "y" ;)
        auto str = (std::string("assume<") + ... + toString(Property{}));
        str += str.back() == '>' ? " >" : ">";
        return str;
      }

      /**Operation on something where the operation is defined.*/
      template<class T, std::enable_if_t<HasOperation<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return assume(std::forward<T>(t), Properties{});
      }

      /**Operation on the components of the
       * operand. FloatingPointClosure<T> will maintain reference and
       * cv qualifiers of T.
       */
      template<class T, std::enable_if_t<!HasOperation<T>::value, int> = 0>
      constexpr decltype(auto) operator()(T&& t) const
      {
        return forwardReturnValue<T>(t);
      }

    };

    template<std::size_t Id>
    struct OperationTraits<IndeterminateOperation<Id> >
    {
      using InvertibleOperation = void;
      using OperationType = IndeterminateOperation<Id>;

      template<class T, class SFINAE = void>
      struct HasOperation
        : FalseType
      {
        // assume elementary type if no indeterminate operation is defined.
        using ResultType = FloatingPointClosure<T>;
      };

      template<class T>
      struct HasOperation<T, std::enable_if_t<(sizeof(decltype(indeterminate(std::declval<T>(), IndexConstant<Id>{}))) >= 0)> >
        : TrueType
      {
        using ResultType = std::decay_t<decltype(indeterminate(std::declval<T>(), IndexConstant<Id>{}))>;
      };

      template<class T>
      using ResultType = typename HasOperation<T>::ResultType;

      template<class Order>
      static constexpr auto polynomialOrder(Order oldOrder)
      {
        return oldOrder;
      }

      template<class Sign>
      static constexpr auto signPropagation(Sign)
      {
        return UnknownExpressionSign{};
      }

      static std::string name()
      {
        // indeterminates are always called "x", unless they are called "y" ;)
        return "X["+std::to_string(Id)+"]";
      }

      /**Operation on something where the operation is defined.*/
      template<class T, std::enable_if_t<HasOperation<T>::value, int> = 0>
      auto operator()(T&& t) const
      {
        return indeterminate(std::forward<T>(t), IndexConstant<Id>{});
      }

      /**Operation on the components of the
       * operand. FloatingPointClosure<T> will maintain reference and
       * cv qualifiers of T.
       */
      template<class T, std::enable_if_t<!HasOperation<T>::value, int> = 0>
      constexpr FloatingPointClosure<T> operator()(T&& t) const
      {
        return t;
      }

    };

    template<class Placeholder>
    struct OperationTraits<PlaceholderOperation<Placeholder> >
    {
      using InvertibleOperation = void;
      using OperationType = PlaceholderOperation<Placeholder>;

      template<class T, class SFINAE = void>
      struct HasOperation
        : FalseType
      {
        // assume elementary type if no placeholder operation is defined.
        using ResultType = T;
      };

      template<class T>
      struct HasOperation<T, VoidType<decltype(placeholder(std::declval<T>(), OperationType{}))> >
        : TrueType
      {
        using ResultType = std::decay_t<decltype(placeholder(std::declval<T>(), OperationType{}))>;
      };

      template<class T>
      using ResultType = typename HasOperation<T>::ResultType;

      template<class Order>
      static constexpr auto polynomialOrder(Order oldOrder)
      {
        return oldOrder;
      }

      template<class Sign>
      static constexpr auto signPropagation(Sign)
      {
        return Sign{};
      }

      template<class T, std::enable_if_t<HasOperation<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return placeholder(std::forward<T>(t), OperationType{});
      }

      /**Operation on the components of the
       * operand. FloatingPointClosure<T> will maintain reference and
       * cv qualifiers of T.
       */
      template<class T, std::enable_if_t<!HasOperation<T>::value, int> = 0>
      constexpr T /*FloatingPointClosure<T>*/ operator()(T&& t) const
      {
        return std::forward<T>(t);
      }

      static std::string name()
      {
        return "?!";
      }

     private:
#if 0
      static std::string idString(UnknownExpression<>)
      {
        return "";
      }
#endif

      template<class T>
      static std::string idString(T)
      {
        return toString(T{});
      }
    };

    template<std::size_t... OperandPos>
    struct OperationTraits<SubExpressionOperation<IndexSequence<OperandPos...> > >
    {
      using TreePos = IndexSequence<OperandPos...>;
      using InvertibleOperation = void;
      using OperationType = SubExpressionOperation<TreePos>;

      template<class T1, class T2, class SFINAE = void>
      struct HasOperation
        : FalseType
      {
        // assume elementary type if no indeterminate operation is defined.
        using ResultType = FloatingPointClosure<T1>;
      };

      template<class T1, class T2>
      struct HasOperation<T1, T2, std::enable_if_t<(sizeof(decltype(subExpression(std::declval<T1>(), std::declval<T2>(), TreePos{}))) >= 0)> >
        : TrueType
      {
        using ResultType = std::decay_t<decltype(subExpression(std::declval<T1>(), std::declval<T2>(), TreePos{}))>;
      };

      template<class T1, class T2>
      using ResultType = typename HasOperation<T1, T2>::ResultType;

      template<class Order>
      static constexpr auto polynomialOrder(std::size_t order1, Order order2)
      {
        return order2;
      }

      template<class S1, class S2>
      static constexpr auto signPropagation(const S1&, const S2&)
      {
        return S2{};
      }

      static std::string name()
      {
        return "SExp"+(size<TreePos>() > 0 ? "{" + toString(TreePos{}) + "}" : "");
      }

//      template<class T1, class T2, std::enable_if_t<HasOperation<T1, T2>::value, int> = 0>
      template<class T1, class T2, std::enable_if_t<IsExpression<T1>::value || IsExpression<T2>::value, int> = 0>
      auto operator()(T1&& t1, T2&& t2) const
      {
        return subExpression(std::forward<T1>(t1), std::forward<T2>(t2), TreePos{});
      }

      /**Void the information about a wrapped typed value by returning
       * a floating point copy.
       */
//      template<class T1, class T2,std::enable_if_t<!HasOperation<T1, T2>::value, int> = 0>
      template<class T1, class T2, std::enable_if_t<!IsExpression<T1>::value && !IsExpression<T2>::value, int> = 0>
      constexpr FloatingPointClosure<T1> operator()(T1&& t1, T2&& t2) const
      {
        return t1;
      }

    };

    //!@} SpecialOperations

    /**@name ComparisonOperations
     *
     * @{
     */

    template<>
    struct OperationTraits<EqOperation>
    {
      using InvertibleOperation = void;
      using OperationType = EqOperation;

      template<class T0, class T1>
      using ResultType = bool;

      template<class Order0, class Order1>
      static constexpr auto polynomialOrder(Order0, Order1)
      {
        using namespace Literals;
        return 0_f;
      }

      template<class Sign0, class Sign1>
      static constexpr auto signPropagation(Sign0, Sign1)
      {
        return SemiPositiveExpressionSign{};
      }

      static std::string name()
      {
        return "==";
      }

      template<class T0, class T1>
      constexpr auto operator()(T0&& t0, T1&& t1) const
      {
        return std::forward<T0>(t0) == std::forward<T1>(t1);
      }
    };

    template<>
    struct OperationTraits<NeOperation>
    {
      using InvertibleOperation = void;
      using OperationType = NeOperation;

      template<class T0, class T1>
      using ResultType = bool;

      template<class Order0, class Order1>
      static constexpr auto polynomialOrder(Order0, Order1)
      {
        using namespace Literals;
        return 0_f;
      }

      template<class Sign0, class Sign1>
      static constexpr auto signPropagation(Sign0, Sign1)
      {
        return SemiPositiveExpressionSign{};
      }

      static std::string name()
      {
        return "!=";
      }

      template<class T0, class T1>
      constexpr auto operator()(T0&& t0, T1&& t1) const
      {
        return std::forward<T0>(t0) != std::forward<T1>(t1);
      }
    };

    template<>
    struct OperationTraits<GtOperation>
    {
      using InvertibleOperation = void;
      using OperationType = GtOperation;

      template<class T0, class T1>
      using ResultType = bool;

      template<class Order0, class Order1>
      static constexpr auto polynomialOrder(Order0, Order1)
      {
        using namespace Literals;
        return 0_f;
      }

      template<class Sign0, class Sign1>
      static constexpr auto signPropagation(Sign0, Sign1)
      {
        return SemiPositiveExpressionSign{};
      }

      static std::string name()
      {
        return ">";
      }

      template<class T0, class T1>
      constexpr auto operator()(T0&& t0, T1&& t1) const
      {
        return std::forward<T0>(t0) > std::forward<T1>(t1);
      }
    };

    template<>
    struct OperationTraits<GeOperation>
    {
      using InvertibleOperation = void;
      using OperationType = GeOperation;

      template<class T0, class T1>
      using ResultType = bool;

      template<class Order0, class Order1>
      static constexpr auto polynomialOrder(Order0, Order1)
      {
        using namespace Literals;
        return 0_f;
      }

      template<class Sign0, class Sign1>
      static constexpr auto signPropagation(Sign0, Sign1)
      {
        return SemiPositiveExpressionSign{};
      }

      static std::string name()
      {
        return ">=";
      }

      template<class T0, class T1>
      constexpr auto operator()(T0&& t0, T1&& t1) const
      {
        return std::forward<T0>(t0) >= std::forward<T1>(t1);
      }
    };

    template<>
    struct OperationTraits<LtOperation>
    {
      using InvertibleOperation = void;
      using OperationType = LtOperation;

      template<class T0, class T1>
      using ResultType = bool;

      template<class Order0, class Order1>
      static constexpr auto polynomialOrder(Order0, Order1)
      {
        using namespace Literals;
        return 0_f;
      }

      template<class Sign0, class Sign1>
      static constexpr auto signPropagation(Sign0, Sign1)
      {
        return SemiPositiveExpressionSign{};
      }

      static std::string name()
      {
        return "<";
      }

      template<class T0, class T1>
      constexpr auto operator()(T0&& t0, T1&& t1) const
      {
        return std::forward<T0>(t0) < std::forward<T1>(t1);
      }
    };

    template<>
    struct OperationTraits<LeOperation>
    {
      using InvertibleOperation = void;
      using OperationType = LeOperation;

      template<class T0, class T1>
      using ResultType = bool;

      template<class Order0, class Order1>
      static constexpr auto polynomialOrder(Order0, Order1)
      {
        using namespace Literals;
        return 0_f;
      }

      template<class Sign0, class Sign1>
      static constexpr auto signPropagation(Sign0, Sign1)
      {
        return SemiPositiveExpressionSign{};
      }

      static std::string name()
      {
        return "<=";
      }

      template<class T0, class T1>
      constexpr auto operator()(T0&& t0, T1&& t1) const
      {
        return std::forward<T0>(t0) <= std::forward<T1>(t1);
      }
    };

    //!@} ComparisonOperations

    /**@name LogicalOperations
     *
     * @{
     */

    template<>
    struct OperationTraits<LogicalNotOperation>
    {
      using InvertibleOperation = LogicalNotOperation;
      using OperationType = LogicalNotOperation;

      template<class T>
      using ResultType = std::decay_t<decltype(!std::declval<T>())>;

      template<class Order>
      static constexpr auto polynomialOrder(Order oldOrder)
      {
        using namespace Literals;
        return 0_f;
      }

      template<class Sign>
      static constexpr auto signPropagation(Sign)
      {
        return ExpressionSign<!Sign::isNonSingular,
                              true, // semi-positive
                              false, // semi-negative
                              false>{};
      }

      static std::string name()
      {
        return "!";
      }

      template<class T>
      constexpr auto operator()(T&& arg) const
      {
        return !std::forward<T>(arg);
      }
    };

    template<>
    struct OperationTraits<LogicalOrOperation>
    {
      using InvertibleOperation = void;
      using OperationType = LogicalOrOperation;

      template<class T1, class T2>
      using ResultType = std::decay_t<decltype(std::declval<T1>() || std::declval<T2>())>;

      static constexpr auto polynomialOrder(unsigned order1, unsigned order2)
      {
        using namespace Literals;
        return 0_f;
      }

      template<class S1, class S2>
      static constexpr auto signPropagation(const S1&, const S2&)
      {
        return ExpressionSign<(S1::isNonSingular || S2::isNonSingular), true, false, 0>{};
      }

      static std::string name()
      {
        return "||";
      }

      template<class T1, class T2>
      constexpr auto operator()(T1&& t1, T2&& t2) const
      {
        return std::forward<T1>(t1) || std::forward<T2>(t2);
      }
    };

    template<>
    struct OperationTraits<LogicalAndOperation>
    {
      using InvertibleOperation = void;
      using OperationType = LogicalAndOperation;

      template<class T1, class T2>
      using ResultType = std::decay_t<decltype(std::declval<T1>() && std::declval<T2>())>;

      static constexpr auto polynomialOrder(unsigned order1, unsigned order2)
      {
        using namespace Literals;
        return 0_f;
      }

      template<class S1, class S2>
      static constexpr auto signPropagation(const S1&, const S2&)
      {
        return ExpressionSign<(S1::isNonSingular && S2::isNonSingular), true, false, 0>{};
      }

      static std::string name()
      {
        return "&&";
      }

      template<class T1, class T2>
      constexpr auto operator()(T1&& t1, T2&& t2) const
      {
        return std::forward<T1>(t1) && std::forward<T2>(t2);
      }
    };

    //!@}

    /**@name AlgebraicOperations
     *
     * @{
     */

    template<>
    struct OperationTraits<PlusOperation>
    {
      using InvertibleOperation = void;
      using OperationType = PlusOperation;

      template<class T1, class T2>
      struct ResultHelper
      {
        using Type = std::decay_t<decltype(std::declval<T1>() + std::declval<T2>())>;
      };

      template<class T>
      struct ResultHelper<T, void>
      {
        using Type = OperationTraits<IdentityOperation>::template ResultType<T>;
      };

      // We need the helper struct as we must not instantiate the
      // binary plus expression if T2 is void.
      template<class T1, class T2 = void>
      using ResultType = typename ResultHelper<T1, T2>::Type;

      static constexpr auto polynomialOrder(unsigned order1, unsigned order2)
      {
        return max(order1, order2);
      }

      template<class S>
      static constexpr auto signPropagation(S)
      {
        return S{};
      }

      template<class S1, class S2>
      static constexpr auto signPropagation(const S1&, const S2&)
      {
        return ExpressionSign<(S1::isNonSingular || S2::isNonSingular)
                              &&
                              ((S1::isSemiPositive && S2::isSemiPositive)
                               ||
                               (S1::isSemiNegative && S2::isSemiNegative)),
                              S1::isSemiPositive && S2::isSemiPositive,
                              S1::isSemiNegative && S2::isSemiNegative,
                              S1::reference + S2::reference>{};
      }

      template<class T>
      constexpr decltype(auto) operator()(T&& arg) const
      {
        return std::forward<T>(arg);
      }

      template<class T1, class T2>
      constexpr auto operator()(T1&& t1, T2&& t2) const
      {
        return std::forward<T1>(t1) + std::forward<T2>(t2);
      }

      static std::string name()
      {
        return "+";
      }
    };

    template<>
    struct OperationTraits<PlusEqOperation>
    {
      using InvertibleOperation = void;
      using OperationType = PlusEqOperation;

      template<class T1, class T2>
      using ResultType = std::decay_t<decltype(std::declval<T1>() += std::declval<T2>())>;

      static constexpr auto polynomialOrder(unsigned order1, unsigned order2)
      {
        return max(order1, order2);
      }

      template<class S1, class S2>
      static constexpr auto signPropagation(const S1&, const S2&)
      {
        return ExpressionSign<(S1::isNonSingular || S2::isNonSingular)
                              &&
                              ((S1::isSemiPositive && S2::isSemiPositive)
                               ||
                               (S1::isSemiNegative && S2::isSemiNegative)),
                              S1::isSemiPositive && S2::isSemiPositive,
                              S1::isSemiNegative && S2::isSemiNegative,
                              S1::reference + S2::reference>{};
      }

      template<class T1, class T2>
      constexpr decltype(auto) operator()(T1&& t1, T2&& t2) const
      {
        return std::forward<T1>(t1) += std::forward<T2>(t2);
      }

      static std::string name()
      {
        return "+=";
      }
    };

    template<>
    struct OperationTraits<MinusOperation>
    {
      using ThisType = OperationTraits<MinusOperation>;
      using InvertibleOperation = MinusOperation;
      using OperationType = MinusOperation;

      template<class T1, class T2>
      struct ResultHelper
      {
        using Type = std::decay_t<decltype(std::declval<T1>() - std::declval<T2>())>;
      };

      template<class T>
      struct ResultHelper<T, std::enable_if_t<HasUnaryMinus<T>::value> >
      {
        using Type = std::decay_t<decltype(-std::declval<T>())>;
      };

      template<class T>
      struct ResultHelper<T, std::enable_if_t<!HasUnaryMinus<T>::value> >
      {
        using Type = std::decay_t<decltype(std::declval<T>())>;
      };

      // We need the helper struct as we must not instantiate the
      // binary minus expression if T2 is void.
      template<class T1, class T2 = void>
      using ResultType = typename ResultHelper<T1, T2>::Type;

      static constexpr auto polynomialOrder(unsigned order1, unsigned order2)
      {
        return max(order1, order2);
      }

      template<class Order>
      static constexpr auto polynomialOrder(Order order)
      {
        return order;
      }

      template<class Sign>
      static constexpr auto signPropagation(const Sign&)
      {
        // switch signs.
        return ExpressionSign<Sign::isNonSingular,
                              Sign::isSemiNegative,
                              Sign::isSemiPositive,
                              -Sign::reference>{};
      }

      template<class S1, class S2>
      static constexpr auto signPropagation(const S1&, const S2&)
      {
        return ExpressionSign<((S1::isNonSingular || S2::isNonSingular)
                               &&
                               ((S1::isSemiPositive && S2::isSemiNegative)
                                ||
                                (S1::isSemiNegative && S2::isSemiPositive))),
                              S1::isSemiPositive && S2::isSemiNegative,
                              S1::isSemiNegative && S2::isSemiPositive,
                              S1::reference - S2::reference>{};
      }

      static std::string name()
      {
        return "-";
      }

      template<class T, std::enable_if_t<HasUnaryMinus<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return -std::forward<T>(t);
      }

      template<class T, std::enable_if_t<!HasUnaryMinus<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        std::decay_t<T> tmp(t);
        return (tmp *= -1);
      }

      template<class T1, class T2>
      constexpr auto operator()(T1&& t1, T2&& t2) const
      {
        return std::forward<T1>(t1) - std::forward<T2>(t2);
      }
    };

    template<>
    struct OperationTraits<MinusEqOperation>
    {
      using InvertibleOperation = void;
      using OperationType = MinusEqOperation;

      template<class T1, class T2>
      using ResultType = std::decay_t<decltype(std::declval<T1>() -= std::declval<T2>())>;

      static constexpr auto polynomialOrder(unsigned order1, unsigned order2)
      {
        return max(order1, order2);
      }

      template<class S1, class S2>
      static constexpr auto signPropagation(const S1&, const S2&)
      {
        return ExpressionSign<(S1::isNonSingular || S2::isNonSingular)
                              &&
                              ((S1::isSemiPositive && S2::isSemiPositive)
                               ||
                               (S1::isSemiNegative && S2::isSemiNegative)),
                              S1::isSemiPositive && S2::isSemiPositive,
                              S1::isSemiNegative && S2::isSemiNegative,
                              S1::reference + S2::reference>{};
      }

      template<class T1, class T2>
      constexpr decltype(auto) operator()(T1&& t1, T2&& t2) const
      {
        return std::forward<T1>(t1) -= std::forward<T2>(t2);
      }

      static std::string name()
      {
        return "-=";
      }
    };

    template<>
    struct OperationTraits<ReciprocalOperation>
    {
      using InvertibleOperation = ReciprocalOperation;
      using OperationType = ReciprocalOperation;

      template<class T, class SFINAE = void>
      struct HasOperation
        : FalseType
      {
        // assume elementary type if no indeterminate operation is defined.
        using ResultType = FloatingPointClosure<T>;
      };

      template<class T>
      struct HasOperation<T, std::enable_if_t<(sizeof(decltype(reciprocal(std::declval<T>()))) >= 0)> >
        : TrueType
      {
        using ResultType = std::decay_t<decltype(reciprocal(std::declval<T>()))>;
      };

      template<class T>
      using ResultType = typename HasOperation<T>::ResultType;

      static std::size_t polynomialOrder(std::size_t oldOrder)
      {
        return oldOrder;
      }

      template<class Sign, std::enable_if_t<Sign::reference == 0, int> = 0>
      static constexpr auto signPropagation(const Sign&)
      {
        return Sign{};
      }

      template<class Sign, std::enable_if_t<(Sign::reference >= 1), int> = 0>
      static constexpr auto signPropagation(const Sign&)
      {
        // switch signs
        return ExpressionSign<Sign::isNonSingular,
                              false,
                              Sign::isSemiPositive,
                              1>{};
      }

      template<class Sign, std::enable_if_t<(Sign::reference <= -1), int> = 0>
      static constexpr auto signPropagation(const Sign&)
      {
        // switch signs
        return ExpressionSign<Sign::isNonSingular,
                              Sign::isSemiNegative,
                              false,
                              -1>{};
      }

      static std::string name()
      {
        return "1_c/";
      }

      /**Operation on something where the operation is defined.*/
      template<class T, std::enable_if_t<HasOperation<T>::value, int> = 0>
      auto operator()(T&& t) const
      {
        return reciprocal(std::forward<T>(t));
      }

      /**Operation on the components of the operand.
       */
      template<
        class T,
        std::enable_if_t<(!HasOperation<T>::value
                          && std::numeric_limits<std::decay_t<T> >::is_integer), int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return 1./FloatingPointClosure<T>(t);
      }

      /**Operation on the components of the operand.
       */
      template<
        class T,
        std::enable_if_t<(!HasOperation<T>::value
                          && !std::numeric_limits<std::decay_t<T> >::is_integer
        ), int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return one(std::forward<T>(t))/std::forward<T>(t);
      }
    };

    template<>
    struct OperationTraits<SquareOperation>
    {
      using InvertibleOperation = SqrtOperation;
      using OperationType = SquareOperation;

      template<class T, class SFINAE = void>
      struct HasOperation
        : FalseType
      {
        // assume elementary type if no indeterminate operation is defined.
        using ResultType = FloatingPointClosure<T>;
      };

      template<class T>
      struct HasOperation<T, std::enable_if_t<(sizeof(decltype(sqr(std::declval<T>()))) >= 0)> >
        : TrueType
      {
        using ResultType = std::decay_t<decltype(sqr(std::declval<T>()))>;
      };

      template<class T>
      using ResultType = typename HasOperation<T>::ResultType;

      // Assuming only real numbers.
      template<class Sign>
      static constexpr auto signPropagation(const Sign&)
      {
        constexpr ssize_t reference = Sign::reference*Sign::reference;
        constexpr bool goodCase = ((Sign::reference >= 0 && Sign::isSemiPositive)
                                   ||
                                   (Sign::reference <= 0 && Sign::isSemiNegative));
        return ExpressionSign<goodCase && Sign::isNonSingular, true, false, goodCase*reference>{};
      }

      static std::string name()
      {
        return "sqr";
      }

      template<class Order>
      static constexpr auto polynomialOrder(Order oldOrder)
      {
        using namespace Literals;
        return 2_f*oldOrder;
      }

      /**Operation on something where the operation is defined.*/
      template<class T, std::enable_if_t<HasOperation<T>::value, int> = 0>
      auto operator()(T&& t) const
      {
        return sqr(std::forward<T>(t));
      }

      template<class T, std::enable_if_t<!HasOperation<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return std::forward<T>(t)*std::forward<T>(t);
      }
    };

    template<>
    struct OperationTraits<SMultiplyOperation>
    {
      using OperationType = SMultiplyOperation;
      using InvertibleOperation = void;

      template<class T1, class T2, class Enable = void>
      struct ResultHelper;

      template<class T1, class T2>
      struct ResultHelper<T1, T2,
                          std::enable_if_t<IsFieldVector<T2>::value && (T2::dimension > 1)> >
      {
        using ResultType = std::decay_t<T2>;
      };

      template<class T1, class T2>
      struct ResultHelper<T1, T2,
                          std::enable_if_t<IsFieldVector<T1>::value && (T1::dimension > 1)> >
      {
        using ResultType = std::decay_t<T1>;
      };

      template<class T1, class T2>
      struct ResultHelper<T1, T2,
                          std::enable_if_t<std::is_convertible<T1, typename FieldTraits<T1>::field_type>::value &&
                                           std::is_convertible<T2, typename FieldTraits<T2>::field_type>::value> >
      {
        using ResultType = std::decay_t<decltype(std::declval<T1>() * std::declval<T2>())>;
      };

      template<class T1, class T2>
      using ResultType = typename ResultHelper<T1, T2>::ResultType;

      template<class Order1, class Order2>
      static constexpr auto polynomialOrder(Order1 order1, Order2 order2)
      {
        return order1 + order2;
      }

      // multiplication by scalars, non-zero property is maintained
      template<class S1, class S2>
      static constexpr auto signPropagation(const S1&, const S2&)
      {
        return ExpressionSign<// non-singular
          (S1::isNonSingular && S2::isNonSingular
           &&
           (((S1::reference >= 0) && S1::isSemiPositive)
            ||
            ((S1::reference <= 0) && S1::isSemiNegative))
           &&
           (((S2::reference >= 0) && S2::isSemiPositive)
            ||
            ((S2::reference <= 0) && S2::isSemiNegative))),
          // >= dim * S1::reference * S2::reference
          ((S1::reference*S2::reference >= 0)
           &&
           ((S1::isSemiPositive && S2::isSemiPositive)
            ||
            (S1::isSemiNegative && S2::isSemiNegative))),
          // >= dim * S1::reference * S2::reference
          ((S1::reference*S2::reference <= 0)
           &&
           ((S1::isSemiPositive && S2::isSemiNegative)
            ||
            (S1::isSemiNegative && S2::isSemiPositive))),
          // new reference
          S1::reference * S2::reference>{};
      }

      static std::string name()
      {
        return "*";
      }

      template<class T1, class T2>
      constexpr auto operator()(T1&& t1, T2&& t2) const
      {
        return std::forward<T1>(t1) * std::forward<T2>(t2);
      }
    };

    template<>
    struct OperationTraits<SMultiplyEqOperation>
    {
      using OperationType = SMultiplyEqOperation;
      using InvertibleOperation = void;

      template<class T1, class T2>
      using ResultType = std::decay_t<decltype(std::declval<T1>() *= std::declval<T2>())>;

      template<class Order1, class Order2>
      static constexpr auto polynomialOrder(Order1 order1, Order2 order2)
      {
        return order1 + order2;
      }

      // multiplication by scalars, non-zero property is maintained
      template<class S1, class S2>
      static constexpr auto signPropagation(const S1&, const S2&)
      {
        return ExpressionSign<// non-singular
          (S1::isNonSingular && S2::isNonSingular
           &&
           (((S1::reference >= 0) && S1::isSemiPositive)
            ||
            ((S1::reference <= 0) && S1::isSemiNegative))
           &&
           (((S2::reference >= 0) && S2::isSemiPositive)
            ||
            ((S2::reference <= 0) && S2::isSemiNegative))),
          // >= dim * S1::reference * S2::reference
          ((S1::reference*S2::reference >= 0)
           &&
           ((S1::isSemiPositive && S2::isSemiPositive)
            ||
            (S1::isSemiNegative && S2::isSemiNegative))),
          // >= dim * S1::reference * S2::reference
          ((S1::reference*S2::reference <= 0)
           &&
           ((S1::isSemiPositive && S2::isSemiNegative)
            ||
            (S1::isSemiNegative && S2::isSemiPositive))),
          // new reference
          S1::reference * S2::reference>{};
      }

      static std::string name()
      {
        return "*=";
      }

      template<class T1, class T2>
      constexpr decltype(auto) operator()(T1&& t1, T2&& t2) const
      {
        return std::forward<T1>(t1) *= std::forward<T2>(t2);
      }
    };

    //!@} AlgebraicOperations

    /**@name MathFunctionOperations
     *
     * @{
     */

    template<>
    struct OperationTraits<SqrtOperation>
    {
      using InvertibleOperation = SquareOperation;
      using OperationType = SqrtOperation;

      template<class Value>
      using ResultType = FloatingPointClosure<Value>;

      /**Keep the old order but convert to a run-time dynamic type.*/
      static std::size_t polynomialOrder(std::size_t oldOrder)
      {
        return oldOrder == 0 ? 0 : std::numeric_limits<std::size_t>::max();
       }

      // Assuming only real numbers.
      template<class Sign>
      static constexpr auto signPropagation(const Sign&)
      {
        return ExpressionSign<Sign::reference >= 0 && Sign::isNonSingular, true, false>{};
      }

      static std::string name()
      {
        return "sqrt";
      }

      template<class T, std::enable_if_t<!IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return sqrt(std::forward<T>(t));
      }

      template<class T, std::enable_if_t<IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return sqrt(floatingPointClosure(std::forward<T>(t)));
      }
    };

    template<>
    struct OperationTraits<ExpOperation>
    {
      using InvertibleOperation = LogOperation;
      using OperationType = ExpOperation;

      template<class Value>
      using ResultType = FloatingPointClosure<Value>;

      template<class Sign>
      static constexpr auto signPropagation(const Sign&)
      {
        return ExpressionSign<Sign::isNonSingular, true, false,
                              std::max(0L, (ssize_t)Sign::isSemiPositive * (Sign::reference + 1L))>{};
      }

      template<class Sign, std::enable_if_t<(Sign::reference < 0), int> = 0>
      static constexpr auto signPropagation(const Sign&)
      {
        return ExpressionSign<Sign::isNonSingular, true, false, 0>{};
      }

        static std::string name()
      {
        return "exp";
      }

      /**Keep the old order but convert to a run-time dynamic type.*/
      static std::size_t polynomialOrder(std::size_t oldOrder)
      {
        return oldOrder == 0 ? 0 : std::numeric_limits<std::size_t>::max();
       }

      template<class T, std::enable_if_t<!IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return exp(std::forward<T>(t));
      }

      template<class T, std::enable_if_t<IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return exp(floatingPointClosure(std::forward<T>(t)));
      }
    };

    template<>
    struct OperationTraits<LogOperation>
    {
      using InvertibleOperation = ExpOperation;
      using OperationType = LogOperation;

      template<class Value>
      using ResultType = FloatingPointClosure<Value>;

      /**Keep the old order but convert to a run-time dynamic type.*/
      static std::size_t polynomialOrder(std::size_t oldOrder)
      {
        return oldOrder == 0 ? 0 : std::numeric_limits<std::size_t>::max();
       }

      template<class Sign>
      static constexpr auto signPropagation(const Sign&)
      {
        return ExpressionSign<!(Sign::reference == 1 && Sign::isZero),
                              (Sign::reference >= 1) && Sign::isSemiPositive,
                              (Sign::reference == 1) && Sign::isSemiNegative, 0>{};
      }

      static std::string name()
      {
        return "log";
      }

      template<class T, std::enable_if_t<!IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return log(std::forward<T>(t));
      }

      template<class T, std::enable_if_t<IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return log(floatingPointClosure(std::forward<T>(t)));
      }
    };

    template<>
    struct OperationTraits<ErfOperation>
    {
      using InvertibleOperation = void;
      using OperationType = ErfOperation;

      template<class Value>
      using ResultType = FloatingPointClosure<Value>;

      template<class Sign>
      static constexpr auto signPropagation(Sign)
      {
        return Sign{};
      }

      static std::string name()
      {
        return "erf";
      }

      static constexpr std::size_t polynomialOrder(std::size_t oldOrder)
      {
        return oldOrder == 0 ? 0 : std::numeric_limits<std::size_t>::max();
      }

      template<class T, std::enable_if_t<!IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return erf(std::forward<T>(t));
      }

      template<class T, std::enable_if_t<IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return erf(floatingPointClosure(std::forward<T>(t)));
      }
    };

    template<>
    struct OperationTraits<LGammaOperation>
    {
      using InvertibleOperation = void;
      using OperationType = LGammaOperation;

      template<class Value>
      using ResultType = FloatingPointClosure<Value>;

      template<class Sign>
      static constexpr auto signPropagation(Sign)
      {
        return SemiPositiveExpressionSign{};
      }

      static std::string name()
      {
        return "lgamma";
      }

      static constexpr std::size_t polynomialOrder(std::size_t oldOrder)
      {
        return oldOrder == 0 ? 0 : std::numeric_limits<std::size_t>::max();
       }

      template<class T, std::enable_if_t<!IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return lgamma(std::forward<T>(t));
      }

      template<class T, std::enable_if_t<IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return lgamma(floatingPointClosure(std::forward<T>(t)));
      }
    };

    template<>
    struct OperationTraits<TGammaOperation>
    {
      using InvertibleOperation = void;
      using OperationType = TGammaOperation;

      template<class Value>
      using ResultType = FloatingPointClosure<Value>;

      template<class Sign>
      static constexpr auto signPropagation(Sign)
      {
        return PositiveExpressionSign{};
      }

      static std::string name()
      {
        return "tgamma";
      }

      static constexpr std::size_t polynomialOrder(std::size_t oldOrder)
      {
        return oldOrder == 0 ? 0 : std::numeric_limits<std::size_t>::max();
      }

      template<class T, std::enable_if_t<!IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return tgamma(std::forward<T>(t));
      }

      template<class T, std::enable_if_t<IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return tgamma(floatingPointClosure(std::forward<T>(t)));
      }
    };

    template<>
    struct OperationTraits<CoshOperation>
    {
      using InvertibleOperation = AcoshOperation;
      using OperationType = CoshOperation;

      template<class Value>
      using ResultType = FloatingPointClosure<Value>;

      static constexpr std::size_t polynomialOrder(std::size_t oldOrder)
      {
        return oldOrder == 0 ? 0 : std::numeric_limits<std::size_t>::max();
      }

      template<class Sign>
      static constexpr auto signPropagation(const Sign&)
      {
        return ExpressionSign<false, // >= 1
                              true, // semi positive,
                              false, // not semi negative,
                              1>{};
      }

      static std::string name()
      {
        return "cosh";
      }

      template<class T, std::enable_if_t<!IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return cosh(std::forward<T>(t));
      }

      template<class T, std::enable_if_t<IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return cosh(floatingPointClosure(std::forward<T>(t)));
      }
    };

    template<>
    struct OperationTraits<SinhOperation>
    {
      using InvertibleOperation = AsinhOperation;
      using OperationType = SinhOperation;

      template<class Value>
      using ResultType = FloatingPointClosure<Value>;

      static constexpr std::size_t polynomialOrder(std::size_t oldOrder)
      {
        return oldOrder == 0 ? 0 : std::numeric_limits<std::size_t>::max();
      }

      template<class Sign>
      static constexpr auto signPropagation(const Sign&)
      {
        return Sign{};
      }

      static std::string name()
      {
        return "sinh";
      }

      template<class T, std::enable_if_t<!IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return sinh(std::forward<T>(t));
      }

      template<class T, std::enable_if_t<IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return sinh(floatingPointClosure(std::forward<T>(t)));
      }
    };

    template<>
    struct OperationTraits<TanhOperation>
    {
      using InvertibleOperation = AtanhOperation;
      using OperationType = TanhOperation;

      template<class Value>
      using ResultType = FloatingPointClosure<Value>;

      static constexpr std::size_t polynomialOrder(std::size_t oldOrder)
      {
        return oldOrder == 0 ? 0 : std::numeric_limits<std::size_t>::max();
      }

      template<class Sign>
      static constexpr auto signPropagation(const Sign&)
      {
        return Sign{};
      }

      static std::string name()
      {
        return "tanh";
      }

      template<class T, std::enable_if_t<!IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return tanh(std::forward<T>(t));
      }

      template<class T, std::enable_if_t<IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return tanh(floatingPointClosure(std::forward<T>(t)));
      }
    };

    template<>
    struct OperationTraits<AcoshOperation>
    {
      using InvertibleOperation = void;
      using OperationType = AcoshOperation;

      template<class Value>
      using ResultType = FloatingPointClosure<Value>;

      static constexpr std::size_t polynomialOrder(std::size_t oldOrder)
      {
        return oldOrder == 0 ? 0 : std::numeric_limits<std::size_t>::max();
      }

      template<class Sign>
      static constexpr auto signPropagation(const Sign&)
      {
        return ExpressionSign<false, // >= 0
                              true, // semi positive,
                              false // not semi negative
                              >{};
      }

      static std::string name()
      {
        return "acosh";
      }

      template<class T, std::enable_if_t<!IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return acosh(std::forward<T>(t));
      }

      template<class T, std::enable_if_t<IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return acosh(floatingPointClosure(std::forward<T>(t)));
      }
    };

    template<>
    struct OperationTraits<AsinhOperation>
    {
      using InvertibleOperation = SinhOperation;
      using OperationType = AsinhOperation;

      template<class Value>
      using ResultType = FloatingPointClosure<Value>;

      static constexpr std::size_t polynomialOrder(std::size_t oldOrder)
      {
        return oldOrder == 0 ? 0 : std::numeric_limits<std::size_t>::max();
      }

      template<class Sign>
      static constexpr auto signPropagation(const Sign&)
      {
        return Sign{};
      }

      static std::string name()
      {
        return "sinh";
      }

      template<class T, std::enable_if_t<!IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return asinh(std::forward<T>(t));
      }

      template<class T, std::enable_if_t<IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return asinh(floatingPointClosure(std::forward<T>(t)));
      }
    };

    template<>
    struct OperationTraits<AtanhOperation>
    {
      using InvertibleOperation = TanhOperation;
      using OperationType = AtanhOperation;

      template<class Value>
      using ResultType = FloatingPointClosure<Value>;

      static constexpr std::size_t polynomialOrder(std::size_t oldOrder)
      {
        return oldOrder == 0 ? 0 : std::numeric_limits<std::size_t>::max();
      }

      template<class Sign>
      static constexpr auto signPropagation(const Sign&)
      {
        return Sign{};
      }

      static std::string name()
      {
        return "atanh";
      }

      template<class T, std::enable_if_t<!IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return atanh(std::forward<T>(t));
      }

      template<class T, std::enable_if_t<IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return atanh(floatingPointClosure(std::forward<T>(t)));
      }
    };

    template<>
    struct OperationTraits<SinOperation>
    {
      using InvertibleOperation = AsinOperation;
      using OperationType = SinOperation;

      template<class Value>
      using ResultType = FloatingPointClosure<Value>;

      static constexpr std::size_t polynomialOrder(std::size_t oldOrder)
      {
        return oldOrder == 0 ? 0 : std::numeric_limits<std::size_t>::max();
      }

      //This operation destroys all knowledge about the sign.
      template<class Sign>
      static constexpr auto signPropagation(const Sign&)
      {
        return UnknownExpressionSign{};
      }

      static std::string name()
      {
        return "sin";
      }

      template<class T, std::enable_if_t<!IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return sin(std::forward<T>(t));
      }

      template<class T, std::enable_if_t<IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return sin(floatingPointClosure(std::forward<T>(t)));
      }
    };

    template<>
    struct OperationTraits<CosOperation>
    {
      using InvertibleOperation = AcosOperation;
      using OperationType = CosOperation;

      template<class Value>
      using ResultType = FloatingPointClosure<Value>;

      static constexpr std::size_t polynomialOrder(std::size_t oldOrder)
      {
        return oldOrder == 0 ? 0 : std::numeric_limits<std::size_t>::max();
      }

      //This operation destroys all knowledge about the sign.
      template<class Sign>
      static constexpr auto signPropagation(const Sign&)
      {
        return UnknownExpressionSign{};
      }

      static std::string name()
      {
        return "cos";
      }

      template<class T, std::enable_if_t<!IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return cos(std::forward<T>(t));
      }

      template<class T, std::enable_if_t<IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return cos(floatingPointClosure(std::forward<T>(t)));
      }
    };

    template<>
    struct OperationTraits<TanOperation>
    {
      using InvertibleOperation = AtanOperation;
      using OperationType = TanOperation;

      template<class Value>
      using ResultType = FloatingPointClosure<Value>;

      static constexpr std::size_t polynomialOrder(std::size_t oldOrder)
      {
        return oldOrder == 0 ? 0 : std::numeric_limits<std::size_t>::max();
      }

      template<class Sign>
      static constexpr auto signPropagation(const Sign&)
      {
        return UnknownExpressionSign{};
      }

      static std::string name()
      {
        return "tan";
      }

      template<class T, std::enable_if_t<!IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return tan(std::forward<T>(t));
      }

      template<class T, std::enable_if_t<IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return tan(floatingPointClosure(std::forward<T>(t)));
      }
    };

    template<>
    struct OperationTraits<AsinOperation>
    {
      using InvertibleOperation = void;
      using OperationType = AsinOperation;

      template<class Value>
      using ResultType = FloatingPointClosure<Value>;

      static constexpr std::size_t polynomialOrder(std::size_t oldOrder)
      {
        return oldOrder == 0 ? 0 : std::numeric_limits<std::size_t>::max();
      }

      template<class Sign>
      static constexpr auto signPropagation(const Sign&)
      {
        return ExpressionSign<((Sign::reference >= 2 || Sign::reference <= -2)
                               ||
                               (Sign::reference == 0 && Sign::isNonSingular)
                               ||
                               (Sign::reference > 0 && Sign::isSemiPositive)
                               ||
                               (Sign::reference < 0 && Sign::isSemiNegative)),
                              (Sign::reference >= 0 && Sign::isSemiPositive),
                              (Sign::reference <= 0 && Sign::isSemiNegative),
                              0>{};
      }

      static std::string name()
      {
        return "asin";
      }

      template<class T, std::enable_if_t<!IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return asin(std::forward<T>(t));
      }

      template<class T, std::enable_if_t<IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return asin(floatingPointClosure(std::forward<T>(t)));
      }
    };

    template<>
    struct OperationTraits<AcosOperation>
    {
      using InvertibleOperation = void;
      using OperationType = AcosOperation;

      template<class Value>
      using ResultType = FloatingPointClosure<Value>;

      static constexpr std::size_t polynomialOrder(std::size_t oldOrder)
      {
        return oldOrder == 0 ? 0 : std::numeric_limits<std::size_t>::max();
      }

      template<class Sign>
      static constexpr auto signPropagation(const Sign&)
      {
        // cos is semiPositive
        return ExpressionSign<(Sign::reference == 0 && Sign::isSemiNegative)
                              ||
                              (Sign::reference == 1 && Sign::isNonSingular),
                              true, false, 0>{};
      }

      static std::string name()
      {
        return "acos";
      }

      template<class T, std::enable_if_t<!IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return acos(std::forward<T>(t));
      }

      template<class T, std::enable_if_t<IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return acos(floatingPointClosure(std::forward<T>(t)));
      }
    };

    template<>
    struct OperationTraits<AtanOperation>
    {
      using InvertibleOperation = void;
      using OperationType = AtanOperation;

      template<class Value>
      using ResultType = FloatingPointClosure<Value>;

      static constexpr std::size_t polynomialOrder(std::size_t oldOrder)
      {
        return oldOrder == 0 ? 0 : std::numeric_limits<std::size_t>::max();
      }

      template<class Sign>
      static constexpr auto signPropagation(const Sign&)
      {
        return ExpressionSign<((Sign::reference >= 2 || Sign::reference <= -2)
                               ||
                               (Sign::reference == 0 && Sign::isNonSingular)
                               ||
                               (Sign::reference == 1 && Sign::isSemiPositive)
                               ||
                               (Sign::reference == -1 && Sign::isSemiNegative)),
                              (Sign::reference >= 0 && Sign::isSemiPositive),
                              (Sign::reference <= 0 && Sign::isSemiNegative),
                              0>{};
      }

      static std::string name()
      {
        return "atan";
      }

      template<class T, std::enable_if_t<!IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return atan(std::forward<T>(t));
      }

      template<class T, std::enable_if_t<IsScalar<T>::value, int> = 0>
      constexpr auto operator()(T&& t) const
      {
        return atan(floatingPointClosure(std::forward<T>(t)));
      }
    };

    template<>
    struct OperationTraits<PowOperation>
    {
      using InvertibleOperation = void;
      using OperationType = PowOperation;

      template<class T1, class T2>
      using ResultType = typename FieldPromotion<T1, FloatingPointClosure<T2> >::Type;

      static constexpr std::size_t polynomialOrder(unsigned order1, unsigned order2)
      {
        return order2 == 0 ? 0 : std::numeric_limits<std::size_t>::max();
      }

      // Assuming only real numbers.
      template<class S1, class S2>
      static constexpr auto signPropagation(const S1&, const S2&)
      {
        return ExpressionSign<S1::reference >= 0 && S1::isNonSingular, true, false>{};
      }

      static std::string name()
      {
        return "^";
      }

      template<class T1, class T2, std::enable_if_t<!IsScalar<T1>::value && !IsScalar<T2>::value, int> = 0>
      constexpr auto operator()(T1&& t1, T2&& t2) const
      {
        return pow(std::forward<T1>(t1), std::forward<T2>(t2));
      }

      template<class T1, class T2, std::enable_if_t<IsScalar<T1>::value && IsScalar<T2>::value, int> = 0>
      constexpr auto operator()(T1&& t1, T2&& t2) const
      {
        return pow(floatingPointClosure(std::forward<T1>(t1)), floatingPointClosure(std::forward<T2>(t2)));
      }

      template<class T, class I, I N,
               std::enable_if_t<(N % 2 == 1
                                 && N < 0
                                 && -N < 20
                                 && IsScalar<T>::value
                 ), int> = 0>
      constexpr auto operator()(T&& t, TypedValue::FractionConstant<I, N, 2>) const
      {
        using namespace Literals;

        return 1_f / sqrt(multiplyLoop<-N>(1_f, [&](auto i) { return std::forward<T>(t); }));
      }

      template<class T, class I, I N,
               std::enable_if_t<(N < 0
                                 && -N < 20
                                 && IsScalar<T>::value
                 ), int> = 0>
      constexpr auto operator()(T&& t, TypedValue::FractionConstant<I, N, 1>) const
      {
        using namespace Literals;

        return 1_f / multiplyLoop<-N>(1_f, [&](auto i) { return std::forward<T>(t); });
      }

      template<class T, class I, I N,
               std::enable_if_t<(N > 0
                                 && N < 20
                                 && IsScalar<T>::value
                 ), int> = 0>
      constexpr auto operator()(T&& t, TypedValue::FractionConstant<I, N, 1>) const
      {
        using namespace Literals;

        return multiplyLoop<N>(1_f, [&](auto i) { return std::forward<T>(t); });
      }

    };

    template<>
    struct OperationTraits<Atan2Operation>
    {
      using InvertibleOperation = void;
      using OperationType = Atan2Operation;

      template<class T1, class T2>
      using ResultType = typename FieldPromotion<T1, T2>::Type;

      static constexpr std::size_t polynomialOrder(unsigned order1, unsigned order2)
      {
        return order1 == 0 && order2 == 0 ? 0 : std::numeric_limits<std::size_t>::max();
      }

      // Assuming only real numbers.
      template<class S1, class S2>
      static constexpr auto signPropagation(const S1&, const S2&)
      {
        return ExpressionSign<((S1::reference >= 0 && S1::isPositive)
                               ||
                               (S1::reference <= 0 && S1::isNegative)),
                              S1::reference >= 0 && S1::isSemiPositive,
                              S1::reference <= 0 && S1::isSemiNegative>{};
      }

      static std::string name()
      {
        return "atan2";
      }

      template<class T1, class T2, std::enable_if_t<!IsScalar<T1>::value && !IsScalar<T2>::value, int> = 0>
      constexpr auto operator()(T1&& t1, T2&& t2) const
      {
        return atan2(std::forward<T1>(t1), std::forward<T2>(t2));
      }

      template<class T1, class T2, std::enable_if_t<IsScalar<T1>::value && IsScalar<T2>::value, int> = 0>
      constexpr auto operator()(T1&& t1, T2&& t2) const
      {
        return atan2(floatingPointClosure(std::forward<T1>(t1)), floatingPointClosure(std::forward<T2>(t2)));
      }
    };

    template<>
    struct OperationTraits<MinOperation>
    {
      using InvertibleOperation = void;
      using OperationType = MinOperation;

      template<class T1, class T2>
      using ResultType = std::decay_t<decltype(min(std::declval<T1>(), std::declval<T2>()))>;

      static constexpr std::size_t polynomialOrder(unsigned order1, unsigned order2)
      {
        return std::max(order1, order2); // don't know
      }

      // Assuming only real numbers.
      template<class S1, class S2>
      static constexpr auto signPropagation(const S1&, const S2&)
      {
        return ExpressionSign<(S1::reference == S2::reference
                               &&
                               ((S1::isNonSingular && S1::isNegative)
                                ||
                                (S2::isNonSingular && S2::isNegative))),
                              (S1::reference == S2::reference
                               &&
                               S1::isSemiPositive
                               &&
                               S2::isSemiPositive),
                              (S1::reference == S2::reference
                               &&
                               S1::isSemiNegative
                               &&
                               S2::isSemiNegative)>{};
      }

      static std::string name()
      {
        return "min";
      }

      template<class T1, class T2>
      constexpr auto operator()(T1&& t1, T2&& t2) const
      {
        return min(std::forward<T1>(t1), std::forward<T2>(t2));
      }
    };

    template<>
    struct OperationTraits<MaxOperation>
    {
      using InvertibleOperation = void;
      using OperationType = MaxOperation;

      template<class T1, class T2>
      using ResultType = std::decay_t<decltype(max(std::declval<T1>(), std::declval<T2>()))>;

      static constexpr std::size_t polynomialOrder(unsigned order1, unsigned order2)
      {
        return std::max(order1, order2); // don't know
      }

      // Assuming only real numbers.
      template<class S1, class S2>
      static constexpr auto signPropagation(const S1&, const S2&)
      {
        return ExpressionSign<(S1::reference == S2::reference
                               &&
                               ((S1::isNonSingular && S1::isNegative)
                                ||
                                (S2::isNonSingular && S2::isNegative))),
                              (S1::reference == S2::reference
                               &&
                               S1::isSemiPositive
                               &&
                               S2::isSemiPositive),
                              (S1::reference == S2::reference
                               &&
                               S1::isSemiNegative
                               &&
                               S2::isSemiNegative)>{};
      }

      static std::string name()
      {
        return "max";
      }

      template<class T1, class T2>
      constexpr auto operator()(T1&& t1, T2&& t2) const
      {
        return max(std::forward<T1>(t1), std::forward<T2>(t2));
      }
    };

    template<>
    struct OperationTraits<TernaryOperation>
    {
      using InvertibleOperation = void;
      using OperationType = TernaryOperation;

      template<class T1>
      using ResultType = double;

      template<class Order>
      static constexpr auto polynomialOrder(Order)
      {
        using namespace Literals;

        return 0_f;
      }

      // Assuming only real numbers.
      template<class S1>
      static constexpr auto signPropagation(const S1&)
      {
        return SemiPositiveExpressionSign{};
      }

      static std::string name()
      {
        return "if";
      }

      template<class T1>
      constexpr auto operator()(T1&& t1) const
      {
        if(t1 > 0)
          return 1.;
        else
          return 0.;
      }
    };

    //!@} MathFunctionOperations

    //! @} ExpressionResults

    //! @} ExpressionTemplates

  } // ACFem

} // Dune

#endif // __DUNE_ACFEM_EXPRESSIONS_OPERATIONTRAITS_HH__
