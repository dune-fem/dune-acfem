#ifndef __DUNE_ACFEM_EXPRESSIONS_SIGNUTIL_HH__
#define __DUNE_ACFEM_EXPRESSIONS_SIGNUTIL_HH__

#include <ostream>

#include "sign.hh"
#include "terminal.hh"

namespace Dune {

  namespace ACFem {

    namespace Expressions {

      template<class T, std::enable_if_t<!IsExpression<T>::value || IsSelfExpression<T>::value, int> = 0>
      constexpr auto expressionSign(T&&)
      {
        return typename ExpressionTraits<T>::Sign{};
      }

      namespace {

        template<class T, std::size_t... Idx>
        constexpr auto expressionSignExpander(T&& t, IndexSequence<Idx...>)
        {
          return Functor<T>::signPropagation(expressionSign(std::forward<T>(t).template operand<Idx>())...);
        }

      }

      template<class T, std::enable_if_t<IsExpression<T>::value && !IsSelfExpression<T>::value, int> = 0>
      constexpr auto expressionSign(T&& t)
      {
        return expressionSignExpander(std::forward<T>(t), MakeIndexSequence<Arity<T>::value>{});
      }

      template<class T>
      using ExpressionSignOf = decltype(expressionSign(std::declval<T>()));

      template<std::ptrdiff_t NewOrigin, class Sign>
      constexpr auto expressionSignAt(Sign, IntConstant<NewOrigin> = IntConstant<NewOrigin>{})
      {
        constexpr std::ptrdiff_t offset = NewOrigin - Sign::reference;
        constexpr bool nonSingular =
          (offset == 0 && Sign::isNonSingular)
          || (offset < 0 && Sign::isSemiPositive)
          || (offset > 0 && Sign::isSemiNegative);
        constexpr bool semiPositive = offset <= 0 && Sign::isSemiPositive;
        constexpr bool semiNegative = offset >= 0 && Sign::isSemiNegative;
        return ExpressionSign<nonSingular, semiPositive, semiNegative, NewOrigin>{};
      }

      template<std::ptrdiff_t NewOrigin, class Sign>
      using ExpressionSignAt = decltype(expressionSignAt<NewOrigin>(Sign{}));

    } // Expressions::

    using Expressions::expressionSign;

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_EXPRESSIONS_SIGNUTIL_HH__
