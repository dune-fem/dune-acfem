#ifndef __DUNE_ACFEM_EXPRESSIONS_TREEEXTRACT_HH__
#define __DUNE_ACFEM_EXPRESSIONS_TREEEXTRACT_HH__

#include "terminal.hh"
#include "treeoperand.hh"

namespace Dune::ACFem::Expressions {

  template<class E, class TreePos, class Parent>
  using PackTreeNode = MPL::TypeTuple<E, TreePos, Parent>;

  template<class E, class TreePos, class Parent>
  using PackTreePair = MPL::TypePair<E, TreePos>;

  template<class E, class TreePos, class Parent>
  using PackTreePosition = TreePos;

  template<class E, class TreePos, class Parent>
  using PackTreeParent = Parent;

  template<class E, class TreePos, class Parent>
  using PackTreeOperand = E;

  template<class E, class TreePos, class... T>
  using ProcessTreeOperands = MPL::TypeTupleCat<T...>;

  namespace {

    template<template<class Expr, class TreePos, class...> class Process,
             template<class...> class Match,
             template<class...> class PackIt,
             template<class...> class Ignore,
             class E, class TreePos, class Parent, class Index, class SFINAE = void>
    struct TreeExtractHelper;

    template<template<class Expr, class TreePos, class...> class Process,
             template<class...> class Match,
             template<class...> class PackIt,
             template<class...> class Ignore,
             class E, class TreePos, class Parent, class ArgIdx>
    struct TreeExtractHelper<Process, Match, PackIt, Ignore, E, TreePos, Parent, ArgIdx,
                             std::enable_if_t<Match<E, Parent>::value> >
    {
      using Type = MPL::TypeTuple<PackIt<E, TreePos, Parent> >;
    };

    /**@internal Recursion node, descend into the expression tree and
     * post-process the results.
     */
    template<template<class Expr, class Pos, class...> class Process,
             template<class...> class Match,
             template<class...> class PackIt,
             template<class...> class Ignore,
             class E, std::size_t... TreePos, class Parent, std::size_t... I>
    struct TreeExtractHelper<Process, Match, PackIt, Ignore, E, TreePosition<TreePos...>, Parent, IndexSequence<I...>,
                             std::enable_if_t<!Match<E, Parent>::value && !Ignore<E>::value> >
    {
      using Type = Process<
        E,
        TreePosition<TreePos...>,
        typename TreeExtractHelper<Process, Match, PackIt, Ignore,
                                   Operand<I, E>, TreePosition<TreePos..., I>, E,
                                   MakeIndexSequence<Arity<Operand<I, E> >::value> >::Type
        ...>;
    };

    template<template<class Expr, class TreePos, class...> class Process,
             template<class...> class Match,
             template<class...> class PackIt,
             template<class...> class Ignore,
             class E, class TreePos, class Parent, class ArgIdx>
    struct TreeExtractHelper<Process, Match, PackIt, Ignore, E, TreePos, Parent, ArgIdx,
                             std::enable_if_t<!Match<E, Parent>::value && Ignore<E>::value> >
    {
      using Type = MPL::TypeTuple<>;
    };

  }

  /**Extract operands of an expression tree.
   *
   * @param Match Filter-predicate. Operands with @c Match<Expr,
   * Parent>::value == @c true are extracted.
   *
   * @param E The expression to examine.
   *
   * @param PackIt If @c Match<T, Parent>::value is @c true then @c
   * PackIt<T, TreePos, Parent> forms the result data for @c T which
   * is then passed on to @a Process.
   *
   * @param TreePos An initial expression tree position pre-pended to
   * the deduced tree position. Can be used in order to form
   * tree-positions relative to a "root" expression while extracting
   * from a sub-expression.
   *
   * @param Ignore Predicate identifying operands which should be
   * excluded from the recursive expression tree traversal. If @c
   * Ignore<T>::value is @c true then the expression sub-tree below @c
   * T is ignored. Still: if @a Match<T, Parent>::value is @c true then @c T
   * is selected for extraction.
   *
   * @param Process Post-process the results found at lower levels.
   *
   */
  template<template<class...> class Match,
           class E,
           template<class...> class PackIt = PackTreeNode,
           class TreePos = TreePosition<>,
           template<class...> class Ignore = IsSelfExpression,
           template<class Expr, class Pos, class...> class Process = ProcessTreeOperands>
  using TreeExtract = typename TreeExtractHelper<Process, Match, PackIt, Ignore, E, TreePos, void, MakeIndexSequence<Arity<E>::value> >::Type;

}

#endif // __DUNE_ACFEM_EXPRESSIONS_TREEEXTRACT_HH__
