#ifndef __DUNE_ACFEM_EXPRESSIONS_TYPETRAITS_HH__
#define __DUNE_ACFEM_EXPRESSIONS_TYPETRAITS_HH__

/**@file
 *
 * Various traits for expressions.
 */

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

#include "../common/types.hh"

namespace Dune
{

  namespace ACFem
  {

    /**@addtogroup TypeTraits
     *
     * @{
     */

    /**@c TrueType if I is an integral type.*/
    template<class I>
    struct IsIntegral
      : std::is_integral<I>
    {};

    /**std::true_type if F is an "elementary" scalar.*/
    template<class F, class = void>
    struct IsScalar
      : FalseType
    {};

    template<class T>
    struct IsScalar<T, std::enable_if_t<!IsDecay<T>::value> >
      : IsScalar<std::decay_t<T> >
    {};

    /**Standard floating point types and integral types are scalars,
     * also FieldVectors of dimension 1 convertible to standard
     * floating point types.
     */
    template<class F>
    struct IsScalar<F,
                    std::enable_if_t<(std::is_convertible<F*, typename FieldTraits<F>::field_type*>::value
                                      &&
                                      (std::numeric_limits<typename FieldTraits<F>::real_type>::is_iec559
                                       ||
                                       std::numeric_limits<typename FieldTraits<F>::real_type>::is_integer))> >
     : TrueType
    {};

    template<class FV, class SFINAE = void>
    struct IsFieldVector
      : FalseType
    {};

    template<class FV>
    struct IsFieldVector<FV, std::enable_if_t<!IsDecay<FV>::value> >
      : IsFieldVector<std::decay_t<FV> >
    {};

    template<class F, int dim>
    struct IsFieldVector<FieldVector<F, dim> >
     : TrueType
    {};

    template<class F>
    struct IsScalar<FieldVector<F, 1> >
     : TrueType
    {};

    template<class M>
    struct IsFieldMatrix
      : FalseType
    {};

    template<class F, int Rows, int Cols>
    struct IsFieldMatrix<FieldMatrix<F, Rows, Cols> >
     : TrueType
    {};

    //!@} TypeTraits

    /**@addtogroup FieldTraits
     * @{
     */

    /**Helper class for specializing Dune::FieldTraits.*/
    template<class T, class SFINAE = void>
    struct FieldTraits
      : Dune::FieldTraits<T>
    {};

    //! @} FieldTraits

  } // ACFem

} // Dune

#endif // __DUNE_ACFEM_EXPRESSIONS_TYPETRAITS_HH__
