#ifndef __DUNE_ACFEM_EXPRESSIONS_TERMINAL_HH__
#define __DUNE_ACFEM_EXPRESSIONS_TERMINAL_HH__

#include "../common/types.hh"

#include "storage.hh"
#include "interface.hh"
#include "tags.hh"
#include "operationtraits.hh"

namespace Dune
{

  namespace ACFem
  {

    namespace Expressions
    {
      /**Terminals may derive from this class to express that they are
       * expressions.
       */
      template<class T>
      struct SelfExpression
        : TerminalExpression
      {
        using OperationType = IdentityOperation;
        using FunctorType = OperationTraits<OperationType>;
        template<std::size_t I>
        using OperandType = ConditionalType<I == 0, T, void>;

        static constexpr auto operation()
        {
          return FunctorType{};
        }

        static constexpr std::size_t arity()
        {
          return 1;
        }

        template<std::size_t I>
        decltype(auto) operand(IndexConstant<I> = IndexConstant<I>{})
        {
          static_assert(I < arity(), "Not so many operands");
          return static_cast<T&>(*this);
        }

        template<std::size_t I>
        decltype(auto) operand(IndexConstant<I> = IndexConstant<I>{}) const
        {
          static_assert(I < arity(), "Not so many operands");
          return static_cast<const T&>(*this);
        }
      };

      /**Identify self-contained terminals, see SelfExpression.*/
      template<class T, class SFINAE = void>
      struct IsSelfExpression
        : BoolConstant<std::is_same<T, Operand<0, T> >::value>
      {};

      template<class T>
      struct IsSelfExpression<T, std::enable_if_t<IsDecay<T>::value && !IsExpression<T>::value> >
        : FalseType
      {};

      template<class T>
      struct IsSelfExpression<T, std::enable_if_t<!IsDecay<T>::value> >
        : IsSelfExpression<std::decay_t<T> >
      {};

      template<class T>
      using IsTerminal = BoolConstant<(IsSelfExpression<T>::value
                                       || HasTag<T, TerminalExpression>::value)>;

      /**@internal @c true for everything which can be converted to an expression.*/
      template<class T, class SFINAE = void>
      struct IsOperand
        : FalseType
      {};

      /**@internal @c true for everything which can be converted to an expression.*/
      template<class T>
      struct IsOperand<T, std::enable_if_t<!IsDecay<T>::value> >
        : IsOperand<std::decay_t<T> >
      {};

      /**@internal @c true for everything which can be converted to an expression.*/
      template<class T>
      struct IsOperand<T, std::enable_if_t<(IsExpression<T>::value && IsDecay<T>::value)> >
        : TrueType
      {};

      /**Traits class in order to inject general object into the
       * expression chain in a controlled manner.
       */
      template<class T, class SFINAE = void>
      struct InjectionTraits;

      template<
        class T,
        std::enable_if_t<(IsOperand<T>::value
                          && !IsExpression<T>::value
          ), int> = 0>
      auto asExpression(T&& t)
      {
        return InjectionTraits<T>::asExpression(std::forward<T>(t));
      }

    } // Expressions

    using Expressions::IsSelfExpression;

    template<class T>
    using IsExpressionOperand = Expressions::IsOperand<T>;

  } // ACFem

} // Dune

#endif // __DUNE_ACFEM_EXPRESSIONS_TERMINAL_HH__
