#ifndef __DUNE_ACFEM_EXPRESSIONS_POWERS_HH__
#define __DUNE_ACFEM_EXPRESSIONS_POWERS_HH__

#include "runtimeequal.hh"
#include "expressionoperations.hh"

namespace Dune
{

  namespace ACFem
  {

    namespace Expressions
    {
      using namespace Literals;

      /**@addtogroup ExpressionTemplates
       *
       * @{
       *
       */

      /**@addtogroup ExpressionTraits
       *
       * @{
       */

      /**Identify the various flavours of drawing powers.*/
      template<class T1, class T2>
      struct AreRuntimeEqual<
        T1, T2,
        std::enable_if_t<(IsExponentiationExpression<T1>::value
                          && IsExponentiationExpression<T2>::value
                          && std::is_same<BaseOfPower<T1>, BaseOfPower<T2> >::value
                          && ExponentOfPower<T1>::value == ExponentOfPower<T2>::value
          )> >
        : TrueType
      {};

      //! @} ExpressionTemplates

    } // Expressions

  } // ACFem

} // Dune

#endif // __DUNE_ACFEM_EXPRESSIONS_POWERS_HH__
