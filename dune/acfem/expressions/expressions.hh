#ifndef __DUNE_ACFEM_EXPRESSIONS_EXPRESSIONS_HH__
#define __DUNE_ACFEM_EXPRESSIONS_EXPRESSIONS_HH__

// slurp in all headers

#include "constantoperations.hh"
#include "cseoptimization.hh"
#include "decay.hh"
#include "densestorage.hh"
#include "expressionoperations.hh"
#include "expressiontraits.hh"
#include "examine.hh"
#include "extract.hh"
#include "interface.hh"
#include "operandpromotion.hh"
#include "operationtraits.hh"
#include "optimizationbase.hh"
#include "policy.hh"
#include "polynomialtraits.hh"
#include "powers.hh"
#include "runtimeequal.hh"
#include "sign.hh"
#include "signutil.hh"
#include "storage.hh"
#include "subexpression.hh"
#include "terminal.hh"
#include "traitsdefault.hh"
#include "transform.hh"
#include "typetraits.hh"

#endif // __DUNE_ACFEM_EXPRESSIONS_EXPRESSIONS_HH__
