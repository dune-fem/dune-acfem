#ifndef __DUNE_ACFEM_EXPRESSIONS_EXAMINE_HH__
#define __DUNE_ACFEM_EXPRESSIONS_EXAMINE_HH__

#include "../mpl/typetuple.hh"
#include "storage.hh"
#include "terminal.hh"

namespace Dune::ACFem::Expressions {

  namespace ExamineImpl {

    namespace {

      template<class E, template<class... Arg> class F, class ArgIndex, class SFINAE = void>
      struct TraverseHelper;

      template<class T>
      struct MakeAritySequence
      {
        using Type = MakeIndexSequence<Arity<T>::value>;
      };

      template<class T, class... Rest>
      struct MakeAritySequence<MPL::TypeTuple<T, Rest...> >
      {
        using Type = MakeIndexSequence<Arity<T>::value>;
      };

      template<class T>
      using AritySequence = typename MakeAritySequence<T>::Type;

      /**Instantiate F on E and the recursively determined result of its arguments.*/
      template<class E, template<class... Arg> class F, std::size_t... ArgIndex>
      struct TraverseHelper<
        E, F, IndexSequence<ArgIndex...>,
        std::enable_if_t<!MPL::IsTypeTupleV<E> && !IsSelfExpression<E>::value && IsExpression<E>::value> >
      {
        using Type =
          typename F<E,
                     typename TraverseHelper<Operand<ArgIndex, E>, F,
                                             AritySequence<Operand<ArgIndex, E> > >::Type...>::Type;
      };

      /**Recursion end-point.*/
      template<class E, template<class... Arg> class F, std::size_t... ArgIndex>
      struct TraverseHelper<
        E, F, IndexSequence<ArgIndex...>,
        std::enable_if_t<!MPL::IsTypeTupleV<E> && (IsSelfExpression<E>::value || !IsExpression<E>::value)> >
      {
        using Type = typename F<E>::Type;
      };

      /**Invoke F on MPL::TypeTuple<E, TreePos>. */
      template<class E, std::size_t... TreePos, template<class... Arg> class F, std::size_t... ArgIndex>
      struct TraverseHelper<
        MPL::TypeTuple<E, IndexSequence<TreePos...> >, F, IndexSequence<ArgIndex...>,
        std::enable_if_t<!IsSelfExpression<E>::value && IsExpression<E>::value> >
      {
        using Type =
          typename F<MPL::TypeTuple<E, IndexSequence<TreePos...> >,
                     typename TraverseHelper<MPL::TypeTuple<Operand<ArgIndex, E>, IndexSequence<TreePos..., ArgIndex> >, F,
                                             AritySequence<Operand<ArgIndex, E> > >::Type...>::Type;
      };

      /**Recursion end-point.*/
      template<class E, std::size_t... TreePos, template<class... Arg> class F, std::size_t... ArgIndex>
      struct TraverseHelper<
        MPL::TypeTuple<E, IndexSequence<TreePos...> >, F, IndexSequence<ArgIndex...>,
        std::enable_if_t<IsSelfExpression<E>::value || !IsExpression<E>::value> >
      {
        using Type = typename F<MPL::TypeTuple<E, IndexSequence<TreePos...> > >::Type;
      };

      /**Invoke F on MPL::TypeTuple<E, Parent>. */
      template<class E, class Parent, template<class... Arg> class F, std::size_t... ArgIndex>
      struct TraverseHelper<
        MPL::TypeTuple<E, Parent>, F, IndexSequence<ArgIndex...>,
        std::enable_if_t<!IsSequence<Parent>::value && !IsSelfExpression<E>::value && IsExpression<E>::value> >
      {
        using Type =
          typename F<MPL::TypeTuple<E, Parent>,
                     typename TraverseHelper<
                       MPL::TypeTuple<Operand<ArgIndex, E>, E>,
                       F,
                       AritySequence<Operand<ArgIndex, E> >
                       >::Type...
                     >::Type;
      };

      /**Recursion end-point.*/
      template<class E, class Parent, template<class... Arg> class F, std::size_t... ArgIndex>
      struct TraverseHelper<
        MPL::TypeTuple<E, Parent>, F, IndexSequence<ArgIndex...>,
        std::enable_if_t<!IsSequence<Parent>::value && (IsSelfExpression<E>::value || !IsExpression<E>::value)> >
      {
        using Type = typename F<MPL::TypeTuple<E, Parent> >::Type;
      };

      /**Invoke F on MPL::TypeTuple<E, TreePos, Parent>. */
      template<class E, std::size_t... TreePos, class Parent, template<class... Arg> class F, std::size_t... ArgIndex>
      struct TraverseHelper<
        MPL::TypeTuple<E, IndexSequence<TreePos...>, Parent>, F, IndexSequence<ArgIndex...>,
        std::enable_if_t<!IsSelfExpression<E>::value && IsExpression<E>::value> >
      {
        using Type =
          typename F<MPL::TypeTuple<E, IndexSequence<TreePos...>, Parent>,
                     typename TraverseHelper<
                       MPL::TypeTuple<Operand<ArgIndex, E>, IndexSequence<TreePos..., ArgIndex>, E>,
                       F,
                       AritySequence<Operand<ArgIndex, E> >
                       >::Type...
                     >::Type;
      };

      /**Recursion end-point.*/
      template<class E, class TreePos, class Parent, template<class... Arg> class F, std::size_t... ArgIndex>
      struct TraverseHelper<
        MPL::TypeTuple<E, TreePos, Parent>, F, IndexSequence<ArgIndex...>,
        std::enable_if_t<IsSelfExpression<E>::value || !IsExpression<E>::value> >
      {
        using Type = typename F<MPL::TypeTuple<E, TreePos, Parent> >::Type;
      };

    }

    /**Traverse the given expression type and instantiate F for each
     * contained expression template. F is invoked with the expression
     * type as first argument and the recursively determnined types
     * generated by instantiating F on all operands. For example for a
     * binary plus expression with two terminal expressions Plus<A, B>
     * the generated type would be:
     *
     * @code
     * typename F<typename Plus<A, B>, typename F<A>::Type, typename F<B>::Type>::Type;
     * @endcode
     *
     * E may be a MPL::TypeTuple of an expression and its position
     * signature and optionally the parent expression. If so, this
     * information is promoted to the operands by adding their
     * position in the expression tree.
     */
    template<class E, template<class... Arg> class F>
    using Traverse = typename TraverseHelper<E, F, AritySequence<E> >::Type;

    /**@internal Helper for Examine.
     *
     * @param AccuFuncor This must be one of LogicalAnd, LogicalOr,
     * BitwiseAnd, BitwiseOr, Sum, Prod as defined in
     * mpl/accumulate.hh
     *
     * @param ExprPredicate The predicate "applied" to each operand
     * and the outer expression in turn. ExprPredicate will be
     * instantiated with T and Rest...: ExprPredicate<T,
     * Rest...>. Note that T may be a
     * @code MPL::TypeTuple<Expr, TreePos, Parent> @endcode.
     *
     * @param Rest Additional arguments passed to the ExprPredicate.
     */
    template<class T, template<class ...> class AccuFunctor, template<class... Arg> class ExprPredicate, class... Rest>
    struct FunctorFactory
    {
      /**Just accumulate all values, F must accept the expression as
       * first template parameter.
       */
      template<class E, class... Args>
      struct Accumulate
      {
        using Type = AccuFunctor<Sequence<T, ExprPredicate<E, Rest...>::value, Args::value...> >;
      };

      /**Feed the accumulated values for the operands back into F.*/
      template<class E, class... Args>
      struct Recurse
      {
        using Type = ExprPredicate<E, AccuFunctor<Sequence<T, Args::value...> >, Rest...>;
      };
    };

    /**Combine the result of the arguments with the result from the
     * current expression.
     */
    template<class E, class T, template<class...> class AccuFunctor, template<class...> class ExprPredicate, class... Rest>
    using ExamineRecurse = Traverse<E, FunctorFactory<T, AccuFunctor, ExprPredicate, Rest...>::template Recurse>;

    /**Accumulate the results over all sub-expressions.*/
    template<class E, class T, template<class...> class AccuFunctor, template<class... Arg> class ExprPredicate, class... Rest>
    using Examine = Traverse<E, FunctorFactory<T, AccuFunctor, ExprPredicate, Rest...>::template Accumulate>;

  } // NS ExamineImpl

  using ExamineImpl::Examine;
  using ExamineImpl::ExamineRecurse;

  /**OR over all contained sub-expression.
   *
   * @param E The expression to examine. E maybe an expression or a
   * MPL::TypeTupe<Expr, TreePos, Parent> where Parent is
   * optional. TreePos and Parent maybe TrueType in which the proper
   * default is chosen. If E is a MPL::TypeTuple then the associated
   * information (parent, tree position) is updated while traversing
   * the experssion tree.
   *
   * @param ExprPrediate Is instantiated in turn as ExprPredicate<T, Rest..>.
   */
  template<class E, template<class... Arg> class ExprPredicate, class... Rest>
  using ExamineOr = BoolConstant<Examine<E, bool, LogicalOr, ExprPredicate, Rest...>::value>;

  /**AND over all contained sub-expression, where F::Type must be convertible to bool.*/
  template<class E, template<class... Arg> class F, class... Rest>
  using ExamineAnd = BoolConstant<Examine<E, bool, LogicalAnd, F, Rest...>::value>;

  /**ADD over all contained sub-expression, where F::Type must be
   * convertible to std::size_t.
   */
  template<class E, template<class... Arg> class F, class... Rest>
  using ExamineAdd = IndexConstant<Examine<E, std::size_t, Sum, F, Rest...>::value>;

  template<class E, class TreePos, class Parent, template<class...> class F, class... Rest>
  struct ExamineTreeOr
    : ExamineOr<MPL::TypeTuple<E, TreePos, Parent>, F, Rest...>
  {};

  template<class E, class TreePos, template<class...> class F, class... Rest>
  struct ExamineTreeOr<E, TreePos, FalseType, F, Rest...>
    : ExamineOr<MPL::TypeTuple<E, TreePos>, F, Rest...>
  {};

  template<class E, class Parent, template<class...> class F, class... Rest>
  struct ExamineTreeOr<E, FalseType, Parent, F, Rest...>
    : ExamineOr<MPL::TypeTuple<E, Parent>, F, Rest...>
  {};

  template<class E, template<class...> class F, class... Rest>
  struct ExamineTreeOr<E, FalseType, FalseType, F, Rest...>
    : ExamineOr<E, F, Rest...>
  {};

  template<class E, class TreePos, class Parent, template<class...> class F, class... Rest>
  struct ExamineTreeAnd
    : ExamineAnd<MPL::TypeTuple<E, TreePos, Parent>, F, Rest...>
  {};

  template<class E, class TreePos, template<class...> class F, class... Rest>
  struct ExamineTreeAnd<E, TreePos, FalseType, F, Rest...>
    : ExamineAnd<MPL::TypeTuple<E, TreePos>, F, Rest...>
  {};

  template<class E, class Parent, template<class...> class F, class... Rest>
  struct ExamineTreeAnd<E, FalseType, Parent, F, Rest...>
    : ExamineAnd<MPL::TypeTuple<E, Parent>, F, Rest...>
  {};

  template<class E, template<class...> class F, class... Rest>
  struct ExamineTreeAnd<E, FalseType, FalseType, F, Rest...>
    : ExamineAnd<E, F, Rest...>
  {};

} // NS Dune::ACFem::Expressions

#endif // __DUNE_ACFEM_EXPRESSIONS_EXAMINE_HH__
