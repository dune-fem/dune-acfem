#ifndef __DUNE_ACFEM_EXPRESSIONS_SUBEXPRESSION_HH__
#define __DUNE_ACFEM_EXPRESSIONS_SUBEXPRESSION_HH__

#include "expressionoperations.hh"
#include "interface.hh"
#include "densestorage.hh"
#include "transform.hh"
#include "../common/literals.hh"

namespace Dune
{

  namespace ACFem
  {

    namespace Expressions
    {
      using namespace Literals;

      /**@addtogroup ExpressionTemplates
       *
       * @{
       *
       */

      /**@addtogroup ExpressionTraits
       *
       * @{
       */

      /**Recurse to the wrapped expression if T fulfills
       * IsSubExpressionExpression<T>.
       */
      template<class T, std::enable_if_t<IsSubExpressionExpression<T>::value, int> = 0>
      constexpr decltype(auto) subExpressionExpression(T&& t)
      {
        return std::forward<T>(t).template operand<1>();
      }

      /**Don't recurse to the wrapped expression if T does not fulfill
       * IsSubExpressionExpression<T>.
       */
      template<
        class T,
        std::enable_if_t<(!IsSubExpressionExpression<T>::value
                          && !IsClosure<T>::value
        ), int> = 0>
      constexpr decltype(auto) subExpressionExpression(T&& t)
      {
        return std::forward<T>(t);
      }

      template<class T, std::enable_if_t<IsClosure<T>::value, int> = 0>
      constexpr decltype(auto) subExpressionExpression(T&& t)
      {
        return subExpressionExpression(asExpression(std::forward<T>(t)));
      }

      template<std::size_t... OperandPos, class T, std::enable_if_t<IsExpression<T>::value, int> = 0>
      auto subExpression(T&& t, IndexSequence<OperandPos...> = IndexSequence<OperandPos...>{})
      {
        using TreePos = IndexSequence<OperandPos...>;
        return subExpression(DenseStorageType<T>{}, std::forward<T>(t), TreePos{});
      }

      template<template<class...> class Storage, std::size_t... OperandPos, class T, std::enable_if_t<IsExpression<T>::value, int> = 0>
      auto subExpression(T&& t, IndexSequence<OperandPos...> = IndexSequence<OperandPos...>{})
      {
        using TreePos = IndexSequence<OperandPos...>;
        return subExpression(Storage<T>(), std::forward<T>(t), TreePos{});
      }

      //! @} ExpressionTraits

      namespace impl {
        template<class Functor>
        struct EvaluateSubExpression;
        template<class Optimize>
        struct ExpandSubExpression;
      }

      /**@addtogroup SubExpressions
       *
       * Sub-expression support.
       *
       * @{
       */

      struct SExpForward
      {
        template<class Src>
        constexpr decltype(auto) operator()(Src&& src) const
        {
          return std::forward<Src>(src);
        }
      };

      struct SExpZero
      {
        template<class Dst>
        constexpr auto operator()(Dst&& dst) const
        {
          return zero(std::forward<Dst>(dst));
        }
      };

      /**Copy the value of the contained sub-expression (operand
       * no. 1) to operand 0 which acts as a cache. Of course, this
       * can only work if operand 0 is mutable in the given context
       * and if operand 1 can be assignt to operand 0.
       *
       * @param t The subexpression expression.
       *
       * @param clear Defaults to false. If set to true then the cache
       * will be set to 0. It is assume that the cache is assignable
       * from its canonical zero() object if it is assignable from the
       * subexpression.
       */
      template<
        class T,
        class Functor = SExpForward,
        std::enable_if_t<(IsSubExpressionExpression<T>::value
                          && std::is_assignable<decltype(std::declval<T>().operand(0_c)), Operand<1, T> >::value
          ), int> = 0>
      void evaluateSubExpression(T&& t, Functor&& f = Functor{})
      {
        if constexpr (!IsConstantExprArg<Operand<1, T> >::value) {
          // The sub-expression might again contain sub-expression which
          // should be evaluated first in order to finally yield the
          // correct result.
          traverse(std::forward<T>(t).operand(1_c), impl::EvaluateSubExpression<Functor>{f});
          std::forward<T>(t).operand(0_c) = asExpression(std::forward<Functor>(f)(std::forward<T>(t).operand(1_c)));
          //std::clog << "Evaluate " <<  asExpression(std::forward<Functor>(f)(std::forward<T>(t).operand(1_c))).name() << std::endl;
        }
      }

      /**Do-nothing dummy for expressions where
       * evaluateSubExpression() would fail otherwise.
       *
       * This makes it legal to to keep references to constant
       * sub-expressions inside an expression in order to support
       * common sub-expression "elimination". The underlying
       * sub-expressions must then be updated separately (as they are
       * references there must be an "external" object that is
       * referenced).
       *
       * @param t An expression which must be SubExpressionOperation.
       *
       * @param clear Ignored.
       */
      template<
        class T,
        class Functor = SExpForward,
        std::enable_if_t<(IsSubExpressionExpression<T>::value
                          && !std::is_assignable<decltype(std::declval<T>().operand(0_c)), Operand<1, T> >::value
        ), int> = 0>
      void evaluateSubExpression(T&& t, Functor = Functor{})
      {}

      /**Strip a possible closure and recurse to the proper expression.*/
      template<class T, class Functor = SExpForward, std::enable_if_t<IsClosure<T>::value, int> = 0>
      void evaluateSubExpression(T&& t, Functor&& f = Functor{})
      {
        evaluateSubExpression(asExpression(std::forward<T>(t)), std::forward<Functor>(f));
      }

      template<class T, class Optimize = OptimizeTop, class Functor = impl::ExpandSubExpression<Optimize> >
      constexpr decltype(auto) expandSubExpression(T&& t, Optimize = Optimize{}, Functor = Functor{})
      {
        static_assert(IsSubExpressionExpression<T>::value,
                      "Attempt to expand subexpression of non subexpression-expression.");
        // make sure we really get the contained type
        return transform(static_cast<Operand<1, T> >(std::forward<T>(t).operand(1_c)), Functor{}, Operate<Optimize>{});
      }

      namespace impl {

        /**Operation functor for transform() which evaluates all
         * contained sub-expressions.
         *
         * @param Clear If @c true then instead of evaluation the
         * sub-expressions all writable cache expressions are set to
         * their canonical zero() object.
         *
         * @note This may not be what one wants to do if an expression
         * contains common subexpressions. How, common subexpression
         * optimization requires more knowledge and work,
         * e.g. replacing all common subexpressions by a reference to
         * a copy outside of the expression, or by replacing operand 0
         * (the storage cache for the value of the subexpression) by a
         * reference to an external storage. So this function is
         * rather a proof of concept.
         */
        template<class Functor>
        struct EvaluateSubExpression
        {
          template<class T, std::enable_if_t<IsSubExpressionExpression<T>::value, int> = 0>
          constexpr auto operator()(T&& t) const
          {
            evaluateSubExpression(std::forward<T>(t), f_);
            return 0;
          }

          Functor f_;
        };

        template<class Optimize>
        struct ExpandSubExpression
        {
          template<class T, std::enable_if_t<IsSubExpressionExpression<T>::value, int> = 0>
          constexpr decltype(auto) operator()(T&& t) const
          {
            return expandSubExpression(std::forward<T>(t), Optimize{});
          }
#if 0
          template<class T, std::enable_if_t<(IsPlaceholderExpression<T>::value
                                              || IsIndeterminateExpression<T>::value
            ), int> = 0>
          constexpr decltype(auto) operator()(T&& t) const
          {
            return std::forward<T>(t);
          }
#endif
        };

      } // NS impl

      /**Evaluate all contained subexpressions. Note that references
       * to constant sub-expressions are silently ignored.
       *
       * @param t Just some expression.
       *
       * @param clear If @c true then the caches are cleared instead
       * of being filled with the evaluated sub-expressions.
       */
      template<class T, class Functor = SExpForward>
      void evaluateAllSubExpressions(T&& t, Functor&& f = Functor{})
      {
        static_assert(IsExpression<T>::value, "Called with something which is not an expression.");

        traverse(std::forward<T>(t), impl::EvaluateSubExpression<Functor>{f});
      }

      /**Expand all contained sub-expressions.*/
      template<class T, class Optimize = OptimizeTop>
      constexpr auto expandAllSubExpressions(T&& t, Optimize = Optimize{})
      {
        return transform(std::forward<T>(t), impl::ExpandSubExpression<Optimize>{}, Operate<Optimize>{});
      }

      //! @} SubExpressions

      //! @} ExpressionTemplates

    } // Expressions

    template<class T>
    struct ExpressionTraits<T, std::enable_if_t<IsSubExpressionExpression<T>::value> >
      : ExpressionTraits<Expressions::Operand<1, T> >
    {
      using BaseType = ExpressionTraits<Expressions::Operand<1, T> >;
      using ExpressionType = std::decay_t<T>;
      static constexpr bool isTypedValue = false;
      static constexpr bool isConstant = BaseType::isConstant || BaseType::isTypedValue;
    };


  } // ACFem

} // Dune

#endif // __DUNE_ACFEM_EXPRESSIONS_SUBEXPRESSION_HH__
