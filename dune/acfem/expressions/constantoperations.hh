#ifndef __DUNE_ACFEM_EXPRESSIONS_INTCONSTOPERATIONS_HH__
#define __DUNE_ACFEM_EXPRESSIONS_INTCONSTOPERATIONS_HH__

#include "../common/types.hh"
#include "../common/fractionconstant.hh"
#include "../common/namedconstant.hh"
#include "expressiontraits.hh"
#include "fieldpromotion.hh"
#include "storage.hh"
#include "weight.hh"

namespace Dune
{
  template<class I, I N, I D>
  struct FieldTraits<ACFem::TypedValue::FractionConstant<I, N, D> >
    : FieldTraits<ACFem::ConditionalType<D == 1, I, ACFem::FloatingPointClosure<I> > >
  {};

  template<class T, char... Name>
  struct FieldTraits<ACFem::TypedValue::NamedConstant<T, Name...> >
    : FieldTraits<T>
  {};

  namespace ACFem
  {

    using TypedValue::toString;
    using TypedValue::FractionConstant;
    using TypedValue::Fraction;
    using TypedValue::IntFraction;
    using TypedValue::intFraction;
    using TypedValue::numerator;
    using TypedValue::denominator;
    using TypedValue::Sign;
    using TypedValue::sign;
    using TypedValue::IsSign;

    /**@addtogroup ExpressionTemplates
     *
     * @{
     *
     */

    /**@addtogroup ExpressionTraits
     * @{
     */

    /**FractionConstants are scalars.*/
    template<class I, I N, I D>
    struct IsScalar<TypedValue::FractionConstant<I, N, D> >
      : TrueType
    {};

    /**NamedConstants are scalars if the wrapped type is.*/
    template<class T, char... Name>
    struct IsScalar<TypedValue::NamedConstant<T, Name...> >
      : IsScalar<T>
    {};

    /**Some FractionConstants are integral.*/
    template<class I, I N>
    struct IsIntegral<TypedValue::FractionConstant<I, N, 1> >
      : TrueType
    {};

    /**Allow scalars to mate with FractionConstants, but do not
     * interfere with std::integral_constant and expression types.
     */
    template<class T, bool Exclusive = true>
    using IsFractionOperand =
      BoolConstant<(IsScalar<T>::value
                    && !(Exclusive && IsFractionConstant<T>::value)
                    && !IsIntegralConstant<T>::value // leave integral constants alone
                    && !IsExpression<T>::value // don't interfere with expression optimizations
      )>;

    /**Use the zero fraction as canonical zero element for scalars.
     */
    template<class T, std::enable_if_t<IsFractionOperand<T, false>::value, int> = 0>
    constexpr auto zero(T&& t)
    {
      return IntFraction<0, 1>{};
    }

    /**Use the one fraction as canonical zero element for scalars.
     */
    template<class T, std::enable_if_t<IsFractionOperand<T, false>::value, int> = 0>
    constexpr auto one(T&& t)
    {
      return IntFraction<1, 1>{};
    }

    /**Override ExpressionTraits for FractionConstant.*/
    template<class Int, Int N, Int D>
    struct ExpressionTraits<FractionConstant<Int, N, D> >
    {
     protected:
      static constexpr ssize_t nSign = (N == 0 ? 0 : (N > 0 ? 1 : -1));
      static constexpr ssize_t dSign = (D == 0 ? 0 : (D > 0 ? 1 : -1));
     public:
      static constexpr bool isSemiPositive = nSign * dSign >= 0;
      static constexpr bool isSemiNegative = nSign * dSign <= 0;
      static constexpr bool isZero = N == 0;
      static constexpr bool isOne = N*D == 1;
      static constexpr bool isMinusOne = N*D == -1;
      static constexpr bool isNonZero = !isZero;
      static constexpr bool isNonSingular = !isZero && D != 0;
      static constexpr bool isPositive = nSign * dSign > 0;
      static constexpr bool isNegaive = nSign * dSign < 0;

      static constexpr bool isVolatile = false;
      static constexpr bool isIndependent = true;
      static constexpr bool isConstant = true;
      static constexpr bool isTypedValue = true;

      using Sign = ExpressionSign<isNonSingular, isSemiPositive, isSemiNegative, 0>;
    };

    /**Override ExpressionTraits for NamedConstant.*/
    template<class T, char... Name>
    struct ExpressionTraits<TypedValue::NamedConstant<T, Name...> >
      : ExpressionTraits<T>
    {
      static constexpr bool isVolatile = false;
      static constexpr bool isIndependent = true;
      static constexpr bool isConstant = true;
      static constexpr bool isTypedValue = true;

      static_assert(IsScalar<T>::value, "NamedConstant only supported for scalars.");

      using Sign = PositiveExpressionSign;
    };

    namespace Expressions {

      template<class T, char... Name>
      constexpr inline std::size_t WeightV<TypedValue::NamedConstant<T, Name...> > = (0 + ... + ((std::size_t)Name));

      template<class Int, Int N, Int D>
      constexpr inline std::size_t WeightV<FractionConstant<Int, N, D> > = 1UL;

    }

    //!@} ExpresssionTraits

    namespace TypedValue {

      /**@addtogroup ConstantOperations
       *
       * @{
       */

      /**@name SmallValuePatterns
       *
       * @{
       */

      /**0*t.*/
      template<class I, class T, std::enable_if_t<IsFractionOperand<T>::value, int> = 0>
      constexpr auto operator*(FractionConstant<I, 0, 1>, T&& t)
      {
        return zero(std::forward<T>(t));
      }

      /**t*=0*/
      template<class I, class T, std::enable_if_t<IsFractionOperand<T>::value, int> = 0>
      constexpr auto& operator*=(T& t, FractionConstant<I, 0, 1>)
      {
        return (t = zero(t));
      }

      /**1*t.*/
      template<class I, class T, std::enable_if_t<IsFractionOperand<T>::value, int> = 0>
      constexpr auto operator*(FractionConstant<I, 1, 1>, T&& t)
      {
        return t;
      }

      /**t*=1.*/
      template<class I, class T, std::enable_if_t<IsFractionOperand<T>::value, int> = 0>
      constexpr auto& operator*=(T& t, FractionConstant<I, 1, 1>)
      {
        return t;
      }

      /**-1*t.*/
      template<class I, class T, std::enable_if_t<IsFractionOperand<T>::value, int> = 0>
      constexpr auto operator*(FractionConstant<I, -1, 1>, T&& t)
      {
        return -t;
      }

      /**t*=-1.*/
      template<class I, class T, std::enable_if_t<IsFractionOperand<T>::value, int> = 0>
      constexpr auto& operator*=(T& t, FractionConstant<I, -1, 1>)
      {
        return -t;
      }

      /**0 + t.*/
      template<class I, class T, std::enable_if_t<IsFractionOperand<T>::value, int> = 0>
      constexpr auto operator+(FractionConstant<I, 0, 1>, T&& t)
      {
        return t;
      }

      /**t+=0.*/
      template<class I, class T, std::enable_if_t<IsFractionOperand<T>::value, int> = 0>
      constexpr auto& operator+=(T& t, FractionConstant<I, 0, 1>)
      {
        return t;
      }

      /**0 - t.*/
      template<class I, class T, std::enable_if_t<IsFractionOperand<T>::value, int> = 0>
      constexpr auto operator-(FractionConstant<I, 0, 1>, T&& t)
      {
        return -t;
      }

      /**t-=0.*/
      template<class I, class T, std::enable_if_t<IsFractionOperand<T>::value, int> = 0>
      constexpr auto& operator-=(T& t, FractionConstant<I, 0, 1>)
      {
        return t;
      }

      /**0/t.*/
      template<class I, class T, std::enable_if_t<IsFractionOperand<T>::value, int> = 0>
      constexpr auto operator/(FractionConstant<I, 0, 1>, T&& t)
      {
        return zero(std::forward<T>(t));
      }

      /**t/1.*/
      template<class I, class T, std::enable_if_t<IsFractionOperand<T>::value, int> = 0>
      constexpr auto operator/(T&& t, FractionConstant<I, 1, 1>)
      {
        return t;
      }

      /**t/=1.*/
      template<class I, class T, std::enable_if_t<IsFractionOperand<T>::value, int> = 0>
      constexpr auto& operator/(T& t, FractionConstant<I, 1, 1>)
      {
        return t;
      }

      /**t/-1.*/
      template<class I, class T, std::enable_if_t<IsFractionOperand<T>::value, int> = 0>
      constexpr auto operator/(T&&t, FractionConstant<I, -1, 1>)
      {
        return -t;
      }

      /**t/=-1.*/
      template<class I, class T, std::enable_if_t<IsFractionOperand<T>::value, int> = 0>
      constexpr auto& operator/(T& t, FractionConstant<I, -1, 1>)
      {
        return -t;
      }

      ///@} SmallValuePatterns

      /**@name IntegralPatterns
       *
       * Expressions with denominator 1.
       *
       * @{
       */

      template<
        class I, I N, class T,
        std::enable_if_t<(N*N > 1
                          && IsFractionOperand<T>::value), int> = 0>
      constexpr auto operator*(FractionConstant<I, N, 1>, T&& t)
      {
        return N*t;
      }

      template<
        class I, I N, I D, class T,
        std::enable_if_t<(N*N > 1
                          && D == 1
                          && IsFractionOperand<T>::value), int> = 0>
      constexpr auto& operator*=(T& t, FractionConstant<I, N, D>)
      {
        return t *= N;
      }

      template<
        class I, I N, I D, class T,
        std::enable_if_t<(N != 0
                          && D == 1
                          && IsFractionOperand<T>::value), int> = 0>
      constexpr auto operator+(FractionConstant<I, N, D>, T&& t)
      {
        return N+t;
      }

      template<
        class I, I N, I D, class T,
        std::enable_if_t<(N != 0
                          && D == 1
                          && IsFractionOperand<T>::value), int> = 0>
      constexpr auto& operator+=(T& t, FractionConstant<I, N, D>)
      {
        return t += N;
      }

      template<
        class I, I N, I D, class T,
        std::enable_if_t<(N != 0
                          && D == 1
                          && IsFractionOperand<T>::value), int> = 0>
      constexpr auto operator-(FractionConstant<I, N, D>, T&& t)
      {
        return N-t;
      }

      template<
        class I, I N, I D, class T,
        std::enable_if_t<(N != 0
                          && D == 1
                          && IsFractionOperand<T>::value), int> = 0>
      constexpr auto operator/(FractionConstant<I, N, D>, T&& t)
      {
        using FType = FieldPromotionType<T, I>;
        return (FType)N / (FType)t;
      }

      template<
        class I, I N, I D, class T,
        std::enable_if_t<(N != 0
                          && D == 1
                          && IsFractionOperand<T>::value), int> = 0>
      constexpr auto& operator/=(T& t, FractionConstant<I, N, D>)
      {
        return t /= N;
      }

      template<
        class I, I N, I D, class T,
        std::enable_if_t<(D == 1
                          && IsFractionOperand<T>::value), int> = 0>
      constexpr bool operator==(FractionConstant<I, N, D>, T&& t)
      {
        return N == t;
      }

      template<
        class I, I N, I D, class T,
        std::enable_if_t<(D == 1
                          && IsFractionOperand<T>::value), int> = 0>
      constexpr bool operator!=(FractionConstant<I, N, D>, T&& t)
      {
        return N != t;
      }

      template<
        class I, I N, I D, class T,
        std::enable_if_t<(D == 1
                          && IsFractionOperand<T>::value), int> = 0>
      constexpr bool operator<=(FractionConstant<I, N, D>, T&& t)
      {
        return N <= t;
      }

      template<
        class I, I N, I D, class T,
        std::enable_if_t<(D == 1
                          && IsFractionOperand<T>::value), int> = 0>
      constexpr bool operator<(FractionConstant<I, N, D>, T&& t)
      {
        return N < t;
      }

      template<
        class I, I N, I D, class T,
        std::enable_if_t<(D == 1
                          && IsFractionOperand<T>::value), int> = 0>
      constexpr bool operator>=(FractionConstant<I, N, D>, T&& t)
      {
        return N >= t;
      }

      template<
        class I, I N, I D, class T,
        std::enable_if_t<(D == 1
                          && IsFractionOperand<T>::value), int> = 0>
      constexpr bool operator>(FractionConstant<I, N, D>, T&& t)
      {
        return N > t;
      }

      //!@} IntegralPatterns

      /**@name GeneralPatterns
       *
       * Expressions with denominator != 1.
       *
       * @{
       */

      template<
        class I, I N, I D, class T,
        std::enable_if_t<(N != 0
                          && D != 1
                          && IsFractionOperand<T>::value), int> = 0>
      constexpr auto operator*(FractionConstant<I, N, D>, T&& t)
      {
        using FType = FieldPromotionType<T, I>;
        return (FType)N/(FType)D*t;
      }

      template<
        class I, I N, I D, class T,
        std::enable_if_t<(N != 0
                          && D != 1
                          && IsFractionOperand<T>::value), int> = 0>
      constexpr auto& operator*=(T& t, FractionConstant<I, N, D>)
      {
        using FType = FieldPromotionType<T, I>;
        return t *= (FType)N/(FType)D;
      }

      template<
        class I, I N, I D, class T,
        std::enable_if_t<(N != 0
                          && D != 1
                          && IsFractionOperand<T>::value), int> = 0>
      constexpr auto operator+(FractionConstant<I, N, D>, T&& t)
      {
        using FType = FieldPromotionType<T, I>;
        return (FType)N/(FType)D+t;
      }

      template<
        class I, I N, I D, class T,
        std::enable_if_t<(N != 0
                          && D != 1
                          && IsFractionOperand<T>::value), int> = 0>
      constexpr auto& operator+=(T& t, FractionConstant<I, N, D>)
      {
        using FType = FieldPromotionType<T, I>;
        return t += (FType)N/(FType)D;
      }

      template<
        class I, I N, I D, class T,
        std::enable_if_t<(N != 0
                          && D != 1
                          && IsFractionOperand<T>::value), int> = 0>
      constexpr auto operator-(FractionConstant<I, N, D>, T&& t)
      {
        using FType = FieldPromotionType<T, I>;
        return (FType)N/(FType)D-t;
      }

      template<
        class I, I N, I D, class T,
        std::enable_if_t<(N != 0
                          && D != 1
                          && IsFractionOperand<T>::value), int> = 0>
      constexpr auto& operator-(T& t, FractionConstant<I, N, D>)
      {
        using FType = FieldPromotionType<T, I>;
        return t -= (FType)N/(FType)D;
      }

      template<
        class I, I N, I D, class T,
        std::enable_if_t<(N != 0
                          && D != 1
                          && IsFractionOperand<T>::value), int> = 0>
      constexpr auto operator/(FractionConstant<I, N, D>, T&& t)
      {
        using FType = FieldPromotionType<T, I>;
        return (FType)N / (FType)D / (FType)t;
      }

      template<
        class I, I N, I D, class T,
        std::enable_if_t<(N != 0
                          && D != 1
                          && IsFractionOperand<T>::value), int> = 0>
      constexpr auto& operator/(T& t, FractionConstant<I, N, D>)
      {
        using FType = FieldPromotionType<T, I>;
        return t /= (FType)N / (FType)D;
      }

      template<
        class I, I N, I D, class T,
        std::enable_if_t<(D != 1
                          && IsFractionOperand<T>::value), int> = 0>
      constexpr bool operator==(FractionConstant<I, N, D>, T&& t)
      {
        return N == D * t;
      }

      template<
        class I, I N, I D, class T,
        std::enable_if_t<(D != 1
                          && IsFractionOperand<T>::value), int> = 0>
      constexpr bool operator!=(FractionConstant<I, N, D>, T&& t)
      {
        return N != D * t;
      }

      template<
        class I, I N, I D, class T,
        std::enable_if_t<(D != 1
                          && IsFractionOperand<T>::value), int> = 0>
      constexpr bool operator<=(FractionConstant<I, N, D>, T&& t)
      {
        return N <= D * t;
      }

      template<
        class I, I N, I D, class T,
        std::enable_if_t<(D != 1
                          && IsFractionOperand<T>::value), int> = 0>
      constexpr bool operator<(FractionConstant<I, N, D>, T&& t)
      {
        return N < D * t;
      }

      template<
        class I, I N, I D, class T,
        std::enable_if_t<(D != 1
                          && IsFractionOperand<T>::value), int> = 0>
      constexpr bool operator>=(FractionConstant<I, N, D>, T&& t)
      {
        return N >= D * t;
      }

      template<
        class I, I N, I D, class T,
        std::enable_if_t<(D != 1
                          && IsFractionOperand<T>::value), int> = 0>
      constexpr bool operator>(FractionConstant<I, N, D>, T&& t)
      {
        return N > D * t;
      }

      template<
        class I, I N, I D, class T,
        std::enable_if_t<IsFractionOperand<T>::value, int> = 0>
      constexpr auto min(FractionConstant<I, N, D> f, T&& t)
      {
        using FType = FieldPromotionType<T, I>;
        return f <= t ? (FType)f : (FType)t;
      }

      template<
        class I, I N, I D, class T,
        std::enable_if_t<IsFractionOperand<T>::value, int> = 0>
      constexpr auto max(FractionConstant<I, N, D> f, T&& t)
      {
        using FType = FieldPromotionType<T, I>;
        return f >= t ? (FType)f : (FType)t;
      }

      //!@} GeneralPatterns

      /**@name CommutationPatterns
       *
       * @{
       */

      /** t * fraction -> fraction * t. */
      template<
        class I, I N, I D, class T,
        std::enable_if_t<IsFractionOperand<T>::value, int> = 0>
      constexpr auto operator*(T&& t, FractionConstant<I, N, D> f)
      {
        return f * t;
      }

      /** t / fraction -> (1/fraction) * t. */
      template<
        class I, I N, I D, class T,
        std::enable_if_t<IsFractionOperand<T>::value, int> = 0>
      constexpr auto operator/(T&& t, FractionConstant<I, N, D> f)
      {
        return Fraction<I, D, N>{} * t;
      }

      /** t + fraction -> fraction + t. */
      template<
      class I, I N, I D, class T,
      std::enable_if_t<IsFractionOperand<T>::value, int> = 0>
      constexpr auto operator+(T&& t, FractionConstant<I, N, D> f)
      {
        return f + t;
      }

      /** t - fraction -> -fraction + t. */
      template<
        class I, I N, I D, class T,
        std::enable_if_t<IsFractionOperand<T>::value, int> = 0>
      constexpr auto operator-(T&& t, FractionConstant<I, N, D> f)
      {
        return -f + t;
      }

      /** t == fraction -> fraction == t. */
      template<
        class I, I N, I D, class T,
        std::enable_if_t<IsFractionOperand<T>::value, int> = 0>
      constexpr bool operator==(T&& t, FractionConstant<I, N, D> f)
      {
        return f == t;
      }

      /** t != fraction -> fraction != t. */
      template<
        class I, I N, I D, class T,
        std::enable_if_t<IsFractionOperand<T>::value, int> = 0>
      constexpr bool operator!=(T&& t, FractionConstant<I, N, D> f)
      {
        return f != t;
      }

      /** t <= fraction -> fraction >= t. */
      template<
        class I, I N, I D, class T,
        std::enable_if_t<IsFractionOperand<T>::value, int> = 0>
      constexpr bool operator<=(T&& t, FractionConstant<I, N, D> f)
      {
        return f >= t;
      }

      /** t < fraction -> fraction > t. */
      template<
        class I, I N, I D, class T,
        std::enable_if_t<IsFractionOperand<T>::value, int> = 0>
      constexpr bool operator<(T&& t, FractionConstant<I, N, D> f)
      {
        return f > t;
      }

      /** t >= fraction -> fraction <= t. */
      template<
        class I, I N, I D, class T,
        std::enable_if_t<IsFractionOperand<T>::value, int> = 0>
      constexpr bool operator>=(T&& t, FractionConstant<I, N, D> f)
      {
        return f <= t;
      }

      /** t > fraction -> fraction < t. */
      template<
        class I, I N, I D, class T,
        std::enable_if_t<IsFractionOperand<T>::value, int> = 0>
      constexpr bool operator>(T&& t, FractionConstant<I, N, D> f)
      {
        return f < t;
      }

      /** min(t, fraction) -> min(fraction, t). */
      template<
        class I, I N, I D, class T,
        std::enable_if_t<IsFractionOperand<T>::value, int> = 0>
      constexpr auto min(T&& t, FractionConstant<I, N, D> f)
      {
        return min(f, t);
      }

      /** max(t, fraction) -> max(fraction, t). */
      template<
        class I, I N, I D, class T,
        std::enable_if_t<IsFractionOperand<T>::value, int> = 0>
      constexpr auto max(T&& t, FractionConstant<I, N, D> f)
      {
        return max(f, t);
      }

      ///@} CommutationPatterns

      //!@} ConstantOperations

      //!@} ExpressionTemplates

    } // NS TypedValue

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_EXPRESSIONS_INTCONSTOPERATIONS_HH__
