#ifndef __DUNE_ACFEM_EXPRESSIONS_FIELDPROMOTION_HH__
#define __DUNE_ACFEM_EXPRESSIONS_FIELDPROMOTION_HH__

/**@file
 *
 * Field promotion stuff
 */

#include <limits>
#include <complex>

#include "../common/types.hh"

#include "policy.hh"
#include "typetraits.hh"

namespace Dune
{

  namespace ACFem
  {

    /**@addtogroup ExpressionTemplates
     *
     * ACFem provides expression templates in order to form algebraic
     * expression for models, grid-functions and parameters.
     *
     * @{
     *
     */

    namespace {

      /**Floating point closure type.*/
      template<class F, class SFINAE = void>
      struct FloatingPointClosureHelper
      {
        using Type = Expressions::Policy::FloatingPointDefault;
      };

      /**Floating point closure type of non decay types is the closure
       * of the decay with the reference and cv qualifiers of the
       * original type.
       */
      template<class F>
      struct FloatingPointClosureHelper<F, std::enable_if_t<!IsDecay<F>::value> >
      {
        using Type = copy_cv_reference_t<F, typename FloatingPointClosureHelper<std::decay_t<F> >::Type>;
      };

      /**Default floating-point closure type is "double".
       */
      template<class F>
      struct FloatingPointClosureHelper<
        F,
        std::enable_if_t<IsDecay<F>::value
                         &&
                         std::numeric_limits<typename FieldTraits<F>::real_type>::is_iec559>
        >
      {
        using Type = typename FieldTraits<F>::real_type;
      };

      template<class T>
      struct FloatingPointClosureHelper<std::complex<T> >
      {
        using Type = std::complex<typename FloatingPointClosureHelper<T>::Type>;
      };

    } // NS anonymous

    /**Template alias.*/
    template<class T>
    using FloatingPointClosure = typename FloatingPointClosureHelper<T>::Type;

    /**Convert in particular integer types to a decent floating point
     * type.
     */
    template<class T, std::enable_if_t<!std::is_same<FloatingPointClosure<std::decay_t<T> >, std::decay_t<T> >::value, int> = 0>
    constexpr auto floatingPointClosure(T&& t)
    {
      return FloatingPointClosure<std::decay_t<T> >(t);
    }

    /**Forward floating point arguments as is.*/
    template<class T, std::enable_if_t<std::is_same<FloatingPointClosure<std::decay_t<T> >, std::decay_t<T> >::value, int> = 0>
    constexpr decltype(auto) floatingPointClosure(T&& t)
    {
      return std::forward<T>(t);
    }

    /**Promote potentially different numeric field types to a
     * "closure" type.
     */
    template<class T1, class T2, class SFINAE = void>
    struct FieldPromotion
    {
      using Type = std::decay_t<decltype(std::declval<typename FieldTraits<std::decay_t<T1> >::field_type>()
                                         *
                                         std::declval<typename FieldTraits<std::decay_t<T2> >::field_type>())>;
    };

    template<class T1, class T2>
    using FieldPromotionType = typename FieldPromotion<T1, T2>::Type;

    //!@} ExpressionTraits

  } // ACFem

} // Dune

#endif // __DUNE_ACFEM_EXPRESSIONS_FIELDPROMOTION_HH__
