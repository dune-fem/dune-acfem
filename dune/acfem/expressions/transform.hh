#ifndef __DUNE_ACFEM_EXPRESSIONS_TRANSFORM_HH__
#define __DUNE_ACFEM_EXPRESSIONS_TRANSFORM_HH__

#include "../common/typetraits.hh"
#include "storage.hh"
#include "terminal.hh"
#include "treeoperand.hh"
#include "examine.hh"

namespace Dune::ACFem::Expressions {

  /**Default operation functor for transform. Just let the given
   * operation operate on the given operands.
   *
   * @param Optimize Should be an OptimizeTag. Default is not to
   * optimize, given that a transformation on an existing expression
   * takes places after it has been optimized already.
   */
  template<class Optimize = OptimizeTop>
  struct Operate
  {
    template<class F, class... Arg>
    constexpr decltype(auto) operator()(F&& f, Arg&&... arg) const
    {
      return operate(Optimize{}, std::forward<F>(f), std::forward<Arg>(arg)...);
    }
  };

  /**Operation functor for transform(). Clone the arguments before
   * operating.
   */
  template<class Optimize = OptimizeTop>
  struct OperateClone
  {
    template<class F, class... Arg>
    constexpr decltype(auto) operator()(F&& f, Arg&&... arg) const
    {
      return operate(Optimize{}, std::forward<F>(f), std::decay_t<Arg>(arg)...);
    }
  };

  /**Operation functor which does not operate and collapes the entire
   * expression to a dummy return value of int 0. This is used to
   * implement traversal of the expression tree.
   */
  struct DontOperate
  {
    template<class F, class... Arg>
    constexpr decltype(auto) operator()(F&& f, Arg&&... arg) const
    {
      return 0;
    }
  };

  using NoTreePos = FalseType;
  using DoTreePos = TrueType;
  using NoParent = FalseType;

  template<class F, class E, class TreePos, class Parent>
  struct IsTransformInvocable
    : std::is_invocable<F, E, TreePos, Parent>
  {};

  template<class F, class E, class TreePos>
  struct IsTransformInvocable<F, E, TreePos, FalseType>
    : std::is_invocable<F, E, TreePos>
  {};

  template<class F, class E, class Parent>
  struct IsTransformInvocable<F, E, FalseType, Parent>
    : std::is_invocable<F, E, Parent>
  {};

  template<class F, class E>
  struct IsTransformInvocable<F, E, FalseType, FalseType>
    : std::is_invocable<F, E>
  {};

  namespace {

    template<class TreePos, std::size_t Idx>
    struct AppendTreePosHelper
    {
      using Type = PushBack<Idx, TreePos>;
    };

    template<std::size_t Idx>
    struct AppendTreePosHelper<FalseType, Idx>
    {
      using Type = FalseType;
    };

  }

  template<class TreePos, std::size_t Idx>
  using AppendTreePos = typename AppendTreePosHelper<TreePos, Idx>::Type;

  template<
    class E, class F,
    class Operation = Operate<OptimizeTop>,
    class TreePos = NoTreePos,
    class Parent = NoParent,
    std::enable_if_t<IsTransformInvocable<F, E, TreePos, Parent>::value, int> = 0>
  constexpr decltype(auto) transform(E&& e, F&& f, Operation&& op = Operation{}, TreePos = TreePos{}, Parent&& parent = Parent{})
  {
    //std::clog << "invoc TR rvref: " << std::is_rvalue_reference<decltype(e)>::value << std::endl;
    //std::clog << "  " << e.name() << std::endl;

    if constexpr (std::is_same<Parent, FalseType>::value) {
      if constexpr (std::is_same<TreePos, FalseType>::value) {
        return std::forward<F>(f)(std::forward<E>(e));
      } else {
        return std::forward<F>(f)(std::forward<E>(e), TreePos{});
      }
    } else {
      return std::forward<F>(f)(std::forward<E>(e), TreePos{}, std::forward<Parent>(parent));
    }
  }

  namespace {

    template<class E, class F, class Operation, class TreePos, class Parent>
    constexpr bool doNotTransform()
    {
      if constexpr (IsTransformInvocable<F, E, TreePos, Parent>::value) {
        return false;
      } else if constexpr (!IsExpression<E>::value) {
        return true;
      } else if constexpr (IsSelfExpression<E>::value) {
        return true;
      } else if constexpr (IsPlaceholderExpression<E>::value) {
        return true;
      } else if constexpr (std::is_same<F, FalseType>::value) {
        return false;
      } else if constexpr (ExamineTreeOr<E, TreePos, Parent, IsApplicableTo, F>::value) {
        return false;
      } else {
        return true;
      }
    }

#if 0
    template<class E, class F, class Operation, class TreePos, class Parent>
    struct DoNotTransform
#endif

  } // NS anonymous

  template<
    class E, class F,
    class Operation = Operate<OptimizeTop>,
    class TreePos = NoTreePos,
    class Parent = NoParent,
    std::enable_if_t<(/**/
#if 1
                      doNotTransform<E, F, Operation, TreePos, Parent>()
#else
                      !IsTransformInvocable<F, E, TreePos, Parent>::value
                      && ((!std::is_same<F, FalseType>::value
                              && !ExamineTreeOr<E, TreePos, Parent, IsApplicableTo, F>::value)
                          // just forward non-expressions (no expression interface)
                          || !IsExpression<E>::value
                          // just forward expression terminals (inifinte recursion)
                          || IsSelfExpression<E>::value
                          // just forward placeholders (not constructible here)
                          || IsPlaceholderExpression<E>::value
                        )
#endif
    ), int> = 0>
  constexpr decltype(auto) transform(E&& e, F&& f, Operation&& op = Operation{}, TreePos = TreePos{}, Parent&& = Parent{})
  {
    //std::clog << "noTR rvref: " << std::is_rvalue_reference<decltype(e)>::value << std::endl;

    return forwardReturnValue<E>(e);
  }

  namespace {

    template<class E, class F,
             class Operation, class TreePos, class Parent,
             std::size_t... ArgIndex>
    decltype(auto) transformHelper(E&& e, F&& f, Operation&& op, TreePos, Parent&&, IndexSequence<ArgIndex...>)
    {
      static_assert(IsExpression<E>::value && Arity<E>::value == sizeof...(ArgIndex),
                    "Internal error: helper function called with inconsistent arguments.");
      //std::clog << "helper rvref: " << std::is_rvalue_reference<decltype(e)>::value << std::endl;

      if constexpr (std::is_same<Parent, FalseType>::value) {
        return
          std::forward<Operation>(op)(
            std::forward<E>(e).operation(),
            transform(
              std::forward<E>(e).template operand<ArgIndex>(),
              std::forward<F>(f),
              std::forward<Operation>(op),
              AppendTreePos<TreePos, ArgIndex>{},
              FalseType{}
              )...
            );
      } else {
        return
          std::forward<Operation>(op)(
            std::forward<E>(e).operation(),
            transform(
              std::forward<E>(e).template operand<ArgIndex>(),
              std::forward<F>(f),
              std::forward<Operation>(op),
              AppendTreePos<TreePos, ArgIndex>{},
              std::forward<E>(e)
              )...
            );
      }
    }
  }

  namespace {

    template<class E, class F, class Operation, class TreePos, class Parent>
    constexpr bool doTransformRecurse()
    {
      if constexpr (IsTransformInvocable<F, E, TreePos, Parent>::value) {
        return false;
      } else if constexpr (!IsExpression<E>::value) {
        return false;
      } else if constexpr (IsSelfExpression<E>::value) {
        return false;
      } else if constexpr (IsPlaceholderExpression<E>::value) {
        return false;
      } else if constexpr (std::is_same<F, FalseType>::value) {
        return true;
      } else if constexpr (ExamineTreeOr<E, TreePos, Parent, IsApplicableTo, F>::value) {
        return true;
      } else {
        return false;
      }
    }

  } // NS anonymous

  template<
    class E, class F,
    class Operation = Operate<OptimizeTop>,
    class TreePos = NoTreePos,
    class Parent = NoParent,
    std::enable_if_t<(/**/
#if 1
                      doTransformRecurse<E, F, Operation, TreePos, Parent>()
#else
                      !IsTransformInvocable<F, E, TreePos, Parent>::value
                      && (std::is_same<F, FalseType>::value
                          || ExamineTreeOr<E, TreePos, Parent, IsApplicableTo, F>::value
                        )
                      // We must not try to descend into non-expresssions
                      && IsExpression<E>::value
                      // We must not descend into self expressions as
                      // they are their own operand
                      && !IsSelfExpression<E>::value
                      // We must not descend into placeholders because
                      // we do not know how to reconstruct them.
                      && !IsPlaceholderExpression<E>::value
#endif
      ), int> = 0>
  decltype(auto) transform(E&& e, F&& f, Operation&& op = Operation{}, TreePos = TreePos{}, Parent&& parent = Parent{})
  {
    //std::clog << "rec TR rvref: " << std::is_rvalue_reference<decltype(e)>::value << std::endl;

    return transformHelper(
      std::forward<E>(e),
      std::forward<F>(f),
      std::forward<Operation>(op),
      TreePos{},
      std::forward<Parent>(parent),
      MakeIndexSequence<Arity<E>::value>{}
      );
  }

  /**Just traverse the expression tree applying the given functor to
   * all matching sub-expressions. However, the expression-operations
   * are not applied, this is a mere tree traversal.
   */
  template<class E, class F, class TreePos = NoTreePos, class Parent = NoParent, std::enable_if_t<IsExpression<E>::value, int> = 0>
  auto traverse(E&& e, F&& f, TreePos = TreePos{}, Parent&& parent = Parent{})
  {
    return transform(std::forward<E>(e), std::forward<F>(f), DontOperate{}, TreePos{}, std::forward<Parent>(parent));
  }

  /**Replace all references to objects by an instance of their decay
   * type.
   */
  template<class E, class Optimize = OptimizeTop, class TreePos = IndexSequence<> >
  auto deepCopy(E&& e, Optimize = Optimize{}, TreePos = TreePos{})
  {
    return transform(std::forward<E>(e), FalseType{}, OperateClone<Optimize>{}, TreePos{});
  }

  /**@internal Default ignore functor. Operands matching this
   * predicate will just be forwarded unaltered by the
   * ReplaceFunctor.
   */
  template<class T, class...>
  struct ReplaceDefaultIgnore
    : BoolConstant<(IsPlaceholderExpression<T>::value
                    || IsIndeterminateExpression<T>::value
                     )>
  {};

  /**Replace all expression operands of type T where @a When<T, From>
   * is std::true_type by an instance of @a To. If @a When<T, From> is
   * not std::true_type but @a Ignore<T, From> is std::true_type then
   * copy the expression operand unaltered.
   *
   * transform() will recurse into the expression tree below @a T if
   * neither When or Ignore give a positive match.
   *
   * The fancy additional variadic template parameters are there to
   * allow additional default template arguments in the template
   * template parameters.
   */
  template<class From, class To,
           template<class...> class When = std::is_same,
           template<class...> class Ignore = ReplaceDefaultIgnore>
  struct ReplaceFunctor
  {
    using FromType = From;

    ReplaceFunctor(To&& to = To{})
      : to_(std::forward<To>(to))
    {}

    template<class T,
             std::enable_if_t<(!When<T, FromType>::value
                               && Ignore<T, FromType>::value
               ), int> = 0>
    constexpr decltype(auto) operator()(T&& t) const
    {
      return forwardReturnValue<T>(t);
    }

    template<class T, std::size_t... TreePos,
             std::enable_if_t<(!When<MPL::TypeTuple<T, IndexSequence<TreePos...> >, FromType>::value
                               && (Ignore<MPL::TypeTuple<T, IndexSequence<TreePos...> >, FromType>::value
                                   || Ignore<T, FromType>::value
                                 )
               ), int> = 0>
    constexpr decltype(auto) operator()(T&& t, IndexSequence<TreePos...>) const
    {
      return forwardReturnValue<T>(t);
    }

    template<class T, class Parent,
             std::enable_if_t<(!IsSequence<Parent>::value
                               && !When<MPL::TypeTuple<T, Parent>, FromType>::value
                               && (Ignore<MPL::TypeTuple<T, Parent>, FromType>::value
                                   || Ignore<T, FromType>::value
                                 )
      ), int> = 0>
    constexpr decltype(auto) operator()(T&& t, Parent&& parent) const
    {
      return forwardReturnValue<T>(t);
    }

    template<class T, class TreePos, class Parent,
             std::enable_if_t<(!When<MPL::TypeTuple<T, TreePos, Parent>, FromType>::value
                               && (Ignore<MPL::TypeTuple<T, TreePos, Parent>, FromType>::value
                                   || Ignore<MPL::TypeTuple<T, TreePos>, FromType>::value
                                   || Ignore<T, FromType>::value
                                 )
      ), int> = 0>
    constexpr decltype(auto) operator()(T&& t, TreePos, Parent&& parent) const
    {
      return forwardReturnValue<T>(t);
    }

    template<class T, std::enable_if_t<When<T, FromType>::value, int> = 0>
    constexpr decltype(auto) operator()(T&& t) const
    {
      return to_;
    }

    template<class T, std::size_t... TreePos, std::enable_if_t<When<MPL::TypeTuple<T, IndexSequence<TreePos...> >, FromType>::value, int> = 0>
    constexpr decltype(auto) operator()(T&& t, IndexSequence<TreePos...>) const
    {
      return to_;
    }

    template<class T, class Parent, std::enable_if_t<!IsIndexSequence<Parent>::value && When<MPL::TypeTuple<T, Parent>, FromType>::value, int> = 0>
    constexpr decltype(auto) operator()(T&&, Parent&&) const
    {
      return to_;
    }

    template<class T, class TreePos, class Parent, std::enable_if_t<When<MPL::TypeTuple<T, TreePos, Parent>, FromType>::value, int> = 0>
    constexpr decltype(auto) operator()(T&&, TreePos, Parent&&) const
    {
      return to_;
    }

    To to_;
  };

  /**Variant o ReplaceFunctor where When may be a simple unary
   * IsSomething trait.
   */
  template<class To,
           template<class...> class When,
           template<class...> class Ignore = ReplaceDefaultIgnore>
  struct SimpleReplaceFunctor
    : ReplaceFunctor<void, To, PredicateProxy<When>::template ForwardFirst, Ignore>
  {
    using ReplaceFunctor<void, To, PredicateProxy<When>::template ForwardFirst, Ignore>::ReplaceFunctor;
  };

  /**@internal The default predicate for TraverseFunctor. In addition
   * to real terminals stop also on placeholders and indeterminates as
   * determined by ReplaceDefaultIgnore.
   */
  template<class E, class Dummy, class SFINAE = void>
  struct DefaultTerminalPredicate
    : BoolConstant<(!IsExpression<E>::value
                    || IsSelfExpression<E>::value
                    || ReplaceDefaultIgnore<E>::value)>
  {};

  template<class E, class Dummy>
  struct DefaultTerminalPredicate<E, Dummy, std::enable_if_t<MPL::IsTypeTupleV<E> && size<E>() != 0> >
    : DefaultTerminalPredicate<MPL::TypeTupleElement<0, E>, Dummy>
  {};

  /**@internal Just traverse the expression tree; stop at the
   * terminals selected by the TerminalPredicate.
   */
  template<template<class...> class TerminalPredicate = DefaultTerminalPredicate>
  struct TraverseFunctor
    : ReplaceFunctor<void, int, AlwaysFalse, TerminalPredicate>
  {};

  /**Replace matching expressions by their operand no. N. Primarily
   * useful to replace wrapper expressions by their wrapped values.
   */
  template<std::size_t N,
           template<class...> class When,
           template<class...> class Ignore = ReplaceDefaultIgnore>
  struct ReplaceByOperand
  {
    template<class T,
             std::enable_if_t<(!When<T>::value
                               && Ignore<T, void>::value
               ), int> = 0>
    constexpr decltype(auto) operator()(T&& t) const
    {
      return std::forward<T>(t);
    }

    template<class T, class TreePos,
             std::enable_if_t<(!When<MPL::TypeTuple<T, TreePos> >::value
                               && (Ignore<MPL::TypeTuple<T, TreePos>, void>::value || Ignore<T, void>::value)
               ), int> = 0>
    constexpr decltype(auto) operator()(T&& t, TreePos) const
    {
      return std::forward<T>(t);
    }

    template<class T, std::enable_if_t<When<T>::value, int> = 0>
    constexpr decltype(auto) operator()(T&& t) const
    {
      return std::forward<T>(t).template operand<N>();
    }

    template<class T, class TreePos, std::enable_if_t<When<MPL::TypeTuple<T, TreePos> >::value, int> = 0>
    constexpr decltype(auto) operator()(T&& t, TreePos) const
    {
      return std::forward<T>(t).template operand<N>();
    }
  };

  template<
    class From, class To, class E, class Optimize = OptimizeTop,
    class TreePos = NoTreePos, class Parent = NoParent>
  auto replace(E&& e, To&& to, From&& = From(), Optimize = Optimize{}, TreePos = TreePos{}, Parent&& parent = Parent{})
  {
    return transform(
      std::forward<E>(e),
      ReplaceFunctor<AsExpression<From>, AsExpression<To> >(
        asExpression(std::forward<To>(to))
        ),
      Operate<Optimize>{},
      TreePos{},
      std::forward<Parent>(parent));
  }

  /**Replace all indeterminate expressions with the given id by @a to.*/
  template<std::size_t Id, class To, class E, class Optimize = OptimizeTop,
           class TreePos = NoTreePos, class Parent = NoParent>
  decltype(auto) replaceIndeterminate(E&& e, To&& to, IndexConstant<Id> = IndexConstant<Id>{}, Optimize = Optimize{}, TreePos = TreePos{}, Parent&& parent = Parent{})
  {
    //std::clog << "replIndet rvref: " << std::is_rvalue_reference<decltype(e)>::value << std::endl;

    return transform(
      std::forward<E>(e),
      ReplaceFunctor<IndexConstant<Id>, AsExpression<To>, IndeterminateMatch>(
        asExpression(std::forward<To>(to))
        ),
      Operate<Optimize>{},
      TreePos{},
      std::forward<Parent>(parent)
      );
  }

  /**Replace all expression where Predicate<?,To> converts to @c true,
   * while not descending into sub-expressions where Ignore<?>
   * converts to true.
   */
  template<template<class...> class Predicate, template<class...> class Ignore,
           class To, class E,
           class Optimize = OptimizeTop, class TreePos = NoTreePos, class Parent = NoParent>
  decltype(auto) replaceMatching(E&& e, To&& to, Optimize = Optimize{}, TreePos = TreePos{}, Parent&& parent = Parent{})
  {
    using Replace = ReplaceFunctor<
      To, To, Predicate, PredicateProxy<Ignore>::template ForwardFirst>;

    return transform(std::forward<E>(e), Replace(std::forward<To>(to)),
                     Operate<Optimize>{}, TreePos{}, std::forward<Parent>(parent));
  }

  /**Replace all expression where Predicate<?,To> cerverts to @c true.
   */
  template<template<class...> class Predicate,
           class To, class E,
           class Optimize = OptimizeTop, class TreePos = NoTreePos, class Parent = NoParent>
  decltype(auto) replaceMatching(E&& e, To&& to, Optimize = Optimize{}, TreePos = TreePos{}, Parent&& parent = Parent{})
  {
    return replaceMatching<Predicate, AlwaysFalse>(std::forward<E>(e), std::forward<To>(to),
                                                   Optimize{}, TreePos{}, std::forward<Parent>(parent));
  }

}

#endif // __DUNE_ACFEM_EXPRESSIONS_TRANSFORM_HH__
