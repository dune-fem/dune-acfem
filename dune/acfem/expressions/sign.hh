#ifndef __DUNE_ACFEM_EXPRESSIONS_SIGN_HH__
#define __DUNE_ACFEM_EXPRESSIONS_SIGN_HH__

#include <ostream>

namespace Dune {

  namespace ACFem {

    namespace Expressions {

      /**A class mainting the sign of an expression during operations.
       *
       * @param[in] nonSingular For scalar means non-zero, for operators
       * invertible. The boundary-value models use this to indicate that
       * the resulting system will be solvable (if applicable).
       *
       * @param[in] semiPositive For scalars means >= 0, for operators
       * positive semi-definite.
       *
       * @param[in] semiNegative For scalars means <= 0, for operators
       * negative semi-definite.
       *
       * @param[in] offset Optional integral value defines the "zero
       * point". Defaults to 0.
       */
      template<bool nonSingular, bool semiPositive, bool semiNegative,
               std::ptrdiff_t offset = 0>
      struct ExpressionSign
      {
        static constexpr bool isNonSingular = nonSingular;
        static constexpr bool isSemiPositive = semiPositive;
        static constexpr bool isSemiNegative = semiNegative;
        static constexpr bool isZero = !isNonSingular && isSemiPositive && isSemiNegative;
        static constexpr bool isNonZero = nonSingular;
        static constexpr bool isPositive = isNonZero && isSemiPositive;
        static constexpr bool isNegative = isNonZero && isSemiNegative;
        static constexpr std::ptrdiff_t reference = offset;
        using Sign = ExpressionSign;
      };

      /**Output operatgor to ease debugging.*/
      template<bool nonSingular, bool semiPositive, bool semiNegative, ssize_t offset>
      std::ostream& operator<<(std::ostream& out, const ExpressionSign<nonSingular, semiPositive, semiNegative, offset>&)
      {
        using Sign = ExpressionSign<nonSingular, semiPositive, semiNegative, offset>;
        out << "{";
        Sign::isSemiPositive && out << "(>=" << Sign::reference << ")";
        Sign::isSemiNegative && out << "(<=" << Sign::reference << ")";
        Sign::isZero && out << "(==" << Sign::reference << ")";
        Sign::isNonZero && out << "(!=" << Sign::reference << ")";
        Sign::isNonSingular && out << "(!/" << Sign::reference << ")";
        Sign::isPositive && out << "(>" << Sign::reference << ")";
        Sign::isNegative && out << "(<" << Sign::reference << ")";
        out << "}";
        return out;
      };

    } // Expressions::

    using Expressions::ExpressionSign;

    /**Sign traits structure with names matching the expression tag structs.*/
    using UnknownExpressionSign = ExpressionSign<false, false, false>;
    using ZeroExpressionSign = ExpressionSign<false, true, true>;
    using NonZeroExpressionSign = ExpressionSign<true, false, false>;
    using PositiveExpressionSign = ExpressionSign<true, true, false>;
    using NegativeExpressionSign = ExpressionSign<true, false, true>;
    using SemiPositiveExpressionSign = ExpressionSign<false, true, false>;
    using SemiNegativeExpressionSign = ExpressionSign<false, false, true>;
    using RegularZeroExpressionSign = ExpressionSign<true, true, true>;

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_EXPRESSIONS_SIGN_HH__
