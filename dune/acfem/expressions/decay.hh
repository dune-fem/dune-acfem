#ifndef __DUNE_ACFEM_EXPRESSIONS_DECAY_HH__
#define __DUNE_ACFEM_EXPRESSIONS_DECAY_HH__

#include "transform.hh"
#include "subexpression.hh"

namespace Dune::ACFem::Expressions {

  /**Operation functor for transform(). Clone the arguments before
   * operating.
   */
  template<class Optimize = OptimizeTop>
  struct OperateDecay
  {
    template<class F, class... Arg>
    constexpr auto operator()(F&& f, Arg&&... arg) const
    {
      return subExpressionExpression(operate(Optimize{}, std::decay_t<F>(f), subExpressionExpression(std::decay_t<Arg>(arg))...));
    }
  };

  /**Replace all references to objects by an instance of their decay
   * type after replacing all sub-expressions by their wrapped
   * expression.
   */
  template<class E, class Optimize = OptimizeTop, class TreePos = IndexSequence<> >
  auto decayCopy(E&& e, Optimize = Optimize{}, TreePos = TreePos{})
  {
    return transform(std::forward<E>(e), FalseType{}, OperateDecay<Optimize>{}, TreePos{});
  }

  /**The type of the expression where all references are replaced by
   * the respective decay type and all sub-expressions by their
   * underlying wrapped expression.
   *
   * This can be used in type traits when
   * comparing expression types where it does not matter whether the
   * contained operands are expressions or not.
   */
  template<class T, class TreePos = IndexSequence<> >
  using DecayExpression = std::decay_t<decltype(decayCopy(std::declval<T>(), OptimizeTop{}, TreePos{}))>;

}

#endif // __DUNE_ACFEM_EXPRESSIONS_DECAY_HH__
