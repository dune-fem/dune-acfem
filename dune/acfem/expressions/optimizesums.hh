#ifndef __DUNE_ACFEM_EXPRESSIONS_OPTIMIZESUMS_HH__
#define __DUNE_ACFEM_EXPRESSIONS_OPTIMIZESUMS_HH__

#include "complexity.hh"
#include "optimizationbase.hh"
#include "optimizegeneral.hh"
#include "runtimeequal.hh"
#include "treeextract.hh"
#include "operationpair.hh"
#include "../common/literals.hh"

namespace Dune
{

  namespace ACFem::Expressions
  {

    namespace Sums
    {

      /**@addtogroup ExpressionTemplates
       *
       * @{
       *
       */

      /**@addtogroup ExpressionOptimization
       *
       * General optimization patterns.
       *
       * @{
       */

      /**@addtogroup SumOptimization
       *
       * @{
       */

      /**@addtogroup ExpensiveOptimizations
       *
       * @{
       */

      using namespace Expressions;

      using SubOptimizationTag = OptimizeOrdinary;

      template<class A, class B, class SFINAE = void>
      struct Swap
      {
        static constexpr bool value = ExpressionWeightV<A> > ExpressionWeightV<B>;
      };

      template<class A, class B>
      struct Swap<
        A, B,
        std::enable_if_t<(std::is_same<typename A::Sign, MinusFunctor>::value
                          && std::is_same<typename B::Sign, PlusFunctor>::value
        )> >
      {
        static constexpr bool value = true;
      };

      template<class A, class B>
      struct Swap<
        A, B,
        std::enable_if_t<(std::is_same<typename A::Sign, PlusFunctor>::value
                          && std::is_same<typename B::Sign, MinusFunctor>::value
        )> >
      {
        static constexpr bool value = false;
      };

      template<class OuterOperation>
      struct PackSumOperand;

      template<>
      struct PackSumOperand<PlusFunctor>
      {
#if 0
        template<class T, class TreePos, class Parent, std::size_t TailPos = Tail<TreePos>::value>;
        struct PackStruct
        {
          using Type = TreeData<PlusFunctor, T, TreePos>;
        };

        template<class T, class TreePos, class Parent>;
        struct PackStruct<T, TreePos, Parent, 1>
        {
          using Type = TreeData<Functor<Parent>, T, TreePos>;
        };

        template<class T, class TreePos, class Parent>
        using PackIt = typename PackStruct<T, TreePos, Parent>::Type;
#else
        template<class T, class TreePos, class Parent>
        using PackIt = TreeData<
          ConditionalType<Tail<TreePos>::value == 0, PlusFunctor, Functor<Parent> >,
          T, TreePos
          >;
#endif
      };

      template<>
      struct PackSumOperand<MinusFunctor>
      {
#if 0
        template<class T, class TreePos, class Parent, std::size_t TailPos = Tail<TreePos>::value>;
        struct PackStruct
        {
          using Type = TreeData<MinusFunctor, T, TreePos>;
        };

        template<class T, class TreePos, class Parent>;
        struct PackStruct<T, TreePos, Parent, 1>
        {
          using Type = TreeData<NestedSumFunctor<MinusFunctor, Functor<Parent> >, T, TreePos>;
        };

        template<class T, class TreePos, class Parent>
        using PackIt = typename PackStruct<T, TreePos, Parent>::Type;
#else
        template<class T, class TreePos, class Parent>
        using PackIt = TreeData<
          ConditionalType<Tail<TreePos>::value == 0, MinusFunctor, NestedSumFunctor<MinusFunctor, Functor<Parent> > >,
          T, TreePos
          >;
#endif
      };

      template<class T, class Parent>
      using IsSumOperand = BoolConstant<(IsPlusOrMinusExpression<Parent>::value
                                         && !IsPlusOrMinusExpression<T>::value)>;

      template<class T>
      using StopRecursion = BoolConstant<!IsPlusOrMinusExpression<T>::value>;

      template<class T, class F = PlusFunctor>
      using LeftSumOperands = TreeExtract<IsSumOperand, T, PackSumOperand<F>::template PackIt, TreePosition<0>, StopRecursion>;

      template<class T, class F = PlusFunctor>
      using RightSumOperands = TreeExtract<IsSumOperand, T, PackSumOperand<F>::template PackIt, TreePosition<1>, StopRecursion>;

      //#################

      template<class T0, class T1,
               class Sign0, class E0, class Pos0,
               class Sign1, class F1, class Node10, class Node11, class Tag1,
               class Sign, class F, class OuterTag>
      constexpr decltype(auto) getOperand(
        T0&& t0, T1&& t1,
        OperationPair<Sign, F, TreeData<Sign0, E0, Pos0>, OperationPair<Sign1, F1, Node10, Node11, Tag1>, OuterTag>
        );

      template<class T0, class T1,
               class Sign0, class F0, class Node00, class Node01, class Tag0,
               class Sign1, class E1, class Pos1,
               class Sign, class F, class OuterTag>
      constexpr decltype(auto) getOperand(
        T0&& t0, T1&& t1,
        OperationPair<Sign, F, OperationPair<Sign0, F0, Node00, Node01, Tag0>, TreeData<Sign1, E1, Pos1>, OuterTag>
        );

      //!@internal Recurse into factorization tuple, then operate.
      template<class T0, class T1,
               class Sign, class Functor,
               class... LeftNode, class RightNode,
               class Tag>
      constexpr decltype(auto) getOperand(
        T0&& t0, T1&& t1,
        OperationPair<Sign, Functor, MPL::TypeTuple<LeftNode...>, RightNode, Tag>
        );

      template<class T0, class T1,
               class Sign, class Functor,
               class LeftNode, class... RightNode,
               class Tag>
      constexpr decltype(auto) getOperand(
        T0&& t0, T1&& t1,
        OperationPair<Sign, Functor, LeftNode, MPL::TypeTuple<RightNode...>, Tag>
        );

      //#################

      template<class T0, class T1, class Sign, class Operand>
      constexpr decltype(auto) getOperand(T0&&, T1&&, TreeData<Sign, Operand, TreePosition<~0UL> >)
      {
        return Operand{};
      }

      //!@internal Read from T0
      template<class T0, class T1, class Arg, std::size_t I0, std::size_t... I, class F>
      constexpr decltype(auto) getOperand(T0&& t0, T1&& t1, TreeData<F, Arg, TreePosition<I0, I...> >)
      {
        if constexpr (I0 == 0) {
          return treeOperand(std::forward<T0>(t0), TreePosition<I...>{});
        } else {
          return treeOperand(std::forward<T1>(t1), TreePosition<I...>{});
        }
      }

      //!@internal Ordinary optimization pair
      template<class T0, class T1,
               class F0, class E0, class Pos0,
               class F1, class E1, class Pos1,
               class Sign, class F, class OptimizeTag>
      constexpr decltype(auto) getOperand(
        T0&& t0, T1&& t1,
        OperationPair<Sign, F, TreeData<F0, E0, Pos0>, TreeData<F1, E1, Pos1>, OptimizeTag>
        )
      {
        //static_assert(!std::is_same<F0, MinusFunctor>::value && !std::is_same<F1, MinusFunctor>::value, "");

        return operate(
          OptimizeTag{},
          F{},
          getOperand(std::forward<T0>(t0), std::forward<T1>(t1), TreeData<F0, E0, Pos0>{}),
          getOperand(std::forward<T0>(t0), std::forward<T1>(t1), TreeData<F1, E1, Pos1>{})
          );
      }

      //!@internal Left-right-nested optimization pair.
      template<class T0, class T1,
               class Sign0, class F0, class Node00, class Node01, class Tag0,
               class Sign1, class F1, class Node10, class Node11, class Tag1,
               class Sign, class F, class OuterTag>
      constexpr decltype(auto) getOperand(
        T0&& t0, T1&& t1,
        OperationPair<
          Sign, F,
          OperationPair<Sign0, F0, Node00, Node01, Tag0>,
          OperationPair<Sign1, F1, Node10, Node11, Tag1>,
          OuterTag>
        )
      {
        using OptPair0 = OperationPair<Sign0, F0, Node00, Node01, Tag0>;
        using OptPair1 = OperationPair<Sign1, F1, Node10, Node11, Tag1>;
        return operate(
          OuterTag{},
          F{},
          getOperand(std::forward<T0>(t0), std::forward<T1>(t1), OptPair0{}),
          getOperand(std::forward<T0>(t0), std::forward<T1>(t1), OptPair1{})
          );
      }

      //!@internal Left-nested optimization pair.
      template<class T0, class T1,
               class Sign0, class F0, class Node00, class Node01, class Tag0,
               class Sign1, class E1, class Pos1,
               class Sign, class F, class OuterTag>
      constexpr decltype(auto) getOperand(
        T0&& t0, T1&& t1,
        OperationPair<Sign, F, OperationPair<Sign0, F0, Node00, Node01, Tag0>, TreeData<Sign1, E1, Pos1>, OuterTag>
        )
      {
        using TreeNode0 = OperationPair<Sign0, F0, Node00, Node01, Tag0>;
        using TreeNode1 = TreeData<Sign1, E1, Pos1>;

        return operate(
          OuterTag{},
          F{},
          getOperand(std::forward<T0>(t0), std::forward<T1>(t1), TreeNode0{}),
          getOperand(std::forward<T0>(t0), std::forward<T1>(t1), TreeNode1{})
          );
      }

      //!@internal Right-nested optimization pair.
      template<class T0, class T1,
               class Sign0, class E0, class Pos0,
               class Sign1, class F1, class Node10, class Node11, class Tag1,
               class Sign, class F, class OuterTag>
      constexpr decltype(auto) getOperand(
        T0&& t0, T1&& t1,
        OperationPair<Sign, F, TreeData<Sign0, E0, Pos0>, OperationPair<Sign1, F1, Node10, Node11, Tag1>, OuterTag>
        )
      {
        using TreeNode0 = TreeData<Sign0, E0, Pos0>;
        using TreeNode1 = OperationPair<Sign1, F1, Node10, Node11, Tag1>;

        return operate(
          OuterTag{},
          F{},
          getOperand(std::forward<T0>(t0), std::forward<T1>(t1), TreeNode0{}),
          getOperand(std::forward<T0>(t0), std::forward<T1>(t1), TreeNode1{})
          );
      }

      struct JustForward
      {
        template<class T>
        constexpr decltype(auto) operator()(T&& t)
        {
          return std::forward<T>(t);
        }
      };

      //!@internal Recurse into factorization tuple, then operate.
      template<class T0, class T1,
               class Sign, class Functor,
               class... LeftNode, class RightNode,
               class Tag>
      constexpr decltype(auto) getOperand(
        T0&& t0, T1&& t1,
        OperationPair<Sign, Functor, MPL::TypeTuple<LeftNode...>, RightNode, Tag>
        )
      {
        //std::clog << "SUMUP: " << __LINE__ << " " << __PRETTY_FUNCTION__ << std::endl;
        return operate(
          Tag{}, //DontOptimize{},
          Functor{},
          sumUp(std::forward<T0>(t0), std::forward<T1>(t1), MPL::TypeTuple<LeftNode...>{}, Tag{}, JustForward{}),
          getOperand(std::forward<T0>(t0), std::forward<T1>(t1), RightNode{})
          );
      }

      //!@internal Recurse into factorization tuple, then operate.
      template<class T0, class T1,
               class Sign, class Functor,
               class LeftNode, class... RightNode,
               class Tag>
      constexpr decltype(auto) getOperand(
        T0&& t0, T1&& t1,
        OperationPair<Sign, Functor, LeftNode, MPL::TypeTuple<RightNode...>, Tag>
        )
      {
        //std::clog << "SUMUP: " << __LINE__ << " " << __PRETTY_FUNCTION__ << std::endl;
        return operate(
          Tag{}, //DontOptimize{},
          Functor{},
          getOperand(std::forward<T0>(t0), std::forward<T1>(t1), LeftNode{}),
          sumUp(std::forward<T0>(t0), std::forward<T1>(t1), MPL::TypeTuple<RightNode...>{}, Tag{}, JustForward{})
          );
      }

      /**@internal Recursion end-point, add the last operand with
       * proper sign and return.
       */
      template<class T0, class T1, class LeftSum, class Last, class F, class Tag>
      constexpr auto sumUpRecursion(T0&& t0, T1&& t1, LeftSum&& leftSum, MPL::TypeTuple<Last>, F&& f, Tag tag)
      {
        //std::clog << "SUMUP: " << __LINE__ << " " << __PRETTY_FUNCTION__ << std::endl;
        return operate( // add the next operand with proper sign
          tag, // optionally recurse into one of the expensive sum-optimization passes in the last step.
          typename Last::Sign{},
          std::forward<LeftSum>(leftSum),
          f(getOperand(std::forward<T0>(t0), std::forward<T1>(t1), Last{}))
          );
      }

      /**@internal Sum-up recursion, add the next element with
       * proper sign and recurse.
       */
      template<class T0, class T1, class LeftSum, class Tail0, class Tail1, class... Tail, class F, class Tag>
      constexpr decltype(auto) sumUpRecursion(T0&& t0, T1&& t1, LeftSum&& leftSum, MPL::TypeTuple<Tail0, Tail1, Tail...>, F&& f, Tag tag)
      {
        //std::clog << "SUMUP: " << __LINE__ << " " << __PRETTY_FUNCTION__ << std::endl;
        return sumUpRecursion(
          std::forward<T0>(t0),
          std::forward<T1>(t1),
          operate( // add the next operand with proper sign
            DontOptimize{},
            typename Tail0::Sign{},
            std::forward<LeftSum>(leftSum),
            f(getOperand(std::forward<T0>(t0), std::forward<T1>(t1), Tail0{}))
            ),
          MPL::TypeTuple<Tail1, Tail...>{},
          std::forward<F>(f),
          tag
          );
      }

      /**@internal Take the provide MPL::TypeTuple<> of expression
       * trees (composed of TreeData and OperationPair) and assemble
       * the actual expression, taking the referred-to operands from
       * the given "real" expressions @a T0 and @a T1.
       *
       * @param T0 The left operand of the original binary expression.
       *
       * @param T1 The right operand of the original binary expression.
       *
       * @param Arg0, Arg1, ArgRest... Sum-operands as "tree-nodes".
       *
       * @param F Optional functor to be applied to the individual
       * sum-operands before summing them up.
       *
       * @param Tag Optimization-tag.
       *
       * @note The recursion results in a sum of the form:
       *
       * @f[
       *   (\dots(\pm f(a_0) \pm f(a_1)) \pm \dots \pm f(a_n))
       * @f]
       *
       */
      template<class T0, class T1, class Arg0, class Arg1, class... ArgRest, class F = JustForward, class Tag = DontOptimize>
      constexpr auto sumUp(T0&& t0, T1&& t1, MPL::TypeTuple<Arg0, Arg1, ArgRest...>, Tag tag = Tag{}, F&& f = F{})
      {
        //std::clog << "SUMUP: " << __LINE__ << " " << __PRETTY_FUNCTION__ << std::endl;
        return sumUpRecursion(
          std::forward<T0>(t0),
          std::forward<T1>(t1),
          operate(
            // left-most operand, can be that this generates an unary minus.
            // TODO: this should better not happen.
            DontOptimize{},
            typename Arg0::Sign{},
            f(getOperand(std::forward<T0>(t0), std::forward<T1>(t1), Arg0{}))
            ),
          MPL::TypeTuple<Arg1, ArgRest...>{},
          std::forward<F>(f),
          tag
          );
      }

      /**@internal Special case of a "sum" with only one operand. This
       * can happen.
       */
      template<class T0, class T1, class Last, class F = JustForward, class Tag = OptimizeGeneric>
      constexpr auto sumUp(T0&& t0, T1&& t1, MPL::TypeTuple<Last>, Tag = Tag{}, F&& f = F{})
      {
        //std::clog << "SUMUP: " << __LINE__ << " " << __PRETTY_FUNCTION__ << std::endl;
        return operate( // add the next operand with proper sign
          DontOptimize{},
          typename Last::Sign{},
          f(getOperand(std::forward<T0>(t0), std::forward<T1>(t1), Last{}))
          );
      }

      ///////////////////////////////////////////

      //!@internal Recursion end-point, OptPair is the best optimization found
      template<class TreeData, class OptPair, ptrdiff_t OldDelta, class... Head, class Undo>
      constexpr decltype(auto) tryAddSumOperand(MPL::TypeTuple<>, MPL::TypeTuple<Head...>, Undo)
      {
        return MPL::TypeTupleMergeOneLeft<OptPair, MPL::TypeTuple<Head...>, Swap>{};
      }

      template<class TreeData, class... Head>
      constexpr decltype(auto) tryAddSumOperand(MPL::TypeTuple<>, MPL::TypeTuple<Head...>)
      {
        return MPL::TypeTuple<>{};
      }

      template<class T>
      constexpr inline ptrdiff_t ComplexityDeltaV = 0;

      template<class Sign, class F, class T0, class T1, class Tag>
      constexpr inline ptrdiff_t ComplexityDeltaV<OperationPair<Sign, F, T0, T1, Tag> > =
        (1 + ComplexityV<T0> + ComplexityV<T1>) - ComplexityV<OperationPair<Sign, F, T0, T1, Tag> >;

      /**@internal Recursion work-horse after finding one
       * optimization, continue searching for a better one.
       *
       *  (+TreeData) + (+Tail0) -> TreeData + Tail0
       *  (+TreeData) + (-Tail0) -> TreeData - Tail0
       *  (-TreeData) + (+Tail0) -> +(Tail0 - TreeData)
       *  (-TreeData) + (-Tail0) -> -(Tail0 + TreeData)
       */
      template<class TreeData, class OptPair, ptrdiff_t OldDelta, class Tail0, class... Tail, class... Head, class... UndoHead>
      constexpr decltype(auto) tryAddSumOperand(
        MPL::TypeTuple<Tail0, Tail...>,
        MPL::TypeTuple<Head...>,
        MPL::TypeTuple<UndoHead...>)
      {
        using F = typename TreeData::Sign;
        using TailF = typename Tail0::Sign;
        if constexpr (std::is_same<F, MinusFunctor>::value
                      &&
                      std::is_same<TailF, PlusFunctor>::value) {
          // swap operands
          using Try = OperationPair</*Sign: */TailF, MinusFunctor, Tail0, TreeData, SubOptimizationTag>;
          constexpr ptrdiff_t complexityDelta = ComplexityDeltaV<Try>;
          if constexpr (complexityDelta > OldDelta) {
            return tryAddSumOperand<TreeData, Try, complexityDelta>(
              MPL::TypeTuple<Tail...>{},
              MPL::TypeTuple<UndoHead...>{},
              MPL::TypeTuple<UndoHead..., Tail0>{}
              );
          } else {
            // no optimization found, continue searching
            return tryAddSumOperand<TreeData, OptPair, OldDelta>(
              MPL::TypeTuple<Tail...>{},
              MPL::TypeTuple<Head..., Tail0>{},
              MPL::TypeTuple<UndoHead..., Tail0>{}
              );
          }
        } else {
          using Try = OperationPair</*Sign: */F, NestedSumFunctor<F, TailF>, TreeData, Tail0, SubOptimizationTag>;
          constexpr ptrdiff_t complexityDelta = ComplexityDeltaV<Try>;
          if constexpr (complexityDelta > OldDelta) {
            return tryAddSumOperand<TreeData, Try, complexityDelta>(
              MPL::TypeTuple<Tail...>{},
              MPL::TypeTuple<UndoHead...>{},
              MPL::TypeTuple<UndoHead..., Tail0>{}
              );
          } else {
            return tryAddSumOperand<TreeData, OptPair, OldDelta>(
              MPL::TypeTuple<Tail...>{},
              MPL::TypeTuple<Head..., Tail0>{},
              MPL::TypeTuple<UndoHead..., Tail0>{}
              );
          }
        }
      }

      /**@internal Recursion work-horse as long as no optimizable
       * operation has been found yet.
       */
      template<class TreeData, class Tail0, class... Tail, class... Head>
      constexpr decltype(auto) tryAddSumOperand(MPL::TypeTuple<Tail0, Tail...>, MPL::TypeTuple<Head...>)
      {
        using F = typename TreeData::Sign;
        using TailF = typename Tail0::Sign;
        if constexpr (std::is_same<F, MinusFunctor>::value
                      &&
                      std::is_same<TailF, PlusFunctor>::value) {
          // swap arguments
          using Try = OperationPair<TailF /*outer*/, MinusFunctor, Tail0, TreeData, SubOptimizationTag>;
          constexpr ptrdiff_t complexityDelta = ComplexityDeltaV<Try>;
          if constexpr (complexityDelta > 0) {
            return tryAddSumOperand<TreeData, Try, complexityDelta>(
              MPL::TypeTuple<Tail...>{},
              MPL::TypeTuple<Head...>{},
              MPL::TypeTuple<Head..., Tail0>{} // undo tuple
              );
          } else {
            return tryAddSumOperand<TreeData>(
              MPL::TypeTuple<Tail...>{},
              MPL::TypeTuple<Head..., Tail0>{}
              );
          }
        } else {
          using Try = OperationPair<F /*outer*/, NestedSumFunctor<F, TailF>, TreeData, Tail0, SubOptimizationTag>;
          constexpr ptrdiff_t complexityDelta = ComplexityDeltaV<Try>;
          if constexpr (complexityDelta > 0) {
            return tryAddSumOperand<TreeData, Try, complexityDelta>(
              MPL::TypeTuple<Tail...>{},
              MPL::TypeTuple<Head...>{},
              MPL::TypeTuple<Head..., Tail0>{} // undo tuple
              );
          } else {
            return tryAddSumOperand<TreeData>(
              MPL::TypeTuple<Tail...>{},
              MPL::TypeTuple<Head..., Tail0>{}
              );
          }
        }
      }

      template<class TreeData, class Tuple>
      using TryAddSumOperand = decltype(tryAddSumOperand<TreeData>(Tuple{}, MPL::TypeTuple<>{}));

      template<class TreeData, class Tuple>
      using IsReducingSumOperand = BoolConstant<(
        TryAddSumOperand<TreeData, Tuple>::size() > 0)>;

      template<class LeftDone,
               class RightProbeSet>
      constexpr decltype(auto) tryReduceSumOperands(LeftDone, RightProbeSet, MPL::TypeTuple<>)
      {
        return MPL::TypePair<LeftDone, RightProbeSet>{};
      }

      template<class... LeftDone,
               class RightProbeSet,
               class LeftProbeSet0, class... LeftProbeSet>
      constexpr decltype(auto) tryReduceSumOperands(
        MPL::TypeTuple<LeftDone...>, // unoptimizable operands of left probe-set
        RightProbeSet, // probe-set including already found optimization pairs
        MPL::TypeTuple<LeftProbeSet0, LeftProbeSet...> // to-be examined operands of left probe-set
        )
      {
        using TryReduce = TryAddSumOperand<LeftProbeSet0, RightProbeSet>;

        if constexpr (TryReduce::size() == 0) {
          return tryReduceSumOperands(
            MPL::TypeTuple<LeftDone..., LeftProbeSet0>{},
            RightProbeSet{},
            MPL::TypeTuple<LeftProbeSet...>{}
            );
        } else {
          return tryReduceSumOperands(
            MPL::TypeTuple<LeftDone...>{},
            TryReduce{},
            MPL::TypeTuple<LeftProbeSet...>{}
            );
        }
      }

      template<class Tuple0, class Tuple1>
      using TryReduceSumOperands =
        decltype(tryReduceSumOperands(
                   MPL::TypeTuple<>{},
                   Tuple1{}, Tuple0{}
                   ));

      template<class Tuple0, class Tuple1>
      using AreReducibleSumOperands =
        BoolConstant<(TryReduceSumOperands<Tuple0, Tuple1>::First::size() < Tuple0::size())>;

      ////////////////////////////////

      template<class F, class T, class Tuple = void>
      struct CountReducibleSumOperandsFor;

      template<class E>
      constexpr bool isPlusMinusOne()
      {
        if constexpr (ExpressionTraits<E>::isOne) {
          return true;
        } else if constexpr (IsUnaryMinusExpression<E>::value) {
          return ExpressionTraits<Operand<0, E> >::isOne;
        } else {
          return ExpressionTraits<E>::isMinusOne;
        }
      }

      template<class F, class T, class E>
      constexpr bool hasProperReducedComplexity()
      {
        if constexpr (FunctorHas<IsProductOperation, F>::value) {
          // ones are generated by factoring out, so do not advocate factoring in
          if constexpr (isPlusMinusOne<T>()) {
            return false;
          } else if constexpr (isPlusMinusOne<E>()) {
            return false;
          } else {
            return hasReducedComplexity<F, T, E>();
          }
        } else {
          return hasReducedComplexity<F, T, E>();
        }
      }

      template<class F, class T>
      constexpr bool hasProperReducedComplexity()
      {
        // double minus does not count here
        if constexpr (IsUnaryMinusExpression<T>::value) {
          return hasReducedComplexity<F, Operand<0, T> >();
        } else {
          return hasReducedComplexity<F, T>();
        }
      }

      template<class F, class... E>
      struct CountReducibleSumOperandsFor<F, MPL::TypeTuple<E...> >
        : IndexConstant<(0UL + ... + hasProperReducedComplexity<F, TreeExpression<E> >())>
      {};

      template<class F, class T, class... E>
      struct CountReducibleSumOperandsFor<F, T, MPL::TypeTuple<E...> >
        : IndexConstant<(0UL + ... + hasProperReducedComplexity<F, T, TreeExpression<E> >())>
      {};

      template<class F, class T, class... E>
      struct CountReducibleSumOperandsFor<F, MPL::TypeTuple<E...>, T>
        : IndexConstant<(0UL + ... + hasProperReducedComplexity<F, TreeExpression<E>, T>())>
      {};

      ///////////////////////////////////////////

      /**Identify suboptimizable operations on left and right sums.*/
      template<class F, class T0, class T1, class SFINAE = void>
      constexpr inline bool IsBinaryOperationOnSumV =
        (CountReducibleSumOperandsFor<F, T0, RightSumOperands<T1> >::value
         +
         CountReducibleSumOperandsFor<F, LeftSumOperands<T0>, T1>::value)
        > 0;

      template<class F, class T0, class T1>
      constexpr inline bool IsBinaryOperationOnSumV<
        F, T0, T1,
        std::enable_if_t<(!FunctorHas<IsDistributiveOperation, F>::value
                          || !(IsPlusOrMinusExpression<T0>::value
                               || IsPlusOrMinusExpression<T1>::value)
        )> > = false;

      template<class F, class T0, class T1,
               std::enable_if_t<IsBinaryOperationOnSumV<F, T0, T1>, int> = 0>
      constexpr decltype(auto) operate(OptimizeExpensive1 tag, F&& f, T0&& t0, T1&& t1)
      {
        if constexpr (CountReducibleSumOperandsFor<F, LeftSumOperands<T0>, T1>::value
            >
            CountReducibleSumOperandsFor<F, T0, RightSumOperands<T1> >::value) {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          //std::clog << "on left sum: " << t0.name() << " * " << t1.name() << std::endl;
          //std::clog << typeString(MPL::TypeTuple<F>{}) << std::endl;
          //static_assert(sizeof(T0) < 0, "");

          DUNE_ACFEM_EXPRESSION_RESULT(
            sumUp(
              std::forward<T0>(t0),
              std::forward<T1>(t1),
              LeftSumOperands<T0>{},
              OptimizeTop{},
              [&t1,&f](auto&& arg) -> decltype(auto) {
                return operate(OptimizeTop{}, std::forward<F>(f), std::forward<decltype(arg)>(arg), std::forward<T1>(t1));
              }
              )
            , "binary operation on left sum="
            );
        } else {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          //std::clog << "on right sum: " << t0.name() << " * " << t1.name() << std::endl;
          //std::clog << typeString(MPL::TypeTuple<F>{}) << std::endl;
          DUNE_ACFEM_EXPRESSION_RESULT(
            sumUp(
              std::forward<T0>(t0),
              std::forward<T1>(t1),
              RightSumOperands<T1>{},
              OptimizeTop{},
              [&t0,&f](auto&& arg) -> decltype(auto) {
                return operate(OptimizeTop{}, std::forward<F>(f), std::forward<T0>(t0), std::forward<decltype(arg)>(arg));
              }
              )
            , "binary operation on right sum"
            );
        }
      }

      /////////////////////////////////////////////////////////////

      /**Identify suboptimizable operations on sums.*/
      template<class F, class T, class SFINAE = void>
      constexpr inline bool IsUnaryOperationOnSumV =
        CountReducibleSumOperandsFor<F, LeftSumOperands<T> >::value > 0;

      template<class F, class T>
      constexpr inline bool IsUnaryOperationOnSumV<
        F, T,
        std::enable_if_t<(!FunctorHas<IsDistributiveOperation, F>::value
                          || !IsPlusOrMinusExpression<T>::value
        )> > = false;

      template<class F, class T, std::enable_if_t<IsUnaryOperationOnSumV<F, T>, int> = 0>
      constexpr auto operate(OptimizeExpensive1 tag, F&& f, T&& t)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        //std::clog << typeString(MPL::TypeTuple<F>{}) << std::endl;
        DUNE_ACFEM_EXPRESSION_RESULT(
          sumUp(
            std::forward<T>(t),
            zero(t),
            LeftSumOperands<T>{},
            OptimizeTop{},
            [&f](auto&& arg) -> decltype(auto) {
              return operate(OptimizeTop{}, std::forward<F>(f), std::forward<decltype(arg)>(arg));
            }
            )
          , "all-to-all sum optmization"
          );
      }

      /////////////////////////////////////////////////////////////

      /**@} ExpensiveOptimizations*/

    } // Sums::

    /**Optimize -0 to 0.*/
    template<class T>
    constexpr inline bool IsUnaryZeroV<MinusFunctor, T> = ExpressionTraits<T>::isZero;

    template<class T0, class T1>
    constexpr inline bool ReturnFirstV<PlusFunctor, T0, T1> = ZerosSignatureV<T0, T1> & 2;

    template<class T0, class T1>
    constexpr inline bool ReturnSecondV<PlusFunctor, T0, T1> = ZerosSignatureV<T0, T1> == 1;

    template<class T0, class T1>
    constexpr inline bool ReturnFirstV<MinusFunctor, T0, T1> = ZerosSignatureV<T0, T1> & 2;

    template<class T0, class T1>
    constexpr inline bool ReturnMinusSecondV<MinusFunctor, T0, T1> = ZerosSignatureV<T0, T1> == 1;

    template<class T0, class T1>
    constexpr inline bool ReturnFirstV<OperationTraits<PlusEqOperation>, T0, T1> = ExpressionTraits<T1>::isZero;

    template<class T0, class T1>
    constexpr inline bool ReturnFirstV<OperationTraits<MinusEqOperation>, T0, T1> = ExpressionTraits<T1>::isZero;

    template<class T0, class T1>
    constexpr inline bool IsBinaryZeroV<MinusFunctor, T0, T1> =
      AreRuntimeEqual<T0, T1>::value;

    namespace Sums {

      template<class F, class T0, class T1, class SFINAE = void>
      constexpr inline std::size_t NormalizeSumSignsV = 0;

      /**Optimize plus operations:
       *
       * + -A + B to B - A
       * + A + (-B) to A - B
       * + - A + (-B) to -(A+B)
       */
      template<class T0, class T1>
      constexpr inline std::size_t NormalizeSumSignsV<PlusFunctor, T0, T1, std::enable_if_t<ZerosSignatureV<T0, T1> == 0> > =
        SignSignatureV<T0, T1>;

      template<class F, class T0, class T1, std::enable_if_t<NormalizeSumSignsV<F, T0, T1> == 1, int> = 0>
      constexpr decltype(auto) operate(OptimizeNormalize, F&&, T0&& t0, T1&& t1)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        DUNE_ACFEM_EXPRESSION_RESULT(
          operate<MinusOperation>(std::forward<T1>(t1), std::forward<T0>(t0).operand(0_c))
          , "normalize -A + B -> B - A"
          );
      }

      template<class F, class T0, class T1, std::enable_if_t<NormalizeSumSignsV<F, T0, T1> == 2, int> = 0>
      constexpr decltype(auto) operate(OptimizeNormalize tag, F&&, T0&& t0, T1&& t1)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        DUNE_ACFEM_EXPRESSION_RESULT(
          operate<MinusOperation>(std::forward<T0>(t0), std::forward<T1>(t1).operand(0_c)),
          "normalize A + (-B) -> A - B"
          );
      }

      template<class F, class T0, class T1, std::enable_if_t<NormalizeSumSignsV<F, T0, T1> == 3, int> = 0>
      constexpr decltype(auto) operate(OptimizeNormalize tag, F&&, T0&& t0, T1&& t1)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        DUNE_ACFEM_EXPRESSION_RESULT(
          operate<MinusOperation>(operate<PlusOperation>(std::forward<T0>(t0).operand(0_c), std::forward<T1>(t1).operand(0_c)))
          , "normalize -A + (-B) -> -(A+B)"
          );
      }

      template<class F, class T0, class T1, class SFINAE = void>
      constexpr inline std::size_t NormalizeDifferenceSignsV = 0;

      /**Optimize plus operations:
       *
       * + -A + B to B - A
       * + A + (-B) to A - B
       * + - A + (-B) to -(A+B)
       */
      template<class T0, class T1>
      constexpr inline std::size_t NormalizeDifferenceSignsV<MinusFunctor, T0, T1, std::enable_if_t<ZerosSignatureV<T0, T1> == 0> > =
        SignSignatureV<T0, T1>;

      /** -A - B -> -(A+B) */
      template<class F, class T0, class T1, std::enable_if_t<NormalizeDifferenceSignsV<F, T0, T1> == 1, int> = 0>
      constexpr decltype(auto) operate(OptimizeNormalize, F&&, T0&& t0, T1&& t1)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        DUNE_ACFEM_EXPRESSION_RESULT(
          operate<MinusOperation>(operate<PlusOperation>(std::forward<T0>(t0).operand(0_c), std::forward<T1>(t1)))
          , "normalize -A - B -> -(A+B)"
          );
      }

      /**  A - (-B) -> A+B */
      template<class F, class T0, class T1, std::enable_if_t<NormalizeDifferenceSignsV<F, T0, T1> == 2, int> = 0>
      constexpr decltype(auto) operate(OptimizeNormalize, F&&, T0&& t0, T1&& t1)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        DUNE_ACFEM_EXPRESSION_RESULT(
          operate<PlusOperation>(std::forward<T0>(t0), std::forward<T1>(t1).operand(0_c))
          , "normalize A - (-B) -> A+B"
          );
      }

      /**  -A - (-B) -> B - A*/
      template<class F, class T0, class T1, std::enable_if_t<NormalizeDifferenceSignsV<F, T0, T1> == 3, int> = 0>
      constexpr decltype(auto) operate(OptimizeNormalize, F&&, T0&& t0, T1&& t1)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        DUNE_ACFEM_EXPRESSION_RESULT(
          operate<MinusOperation>(std::forward<T1>(t1).operand(0_c), std::forward<T0>(t0).operand(0_c)),
          "normalize -A - (-B) -> B - A"
          );
      }

      // surprisingly the following seems to be rather expensive.
      template<class F, class T, class SFINAE = void>
      constexpr inline bool FoldMinusIntoRightDifferenceV = false;

      template<class T>
      constexpr inline bool FoldMinusIntoRightDifferenceV<
        MinusFunctor, T, std::enable_if_t<IsProductExpression<T>::value>
        > = (IsBinaryMinusExpression<Operand<1, T> >::value
             && !AreRuntimeEqual<Operand<0, T>, Operand<1, T> >::value);

      /**@internal -(A * (C -B)) -> A * (B - C).
       *
       * We use "OptimizeLast" here because (C - B) normally should be
       * as "optimizable" as (B - C), so when this pattern should
       * occur it does not make sense to feed (B - C) again into the optimizer.
       */
      template<class F, class T, std::enable_if_t<FoldMinusIntoRightDifferenceV<F, T>, int> = 0>
      constexpr decltype(auto) operate(OptimizeLast tag, F&&, T&& t)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        DUNE_ACFEM_EXPRESSION_RESULT(
          operate(
            optimizeNext(tag),
            std::forward<T>(t).operation(),
            std::forward<T>(t).operand(0_c),
            operate(
              optimizeNext(tag),
              MinusFunctor{},
              std::forward<T>(t).operand(1_c).operand(1_c),
              std::forward<T>(t).operand(1_c).operand(0_c)
              )
            )
          , "-(A * (C -B)) -> A * (B - C)"
          );
      }

      template<class F, class T, class SFINAE = void>
      constexpr inline bool FoldMinusIntoLeftDifferenceV = false;

      template<class T>
      constexpr inline bool FoldMinusIntoLeftDifferenceV<
        MinusFunctor, T,
        std::enable_if_t<IsProductExpression<T>::value>
        > = (IsBinaryMinusExpression<Operand<0, T> >::value
             && !IsBinaryMinusExpression<Operand<1, T> >::value
             && !AreRuntimeEqual<Operand<0, T>, Operand<1, T> >::value);

      /**@internal -((A-B)*C) -> ((B-A)*C).
       *
       * We use "OptimizeLast" here because (A - B) normally should be
       * as "optimizable" as (B - A), so when this pattern should
       * occur it does not make sense to feed (B - A) again into the optimizer.
       */
      template<class F, class T, std::enable_if_t<FoldMinusIntoLeftDifferenceV<F, T>, int> = 0>
      constexpr decltype(auto) operate(OptimizeLast tag, F&&, T&& t)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        DUNE_ACFEM_EXPRESSION_RESULT(
          operate(
            optimizeNext(tag),
            std::forward<T>(t).operation(),
            operate(
              optimizeNext(tag),
              MinusFunctor{},
              std::forward<T>(t).operand(0_c).operand(1_c),
              std::forward<T>(t).operand(0_c).operand(0_c)
              ),
            std::forward<T>(t).operand(1_c)
            )
          , "-((A-B)*C) -> ((B-A)*C)"
          );
      }

      /**Remove do-nothing operations.*/
      template<class T>
      constexpr decltype(auto) operate(OptimizeTerminal3, OperationTraits<IdentityOperation >, T&& t)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        DUNE_ACFEM_EXPRESSION_RESULT(
          forwardReturnValue<T>(t)
          , "identity"
          );
      }

      /**Remove do-nothing operations.*/
      template<class T>
      constexpr decltype(auto) operate(OptimizeTerminal3, OperationTraits<PlusOperation >, T&& t)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        DUNE_ACFEM_EXPRESSION_RESULT(
          forwardReturnValue<T>(t)
          , "unary plus"
          );
      }

      /////////////////////////////////////////////////////////////////////////

      //! @} SumOptimization

      //! @} ExpressionOptimizations

    } // Sums::

    using Sums::operate;

  } // ACFem::Expressions::

} // Dune::

#endif // __DUNE_ACFEM_EXPRESSIONS_OPTIMIZESUMS_HH__
