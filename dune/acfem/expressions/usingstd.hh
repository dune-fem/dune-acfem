/*@file Inject using declarations into the surrounding namespace.
 */

/**@addtogroup ExpressionTemplates
 *
 * @{
 *
 */

/**@addtogroup ExpressionInterface
 *
 * @{
 */

using std::sqrt;

using std::cos;
using std::sin;
using std::tan;

using std::acos;
using std::asin;
using std::atan;

using std::exp;
using std::log;

using std::cosh;
using std::sinh;
using std::tanh;

using std::acosh;
using std::asinh;
using std::atanh;

using std::erf;
using std::lgamma;
using std::tgamma;

using std::ceil;
using std::floor;
using std::round;

using std::atan2;
using std::pow;

using std::min;
using std::max;

//!@}

//!@}
