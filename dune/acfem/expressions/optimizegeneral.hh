#ifndef __DUNE_ACFEM_EXPRESSIONS_OPTMIZEGENERAL_HH__
#define __DUNE_ACFEM_EXPRESSIONS_OPTMIZEGENERAL_HH__

#include "storage.hh"
#include "interface.hh"
#include "optimizationbase.hh"
#include "../common/literals.hh"

namespace Dune
{

  namespace ACFem
  {

    namespace Expressions
    {

      /**@addtogroup ExpressionTemplates
       *
       * @{
       *
       */

      /**@addtogroup ExpressionOptimization
       *
       * General optimization patterns.
       *
       * @{
       */

      template<class T0, class T1>
      constexpr inline std::size_t ZerosSignatureV =
        ExpressionTraits<T0>::isZero + 2*ExpressionTraits<T1>::isZero;

      template<class T0, class T1>
      constexpr inline std::size_t OnesSignatureV =
        ExpressionTraits<T0>::isOne + 2*ExpressionTraits<T1>::isOne;

      template<class T0, class T1>
      constexpr inline std::size_t MinusOnesSignatureV =
        ExpressionTraits<T0>::isMinusOne + 2*ExpressionTraits<T1>::isMinusOne;

      template<class T0, class T1>
      constexpr inline std::size_t SignSignatureV =
        IsUnaryMinusExpression<T0>::value + 2 * IsUnaryMinusExpression<T1>::value;

      //////////////////

      template<class F, class T0, class T1, class SFINAE = void>
      constexpr inline bool ReturnFirstV = false;

      template<class F, class T0, class T1, class SFINAE = void>
      constexpr inline bool ReturnSecondV = false;

      template<class F, class T0, class T1, class SFINAE = void>
      constexpr inline bool ReturnMinusFirstV = false;

      template<class F, class T0, class T1, class SFINAE = void>
      constexpr inline bool ReturnMinusSecondV = false;

      template<class F, class T0, class T1, class SFINAE = void>
      constexpr inline bool IsBinaryZeroV = false;

      template<class F, class T>
      constexpr inline bool IsUnaryZeroV = false;

      template<class F, class T>
      constexpr inline bool IsUnaryOneV = false;

      // canonical zero shortcut <- identity operation
      template<class T>
      constexpr auto zero(T&& t)
      {
        return zero(OperationTraits<IdentityOperation>{}, std::forward<T>(t));
      }

      // Optimizations performed at top-level

      template<class F, class T0, class T1, std::enable_if_t<ReturnFirstV<F, T0, T1>, int> = 0>
      constexpr decltype(auto) operate(OptimizeNormalize, F&&, T0&& t0, T1&& t1)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        DUNE_ACFEM_EXPRESSION_RESULT(
          forwardReturnValue<T0>(t0)
          , "forward first"
          );
      }

      template<class F, class T0, class T1, std::enable_if_t<ReturnSecondV<F, T0, T1>, int> = 0>
      constexpr decltype(auto) operate(OptimizeNormalize, F&&, T0&& t0, T1&& t1)
      {

        DUNE_ACFEM_RECORD_OPTIMIZATION;

        DUNE_ACFEM_EXPRESSION_RESULT(
          forwardReturnValue<T1>(t1)
          , "forward second"
          );
      }

      template<class F, class T0, class T1, std::enable_if_t<ReturnMinusFirstV<F, T0, T1>, int> = 0>
      constexpr decltype(auto) operate(OptimizeNormalize, F&&, T0&& t0, T1&& t1)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        DUNE_ACFEM_EXPRESSION_RESULT(
          operate<MinusOperation>(std::forward<T0>(t0))
          , "minus first"
          );
      }

      template<class F, class T0, class T1, std::enable_if_t<ReturnMinusSecondV<F, T0, T1>, int> = 0>
      constexpr decltype(auto) operate(OptimizeNormalize, F&&, T0&& t0, T1&& t1)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        DUNE_ACFEM_EXPRESSION_RESULT(
          operate<MinusOperation>(std::forward<T1>(t1))
          , "minus second"
          );
      }

      template<class F, class T0, class T1, std::enable_if_t<IsBinaryZeroV<F, T0, T1>, int> = 0>
      constexpr auto operate(OptimizeNormalize0, F&& f, T0&& t0, T1&& t1)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        DUNE_ACFEM_EXPRESSION_RESULT(
          zero(std::forward<F>(f), std::forward<T0>(t0), std::forward<T1>(t1))
          , "binary zero"
          );
      }

      template<class F, class T, std::enable_if_t<IsUnaryZeroV<F, T>, int> = 0>
      constexpr auto operate(OptimizeNormalize, F&& f, T&& t)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        DUNE_ACFEM_EXPRESSION_RESULT(
          zero(std::forward<F>(f), std::forward<T>(t))
          , "unary zero"
          );
      }

      template<class F, class T, std::enable_if_t<IsUnaryOneV<F, T>, int> = 0>
      constexpr auto operate(OptimizeNormalize, F&&, T&& t)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        DUNE_ACFEM_EXPRESSION_RESULT(
          one(std::forward<T>(t))
          , "unary one"
          );
      }

      // Optimizations performed to post-process the outcome of the more complex patterns.

      template<class F, class T0, class T1, std::enable_if_t<ReturnFirstV<F, T0, T1>, int> = 0>
      constexpr decltype(auto) operate(OptimizeTerminal3, F&&, T0&& t0, T1&& t1)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        DUNE_ACFEM_EXPRESSION_RESULT(
          forwardReturnValue<T0>(t0)
          , "terminal return first"
          );
      }

      template<class F, class T0, class T1, std::enable_if_t<ReturnSecondV<F, T0, T1>, int> = 0>
      constexpr decltype(auto) operate(OptimizeTerminal3, F&&, T0&& t0, T1&& t1)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        DUNE_ACFEM_EXPRESSION_RESULT(
          forwardReturnValue<T1>(t1)
          , "terminal return second"
          );
      }

      template<class F, class T0, class T1, std::enable_if_t<IsBinaryZeroV<F, T0, T1>, int> = 0>
      constexpr decltype(auto) operate(OptimizeTerminal2, F&& f, T0&& t0, T1&& t1)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        DUNE_ACFEM_EXPRESSION_RESULT(
          zero(std::forward<F>(f), std::forward<T0>(t0), std::forward<T1>(t1))
          , "terminal binary zero"
          );
      }

      template<class F, class T, std::enable_if_t<IsUnaryZeroV<F, T>, int> = 0>
      constexpr auto operate(OptimizeTerminal3, F&& f, T&& t)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        DUNE_ACFEM_EXPRESSION_RESULT(
          zero(std::forward<F>(f), std::forward<T>(t))
          , "terminal unary zero"
          );
      }

      template<class F, class T, std::enable_if_t<IsUnaryOneV<F, T>, int> = 0>
      constexpr auto operate(OptimizeTerminal3, F&&, T&& t)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;
        return one(std::forward<T>(t));
      }

      ///////////////////////////////////////////

      //! @} ExpressionOptimization

      //! @} ExpressionOptimizations

    } // Expressions

  } // ACFem

} // Dune

#endif // __DUNE_ACFEM_EXPRESSIONS_OPTMIZEGENERAL_HH__
