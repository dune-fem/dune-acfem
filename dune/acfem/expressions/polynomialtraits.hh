#ifndef __DUNE_ACFFEM_EXPRESSIONS_POLYNOMIALTRAITS_HH_
#define __DUNE_ACFFEM_EXPRESSIONS_POLYNOMIALTRAITS_HH_

#include "../common/literals.hh"

#include "expressionoperations.hh"
#include "operationtraits.hh"
#include "terminal.hh"

namespace Dune {

  namespace ACFem {

    namespace Expressions {

      using namespace ACFem::Literals;

      constexpr inline std::size_t UndeterminedPolynomialDegree = 1024;

      template<class E, std::size_t IndeterminateId, class SFINAE = void>
      struct HasStaticOrderMethod
        : FalseType
      {};

      template<class E, std::size_t IndeterminateId>
      struct HasStaticOrderMethod<E, IndeterminateId, VoidType<decltype(std::decay_t<E>::order(IndexConstant<IndeterminateId>{}))> >
        : TrueType
      {};

      template<class E, std::size_t IndeterminateId, class SFINAE = void>
      struct HasOrderMethod
        : FalseType
      {};

      template<class E>
      struct HasOrderMethod<E, 0, VoidType<decltype(std::declval<E>().order())> >
        : TrueType
      {};

      template<class E, std::size_t IndeterminateId, class SFINAE = void>
      struct HasConstantOperands
        : FalseType
      {};

      namespace {

        template<class E, std::size_t IndeterminateId, std::size_t... I>
        constexpr auto polynomialDegreeExpander(E&& e, IndexConstant<IndeterminateId>, IndexSequence<I...>)
        {
          return Functor<E>::polynomialOrder(
            polynomialDegree(PriorityTag<42>{}, std::forward<E>(e).template operand<I>(), IndexConstant<IndeterminateId>{})...);
        }

        template<class Number>
        constexpr auto clampOrder(Number&& arg)
        {
          if constexpr (IsTypedValue<Number>::value) {
            return Number{};
          } else {
            return std::min(static_cast<std::size_t>(arg), UndeterminedPolynomialDegree);
          }
        }

      }

      /**Polynomial degree of an expression. Implementations can
       * override this by implementing an alike call pattern with a
       * hight PriorityTag.
       */
      template<std::size_t IndeterminateId, class E>
      constexpr auto polynomialDegree(PriorityTag<0>, E&& e, IndexConstant<IndeterminateId> = IndexConstant<IndeterminateId>{})
      {
        if constexpr (IsIndeterminateExpression<E>::value) {
          if constexpr (IndeterminateTraits<E>::id_ == IndeterminateId) {
            return 1_f;
          } else {
            return 0_f;
          }
        }
        else if constexpr (HasStaticOrderMethod<E, IndeterminateId>::value) {
          return clampOrder(std::decay_t<E>::order(IndexConstant<IndeterminateId>{}));
        }
        else if constexpr (HasOrderMethod<E, IndeterminateId>::value) {
          return clampOrder(std::forward<E>(e).order());
        }
        else if constexpr (IsSquareExpression<E>::value) {
          return clampOrder(2_f * polynomialDegree<IndeterminateId>(PriorityTag<42>{}, std::forward<E>(e).operand(0_c)));
        }
        else if constexpr (IsSelfExpression<E>::value) {
          return 0_f;
        }
        else if constexpr (IsPlaceholderExpression<E>::value) {
          return 0_f;
        }
        else if constexpr (!IsExpression<E>::value) {
          return 0_f;
        } else {
          // recurse to OperationTraits
          return clampOrder(
            polynomialDegreeExpander(
              std::forward<E>(e),
              IndexConstant<IndeterminateId>{},
              MakeIndexSequence<Arity<E>::value>{}
              )
            );
        }
      }

      /////////////////////////////////////////////////////////////////////

      template<class E, std::size_t IndeterminateId = 0>
      constexpr auto polynomialDegree(E&& e, IndexConstant<IndeterminateId> = IndexConstant<IndeterminateId>{})
      {
        return clampOrder(polynomialDegree(PriorityTag<42>{}, std::forward<E>(e), IndexConstant<IndeterminateId>{}));
      }

      template<std::size_t IndeterminateId, class E>
      constexpr auto polynomialDegree(E&& e)
      {
        return polynomialDegree(std::forward<E>(e), IndexConstant<IndeterminateId>{});
      }

      template<class E, class IndeterminateId = IndexConstant<0>, class SFINAE = void>
      struct HasConstantPolynomialDegree
        : FalseType
      {};

      template<class E, class IndeterminateId>
      struct HasConstantPolynomialDegree<
        E, IndeterminateId,
        std::enable_if_t<IsTypedValue<decltype(polynomialDegree(std::declval<E>(), IndeterminateId{}))>::value> >
        : TrueType
      {};

      template<class E, std::size_t IndeterminateId = 0>
      constexpr bool hasConstantPolynomialDegree(IndexConstant<IndeterminateId> = IndexConstant<IndeterminateId>{})
      {
        return HasConstantPolynomialDegree<E, IndexConstant<IndeterminateId> >::value;
      }

      template<class E, std::size_t IndeterminateId = 0,
               std::enable_if_t<HasConstantPolynomialDegree<E, IndexConstant<IndeterminateId> >::value, int> = 0>
      constexpr auto polynomialDegree(IndexConstant<IndeterminateId> = IndexConstant<IndeterminateId>{})
      {
        using DegreeType = decltype(polynomialDegree(std::declval<E>(), IndexConstant<IndeterminateId>{}));
        return DegreeType{};
      }

      template<std::size_t Degree, class E, std::size_t IndeterminateId = 0>
      constexpr bool hasConstantPolynomialDegreeOf(IndexConstant<IndeterminateId> = IndexConstant<IndeterminateId>{})
      {
        if constexpr (HasConstantPolynomialDegree<E, IndexConstant<IndeterminateId> >::value) {
          return BoolConstant<(Degree == polynomialDegree<E, IndeterminateId>())>::value;
        } else {
          return false;
        }
      }

      template<std::size_t Degree, class E, class IndeterminateId = IndexConstant<0> >
      using HasConstantPolynomialDegreeOf = BoolConstant<hasConstantPolynomialDegreeOf<Degree, E, IndeterminateId::value>()>;

      template<class E, class IndeterminateId = IndexConstant<0> >
      using HasPolynomialDegreeZero = HasConstantPolynomialDegreeOf<0, E, IndeterminateId>;

      template<std::size_t Degree, class E, std::size_t IndeterminateId = 0>
      constexpr bool hasConstantPolynomialDegreeAtMost(IndexConstant<IndeterminateId> = IndexConstant<IndeterminateId>{})
      {
        if constexpr (HasConstantPolynomialDegree<E, IndexConstant<IndeterminateId> >::value) {
          return BoolConstant<(Degree >= polynomialDegree<E, IndeterminateId>())>::value;
        } else {
          return false;
        }
      }

      template<std::size_t Degree, class E, class IndeterminateId = IndexConstant<0> >
      using HasConstantPolynomialDegreeAtMost = BoolConstant<hasConstantPolynomialDegreeAtMost<Degree, E, IndeterminateId::value>()>;

      /**Optimize for the case of constant operands.*/
      template<std::size_t IndeterminateId, class E>
      constexpr auto polynomialDegree(PriorityTag<42>, E&& e, IndexConstant<IndeterminateId> = IndexConstant<IndeterminateId>{})
      {
        if constexpr (IsSelfExpression<E>::value
                      || IsPlaceholderExpression<E>::value
                      || IsIndeterminateExpression<E>::value
          ) {
          return clampOrder(polynomialDegree(PriorityTag<41>{}, std::forward<E>(e), IndexConstant<IndeterminateId>{}));
        } else if constexpr (AllOperandsAre<HasPolynomialDegreeZero, E, IndexConstant<IndeterminateId> >::value) {
          return 0_f; // keep it at zero
        } else {
          return clampOrder(polynomialDegree(PriorityTag<41>{}, std::forward<E>(e), IndexConstant<IndeterminateId>{}));
        }
      }

    } // NS Expressions

    using Expressions::polynomialDegree;
    using Expressions::HasConstantPolynomialDegree;
    using Expressions::hasConstantPolynomialDegree;
    using Expressions::hasConstantPolynomialDegreeOf;
    using Expressions::hasConstantPolynomialDegreeAtMost;

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFFEM_EXPRESSIONS_POLYNOMIALTRAITS_HH_
