#ifndef __DUNE_ACFEM_EXPRESSIONS_STORAGE_HH__
#define __DUNE_ACFEM_EXPRESSIONS_STORAGE_HH__

#include "../common/types.hh"
#include "../mpl/access.hh"
#include "policy.hh"
#include "typetraits.hh"
#include "expressiontraits.hh"
#include "expressionoperations.hh"

namespace Dune
{

  namespace ACFem
  {

    namespace Expressions
    {
      /**@addtogroup ExpressionTemplates
       *
       * @{
       */

      /**@addtogroup ExpressionStorage
       *
       * Expressions store their arguments as copies if constructed from
       * rvalue references or as references if constructed from lvalues.
       *
       * @{
       */

      namespace StorageImpl {

        template<class Op, class SFINAE = void>
        struct IsSubExpressionOperation
          : FalseType
        {};

        template<class TreePos>
        struct IsSubExpressionOperation<SubExpressionOperation<TreePos> >
          : TrueType
        {};

        template<class C, class SFINAE = void>
        struct HasSubExpressionOperation
          : FalseType
        {};

        template<class C>
        struct HasSubExpressionOperation<C, VoidType<typename std::decay_t<C>::OperationType> >
          : IsSubExpressionOperation<typename std::decay_t<C>::OperationType>
        {};

      }

      /**Force some things to always be emitted as non-references.*/
      template<class T>
      static constexpr bool EmitByValueV = IsTypedValue<T>::value && !StorageImpl::HasSubExpressionOperation<T>::value;

      template<class F, class... T>
      class Storage
      {
        using ThisType = Storage;
       protected:
        using StorageType = std::tuple<T...>;
       public:
        using FunctorType = F;
        using OperationType = typename FunctorType::OperationType;

        template<std::size_t I>
        using OperandType = TupleElement<I, StorageType>;

        template<std::size_t I>
        using DecayOperand = std::decay_t<OperandType<I> >;

        static constexpr std::size_t arity_ = sizeof...(T);

       private:
        // work around a clang is_constructible bug when
        // is_constructible would be called with too few
        // arguments. clang seemingly tries to test
        // default-construction of missing arguments and fails.
        template<class... C>
        struct TypeTuple
        {};

        template<class Args, class SFINAE = void>
        struct IsStorageConstructibleHelper
          : FalseType
        {};

        // for clang: allow failure if the number of arguments does not match.
        template<class... TArg>
        struct IsStorageConstructibleHelper<
          TypeTuple<TArg...>,
          std::enable_if_t<(sizeof...(T)) !=  (sizeof...(TArg))>
          >
          : FalseType
        {};

        template<class... TArg>
        struct IsStorageConstructibleHelper<
          TypeTuple<TArg...>,
          std::enable_if_t<(sizeof...(T)) ==  (sizeof...(TArg))>
          >
          : std::is_constructible<StorageType, TArg...>
        {};

        template<class... TArg>
        using IsStorageConstructible = IsStorageConstructibleHelper<TypeTuple<TArg...> >;

       public:
        template<
          class... TArg,
          std::enable_if_t<IsStorageConstructible<TArg...>::value, int> = 0>
        Storage(const FunctorType& f, TArg&&... t)
          : functor_(f), operands_(std::forward<TArg>(t)...)
        {}

        template<class... TArg, std::enable_if_t<IsStorageConstructible<TArg...>::value, int> = 0>
        Storage(TArg&&... t)
          : Storage(F{}, std::forward<TArg>(t)...)
        {}

        FunctorType operation()&&
        {
          return functor_;
        }

        decltype(auto) operation()&
        {
          return functor_;
        }

        const auto& operation() const&
        {
          return functor_;
        }

        /**Force some things to always be emitted as non-references.*/
        template<std::size_t I>
        static constexpr bool emitByValue_ = EmitByValueV<OperandType<I> >;

        template<
          std::size_t I,
          std::enable_if_t<emitByValue_<I>, int> = 0>
        DecayOperand<I> operand(IndexConstant<I> = IndexConstant<I>{}) const
        {
          return get<I>(operands_);
        }

        /**Return one of the stored operands in an rvalue context. In
         * this case exactly the contained type is returned, i.e. a
         * copy for objects or a reference for references.
         *
         * As a special exception operands for which IsTypedValue
         * evaluates to std::true_type are always returned as copy. It
         * is assumed that those do not contain any runtime
         * information.
         */
        template<std::size_t I, std::enable_if_t<!emitByValue_<I>, int> = 0>
        OperandType<I>&& operand(IndexConstant<I> = IndexConstant<I>{}) &&
        {
          return get<I>(operands_);
        }

        /**Return one of the stored operands in a mutable reference
         * context. In this case we assume that it is safe to return a
         * reference as the storage container should be alive in a
         * real object.
         *
         * As a special exception operands for which IsTypedValue
         * evaluates to std::true_type are always returned as copy. It
         * is assumed that those do not contain any runtime
         * information.
         */
        template<
          std::size_t I,
          std::enable_if_t<!emitByValue_<I>, int> = 0>
        decltype(auto) operand(IndexConstant<I> = IndexConstant<I>{}) &
        {
          return get<I>(operands_);
        }

        /**Return one of the stored operands in a const reference
         * context. In this case we assume that it is safe to return a
         * reference as the storage container should be alive in a
         * real object.
         *
         * As a special exception operands for which IsTypedValue
         * evaluates to std::true_type are always returned as copy. It
         * is assumed that those do not contain any runtime
         * information.
         */
        template<
          std::size_t I,
          std::enable_if_t<!emitByValue_<I>, int> = 0>
        decltype(auto) operand(IndexConstant<I> = IndexConstant<I>{}) const&
        {
          return get<I>(operands_);
        }

        static constexpr std::size_t arity()
        {
          return sizeof...(T);
        }

       protected:
        F functor_;
        StorageType operands_;
      };

      /////////////////////////////////////////////////////////////////////////

      template<class F, class T0, class T1>
      class Storage<F, T0, T1>
      {
        using ThisType = Storage;
       public:
        using FunctorType = F;
        using OperationType = typename FunctorType::OperationType;

        template<std::size_t I>
        using OperandType = ConditionalType<I == 0, T0, T1>;

        template<std::size_t I>
        using DecayOperand = std::decay_t<OperandType<I> >;

        static constexpr std::size_t arity_ = 2;

        template<
          class T0Arg, class T1Arg,
          std::enable_if_t<std::is_constructible<T0, T0Arg>::value && std::is_constructible<T1, T1Arg>::value, int> = 0>
        Storage(const FunctorType& f, T0Arg&& t0, T1Arg&& t1)
          : functor_(f), t0_(std::forward<T0Arg>(t0)), t1_(std::forward<T1Arg>(t1))
        {}

        template<
          class T0Arg, class T1Arg,
          std::enable_if_t<std::is_constructible<T0, T0Arg>::value && std::is_constructible<T1, T1Arg>::value, int> = 0>
        Storage(T0Arg&& t0, T1Arg&& t1)
          : Storage(F{}, std::forward<T0Arg>(t0), std::forward<T1Arg>(t1))
        {}

        FunctorType operation()&&
        {
          return functor_;
        }

        decltype(auto) operation()&
        {
          return functor_;
        }

        const auto& operation() const&
        {
          return functor_;
        }

        /**Return one of the stored operands in an rvalue context. In
         * this case exactly the contained type is returned, i.e. a
         * copy for objects or a reference for references.
         *
         * As a special exception operands for which IsTypedValue
         * evaluates to std::true_type are always returned as copy. It
         * is assumed that those do not contain any runtime
         * information.
         */
        template<std::size_t I>
        decltype(auto) operand(IndexConstant<I> = IndexConstant<I>{}) &&
        {
          if constexpr (I == 0) {
            if constexpr (EmitByValueV<T0>) {
              return static_cast<std::decay_t<T0> >(t0_);
            } else {
              return static_cast<T0&&>(t0_);
            }
          } else {
            if constexpr (EmitByValueV<T1>) {
              return static_cast<std::decay_t<T1> >(t1_);
            } else {
              return static_cast<T1&&>(t1_);
            }
          }
        }

        /**Return one of the stored operands in a mutable reference
         * context. In this case we assume that it is safe to return a
         * reference as the storage container should be alive in a
         * real object.
         *
         * As a special exception operands for which IsTypedValue
         * evaluates to std::true_type are always returned as copy. It
         * is assumed that those do not contain any runtime
         * information.
         */
        template<std::size_t I>
        decltype(auto) operand(IndexConstant<I> = IndexConstant<I>{}) &
        {
          if constexpr (I == 0) {
            if constexpr (EmitByValueV<T0>) {
              return static_cast<std::decay_t<T0> >(t0_);
            } else {
              return static_cast<T0&>(t0_);
            }
          } else {
            if constexpr (EmitByValueV<T1>) {
              return static_cast<std::decay_t<T1> >(t1_);
            } else {
              return static_cast<T1&>(t1_);
            }
          }
        }

        /**Return one of the stored operands in a const reference
         * context. In this case we assume that it is safe to return a
         * reference as the storage container should be alive in a
         * real object.
         *
         * As a special exception operands for which IsTypedValue
         * evaluates to std::true_type are always returned as copy. It
         * is assumed that those do not contain any runtime
         * information.
         */
        template<std::size_t I>
        decltype(auto) operand(IndexConstant<I> = IndexConstant<I>{}) const&
        {
          if constexpr (I == 0) {
            if constexpr (EmitByValueV<T0>) {
              return static_cast<std::decay_t<T0> >(t0_);
            } else {
              return static_cast<const T0&>(t0_);
            }
          } else {
            if constexpr (EmitByValueV<T1>) {
              return static_cast<std::decay_t<T1> >(t1_);
            } else {
              return static_cast<const T1&>(t1_);
            }
          }
        }

        static constexpr std::size_t arity()
        {
          return 2;
        }

       protected:
        F functor_;
        T0 t0_;
        T1 t1_;
      };

      /////////////////////////////////////////////////////////////////////////

      template<class F, class T0>
      class Storage<F, T0>
      {
        using ThisType = Storage;
       public:
        using FunctorType = F;
        using OperationType = typename FunctorType::OperationType;

        template<std::size_t I>
        using OperandType = T0;

        template<std::size_t I>
        using DecayOperand = std::decay_t<T0>;

        static constexpr std::size_t arity_ = 1;

        template<
          class T0Arg,
          std::enable_if_t<std::is_constructible<T0, T0Arg>::value, int> = 0>
        Storage(const FunctorType& f, T0Arg&& t0)
          : functor_(f), t0_(std::forward<T0Arg>(t0))
        {}

        template<
          class T0Arg,
          std::enable_if_t<std::is_constructible<T0, T0Arg>::value, int> = 0>
        Storage(T0Arg&& t0)
          : Storage(F{}, std::forward<T0Arg>(t0))
        {}

        FunctorType operation()&&
        {
          return functor_;
        }

        decltype(auto) operation()&
        {
          return functor_;
        }

        const auto& operation() const&
        {
          return functor_;
        }

       private:
        /**Force some things to always be emitted as non-references.*/
        static constexpr bool emitByValue_ = EmitByValueV<T0>;

       public:
        template<std::size_t I>
        decltype(auto) operand(IndexConstant<I> = IndexConstant<I>{}) &&
        {
          if constexpr (emitByValue_) {
            return static_cast<std::decay_t<T0> >(t0_);
          } else {
            return static_cast<T0&&>(t0_);
          }
        }

        template<std::size_t I>
        decltype(auto) operand(IndexConstant<I> = IndexConstant<I>{}) &
        {
          if constexpr (emitByValue_) {
            return static_cast<std::decay_t<T0> >(t0_);
          } else {
            return static_cast<T0&>(t0_);
          }
        }

        template<std::size_t I>
        decltype(auto) operand(IndexConstant<I> = IndexConstant<I>{}) const&
        {
          if constexpr (emitByValue_) {
            return static_cast<std::decay_t<T0> >(t0_);
          } else {
            return static_cast<const T0&>(t0_);
          }
        }

        static constexpr std::size_t arity()
        {
          return 1;
        }

       protected:
        F functor_;
        T0 t0_;
      };

      /////////////////////////////////////////////////////////////////////////

      template<class T, class SFINAE = void>
      struct IsExpression
        : FalseType
      {};

      /**Identify something conforming to the expression interface.*/
      template<class T>
      struct IsExpression<
        T,
        VoidType<void
                 // check for methods, gcc does not like this with non-static methods.
//		 , decltype(std::declval<std::decay_t<T> >().operation())
//		 , decltype(std::declval<T>().template operand<0>())
                 , decltype(std::decay_t<T>::arity())
                 // check for types
#if 0
                 , typename std::decay_t<T>::template OperandType<0>
                 , typename std::decay_t<T>::FunctorType
                 , typename std::decay_t<T>::OperationType
#endif
                 >
          >
          : TrueType
      {
#if 0
	static_assert(sizeof(decltype(std::declval<T>().template operand<0>())) >= 0,
		      "Expressions should define the operand template.");
#endif
      };

      template<class F, class SFINAE = void>
      struct IsFunctor
        : FalseType
      {};

      template<class F>
      struct IsFunctor<F, VoidType<typename std::decay_t<F>::OperationType, typename std::decay_t<F>::InvertibleOperation> >
        : TrueType
      {};

      namespace impl {
        template<class T, class SFINAE = void>
        struct ExpressionFunctor
        {
          using Type  = void;
        };

        template<class T>
        struct ExpressionFunctor<T, std::enable_if_t<IsExpression<T>::value> >
        {
          using Type = typename std::decay_t<T>::FunctorType;
        };

        template<class T, class SFINAE = void>
        struct ExpressionOperation
        {
          using Type = void;
        };

        template<class T>
        struct ExpressionOperation<T, std::enable_if_t<IsExpression<T>::value> >
        {
          using Type = typename std::decay_t<T>::OperationType;
        };

        template<class T, class SFINAE = void>
        struct ExpressionArity
        {
          using Type = IndexConstant<0>;
        };

        template<class T>
        struct ExpressionArity<T, std::enable_if_t<IsExpression<T>::value> >
        {
          using Type = IndexConstant<std::decay_t<T>::arity()>;
        };

        template<std::size_t I, class T, class SFINAE = void>
        struct ExpressionOperand;

        template<std::size_t I, class T>
        struct ExpressionOperand<
          I, T,
          std::enable_if_t<(ExpressionArity<T>::Type::value > I)> >
        {
          using Type = typename std::decay_t<T>::template OperandType<I>;
        };

      }

      template<std::size_t I, class T>
      using Operand = typename impl::ExpressionOperand<I, T>::Type;

      template<class T>
      using Functor = typename impl::ExpressionFunctor<T>::Type;

      template<class T>
      using Operation = typename impl::ExpressionOperation<T>::Type;

      template<class T>
      using Arity = typename impl::ExpressionArity<T>::Type;

      /**TrueType if T is an expression of arity N, otherwise FalseType.*/
      template<std::size_t N, class T>
      struct IsExpressionOfArity
        : BoolConstant<Arity<T>::value == N>
      {};

      /**TrueType if T is an expression of arity 1, otherwise FalseType.*/
      template<class T>
      struct IsUnaryExpression
        : IsExpressionOfArity<1, T>
      {};

      /**TrueType if T is an expression of arity 2, otherwise FalseType.*/
      template<class T>
      struct IsBinaryExpression
        : IsExpressionOfArity<2, T>
      {};

      namespace {

        template<template<class...> class Predicate, class E, class Seq, class... Rest>
        struct AnyOperandIsExpander;

        template<template<class...> class Predicate, class E, std::size_t... I, class... Rest>
        struct AnyOperandIsExpander<Predicate, E, IndexSequence<I...>, Rest...>
          : BoolConstant<(... || Predicate<Operand<I, E>, Rest...>::value)>
        {};

        template<template<class...> class Predicate, class E, class Seq, class... Rest>
        struct AllOperandsAreExpander;

        template<template<class...> class Predicate, class E, std::size_t... I, class... Rest>
        struct AllOperandsAreExpander<Predicate, E, IndexSequence<I...>, Rest...>
          : BoolConstant<(... && Predicate<Operand<I, E>, Rest...>::value)>
        {};

      }

      template<template<class...> class Predicate, class T, class... Rest>
      using AnyOperandIs = AnyOperandIsExpander<Predicate, T, MakeIndexSequence<Arity<T>::value>, Rest...>;

      template<template<class...> class Predicate, class T, class... Rest>
      using AllOperandsAre = AllOperandsAreExpander<Predicate, T, MakeIndexSequence<Arity<T>::value>, Rest...>;

      template<template<class...> class Predicate, std::size_t N, class T, class SFINAE = void>
      struct OperandHasProperty
        : FalseType
      {};

      template<template<class...> class Predicate, std::size_t N, class T>
      struct OperandHasProperty<Predicate, N, T, std::enable_if_t<(Arity<T>::value > N)> >
        : Predicate<Operand<N, T> >
      {};

      /**@interal SFINAE default for examining the functors of
       * operands when we do not know whether T has sufficiently many operands.
       */
      template<class T, class SFINAE = void>
      struct SameOperandFunctors
        : FalseType
      {};

      /**@internal Unary expression operands have identical functors ;).*/
      template<class T>
      struct SameOperandFunctors<T, std::enable_if_t<IsUnaryExpression<T>::value> >
        : TrueType
      {};

      /**@internal Binary expression operands have to be examined.*/
      template<class T>
      struct SameOperandFunctors<
        T,
        std::enable_if_t<(IsBinaryExpression<T>::value
                          && std::is_same<Functor<Operand<0, T> >, Functor<Operand<1, T> > >::value
        )> >
        : TrueType
      {};

      /**@internal Arbitrary expression operands are not implemented.*/
      template<class T>
      struct SameOperandFunctors<
        T,
        std::enable_if_t<(IsExpression<T>::value
                          && Arity<T>::value> 2
        )> >
        : FalseType
      {
        static_assert(Arity<T>::value> 2, "Not Implemented for more than binary expressions.");
      };

      template<std::size_t I, class T>
      decltype(auto) operand(T&& t, IndexConstant<I> = IndexConstant<I>{})
      {
        static_assert(IsExpression<T>::value, "Trying to get operand from non-expression.");
        return std::forward<T>(t).template operand<I>();
      }

      template<class T>
      decltype(auto) operation(T&& t)
      {
        static_assert(IsExpression<T>::value, "Trying to get operation from non-expression.");
        return t.operation();
      }

      template<class T>
      constexpr std::size_t arity()
      {
        static_assert(IsExpression<T>::value, "Trying to get arity from non-expression.");
        return std::decay_t<T>::arity();
      }

      template<class T>
      constexpr std::size_t arity(T&&)
      {
        return arity<T>();
      }

      template<std::size_t I, class T>
      constexpr inline bool ByValueOperandV = EmitByValueV<Operand<I, T> >;

      /**Evaluate to @c true if an instance of T can be treated as
       * constant argument to an expresion. Knowledge about this opens
       * optimization possibilities.
       *
       * @param[in] T Some type, references are allowed.
       */
      template<class T>
      using IsConstantExprArg =
        BoolConstant<!ExpressionTraits<T>::isVolatile
                     &&
                     (ExpressionTraits<T>::isTypedValue
                      || ExpressionTraits<T>::isConstant
                      || (!std::is_reference<T>::value && ExpressionTraits<T>::isIndependent))>;

      /**@internal Check for dangling reference returned from operand() call.*/
      template<std::size_t I, class T>
      constexpr void danglingReferenceCheck(T&& t, IndexConstant<I> = IndexConstant<I>{})
      {
        // This could happen if ByValueOperand is in effect
        static_assert(!(std::is_rvalue_reference<decltype(t)>::value
                        && std::is_reference<Operand<I, T> >::value
                        && !ByValueOperandV<I, T>
                        && !std::is_reference<decltype(std::forward<T>(t).template operand<I>())>::value),
                      "Reference operand returned by value from rvalue expression.");

        // This is the bad one
        static_assert(!(std::is_rvalue_reference<decltype(t)>::value
                        && !std::is_reference<Operand<I, T> >::value
                        && std::is_lvalue_reference<decltype(std::forward<T>(t).template operand<I>())>::value),
                      "Non-reference operand returned by reference from rvalue expression.");
      }

      /**Generate an expression storage container.*/
      template<class F, class... T>
      constexpr auto storage(const F& f, T&&... t)
      {
        return Storage<F, T...>(f, std::forward<T>(t)...);
      }

      //! @} ExpressionStorage

      //! @} ExpressionTemplates

    } // Expressions

    using Expressions::IsExpression;
    using Expressions::IsExpressionOfArity;
    using Expressions::IsUnaryExpression;
    using Expressions::IsBinaryExpression;
    using Expressions::IsConstantExprArg;

  } // ACFem

} // Dune

#endif // __DUNE_ACFEM_EXPRESSIONS_STORAGE_HH__
