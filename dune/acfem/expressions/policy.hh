#ifndef __DUNE_ACFEM_EXPRESSIONS_POLICY_HH__
#define __DUNE_ACFEM_EXPRESSIONS_POLICY_HH__

#include "../common/types.hh"

namespace Dune {

  namespace ACFem {

    namespace Expressions {

      namespace Policy {

        using FloatingPointDefault = double;

        /**Used to disambiguate ExpressionTraits implementations, see
         * traitsdefault.hh for its usage.
         */
        using TraitsLevelMax = IndexConstant<10>;

        /**Maximum optimization levels. Optimization levels do @b not
         * model increasing optimization with higher level but are
         * used to disambiguate optimization patterns. Choosing a
         * larger value will potentially increase the compilation
         * time.
         */
        using OptimizationLevelMax = IndexConstant<15>;

        /**The default weight of an expression if not overridden. The
         * weight of an expression can be used to sort expression
         * types.
         */
        constexpr inline std::size_t WeightDefaultV = 1000UL;

      } // NS Policy

    } // NS Expressions

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_COMMON_POLICY_HH__
