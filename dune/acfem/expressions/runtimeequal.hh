#ifndef __DUNE_ACFEM_EXPRESSIONS_RUNTIMEEQUAL_HH__
#define __DUNE_ACFEM_EXPRESSIONS_RUNTIMEEQUAL_HH__

#include "decay.hh"
#include "storage.hh"
#include "interface.hh"
#include "terminal.hh"
#include "transform.hh"
#include "../common/literals.hh"

namespace Dune
{

  namespace ACFem
  {

    namespace Expressions
    {

      /**@addtogroup ExpressionTemplates
       *
       * @{
       *
       */

      /**@addtogroup ExpressionTraits
       *
       * @{
       */

      template<class T, class SFINAE = void>
      struct IsRuntimeEqual
        : FalseType
      {};

      template<class T, class SFINAE = void>
      struct IsBasicRuntimeEqual
        : FalseType
      {};

      template<class T>
      struct IsBasicRuntimeEqual<
        T,
        std::enable_if_t<(!IsSubExpressionExpression<T>::value
                          && (IsTypedValue<T>::value
                              || IsIndeterminateExpression<T>::value
                              || HasTag<T, RuntimeEqualExpression>::value)
        )> >
        : TrueType
      {};

      namespace {

        template<std::size_t ArgIdx, class T, class SFINAE = void>
        struct IsRTEqualOperand
          : FalseType
        {};

        template<std::size_t ArgIdx, class T>
        struct IsRTEqualOperand<
          ArgIdx,
          T,
          std::enable_if_t<IsRuntimeEqual<std::enable_if_t<!IsSelfExpression<T>::value, Operand<ArgIdx, T> > >::value>
          >
          : TrueType
        {};

        template<class T, class Seq, class SFINAE = void>
        struct HasRTEqualOperands
          : FalseType
        {};

        template<class T, std::size_t... ArgIdx>
        struct HasRTEqualOperands<
          T,
          IndexSequence<ArgIdx...>,
          std::enable_if_t<AccumulateSequence<LogicalAndFunctor, BoolSequence<IsRTEqualOperand<ArgIdx, T>::value...> >::value>
          >
          : TrueType
        {};
      }

      template<class T>
      struct IsRuntimeEqual<T, std::enable_if_t<IsBasicRuntimeEqual<T>::value> >
        : TrueType
      {};

      template<class T>
      struct IsRuntimeEqual<
        T,
        std::enable_if_t<(!IsBasicRuntimeEqual<T>::value
                          && IsExpression<T>::value
                          && !IsDynamicOperation<Operation<T> >::value
                          && HasRTEqualOperands<T, MakeIndexSequence<Arity<T>::value> >::value
        )> >
        : TrueType
      {};

      /**FalseType by default.*/
      template<class T1, class T2, class SFINAE = void>
      struct AreRuntimeEqual
        : std::is_same<DecayExpression<T1>, DecayExpression<T2> >
      {};

      /**Decide whether to consider object of the given types as equal
       * at runtime, although their actual value may not be constant.
       */
      template<class T1, class T2>
      struct AreRuntimeEqual<
        T1, T2,
        std::enable_if_t<(!IsSubExpressionExpression<T1>::value
                          && !IsSubExpressionExpression<T2>::value
                          && (!IsRuntimeEqual<T2>::value || !IsRuntimeEqual<T1>::value)
        )> >
        : FalseType
      {};

      /**SubExpressionOperation expressions qualify for IsRuntimeEqual
       * if the contained sub-expression qualifies.
       */
      template<class T>
      struct IsBasicRuntimeEqual<
        T,
        std::enable_if_t<IsSubExpressionExpression<T>::value> >
        : IsRuntimeEqual<Operand<1, T> >
      {};

      /**FalseType by default.*/
      template<class T1, class T2>
      struct AreRuntimeEqual<
        T1, T2,
        std::enable_if_t<(IsSubExpressionExpression<T1>::value
                          &&
                          IsSubExpressionExpression<T2>::value)> >
        : AreRuntimeEqual<Operand<1, T1>, Operand<1, T2> >
      {};

      template<class T1, class T2>
      struct AreRuntimeEqual<
        T1, T2,
        std::enable_if_t<(IsSubExpressionExpression<T1>::value
                          &&
                          !IsSubExpressionExpression<T2>::value)> >
        : AreRuntimeEqual<Operand<1, T1>, T2>
      {};

      template<class T1, class T2>
      struct AreRuntimeEqual<
        T1, T2,
        std::enable_if_t<(!IsSubExpressionExpression<T1>::value
                          &&
                          IsSubExpressionExpression<T2>::value)> >
        : AreRuntimeEqual<T1, Operand<1, T2> >
      {};

      //! @} ExpressionTraits

      //! @} ExpressionTemplates

    } // Expressions

    /**@copydoc Expressions::IsRuntimeEqual.*/
    template<class T>
    using IsRuntimeEqualExpression = Expressions::IsRuntimeEqual<T>;

    /**@copydoc Expressions::AreRuntimeEqual.*/
    template<class T1, class T2>
    using AreRuntimeSameExpressions = Expressions::AreRuntimeEqual<T1, T2>;

    template<class T>
    constexpr bool isRuntimeEqualExpression(T&& = T{})
    {
      return IsRuntimeEqualExpression<T>::value;
    }

    template<class T1, class T2>
    constexpr bool areRuntimeEqualExpressions(T1&& = T1{}, T2&& = T2{})
    {
      return AreRuntimeSameExpressions<T1, T2>::value;
    }

  } // ACFem

} // Dune

#endif // __DUNE_ACFEM_EXPRESSIONS_RUNTIMEEQUAL_HH__
