#ifndef __DUNE_ACFEM_EXPRESSIONS_DYNAMICSTORAGE_HH__
#define __DUNE_ACFEM_EXPRESSIONS_DYNAMICSTORAGE_HH__

#include "expressiontraits.hh"
#include "storage.hh"
#include "optimizationbase.hh"

namespace Dune
{

  namespace ACFem
  {

    namespace Expressions
    {

      /**@addtogroup ExpressionTemplates
       *
       * @{
       *
       */

      /**@addtogroup ExpressionInterface
       *
       * @{
       */

      /**A traits class which provides a type suitable to store the
       * values of the given expression T and any expressions of
       * "similar" type. This is meant to be dense (floating point)
       * storage. Expression implementation should specialize this
       * class in order to give abstract access to a type which can
       * hold the values of any expression.
       */
      template<class T, class SFINAE = void>
      struct DenseStorage;

      template<class T>
      struct DenseStorage<T, std::enable_if_t<IsScalar<T>::value> >
      {
        using Type = FloatingPointClosure<T>;
      };

      template<class T>
      using DenseStorageType = typename DenseStorage<std::decay_t<T> >::Type;

      template<class T, class Copy = FalseType>
      constexpr auto denseStorage(T&& t, Copy = Copy{})
      {
        return DenseStorageType<T>{};
      }

      template<class T>
      constexpr auto denseStorage(T&& t, TrueType)
      {
        return DenseStorageType<T>(std::forward<T>(t));
      }

      template<bool copy, class T>
      constexpr auto denseStorage(T&& t)
      {
        return denseStorage(std::forward<T>(t), BoolConstant<copy>{});
      }

      //! @} ExpressionInterface

      //! @} ExpressionTemplates

    } // Expressions

  } // ACFem

} // Dune

#endif // __DUNE_ACFEM_EXPRESSIONS_DENSESTORAGE_HH__
