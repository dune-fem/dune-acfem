#ifndef __DUNE_ACFEM_EXPRESSIONS_OPTIMIZATIONPROFILE_HH__
#define __DUNE_ACFEM_EXPRESSIONS_OPTIMIZATIONPROFILE_HH__

#include <iostream>
#include <fstream>
#include <map>

#include "../common/tostring.hh"

namespace Dune {

  namespace ACFem {

    namespace Expressions {

#ifndef DUNE_ACFEM_PROFILE_OPTIMIZATION
# define DUNE_ACFEM_RECORD_OPTIMIZATION /**/
# define DUNE_ACFEM_EXPRESSION_RESULT(EXPR, ...) return EXPR
#else
# define DUNE_ACFEM_RECORD_OPTIMIZATION                                 \
      Dune::ACFem::Expressions::OptimizationProfiler::recordHit(__LINE__, __FILE__, __PRETTY_FUNCTION__)

#if DUNE_ACFEM_TRACE_OPTIMIZATION
# define DUNE_ACFEM_EXPRESSION_RESULT(EXPR, ...)        \
{                                                       \
  decltype(auto) result = EXPR;                         \
  std::clog << "Expression: "                           \
            << toString(result)                         \
            << " ["                                     \
            << std::string(__VA_ARGS__)                 \
            << "]"                                      \
            << std::endl;                               \
  return forwardReturnValue<decltype(result)>(result);  \
}
#else
# define DUNE_ACFEM_EXPRESSION_RESULT(EXPR, ...) return EXPR
#endif

      class OptimizationProfiler
      {
        using KeyType = std::pair<std::size_t, std::string>;
        using DataType = std::pair<std::size_t, std::string>;
        using StorageType = std::map<KeyType, DataType>;

       public:
        static void recordHit(std::size_t line, const std::string& file, const std::string& prettyFunction)
        {
          static OptimizationProfilerDumper dumper;
          (void)dumper;

          auto key = std::make_pair(line, file);
          const auto& old = data_.find(key);
          const auto& fct = old == data_.end() ? parsePrettyFunction(prettyFunction) : old->second.second;
          data_[key] = std::make_pair(++(data_[key].first), fct);
#if DUNE_ACFEM_TRACE_OPTIMIZATION
          std::clog << file << ":" << line << std::endl
                    << prettyFunction << std::endl
                    << std::endl;
#endif
        }

        static void clear()
        {
          data_.clear();
        }

        static void dump(std::ostream& out)
        {
          out << "COUNT LINE File FUNCTION" << std::endl;
          for (auto&& kv : data_) {
            const auto& key = kv.first;
            const auto& data = kv.second;

            out << data.first << " "
                << key.first << " "
                << "\"" << key.second << "\" "
                << "\"" << parsePrettyFunction(data.second) << "\""
                << std::endl;
          }
        }

        static void read(std::istream& in)
        {
          std::string rest;
          std::string file;
          std::string fct;
          std::size_t count;
          std::size_t line;
          if (getline(in, rest)) {
            while (getline(in, rest)) {
              std::istringstream iss(rest);
              if (iss >> count >> line) {
                rest = rest.substr(rest.find("\"")+1);
                auto posQuote = rest.find("\"");
                file = rest.substr(0, posQuote);
                rest = rest.substr(posQuote+3); // " "
                posQuote = rest.find("\"");
                fct = rest.substr(0, posQuote);

                auto key = std::make_pair(line, file);
                const auto& old = data_.find(key);
                if (old != data_.end()) {
                  if (old->second.second == fct) {
                    count += old->second.first;
                  }
                }
                data_[key] = std::make_pair(count, fct);
              }
            }
          }
        }

        static std::string parsePrettyFunction(const std::string& prettyFunction)
        {
          std::vector<std::string> tokens;
#if DUNE_ACFEM_IS_GCC(0, 9999)
          auto fctEnd = prettyFunction.find(" [with ");
#elif DUNE_ACFEM_IS_CLANG(0, 9999)
          auto fctEnd = prettyFunction.find(" [");
#endif
          auto fct = prettyFunction.substr(0, fctEnd);
          auto rest = prettyFunction.substr(fctEnd+sizeof(" [with ")-1);
          //std::clog << "rest: " << rest << std::endl;
          auto pos = std::string::npos;
          while ((pos = rest.find(";")) != std::string::npos) {
            tokens.push_back(rest.substr(0, pos-1));
            rest = rest.substr(pos+2);
          }
          tokens.push_back(rest.substr(0, rest.size()-1));
          for(auto&& token : tokens) {
            //std::clog << "token: \"" << token << "\"" << std::endl;
            if (token.find("typename std::enable_if") == 0) {
              const auto pfxLen = sizeof("typename");
              fct += "; " + token.substr(pfxLen, token.rfind("::type ")-pfxLen);
            }
          }

          return fct;
        }

       private:

        struct OptimizationProfilerDumper
        {
          ~OptimizationProfilerDumper()
          {
            std::string dumpFileName = "acfem-expression-profile.dump";
            std::ifstream oldDumpFile;
            oldDumpFile.open(dumpFileName);
            if (oldDumpFile.is_open()) {
              OptimizationProfiler::read(oldDumpFile);
              oldDumpFile.close();
            }
            std::ofstream dumpFile;
            dumpFile.open(dumpFileName);
            if (dumpFile.is_open()) {
              OptimizationProfiler::dump(dumpFile);
              dumpFile.close();
            }
          }
        };

        inline static StorageType data_;
      };

#endif

    } // Expressions::

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_EXPRESSIONS_OPTIMIZATIONPROFILE_HH__
