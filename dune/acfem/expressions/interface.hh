#ifndef __DUNE_ACFEM_EXPRESSIONS_INTERFACE_HH__
#define __DUNE_ACFEM_EXPRESSIONS_INTERFACE_HH__

#include "../common/literals.hh"
#include "../common/typetraits.hh"
#include "expressiontraits.hh"
#include "storage.hh"
#include "optimizationbase.hh"

namespace Dune
{

  namespace ACFem
  {

    namespace Expressions
    {

      /**@addtogroup ExpressionTemplates
       *
       * @{
       *
       */

      /**@addtogroup ExpressionInterface
       *
       * Helper constructs in order to inject non-expressions into the
       * expression chain and to interface generated expressions to
       * pre-existing non-expressions. One example is the conversion of
       * a Dune::FieldMatrix into a tensor and wrapping a generated
       * tensor expressions into a class which can be converted back to
       * a Dune::FieldMatrix.
       *
       * @{
       */

      /**An expression closure is a wrapper class which wraps an
       * expression in order to interface to other existing
       * interface. See Tensor::TensorResult and
       * FunctionExpressions::BindableClosure.
       *
       * In order to generate a new closure an implementation must
       * define the wrapper class and speicialize this tratis-class for
       * it and specialize the global template function
       * expressionClosure().
       */
      template<class T, class SFINAE = void>
      struct IsClosure
        : FalseType
      {};

      /**Forward references to the real traits class.*/
      template<class T>
      struct IsClosure<T, std::enable_if_t<!IsDecay<T>::value> >
        : IsClosure<std::decay_t<T> >
      {};

      /**Provide the type ExpressionType for non-closure expressions.*/
      template<class T, class SFINAE = void>
      struct ClosureTraits
      {
        using ExpressionType = T;
      };

      /**Provide the type ExpressionType for closure expressions.*/
      template<class T>
      struct ClosureTraits<T, std::enable_if_t<IsClosure<T>::value> >
      {
        using ExpressionType = Operand<0, T>;
      };

      /**Type alias shortcut to get hold of the type contained in an
       * "expression closure". Returns T itself if T is not a closure
       * type.
       */
      template<class T>
      using EnclosedType = typename ClosureTraits<T>::ExpressionType;

      template<class T>
      using AsExpression = RemoveRValueReferenceType<decltype(asExpression(std::declval<T>()))>;

      /**Do-nothing default implementation for pathologic cases
       *
       * * T is already a closure expression
       * * T is a scalar which is not an expression
       */
      template<
        class T,
        std::enable_if_t<(Expressions::IsClosure<T>::value
                          || (!IsExpression<T>::value
                              && IsScalar<T>::value)
        ), int> = 0>
      constexpr decltype(auto) expressionClosure(T&& t)
      {
        return std::forward<T>(t);
      }

      struct Closure
      {
        template<class T>
        constexpr decltype(auto) operator()(T&& t) const
        {
          return expressionClosure(std::forward<T>(t));
        }
      };

      struct Disclosure
      {
        template<class T>
        constexpr decltype(auto) operator()(T&& t) const
        {
          return std::forward<T>(t);
        }
      };

      /**Return a non-closure expression as is.*/
      template<
        class T,
        std::enable_if_t<(IsExpression<T>::value
                          && !IsClosure<T>::value
        ), int> = 0>
      constexpr decltype(auto) asExpression(T&& t)
      {
        return std::forward<T>(t);
      }

      /**Return the enclosed expression if a T is an expression closure.*/
      template<
        class T,
        std::enable_if_t<(IsExpression<T>::value
                          && IsClosure<T>::value
        ), int> = 0>
      constexpr decltype(auto) asExpression(T&& t)
      {
        using namespace Literals;
        danglingReferenceCheck<0>(std::forward<T>(t));
        return std::forward<T>(t).operand(0_c);
      }

      template<class T>
      using IsPromotedTopLevel = BoolConstant<IsClosure<T>::value || IsTypedValueReference<T>::value || IsIntegralConstant<T>::value>;

      /**The purpose of this function is to promote operands to other
       * types, e.g. to promote scalars to vectorial expressions
       * before feeding them into some arithmetic operation. This is
       * used in operandpromotion.hh to define operator function
       * overrides which first to the promotion and then call the
       * operator function for the promoted operands. This function
       * must not instantiate if an operator already exists wihtout
       * type promotion.
       */
      template<std::size_t N, class... T,
               std::enable_if_t<(NthIs<N, IsClosure, T...>::value && !AnyIs<IsIntegralConstant, T...>::value), int> = 0>
      constexpr decltype(auto) operandPromotion(T&&... t)
      {
        return asExpression(get<N>(std::forward_as_tuple(std::forward<T>(t)...)));
      }

      /**Promote typed values to non-reference types.*/
      template<std::size_t N, class... T,
               std::enable_if_t<(!NthIs<N, IsClosure, T...>::value
                                 && NthIs<N, IsTypedValueReference, T...>::value
                                 && !AnyIs<IsIntegralConstant, T...>::value
        ), int> = 0>
      constexpr auto operandPromotion(T&&... t)
      {
        return std::decay_t<TupleElement<N, std::tuple<T...> > >(get<N>(std::forward_as_tuple(std::forward<T>(t)...)));
      }

      template<std::size_t N, class... T,
               std::enable_if_t<(AnyIs<IsPromotedTopLevel, T...>::value
                                 && !NthIs<N, IsPromotedTopLevel, T...>::value
       ), int> = 0>
      constexpr decltype(auto) operandPromotion(T&&... t)
      {
        return std::forward<TupleElement<N, std::tuple<T...> > >(get<N>(std::forward_as_tuple(std::forward<T>(t)...)));
      }

      //! @} ExpressionInterface

      //! @} ExpressionTemplates

    } // Expressions

    using Expressions::asExpression;

  } // ACFem

} // Dune

#endif // __DUNE_ACFEM_EXPRESSIONS_INTERFACE_HH__
