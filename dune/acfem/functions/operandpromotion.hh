#ifndef __DUNE_ACFEM_FUNCTIONS_OPERANDPROMOTION_HH__
#define __DUNE_ACFEM_FUNCTIONS_OPERANDPROMOTION_HH__

#include "../common/literals.hh"
#include "../mpl/compare.hh"
#include "../expressions/interface.hh"
#include "../tensors/tensorbase.hh"

#include "policy.hh"
#include "localfunctiontraits.hh"
#include "functiontraits.hh"
#include "placeholders/localfunctionplaceholder.hh"

namespace Dune
{
  namespace ACFem
  {
    namespace GridFunction
    {
      using namespace Literals;
      using Expressions::IsClosure;
      using Expressions::IsPromotedTopLevel;

      /**@internal Allow promotion of any tensor-like operands if any
       * of the other operands already is a BindableTensorFunction.
       */
      template<class... T>
      struct PromoteTensorOperands
        : BoolConstant<(// Handled top-level
                        !AnyIs<IsPromotedTopLevel, T...>::value
                        // Don't if all arguments are allready tensor functions
                        && !AllAre<IsTensorFunction, T...>::value
                        // One is already a function
                        && AnyIs<IsTensorFunction, T...>::value
                        // All are admissible in principle
                        && AllAre<OrPredicate<
                             IsTensorOperand, IsTensorFunction>::template Predicate, T...>::value
        )>
      {};

      /**@internal Only instantiate if any of the operands is allowed
       * to and can be promoted and in this case pass function just
       * on.
       */
      template<std::size_t N, class... T,
               std::enable_if_t<(PromoteTensorOperands<T...>::value
                                 && NthIs<N, IsTensorFunction, T...>::value
                 ), int> = 0>
      constexpr decltype(auto) operandPromotion(T&&... t)
      {
        return std::forward<TypePackElement<N, T...> >(
          get<N>(std::forward_as_tuple(std::forward<T>(t)...))
          );
      }

      /**@internal Only instantiate if any of the operands is allowed
       * to and can be promoted and in this case promote anything
       * which can be converted into a tensor into
       * BindableTensorFunction.
       */
      template<std::size_t N, class... T,
               std::enable_if_t<(PromoteTensorOperands<T...>::value
                                 && !NthIs<N, IsTensorFunction, T...>::value
                 ), int> = 0>
      constexpr decltype(auto) operandPromotion(T&&... t)
      {
        const auto& fct = get<IndexOfMatching<IsTensorFunction, T...>::value>(std::forward_as_tuple(std::forward<T>(t)...));

        return gridFunction(
          fct.gridPart(),
          tensor(std::forward<TypePackElement<N, T...> >(
                   get<N>(std::forward_as_tuple(std::forward<T>(t)...))
                   )),
          fct.autoDiffOrder(),
          fct.indeterminateId()
          );
      }

      /////////////////////////////////////////////////////////////////////////

      /**@internal Allow promotion of any operand which can be stuffed
       * into a Fem::ConstLocalFunction provided any of the other
       * operands already is a BindableTensorFunction.
       */
      template<class... T>
      struct PromoteFunctionOperands
        : BoolConstant<(// Don't if all arguments are allready tensor functions
                        !AllAre<IsTensorFunction, T...>::value
                        // One is already a function
                        && AnyIs<IsTensorFunction, T...>::value
                        // Any is admissible
                        && AllAre<OrPredicate<
                             IsConstLocalFunctionOperand, IsTensorFunction>::template Predicate, T...>::value
        )>
      {};

      /**@internal Only instantiate if any of the operands is allowed
       * to and can be promoted and in this case pass function just
       * on.
       */
      template<std::size_t N, class... T,
               std::enable_if_t<(PromoteFunctionOperands<T...>::value
                                 && NthIs<N, IsTensorFunction, T...>::value
                 ), int> = 0>
      constexpr decltype(auto) operandPromotion(T&&... t)
      {
        return std::forward<TupleElement<N, std::tuple<T...> > >(
          get<N>(std::forward_as_tuple(std::forward<T>(t)...))
          );
      }

      /**@internal Only instantiate if any of the operands is allowed
       * to and can be promoted and in this case promote anything
       * which can be converted into a LocalFunctionPlaceholder into a
       * BindableTensorFunction.
       */
      template<std::size_t N, class... T,
               std::enable_if_t<(PromoteFunctionOperands<T...>::value
                                 && !NthIs<N, IsTensorFunction, T...>::value
                 ), int> = 0>
      constexpr decltype(auto) operandPromotion(T&&... t)
      {
        const auto& fct = get<IndexOfMatching<IsTensorFunction, T...>::value>(std::forward_as_tuple(std::forward<T>(t)...));

        return gridFunction(
          fct.gridPart(),
          localFunctionPlaceholder(
            std::forward<TupleElement<N, std::tuple<T...> > >(
              get<N>(std::forward_as_tuple(std::forward<T>(t)...))
              )),
          fct.autoDiffOrder(),
          fct.indeterminateId()
          );
      }

      /////////////////////////////////////////////////////////////////////////

      /**@internal Allow promotion of any operand which can be stuffed
       * into a Fem::ConstLocalFunction. Allow any tensor operand to
       * be combined. In this case the indeterminate id cannot be
       * determined, so we use the default 0.
       */
      template<std::size_t N, class... T>
      struct PromoteAllFunctionOperands
        : BoolConstant<(// Handled top-level
                        !AnyIs<IsClosure, T...>::value
                        // None is already a tensor function (handled elsewhere)
                        && !AnyIs<IsTensorFunction, T...>::value
                        // One is a Fem::ConstLocalFunction or similar
                        && AnyIs<IsConstLocalFunctionOperand, T...>::value
                        // All are admissible as tensors in principle or are Fem::LocalFunction's
                        && AllAre<OrPredicate<
                             IsTensorOperand, IsConstLocalFunctionOperand>::template Predicate, T...>::value
        )>
      {};

      /**@internal Promote local functions to BindableTensorFunction
       * if combined with each other or with tensors.
       */
      template<std::size_t N, class... T,
               std::enable_if_t<(PromoteAllFunctionOperands<N, T...>::value
                                 && NthIs<N, IsConstLocalFunctionOperand, T...>::value
                 ), int> = 0>
      constexpr decltype(auto) operandPromotion(T&&... t)
      {
        return gridFunction(
          localFunctionPlaceholder(
            std::forward<TupleElement<N, std::tuple<T...> > >(
              get<N>(std::forward_as_tuple(std::forward<T>(t)...))
              )),
          Policy::IndeterminateId{}
          );
      }

      /**@internal Promote tensors to BindableTensorFunction's when
       * combined with Fem::LocalFunction. The indeterminate id is 0
       * in this case as it cannot be determined (well, unless the
       * tensor contains an indeterminate ...)
       */
      template<std::size_t N, class... T,
               std::enable_if_t<(PromoteAllFunctionOperands<N, T...>::value
                                 && !NthIs<N, IsConstLocalFunctionOperand, T...>::value
                 ), int> = 0>
      constexpr decltype(auto) operandPromotion(T&&... t)
      {
        const auto& fct = get<IndexOfMatching<IsConstLocalFunctionOperand, T...>::value>(std::forward_as_tuple(std::forward<T>(t)...));

        return gridFunction(
          fct.gridPart(),
          tensor(std::forward<TupleElement<N, std::tuple<T...> > >(
                   get<N>(std::forward_as_tuple(std::forward<T>(t)...))
                   )),
          Policy::IndeterminateId{}
          );
      }

      template<
        class T,
        std::enable_if_t<(sizeof(derivative(operandPromotion<0>(std::declval<T>()))) >= 0), int> = 0>
      constexpr decltype(auto) derivative(T&& t)
      {
        return derivative(operandPromotion<0>(std::forward<T>(t)));
      }

      template<
        std::size_t N, class T,
        std::enable_if_t<(sizeof(derivative<N>(operandPromotion<0>(std::declval<T>()))) >= 0), int> = 0>
      constexpr decltype(auto) derivative(T&& t)
      {
        return derivative<N>(operandPromotion<0>(std::forward<T>(t)));
      }

      template<
        class T,
        std::enable_if_t<(sizeof(ternary(operandPromotion<0>(std::declval<T>()))) >= 0), int> = 0>
      constexpr decltype(auto) ternary(T&& t)
      {
        return ternary(operandPromotion<0>(std::forward<T>(t)));
      }

    } // NS GridFunction

    using GridFunction::derivative;

  } // NS ACFem

  namespace Fem {

    using ACFem::GridFunction::operandPromotion;

  }

} // NS Dune

#endif //  __DUNE_ACFEM_FUNCTIONS_OPERANDPROMOTION_HH__
