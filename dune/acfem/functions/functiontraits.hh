#ifndef __DUNE_ACFEM_FUNCTIONS_FUNCTIONTRAITS_HH__
#define __DUNE_ACFEM_FUNCTIONS_FUNCTIONTRAITS_HH__

#include "../common/types.hh"
#include "../expressions/find.hh"
#include "../expressions/treeoperand.hh"
#include "../expressions/extract.hh"
#include "../tensors/expressions.hh"

#include "localfunctiontraits.hh"

namespace Dune {

  namespace ACFem {

    namespace GridFunction {

      using Expressions::TreeFind;

      // forward
      template<class Expr, class GridPart, std::size_t AutoDiffOrder, std::size_t IndeterminateId>
      class BindableTensorFunction;

      template<class T>
      constexpr std::size_t autoDiffOrder()
      {
        return decltype(std::decay_t<T>::autoDiffOrder())::value;
      }

      /**@internal Default to FalseType.*/
      template<class T>
      struct IsTensorFunction
        : FalseType
      {};

      /**@internal Forward to decay_t<>.*/
      template<class T>
      struct IsTensorFunction<T&>
        : IsTensorFunction<std::decay_t<T>>
      {};

      /**@internal Forward to decay_t<>.*/
      template<class T>
      struct IsTensorFunction<T&&>
        : IsTensorFunction<std::decay_t<T>>
      {};

      /**Identify a BindableTensorFunction.*/
      template<class Expr, class GridPart, std::size_t AutoDiffOrder, std::size_t IndeterminateId>
      struct IsTensorFunction<BindableTensorFunction<Expr, GridPart, AutoDiffOrder, IndeterminateId>>
        : TrueType
      {};

      template<class T, class SFINAE = void>
      struct RangeTensorTraits
        : TensorTraits<void>
      {};

      template<class T>
      struct RangeTensorTraits<T, std::enable_if_t<!IsDecay<T>::value> >
        : RangeTensorTraits<std::decay_t<T> >
      {};

      template<class Expr, class GridPart, std::size_t AutoDiffOrder, std::size_t IndeterminateId>
      struct RangeTensorTraits<BindableTensorFunction<Expr, GridPart, AutoDiffOrder, IndeterminateId> >
        : TensorTraits<Expr>
      {};

      template<class T>
      struct RangeTensorTraits<
        T,
        std::enable_if_t<(IsDecay<T>::value
                          && IsWrappableByConstLocalFunction<T>::value
                          && !IsTensorFunction<T>::value
                          && T::FunctionSpaceType::dimRange == 1
        )> >
        : TensorTraits<typename T::FunctionSpaceType::RangeFieldType>
      {};

      template<class T>
      struct RangeTensorTraits<
        T,
        std::enable_if_t<(IsDecay<T>::value
                          && IsWrappableByConstLocalFunction<T>::value
                          && !IsTensorFunction<T>::value
                          && T::FunctionSpaceType::dimRange > 1
        )> >
        : TensorTraits<typename T::RangeType>
      {};

      template<class T>
      using RangeTensorType = typename RangeTensorTraits<T>::TensorType;

      //!@internal default FalseType
      template<class T, std::size_t AtLeastRequestedOrder = 0UL, std::size_t AtMostRequestedOrder = ~0UL>
      struct HasRegularity
        : IsTensorFunction<T>
      {};

      template<class T, std::size_t AtLeastRequestedOrder, std::size_t AtMostRequestedOrder>
      struct HasRegularity<T&, AtLeastRequestedOrder, AtMostRequestedOrder>
        : HasRegularity<std::decay_t<T>, AtLeastRequestedOrder, AtMostRequestedOrder>
      {};

      template<class T, std::size_t AtLeastRequestedOrder, std::size_t AtMostRequestedOrder>
      struct HasRegularity<T&&, AtLeastRequestedOrder, AtMostRequestedOrder>
        : HasRegularity<std::decay_t<T>, AtLeastRequestedOrder, AtMostRequestedOrder>
      {};

      /**Evaluate to TrueType if the given BindableTensorFunction
       * fufils the regularity requirements given by the "box
       * constaints" @a AtLeastRequestedOrder and @a
       * AtMostRequestedOrder.
       */
      template<class Expr, class GridPart, std::size_t AutoDiffOrder, std::size_t IndeterminateId,
               std::size_t AtLeastRequestedOrder, std::size_t AtMostRequestedOrder>
      struct HasRegularity<BindableTensorFunction<Expr, GridPart, AutoDiffOrder, IndeterminateId>,
                           AtLeastRequestedOrder, AtMostRequestedOrder>
        : BoolConstant<(AutoDiffOrder >= AtLeastRequestedOrder && AutoDiffOrder <= AtMostRequestedOrder)>
      {};

      /**@internal Default FalseType.*/
      template<class T, class SFINAE = void>
      struct HasGridPartMethod
        : FalseType
      {};

      /**@c TrueType if a T has a gridPart() method.*/
      template<class T>
      struct HasGridPartMethod<T, VoidType<decltype(std::declval<T>().gridPart())> >
        : TrueType
      {};

      /**@internal Default FalseType.*/
      template<class T, class SFINAE = void>
      struct HasRangeType
        : FalseType
      {};

      /**@c TrueType if T provides a RangeType.*/
      template<class T>
      struct HasRangeType<T, VoidType<typename std::decay_t<T>::RangeType> >
        : TrueType
      {};

      template<class T, class SFINAE = void>
      struct IsGridPartProvider
        : FalseType
      {};

      template<class T>
      struct IsGridPartProvider<T, std::enable_if_t<HasGridPartMethod<T>::value> >
        : TrueType
      {};

      template<class T>
      struct IsGridPartProvider<
        T,
        std::enable_if_t<(!HasGridPartMethod<T>::value
                          && !std::is_same<FalseType, TreeFind<HasGridPartMethod, T> >::value
          )> >
        : TrueType
      {};

      template<class T, std::enable_if_t<HasGridPartMethod<T>::value, int> = 0>
      const auto& gridPart(T&& t)
      {
        return t.gridPart();
      }

      template<class T, std::enable_if_t<!HasGridPartMethod<T>::value && IsGridPartProvider<T>::value, int> = 0>
      const auto& gridPart(T&& t)
      {
        using TreePos = typename TreeFind<HasGridPartMethod, T>::Second;
        return treeOperand(std::forward<T>(t), TreePos{}).gridPart();
      }

      template<class T, class SFINAE = void>
      struct HasSkeletonSupport
        : FalseType
      {};

      template<class T>
      struct HasSkeletonSupport<
        T, VoidType<decltype(std::declval<T>().classifyIntersection(FalseType{}))> >
        : TrueType
      {};

      template<class T>
      constexpr bool hasSkeletonSupport()
      {
        return HasSkeletonSupport<T>::value;
      }

      template<class T>
      constexpr bool hasSkeletonSupport(T&&)
      {
        return HasSkeletonSupport<T>::value;
      }

      /**@internal BindableTensorFunction does not need a closure.*/
      template<class T, std::enable_if_t<(IsTensorFunction<T>::value && !Expressions::IsClosure<T>::value), int> = 0>
      constexpr decltype(auto) expressionClosure(T&& t)
      {
        return std::forward<T>(t);
      }

    } // NS GridFunction

    using GridFunction::IsTensorFunction;

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_FUNCTIONS_FUNCTIONTRAITS_HH__
