#include <config.h>

#include <libgen.h>
#include <stdexcept>

#include "../../common/scopedredirect.hh"

/**@addtogroup GridFunctions
 * @{
 */

//!@} GridFunctions

#include <iostream>
#include <sstream>

#include <dune/common/exceptions.hh>
#include <dune/common/timer.hh>
#include <dune/fem/io/parameter.hh>

// Helper to get to the reference element with reasonable effort
#include <dune/fem/space/common/allgeomtypes.hh>

#include <dune/fem/gridpart/adaptiveleafgridpart.hh>
#include <dune/fem/space/common/interpolate.hh>

//include norms
#include <dune/fem/misc/l1norm.hh>
#include <dune/fem/misc/l2norm.hh>
#include <dune/fem/misc/lpnorm.hh>
#include <dune/fem/misc/h1norm.hh>
#include <dune/fem/misc/domainintegral.hh>

// some quadrature stuff for evaluateQuadrature
#include "../../common/quadrature.hh"

#include "../../common/geometry.hh"

// Simplification for discrete functions
#include "../../common/discretefunctionselector.hh"

// All includes for grid-functions.
#include "../functions.hh"

#ifdef STRING
# undef STRING
#endif

#define STRING__(A) #A
#define STRING(A) STRING__(A)
#define MODULENAME STRING(MODULE)

#if 0
// Defines are set by CMakeLists.txt
// timings are ca.
# define DEFAULT 1 // 80 s
# define DISCRETE_FUNCTION 0 // 40 s
# define COS 0 // 60 s
# define GAUSSIAN 0 // 90 s
# define COS_PRODUCT 0 // 200 s
# define BASIC_OPERATIONS 0 // 40 s
# define MATH_FUNCTIONS 0 // 40 s
#endif

/**@addtogroup GridFunctions
 * @{
 */

/**@addtogroup GridFunctionTests
 * @{
 */

using namespace Dune::ACFem;
using namespace Dune::ACFem::Literals;

template<class GridPart, class Field = double>
class NormalizeFunction
  : public Dune::Fem::BindableGridFunction<GridPart, Dune::FieldVector<Field, GridPart::dimensionworld> >
{
  using BaseType = Dune::Fem::BindableGridFunction<GridPart, Dune::FieldVector<Field, GridPart::dimensionworld> >;
 public:
#if DUNE_ACFEM_IS_CLANG(0,99)
  using Dune::Fem::BindableGridFunction<GridPart, Dune::FieldVector<Field, GridPart::dimensionworld> >::BindableGridFunction;
#else
  using BaseType::BindableGridFunction;
#endif

  template<class Point>
  void evaluate(const Point& x_, typename BaseType::RangeType& result) const
  {
    result = BaseType::global(x_);
    result /= result.two_norm();
  }

  unsigned int order() const
  {
    return 2;
  }

  std::string name() const
  {
    return "blub";
  }

};

// cos-product, the RHS of some example poisson problem
template<class GridPart>
class MyCosProduct
  : public Dune::Fem::BindableGridFunction<GridPart, Dune::FieldVector<double, 1> >
{
  using BaseType = Dune::Fem::BindableGridFunction<GridPart, Dune::FieldVector<double, 1> >;
 public:
#if DUNE_ACFEM_IS_CLANG(0, 99)
  // error: dependent using declaration resolved to type without 'typename' using BaseType::BindableGridFunction;
  using Dune::Fem::BindableGridFunction<GridPart, Dune::FieldVector<double, 1> >::BindableGridFunction;
#else
  using BaseType::BindableGridFunction;
#endif

  template<class Point>
  void evaluate(const Point& x_, typename BaseType::RangeType& result) const
  {
    auto x = BaseType::global(x_);
    result = 1.;
    for (int i = 0; i < BaseType::FunctionSpaceType::dimDomain; ++i) {
      result *= cos(2*M_PI*x[i]);
    }
    result *= (8*M_PI*M_PI+1);
  }
  unsigned int order() const
  {
    return 2;
  }
  std::string name() const
  {
    return "blub";
  }
};

template<class F1, class F2>
auto sameValue(F1&& f1, F2&& f2)
{
  auto lf1 = constLocalFunction(f1);
  auto lf2 = constLocalFunction(f2);

  for (const auto& entity : elements(f1.gridPart())) {

    const auto& geometry = entity.geometry();
    volatile auto center = geometry.local(geometry.center());
    using LocalDomainType = std::decay_t<decltype(center)>;

    lf1.bind(entity);
    lf2.bind(entity);

    auto diff =
      lf1.evaluate(const_cast<const LocalDomainType&>(center))
      -
      lf1.evaluate(const_cast<const LocalDomainType&>(center));

    if (diff.two_norm2() > 1e-12) {
      throw std::runtime_error("functions have differing values.");
    }

    lf2.unbind();
    lf1.unbind();
  };
};

template<class Function>
auto timingTest(Function&& f, std::size_t count)
{
  auto lf = constLocalFunction(f);

  for (const auto& entity : elements(f.gridPart())) {

    const auto& geometry = entity.geometry();
    volatile auto center = geometry.local(geometry.center());
    using LocalDomainType = std::decay_t<decltype(center)>;

    lf.bind(entity);

    Dune::Timer timer;
    for (unsigned i = 0; i < count; ++i) {
      volatile auto result = lf.evaluate(const_cast<const LocalDomainType&>(center));
      (void)result;
    }
    auto elapsed = timer.elapsed();
    timer.stop();

    lf.unbind();

    return elapsed;
  };

  return 0.;
};

namespace Dune {
  template<class T, int N>
  bool operator<(const Dune::FieldVector<T, N>& a, const Dune::FieldVector<T, N>& b)
  {
    for(unsigned int i = 0; i < a.size(); ++i)
    {
      if(std::abs(a[i]-b[i]) > 1e-8)
      {
        return a[i] < b[i];
      }
    }
    return a.two_norm2() < b.two_norm2();
  }
}

/**Test routine which is called with the GridFunctionExpression from
 * the main-program. We simply walk over the grid and evaluate the
 * local function and its jacobian on each element.
 */
template<class GridFunction>
void gridWalkTest(GridFunction&& f)
{
  std::clog << "********** BIG FAT NOTE: BULK ITERATION **********"
            << std::endl
            << std::endl;

  std::clog << "Function: "
            << f.name()
            << std::endl
            << std::endl;

  using GridFunctionType = std::decay_t<GridFunction>;

  // Wow. This is really complicated, what a heck-meck just to get
  // hold of the reference element.
  typedef typename GridFunctionType::GridPartType GridPartType;
  typedef Dune::Fem::CachingLumpingQuadrature<GridPartType, 0> QuadratureType;

  typedef typename GridFunctionType::DomainType DomainType;
  typedef typename GridFunctionType::RangeType RangeType;
  typedef typename GridFunctionType::JacobianRangeType JacobianRangeType;
  typedef typename GridFunctionType::HessianRangeType HessianRangeType;

  const GeometryInformation<GridPartType> geometryInformation;

  std::map<DomainType, typename GridFunctionType::EntityType> leafElements;

  for (const auto& entity : elements(f.gridPart())) {
    leafElements[entity.geometry().center()] = entity;
  }

  auto flocal = constLocalFunction(f);

  for (auto mapEl : leafElements) {
    const auto& entity = mapEl.second;

    flocal.bind(entity);

    // Local "center", whatever that might be
    const auto& localCenter(geometryInformation.localCenter(entity.geometry().type()));

    // This cannot be handled with "auto" as these are no return type.
    RangeType value;
    JacobianRangeType jacobian;
    HessianRangeType hessian;

    // Evaluate the local function at the barycenter and print some noise

    std::clog << "Local   : " << localCenter << std::endl;
    std::clog << "Global  : " << entity.geometry().center() << std::endl;

    flocal.evaluate(*localCenter, value);
    std::clog << "Value   : " << value << std::endl;

    flocal.jacobian(*localCenter, jacobian);
    std::clog << "Jacobian: " << std::endl << jacobian << std::endl;

    flocal.hessian(*localCenter, hessian);
    std::clog << "Hessian: " << std::endl << hessian << std::endl;

    // Check for evaluateQuadrature

    QuadratureType quad(entity, 1);
    std::vector<RangeType> quadValues(quad.nop());
    std::vector<JacobianRangeType> quadJacobians(quad.nop());
    std::vector<HessianRangeType> quadHessians(quad.nop());

    flocal.evaluateQuadrature(quad, quadValues);
    std::clog << "Quadrature Values:" << std::endl;
    for (unsigned qp = 0; qp < quad.nop(); ++qp) {
      std::clog << "Value @ " << entity.geometry().global(Dune::Fem::coordinate(quad[qp]))
                << std::endl
                << quadValues[qp]
                << std::endl;
    }

    flocal.evaluateQuadrature(quad, quadJacobians);
    std::clog << "Quadrature Jacobians:" << std::endl;
    for (unsigned qp = 0; qp < quad.nop(); ++qp) {
      std::clog << "Jacobian @ " << entity.geometry().global(Dune::Fem::coordinate(quad[qp]))
                << std::endl
                << quadJacobians[qp]
                << std::endl;
    }

#if 0
    // not supported by Dune::Fem
    flocal.evaluateQuadrature(quad, quadHessians);
    std::clog << "Quadrature Hessians:" << std::endl;
    for (unsigned qp = 0; qp < quad.nop(); ++qp) {
      std::clog << "Hessian @ " << entity.geometry().global(Dune::Fem::coordinate(quad[qp]))
                << std::endl
                << quadHessians[qp]
                << std::endl;
    }
#endif

    flocal.unbind();

  } // mesh loop

}

template<class DiscreteFunction>
void normTest(DiscreteFunction&& expression)
{
  using ExpressionType = std::decay_t<DiscreteFunction>;
  typedef typename ExpressionType::FunctionSpaceType FunctionSpaceType;
  typedef typename ExpressionType::GridPartType GridPartType;

  auto& gridPart = const_cast<GridPartType&>(expression.gridPart());
  //for most norms, order defaults to space().order * 2
  std::size_t order = expression.order() * 2;

  typedef Dune::Fem::L2Norm<GridPartType> L2NormType;
  L2NormType l2Norm(gridPart, order);
  typedef Dune::Fem::H1Norm<GridPartType> H1NormType;
  H1NormType h1Norm(gridPart, order);
  typedef Dune::Fem::L1Norm<GridPartType> L1NormType;
  L1NormType l1Norm(gridPart, order);
  typedef Dune::Fem::LPNorm<GridPartType> LPNormType;
  LPNormType l3Norm(gridPart,3, order);
  LPNormType l4Norm(gridPart,4, order);
  typedef Dune::Fem::Integral<GridPartType> IntegralType;
  IntegralType integral(gridPart, order);

  auto X  = gridFunction(gridPart, [](auto&& x) { return x; });

  std::clog << "Norms of " << expression.name() << std::endl
            << std::endl;

  std::clog << "L1-Norm:  " << l1Norm.norm(expression) << std::endl
            << "L2-Norm:  " << l2Norm.norm(expression) << std::endl
            << "L3-Norm:  " << l3Norm.norm(expression) << std::endl
            << "L4-Norm:  " << l4Norm.norm(expression) << std::endl
            << "H1-Norm:  " << h1Norm.norm(expression) << std::endl
            << "Integral: " << integral.norm(expression) << std::endl  << std::endl;

  std::clog << "Norms of X * (" << expression.name() << ")" << std::endl
            << "Optimized: " << (X * expression).name() << std::endl
            << std::endl;

  std::clog << "L1-Norm:  " << l1Norm.norm(X * expression) << std::endl
            << "L2-Norm:  " << l2Norm.norm(X * expression) << std::endl
            << "L3-Norm:  " << l3Norm.norm(X * expression) << std::endl
            << "L4-Norm:  " << l4Norm.norm(X * expression) << std::endl
            << "H1-Norm:  " << h1Norm.norm(X * expression) << std::endl
            << "Integral: " << integral.norm(X * expression) << std::endl
            << std::endl;

  std::clog << "Norms of (X * X) * (" << expression.name() << ")" << std::endl
            << "Optimized: " << ((X * X)  * expression).name() << std::endl
            << std::endl;

  std::clog << "L1-Norm:  " << l1Norm.norm((X * X)  * expression) << std::endl
            << "L2-Norm:  " << l2Norm.norm((X * X)  * expression) << std::endl
            << "L3-Norm:  " << l3Norm.norm((X * X)  * expression) << std::endl
            << "L4-Norm:  " << l4Norm.norm((X * X)  * expression) << std::endl
            << "H1-Norm:  " << h1Norm.norm((X * X)  * expression) << std::endl
            << "Integral: " << integral.norm((X * X)  * expression) << std::endl  << std::endl;

  auto interpolationDGSpace = discreteFunctionSpace<FunctionSpaceType::dimRange>(gridPart, lagrangeDG<POLORDER>);
  auto interpolationDGUh = discreteFunction(interpolationDGSpace, "dGUh");

  Dune::Fem::interpolate(expression, interpolationDGUh);

  std::clog << std::endl << "DG-DOFs of interpolation test" << std::endl;
  {
    auto dend(interpolationDGUh.dend());
    for (auto it = interpolationDGUh.dbegin(); it != dend; ++it) {
      std::clog << *it << std::endl;
    }
  }

  std::clog << "Norms of I_DG(" << expression.name() <<  ") - (" << expression.name() << ")"
            << std::endl
            << std::endl;

  std::clog << "L1-Norm: " << l1Norm.distance(interpolationDGUh, expression) << std::endl
            << "L2-Norm: " << l2Norm.distance(interpolationDGUh, expression) << std::endl
            << "L3-Norm: " << l3Norm.distance(interpolationDGUh, expression) << std::endl
            << "L4-Norm: " << l4Norm.distance(interpolationDGUh, expression) << std::endl
            << "H1-Norm: " << h1Norm.distance(interpolationDGUh, expression) << std::endl
            << "Integral: " << integral.distance(interpolationDGUh, expression) << std::endl  << std::endl;
}

template<class F>
void printOrder(F&& f)
{
  std::clog << "Order of " << f.name() << " is " << f.order() << std::endl;
  std::clog << "Order of D(" << f.name() << ") is " << derivative(f).order() << std::endl;
}

//!Embedded grid-function test DGF-"file"
const std::string dgfData(
  "DGF\n"
  "\n"
  "%Interval\n"
  "% -1   -1\n"
  "% 1   1\n"
  "% 1   1\n"
  "#\n"
  "Vertex\n"
  "-1  -1\n"
  " 1  -1\n"
  " 1   1\n"
  "-1   1\n"
  " 0   0\n"
  "#\n"
  "\n"
  "SIMPLEX\n"
  "0 1 4\n"
  "1 2 4\n"
  "2 3 4\n"
  "3 0 4\n"
  "#\n"
  "\n"
  "BoundarySegments\n"
  "1   0 1  : blah1  % right boundary\n"
  "2   1 2  : blah2  % upper boundary\n"
  "3   2 3  : blah3  % left boundary\n"
  "4   3 0  : blah4  % lower boundary\n"
  "#\n"
  "GridParameter\n"
  "\n"
  "% longest or arbitrary (see DGF docu)\n"
  "refinementedge longest\n"
  "% there is zarro information on this ... :(\n"
  "overlap 0\n"
  "% whatever this may be ...\n"
  "tolerance 1e-12\n"
  "% silence?\n"
  "verbose 0\n"
  "#\n");

/** Test-template main program. Instantiate a single expression
 * template and evaluate it on a simple grid.
 */
int main(int argc, char *argv[])
{
  try {
    // General setup
    Dune::Fem::MPIManager::initialize(argc, argv);

    // append parameter
    Dune::Fem::Parameter::append(argc, argv);

    // append default parameter file
    Dune::Fem::Parameter::append(SRCDIR "/parameter");

    //redirect the stream cerr to cout
    //so we only get the output of clog on stderr
    ScopedRedirect redirect(std::cerr, std::cout);

    // reduce precision and use normalized FP output
    // std::clog << std::scientific << std::setprecision(4);
    std::clog << std::fixed << std::setprecision(8);

    // type of hierarchical grid
    typedef Dune::GridSelector::GridType HGridType;

    // the method rank and size from MPIManager are static
    if (Dune::Fem::MPIManager::rank() == 0) {
      std::clog << "Loading embedded macro grid: "
                << std::endl
                << "=================================" << std::endl
                << dgfData
                << "=================================" << std::endl
                << std::endl;
    }

    // we try to be self-contained an simply load the embedded simple dgf-file
    std::istringstream dgfStream(dgfData);

    // construct macro using the DGF Parser
    Dune::GridPtr<HGridType> gridPtr(dgfStream);
    HGridType& grid = *gridPtr;

    // do initial load balance
    grid.loadBalance();

    auto gridPart = leafGridPart<Dune::InteriorBorder_Partition>(grid);

    auto space = discreteFunctionSpace<HGridType::dimensionworld>(gridPart, lagrange<POLORDER>);
    auto scalarSpace = discreteFunctionSpace(gridPart, lagrange<POLORDER>);

    auto Uh = discreteFunction(space, "Uh");
    auto scalarUh = discreteFunction(scalarSpace, "scalarUh");

    Dune::Fem::interpolate(sqr(gridFunction(gridPart, [](auto&& x) { return x; }, 0_c)), Uh);
    Dune::Fem::interpolate(frobenius2(gridFunction(gridPart, [](auto&& x) { return x; }, 0_c)), scalarUh);

    using FunctionSpaceType [[maybe_unused]] = decltype(space);

#if SEPARATETESTS
    const std::string testName = Dune::Fem::Parameter::getValue<std::string>(
      "testName",
      std::string(basename(argv[0])).substr(std::string(MODULENAME).size()+1)
      );
    std::clog << "Module " << MODULENAME << std::endl;
#else
    const std::string testName = Dune::Fem::Parameter::getValue<std::string>("testName", "default");
#endif

    if (false) {
    }
#if DEFAULT
    else if (testName == "default") {
      auto function = gridFunction(gridPart, [](auto&& x) {
          return sqrt(inner(x, x));
        });
      function.printInfo();
      gridWalkTest(function);
      normTest(function);
    }
#endif
#if CONSTANT_GENERATOR
    else if (testName == "constant-generator") {
      auto function = gridFunction(gridPart, [](auto&& x) {
          return 1;
        });
      function.printInfo();
      gridWalkTest(function);
      normTest(function);
    }
#endif
#if DISCRETE_FUNCTION
    else if (testName == "discrete-function") {
      auto function = gridFunction(Uh, 1_c);
      gridWalkTest(function);
      function.printInfo();
    }
#endif
#if COS
    else if (testName == "cos") {
      auto X = gridFunction(gridPart, [](auto&& x) { return x; });
      auto F = cos(X);
      F.printInfo();
      normTest(F);
    }
#endif
#if SIN
    else if (testName == "sin") {
      auto X = gridFunction(gridPart, [](auto&& x) { return x; });
      auto F = sin(X);
      F.printInfo();
      normTest(F);
    }
#endif
#if GAUSSIAN
    else if (testName == "gaussian") {
      // This test is here because it is the standard RHS for the
      // standard poisson example
      auto X = gridFunction(gridPart, [](auto&& x) { return x; });
      auto C  = 1_f / 2_f;
      auto exactSolution = gridFunction(exp(-C*X*X), 1_c);
      exactSolution.printInfo();
      auto dimDomain = intFraction<FunctionSpaceType::dimDomain>();
      auto F = 2_f * C * (dimDomain - 2_f * C * X * X) * exactSolution;
      F.printInfo();
      normTest(F);
    }
#endif
#if COS_PRODUCT
    else if (testName == "cos-product") {
      // This test is here because F is used in the poisson-dg example
      auto X = gridFunction(gridPart, [](auto&& x) { return x; });
      auto exactSolution = gridFunction(
        multiplyLoop<FunctionSpaceType::dimDomain>(1_f, [&X](auto i){
            return cos(2_f*M::pi*X[i]);
          }),
        1_c);
      auto F = (8_f*M::pi*M::pi+1_f)*exactSolution;
      F.printInfo();
      normTest(F);
    }
#endif
#if RUDIMENTARY_TIMINGS_VECTOR
    else if (testName == "rudimentary-timings-vector") {
      const std::size_t count = Dune::Fem::Parameter::getValue<std::size_t>("timing.iterations", 10000);

      std::clog << "Iterations: " << count << std::endl;

      auto f1 = gridFunction(gridPart, [](auto&& x) { return x / sqrt(inner(x, x)); }, 0_c);
      std::clog << "TensorFunction timing: " << timingTest(f1, count) << std::endl;

      auto f2 = NormalizeFunction<decltype(gridPart)>(gridPart);
      std::clog << "Fem::Function  timing: " << timingTest(f2, count) << std::endl;

      sameValue(f1, f2);

      f1.printInfo();
    }
#endif
#if RUDIMENTARY_TIMINGS
    else if (testName == "rudimentary-timings") {
      const std::size_t count = Dune::Fem::Parameter::getValue<std::size_t>("timing.iterations", 10000);

      std::clog << "Iterations: " << count << std::endl;

      auto X = gridFunction(gridPart, [](auto&& x) { return x; });
      auto exactSolution = gridFunction(gridPart, [](auto&& x) {
          return multiplyLoop<FunctionSpaceType::dimDomain>(1_f, [&x](auto i){
              return cos(2_f*M::pi*x[i]);
            });
        },
        0_c);
      auto F = (8_f*M::pi*M::pi+1_f)*exactSolution;
//      F.printInfo();
      std::clog << "TypedValue timing: " << timingTest(F, count) << std::endl;
#if 1
      {
        auto X = gridFunction(gridPart, [](auto&& x) { return x; });
        auto exactSolution = gridFunction(gridPart, [](auto&& x) {
            return multiplyLoop<FunctionSpaceType::dimDomain>(1_f, [&x](auto i){
                return cos(2*M_PI*x[i]);
              });
          },
          0_c);
        auto F = (8*M_PI*M_PI+1)*exactSolution;
        std::clog << "Ordinary constants timing: " << timingTest(F, count) << std::endl;
        //F.printInfo();
      }
#endif
#if 1
      {
        std::clog << "Hand-made timing: " << timingTest(MyCosProduct<decltype(gridPart)>(gridPart), count) << std::endl;
      }

      sameValue(F, MyCosProduct<decltype(gridPart)>(gridPart));
#endif
    }
#endif
#if BASIC_OPERATIONS
    else if (testName == "basic-operations") {
      (-Uh).printInfo();
      (Uh+Uh).printInfo();
      (Uh-Uh).printInfo();
      (Uh*Uh).printInfo();
      (Uh / scalarUh).printInfo();
      (Uh*3.0).printInfo();
      (Uh/3.0).printInfo();
    }
#endif
#if ASSUME
    else if (testName == "assume") {
      auto x = gridFunction(gridPart, [](auto&& x) { return x; });
      auto xPlus = assume<PositiveExpression>(x);

      static_assert(xPlus > 0_c, "assume(Expr > 0) failed");
      static_assert(!(x > 0_c), "!(Expr > 0) failed");
    }
#endif
#if DERIVATIVE
    else if (testName == "derivative") {

      derivative(scalarUh).printInfo();

      std::clog << std::endl << "***********************************************" << std::endl << std::endl;

      derivative<2>(scalarUh).printInfo();

      std::clog << std::endl << "***********************************************" << std::endl << std::endl;

      derivative(Uh).printInfo();

      std::clog << std::endl << "***********************************************" << std::endl << std::endl;

      derivative<2>(Uh).printInfo();
    }
#endif
#if MATH_FUNCTIONS
    else if (testName == "math-functions") {
      auto uhTensor = localFunctionPlaceholder(Uh);
      auto f = gridFunction(gridPart, uhTensor);

      // direct grid-function access
      sqrt(f).printInfo();

      // promotion from tensor
      sqrt(uhTensor);

      // promotion from discrete function
      sqrt(Uh).printInfo();
      sqr(Uh).printInfo();
      cos(Uh).printInfo();
      sin(Uh).printInfo();
      tan(Uh).printInfo();
      acos(Uh).printInfo();
      asin(Uh).printInfo();
      atan(Uh).printInfo();
      erf(Uh).printInfo();
      exp(Uh).printInfo();
      log(Uh).printInfo();
      cosh(Uh).printInfo();
      sinh(Uh).printInfo();
      tanh(Uh).printInfo();
      acosh(Uh).printInfo();
      asinh(Uh).printInfo();
      atanh(Uh).printInfo();
      atan2(Uh, Uh).printInfo();
      pow(Uh, Uh).printInfo();

      auto X = gridFunction(gridPart, [](auto&& x) { return x; });
      min(X, X).printInfo();
      max(X, X).printInfo();
    }
#endif
    else {
      DUNE_THROW(Dune::NotImplemented, "Test \""+testName+"\" not implemented");
    }

    return EXIT_SUCCESS;
  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
    return EXIT_FAILURE;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
    return EXIT_FAILURE;
  }
}

//!@} GridFunctionTests

//!@} GridFunctions
