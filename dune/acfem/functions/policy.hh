#ifndef __DUNE_ACFEM_FUNCTIONS_POLICY_HH__
#define __DUNE_ACFEM_FUNCTIONS_POLICY_HH__

#include "../common/types.hh"

namespace Dune::ACFem::GridFunction::Policy
{
  using IndeterminateId = IndexConstant<0>;

  constexpr auto indeterminateId()
  {
    return IndeterminateId::value;
  }

  using AutoDiffLimit = IndexConstant<2>;

  constexpr auto autoDiffLimit()
  {
    return AutoDiffLimit::value;
  }

}

#endif //  __DUNE_ACFEM_FUNCTIONS_POLICY_HH__
