#ifndef __DUNE_ACFEM_FUNCTIONS_LOCALFUNCTIONTRAITS_HH__
#define __DUNE_ACFEM_FUNCTIONS_LOCALFUNCTIONTRAITS_HH__

#include <dune/fem/function/localfunction/temporary.hh>
#include <dune/fem/function/localfunction/const.hh>

#include "../common/types.hh"
#include "../common/quadraturepoint.hh"

namespace Dune {

  namespace ACFem {

    namespace GridFunction {

      /**@internal Default to FalseType.*/
      template<class T, class SFINAE = void>
      struct IsLocalFunction
        : FalseType
      {};

      /**@intenal Forward to decay_t<>.*/
      template<class T>
      struct IsLocalFunction<T, std::enable_if_t<!IsDecay<T>{}> >
        : IsLocalFunction<std::decay_t<T> >
      {};

      /**Identify a Fem::LocalFunction.*/
      template<class T>
      struct IsLocalFunction<
        T,
        std::enable_if_t<(IsDecay<T>{}
                          && std::is_same<void, decltype(std::declval<T>().bind(std::declval<typename T::EntityType>()))>{}
                          && std::is_same<void, decltype(std::declval<T>().evaluate(
                                                           std::declval<PlaceholderQuadraturePoint<typename T::DomainType> >(),
                                                           std::declval<typename T::RangeType&>()))>{}
        )> >
        : std::true_type
      {};

      ///////////////////////////////////////////////////////

      /**@internal Default FalseType.*/
      template<class T, class SFINAE = void>
      struct IsConstLocalFunction
        : FalseType
      {};

      /**@internal Forward to decay_t.*/
      template<class T>
      struct IsConstLocalFunction<T, std::enable_if_t<!IsDecay<T>{}> >
        : IsConstLocalFunction<std::decay_t<T> >
        {};

      /**TrueType if a T can be wrapped into a Fem::ConstLocalFunction.*/
      template<class T>
      struct IsConstLocalFunction<
        T,
        std::enable_if_t<(IsDecay<T>{}
                          && std::is_same<T, Fem::ConstLocalFunction<typename T::GridFunctionType> >{}
        )> >
        : TrueType
      {};

      ////////////////////////////////////////////

      template<class T, class SFINAE = void>
      struct IsDiscreteFunctionSpace
        : FalseType
      {};

      template<class T>
      struct IsDiscreteFunctionSpace<T, std::enable_if_t<!IsDecay<T>{}> >
        : IsDiscreteFunctionSpace<T, std::decay_t<T> >
      {};

      template<class T>
      struct IsDiscreteFunctionSpace<T, std::enable_if_t<(IsDecay<T>{} && std::is_same<T, typename T::DiscreteFunctionSpaceType>{})> >
        : TrueType
      {};

      ////////////////////////////////////////////

      /**@internal Default FalseType.*/
      template<class T, class SFINAE = void>
      struct IsWrappableByConstLocalFunction
        : FalseType
      {};

      /**@internal Forward to decay_t.*/
      template<class T>
      struct IsWrappableByConstLocalFunction<T, std::enable_if_t<!IsDecay<T>{}> >
        : IsWrappableByConstLocalFunction<std::decay_t<T> >
      {};

      /**TrueType if a T can be wrapped into a Fem::ConstLocalFunction.*/
      template<class T>
      struct IsWrappableByConstLocalFunction<
        T,
        std::enable_if_t<(IsDecay<T>{}
                          && std::is_base_of<Fem::HasLocalFunction, T>{}
          )> >
        : TrueType
      {};

      //////////////////////////////////////

      /**@c TrueType if T can be passed through constLocalFunction().*/
      template<class T>
      struct IsConstLocalFunctionOperand
        : BoolConstant<IsConstLocalFunction<T>::value || IsWrappableByConstLocalFunction<T>::value>
      {};

      /**Wrap an F into a Fem::ConstLocalFunction if directly allowed
       * by Fem::ConstLocalFunction.
       */
      template<
        class F,
        std::enable_if_t<(!IsConstLocalFunction<F>::value
                          && IsWrappableByConstLocalFunction<F>::value
        ), int> = 0>
      constexpr auto ensureConstLocalFunction(F&& f)
      {
        return Fem::ConstLocalFunction<std::decay_t<F> >(std::forward<F>(f));
      }

      /**@internal Forward a Fem::ConstLocalFunction as is.*/
      template<class F, std::enable_if_t<IsConstLocalFunction<F>::value, int> = 0>
      constexpr decltype(auto) ensureConstLocalFunction(F&& f)
      {
        return std::forward<F>(f);
      }

      /**Generate a temporary local function for the given discrete space.*/
      template<class DiscreteSpace, class Dof = typename DiscreteSpace::RangeFieldType>
      Fem::TemporaryLocalFunction<DiscreteSpace, Dof>
      temporaryLocalFunction(const DiscreteSpace& space, const Dof& dummy = Dof())
      {
        return Fem::TemporaryLocalFunction<DiscreteSpace, Dof>(space);
      }

    } // NS GridFunction

    using GridFunction::IsConstLocalFunction;
    using GridFunction::IsDiscreteFunctionSpace;
    using GridFunction::IsWrappableByConstLocalFunction;
    using GridFunction::ensureConstLocalFunction;
    using GridFunction::temporaryLocalFunction;

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_FUNCTIONS_LOCALFUNCTIONTRAITS_HH__
