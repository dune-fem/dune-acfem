#ifndef __DUNE_ACFEM_FUNCTIONS_MODULES_ELEMENTMEASURE_HH__
#define __DUNE_ACFEM_FUNCTIONS_MODULES_ELEMENTMEASURE_HH__

#include "../policy.hh"
#include "../placeholders/elementmeasure.hh"
#include "../gridfunction.hh"

namespace Dune {

  namespace ACFem {

    namespace GridFunction {

      /**@addtogroup GridFunctions
       * @{
       */

      template<class GridPart, std::size_t IndeterminateId = Policy::indeterminateId()>
      auto elementMeasure(const GridPart& gridPart, IndexConstant<IndeterminateId> id = IndexConstant<IndeterminateId>{})
      {
        return gridFunction(elementMeasurePlaceholder(gridPart), 0_c, id);
      }

      ///@} GridFunctions

    } // NS GridFunction

    using GridFunction::elementMeasure;

  } // NS ACFem

} // NS Dune

#endif //  __DUNE_ACFEM_FUNCTIONS_MODULES_ELEMENTMEASURE_HH__
