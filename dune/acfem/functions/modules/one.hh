#ifndef __DUNE_ACFEM_FUNCTIONS_MODULES_ONE_HH__
#define __DUNE_ACFEM_FUNCTIONS_MODULES_ONE_HH__

#include "../policy.hh"
#include "../functiontraits.hh"
#include "../bindabletensorfunction.hh"

namespace Dune {

  namespace ACFem {

    namespace GridFunction {

      /**One function given function space and grid-part.*/
      template<class FunctionSpace, class GridPart, std::size_t IndeterminateId = Policy::indeterminateId()>
      constexpr auto one(const GridPart& gridPart,
                         const FunctionSpace& space = FunctionSpace{},
                         IndexConstant<IndeterminateId> id = IndexConstant<IndeterminateId>{})
      {
        using TensorType = typename TensorTraits<typename FunctionSpace::RangeType>::TensorType;
        return gridFunction(
          gridPart,
          Tensor::one(Expressions::asExpression(TensorType{})),
          2_c, id);
      }

      /**The canonical one object for anything providing a RangeType and a gridPart() method.*/
      template<
        class T,
        std::size_t IndeterminateId = Policy::indeterminateId(),
        std::enable_if_t<HasGridPartMethod<T>::value && HasRangeType<T>::value, int> = 0>
      constexpr auto one(T&& t, IndexConstant<IndeterminateId> id = IndexConstant<IndeterminateId>{})
      {
        using TensorType = typename TensorTraits<typename std::decay_t<T>::RangeType>::TensorType;
        return gridFunction(
          t.gridPart(),
          Tensor::one(Expressions::asExpression(TensorType{})),
          2_c, id);
      }

    } // NS GridFunction

    using GridFunction::one;

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_FUNCTIONS_MODULES_ONE_HH__
