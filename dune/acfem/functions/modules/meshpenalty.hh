#ifndef __DUNE_ACFEM_FUNCTIONS_MODULES_MESHPENALTY_HH__
#define __DUNE_ACFEM_FUNCTIONS_MODULES_MESHPENALTY_HH__

#include "../expressions.hh"
#include "elementmeasure.hh"

namespace Dune {

  namespace ACFem {

    namespace GridFunction {

      /**@addtogroup GridFunctions
       * @{
       */

      /**Create a penalty function diverging with the inverse element
       * diameter towards infinity.
       */
      template<class GridPart>
      auto meshPenalty(const GridPart& gridPart)
      {
        auto exponent = -intFraction<1, GridPart::dimension>();
        return pow(elementMeasure(gridPart), exponent);
      }

      ///@} GridFunctions

    } // NS GridFunction

    using GridFunction::meshPenalty;

  } // NS ACFem

} // NS Dune

#endif //  __DUNE_ACFEM_FUNCTIONS_MODULES_MESHPENALTY_HH__
