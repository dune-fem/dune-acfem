#ifndef __DUNE_ACFEM_FUNCTIONS_MODULES_ZERO_HH__
#define __DUNE_ACFEM_FUNCTIONS_MODULES_ZERO_HH__

#include "../policy.hh"
#include "../functiontraits.hh"
#include "../bindabletensorfunction.hh"

namespace Dune {

  namespace ACFem {

    namespace GridFunction {

      /**Zero function given function space and grid-part.*/
      template<class FunctionSpace, class GridPart, std::size_t IndeterminateId = Policy::indeterminateId()>
      constexpr auto zeros(const GridPart& gridPart,
                           const FunctionSpace& space = FunctionSpace{},
                           IndexConstant<IndeterminateId> id = IndexConstant<IndeterminateId>{})
      {
        using FemRangeType = typename FunctionSpace::RangeType;
        using FieldType = typename FieldTraits<FemRangeType>::field_type;
        using RangeType = std::conditional_t<std::is_base_of<FieldVector<FieldType, 1>, FemRangeType>::value,
                                             FieldType, FemRangeType>;

        using TensorType = typename TensorTraits<RangeType>::TensorType;
        return gridFunction(
          gridPart,
          Tensor::zeros<TensorType>(Expressions::Disclosure{}),
          2_c, id);
      }

      /**The canonical zero object for anything providing a RangeType
       * and a gridPart() method.
       */
      template<
        class T,
        std::size_t IndeterminateId = Policy::indeterminateId(),
        std::enable_if_t<HasGridPartMethod<T>::value && HasRangeType<T>::value, int> = 0>
      constexpr auto zeros(T&& t, IndexConstant<IndeterminateId> id = IndexConstant<IndeterminateId>{})
      {
        // Scalar, 1d vector decay
        using FemRangeType = typename std::decay_t<T>::RangeType;
        using FieldType = typename FieldTraits<FemRangeType>::field_type;
        using RangeType = std::conditional_t<std::is_base_of<FieldVector<FieldType, 1>, FemRangeType>::value,
                                             FieldType, FemRangeType>;

        using TensorType = typename TensorTraits<RangeType>::TensorType;
        return gridFunction(
          t.gridPart(),
          Tensor::zeros<TensorType>(Expressions::Disclosure{}),
          2_c, id);
      }

      /**@internal Zeros-provider for the general expression
       * interdface. The functor argument is ignored.
       */
      template<class F, class T>
      constexpr auto zero(F&&, T&& t)
      {
        return zeros(std::forward<T>(t));
      }

      template<class FunctionSpace, class GridPart>
      using ZeroGridFunction = std::decay_t<decltype(zeros<FunctionSpace>(std::declval<GridPart>()))>;

    } // NS GridFunction

    using GridFunction::ZeroGridFunction;

    using GridFunction::zeros;

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_FUNCTIONS_MODULES_ZERO_HH__
