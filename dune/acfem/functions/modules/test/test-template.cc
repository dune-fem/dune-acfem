#include <config.h>

#include <iostream>
#include <sstream>

#include <dune/common/exceptions.hh>
#include <dune/fem/io/parameter.hh>

#include <dune/fem/gridpart/adaptiveleafgridpart.hh>

#include "../../../common/scopedredirect.hh"
#include "../../../common/geometry.hh"
#include "../../../common/gridfunctionspace.hh"
#include "../../../common/quadraturepoint.hh"

#include "../../../models/basicmodels.hh"

#include "../../basicfunctions.hh"

/**@addtogroup Tests
 * @{
 */

using namespace Dune::ACFem;
using namespace Dune::ACFem::Literals;

#ifndef TEST_DGF_DATA
# define TEST_DGF_DATA dgfSimplexData2d
#endif

//Embedded test DGF-"file" with a simple interval
const std::string dgfIntervalData2d(R"(DGF

Interval
 0   0
 1   1
 1   1
#

GridParameter
% longest or arbitrary (see DGF docu)
refinementedge longest
overlap 0
#

BoundaryDomain
default 3
1   0 0   1 0  % lower boundary
2   0 1   1 1  % upper boundary
#

#)");

//Embedded test DGF-"file" with some simplices
const std::string dgfSimplexData2d(R"(DGF

Vertex
 -1  -1
  1  -1
  1   1
 -1   1
  0   0
#

SIMPLEX
 0 1 4
 1 2 4
 2 3 4
 3 0 4
#

BoundarySegments
 1   0 1  : blah1  % right boundary
 2   1 2  : blah2  % upper boundary
 3   2 3  : blah3  % left boundary
 4   3 0  : blah4  % lower boundary
#
GridParameter
 % longest or arbitrary (see DGF docu)
 refinementedge longest
 % there is zarro information on this ... :(
 overlap 0
 % whatever this may be ...
 tolerance 1e-12
 % silence?
 verbose 0
#)");

namespace Dune {

  // ... in order to sort elements by their geometrical center
  template<class T, int N>
  bool operator<(const FieldVector<T, N>& a, const FieldVector<T, N>& b)
  {
    for(unsigned int i = 0; i < a.size(); ++i)
    {
      if(std::abs(a[i]-b[i]) > 1e-8)
      {
        return a[i] < b[i];
      }
    }
    return a.two_norm2() < b.two_norm2();
  }
}

template<class F, std::enable_if_t<IsTensorFunction<F>::value, int> = 0>
void printInfo(std::ostream& out, F&& f)
{
  f.printInfo(out);
}

template<class F, std::enable_if_t<!IsTensorFunction<F>::value, int> = 0>
void printInfo(std::ostream& out, F&& f)
{
  out << "Not a BindableTensorFunction: "
      << f.name()
      << std::endl;
  out << "Suggested quadrature order: " << f.order() << std::endl;
}

/**Test routine which is called with the GridFunctionExpression from
 * the main-program. We simply walk over the grid and evaluate the
 * local function and its jacobian on each element.
 */
template<class Function>
void gridWalkTest(Function&& function)
{
  using FunctionType = std::decay_t<Function>;
  using GridPartType = typename FunctionType::GridPartType;
  using EntityType = typename FunctionType::EntityType;
  using DomainType = typename FunctionType::DomainType;
  using RangeType = typename FunctionType::RangeType;
  using JacobianRangeType = typename FunctionType::JacobianRangeType;
  using HessianRangeType = typename FunctionType::HessianRangeType;

  const auto& gridPart = function.gridPart();

  std::map<DomainType, EntityType> leafElements;
  // Sort elements by center to make the traversal order below unique
  for (const auto& entity : elements(gridPart)) {
    leafElements[entity.geometry().center()] = entity;
  }

  using GeometryInformation = GeometryInformation<GridPartType>;

  RangeType values;
  JacobianRangeType jacobian;
  HessianRangeType hessian;

  bool hasPrintedHack = false;

  // Loop over the sorted elements.
  for (auto mapEl : leafElements) {

    const auto& entity = mapEl.second;

    function.bind(entity);

    if constexpr (GridFunction::hasSkeletonSupport<Function>()) {

      auto iend(gridPart.iend(entity));
      for (auto iit = gridPart.ibegin(entity); iit != iend; ++iit) {
        const auto& intersection(*iit);

        if (intersection.neighbor() || !intersection.boundary()) {
          continue;
        }

        auto active = function.classifyIntersection(intersection);

        std::clog << "Boundary Id: " << intersection.impl().boundaryId() << std::endl
                  << "Active:      " << active.first << std::endl
                  << "Components:  " << active.second << std::endl
                  << std::endl;

        if (active.first) {

          const auto& localCenter = entity.geometry().local(intersection.geometry().center());

          function.evaluate(*localCenter, values);

          if (!hasPrintedHack) {
            hasPrintedHack = true;
            std::clog << "**** Function: **** "
                      << std::endl
                      << function.name()
                      << std::endl
                      << std::endl;
            printInfo(std::clog, std::forward<Function>(function));
          }

          std::clog << "Local center   : " << localCenter << std::endl;
          std::clog << "Global center  : " << intersection.geometry().center() << std::endl;

          function.jacobian(*localCenter, jacobian);
          function.hessian(*localCenter, hessian);

          std::clog << "Values:" << std::endl
                    << values << std::endl
                    << std::endl;

          std::clog << "Jacobian:" << std::endl
                    << jacobian << std::endl
                    << std::endl;

          std::clog << "Hessian: " << std::endl
                    << hessian << std::endl
                    << std::endl;
        }

      }

    } else { // boundary

      // Local "center", whatever that might be
      const auto& localCenter(GeometryInformation::localCenter(entity.geometry().type()));

      function.evaluate(*localCenter, values);

      if (!hasPrintedHack) {
        hasPrintedHack = true;
        std::clog << "**** Function: **** "
                  << std::endl
                  << function.name()
                  << std::endl
                  << std::endl;
        printInfo(std::clog, std::forward<Function>(function));
      }

      std::clog << "Local Center:  " << localCenter << std::endl;
      std::clog << "Global Center: " << entity.geometry().center() << std::endl;

      function.jacobian(*localCenter, jacobian);
      function.hessian(*localCenter, hessian);

      std::clog << "Values:" << std::endl
                << values << std::endl
                << std::endl;

      std::clog << "Jacobian:" << std::endl
                << jacobian << std::endl
                << std::endl;

      std::clog << "Hessian: " << std::endl
                << hessian << std::endl
                << std::endl;

    } // ! boundary

    function.unbind();

  } // ordered mesh loop

}

/**Test-template main program. Instantiate a single expression
 * template and evaluate it on a simple grid.
 */
int main(int argc, char *argv[])
{
  try {
    // General setup
    Dune::Fem::MPIManager::initialize(argc, argv);

    // append parameter
    Dune::Fem::Parameter::append(argc, argv);

    // append default parameter file
    Dune::Fem::Parameter::append(SRCDIR "/parameter");

    //redirect the stream cerr to cout
    //so we only get the output of clog on stderr
    ScopedRedirect redirect(std::cerr, std::cout);

    // reduce precision and use normalized FP output
    // std::clog << std::scientific << std::setprecision(4);
    std::clog << std::fixed << std::setprecision(8);

    // type of hierarchical grid
    typedef Dune::GridSelector::GridType HGridType;

    // the method rank and size from MPIManager are static
    if (Dune::Fem::MPIManager::rank() == 0) {
      std::clog << "Loading embedded macro grid: "
                << std::endl
                << "=================================" << std::endl
                << TEST_DGF_DATA
                << "=================================" << std::endl
                << std::endl;
    }

    // we try to be self-contained an simply load the embedded simple dgf-file
    std::istringstream dgfStream(TEST_DGF_DATA);

    // construct macro using the DGF Parser
    Dune::GridPtr<HGridType> gridPtr(dgfStream);
    HGridType& grid = *gridPtr;

    // do initial load balance
    grid.loadBalance();

    typedef Dune::Fem::AdaptiveLeafGridPart<HGridType, Dune::InteriorBorder_Partition> GridPartType;
    GridPartType gridPart(grid);

    const auto testName = Dune::Fem::Parameter::getValue<std::string>("testName", "test-default");

    if (testName == "default") {
      gridWalkTest(elementMeasure(gridPart));
    }
    else if (testName == "elementmeasure") {
      gridWalkTest(elementMeasure(gridPart));
    }
    else if (testName == "identity") {
      gridWalkTest(identityGridFunction(gridPart));
    }
    else if (testName == "identity-components") {
      gridWalkTest(identityGridFunction(gridPart)[0_c]);
      gridWalkTest(identityGridFunction(gridPart)[1_c]);
    }
    else if (testName == "one") {
      gridWalkTest(one(elementMeasure(gridPart)));
    }
    else if (testName == "meshpenalty") {
      gridWalkTest(meshPenalty(gridPart));
    }
    else if (testName == "modeldirichletvalues") {
      // generate a basic model and see what happens ...
      auto X0 = identityGridFunction(gridPart)[0_c];

      gridWalkTest(modelDirichletValues(laplacianModel(X0) + dirichletBoundaryModel(X0), gridPart));
      gridWalkTest(modelDirichletValues(laplacianModel(X0), gridPart));
    }
    else if (testName == "zero") {
      gridWalkTest(zero(elementMeasure(gridPart)));
    }
    else {
      DUNE_THROW(Dune::NotImplemented, "Test \""+testName+"\" not implemented");
    }

    return EXIT_SUCCESS;
  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
    return EXIT_FAILURE;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
    return EXIT_FAILURE;
  }
}

//!@} Tests

//!@} GridFunctions
