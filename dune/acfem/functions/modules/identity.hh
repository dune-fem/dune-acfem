#ifndef __DUNE_ACFEM_FUNCTIONS_MODULES_IDENTITY_HH__
#define __DUNE_ACFEM_FUNCTIONS_MODULES_IDENTITY_HH__

#include "../../tensors/operations/indeterminate.hh"
#include "../policy.hh"
#include "../functiontraits.hh"
#include "../gridfunction.hh"

namespace Dune {

  namespace ACFem {

    namespace GridFunction {

      /**Generate the "X" function which simply returns the global
       * co-ordinates of the given quadrature point.
       */
      template<class GridPart, std::size_t IndeterminateId = Policy::indeterminateId()>
      constexpr auto identityGridFunction(const GridPart& gridPart, IndexConstant<IndeterminateId> id = IndexConstant<IndeterminateId>{})
      {
        return gridFunction(gridPart, [](auto&& x) { return x; }, 2_c, id);
      }

    } // NS GridFunction

    using GridFunction::identityGridFunction;

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_FUNCTIONS_MODULES_IDENTITY_HH__
