#ifndef __DUNE_ACFEM_FUNCTIONS_PLACEHOLDERS_LOCALFUNCTIONPLACEHOLDER_HH__
#define __DUNE_ACFEM_FUNCTIONS_PLACEHOLDERS_LOCALFUNCTIONPLACEHOLDER_HH__

#include <dune/fem/function/localfunction/const.hh>

#include "../policy.hh"
#include "../localfunctiontraits.hh"
#include "../../tensors/operations/placeholder.hh"
#include "../../tensors/bindings/dune/fieldtensor.hh"

namespace Dune {

  namespace ACFem {

    namespace GridFunction {

      using namespace Literals;
      using Tensor::PlaceholderTensor;
      using Tensor::FieldVectorTensor;
      using Tensor::ScalarDecayFieldTensor;

      /**@addtogroup GridFunctions
       * @{
       */

      /**A placeholder tensor which wraps a Dune::Fem LocalFunction.
       *
       * @param LocalFunction The local function which provides the values.
       *
       * @param IndeterminateId The id of the indeterminate tensor the
       * placeholder is assumed to depend on. The derivative method is
       * overloaded such that the derivative w.r.t. to IndeterminateId
       * will generate the placeholder tensor which takes its values
       * from the jacobian() method of the wrapped LocalFunction.
       */
      template<class LocalFunction, std::size_t IndeterminateId = Policy::indeterminateId()>
      class LocalFunctionPlaceholder
        : public PlaceholderTensor<ScalarDecayFieldTensor<typename std::decay_t<LocalFunction>::RangeType>, LocalFunctionPlaceholder<LocalFunction, IndeterminateId> >
      {
        using ThisType = LocalFunctionPlaceholder;
        using BaseType = PlaceholderTensor<ScalarDecayFieldTensor<typename std::decay_t<LocalFunction>::RangeType>, ThisType>;
       public:
        using LocalFunctionType = std::decay_t<LocalFunction>;
        using GridFunctionType = typename LocalFunctionType::GridFunctionType;
        using RangeType = typename LocalFunctionType::RangeType;
        using DomainType = typename LocalFunctionType::DomainType;
        using TensorType = ScalarDecayFieldTensor<RangeType>;
        using GridPartType = typename LocalFunctionType::GridPartType;
        using EntityType = typename LocalFunctionType::EntityType;

        static_assert(!RefersConst<LocalFunction>::value,
                      "LocalFunction placeholders cannot be built from const qualified local functions");

        static constexpr std::size_t indeterminateId_ = IndeterminateId;

        using BaseType::operand;

        template<class Arg, std::enable_if_t<std::is_constructible<LocalFunctionType, Arg>::value, int> = 0>
        LocalFunctionPlaceholder(Arg&& localFunction)
          : BaseType(TensorType(), Expressions::Functor<BaseType>{})
          , localFunction_(std::forward<Arg>(localFunction))
        {}

        constexpr unsigned int order() const
        {
          return localFunction_.gridFunction().order();
        }

        /**Trigger the update of the stored values by evaluating the
         * wrapped localfunction. Calling setValue() without binding
         * the wrapped LocalFunction to an entity by calling the
         * bind() method may cause runtime errors.
         *
         * @param[in] Point
         */
        template<class Point,
                 std::enable_if_t<(IsQuadraturePoint<Point>::value || IsFieldVector<Point>::value), int> = 0>
        void setValue(const Point& point)
        {
          assert(localFunction_.entity() != EntityType());
          operand(0_c) = localFunction_.evaluate(point);
        }

        /**Bind the wrapped LocalFunction to the given entity.
         *
         * @param entity The entity to bind to.
         */
        void bind(const EntityType& entity)
        {
          localFunction_.bind(entity);
        }

        /**Unbind the wrapped LocalFunction from any Entity it is
         * bound to.
         */
        void unbind()
        {
          localFunction_.unbind();
        }

        /**Return the wrapped LocalFunction while maintaining its
         * reference and const qualification.
         */
        LocalFunction localFunction() &&
        {
          return localFunction_;
        }

        /**Return the wrapped LocalFunction as a mutable reference.*/
        auto& localFunction() &
        {
          return localFunction_;
        }

        /**Return the wrapped LocalFunction as a const reference.*/
        const auto& localFunction() const&
        {
          return localFunction_;
        }

        /**Return the wrapped grid-function.*/
        const auto& gridFunction() const
        {
          return localFunction_.gridFunction();
        }

        /**Return the gridPart of the wrapped function.*/
        const auto& gridPart() const
        {
          return localFunction_.gridFunction().gridPart();
        }

        /**Return the id of the indeterminate the placeholder is
         * assumed to depend on.
         */
        static constexpr auto indeterminateId()
        {
          return IndexConstant<indeterminateId_>::value;
        }

        std::string name() const
        {
          // use gridFunction name
          return localFunction_.gridFunction().name() + "(X["+toString(indeterminateId_)+"])";
        }

       private:
        LocalFunction localFunction_;
      };

      /**std::true_type if T is a LocalFunctionPlaceholder. Reference
       * arguments are allowed and will recurse to the traits struct
       * for their decay-type.
       */
      template<class T>
      struct IsLocalFunctionPlaceholder
        : FalseType
      {};

      template<class T>
      struct IsLocalFunctionPlaceholder<T&>
        : IsLocalFunctionPlaceholder<std::decay_t<T> >
      {};

      template<class T>
      struct IsLocalFunctionPlaceholder<T&&>
        : IsLocalFunctionPlaceholder<std::decay_t<T> >
      {};

      template<class LocalFunction>
      struct IsLocalFunctionPlaceholder<LocalFunctionPlaceholder<LocalFunction> >
        : TrueType
      {};

      /**Generate a LocalFunctionPlaceholder for a Fem::ConstLocalFunction.
       *
       * @param[in] f The local function to wrap. cvr qualifications
       * are captured by the generated expressions.
       *
       * @param[in] id The id of the indeterminate tensor the
       * placeholder is assumed to depend on. The derivative method is
       * overloaded such that the derivative w.r.t. to IndeterminateId
       * will generate the placeholder tensor which takes its values
       * from the jacobian() method of the wrapped LocalFunction.
       *
       * @return A LocalFunctionPlaceholder tensor which stores f as
       * copy if it was an rvalue reference or as the respective
       * reference type if it was an lvalue reference type.
       */
      template<class F, std::size_t IndeterminateId = Policy::indeterminateId(), class Closure = Expressions::Closure,
               std::enable_if_t<IsConstLocalFunction<F>::value, int> = 0>
      auto localFunctionPlaceholder(F&& f, IndexConstant<IndeterminateId> id = IndexConstant<IndeterminateId>{}, Closure closure = Closure{})
      {
        static_assert(!RefersConst<F>::value, "F must not be const.");

        return closure(LocalFunctionPlaceholder<F, IndeterminateId>(std::forward<F>(f)));
      }

      /**Pipe F through constLocalFunction() if it is not already a Fem::ConstLocalFunction.*/
      template<class F, std::size_t IndeterminateId = Policy::indeterminateId(), class Closure = Expressions::Closure,
               std::enable_if_t<IsWrappableByConstLocalFunction<F>::value, int> = 0>
      auto localFunctionPlaceholder(F&& f, IndexConstant<IndeterminateId> id = IndexConstant<IndeterminateId>{}, Closure closure = Closure{})
      {
        return localFunctionPlaceholder(ensureConstLocalFunction(std::forward<F>(f)), id, Closure{});
      }

      ///@} GridFunctions

    } // NS GridFunction

    using GridFunction::IsLocalFunctionPlaceholder;
    using GridFunction::localFunctionPlaceholder;

  } // NS ACFem

  /**Specialize Dune::FieldTraits. This has to be done explicitly as
   * Dune::FieldTraits does not allow for an SFINAE dummy argument.
   */
  template<class LocalFunction, std::size_t IndeterminateId>
  struct FieldTraits<ACFem::GridFunction::LocalFunctionPlaceholder<LocalFunction, IndeterminateId> >
    : FieldTraits<ACFem::GridFunction::PlaceholderTensor<ACFem::GridFunction::FieldVectorTensor<typename std::decay_t<LocalFunction>::RangeType>, ACFem::GridFunction::LocalFunctionPlaceholder<LocalFunction, IndeterminateId> > >
  {};

} // NS Dune

#endif //  __DUNE_ACFEM_FUNCTIONS_PLACEHOLDERS_LOCALFUNCTIONPLACEHOLDER_HH__
