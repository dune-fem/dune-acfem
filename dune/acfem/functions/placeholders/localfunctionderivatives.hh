#ifndef __DUNE_ACFEM_FUNCTIONS_PLACEHOLDERS_LOCALFUNCTIONDERIVATIVES_HH__
#define __DUNE_ACFEM_FUNCTIONS_PLACEHOLDERS_LOCALFUNCTIONDERIVATIVES_HH__

#include "../../tensors/operations/derivative.hh"

#include "localfunctionplaceholder.hh"
#include "localfunctionjacobianplaceholder.hh"
#include "localfunctionhessianplaceholder.hh"

namespace Dune {

  namespace ACFem {

    namespace GridFunction {

      /**@addtogroup GridFunctions
       * @{
       */

      /**Geneate the apropriate LocalFunctionJacobianPlaceholder if
       * differentiation w.r.t. the correct indetermiante id is
       * attempted.
       */
      template<
        std::size_t Id, class Signature, class T,
        std::enable_if_t<(IsLocalFunctionPlaceholder<T>::value
                          && Id == std::decay_t<T>::indeterminateId()
        ), int> = 0>
      constexpr auto doDerivative(T&& t, PriorityTag<1>)
      {
        return localFunctionJacobianPlaceholder(
          std::forward<T>(t).localFunction(),
          IndexConstant<Id>{},
          Expressions::Disclosure{}
          );
      }

      /**Geneate the apropriate LocalFunctionHessianPlaceholder if
       * differentiation w.r.t. the correct indetermiante id is
       * attempted.
       */
      template<
        std::size_t Id, class Signature, class T,
        std::enable_if_t<(IsLocalFunctionJacobianPlaceholder<T>::value
                          && Id == std::decay_t<T>::indeterminateId()
        ), int> = 0>
      constexpr auto doDerivative(T&& t, PriorityTag<1>)
      {
        return localFunctionHessianPlaceholder(
          std::forward<T>(t).localFunction(),
          IndexConstant<Id>{},
          Expressions::Disclosure{}
          );
      }

#if 0
      /**Generate an error if computation of higher derivatives is
       * attempted.
       */
      template<
        std::size_t Id, class Signature, class T,
        std::enable_if_t<(IsLocalFunctionHessianPlaceholder<T>::value
                          && Id == std::decay_t<T>::indeterminateId()
        ), int> = 0>
      constexpr auto doDerivative(T&& t, PriorityTag<1>)
      {
        static_assert(IsLocalFunctionHessianPlaceholder<T>::value && false,
                      "Only up to second derivatives are provided by Dune::Fem functions.");
      }
#endif

      ///@} GridFunctions

    } // NS GridFunction

    namespace Tensor {

      using GridFunction::doDerivative;

    }

  } // NS ACFem

} // NS Dune

#endif //  __DUNE_ACFEM_FUNCTIONS_PLACEHOLDERS_LOCALFUNCTIONDERIVATIVES_HH__
