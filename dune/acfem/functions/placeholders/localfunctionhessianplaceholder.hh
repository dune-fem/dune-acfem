#ifndef __DUNE_ACFEM_FUNCTIONS_PLACEHOLDERS_LOCALFUNCTIONHESSIANPLACEHOLDER_HH__
#define __DUNE_ACFEM_FUNCTIONS_PLACEHOLDERS_LOCALFUNCTIONHESSIANPLACEHOLDER_HH__

#include "../policy.hh"
#include "../localfunctiontraits.hh"
#include "../../tensors/operations/indeterminate.hh"
#include "../../tensors/operations/placeholder.hh"
#include "../../tensors/bindings/dune/fieldtensor.hh"

namespace Dune {

  namespace ACFem {

    namespace GridFunction {

      using namespace Literals;
      using Tensor::PlaceholderTensor;
      using Tensor::FieldVectorTensor;
      using Tensor::ScalarDecayFieldTensor;

      /**@addtogroup GridFunctions
       * @{
       */

      template<class LocalFunction, std::size_t IndeterminateId = Policy::indeterminateId()>
      class LocalFunctionHessianPlaceholder
        : public PlaceholderTensor<ScalarDecayFieldTensor<typename std::decay_t<LocalFunction>::HessianRangeType>, LocalFunctionHessianPlaceholder<LocalFunction, IndeterminateId> >
      {
        using ThisType = LocalFunctionHessianPlaceholder;
        using BaseType = PlaceholderTensor<ScalarDecayFieldTensor<typename std::decay_t<LocalFunction>::HessianRangeType>, ThisType>;
       public:
        using LocalFunctionType = std::decay_t<LocalFunction>;
        using DomainType = typename LocalFunctionType::DomainType;
        using HessianRangeType = typename LocalFunctionType::HessianRangeType;
        using TensorType = ScalarDecayFieldTensor<HessianRangeType>;
        using GridPartType = typename LocalFunctionType::GridPartType;
        using EntityType = typename LocalFunctionType::EntityType;

        static constexpr std::size_t indeterminateId_ = IndeterminateId;

        using BaseType::operand;

        template<class Arg, std::enable_if_t<std::is_constructible<LocalFunction, Arg>::value, int> = 0>
        LocalFunctionHessianPlaceholder(Arg&& localFunction)
          : BaseType(TensorType(), Expressions::Functor<BaseType>{})
          , localFunction_(std::forward<Arg>(localFunction))
        {}

        constexpr unsigned int order() const
        {
          return localFunction_.gridFunction().order() - 2;
        }

        template<class Point,
                 std::enable_if_t<(IsQuadraturePoint<Point>::value || IsFieldVector<Point>::value), int> = 0>
        void setValue(const Point& point)
        {
          assert(localFunction_.entity() != EntityType());
          if constexpr ((TensorTraits<HessianRangeType>::rank - TensorTraits<TensorType>::rank) == 1
                          && TensorTraits<HessianRangeType>::template dim<0>() == 1)
            operand(0_c) = localFunction_.hessian(point)[0];
          else
            operand(0_c) = localFunction_.hessian(point);
        }

        void bind(const EntityType& entity)
        {
          localFunction_.bind(entity);
        }

        void unbind()
        {
          localFunction_.unbind();
        }

        LocalFunction localFunction() &&
        {
          return localFunction_;
        }

        auto& localFunction() &
        {
          return localFunction_;
        }

        const auto& localFunction() const&
        {
          return localFunction_;
        }

        /**Return the gridPart of the wrapped function.*/
        const auto& gridPart() const
        {
          return localFunction_.gridFunction().gridPart();
        }

        static constexpr std::size_t indeterminateId()
        {
          return indeterminateId_;
        }

        std::string name() const
        {
          // use gridfunction name
          return "D2"+localFunction_.gridFunction().name() + "(X["+toString(indeterminateId_)+"])";
        }

       private:
        LocalFunction localFunction_;
      };

      template<class T, class SFINAE = void>
      struct IsLocalFunctionHessianPlaceholder
        : FalseType
      {};

      template<class T>
      struct IsLocalFunctionHessianPlaceholder<T, std::enable_if_t<!IsDecay<T>::value> >
        : IsLocalFunctionHessianPlaceholder<std::decay_t<T> >
      {};

      template<class LocalFunction, std::size_t IndeterminateId>
      struct IsLocalFunctionHessianPlaceholder<LocalFunctionHessianPlaceholder<LocalFunction, IndeterminateId> >
        : TrueType
      {};

      template<class F,
               std::size_t IndeterminateId = Policy::indeterminateId(),
               class Closure = Expressions::Closure,
               std::enable_if_t<IsConstLocalFunction<F>::value, int> = 0>
      auto localFunctionHessianPlaceholder(F&& f, IndexConstant<IndeterminateId> id = IndexConstant<IndeterminateId>{}, Closure closure = Closure{})
      {
        return closure(LocalFunctionHessianPlaceholder<F, IndeterminateId>(std::forward<F>(f)));
      }

      template<class F,
               std::size_t IndeterminateId = Policy::indeterminateId(),
               class Closure = Expressions::Closure,
               std::enable_if_t<IsWrappableByConstLocalFunction<F>::value, int> = 0>
      auto localFunctionHessianPlaceholder(F&& f, IndexConstant<IndeterminateId> id = IndexConstant<IndeterminateId>{}, Closure = Closure{})
      {
        return localFunctionHessianPlaceholder(ensureConstLocalFunction(std::forward<F>(f)), id, Closure{});
      }

      ///@} GridFunctions

    } // NS GridFunction

  } // NS ACFem

  /**Specialize Dune::FieldTraits. This has to be done explicitly as
   * Dune::FieldTraits does not allow for an SFINAE dummy argument.
   */
  template<class LocalFunction, std::size_t IndeterminateId>
  struct FieldTraits<ACFem::GridFunction::LocalFunctionHessianPlaceholder<LocalFunction, IndeterminateId> >
    : FieldTraits<ACFem::GridFunction::PlaceholderTensor<ACFem::Tensor::FieldVectorTensor<typename std::decay_t<LocalFunction>::HessianRangeType>, ACFem::GridFunction::LocalFunctionHessianPlaceholder<LocalFunction, IndeterminateId> > >
  {};

} // NS Dune

#endif //  __DUNE_ACFEM_FUNCTIONS_PLACEHOLDERS_LOCALFUNCTIONHESSIANPLACEHOLDER_HH__
