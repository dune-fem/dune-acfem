#ifndef __DUNE_ACFEM_FUNCTIONS_PLACEHOLDERS_ELEMENTMEASURE_HH__
#define __DUNE_ACFEM_FUNCTIONS_PLACEHOLDERS_ELEMENTMEASURE_HH__

#include "../../tensors/operations/placeholder.hh"
#include "../../tensors/bindings/dune/fieldtensor.hh"

namespace Dune {

  namespace ACFem {

    namespace GridFunction {

      using namespace Literals;
      using Tensor::PlaceholderTensor;
      using Tensor::FieldVectorTensor;

      /**@addtogroup GridFunctions
       * @{
       */

      /**A placeholder tensor which evaluates to the measure of the
       * element after binding it.
       *
       * @param GridPart The GridPart of the elements. The GridPart
       * determines the EntityType and the FieldType.
       */
      template<class GridPart>
      class ElementMeasurePlaceholder
        : public PlaceholderTensor<FieldVectorTensor<typename GridPart::ctype>, ElementMeasurePlaceholder<GridPart> >
      {
       public:
        using TensorType = FieldVectorTensor<typename GridPart::ctype>;
        using EntityType = typename GridPart::template Codim<0>::EntityType;
       private:
        using ThisType = ElementMeasurePlaceholder;
        using BaseType = PlaceholderTensor<TensorType, ThisType>;
       public:
        using GridPartType = GridPart;
        using BaseType::operator();

        /**Construct an element-measure placeholder from the given
         * grid-part.
         */
        ElementMeasurePlaceholder(const GridPart& gridPart)
          : BaseType(TensorType(-1.0), Expressions::Functor<BaseType>{})
          , gridPart_(gridPart)
        {}

        /**Bind the wrapped LocalFunction to the given entity.
         *
         * @param entity The entity to bind to.
         */
        void bind(const EntityType& entity)
        {
          (*this)() = entity.geometry().volume();
        }

        /**Return the gridPart. Used for construction.*/
        const GridPartType& gridPart() const
        {
          return gridPart_;
        }

        static std::string name()
        {
          return "|E|";
        }

       private:
        const GridPartType& gridPart_;
      };

      template<class GridPart>
      auto elementMeasurePlaceholder(const GridPart& gridPart)
      {
        return expressionClosure(ElementMeasurePlaceholder<GridPart>(gridPart));
      }

      ///@} GridFunctions

    } // NS GridFunction

  } // NS ACFem

  /**Specialize Dune::FieldTraits. This has to be done explicitly as
   * Dune::FieldTraits does not allow for an SFINAE dummy argument.
   */
  template<class GridPart>
  struct FieldTraits<ACFem::GridFunction::ElementMeasurePlaceholder<GridPart> >
    : FieldTraits<ACFem::GridFunction::PlaceholderTensor<ACFem::GridFunction::FieldVectorTensor<typename GridPart::ctype>, ACFem::GridFunction::ElementMeasurePlaceholder<GridPart> > >
  {};

} // NS Dune

#endif //  __DUNE_ACFEM_FUNCTIONS_PLACEHOLDERS_ELEMENTMEASURE_HH__
