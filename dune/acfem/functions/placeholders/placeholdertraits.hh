#ifndef __DUNE_ACFEM_FUNCTIONS_PLACEHOLDERS_PLACEHOLDERTRAITS_HH__
#define __DUNE_ACFEM_FUNCTIONS_PLACEHOLDERS_PLACEHOLDERTRAITS_HH__

#include "../../common/types.hh"
#include "../../common/quadraturepoint.hh"

namespace Dune {

  namespace ACFem {

    namespace GridFunction {

      template<class Placeholder, class SFINAE = void>
      struct HasBind
        : FalseType
      {};

      /**@internal std::true_type if bind() is implemented.*/
      template<class Placeholder>
      struct HasBind<
        Placeholder,
        VoidType<decltype(std::declval<Placeholder>().bind(std::declval<typename std::decay_t<Placeholder>::EntityType>()))> >
        : TrueType
      {};

      template<class Placeholder>
      constexpr inline bool hasBind(Placeholder&&)
      {
        return HasBind<Placeholder>::value;
      }

      template<class Placeholder, class SFINAE = void>
      struct HasUnbind
        : FalseType
      {};

      /**@internal std::true_type if unbind() is implemented.*/
      template<class Placeholder>
      struct HasUnbind<
        Placeholder,
        VoidType<decltype(std::declval<Placeholder>().unbind())> >
        : TrueType
      {};

      template<class Placeholder>
      constexpr inline bool hasUnbind(Placeholder&&)
      {
        return HasUnbind<Placeholder>::value;
      }

      template<class Placeholder, class SFINAE = void>
      struct HasSetValue
        : FalseType
      {};

      /**@internal std::true_type if setValue() is implemented.*/
      template<class Placeholder>
      struct HasSetValue<
        Placeholder,
        VoidType<decltype(std::declval<Placeholder>().setValue(std::declval<PlaceholderQuadraturePoint<typename std::decay_t<Placeholder>::DomainType> >() ) ) >
        >
        : TrueType
      {};

      template<class Placeholder>
      constexpr bool hasSetValue(Placeholder&&)
      {
        return HasSetValue<Placeholder>::value;
      }

      template<class Placeholder, class SFINAE = void>
      struct HasIndeterminateSetValue
        : FalseType
      {};

      /**@internal std::true_type if setValue(Indeterminate) is implemented.*/
      template<class Placeholder>
      struct HasIndeterminateSetValue<
        Placeholder,
        VoidType<decltype(std::declval<Placeholder>().setValue(std::declval<typename std::decay_t<Placeholder>::IndeterminateType>()))> >
        : TrueType
      {};

      template<class Placeholder>
      constexpr inline bool hasIndeterminateSetValue(Placeholder&&)
      {
        return HasIndeterminateSetValue<Placeholder>::value;
      }

    } // GridFunction

  } // ACFem

} // Dune

#endif // __DUNE_ACFEM_FUNCTIONS_PLACEHOLDERS_PLACEHOLDERTRAITS_HH__
