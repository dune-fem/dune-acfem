#include <config.h>

#include "../../../common/scopedredirect.hh"

#include <iostream>
#include <sstream>

#include <dune/common/exceptions.hh>
#include <dune/fem/io/parameter.hh>

#include <dune/fem/gridpart/adaptiveleafgridpart.hh>
// Helper to get to the reference element with reasonable effort
#include <dune/fem/space/common/allgeomtypes.hh>

#include "../../../common/geometry.hh"
#include "../../../common/gridfunctionspace.hh"
#include "../../../common/discretefunctionselector.hh"

#include "../elementmeasure.hh"
#include "../localfunctionplaceholder.hh"
#include "../localfunctionjacobianplaceholder.hh"
#include "../localfunctionhessianplaceholder.hh"
#include "../localfunctionderivatives.hh"
#include "../placeholdertraits.hh"

#include "../../../tensors/tensor.hh"

/**@addtogroup Tests
 * @{
 */

using namespace Dune::ACFem;
using namespace Dune::ACFem::GridFunction;

#ifndef TEST_DGF_DATA
# define TEST_DGF_DATA dgfSimplexData2d
#endif

//Embedded test DGF-"file" with a simple interval
const std::string dgfIntervalData2d(R"(DGF

Interval
 0   0
 1   1
 1   1
#

GridParameter
% longest or arbitrary (see DGF docu)
refinementedge longest
overlap 0
#

BoundaryDomain
default 3
1   0 0   1 0  % lower boundary
2   0 1   1 1  % upper boundary
#

#)");

//Embedded test DGF-"file" with some simplices
const std::string dgfSimplexData2d(R"(DGF

Vertex
 -1  -1
  1  -1
  1   1
 -1   1
  0   0
#

SIMPLEX
 0 1 4
 1 2 4
 2 3 4
 3 0 4
#

BoundarySegments
 1   0 1  : blah1  % right boundary
 2   1 2  : blah2  % upper boundary
 3   2 3  : blah3  % left boundary
 4   3 0  : blah4  % lower boundary
#
GridParameter
 % longest or arbitrary (see DGF docu)
 refinementedge longest
 % there is zarro information on this ... :(
 overlap 0
 % whatever this may be ...
 tolerance 1e-12
 % silence?
 verbose 0
#)");

namespace Dune {

  // ... in order to sort elements by their geometrical center
  template<class T, int N>
  bool operator<(const FieldVector<T, N>& a, const FieldVector<T, N>& b)
  {
    for(unsigned int i = 0; i < a.size(); ++i)
    {
      if(std::abs(a[i]-b[i]) > 1e-8)
      {
        return a[i] < b[i];
      }
    }
    return a.two_norm2() < b.two_norm2();
  }
}

template<class T, class Entity, std::enable_if_t<HasBind<T>::value, int> = 0>
void bind(T& t, const Entity& entity)
{
  t.bind(entity);
}

template<class T, std::enable_if_t<HasUnbind<T>::value, int> = 0>
void unbind(T& t)
{
  t.unbind();
}

template<class T, class Quadrature, std::enable_if_t<HasSetValue<T>::value, int> = 0>
void setValue(T& t, const QuadraturePoint<Quadrature>& pt)
{
  t.setValue(pt);
}

template<class T, class Entity, std::enable_if_t<!HasBind<T>::value, int> = 0>
void bind(T& t, const Entity& entity)
{}

template<class T, std::enable_if_t<!HasUnbind<T>::value, int> = 0>
void unbind(T& t)
{}

template<class T, class Quadrature, std::enable_if_t<!HasSetValue<T>::value, int> = 0>
void setValue(T& t, const QuadraturePoint<Quadrature>& pt)
{}

/**Test routine which is called with the GridFunctionExpression from
 * the main-program. We simply walk over the grid and evaluate the
 * local function and its jacobian on each element.
 */
template<class GridPart, class Placeholder>
void gridWalkTest(GridPart&& gridPart, Placeholder&& closure)
{
  using GridPartType = std::decay_t<GridPart>;
  using EntityType = typename GridPartType::template Codim<0>::EntityType;

  using FunctionSpaceType = VectorGridFunctionSpace<GridPartType>;
  //using ScalarFunctionSpaceType = ScalarGridFunctionSpace<GridPartType>;
  //using ScalarFunctionSpaceType = typename FunctionSpaceType::ScalarFunctionSpaceType;
  using DomainType = typename FunctionSpaceType::DomainType;

  std::map<DomainType, EntityType> leafElements;
  // Sort elements by center to make the traversal order below unique
  for (const auto& entity : elements(gridPart)) {
    leafElements[entity.geometry().center()] = entity;
  }

  using GeometryInformation = GeometryInformation<GridPartType>;

  std::clog << "*** GridWalk for " << closure.name() << std::endl;

  auto placeholder = asExpression(std::forward<Placeholder>(closure));
  auto x0 = Tensor::indeterminate<0>(DomainType{});

  std::clog << "  bind():     " << hasBind(placeholder) << std::endl;
  std::clog << "  unbind():   " << hasUnbind(placeholder) << std::endl;
  std::clog << "  setValue(): " << hasSetValue(placeholder) << std::endl;

  // Loop over the sorted elements.
  for (auto mapEl : leafElements) {
    const auto& entity = mapEl.second;

    // Local "center", whatever that might be
    const auto& localCenter(GeometryInformation::localCenter(entity.geometry().type()));

    std::clog << "Local Center:  " << localCenter << std::endl;
    std::clog << "Global Center: " << entity.geometry().center() << std::endl;

    bind(placeholder, entity);
    setValue(placeholder, *localCenter);

    std::clog << "Placeholder after Bind:" << std::endl;
    std::clog << placeholder << std::endl;

    std::clog << "Placeholder derivative after Bind:" << std::endl;
    std::clog << derivative(placeholder, x0) << std::endl;

    unbind(placeholder);

  } // ordered mesh loop

}

/**Test-template main program. Instantiate a single expression
 * template and evaluate it on a simple grid.
 */
int main(int argc, char *argv[])
{
  try {
    // General setup
    Dune::Fem::MPIManager::initialize(argc, argv);

    // append parameter
    Dune::Fem::Parameter::append(argc, argv);

    // append default parameter file
    Dune::Fem::Parameter::append(SRCDIR "/parameter");

    //redirect the stream cerr to cout
    //so we only get the output of clog on stderr
    ScopedRedirect redirect(std::cerr, std::cout);

    // reduce precision and use normalized FP output
    // std::clog << std::scientific << std::setprecision(4);
    std::clog << std::fixed << std::setprecision(8);

    // type of hierarchical grid
    typedef Dune::GridSelector::GridType HGridType;

    // the method rank and size from MPIManager are static
    if (Dune::Fem::MPIManager::rank() == 0) {
      std::clog << "Loading embedded macro grid: "
                << std::endl
                << "=================================" << std::endl
                << TEST_DGF_DATA
                << "=================================" << std::endl
                << std::endl;
    }

    // we try to be self-contained an simply load the embedded simple dgf-file
    std::istringstream dgfStream(TEST_DGF_DATA);

    // construct macro using the DGF Parser
    Dune::GridPtr<HGridType> gridPtr(dgfStream);
    HGridType& grid = *gridPtr;

    // do initial load balance
    grid.loadBalance();

    auto gridPart = leafGridPart<Dune::InteriorBorder_Partition>(grid);
    auto space = discreteFunctionSpace<HGridType::dimensionworld>(gridPart, lagrange<POLORDER>);
    auto Uh = discreteFunction(space, "Uh");

    auto dend(Uh.dend());
    for (auto it = Uh.dbegin(); it != dend; ++it) {
      *it = 1; // simply generate a constant function
    }
    ///////////////////// >//

    const auto testName = Dune::Fem::Parameter::getValue<std::string>("testName");

    std::clog << "*** Running test \"" << testName << "\" ***" << std::endl << std::endl;
    if (testName == "default") {
      gridWalkTest(gridPart, elementMeasurePlaceholder(gridPart));
    }
    else if (testName == "elementmeasure") {
      gridWalkTest(gridPart, elementMeasurePlaceholder(gridPart));
    }
    else if (testName == "localfunctionplaceholder") {
      gridWalkTest(gridPart, localFunctionPlaceholder(Uh));
    }
    else if (testName == "localfunctionjacobianplaceholder") {
      gridWalkTest(gridPart, localFunctionJacobianPlaceholder(Uh));
    }
    else if (testName == "localfunctionhessianplaceholder") {
      gridWalkTest(gridPart, localFunctionHessianPlaceholder(Uh));
    }
    else if (testName == "localfunctionderivatives") {
      auto x0 = Tensor::indeterminate<0>(decltype(Uh)::DomainType());
      std::clog << "Derivatives drawn from Uh" << std::endl;
      gridWalkTest(gridPart, Tensor::derivative(localFunctionPlaceholder(Uh), x0));
      gridWalkTest(gridPart, Tensor::derivative<2>(localFunctionPlaceholder(Uh), x0));
      // No 2nd derivative ;)
      // gridWalkTest(gridPart, Tensor::derivative<3>(localFunctionPlaceholder(Uh), x0));
      std::clog << "Derivatives drawn from DUh" << std::endl;
      gridWalkTest(gridPart, Tensor::derivative(localFunctionJacobianPlaceholder(Uh), x0));
      // std::clog << "Derivatives drawn from D^2Uh" << std::endl;
      // There is none ;)
      gridWalkTest(gridPart, Tensor::derivative(localFunctionHessianPlaceholder(Uh), x0));
      {
        auto expr = localFunctionPlaceholder(Uh) * x0;
        std::clog << "Derivatives drawn from " << expr.name() << std::endl;
        gridWalkTest(gridPart, Tensor::derivative(expr, x0));
        gridWalkTest(gridPart, Tensor::derivative<2>(expr, x0));
      }
      {
        auto expr = elementMeasurePlaceholder(gridPart);
        std::clog << "Derivatives drawn from " << expr.name() << std::endl;
        gridWalkTest(gridPart, Tensor::derivative(expr, x0));
      }
    }
    else {
      DUNE_THROW(Dune::NotImplemented, "Test \""+testName+"\" not implemented");
    }

    return EXIT_SUCCESS;
  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
    return EXIT_FAILURE;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
    return EXIT_FAILURE;
  }
}

//!@} Tests

//!@} GridFunctions
