#ifndef __DUNE_ACFEM_FUNCTIONS_GRIDFUNCTION_HH__
#define __DUNE_ACFEM_FUNCTIONS_GRIDFUNCTION_HH__

#include "../common/gridfunctionspace.hh"

#include "policy.hh"
#include "functiontraits.hh"
#include "bindabletensorfunction.hh"

namespace Dune {

  namespace ACFem {

    namespace GridFunction {

      /**@addtogroup GridFunctions
       * @{
       */

      /**Generate a BindableGridFunction which wraps a copy of T.*/
      template<class GridPart, class T, std::size_t MaxOrder = Policy::autoDiffLimit(), std::size_t IndeterminateId = Policy::indeterminateId(),
               std::enable_if_t<(IsTensor<T>::value && !IsClosure<T>::value), int> = 0>
      auto gridFunction(
        const GridPart& gridPart,
        T&& t,
        IndexConstant<MaxOrder> maxOrder = IndexConstant<MaxOrder>{},
        IndexConstant<IndeterminateId> id = IndexConstant<IndeterminateId>{})
      {
        return BindableTensorFunction<T, GridPart, MaxOrder, IndeterminateId>(std::forward<T>(t), gridPart);
      }

      /**@internal Closure trampoline in order to ease type-deduction.*/
      template<class GridPart, class T, std::size_t MaxOrder = Policy::autoDiffLimit(), std::size_t IndeterminateId = Policy::indeterminateId(),
               std::enable_if_t<(IsTensor<T>::value && IsClosure<T>::value), int> = 0>
      auto gridFunction(
        const GridPart& gridPart,
        T&& t,
        IndexConstant<MaxOrder> maxOrder = IndexConstant<MaxOrder>{},
        IndexConstant<IndeterminateId> id = IndexConstant<IndeterminateId>{})
      {
        return gridFunction(gridPart, Expressions::asExpression(std::forward<T>(t)), maxOrder, id);
      }

      /**@internal Non-tensor operand trampoline.*/
      template<class GridPart, class T, std::size_t MaxOrder = Policy::autoDiffLimit(), std::size_t IndeterminateId = Policy::indeterminateId(),
               std::enable_if_t<(Tensor::IsNonTensorTensorOperand<T>::value), int> = 0>
      auto gridFunction(
        const GridPart& gridPart,
        T&& t,
        IndexConstant<MaxOrder> maxOrder = IndexConstant<MaxOrder>{},
        IndexConstant<IndeterminateId> id = IndexConstant<IndeterminateId>{})
      {
        return gridFunction(gridPart, Tensor::tensor(std::forward<T>(t)), maxOrder, id);
      }

      /**Generate a BindableGridFunction which wraps a copy of T. This
       * version is for the case that the to-be-wrapped tensor
       * expression contains placeholders with grid-parts.
       */
      template<class T,
               std::size_t MaxOrder = Policy::autoDiffLimit(),
               std::size_t IndeterminateId = Policy::indeterminateId(),
               std::enable_if_t<(IsTensor<T>::value && IsGridPartProvider<T>::value), int> = 0>
      auto gridFunction(
        T&& t,
        IndexConstant<MaxOrder> maxOrder = IndexConstant<MaxOrder>{},
        IndexConstant<IndeterminateId> id = IndexConstant<IndeterminateId>{})
      {
        return gridFunction(gridPart(t), std::forward<T>(t), maxOrder, id);
      }

      /**Generate a BindableGridFunction from a cooked lambda.
       *
       * @param gridPart The grid the function will live on.
       *
       * @param f A callable. @a f must accept a single argument which
       * is a placeholder for the indeterminate, e.g. in order to
       * generate a function which represents the Euclidean norm one
       * could code like follows:
       *
       * @code
       * auto myFct = gridFunction(gridPart,[](auto&& X) {
       *     return sqrt(inner(X, X));
       *   });
       * @endcode
       *
       * @param MaxOrder The maximum order of FAD (Forward Auto Diff)
       * derivatives provided.
       *
       * @param IndeterminateId The id of the indeterminate for the
       * "world" variable. Defaults to 0.
       */
      template<class GridPart, class F,
               std::size_t MaxOrder = Policy::autoDiffLimit(),
               std::size_t IndeterminateId = Policy::indeterminateId(),
               std::enable_if_t<(   !IsTensor<GridPart>::value
                                 && !IsTensorOperand<F>::value
                                 && !IsTensorFunction<F>::value
                                 && !IsWrappableByConstLocalFunction<F>::value
                                 && !IsWrappableByConstLocalFunction<GridPart>::value
        ), int> = 0>
      constexpr auto gridFunction(const GridPart& gridPart, F&& f,
                                  IndexConstant<MaxOrder> = IndexConstant<MaxOrder>{},
                                  IndexConstant<IndeterminateId> = IndexConstant<IndeterminateId>{})
      {
        using FunctionSpace = ScalarGridFunctionSpace<GridPart>;
        using DomainType = typename FunctionSpace::DomainType;
        return gridFunction(
          gridPart,
          f(indeterminate<IndeterminateId>(DomainType{})),
          IndexConstant<MaxOrder>{},
          IndexConstant<IndeterminateId>{}
          );
      }

      template<class GridPart, class Fct,
               std::size_t MaxOrder,
               std::size_t IndeterminateId = std::decay_t<Fct>::indeterminateId_,
               std::enable_if_t<IsTensorFunction<Fct>::value, int> = 0>
      constexpr decltype(auto) gridFunction(const GridPart& gridPart, Fct&& fct,
                                            IndexConstant<MaxOrder> maxOrder,
                                            IndexConstant<IndeterminateId> id = IndexConstant<IndeterminateId>{})
      {
        static_assert(IndeterminateId == std::decay_t<Fct>::indeterminateId_,
                      "Can only rewrap grid-function with the same indetermiante id.");

        if constexpr (std::decay_t<Fct>::requestedADOrder_ == MaxOrder) {
          return std::forward<Fct>(fct);
        } else {
          return gridFunction(gridPart, std::forward<Fct>(fct).expression(), maxOrder, id);
        }
      }

      template<class Fct,
               std::size_t MaxOrder,
               std::enable_if_t<IsTensorFunction<Fct>::value, int> = 0>
      constexpr decltype(auto) gridFunction(Fct&& fct, IndexConstant<MaxOrder> maxOrder)
      {
        return gridFunction(fct.gridPart(), std::forward<Fct>(fct).expression(), maxOrder, fct.indeterminateId());
      }

      template<class Fct,
               std::size_t MaxOrder = Policy::autoDiffLimit(), std::size_t IndeterminateId = Policy::indeterminateId(),
               std::enable_if_t<!IsTensorFunction<Fct>::value && IsWrappableByConstLocalFunction<Fct>::value, int> = 0>
        constexpr decltype(auto) gridFunction(
          Fct&& fct,
          IndexConstant<MaxOrder> maxOrder = IndexConstant<MaxOrder>{},
          IndexConstant<IndeterminateId> id = IndexConstant<IndeterminateId>{})
      {
        return gridFunction(fct.gridPart(), localFunctionPlaceholder(std::forward<Fct>(fct)), maxOrder, id);
      }

      ///@} GridFunctions

    } // NS GridFunction

    using GridFunction::gridFunction;

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_FUNCTIONS_GRIDFUNCTION_HH__
