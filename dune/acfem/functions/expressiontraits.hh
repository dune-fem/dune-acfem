#ifndef __DUNE_ACFEM_FUNCTIONS_EXPRESSION_TRAITS_HH__
#define __DUNE_ACFEM_FUNCTIONS_EXPRESSION_TRAITS_HH__

#include "../expressions/complexity.hh"
#include "functiontraits.hh"
#include "bindabletensorfunction.hh"

namespace Dune {

  namespace ACFem {

    template<class Expr, class GridPart, std::size_t AutoDiffOrder, std::size_t IndeterminateId>
    struct ExpressionTraits<GridFunction::BindableTensorFunction<Expr, GridPart, AutoDiffOrder, IndeterminateId> >
      : ExpressionTraits<Expr>
    {
      using ExpressionType = GridFunction::BindableTensorFunction<Expr, GridPart, AutoDiffOrder, IndeterminateId>;
    };

    namespace Expressions {

      template<class T>
      constexpr inline std::size_t ComplexityV<T, std::enable_if_t<IsTensorFunction<T>::value> > =
        ComplexityV<typename std::decay_t<T>::ValuesType>;

    }
  }

}

#endif // __DUNE_ACFEM_FUNCTIONS_EXPRESSION_TRAITS_HH__
