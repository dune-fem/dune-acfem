#ifndef __DUNE_ACFEM_FUNCTIONS_BASICFUNCTIONS_HH__
#define __DUNE_ACFEM_FUNCTIONS_BASICFUNCTIONS_HH__

#include "functiontraits.hh"
#include "localfunctiontraits.hh"
#include "modules/elementmeasure.hh"
#include "modules/identity.hh"
#include "modules/meshpenalty.hh"
#include "modules/modeldirichletvalues.hh"
#include "modules/one.hh"
#include "modules/zero.hh"

namespace Dune {

  namespace ACFem {

    namespace GridFunction {

    } // NS GridFunction

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_FUNCTIONS_BASICFUNCTIONS_HH__
