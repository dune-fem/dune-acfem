#ifndef __DUNE_ACFEM_FUNCTIONS_EXPRESSIONS_HH__
#define __DUNE_ACFEM_FUNCTIONS_EXPRESSIONS_HH__

#include "../expressions/operandpromotion.hh"
#include "functiontraits.hh"
#include "expressiontraits.hh"
#include "gridfunction.hh"
#include "operandpromotion.hh"

namespace Dune::ACFem {

  namespace GridFunction {

    using Tensor::Seq;

    // Operator Argument Traits
    // ========================

    template<class T>
    struct IsOperand
      : BoolConstant<(IsTensorFunction<T>::value && !Expressions::IsPromotedTopLevel<T>::value)>
    {};

    template<class T>
    using IsUnaryOperand = IsOperand<T>;

    template<class T0, class T1>
    struct AreBinaryOperands
      : BoolConstant<IsUnaryOperand<T0>::value && IsUnaryOperand<T1>::value>
    {};

    template<class Optimize, class F, class T0, class... Tk,
             std::enable_if_t<(IsOperand<T0>::value && ... && IsOperand<Tk>::value), int> = 0>
    constexpr decltype(auto) operate(Optimize, F&& f, T0&& t0, Tk&&... tk)
    {
      DUNE_ACFEM_RECORD_OPTIMIZATION;

      return gridFunction(
        t0.gridPart(),
        finalize(Optimize{},
                 std::forward<F>(f),
                 std::forward<T0>(t0).expression(),
                 std::forward<Tk>(tk).expression()...
          ),
        IndexConstant<min(IndexSequence<autoDiffOrder<T0>(), autoDiffOrder<Tk>()...>{})>{},
        t0.indeterminateId()
        );
    }

    template<class T0, class T1, std::enable_if_t<IsUnaryOperand<T0>::value || IsUnaryOperand<T1>::value, int> = 0>
    constexpr auto multiplyScalars(T0&& t0, T1&& t1)
    {
      return std::forward<T0>(t0) * std::forward<T1>(t1);
    }

    // Unary Operands
    // ==============

    template<class T, std::enable_if_t<IsUnaryOperand<T>::value, int> = 0>
    auto operator-(T&& t)
    {
      return gridFunction(
        t.gridPart(),
        -std::forward<T>(t).expression(),
        t.autoDiffOrder(),
        t.indeterminateId()
        );
    }

    // Binary Operands
    // ===============

    /**Add two tensor functions.*/
    template<class T0, class T1,
             std::enable_if_t<AreBinaryOperands<T0, T1>::value, int> = 0>
    auto operator+(T0&& t0, T1&& t1)
    {
      static_assert(std::decay_t<T0>::indeterminateId() == std::decay_t<T1>::indeterminateId(),
                    "Indeterminate id's do not match");
      assert(&t0.gridPart() == &t1.gridPart());

      return gridFunction(
        t0.gridPart(),
        std::forward<T0>(t0).expression() + std::forward<T1>(t1).expression(),
        IndexConstant<std::min(autoDiffOrder<T0>(), autoDiffOrder<T1>())>{},
        t0.indeterminateId()
        );
    }

    /**Subtract two tensor functions.*/
    template<class T0, class T1, std::enable_if_t<AreBinaryOperands<T0, T1>::value, int> = 0>
    auto operator-(T0&& t0, T1&& t1)
    {
      static_assert(std::decay_t<T0>::indeterminateId() == std::decay_t<T1>::indeterminateId(),
                    "Indeterminate id's do not match");
      assert(&t0.gridPart() == &t1.gridPart());

      return gridFunction(
        t0.gridPart(),
        std::forward<T0>(t0).expression() - std::forward<T1>(t1).expression(),
        IndexConstant<std::min(autoDiffOrder<T0>(), autoDiffOrder<T1>())>{},
        t0.indeterminateId());
    }

    /**Multiply two tensor functions.*/
    template<class T0, class T1, std::enable_if_t<AreBinaryOperands<T0, T1>::value, int> = 0>
    auto operator*(T0&& t0, T1&& t1)
    {
      static_assert(std::decay_t<T0>::indeterminateId() == std::decay_t<T1>::indeterminateId(),
                    "Indeterminate id's do not match");
      assert(&t0.gridPart() == &t1.gridPart());

      return gridFunction(
        t0.gridPart(),
        std::forward<T0>(t0).expression() * std::forward<T1>(t1).expression(),
        IndexConstant<std::min(autoDiffOrder<T0>(), autoDiffOrder<T1>())>{},
        t0.indeterminateId()
        );
    }

    /**Divide two tensor functions.*/
    template<class T0, class T1, std::enable_if_t<AreBinaryOperands<T0, T1>::value, int> = 0>
    auto operator/(T0&& t0, T1&& t1)
    {
      static_assert(std::decay_t<T0>::indeterminateId() == std::decay_t<T1>::indeterminateId(),
                    "Indeterminate id's do not match");
      assert(&t0.gridPart() == &t1.gridPart());

      return gridFunction(
        t0.gridPart(),
        std::forward<T0>(t0).expression() / std::forward<T1>(t1).expression(),
        IndexConstant<std::min(autoDiffOrder<T0>(), autoDiffOrder<T1>())>{},
        t0.indeterminateId()
        );
    }

    /**@name TensorOperations
     *
     * @{
     */

    /**Regular einsum() with given contraction dimensions.*/
    template<class T0Seq, class T1Seq, class T0, class T1,
             std::enable_if_t<AreBinaryOperands<T0, T1>::value, int> = 0>
    auto einsum(T0&& t0, T1&& t1)
    {
      static_assert(std::decay_t<T0>::indeterminateId() == std::decay_t<T1>::indeterminateId(), "Indeterminates have to match.");
      return gridFunction(
        t0.gridPart(),
        einsum<T0Seq, T1Seq>(std::forward<T0>(t0).expression(), std::forward<T1>(t1).expression()),
        IndexConstant<std::min(autoDiffOrder<T0>(), autoDiffOrder<T1>())>{},
        t0.indeterminateId()
        );
    }

    /**Single-argument is the contraction with the "eye" tensor.*/
    template<class T, std::enable_if_t<IsUnaryOperand<T>::value, int> = 0>
    auto einsum(T&& t)
    {
      return gridFunction(
        t.gridPart(),
        einsum(std::forward<T>(t).expression()),
        t.autoDiffOrder(),
        t.indetermianteId()
        );
    }

    /**This is the greedy contract-as-much-as-possible version,
     * summing over all leading indices with equal dimensions.
     *
     * @param[in] left Left argument.
     *
     * @param[in] right Right argument.
     *
     * @return Contracted tensor, contraction runs over all leading
     * indices with equal dimensions.
     */
    template<class T0, class T1, std::enable_if_t<AreBinaryOperands<T0, T1>::value, int> = 0>
    auto einsum(T0&& t0, T1&& t1)
    {
      static_assert(std::decay_t<T0>::indeterminateId() == std::decay_t<T1>::indeterminateId(), "Indeterminates have to match.");
      return gridFunction(
        t0.gridPart(),
        einsum(std::forward<T0>(t0).expression(), std::forward<T1>(t1).expression()),
        IndexConstant<std::min(autoDiffOrder<T0>(), autoDiffOrder<T1>())>{},
        t0.indeterminateId()
        );
    }

    /**Compute the trace of the given tensor-valued function.*/
    template<class T, std::enable_if_t<IsUnaryOperand<T>::value, int> = 0>
    auto trace(T&& t)
    {
      return gridFunction(
        t.gridPart(),
        trace(std::forward<T>(t).expression()),
        t.autoDiffOrder(),
        t.indeterminateId()
        );
    }

    /**Contraction over all indices, this is just the generalization
     * of the scalar product.
     */
    template<class T0, class T1, std::enable_if_t<AreBinaryOperands<T0, T1>::value, int> = 0>
    auto inner(T0&& t0, T1&& t1)
    {
      static_assert(std::decay_t<T0>::signature() == std::decay_t<T1>::signature(),
                    "Tensor signature has to coincide for inner product.");
      return gridFunction(
        t0.gridPart(),
        inner(std::forward<T0>(t0).expression(), std::forward<T1>(t1).expression()),
        IndexConstant<std::min(autoDiffOrder<T0>(), autoDiffOrder<T1>())>{},
        t0.indeterminateId()
        );
    }

    /**Compute the square of the frobenius norm.*/
    template<class T, std::enable_if_t<IsUnaryOperand<T>::value, int> = 0>
    auto frobenius2(T&& t)
    {
      return gridFunction(
        t.gridPart(),
        frobenius2(std::forward<T>(t).expression()),
        t.autoDiffOrder(),
        t.indeterminateId()
        );
    }

    /**Contraction over the given number @a N of indices where the
     * contraction runs over the right-most @a N indices of @a T0.
     */
    template<std::size_t N, class T0, class T1, std::enable_if_t<AreBinaryOperands<T0, T1>::value, int> = 0>
    auto contractInner(T0&& t0, T1&& t1, IndexConstant<N> = IndexConstant<N>{})
    {
      static_assert(std::decay_t<T0>::signature() == std::decay_t<T1>::signature(),
                    "Tensor signature has to coincide for inner product.");
      return gridFunction(
        t0.gridPart(),
        contractInner<N>(std::forward<T0>(t0).expression(), std::forward<T1>(t1).expression()),
        IndexConstant<std::min(autoDiffOrder<T0>(), autoDiffOrder<T1>())>{},
        t0.indeterminateId()
        );
    }

    /**Contraction over the left-most index of @a T0 and the
     * right-most index of @a T1.
     */
    template<class T0, class T1, std::enable_if_t<AreBinaryOperands<T0, T1>::value, int> = 0>
    auto contractInner(T0&& t0, T1&& t1)
    {
      static_assert(std::decay_t<T0>::signature() == std::decay_t<T1>::signature(),
                    "Tensor signature has to coincide for inner product.");
      return gridFunction(
        t0.gridPart(),
        contractInner(std::forward<T0>(t0).expression(), std::forward<T1>(t1).expression()),
        IndexConstant<std::min(autoDiffOrder<T0>(), autoDiffOrder<T1>())>{},
        t0.indeterminateId()
        );
    }

    //! reshape tensor signature for functions
    template<std::size_t... I, class T, std::enable_if_t<IsUnaryOperand<T>::value, int> = 0>
    auto reshape(T&& t, Seq<I...> = Seq<I...>{})
    {
      return gridFunction(
        t.gridPart(),
        reshape<I...>(std::forward<T>(t).expression()),
        t.indeterminateId()
        );
    }

    /**Dynamic restriction with explicitly specified index positions.
     */
    template<std::size_t... IndexPositions, class T, class... Indices,
             std::enable_if_t<(IsUnaryOperand<T>::value
                               && IsIntegralPack<Indices...>::value
      ), int> = 0>
    auto restriction(T&& t, Indices... indices)
    {
      return gridFunction(
        t.gridPart(),
        restriction<IndexPositions...>(std::forward<T>(t).expression(), indices...),
        t.autoDiffOrder(),
        t.indeterminateId()
        );
    }

    /**Generator function for restrictions, PivotIndices is either a
     * "tuple-like" or a sequence.
     */
    template<std::size_t... IndexPositions, class T, class PivotIndices,
             std::enable_if_t<(IsUnaryOperand<T>::value
                               && AllowsGet<PivotIndices>::value
      ), int> = 0>
    auto restriction(T&& t, PivotIndices&& pivotIndices, Seq<IndexPositions...> = Seq<IndexPositions...>{})
    {
      return gridFunction(
        t.gridPart(),
        restriction<IndexPositions...>(std::forward<T>(t).expression(), std::forward<PivotIndices>(pivotIndices)),
        t.autoDiffOrder(),
t.indeterminateId()
        );
    }

    /**Apply a permuation of the index positions.*/
    template<std::size_t... I, class T, std::enable_if_t<IsUnaryOperand<T>::value, int> = 0>
    auto transpose(T&& t, Seq<I...> = Seq<I...>{})
    {
      return gridFunction(
        t.gridPart(),
        transpose<I...>(std::forward<T>(t).expression()),
        t.indeterminateId
        );
    }

    /**Transposition with no index positions implicitly transposes
     * the first two positions.
     */
    template<class T, std::enable_if_t<IsUnaryOperand<T>::value, int> = 0>
    decltype(auto) transpose(T&& t)
    {
      return transpose<1, 0>(std::forward<T>(t));
    }

    //! @}

    /**@name MathFunctions
     *
     * @{
     */

    //!
    template<class T, std::enable_if_t<IsTensorFunction<T>::value, int> = 0>
    auto sqrt(T&& t)
    {
      return gridFunction(
        t.gridPart(),
        sqrt(std::forward<T>(t).expression()),
        t.autoDiffOrder(),
        t.indeterminateId());
    }

    //!
    template<class T, std::enable_if_t<IsTensorFunction<T>::value, int> = 0>
    auto sqr(T&& t)
    {
      return gridFunction(
        t.gridPart(),
        sqr(std::forward<T>(t).expression()),
        t.autoDiffOrder(),
        t.indeterminateId()
        );
    }

    //!
    template<class T, std::enable_if_t<IsTensorFunction<T>::value, int> = 0>
    auto reciprocal(T&& t)
    {
      return gridFunction(
        t.gridPart(),
        reciprocal(std::forward<T>(t).expression()),
        t.autoDiffOrder(),
        t.indeterminateId()
        );
    }

    //!
    template<class T, std::enable_if_t<IsTensorFunction<T>::value, int> = 0>
    auto cos(T&& t)
    {
      return gridFunction(
        t.gridPart(),
        cos(std::forward<T>(t).expression()),
        t.autoDiffOrder(),
        t.indeterminateId()
        );
    }

    //!
    template<class T, std::enable_if_t<IsTensorFunction<T>::value, int> = 0>
    auto sin(T&& t)
    {
      return gridFunction(
        t.gridPart(),
        sin(std::forward<T>(t).expression()),
        t.autoDiffOrder(),
        t.indeterminateId()
        );
    }

    //!
    template<class T, std::enable_if_t<IsTensorFunction<T>::value, int> = 0>
    auto tan(T&& t)
    {
      return gridFunction(
        t.gridPart(),
        tan(std::forward<T>(t).expression()),
        t.autoDiffOrder(),
        t.indeterminateId()
        );
    }

    //!
    template<class T, std::enable_if_t<IsTensorFunction<T>::value, int> = 0>
    auto acos(T&& t)
    {
      return gridFunction(
        t.gridPart(),
        acos(std::forward<T>(t).expression()),
        t.autoDiffOrder(),
        t.indeterminateId()
        );
    }

    //!
    template<class T, std::enable_if_t<IsTensorFunction<T>::value, int> = 0>
    auto asin(T&& t)
    {
      return gridFunction(
        t.gridPart(),
        asin(std::forward<T>(t).expression()),
        t.autoDiffOrder(),
        t.indeterminateId()
        );
    }

    //!
    template<class T, std::enable_if_t<IsTensorFunction<T>::value, int> = 0>
    auto atan(T&& t)
    {
      return gridFunction(
        t.gridPart(),
        atan(std::forward<T>(t).expression()),
        t.autoDiffOrder(),
        t.indeterminateId()
        );
    }

    //!
    template<class T, std::enable_if_t<IsTensorFunction<T>::value, int> = 0>
    auto erf(T&& t)
    {
      return gridFunction(
        t.gridPart(),
        erf(std::forward<T>(t).expression()),
        t.autoDiffOrder(),
        t.indeterminateId()
        );
    }

    //!
    template<class T, std::enable_if_t<IsTensorFunction<T>::value, int> = 0>
    auto exp(T&& t)
    {
      return gridFunction(
        t.gridPart(),
        exp(std::forward<T>(t).expression()),
        t.autoDiffOrder(),
        t.indeterminateId()
        );
    }

    //!
    template<class T, std::enable_if_t<IsTensorFunction<T>::value, int> = 0>
    auto log(T&& t)
    {
      return gridFunction(
        t.gridPart(),
        log(std::forward<T>(t).expression()),
        t.autoDiffOrder(),
        t.indeterminateId()
        );
    }

    //!
    template<class T, std::enable_if_t<IsTensorFunction<T>::value, int> = 0>
    auto cosh(T&& t)
    {
      return gridFunction(
        t.gridPart(),
        cosh(std::forward<T>(t).expression()),
        t.autoDiffOrder(),
        t.indeterminateId()
        );
    }

    //!
    template<class T, std::enable_if_t<IsTensorFunction<T>::value, int> = 0>
    auto sinh(T&& t)
    {
      return gridFunction(
        t.gridPart(),
        sinh(std::forward<T>(t).expression()),
        t.autoDiffOrder(),
        t.indeterminateId()
        );
    }

    //!
    template<class T, std::enable_if_t<IsTensorFunction<T>::value,int> = 0>
    auto tanh(T&& t)
    {
      return gridFunction(
        t.gridPart(),
        tanh(std::forward<T>(t).expression()),
        t.autoDiffOrder(),
        t.indeterminateId()
        );
    }

    //!
    template<class T, std::enable_if_t<IsTensorFunction<T>::value, int> = 0>
    auto acosh(T&& t)
    {
      return gridFunction(
        t.gridPart(),
        acosh(std::forward<T>(t).expression()),
        t.autoDiffOrder(),
        t.indeterminateId()
        );
    }

    //!
    template<class T, std::enable_if_t<IsTensorFunction<T>::value, int> = 0>
    auto asinh(T&& t)
    {
      return gridFunction(
        t.gridPart(),
        asinh(std::forward<T>(t).expression()),
        t.autoDiffOrder(),
        t.indeterminateId()
        );
    }

    //!
    template<class T, std::enable_if_t<IsTensorFunction<T>::value, int> = 0>
    auto atanh(T&& t)
    {
      return gridFunction(
        t.gridPart(),
        atanh(std::forward<T>(t).expression()),
        t.autoDiffOrder(),
        t.indeterminateId()
        );
    }

    //!
    template<class T0, class T1, std::enable_if_t<AreBinaryOperands<T0, T1>::value, int> = 0>
    auto atan2(T0&& t0, T1&& t1)
    {
      static_assert(std::decay_t<T0>::indeterminateId() == std::decay_t<T1>::indeterminateId(),
                    "Indeterminate id's do not match");
      assert(&t0.gridPart() == &t1.gridPart());

      return gridFunction(
        t0.gridPart(),
        atan2(std::forward<T0>(t0).expression(), std::forward<T1>(t1).expression()),
        IndexConstant<std::min(autoDiffOrder<T0>(), autoDiffOrder<T1>())>{},
        t0.indeterminateId()
        );
    }

    //!
    template<class T0, class T1, std::enable_if_t<AreBinaryOperands<T0, T1>::value, int> = 0>
    auto pow(T0&& t0, T1&& t1) {
      static_assert(std::decay_t<T0>::indeterminateId() == std::decay_t<T1>::indeterminateId(),
                    "Indeterminate id's do not match");
      assert(&t0.gridPart() == &t1.gridPart());

      return gridFunction(
        t0.gridPart(),
        pow(std::forward<T0>(t0).expression(), std::forward<T1>(t1).expression()),
        IndexConstant<std::min(autoDiffOrder<T0>(), autoDiffOrder<T1>())>{},
        t0.indeterminateId()
        );
    }

    //!
    template<class T0, class T1, std::enable_if_t<AreBinaryOperands<T0, T1>::value, int> = 0>
    auto min(T0&& t0, T1&& t1) {
      static_assert(std::decay_t<T0>::indeterminateId() == std::decay_t<T1>::indeterminateId(),
                    "Indeterminate id's do not match");
      assert(&t0.gridPart() == &t1.gridPart());

      return gridFunction(
        t0.gridPart(),
        min(std::forward<T0>(t0).expression(), std::forward<T1>(t1).expression()),
        IndexConstant<std::min(autoDiffOrder<T0>(), autoDiffOrder<T1>())>{},
        t0.indeterminateId()
        );
    }

    //!
    template<class T0, class T1, std::enable_if_t<AreBinaryOperands<T0, T1>::value, int> = 0>
    auto max(T0&& t0, T1&& t1) {
      static_assert(std::decay_t<T0>::indeterminateId() == std::decay_t<T1>::indeterminateId(),
                    "Indeterminate id's do not match");
      assert(&t0.gridPart() == &t1.gridPart());

      return gridFunction(
        t0.gridPart(),
        max(std::forward<T0>(t0).expression(), std::forward<T1>(t1).expression()),
        IndexConstant<std::min(autoDiffOrder<T0>(), autoDiffOrder<T1>())>{},
        t0.indeterminateId()
        );
    }

    //!
    template<class T0, std::enable_if_t<IsUnaryOperand<T0>::value, int> = 0>
    auto ternary(T0&& t0) {

      return gridFunction(
        t0.gridPart(),
        ternary(std::forward<T0>(t0).expression()),
        IndexConstant<autoDiffOrder<T0>()>{},
        t0.indeterminateId()
        );
    }

    //! @} MathFunctions

    /**Compute the N-th symbolic derivative of a BindableTensorFunction.*/
    template<std::size_t N, class T, std::enable_if_t<IsUnaryOperand<T>::value, int> = 0>
    auto derivative(T&& t)
    {
      return gridFunction(
        t.gridPart(),
        derivative<N>(std::forward<T>(t).expression(), t.indeterminate()),
        IndexConstant<std::min(autoDiffOrder<T>()-N, 0UL)>{},
        t.indeterminateId()
        );
    }

    /**Compute the symbolic derivative of a BindableTensorFunction.*/
    template<class T, std::enable_if_t<IsUnaryOperand<T>::value, int> = 0>
    auto derivative(T&& t)
    {
      return derivative<1>(std::forward<T>(t));
    }

    /**Assume properties.*/
    template<class... Property, class T, std::enable_if_t<IsUnaryOperand<T>::value, int> = 0>
    auto assume(T&& t, MPL::TagContainer<Property...> = MPL::TagContainer<Property...>{})
    {
      return gridFunction(
        t.gridPart(),
        assume<Property...>(std::forward<T>(t).expression()),
        IndexConstant<std::min(autoDiffOrder<T>(), 0UL)>{},
        t.indeterminateId()
        );
    }

  } // namespace GridFunction

#if 0
  // FIXME, needed?
  using GridFunction::operator-;
  using GridFunction::operator+;
  using GridFunction::operator*;
  using GridFunction::operator/;

  using GridFunction::min;
  using GridFunction::max;
#endif

} // namespace Dune::ACFem

#endif // __DUNE_ACFEM_FUNCTIONS_EXPRESSIONS_HH__
