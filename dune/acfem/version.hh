#ifndef __DUNE_ACFEM_VERION_HH__
#define __DUNE_ACFEM_VERION_HH__

/*This file is designed to define macros for deprecation (see <dune/fem/version.hh>) and/or for comparison of dune-module versions (see <dune/common/version.hh>)*/
// #include <dune/fem/version.hh>

namespace Dune {
namespace ACFem {

// nothing for now

} // ACFem

} // Dune

#endif // __DUNE_ACFEM_VERION_HH__
