#ifndef __DUNE_ACFEM_PARAMETERS_TIMEVIEW_HH__
#define __DUNE_ACFEM_PARAMETERS_TIMEVIEW_HH__

#include "../common/timeview.hh"
#include "../common/timeprovidertraits.hh"
#include "../tensors/tensor.hh"
#include "../expressions/interface.hh"
#include "../expressions/terminal.hh"

namespace Dune {

  namespace ACFem {

    namespace Parameter {

      /**@addtogroup Parameters
       * @{
       */

      template<class TimeProvider = Fem::TimeProvider<> >
      class TimeView
        : public Tensor::TensorBase<typename TimeProviderTraits<TimeProvider>::TimeStepType,
                                    Tensor::Seq<>, TimeView<TimeProvider> >
        , public Expressions::SelfExpression<TimeView<TimeProvider> >
      {
        using BaseType = Tensor::TensorBase<typename TimeProviderTraits<TimeProvider>::TimeStepType,
                                            Tensor::Seq<>, TimeView>;
       public:
        using typename BaseType::FieldType;
        using TimeViewType = ACFem::TimeView<TimeProvider>;

        /**Constructor.
         *
         * @param[in] tp Super-ordinate time-provider.
         *
         * @param[in] theta The fraction of the current time step which
         * defines the global time we evaluate to.
         */
        TimeView(const TimeProvider& tp, const FieldType& theta = 0.0)
          : timeView_(tp, theta)
        {}

        FieldType operator()() const
        {
          return timeView_.time();
        }

        FieldType operator()(Tensor::Seq<>) const
        {
          return (*this)();
        }

       protected:
        TimeViewType timeView_;
      };

      template<class TimeProvider>
      auto timeView(const TimeProvider& tp, const typename TimeProviderTraits<TimeProvider>::TimeStepType& theta = 0.0)
      {
        return expressionClosure(TimeView<TimeProvider>(tp, theta));
      }

      //! @} Parameters

    } // NS Parameter

    using Parameter::timeView;

  } // NS ACFem

  template<class TimeProvider>
  struct FieldTraits<ACFem::Parameter::TimeView<TimeProvider> >
    : FieldTraits<typename ACFem::Parameter::TimeView<TimeProvider>::FieldType>
  {};

} // NS Dune

#endif // __DUNE_ACFEM_PARAMETERS_TIMEVIEW_HH__
