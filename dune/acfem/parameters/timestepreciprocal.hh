#ifndef __DUNE_ACFEM_PARAMETERS_TIMESTEPRECIPROCAL_HH__
#define __DUNE_ACFEM_PARAMETERS_TIMESTEPRECIPROCAL_HH__

#include <dune/fem/solver/timeprovider.hh>

#include "../common/timeprovidertraits.hh"
#include "../expressions/terminal.hh"
#include "../tensors/tensorbase.hh"

namespace Dune {

  namespace ACFem {

    namespace Parameter {

      /**@addtogroup Parameters
       *
       * @{
       */

      template<class TimeProvider = Fem::TimeProvider<> >
      class TimeStepReciprocal
        : public Tensor::TensorBase<typename TimeProviderTraits<TimeProvider>::TimeStepType,
                                    Tensor::Seq<>, TimeStepReciprocal<TimeProvider> >
        , public Expressions::SelfExpression<TimeStepReciprocal<TimeProvider> >
        , public PositiveExpression
      {
        using BaseType = Tensor::TensorBase<typename TimeProviderTraits<TimeProvider>::TimeStepType,
                                            Tensor::Seq<>, TimeStepReciprocal>;
       public:
        using typename BaseType::FieldType;

        TimeStepReciprocal(const TimeProvider& tp)
          : timeProvider_(tp)
        {}

        FieldType operator()() const
        {
          return timeProvider_.inverseDeltaT();
        }

        FieldType operator()(Tensor::Seq<>) const
        {
          return (*this)();
        }

       protected:
        const TimeProvider& timeProvider_;
      };

      template<class TimeProvider>
      auto timeStepReciprocal(const TimeProvider& tp)
      {
        return expressionClosure(TimeStepReciprocal<TimeProvider>(tp));
      }

      //! @} Parameters

    } // NS Parameter

    using Parameter::timeStepReciprocal;

  } // NS ACFem

  template<class TimeProvider>
  struct FieldTraits<ACFem::Parameter::TimeStepReciprocal<TimeProvider> >
    : FieldTraits<typename ACFem::Parameter::TimeStepReciprocal<TimeProvider>::FieldType>
  {};

} // namespace Dune

#endif // __DUNE_ACFEM_PARAMETERS_TIMESTEPRECIPROCAL_HH__
