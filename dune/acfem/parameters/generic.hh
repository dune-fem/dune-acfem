#ifndef __DUNE_ACFEM_PARAMETERS_GENERIC_HH__
#define __DUNE_ACFEM_PARAMETERS_GENERIC_HH__

// Slurping in parameters through Fem::Parameter

#include <dune/fem/io/parameter.hh>

#include "../expressions/operandpromotion.hh"
#include "../expressions/densestorage.hh"
#include "../tensors/tensorbase.hh"
#include "../tensors/operations/placeholder.hh"
#include "../tensors/operations/assume.hh"

namespace Dune {

  namespace ACFem {

    namespace Parameter {

      /**@addtogroup Parameters
       * @{
       */

      /**A placeholder tensor which can be initialized in
       * particular by a Fem::Parameter value.
       */
      template<class FV>
      class Generic
        : public Tensor::PlaceholderTensor<Tensor::DenseStorage<FV>, Generic<FV> >
      {
        using ThisType = Generic;
        using BaseType = Tensor::PlaceholderTensor<Tensor::DenseStorage<FV>, ThisType>;
        using DecayVectorType = std::decay_t<FV>;
       public:
        using FieldVectorType = FV;
        using StorageType = Tensor::DenseStorage<FV>;

        Generic(StorageType&& arg = StorageType{}, const std::string& key = "")
          : BaseType(std::forward<StorageType>(arg))
          , key_(key == "" ? BaseType::name() : key)
        {}

        Generic(const std::string& key)
          : Generic(Fem::Parameter::getValue<DecayVectorType>(key), key)
        {}

        Generic(const std::string& key, FieldVectorType&& dflt)
          : Generic(Fem::Parameter::getValue<DecayVectorType>(key, dflt), key)
        {}

        std::string name() const
        {
          return key_;
        }

        std::string key_ = "";
      };

      template<class... Properties, class FV, std::enable_if_t<!std::is_constructible<std::string, FV>::value, int> = 0>
      constexpr auto parameter(const FV& value)
      {
        return assume<Properties...>(Generic<FV>(value));
      }

      template<class FV, class... Properties>
      constexpr auto parameter(const std::string& key)
      {
        return assume<Properties...>(Generic<FV>(key));
      }

      template<class FV, class... Properties>
      constexpr auto parameter(const std::string& key, const FV& dflt)
      {
        return assume<Properties...>(Generic<FV>(key, FV(dflt)));
      }

      /** Example:
       *
       * @code
       * auto deltaT = uniqueParameter<0>(0.1);
       * @endcode
       */
      template<std::size_t Id, class FV, class... Properties>
      constexpr auto uniqueParameter(const FV& value)
      {
        return assume<UniqueExpression<Id>, Properties...>(Generic<FV>(value));
      }

      /** Example, read from parameter file:
       *
       * @code
       * auto deltaT = uniqueParameter<0, double>("heat.deltaT");
       * @endcode
       */
      template<std::size_t Id, class FV, class... Properties>
      constexpr auto uniqueParameter(const std::string& key)
      {
        return assume<UniqueExpression<Id>, Properties...>(Generic<FV>(key));
      }

      /** Example, read from parameter file with default value:
       *
       * @code
       * auto deltaT = uniqueParameter<0>("heat.deltaT", 0.1);
       * @endcode
       */
      template<std::size_t Id, class... Properties, class FV>
      constexpr auto uniqueParameter(const std::string& key, const FV& dflt)
      {
        return assume<UniqueExpression<Id>, Properties...>(Generic<FV>(key, FV(dflt)));
      }

      template<std::size_t Id, class FV, class... Properties>
      constexpr auto positiveParameter(const FV& value)
      {
        static_assert(std::is_convertible<FV, double>::value,
                      "Positive parameters must be scalars");
        assert(value > 0);

        return uniqueParameter<Id, PositiveExpression, Properties...>(value);
      }

      template<std::size_t Id, class FV, class... Properties>
      constexpr auto positiveParameter(const std::string& key)
      {
        static_assert(std::is_convertible<FV, double>::value,
                      "Positive parameters must be scalars");

        return uniqueParameter<Id, FV, PositiveExpression, Properties...>(key);
      }

      template<std::size_t Id, class FV, class... Properties>
      constexpr auto positiveParameter(const std::string& key, const FV& dflt)
      {
        static_assert(std::is_convertible<FV, double>::value,
                      "Positive parameters must be scalars");
        assert(dflt > 0);

        return uniqueParameter<Id, PositiveExpression, Properties...>(key, dflt);
      }

      //!@} Parameters

    } // NS Parameter

    template<class FV>
    using GenericParameter = Parameter::Generic<FV>;

    using Parameter::parameter;
    using Parameter::uniqueParameter;
    using Parameter::positiveParameter;

  } // NS ACFem

  template<class FV>
  struct FieldTraits<ACFem::Parameter::Generic<FV> >
    : FieldTraits<typename FieldTraits<FV>::field_type>
  {};

} // NS Dune

#endif // __DUNE_ACFEM_PARAMETERS_GENERIC_HH__
