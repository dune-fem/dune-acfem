#include <config.h>

#include "../../common/scopedredirect.hh"

#include <iostream>
#include <sstream>
#include <stdexcept>

#include <dune/common/exceptions.hh>
#include <dune/fem/io/parameter.hh>

#include <dune/fem/gridpart/adaptiveleafgridpart.hh>
// Helper to get to the reference element with reasonable effort
#include <dune/fem/space/common/allgeomtypes.hh>

#include "../../common/geometry.hh"
#include "../../common/gridfunctionspace.hh"

#include "../../tensors/tensor.hh"
#include "../generic.hh"
#include "../timestep.hh"
#include "../timestepreciprocal.hh"
#include "../timeview.hh"

/**@addtogroup Tests
 * @{
 */

using namespace Dune;
using namespace Dune::ACFem;

#ifdef NDEBUG
# undef NDEBUG
#endif

#ifndef TEST_DGF_DATA
# define TEST_DGF_DATA dgfSimplexData2d
#endif

//Embedded test DGF-"file" with a simple interval
const std::string dgfIntervalData2d(R"(DGF

Interval
 0   0
 1   1
 1   1
#

GridParameter
% longest or arbitrary (see DGF docu)
refinementedge longest
overlap 0
#

BoundaryDomain
default 3
1   0 0   1 0  % lower boundary
2   0 1   1 1  % upper boundary
#

#)");

//Embedded test DGF-"file" with some simplices
const std::string dgfSimplexData2d(R"(DGF

Vertex
 -1  -1
  1  -1
  1   1
 -1   1
  0   0
#

SIMPLEX
 0 1 4
 1 2 4
 2 3 4
 3 0 4
#

BoundarySegments
 1   0 1  : blah1  % right boundary
 2   1 2  : blah2  % upper boundary
 3   2 3  : blah3  % left boundary
 4   3 0  : blah4  % lower boundary
#
GridParameter
 % longest or arbitrary (see DGF docu)
 refinementedge longest
 % there is zarro information on this ... :(
 overlap 0
 % whatever this may be ...
 tolerance 1e-12
 % silence?
 verbose 0
#)");

/**Test-template main program. Instantiate a single expression
 * template and evaluate it on a simple grid.
 */
int main(int argc, char *argv[])
{
  try {
    // General setup
    Dune::Fem::MPIManager::initialize(argc, argv);

    // append parameter
    Dune::Fem::Parameter::append(argc, argv);

    // append default parameter file
    Dune::Fem::Parameter::append(SRCDIR "/parameter");

    //redirect the stream cerr to cout
    //so we only get the output of clog on stderr
    ScopedRedirect redirect(std::cerr, std::cout);

    // reduce precision and use normalized FP output
    // std::clog << std::scientific << std::setprecision(4);
    std::cout << std::fixed << std::setprecision(8);

    // type of hierarchical grid
    typedef Dune::GridSelector::GridType HGridType;

    // the method rank and size from MPIManager are static
    if (Dune::Fem::MPIManager::rank() == 0) {
      std::cout << "Loading embedded macro grid: "
                << std::endl
                << "=================================" << std::endl
                << TEST_DGF_DATA
                << "=================================" << std::endl
                << std::endl;
    }

    // we try to be self-contained an simply load the embedded simple dgf-file
    std::istringstream dgfStream(TEST_DGF_DATA);

    // construct macro using the DGF Parser
    Dune::GridPtr<HGridType> gridPtr(dgfStream);
    HGridType& grid = *gridPtr;

    // do initial load balance
    grid.loadBalance();

    typedef Dune::Fem::AdaptiveLeafGridPart<HGridType, Dune::InteriorBorder_Partition> GridPartType;
    GridPartType gridPart(grid);

    const auto testName = Dune::Fem::Parameter::getValue<std::string>("testName", "default");

    if (testName == "default") {
      Fem::GridTimeProvider<HGridType> timeProvider(0.0, grid);
      timeProvider.init(0.1);

      auto p0 = parameter<double>("generic0");
      std::cout << "generic without default: " << p0 << std::endl;
      if (!(p0 == Fem::Parameter::getValue<double>("generic0"))) {
        throw std::runtime_error("Test \""+testName+"\" failed");
      }

      auto p1 = parameter<double>("generic1", 0.1);
      std::cout << "generic with default 0.1: " << p1 << std::endl;
      if (!(p1 == Fem::Parameter::getValue<double>("generic1", 0.1))) {
        throw std::runtime_error("Test \""+testName+"\" failed");
      }

      std::cout << (3. * p1) << std::endl;
      std::cout << typeString(3. * p1) << std::endl;

      auto p2 = timeStep(timeProvider);
      std::cout << "timestep 0.1: " << p2 << std::endl;
      if (!(p2 == timeProvider.deltaT())) {
        throw std::runtime_error("Test \""+testName+"\" failed");
      }

      std::cout << (3. * p2) << std::endl;
      std::cout << typeString(3. * p2) << std::endl;

      auto p3 = timeStepReciprocal(timeProvider);
      std::cout << "timestep 0.1 reciprocal: " << p3 << std::endl;
      if (!(p3 == timeProvider.inverseDeltaT())) {
        throw std::runtime_error("Test \""+testName+"\" failed");
      }

      std::cout << (3. * p3) << std::endl;
      std::cout << typeString(3. * p3) << std::endl;

      auto p4 = timeView(timeProvider, 0.5);
      std::cout << "timeView center: " << p4 << std::endl;
      if (!(p4 == timeProvider.time() + 0.5 * timeProvider.deltaT())) {
        throw std::runtime_error("Test \""+testName+"\" failed");
      }

      std::cout << (3. * p4) << std::endl;
      std::cout << typeString(3. * p4) << std::endl;

    } else {
      throw std::invalid_argument("Test \""+testName+"\" not implemented");
    }

    std::clog << "This is the only \"cerr\" output which should be produced by this test." << std::endl;

    return EXIT_SUCCESS;
  }
  catch (std::exception &e){
    std::clog << "Exception: " << e.what() << std::endl;
    return EXIT_FAILURE;
  }
  catch (...){
    std::clog << "Unknown exception thrown!" << std::endl;
    return EXIT_FAILURE;
  }
}

//!@} Tests

//!@} GridFunctions
