#ifndef __DUNE_ACFEM_PARAMETERS_TIMESTEP_HH__
#define __DUNE_ACFEM_PARAMETERS_TIMESTEP_HH__

#include <dune/fem/solver/timeprovider.hh>

#include "../common/timeprovidertraits.hh"
#include "../tensors/tensorbase.hh"
#include "../expressions/terminal.hh"

namespace Dune {

  namespace ACFem {

    namespace Parameter {

      /**@addtogroup Parameters
       * @{
       */

      template<class TimeProvider = Fem::TimeProvider<> >
      class TimeStep
        : public Tensor::TensorBase<typename TimeProviderTraits<TimeProvider>::TimeStepType, Tensor::Seq<>, TimeStep<TimeProvider> >
        , public PositiveExpression
        , public Expressions::SelfExpression<TimeStep<TimeProvider> >
      {
        using BaseType = Tensor::TensorBase<typename TimeProviderTraits<TimeProvider>::TimeStepType,
                                            Tensor::Seq<>, TimeStep>;
       public:
        using typename BaseType::FieldType;

        TimeStep(const TimeProvider& tp)
          : timeProvider_(tp)
        {}

        FieldType operator()() const
        {
          return timeProvider_.deltaT();
        }

        FieldType operator()(Tensor::Seq<>) const
        {
          return (*this)();
        }

       protected:
        const TimeProvider& timeProvider_;
      };

      template<class TimeProvider>
      auto timeStep(const TimeProvider& tp)
      {
        return expressionClosure(TimeStep<TimeProvider>(tp));
      }

      //! @} Parameters

    } // NS Parameter

    using Parameter::timeStep;

  } // namespace ACFem

  template<class TimeProvider>
  struct FieldTraits<ACFem::Parameter::TimeStep<TimeProvider> >
    : FieldTraits<typename ACFem::Parameter::TimeStep<TimeProvider>::FieldType>
  {};

} // namespace Dune

#endif // __DUNE_ACFEM_PARAMETERS_TIMESTEP_HH__
