#ifndef __DUNE_ACFEM_TENSORS_MODULES_CONSTANT_HH__
#define __DUNE_ACFEM_TENSORS_MODULES_CONSTANT_HH__

#include "../../common/fractionconstant.hh"
#include "../../expressions/terminal.hh"
#include "../tensorbase.hh"
#include "../bindings.hh"

namespace Dune {

  namespace ACFem {

    namespace Tensor {

      /**@addtogroup Tensors
       * @{
       */

      /**@addtogroup TensorAtoms
       * @{
       */

      template<class Field, class Signature>
      class ConstantTensor;

      template<class T, class Field>
      constexpr inline bool IsConstantTensorV = false;

      template<class T, class Field>
      constexpr inline bool IsConstantTensorV<T&, Field> = IsConstantTensorV<std::decay_t<T>, Field>;

      template<class Signature, class Field>
      constexpr inline bool IsConstantTensorV<ConstantTensor<Field, Signature>, Field> = true;

      template<class T, class SFINAE = void>
      struct IsConstantTensor
        : FalseType
      {};

      template<class T>
      struct IsConstantTensor<T, std::enable_if_t<!IsDecay<T>::value> >
        : IsConstantTensor<std::decay_t<T> >
      {};

      template<class Field, class Dims>
      struct IsConstantTensor<ConstantTensor<Field, Dims> >
        : TrueType
      {};

      template<class T>
      struct IsNamedConstantTensor
        : FalseType
      {};

      template<class T>
      struct IsNamedConstantTensor<T&>
        : IsNamedConstantTensor<std::decay_t<T> >
      {};

      template<class T>
      struct IsNamedConstantTensor<T&&>
        : IsNamedConstantTensor<std::decay_t<T> >
      {};

      template<class Dims, class T, char... Name>
      struct IsNamedConstantTensor<ConstantTensor<TypedValue::NamedConstant<T, Name...>, Dims> >
        : TrueType
      {};

      template<class T>
      struct IsFractionConstantTensor
        : FalseType
      {};

      template<class T>
      struct IsFractionConstantTensor<T&>
        : IsFractionConstantTensor<std::decay_t<T> >
      {};

      template<class T>
      struct IsFractionConstantTensor<T&&>
        : IsFractionConstantTensor<std::decay_t<T> >
      {};

      template<class Dims, class Int, Int Numerator, Int Denominator>
      struct IsFractionConstantTensor<ConstantTensor<FractionConstant<Int, Numerator, Denominator>, Dims> >
        : TrueType
      {};

      template<class T>
      struct IsOnes
        : FalseType
      {};

      template<class T>
      struct IsOnes<T&>
        : IsOnes<std::decay_t<T> >
      {};

      template<class T>
      struct IsOnes<T&&>
        : IsOnes<std::decay_t<T> >
      {};

      template<class Dims, class Int>
      struct IsOnes<ConstantTensor<FractionConstant<Int, 1, 1>, Dims> >
        : TrueType
      {};

      template<class T, class SFINAE = void>
      struct ConstantDataWrapper;

      template<class T>
      struct ConstantDataWrapper<T, std::enable_if_t<IsTypedValue<T>::value> >
      {
        using DataType = T;

        ConstantDataWrapper()
        {};

        template<class DataArg, std::enable_if_t<std::is_constructible<DataType, DataArg>::value, int> = 0>
        ConstantDataWrapper(DataArg&& = DataArg{})
        {};

        static_assert(!IsTypedValue<DataType>::value || IsDecay<DataType>::value,
                      "Typed values should not be stored as references.");

        static constexpr T data() { return T{}; }
      protected:
        DataType data_;
      };

      template<class T>
      struct ConstantDataWrapper<T, std::enable_if_t<!IsTypedValue<T>::value> >
      {
        using DataType = T;

        template<class DataArg, std::enable_if_t<std::is_constructible<DataType, DataArg>::value, int> = 0>
        ConstantDataWrapper(DataArg&& data = DataArg{})
          : data_(std::forward<DataArg>(data))
        {
          //std::clog << "wrapper: " << data_ << std::endl;
          //std::clog << typeString(MPL::TypeTuple<T>{}) << std::endl;
        }

        decltype(auto) data() const&
        {
          return data_;
        }

        decltype(auto) data() &
        {
          return data_;
        }

        DataType data() &&
        {
          return data_;
        }
      protected:
        DataType data_;
      };

      /**A tensor where all elements evaluate to the same constant
       * value.
       */
      template<class Field, std::size_t... Dimensions>
      class ConstantTensor<Field, Seq<Dimensions...> >
        : public TensorBase<std::decay_t<Field>, Seq<Dimensions...>, ConstantTensor<Field, Seq<Dimensions...> > >
        , public ConstantDataWrapper<Field>
        , public Expressions::SelfExpression<ConstantTensor<Field, Seq<Dimensions...> > >
        , public MPL::UniqueTags<ConditionalType<IsConstantExprArg<Field>::value, ConstantExpression, void>,
                                 ConditionalType<IsTypedValue<Field>::value, TypedValueExpression, void> >
      {
        using ThisType = ConstantTensor;
        using BaseType = TensorBase<std::decay_t<Field>, Seq<Dimensions...>, ConstantTensor<Field, Seq<Dimensions...> > >;
        using DataWrapperType = ConstantDataWrapper<Field>;
       public:
        using BaseType::rank;
        using typename BaseType::FieldType;
        using typename BaseType::Signature;
        using typename DataWrapperType::DataType;
        using DataWrapperType::data;

        // This really should not take part in implicit type conversions.
        template<class DataArg, std::enable_if_t<std::is_constructible<DataType, DataArg>::value, int> = 0>
        explicit ConstantTensor(DataArg&& data)
          : DataWrapperType(std::forward<DataArg>(data))
        {}

        /**Allow default construction if contained types fulfill IsTypedValue.*/
        template<
          class... Dummy,
          std::enable_if_t<(sizeof...(Dummy) == 0
                            && IsTypedValue<DataType>::value), int> = 0>
        ConstantTensor(Dummy&&...)
          : DataWrapperType()
        {
          //std::clog << "ConstantTensor: " << DataWrapperType::data_ << std::endl;
          //std::clog << typeString(MPL::TypeTuple<Field>{}) << std::endl;
        }

        /**Insert the current view-indices at their proper positions and
         * foward to the underlying "host" tensor.
         */
        template<class... Dims,
                 std::enable_if_t<(sizeof...(Dims) == rank
                                   &&
                                   IsIntegralPack<Dims...>::value)
                                  , int> = 0>
        decltype(auto) operator()(Dims... indices) const
        {
          //std::clog << "returning const: " << data() << std::endl;
          return data();
        }

        /**Constant access from index-sequence, zero optimization.*/
        template<std::size_t... Indices,
                 std::enable_if_t<(sizeof...(Indices) == rank
                                   && ThisType::template isZero<Indices...>()
          ), int> = 0>
        decltype(auto) constexpr operator()(Seq<Indices...>) const
        {
          //std::clog << "returning const: 0_c " << std::endl;
          return IntFraction<0>{};
        }

        /**Constant access from index-sequence, zero optimization.*/
        template<std::size_t... Indices,
                 std::enable_if_t<(sizeof...(Indices) == rank
                                   && !ThisType::template isZero<Indices...>()
          ), int> = 0>
        decltype(auto) constexpr operator()(Seq<Indices...>) const
        {
          //std::clog << "returning const: " << data() << std::endl;
          return data();
        }

        template<std::size_t... Indices, class Pos = MakeIndexSequence<sizeof...(Indices)> >
        static bool constexpr isZero(Seq<Indices...> = Seq<Indices...>{}, Pos = Pos{})
        {
          return ExpressionTraits<FieldType>::isZero;
        }

        // in order to support projection optimizations we fake a non-const lookAt()
        template<class... Whatever>
        void lookAt(Whatever&&...)
        {}

        std::string name() const
        {
          std::string prefix;
          if (std::is_same<DataType, IntFraction<0> >{}) {
            prefix = "zeros<";
          } else if(std::is_same<DataType, IntFraction<1> >{}) {
            prefix = "ones<";
          } else {
            prefix = "tensor<"+toString(data());
            if (std::is_reference<DataType>::value) {
              prefix += std::is_const<DataType>::value ? "cref" : "ref";
            }
            prefix += rank > 0 ? "@" : "";
          }

          return prefix+toString(Signature{})+">";
        }
      };

      template<std::size_t... Dimensions, class T, class F = Closure>
      constexpr auto constantTensor(T&& t, Seq<Dimensions...> = Seq<Dimensions...>{}, F closure = F{})
      {
        using ValueType = ConditionalType<IsTypedValue<T>::value, std::decay_t<T>, T>;
        return closure(ConstantTensor<ValueType, Seq<Dimensions...> >(std::forward<T>(t)));
      }

      template<class T, class Tensor, class F = Closure,
               std::enable_if_t<IsTensorOperand<Tensor>::value, int> = 0>
      constexpr auto constantTensor(T&& t, Tensor&&, F = F{})
      {
        using Signature = typename TensorTraits<Tensor>::Signature;
        return constantTensor(std::forward<T>(t), Signature{}, F{});
      }

      template<std::size_t... Dimensions, class F = Closure>
      auto zeros(Seq<Dimensions...> = Seq<Dimensions...>{}, F = F{})
      {
        return constantTensor(IntFraction<0>{}, Seq<Dimensions...>{}, F{});
      }

      template<class T, class F = Closure,
               std::enable_if_t<IsTensor<T>::value, int> = 0>
      constexpr auto zeros(F = F{})
      {
        using Signature = typename TensorTraits<T>::Signature;
        return zeros(Signature{}, F{});
      }

      template<class T, class F = Closure,
               std::enable_if_t<IsTensorOperand<T>::value, int> = 0>
      constexpr auto zeros(T&&, F = F{})
      {
        return zeros<T>(F{});
      }

      template<std::size_t... Dimensions, class F = Closure>
      constexpr auto ones(Seq<Dimensions...> = Seq<Dimensions...>{}, F = F{})
      {
        return constantTensor(IntFraction<1>{}, Seq<Dimensions...>{}, F{});
      }

      template<class T, class F = Closure,
               std::enable_if_t<IsTensor<T>::value, int> = 0>
      constexpr auto ones(F = F{})
      {
        using Signature = typename TensorTraits<T>::Signature;
        return ones(Signature{}, F{});
      }

      template<class T, class F = Closure,
               std::enable_if_t<IsTensor<T>::value, int> = 0>
      constexpr auto ones(T&&, F = F{})
      {
        return ones<T>(F{});
      }

      template<class T>
      using Zeros = ConditionalType<IsSequence<T>::value,
                                    ConstantTensor<IntFraction<0>, T>,
                                    ConstantTensor<IntFraction<0>, typename TensorTraits<T>::Signature> >;

      template<class T>
      using Ones = ConditionalType<IsSequence<T>::value,
                                   ConstantTensor<IntFraction<1>, T>,
                                   ConstantTensor<IntFraction<1>, typename TensorTraits<T>::Signature> >;

      ///@} TensorAtoms

      ///@} Tensors

    } // NS Tensor

    namespace Expressions {

      template<class T, class Seq>
      constexpr inline std::size_t WeightV<Tensor::ConstantTensor<T, Seq> > = WeightV<std::decay_t<T> >;

    }

  } // NS ACFem

  template<class Field, class Signature>
  struct FieldTraits<ACFem::Tensor::ConstantTensor<Field, Signature> >
    : FieldTraits<std::decay_t<Field> >
  {};

} // NS Dune

#endif // __DUNE_ACFEM_TENSORS_MODULES_CONSTANT_HH__
