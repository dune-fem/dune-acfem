#ifndef __DUNE_ACFEM_TENSORS_MODULES_LINEARSTORAGE_HH__
#define __DUNE_ACFEM_TENSORS_MODULES_LINEARSTORAGE_HH__

#include <initializer_list>
#include "../../common/fractionconstant.hh"
#include "../../common/ostream.hh"
#include "../../expressions/terminal.hh"
#include "../tensorbase.hh"
#include "../bindings.hh"

namespace Dune {

  namespace ACFem {

    namespace Tensor {

      using ACFem::operator<<;
      using Dune::operator<<;

      /**@addtogroup Tensors
       * @{
       */

      /**@addtogroup TensorAtoms
       * @{
       */

      /**A tensor wrapping an object with an operator[].*/
      template<class Container, class Signature>
      class LinearStorageTensor;

      template<class T, class SFINAE = void>
      struct IsLinearStorageTensor
        : FalseType
      {};

      template<class T>
      struct IsLinearStorageTensor<T, std::enable_if_t<!IsDecay<T>::value> >
        : IsLinearStorageTensor<std::decay_t<T> >
      {};

      template<class Container, class Signature>
      struct IsLinearStorageTensor<LinearStorageTensor<Container, Signature> >
        : TrueType
      {};


      /**A tensor wrapping an object with an operator[] and a value_type.*/
      template<class Container, std::size_t... Dimensions>
      class LinearStorageTensor<Container, Seq<Dimensions...> >
        : public TensorBase<typename std::decay_t<Container>::value_type, Seq<Dimensions...>, LinearStorageTensor<Container, Seq<Dimensions...> > >
        , public Expressions::SelfExpression<LinearStorageTensor<Container, Seq<Dimensions...> > >
        , public MPL::UniqueTags<ConditionalType<IsConstantExprArg<Container>::value, ConstantExpression, void> >
      {
        using ThisType = LinearStorageTensor;
        using BaseType = TensorBase<typename std::decay_t<Container>::value_type, Seq<Dimensions...>, LinearStorageTensor<Container, Seq<Dimensions...> > >;
       public:
        using BaseType::rank;
        using BaseType::dim;
        using typename BaseType::FieldType;
        using typename BaseType::Signature;

        // This really should not take part in implicit type conversions.
        template<class Arg, std::enable_if_t<std::is_constructible<Container, Arg>::value, int> = 0 >
        explicit LinearStorageTensor(Arg&& data)
          : data_(std::forward<Arg>(data))
        {
          if constexpr (HasResize<Container>::value) {
            data_.resize(dim());
          }
          assert(data_.size() == dim());
        }

        template<class T, std::enable_if_t<std::is_constructible<FieldType, T>::value && !std::is_constructible<Container, std::initializer_list<T> >::value, int> = 0 >
        LinearStorageTensor(std::initializer_list<T> list)
        {
          if constexpr (HasResize<Container>::value) {
            data_.resize(dim());
          }
          assert(data_.size() == dim());
          assert(list.size() == dim());
          int i =0;
          for(T el : list )
          {
            data_[i] = el;
            ++i;
          }
        }

        /**Allow default construction.*/
        LinearStorageTensor()
        {
          if constexpr (HasResize<Container>::value) {
            data_.resize(dim());
          }
          assert(data_.size() == dim());
        }

        LinearStorageTensor(const LinearStorageTensor& other)
        : data_(other.data_)
        {}

        LinearStorageTensor(LinearStorageTensor&& other)
          : data_(std::move(other).data_)
        {
          assert(data_.size() == dim());
        }

        /**Copy from any other tensor.*/
        template<class Tensor,
                 std::enable_if_t<(IsTensor<Tensor>::value
                                   && !RefersConst<Container>::value
                                   && !std::is_same<Tensor, LinearStorageTensor>::value
                                   && std::is_same<typename Tensor::Signature, Signature>::value
                   ), int> = 0>
        explicit LinearStorageTensor(const Tensor& other)
        {
          (*this) = other;
        }

        /**Assignment from any other tensor.*/
        template<class T,
                 std::enable_if_t<(IsTensor<T>::value
                                   && !RefersConst<Container>::value
                                   && !std::is_same<T, LinearStorageTensor>::value
                                   && std::is_same<Signature, typename TensorTraits<T>::Signature>::value),
                                  int> = 0>
        LinearStorageTensor& operator=(T&& other)
        {
          forLoop<dim()>([&](auto i) {
              data_[i.value] = other(multiIndex(Signature{}, i));
              //std::clog << "i " << i.value << " <- " << multiIndex(Signature{}, i) << std::endl;
            });
          return *this;
        }

        /**Translate the given multi-index positions into a linear
         * index and forward to the underlying storage.
         */
        template<class... Indices,
                 std::enable_if_t<(sizeof...(Indices) == rank
                                   &&
                                   IsIntegralPack<Indices...>::value)
                                  , int> = 0>
        constexpr decltype(auto) operator()(Indices... indices)
        {
          const std::size_t index = flattenMultiIndex(IndexSequence<Dimensions...>{}, indices...);
          //std::clog << "index " << index << " <- " << std::array<std::size_t, rank>({{ (std::size_t)indices... }}) << std::endl;
          return data_[index];
        }

        /**Translate the given multi-index positions into a linear
         * index and forward to the underlying storage.
         */
        template<class... Indices,
                 std::enable_if_t<(sizeof...(Indices) == rank
                                   &&
                                   IsIntegralPack<Indices...>::value)
                                  , int> = 0>
        constexpr decltype(auto) operator()(Indices... indices) const
        {
          const std::size_t index = flattenMultiIndex(IndexSequence<Dimensions...>{}, indices...);
          //std::clog << "index " << index << " <- " << std::array<std::size_t, rank>({{ (std::size_t)indices... }}) << std::endl;
          return data_[index];
        }

        /**Constant access from index-sequence.*/
        template<std::size_t... Indices,
                 std::enable_if_t<sizeof...(Indices) == rank, int> = 0>
        decltype(auto) constexpr operator()(Seq<Indices...>) const
        {
          static_assert(sizeof...(Indices) == rank, "Number of indices differs from tensor rank.");
          return (*this)(Indices...);
        }

        /**Constant access from index-sequence.*/
        template<std::size_t... Indices,
                 std::enable_if_t<sizeof...(Indices) == rank, int> = 0>
        decltype(auto) constexpr operator()(Seq<Indices...>)
        {
          static_assert(sizeof...(Indices) == rank, "Number of indices differs from tensor rank.");
          return (*this)(Indices...);
        }

        std::string name() const
        {
          return "LST(" + refPrefix() + name<Signature>() + ")";
        }

       private:
        std::string refPrefix() const
        {
          constexpr bool isRef = std::is_lvalue_reference<Container>::value;
          constexpr bool isConst = std::is_const<std::remove_reference_t<Container> >::value;

          return isRef ? (isConst ? "cref" : "ref") : "";
        }

        template<class Sig, std::enable_if_t<Sig::size() == 0, int> = 0>
        std::string name() const
        {
          return toString(data_);
        }

        template<class Sig, std::enable_if_t<Sig::size() == 1, int> = 0>
        std::string name() const
        {
          return "[" + toString(data_) + "]";
        }

        template<class Sig, std::enable_if_t<(Sig::size() > 1), int> = 0>
        std::string name() const
        {
          return "<" + toString(Sig{}) + ">";
        }

        Container data_;
      };

      template<std::size_t... Dimensions, class T>
      auto linearStorageTensor(T&& t, Seq<Dimensions...> = Seq<Dimensions...>{})
      {
        return expressionClosure(LinearStorageTensor<T, Seq<Dimensions...> >(std::forward<T>(t)));
      }

      ///@} TensorAtoms

      ///@} Tensors

    } // NS Tensor

  } // NS ACFem

  template<class Container, class Signature>
    struct FieldTraits<ACFem::Tensor::LinearStorageTensor<Container, Signature> >
    : FieldTraits<std::decay_t<typename std::decay_t<Container>::value_type> >
  {};

} // NS Dune

#endif // __DUNE_ACFEM_TENSORS_MODULES_LINEARSTORAGE_HH__
