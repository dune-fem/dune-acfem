#ifndef __DUNE_ACFEM_TENSORS_MODULES_EYE_HH__
#define __DUNE_ACFEM_TENSORS_MODULES_EYE_HH__

#include "../tensorbase.hh"
#include "../expressiontraits.hh"
#include "constant.hh"

#ifndef SPECIALIZEDTRAITS_DEFINED
# error
#endif

namespace Dune {

  namespace ACFem {

    namespace Tensor {

      /**@addtogroup Tensors
       * @{
       */

      /**@addtogroup TensorAtoms
       * @{
       */

      template<class Seq, class Field = IntFraction<1> >
      class Eye;

      template<class Seq, class Field>
      struct HasSpecializedExpressionTraits<Eye<Seq, Field> >
        : TrueType
      {};

      template<class, class = void>
      struct IsEye
        : FalseType
      {};

      template<class T>
      struct IsEye<T, std::enable_if_t<!IsDecay<T>::value> >
        : IsEye<std::decay_t<T> >
      {};

      template<class Seq, class Field>
      struct IsEye<Eye<Seq, Field> >
        : TrueType
      {};

      /**"Identity" tensor: evaluates to 1 if all indices are the same,
       * to 0 else.
       */
      template<class Field, std::size_t... Dimensions>
      class Eye<Seq<Dimensions...>, Field>
        : public TensorBase<FloatingPointClosure<Field>, Seq<Dimensions...>, Eye<Seq<Dimensions...>, Field> >
        , public Expressions::SelfExpression<Eye<Seq<Dimensions...>, Field> >
        , public MPL::UniqueTags<ConstantExpression,
                                 ConditionalType<IsTypedValue<Field>::value, TypedValueExpression, void> >
      {
        using BaseType = TensorBase<FloatingPointClosure<Field>, Seq<Dimensions...>, Eye<Seq<Dimensions...>, Field> >;
       public:
        using BaseType::rank;
        using typename BaseType::Signature;
        using typename BaseType::FieldType;
        using ValueType = Field;

        static_assert(IsTypedValue<ValueType>::value,
                      "Eye tensors must not be build with dynamic values.");

        /**Insert the current view-indices at their proper positions and
         * foward to the underlying "host" tensor.
         */
        template<class... Dims,
                 std::enable_if_t<(sizeof...(Dims) == rank
                                   &&
                                   IsIntegralPack<Dims...>::value)
                                  , int> = 0>
        FieldType operator()(Dims... indices) const
        {
          if (!isConstant(std::forward_as_tuple(indices...))) {
            return FieldType(0);
          } else if (IsTypedValue<ValueType>::value) {
            return FieldType(ValueType{});
          } else {
            return FieldType(1);
          }
        }

        template<std::size_t... Indices, class Pos = MakeIndexSequence<sizeof...(Indices)> >
        static bool constexpr isZero(Seq<Indices...> = Seq<Indices...>{}, Pos = Pos{})
        {
          return !(isConstant(Seq<Indices...>{}));
        }

        template<std::size_t... Indices, std::enable_if_t<sizeof...(Indices) == rank, int> = 0>
        decltype(auto) operator()(Seq<Indices...> arg) const
        {
          using ReturnType = ConditionalType<isZero<Indices...>(),
                                             IntFraction<0>,
                                             ConditionalType<IsTypedValue<ValueType>::value,
                                                             ValueType,
                                                             IntFraction<1> > >;
          return ReturnType{};
        }

        static constexpr ValueType value()
        {
          return ValueType{};
        }

        std::string name() const
        {
          if (!ExpressionTraits<ValueType>::isOne) {
            return "eye<"+toString(ValueType{}) + "@" + toString(Signature{})+">";
          } else {
            return "eye<"+toString(Signature{})+">";
          }
        }

      };

      template<std::size_t... Dimensions, class F = Expressions::Closure>
      auto eye(Seq<Dimensions...>, F closure = F{})
      {
        if constexpr (sizeof...(Dimensions) > 1) {
          return closure(Eye<Seq<Dimensions...>, IntFraction<1> >{});
        } else {
          return constantTensor(1_f, Seq<Dimensions...>{}, F{});
        }
      }

      template<std::size_t... Dimensions, class F = Expressions::Closure,
               std::enable_if_t<!IsTensorOperand<F>::value, int> = 0>
      auto eye(F closure = F{})
      {
        if constexpr (sizeof...(Dimensions) > 1) {
          return closure(Eye<Seq<Dimensions...>, IntFraction<1> >{});
        } else {
          return constantTensor(1_f, Seq<Dimensions...>{}, F{});
        }
      }

      template<class T, class F = Expressions::Closure,
               std::enable_if_t<IsTensorOperand<T>::value, int> = 0>
      auto eye(T&&, F = F{})
      {
        return eye(typename TensorTraits<T>::Signature{}, F{});
      }

      ///@} TensorAtoms

      ///@} Tensors

    } // NS Tensor

    template<class Seq, class Field>
    struct ExpressionTraits<Tensor::Eye<Seq, Field> >
      : ExpressionTraits<Field>
    {
     private:
      using BaseType = ExpressionTraits<Field>;
     public:
      using ExpressionType = Tensor::Eye<Seq, Field>;
      static constexpr bool isOne = BaseType::isOne && ExpressionType::rank == 0;
      static constexpr bool isMinusOne = BaseType::isMinusOne && ExpressionType::rank == 0;
      static constexpr bool isNonZero = BaseType::isNonZero && ExpressionType::rank == 0;
      static constexpr bool isPositive = BaseType::isPositive && ExpressionType::rank == 0;
      static constexpr bool isNegative = BaseType::isNegative && ExpressionType::rank == 0;
      using Sign = ExpressionSign<isNonZero, BaseType::isSemiPositive, BaseType::isSemiNegative>;
   };

  } // NS ACFem

  template<class Field, class Signature>
  struct FieldTraits<ACFem::Tensor::Eye<Signature, Field> >
    : FieldTraits<std::decay_t<Field> >
  {};

} // NS Dune

#endif // __DUNE_ACFEM_TENSORS_MODULES_EYE_HH__
