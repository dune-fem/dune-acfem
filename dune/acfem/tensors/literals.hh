#ifndef __DUNE_ACFEM_TENSORS_LITERALS_HH__
#define __DUNE_ACFEM_TENSORS_LITERALS_HH__

#include "../common/namedconstant.hh"
#include "../common/literals.hh"
#include "modules/constant.hh"

namespace Dune
{
  namespace ACFem
  {
    namespace Tensor
    {
      namespace Symbol
      {

        namespace M
        {

          /**@addtogroup Tensors
           * @{
           */

          /**@addtogroup TensorSymbols
           *
           * Provide scalar tensors which wrap the standard math constants.
           *
           * @{
           */

          //!
          inline auto log2E = constantTensor(TypedValue::NamedConstant<double, 'L', 'O', 'G', '2', 'E'>{});

          //!
          inline auto log10E = constantTensor(TypedValue::NamedConstant<double, 'L', 'O', 'G', '1', '0', 'E'>{});

          //!
          inline auto ln2 = constantTensor(TypedValue::NamedConstant<double, 'L', 'N', '2'>{});

          //!
          inline auto ln10 = constantTensor(TypedValue::NamedConstant<double, 'L', 'N', '1', '0'>{});

          //!
          inline auto e = constantTensor(TypedValue::NamedConstant<double, 'E'>{});

          //!
          inline auto pi = constantTensor(TypedValue::NamedConstant<double, 'P', 'I'>{});

          //!
          inline auto pi_2 = constantTensor(TypedValue::NamedConstant<double, 'P', 'I', '_', '2'>{});

          //!
          inline auto pi_4 = constantTensor(TypedValue::NamedConstant<double, 'P', 'I', '_', '4'>{});

          //!
          inline auto sqrt2 = constantTensor(TypedValue::NamedConstant<double, 'S', 'Q', 'R', 'T', '2'>{});

          //!
          inline auto sqrt1_2 = constantTensor(TypedValue::NamedConstant<double, 'S', 'Q', 'R', 'T', '1', '_', '2'>{});

          //!@} TensorSymbols

          //!@} Tensors

        } // NS M

      } // NS Symbol

      using namespace Symbol;

    } // NS Tensor

    namespace Literals {

      using namespace Tensor::Symbol;

    }

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_TENSORS_LITERALS_HH__
