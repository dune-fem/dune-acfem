#ifndef __DUNE_ACFEM_TENSORS_BINARYEXPRESSION_HH__
#define __DUNE_ACFEM_TENSORS_BINARYEXPRESSION_HH__

#include "../common/literals.hh"
#include "../expressions/storage.hh"
#include "../expressions/expressionoperations.hh"

#include "tensorbase.hh"
#include "expressionoperations.hh"

namespace Dune {

  namespace ACFem {

    namespace Tensor {

      using ACFem::operator==;

      /**@addtogroup Tensors
       * @{
       */

      /**@addtogroup TensorExpressions
       * @{
       */

      template<class F, class TL, class TR>
      class BinaryTensorExpression
        : public TensorBase<typename F::template ResultType<typename TensorTraits<TL>::FieldType,
                                                            typename TensorTraits<TR>::FieldType>,
                            typename TensorTraits<TL>::Signature,
                            BinaryTensorExpression<F, TL, TR> >
        , public Expressions::Storage<F, TL, TR>
      // Expressions model rvalues. We are constant if the underlying
      // expression claims to be so or if we store a copy and the
      // underlying expression is "independent" (i.e. determined at
      // runtime only by the contained data)
      {
        using LeftTraits = TensorTraits<TL>;
        using LeftType = typename LeftTraits::TensorType;
        using RightTraits = TensorTraits<TR>;
        using RightType = typename RightTraits::TensorType;
        using Field = typename F::template ResultType<typename LeftTraits::FieldType,
                                                      typename RightTraits::FieldType>;
        using BaseType = TensorBase<Field, typename LeftTraits::Signature, BinaryTensorExpression>;
        using StorageType = Expressions::Storage<F, TL, TR>;
       public:
        using typename StorageType::FunctorType;
        using StorageType::operation;
        using StorageType::operand;
        using typename BaseType::Signature;
        using BaseType::rank;

        static_assert(IsTensor<TL>::value && IsTensor<TR>::value,
                      "At least one of the operands is not qualified for a binary tensor operation.");

        static_assert(LeftTraits::signature() == RightTraits::signature(),
                      "Signatures do not match in binary component-wise operation.");

        BinaryTensorExpression(TL&& tl, TR&& tr, F&& f)
          : StorageType(std::forward<F>(f), std::forward<TL>(tl), std::forward<TR>(tr))
        {}

        /**Allow default construction if contained types fulfill
         * IsTypedValue and the operation is not dynamic.
         */
        template<
          class... Dummy,
          std::enable_if_t<(sizeof...(Dummy) == 0
                            && IsTypedValue<TL>::value
                            && IsTypedValue<TR>::value
                            && !FunctorHas<IsDynamicOperation, F>::value
            ), int> = 0>
        BinaryTensorExpression(Dummy&&...)
          : StorageType(TL{}, TR{})
        {}

        template<class Other,
                 std::enable_if_t<(IsTensorOperand<Other>::value
                                   && BaseType::signature() == TensorTraits<Other>::signature()
                                   && std::is_assignable<decltype(F{}(std::declval<typename LeftTraits::ElementZero>(),
                                                                      std::declval<typename RightTraits::ElementZero>())),
                                                         typename TensorTraits<Other>::FieldType>::value
          ), int> = 0>
        BinaryTensorExpression& operator=(Other&& other)
        {
          forLoop<BaseType::dim()>([this,other] (auto i) {
              auto index = multiIndex(BaseType::signature(), i);
              operation()(operand(0_c)(index), operand(1_c)(index)) = std::forward<Other>(other)(index);
            });
          return *this;
        }

        template<std::size_t... Indices, class Pos = MakeIndexSequence<sizeof...(Indices)> >
        static bool constexpr isZero(Seq<Indices...> seq = Seq<Indices...>{}, Pos pos = Pos{})
        {
          // This is WRONG for more complicated stuff
          if (LeftType::isZero(seq, pos) && RightType::isZero(seq, pos)) {
            return FunctorType::signPropagation(ZeroExpressionSign{}, ZeroExpressionSign{}).isZero;
          } else if (LeftType::isZero(seq, pos)) {
            return FunctorType::signPropagation(ZeroExpressionSign{}, UnknownExpressionSign{}).isZero;
          } else if (RightType::isZero(seq, pos)) {
            return FunctorType::signPropagation(UnknownExpressionSign{}, ZeroExpressionSign{}).isZero;
          } else {
            return FunctorType::signPropagation(UnknownExpressionSign{}, UnknownExpressionSign{}).isZero;
          }
        }

        template<class... Dims,
                 std::enable_if_t<(rank == sizeof...(Dims)
                                   &&
                                   IsIntegralPack<Dims...>::value)
                                  , int> = 0>
        decltype(auto) operator()(Dims... indices)
        {
          return operation()(operand(0_c)(indices...), operand(1_c)(indices...));
        }

        template<class... Dims,
                 std::enable_if_t<(rank == sizeof...(Dims)
                                   &&
                                   IsIntegralPack<Dims...>::value)
                                  , int> = 0>
        decltype(auto) operator()(Dims... indices) const
        {
          return operation()(operand(0_c)(indices...), operand(1_c)(indices...));
        }

        template<std::size_t... Indices,
                 std::enable_if_t<(rank == sizeof...(Indices)
                                   && LeftTraits::hasMutableCallOperator
                                   && RightTraits::hasMutableCallOperator
          ), int> = 0>
        decltype(auto) operator()(Seq<Indices...>)
        {
          return operation()(operand(0_c)(Seq<Indices...>{}), operand(1_c)(Seq<Indices...>{}));
        }

        template<std::size_t... Indices,
                 std::enable_if_t<(rank == sizeof...(Indices)), int> = 0>
        decltype(auto) operator()(Seq<Indices...>) const
        {
          return operation()(operand(0_c)(Seq<Indices...>{}), operand(1_c)(Seq<Indices...>{}));
        }

        // in order to support projection optimizations we fake a non-const lookAt()
        template<class... Whatever>
        void lookAt(Whatever... indices)
        {
          operand(0_c).lookAt(indices...);
          operand(1_c).lookAt(indices...);
        }

        std::string name() const
        {
          std::string pfxL = std::is_reference<TL>::value ? (RefersConst<TL>::value ? "cref" : "ref") : "";
          std::string pfxR = std::is_reference<TR>::value ? (RefersConst<TR>::value ? "cref" : "ref") : "";

#if 0
          if constexpr (std::is_reference<TL>::value) {
            pfxL += "[@"+toString(&operand(0_c))+"]";
          }
          if constexpr (std::is_reference<TR>::value) {
            pfxR += "[@"+toString(&operand(1_c))+"]";
          }
#endif

          return operationName(operation(), pfxL+operand(0_c).name(), pfxR+operand(1_c).name());
        }
      };

      /**Generate a binary tensor expression.*/
      template<class F, class T0, class T1, std::enable_if_t<AreProperTensors<T0, T1>::value && !FunctorHas<IsTensorOperation, F>::value, int> = 0>
      constexpr auto operate(Expressions::DontOptimize, F&& f, T0&& t0, T1&& t1)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        using FType = std::decay_t<F>;
        return BinaryTensorExpression<FType, T0, T1>(std::forward<T0>(t0), std::forward<T1>(t1), FType(f));
      }

      /////////////////////////////////////////////////////////////////////////

      /// @} TensorExpressions

      /// @} Tensors

    } // NS Tensor

  } // NS ACFem

  template<class T1, class T2, class F>
  struct FieldTraits<ACFem::Tensor::BinaryTensorExpression<F, T1, T2> >
  //    : FieldTraits<typename ACFem::FieldPromotion<T1, T2>::Type>
    : FieldTraits<typename F::template ResultType<typename ACFem::TensorTraits<T1>::FieldType, typename ACFem::TensorTraits<T2>::FieldType> >
  {};

} // NS Dune

#endif // __DUNE_ACFEM_TENSORS_BINARYEXPRESSION_HH__
