#ifndef __DUNE_ACFEM_TENSORS_TENSOR_HH__
#define __DUNE_ACFEM_TENSORS_TENSOR_HH__

// include core-fixes
#include "../core-fixes.hh"

// slurp in all tensor headers.
#include "densestorage.hh"
#include "expressions.hh"
#include "expressionoperations.hh"
#include "expressiontraits.hh"
#include "literals.hh"
#include "modules.hh"
#include "operandpromotion.hh"
#include "operations.hh"
#include "operationtraits.hh"
#include "optimization.hh"
#include "ostream.hh"
#include "polynomialtraits.hh"

// interface to something "external"
#include "bindings.hh"

#endif // __DUNE_ACFEM_TENSORS_TENSOR_HH__
