#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{
  template<class Operation, class T0, class T1>
  auto testAssociativity(T0&& t0, T1&& t1)
  {
    auto f = OperationTraits<Operation>{};
    decltype(auto) expr = operate(Expressions::DontOptimize{}, f, asExpression(std::forward<T0>(t0)), asExpression(std::forward<T1>(t1)));
    decltype(auto) rightExpr = associateRight(f, std::forward<T0>(t0), std::forward<T1>(t1), Expressions::DontOptimize{});
    decltype(auto) leftExpr = associateLeft(f, std::forward<T0>(t0), std::forward<T1>(t1), Expressions::DontOptimize{});

    std::clog << "Expression:" << std::endl;
    printTensor(expr, false);
    std::clog << std::endl;


    std::clog << "Right associated Expression:" << std::endl;
    printTensor(rightExpr, false);

    auto rightDifference = rightExpr - expr;
    std::clog << "  correct: " << (rightDifference == zero(rightDifference))
              << std::endl
              << std::endl;

    std::clog << "Left associated Expression:" << std::endl;
    printTensor(leftExpr, false);

    auto leftDifference = leftExpr - expr;
    std::clog << "  correct: " << (leftDifference == zero(leftDifference))
              << std::endl
              << std::endl;
  }

  void associativeProducts()
  {
    auto ts = FieldTensor<double>(2);
    auto tones = FieldTensor<double, 2, 2, 2>(ones<2,2,2>());
    auto teye = FieldTensor<double, 2, 2, 2>(eye<2,2,2>());

    auto p0 = placeholder(tones);
    auto p1 = placeholder(teye);
    auto p2 = placeholder(teye);

    testAssociativity<EinsumOperation<Seq<>, Seq<>, Seq<> > >(ts*ts, ts);
    testAssociativity<EinsumOperation<Seq<>, Seq<>, Seq<> > >(ts, ts*ts);
    testAssociativity<EinsumOperation<Seq<2>, Seq<2>, Seq<2> > >(einsum<Seq<2>, Seq<1> >(p0, p1), p2);
    testAssociativity<EinsumOperation<Seq<0>, Seq<2>, Seq<2> > >(einsum<Seq<1>, Seq<2> >(p0, p1), p2);
    testAssociativity<EinsumOperation<Seq<2>, Seq<2>, Seq<2> > >(p0, einsum<Seq<2>, Seq<1> >(p1, p2));

    testAssociativity<TensorProductOperation<Seq<0,1>, Seq<0,1>, Seq<2,2> > >(multiply<Seq<0,1>,Seq<0,1> >(p0, p1), p2);
    testAssociativity<TensorProductOperation<Seq<0,1>, Seq<0,1>, Seq<2,2> > >(p0, multiply<Seq<0,1>,Seq<0,1> >(p1, p2));
  }
}
