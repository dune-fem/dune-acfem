#include "test-template.hh"

#include "../../expressions/treeextract.hh"
#include "../../expressions/treeoperand.hh"

namespace Dune::ACFem::Tensor::Testing
{

  template<class T, class Parent>
  struct IsSumOperand
    : BoolConstant<(IsPlusOrMinusExpression<Parent>::value
                    && !IsPlusOrMinusExpression<T>::value)>
  {};

  template<class T, class Pos, class Parent>
  using PackWithSign = MPL::TypeTuple<T, Pos, BoolConstant<IsBinaryMinusExpression<Parent>::value> >;

  void treeExtract()
  {
    auto x0 = indeterminate<0,3>();
    auto x1 = indeterminate<1,3,3>();
    auto x2 = indeterminate<2,3,3>();
    auto x3 = indeterminate<3,3,3>();

    auto e = outer(x0, x0) - x1 - (x2 - x3);

    printTensor(e);

    using E = decltype(e);
    using TreeData = Expressions::TreeExtract<IsSumOperand, decltype(e)>;
    using TreePos = Expressions::TreeExtract<IsSumOperand, decltype(e), Expressions::PackTreePosition>;
    using TreeParent = Expressions::TreeExtract<IsSumOperand, decltype(e), Expressions::PackTreeParent>;
    using TreeOperand = Expressions::TreeExtract<IsSumOperand, decltype(e), Expressions::PackTreeOperand>;
    using TreeDataWithSign = Expressions::TreeExtract<IsSumOperand, decltype(e), PackWithSign>;

    static_assert(size<TreeData>() == size<TreePos>()
                  && size<TreeData>() == size<TreeParent>()
                  && size<TreeData>() == size<TreeOperand>(),
                  "Mismatching tuple sizes.");

    std::clog << typeString((TreeData::Get<0>::Get<0>*)nullptr) << std::endl;

    forLoop<size<TreeData>()>(
      [&](auto i) {
        constexpr std::size_t I = decltype(i)::value;
        //std::clog << typeString((typename TreeData::Get<I>::template Get<0>*)nullptr) << std::endl;
        static_assert(std::is_same<typename TreeData::Get<I>::template Get<0>, TreeOperand::Get<I> >::value,
                      "Operands do not match.");

        static_assert(std::is_same<typename TreeData::Get<I>::template Get<1>, TreePos::Get<I> >::value,
                      "Operands do not match.");

        static_assert(std::is_same<typename TreeData::Get<I>::template Get<2>, TreeParent::Get<I> >::value,
                      "Operands do not match.");

        static_assert(TreeDataWithSign::Get<I>::template Get<2>::value
                      ==
                      IsBinaryMinusExpression<TreeParent::Get<I>>::value,
                      "Minus mismatch.");

        static_assert(std::is_same<TreeOperand::Get<I>, Expressions::TreeOperand<TreePos::Get<I>, E> >::value,
                      "Mismatch in TreeOperand.");

        std::clog << treeOperand(std::forward<E>(e), TreePos::Get<I>{}).name() << std::endl;
      });

  }

}
