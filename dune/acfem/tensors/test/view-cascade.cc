#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{

  void defaultTest()
  {
    FieldTensor<double, 3, 4, 5, 6> t;

    std::clog << operandPromotion<0>(intFraction<2>(), t).name() << std::endl;
    std::clog << operandPromotion<1>(intFraction<2>(), t).name() << std::endl;
    std::clog << (intFraction<2>()*t).name() << std::endl;

    std::clog << "Tensor Type: " << typeString(t) << std::endl;
    t(1, 2, 3, 4) = 5.0;
    t(1, 2, 3, 4) *= 2.0;
    std::clog << "t(1, 2, 3, 4): " << t(1, 2, 3, 4) << std::endl;
    std::clog << "rank(t): " << t.rank << std::endl;
    std::clog << "signature(t): " << typeString(decltype(t)::Signature{}) << std::endl;
  }
}
