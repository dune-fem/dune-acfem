#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{
  void mathFunctions()
  {
    printTensor(sqrt(eye<2,2>()));
    printTensor(sqr(eye<2,2>()));
    printTensor(sin(eye<2,2>()));
    printTensor(cos(eye<2,2>()));
    printTensor(tan(eye<2,2>()));
    printTensor(acos(eye<2,2>()));
    printTensor(asin(eye<2,2>()));
    printTensor(atan(eye<2,2>()));
    printTensor(exp(eye<2,2>()));
    printTensor(log(eye<2,2>()));
    printTensor(erf(eye<2,2>()));
    printTensor(lgamma(eye<2,2>()));
    printTensor(tgamma(eye<2,2>()));
    printTensor(atan2(eye<2,2>(), eye<2,2>()));
    printTensor(pow(eye<2,2>(), intFraction<3,2>()));
    printTensor(min(eye<2,2>(), eye<2,2>()));
    printTensor(max(eye<2,2>(), eye<2,2>()));
  }
}
