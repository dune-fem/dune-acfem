#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{
  void unaryMinus()
  {
    // Does it work with constant tensors?
    auto t1 = -eye<3, 3>();
    auto t2 = -zeros<3, 3>();
    auto t3 = -ones<3, 3>();

    std::clog << "-eye " << std::endl << t1 << std::endl;
    std::clog << "type(-eye): " << typeString(t1) << std::endl;
    std::clog << "-zeros " << std::endl << t2 << std::endl;
    std::clog << "type(-zeros) " << typeString(t2) << std::endl;
    std::clog << "-ones " << std::endl << t3 << std::endl;
    std::clog << "type(-ones) " << typeString(t3) << std::endl;
  }
}
