#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{
  void fieldMatrix()
  {
    auto m = Dune::FieldMatrix<double, 3, 4>(42);
    auto t1 = tensor(m);
    t1 = eye(t1);
    std::clog << typeString(t1) << std::endl;
    std::clog << "t1: " << std::endl << t1 << std::endl;

    auto t2 = tensor(Dune::FieldMatrix<double, 3, 4>(42));
    t2 = eye(t2);
    std::clog << typeString(t2) << std::endl;
    std::clog << "t2: " << std::endl << t2 << std::endl;
  }
}
