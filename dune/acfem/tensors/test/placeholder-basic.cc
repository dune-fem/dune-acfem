#include "test-template.hh"
#include "test-replacement.hh"

#include "../operations/dependson.hh"

using namespace Dune::ACFem;
using namespace Dune::ACFem::Tensor;

template<class Signature, class Field = double>
class TestPlaceholder;

template<std::size_t... Dims, class Field>
class TestPlaceholder<Seq<Dims...>, Field>
  : public PlaceholderTensor<FieldTensor<Field, Dims...>, TestPlaceholder<Seq<Dims...>, Field> >
{
  using ThisType = TestPlaceholder;
  using BaseType = PlaceholderTensor<FieldTensor<Field, Dims...>, ThisType>;
 public:
  using TensorType = FieldTensor<Field, Dims...>;

  TestPlaceholder(const TensorType& t = TensorType())
    : BaseType(TensorType(t), Expressions::Functor<BaseType>{})
  {}

  template<std::size_t Id>
  static constexpr auto order(IndexConstant<Id>)
  {
    return Id == 0 ? 42 : 0;
  }
};

template<class T, class SFINAE = void>
struct IsTestPlaceholder
  : FalseType
{};

template<class T>
struct IsTestPlaceholder<T, std::enable_if_t<!IsDecay<T>::value> >
  : IsTestPlaceholder<std::decay_t<T> >
{};

template<class Signature, class Field>
struct IsTestPlaceholder<TestPlaceholder<Signature, Field> >
  : TrueType
{};

namespace Dune {

  template<class Signature, class Field>
  struct FieldTraits<TestPlaceholder<Signature, Field> >
    : FieldTraits<Field>
  {};
}

/**Pretend we depend on the first indeterminate and generate just a new placeholder.*/
template<
  std::size_t Id, class Signature, class T,
  std::enable_if_t<(true
                    && Id == 0
                    && IsTestPlaceholder<T>::value
                     ), int> = 0>
constexpr auto doDerivative(T&& t, Dune::PriorityTag<1>)
{
  using DerivativeSignature = SequenceCat<typename TensorTraits<T>::Signature, Signature>;
  return TestPlaceholder<DerivativeSignature, typename TensorTraits<T>::FieldType>();
}

template<
  std::size_t Id, class Signature, class T,
  std::enable_if_t<(true
                    && Id != 0
                    && IsTestPlaceholder<T>::value
                     ), int> = 0>
constexpr auto doDerivative(T&& t, IndexConstant<Id> = IndexConstant<Id>{}, Signature = Signature{})
{
  using DerivativeSignature = SequenceCat<typename TensorTraits<T>::Signature, Signature>;
  return zeros(DerivativeSignature{});
}

/**Bare PlaceholderTensor is treated as constant.*/
template<std::size_t Id, class Signature, class T,
         std::enable_if_t<(true
                           && IsPlaceholderExpression<T>::value
                           && IsPlaceholderTensor<T>::value
                            ), int> = 0>
constexpr auto doDerivative(T&& t, IndexConstant<Id> = IndexConstant<Id>{}, Signature = Signature{})
{
  using DerivativeSignature = SequenceCat<typename TensorTraits<T>::Signature, Signature>;
  return zeros(DerivativeSignature{});
}

template<class Signature, class Field = double>
class TestPlaceholder2
  : public TestPlaceholder<Signature, Field>
  , public RuntimeEqualExpression
{
 public:
  using TestPlaceholder<Signature, Field>::TestPlaceholder;
};

namespace Dune {
  template<class Signature, class Field>
  struct FieldTraits<TestPlaceholder2<Signature, Field> >
    : FieldTraits<Field>
  {};
}

namespace Dune::ACFem::Tensor::Testing
{
  void placeholderBasic()
  {
    auto p0 = placeholder(eye<3,3>());
    auto p1 = placeholder(eye<3,3>());

    static_assert(!Expressions::IsBasicRuntimeEqual<decltype(p0)>::value,
                  "Placeholders must not fulfil IsBasicRuntimeEqual<> predicate.");

    static_assert(!Expressions::IsRuntimeEqual<decltype(p0)>::value,
                  "Placeholders must not fulfil IsRuntimeEqual<> predicate.");

    //testReplacement(p0 + p0, 2.0, p0, __LINE__);
    testReplacement(p0 + p0, tensor(2.0), p0, __LINE__);

    testReplacement(p0 * p1, ones<3,3>(), p0, __LINE__);
    testReplacement(p1 * p0, eye<3,3>(), p1, __LINE__);

    // This did work but shouldn't as * here is a scalar product
    // testReplacement(p1 * p0, 3.0, p1, __LINE__);

    auto p2 = TestPlaceholder<Seq<3,3> >();

    auto x0 = indeterminate<0,3>();
    auto x1 = indeterminate<1,3>();

    std::clog << "Derivative of bare placeholders should yield zero:" << std::endl;
    printTensor(derivative(p0*p1, x0));
    std::clog << "Derivative of overloaded placeholders should yield the expected result:" << std::endl;
    printTensor(derivative(p2*p2, x0));
    std::clog << "Type: " << typeString(derivative(p2*p2, x0)) << std::endl;
    std::clog << "Derivative of overloaded placeholders w.r.t. to wrong indeterminate should yield zero:" << std::endl;
    printTensor(p2*p2, true, true);
    printTensor(derivative(p2*p2, x1), true, true);

    auto p3 = TestPlaceholder2<Seq<3,3> >();

    static_assert(Expressions::IsRuntimeEqual<decltype(p3)>::value,
                  "Property-tag override failed");
    printTensor(p3+p3, true, true);
  }
}
