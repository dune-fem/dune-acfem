#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{
  void operandPromotionBasic()
  {
    FieldTensor<double, 3, 4, 5, 6> t;

    std::clog << operandPromotion<0>(intFraction<2>(), t).name() << std::endl;
    std::clog << operandPromotion<1>(intFraction<2>(), t).name() << std::endl;

    FieldMatrix<double, 1, 2> m0, m1;
    std::clog << operandPromotion<0>(m0, m1).name() << std::endl;
    std::clog << operandPromotion<1>(m0, m1).name() << std::endl;
  }
}
