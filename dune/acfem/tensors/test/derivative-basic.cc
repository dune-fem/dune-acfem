#include "test-template.hh"
#include "test-replacement.hh"

namespace Dune::ACFem::Tensor::Testing
{
  void derivativeBasic()
  {
    auto x0 = indeterminate<0,3>();
    auto x1 = indeterminate<1,3,3>();

    std::clog << std::endl << "Derivative of indet" << std::endl;
    printTensor(derivative(x0, x0));

    std::clog << std::endl << "Derivative of other indet" << std::endl;
    printTensor(derivative(x1, x0));

    std::clog << std::endl << "Derivative of scalar" << std::endl;
    printTensor(derivative(3.0, x0));

    std::clog << std::endl << "Derivative of einsum" << std::endl;
    printTensor(derivative(x0 * x0, x0));

    std::clog << std::endl << "Derivative of mixed tensor einsum" << std::endl;
    printTensor(x0 * x1);
    printTensor(derivative(x0 * x1, x1));
    printTensor(derivative(derivative(x0 * x1, x1), x0));

    std::clog << std::endl << "Derivative of tensor einsum with replaced indeterminate" << std::endl;
    testIndeterminateReplacement<0>(derivative(x0 * x1, x1), eye<3,3>(), __LINE__);
  }
}
