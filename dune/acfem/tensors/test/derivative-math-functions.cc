#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{
  template<class T, class X>
  void printDerivative(T&& t, X&& x)
  {
    printTensor(std::forward<T>(t), "Values");
    printTensor(derivative(std::forward<T>(t), std::forward<X>(x)), "Derivative");
  }

  void derivativeMathFunctions()
  {
    auto x0 = indeterminate<0>(FieldTensor<double, 3>());
    //x0 = intFraction<1,2>()*kroneckerDelta<0>(Seq<3>{});
    x0 = tensor(kroneckerDelta<0>(Seq<3>{}));

    printDerivative(sqrt(x0), x0);
    printDerivative(sqr(x0), x0);
    printDerivative(sin(x0), x0);
    printDerivative(cos(x0), x0);
    printDerivative(tan(x0), x0);
    printDerivative(acos(x0), x0);
    printDerivative(asin(x0), x0);
    printDerivative(atan(x0), x0);
    printDerivative(exp(x0), x0);
    printDerivative(log(x0), x0);
    printDerivative(erf(x0), x0);
    //printTensor(gamma(eye<2,2>());
    //printDerivative(atan2(x0, x0), x0);
    printDerivative(pow(x0, intFraction<3,2>()), x0);
  }
}
