#include <dune/common/timer.hh>

#include "test-template.hh"
#include "../bindings/dune/densevectorview.hh"

// make the compiler believe it has to emit code :)
using DuneTensorType = Dune::FieldVector<Dune::FieldVector<Dune::FieldVector<double, 5>, 4>, 3>;
DuneTensorType* O0Ptr;

using namespace Dune::ACFem::Tensor;

extern void denseVectorAssign1(const std::decay_t<decltype(eye<3,4,5>())>& t);
extern void denseVectorAssign2(const std::decay_t<decltype(eye<3,4,5>())>& t);
extern void denseVectorAssign3(const std::decay_t<decltype(eye<3,4,5>())>& t);
extern void denseVectorAssign4(const DuneTensorType& t);
extern void denseVectorAssign5(const FieldVectorTensor<DuneTensorType>& t);

namespace Dune::ACFem::Tensor::Testing
{

  void densevectorview()
  {
    // When the stuff below works we can replace the TensorResult
    // closure by a DenseVectorView which implements the
    // Dune::DenseVector interface.

    auto t = eye<3,4,5>();

    std::clog << "*** should be a Dune::DenseVector: ***" << std::endl;
    std::clog << typeString(denseVectorView(t)) << std::endl;
    std::clog << typeString(denseVectorView(t)[0]) << std::endl;
    std::clog << typeString(denseVectorView(t)[0][0]) << std::endl;
    std::clog << typeString(denseVectorView(t)[0][0][0]) << std::endl;
    std::clog << denseVectorView(t)[0][0][0] << std::endl;

    {
      auto t = blockEye<3,1,2>();
      const auto& tmp = denseVectorView(t)[0][0];
      const auto& tmp1 = denseVectorView(t)[0][0][0];

      using InnerFVType = Dune::FieldVector<Dune::FieldMatrix<double, 2, 2>, 2>;
      using OuterFVType = Dune::FieldVector<InnerFVType, 1>;
      OuterFVType fv;

      // ok, there is a type-cast operator.
      static_assert(std::is_convertible<OuterFVType, InnerFVType>::value,
                    "Attention!!! The excessive convertibility between field-vectors has been remove!");

      using DVView = std::decay_t<decltype(tmp)>;
      using DVView1 = std::decay_t<decltype(tmp1)>;

      static_assert(!std::is_convertible<DVView, InnerFVType>::value,
                    "Excessive convertibility detectd.");

      static_assert(!std::is_convertible<DVView, DVView1>::value,
                    "Excessive convertibility beetween DenseVectorView's detectd.");

      fv = tmp;
    }
#if 1
    // works after adding operator=() to ExplicitFieldVector
    {
      // eye<3,4,5>
      static_assert(IsFieldVectorSizeCorrect<std::decay_t<decltype(denseVectorView(t)[0][0])>, 5>::value,
                    "blah");
      Fem::ExplicitFieldVector<double, 5> fv1 = denseVectorView(t)[0][0];
      fv1 =  denseVectorView(t)[0][0];
      std::clog << fv1 << std::endl;
      Fem::ExplicitFieldVector<Fem::ExplicitFieldVector<double, 5>, 4> fv2  = denseVectorView(t)[0];
      fv2 = denseVectorView(t)[0];
      std::clog << fv2 << std::endl;
      Fem::ExplicitFieldVector<Fem::ExplicitFieldVector<Fem::ExplicitFieldVector<double, 5>, 4>, 3> fv3 = denseVectorView(t);
      fv3 = denseVectorView(t);
      std::clog << fv3 << std::endl;
    }
#endif
#if 1
    {
      Dune::FieldVector<double, 5> fv1 = denseVectorView(t)[0][0];
      fv1 =  denseVectorView(t)[0][0];
      std::clog << fv1 << std::endl;

      Dune::FieldVector<Dune::FieldVector<double, 5>, 4> fv2 = denseVectorView(t)[0];
      fv2 = denseVectorView(t)[0];
      std::clog << fv2 << std::endl;

      using Type = Dune::FieldVector<Dune::FieldVector<Dune::FieldVector<double, 5>, 4>, 3>;
      Type tmp;
      Type fv3 = tmp;
      asm volatile("BLAHBLAHBLAH1:");
      fv3 = denseVectorView(t);
      asm volatile("BLAHBLAHBLAH2:");
      fv3 = t;
      std::clog << "t: " << std::endl << t << std::endl;
      std::clog << "fvt: " << std::endl << tensor(fv3) << std::endl;
      asm volatile("BLAHBLAHBLAH3:");
      std::clog << fv3 << std::endl;

      constexpr std::size_t count = 10000000; // 100000000;

      O0Ptr = &fv3;
      Timer timer;
      for(std::size_t i = 0; i < count; ++i) {
        asm volatile ("Assign1Marker:");
        denseVectorAssign1(t);
        asm volatile ("Assign1MarkerEnd:");
      }
      auto elapsed1 = timer.elapsed();
      timer.stop();
      // the actual value does not matter
      std::cout << "dense vector assign: " << elapsed1 << std::endl;

      timer.reset();
      timer.start();
      for(std::size_t i = 0; i < count; ++i) {
        asm volatile ("Assign2Marker:");
        denseVectorAssign2(t);
        asm volatile ("Assign2MarkerEnd:");
      }
      auto elapsed2 = timer.elapsed();
      timer.stop();
      // the actual value does not matter
      std::cout << "tmp dense assign: " << elapsed2 << std::endl;

      timer.reset();
      timer.start();
      for(std::size_t i = 0; i < count; ++i) {
        asm volatile ("Assign3Marker:");
        denseVectorAssign3(t);
        asm volatile ("Assign3MarkerEnd:");
      }
      auto elapsed3 = timer.elapsed();
      timer.stop();
      // the actual value does not matter
      std::cout << "cast assign: " << elapsed3 << std::endl;

      auto fv3tmp = fv3;
      timer.reset();
      timer.start();
      for(std::size_t i = 0; i < count; ++i) {
        asm volatile ("Assign4Marker:");
        denseVectorAssign4(fv3tmp);
        asm volatile ("Assign4MarkerEnd:");
      }
      auto elapsed4 = timer.elapsed();
      timer.stop();
      // the actual value does not matter
      std::cout << "native assign: " << elapsed4 << std::endl;

      auto fvt = fieldTensor(std::move(fv3));
      timer.reset();
      timer.start();
      for(std::size_t i = 0; i < count; ++i) {
        asm volatile ("Assign5Marker:");
        denseVectorAssign5(fvt);
        asm volatile ("Assign5MarkerEnd:");
      }
      auto elapsed5 = timer.elapsed();
      timer.stop();
      // the actual value does not matter
      std::cout << "fvtensor assign: " << elapsed5 << std::endl;

      // Output comparisons, elapsed2 should be best
      //std::clog << "Temporay dense assign is best:" << (elapsed2 < std::min(elapsed1, elapsed3)) << std::endl;
    }
#endif
    {
      Dune::FieldMatrix<double, 4, 5> fm;
      Dune::FieldVector<FieldVector<double, 5>, 4> fv2;
      fm = fv2;

//      Dune::DenseMatrixAssigner<decltype(fm), decltype(denseVectorView(t)[0])>::apply(fm, denseVectorView(t)[0]);

      fm = denseVectorView(t)[0];

      Dune::FieldVector<Dune::FieldMatrix<double, 4, 5>, 3> fmv;
      Dune::FieldVector<Dune::FieldVector<Dune::FieldVector<double, 5>, 4>, 3> fv3;

      fmv = fv3;
      fmv = t;// denseVectorView(t); //ambiguous overload for operator=

      auto t2 = kroneckerDelta<0,0,0,0>(Seq<1,1,2,2>{});

      FieldVector<FieldVector<FieldMatrix<double, 2, 2>, 1>, 1> fv4;
      fv4 = t2;

      Fem::ExplicitFieldVector<Fem::ExplicitFieldVector<FieldMatrix<double, 2, 2>, 1>, 1> fv5;
      std::clog << "assignable: " << std::is_convertible<decltype(t2), decltype(fv4)>{} << std::endl;
      std::clog << "assignable: " << std::is_convertible<decltype(t2), decltype(fv5)>{} << std::endl;
      fv5 = t2;
    }
    {
      Dune::FieldMatrix<double, 4, 5> fm;
      Dune::Fem::ExplicitFieldVector<FieldVector<double, 5>, 4> fv2;
      fm = fv2;
      fm = denseVectorView(t)[0];

      Dune::Fem::ExplicitFieldVector<Dune::FieldMatrix<double, 4, 5>, 3> fmv;
      Dune::Fem::ExplicitFieldVector<Dune::FieldVector<FieldVector<double, 5>, 4>, 3> fv3;

      fmv = fv3;
      fmv = denseVectorView(t);
    }
    {
      //Dune::Fem::ExplicitFieldVector<Dune::FieldMatrix<double, 2, 2>, 1> hm(0);
      Dune::FieldMatrix<double, 2, 2> m22(0);
      Dune::FieldVector<Dune::FieldMatrix<double, 2, 2>, 1> hm(0);
      Dune::FieldMatrix<double, 1, 2> fm(0);
      Dune::FieldVector<double, 2> fv(0);
      auto fmt = fieldTensor(std::move(fm));
      fmt = eye<1,2>();
      fm = fmt;
      std::clog << "fm: " << fm << std::endl;
      //fv = fmt; // this does not work
      //hm = fmt;
      //m22 = fmt;
      //m22 = fm;
      std::clog << "hm: " << hm << std::endl;

      int ambiguous(const Fem::ExplicitFieldVector<Dune::FieldMatrix<double, 2, 2>, 1>&);
      int ambiguous(const Fem::ExplicitFieldVector<double, 2>&);
      int ambiguous(const FieldMatrix<double, 1, 2>&);

      (void)sizeof(decltype(ambiguous(hm)));
      (void)sizeof(decltype(ambiguous(fv)));
      (void)sizeof(decltype(ambiguous(fm)));
      //(void)sizeof(decltype(ambiguous(fmt)));
    }
#if 0
    {
      Dune::FieldMatrix<double, 1, 1> fm;
      Dune::FieldVector<double, 1> fv;
      auto fmt = fieldTensor(std::move(fm));
      fm = fmt;
//      fv = fmt;

      int ambiguous(const Fem::ExplicitFieldVector<double, 1>&);
      int ambiguous(const FieldMatrix<double, 1, 1>&);

      sizeof(decltype(ambiguous(fv)));
      sizeof(decltype(ambiguous(fm)));
      sizeof(decltype(ambiguous(fmt)));
    }
#endif
  }
}
