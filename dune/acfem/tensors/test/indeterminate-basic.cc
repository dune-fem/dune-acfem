#include "test-template.hh"
#include "test-replacement.hh"

namespace Dune::ACFem::Tensor::Testing
{
  void indeterminateBasic()
  {
    auto x0 = indeterminate<0>(eye<3,3>());
    auto x1 = indeterminate<1,3,3>();
    auto x1_ = indeterminate<1>(eye<3,3>());

    // This will no longer work.
    //testReplacement(x0 + x0, 2.0, x0, __LINE__);
    testReplacement(x0 + x0, tensor(2.0), x0, __LINE__);

    testReplacement(x0 * x1, ones<3,3>(), x0, __LINE__);
    testReplacement(x1 * x0, eye<3,3>(), x1, __LINE__);

    // This will no longer work.
    // testReplacement(x1 * x0, 3.0, x1, __LINE__);

    testIndeterminateReplacement<0>(x0 * x1_, eye<3,3>(), __LINE__);
    testIndeterminateReplacement<1>(x1 * x1_, eye<3,3>(), __LINE__);

    std::clog << typeString(x0 * x1) << std::endl;
    std::clog << typeString(deepCopy(x0 * x1)) << std::endl;

    testIndeterminateReplacement<0>(x0, asExpression(x0), __LINE__);
    testIndeterminateReplacement<1>(asExpression(x1), asExpression(x0), __LINE__);

    decltype(auto) result = Expressions::replaceIndeterminate<1>(asExpression(x1), asExpression(x0));
    std::clog << "Preserve Atoms: " << ((void *)&asExpression(x0) == (void *)&asExpression(result)) << std::endl;
  }
}
