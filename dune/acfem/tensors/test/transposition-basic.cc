#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{
  void transposition()
  {
    auto t1 = transpose<1, 0>(kroneckerDelta(Seq<3,3>{}, Seq<0, 2>{}));
    std::clog << "Name: " << t1.name() << std::endl;
    std::clog << "SelfTr: " << isSelfTransposed<1,0>(t1) << std::endl;
    std::clog << "Perm-Matrix: " << std::endl << t1 << std::endl;
    std::clog << "Type: " << typeString(t1) << std::endl << std::endl;

    auto t2 = transpose<1, 0>(kroneckerDelta(Seq<3,3>{}, Seq<0, 1>{}));
    std::clog << "Name: " << t2.name() << std::endl;
    std::clog << "SelfTr: " << isSelfTransposed<1,0>(t2) << std::endl;
    std::clog << "Perm-Matrix: " << std::endl << t2 << std::endl;
    std::clog << "Type: " << typeString(t2) << std::endl << std::endl;

    auto t3 = transpose(kroneckerDelta(Seq<3,3>{}, Seq<1, 1>{}));
    std::clog << "Name: " << t3.name() << std::endl;
    std::clog << "SelfTr: " << isSelfTransposed(t3) << std::endl;
    std::clog << "Perm-Matrix: " << std::endl << t3 << std::endl;
    std::clog << "Type: " << typeString(t3) << std::endl << std::endl;

    auto t4 = transpose<1,0>(transpose<0,2,1>(fieldTensor(eye<3,3,3>())));
    std::clog << "Name: " << t4.name() << std::endl;
    std::clog << "SelfTr: " << isSelfTransposed<1,0>(t4) << std::endl;
    std::clog << "Perm-Tensor: " << std::endl << t4 << std::endl;
    std::clog << "Type: " << typeString(t4) << std::endl << std::endl;

    auto t5 = transpose(kroneckerDelta(Seq<3,3>{}, Seq<1, 1>{}) + eye<3,3>());
    std::clog << "Name: " << t5.name() << std::endl;
    std::clog << "SelfTr: " << isSelfTransposed(t5) << std::endl;
    std::clog << "Perm-Matrix: " << std::endl << t5 << std::endl;
    std::clog << "Type: " << typeString(t5) << std::endl << std::endl;

    auto t6 = transpose(3*kroneckerDelta(Seq<3,3>{}, Seq<1, 1>{}));
    std::clog << "Name: " << t6.name() << std::endl;
    std::clog << "SelfTr: " << isSelfTransposed(t6) << std::endl;
    std::clog << "Perm-Matrix: " << std::endl << t6 << std::endl;
    std::clog << "Type: " << typeString(t6) << std::endl << std::endl;

    auto t7 = transpose(sin(kroneckerDelta(Seq<3,3>{}, Seq<1, 1>{})));
    std::clog << "Name: " << t7.name() << std::endl;
    std::clog << "SelfTr: " << isSelfTransposed(t7) << std::endl;
    std::clog << "Perm-Matrix: " << std::endl << t7 << std::endl;
    std::clog << "Type: " << typeString(t7) << std::endl << std::endl;

    auto t8 = transpose<0,2,1>(-FieldTensor<double, 3, 3, 3>(eye<3,3,3>()));
    std::clog << "Name: " << t8.name() << std::endl;
    std::clog << "SelfTr: " << isSelfTransposed(t8) << std::endl;
    std::clog << "Perm-Tensor: " << std::endl << t8 << std::endl;
    std::clog << "Type: " << typeString(t8) << std::endl << std::endl;
  }
}
