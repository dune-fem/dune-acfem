#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{
  void constantAccess()
  {
    auto m = FieldTensor<double, 3, 3>();
    auto v = FieldTensor<double, 3>();
    static_assert(IsTensor<decltype(m)>::value, "blah");
    m = ones(m);
    for (unsigned i = 0; i < v.dim<0>(); ++i) {
      v(i) = i+1;
    }
    auto t = einsum<Seq<0, 1>, Seq<0, 1> >(eye<3, 3, 3>(), einsum<Seq<>, Seq<> >(v, m));
    std::clog << "DiagMult: " << std::endl << t << std::endl;
    std::clog << "Rank: " << t.rank << std::endl;
    //std::clog << "Type: " << typeString(t) << std::endl;
    std::clog << "Dynamic  Access to (1,2): " << t(1, 2) << std::endl;
    std::clog << "Constant Access to (1,2): " << t(Seq<1, 2>{}) << std::endl;
  }
}
