#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{
  void derivativeCos()
  {
    std::clog << "********** derivatives of scalar cos **********" << std::endl;
    auto xs = indeterminate<0>(tensor(3.0));
    printTensor(cos(xs));
    printTensor(derivative(cos(xs), xs));
    printTensor(derivative<2>(cos(xs), xs));
    printTensor(derivative<1,1>(cos(xs), xs, xs));

    std::clog << "********** derivatives of componentwise cos **********" << std::endl;
    auto x0 = indeterminate<0,3>();
    printTensor(cos(x0));
    printTensor(derivative(cos(x0), x0));
    printTensor(derivative<2>(cos(x0), x0));
    printTensor(derivative<1,1>(cos(x0), x0, x0));
    printTensor(derivative<3>(cos(x0), x0));
  }
}
