#ifndef __DUNE_ACFEM_TENSORS_TEST_DERIVATIVE_COS_PRODUCT_HH__
#define __DUNE_ACFEM_TENSORS_TEST_DERIVATIVE_COS_PRODUCT_HH__

#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{
  template<std::size_t DIM>
  void derivativeCosProduct()
  {
    auto X = indeterminate<0, DIM>();

    auto u0 = multiplyLoop<DIM>(1_f, [&X](auto i) {
        return cos(2_f*M::pi*X[i]);
      });

    printTensor(u0, std::string("Values"));

    printTensor(derivative(std::move(u0), X), std::string("First Deriviatives"));

    printTensor(derivative<2>(std::move(u0), X), std::string("Second Deriviatives"));

    printTensor(trace(derivative<2>(std::move(u0), X)), std::string("Laplacian"));
//    printTensor(derivative<3>(std::move(u0), X), std::string("Third Deriviatives"));
//    printTensor(derivative<4>(std::move(u0), X), std::string("Fourth Deriviatives"));
    std::clog << std::endl;
  }

}

#endif // __DUNE_ACFEM_TENSORS_TEST_DERIVATIVE_COS_PRODUCT_HH__
