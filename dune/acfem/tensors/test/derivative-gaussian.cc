#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{
  void derivativeGaussian()
  {
    // test the derivatives of the standard "ellipt" example
    auto x0 = indeterminate<0,3>();

#if 1
    auto C0 = fieldTensor(0.5);
    auto u0 = exp(-C0*x0*x0);
    std::clog << "Gaussian with factor " << C0.name() << std::endl;
    printTensor(u0, std::string("Values"));
    printTensor(derivative(std::move(u0), x0), std::string("First Deriviatives"));
    printTensor(derivative<2>(std::move(u0), x0), std::string("Second Deriviatives"));
    printTensor(trace(derivative<2>(std::move(u0), x0)), std::string("Laplacian"));
    //printTensor(derivative<3>(std::move(u0), x0), std::string("Third Deriviatives"));
    //printTensor(derivative<4>(std::move(u0), x0), std::string("Fourth Deriviatives"));
#endif

#if 1
    auto C1 = 1_f / 2_f;
    auto u1 = exp(-C1*x0*x0);
    std::clog << "Gaussian with factor " << C1 << std::endl;
    printTensor(u1, std::string("Values"));
    printTensor(derivative(std::move(u1), x0), std::string("First Deriviatives"));
    printTensor(derivative<2>(std::move(u1), x0), std::string("Second Deriviatives"));
    printTensor(trace(derivative<2>(std::move(u1), x0)), std::string("Laplacian"));
    //printTensor(derivative<3>(std::move(u1), x0), std::string("Third Deriviatives"));
    //printTensor(derivative<4>(std::move(u1), x0), std::string("Fourth Deriviatives"));
    std::clog << std::endl;
#endif

#if 1
    auto C2 = 0.5;
    auto u2 = exp(-std::move(C2)*x0*x0);
    std::clog << "Gaussian with factor " << C2 << std::endl;
    printTensor(u2, std::string("Values"));
    printTensor(derivative(std::move(u2), x0), std::string("First Deriviatives"));
    printTensor(derivative<2>(std::move(u2), x0), std::string("Second Deriviatives"));
    printTensor(trace(derivative<2>(std::move(u2), x0)), std::string("Laplacian"));
    //printTensor(derivative<3>(std::move(u2), x0), std::string("Third Deriviatives"));
    //printTensor(derivative<4>(std::move(u2), x0), std::string("Fourth Deriviatives"));
    std::clog << std::endl;
#endif
  }
}
