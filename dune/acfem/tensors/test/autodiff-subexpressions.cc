#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{
  void autodiffSubexpressions()
  {
    auto x0 = indeterminate<0>(FieldTensor<double, 3>());
    auto lengthX0 = sqrt(x0*x0);

    auto expr = lengthX0*x0;
    printTensor(expr);

    auto cascadeAD = autoDiffCSECascade<3>(expr, x0);

    forLoop<cascadeAD.depth()>([&](auto i) {
	constexpr size_t depth = cascadeAD.depth() - i.value - 1;
        std::clog << "****** depth " << depth << std::endl;
        forEach(cascadeAD.get<depth>(),[&](auto&& t) { // mind the &
	    std::clog << "  Tensor: " << t.name() << std::endl;
            x0 = 2*kroneckerDelta<0>(Seq<3>{});
            std::clog << "  Evaluate at " << x0.name() << std::endl;
            evaluateSubExpression(t, cascadeAD.sExpTraits());
	    printTensor(t);
          });
      });

    std::clog << std::endl;
    std::clog << "**** Order 1 eval of cascade:" << std::endl;
    forLoop<cascadeAD.depth()>([&](auto i) {
        constexpr size_t depth = cascadeAD.depth() - i.value - 1;
        std::clog << "****** depth " << depth << std::endl;
        forEach(cascadeAD.get<depth>(),[&](auto&& t) { // mind the &
            std::clog << "  Tensor: " << t.name() << std::endl;
            x0 = intFraction<1,2>()*kroneckerDelta<0>(Seq<3>{});
            std::clog << "  Evaluate at " << x0.name() << std::endl;
            evaluateSubExpression(t, cascadeAD.sExpTraits().upTo(1_c));
            printTensor(t);
          });
      });

  }
}
