#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{
  void restrictionCascade()
  {
    FieldTensor<double, 3, 4, 5, 6> t;
    auto v1 = restriction<0, 2>(std::move(t));
    auto v2 = restriction<1>(std::move(v1));
    auto v3 = restriction<0>(std::move(v2));

    // This only works if the optimizer joins adjacent "restriction" operations.
    v3->lookAt(1, 1, 1, 1);

    std::clog << "Looking at: " << v3->lookAt() << std::endl;
    v3() = 42.;

    std::clog << "View 1: " << typeString(v1) << std::endl;
    std::clog << "View 2: " << typeString(v2) << std::endl;
    std::clog << "View 3: " << typeString(v3) << std::endl;

    std::clog << "v3() / t(1, 1, 1, 1) " << v3() << " / " << t(1, 1, 1, 1) << std::endl;
    t(1, 1, 1, 1) = 3.0;
    std::clog << "v3 / t(1, 1, 1, 1) " << v3 << " / " << t(1, 1, 1, 1) << std::endl;
  }
}
