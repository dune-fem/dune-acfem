#ifndef __DUNE_ACFEM_TENSORS_TEST_TEST_REPLACEMENT_HH__
#define __DUNE_ACFEM_TENSORS_TEST_TEST_REPLACEMENT_HH__

#include "../../common/ostream.hh"
#include "../../expressions/polynomialtraits.hh"
#include "../tensor.hh"

namespace Dune
{
  namespace ACFem
  {
    namespace Tensor
    {
      namespace Testing
      {
        template<class From, class To, class E>
        auto testReplacement(E&& e, To&& to, From&& from, int line)
        {
          std::string pfx = ""; //toString(line) + " ";
          std::clog << pfx+"testReplacement()" << std::endl;
          std::clog << std::endl << pfx+"From:" << std::endl;
          printTensor(from);
          std::clog << std::endl << pfx+"To:" << std::endl;
          printTensor(to);
          std::clog << std::endl << pfx+"Expression:" << std::endl;
          printTensor(e);

          auto result = replace(std::forward<E>(e), std::forward<To>(to), std::forward<From>(from));

          std::clog << std::endl << pfx+"Replaced: " << std::endl;
          printTensor(result);

#if 0
          using Expr = Expressions::AsExpression<decltype(result)>;
          if constexpr (Arity<Expr>::value == 2) {
            auto expr = asExpression(result);
            std::clog << "arg0 " << expr.operand(0_c).name() << std::endl;
            std::clog << " const: " << IsConstantExprArg<Operand<0, Expr> >::value << std::endl;
            std::clog << "arg1 " << expr.operand(1_c).name() << std::endl;
            std::clog << " const: " << IsConstantExprArg<Operand<0, Expr> >::value << std::endl;
          }
#endif

          std::clog << std::endl;
          std::clog << pfx+"Original Type:     " << typeString(std::forward<E>(e)) << std::endl;
          std::clog << pfx+"Injected Type:     " << typeString(std::forward<To>(to)) << std::endl;
          std::clog << pfx+"Replacement Type:  " << typeString(result) << std::endl;
          std::clog << pfx+"Replacement Value: " << std::endl << result << std::endl;
          std::clog << std::endl;
        }

        template<std::size_t Id, class To, class E>
        decltype(auto) testIndeterminateReplacement(E&& e, To&& to, int line, IndexConstant<Id> = IndexConstant<Id>{})
        {
          std::string pfx = ""; //toString(line) + " ";
          std::clog << pfx+"testIndeterminateReplacement<" << Id << ">()" << std::endl;
          std::clog << std::endl << pfx+"Replace all indeterminants with id " << Id << std::endl;
          std::clog << std::endl << pfx+"To:" << std::endl;
          printTensor(to);
          std::clog << std::endl << pfx+"Expression:" << std::endl;
          printTensor(e);

          decltype(auto) result = Expressions::replaceIndeterminate<Id>(std::forward<E>(e), std::forward<To>(to));

          std::clog << std::endl << "Replaced: " << std::endl;
          printTensor(result);

          std::clog << std::endl;
          std::clog << pfx+"Original Type:     " << typeString(std::forward<E>(e)) << std::endl;
          std::clog << pfx+"Injected Type:     " << typeString(std::forward<To>(to)) << std::endl;
          std::clog << pfx+"Replacement Type:  " << typeString(result) << std::endl;
          std::clog << pfx+"Replacement Value: " << std::endl << result << std::endl;
          std::clog << std::endl;
        }

        template<class T1, class T2, class Pos1 = FalseType, class Pos2 = FalseType >
        auto testProductOptimization(T1&& t1, T2&& t2, Pos1 = Pos1{}, Pos2 = Pos2{})
        {
          constexpr std::size_t commonDims = CommonHead<typename TensorTraits<T1>::Signature,
                                                        typename TensorTraits<T2>::Signature>{};
          using Contract1 = ConditionalType<std::is_same<Pos1, FalseType >::value,
                                            MakeIndexSequence<commonDims>,
                                            Pos1>;
          using Contract2 = ConditionalType<std::is_same<Pos2, FalseType >::value,
                                            MakeIndexSequence<commonDims>,
                                            Pos2>;

          std::clog << "Sig1: " << TensorTraits<T1>::signature() << std::endl;
          std::clog << "Contract1: " << Contract1{} << std::endl;
          std::clog << "Sig2: " << TensorTraits<T2>::signature() << std::endl;
          std::clog << "Contract2: " << Contract2{} << std::endl;

          decltype(auto) result = multiply<Contract1, Contract2>(std::forward<T1>(t1), std::forward<T2>(t2));

          std::clog << Tensor::name(std::forward<T1>(t1)) << " . " << Tensor::name(std::forward<T2>(t2))
                    << " -> "
                    << result.name()
                    << std::endl
                    << "Signature: " << result.signature()
                    << std::endl
                    << result
                    << std::endl
                    << typeString(result)
                    << std::endl;
          auto fvt1 = fieldTensor(std::forward<T1>(t1));
          auto fvt2 = fieldTensor(std::forward<T2>(t2));
          auto difference = result - multiply<Contract1, Contract2>(fvt1, fvt2);
          std::clog << "Optimization is correct: " << (difference == zero(difference))
                    << std::endl
                    << std::endl;
          if (difference != zero(difference))  {
            std::clog << "  l RTEQ: " << Expressions::AreRuntimeEqual<decltype(result), decltype(fvt1)>::value << std::endl;
            std::clog << "  r RTEQ: " << Expressions::AreRuntimeEqual<decltype(result), decltype(fvt2)>::value << std::endl;
            std::clog << "  Difference:" << std::endl;
            printTensor(difference);
          }
        }

        template<class T1, class T2, class Pos1 = FalseType, class Pos2 = FalseType >
        auto testEinsumOptimization(T1&& t1, T2&& t2, Pos1 = Pos1{}, Pos2 = Pos2{})
        {
          constexpr std::size_t commonDims = CommonHead<typename TensorTraits<T1>::Signature,
                                                        typename TensorTraits<T2>::Signature>{};
          using Contract1 = ConditionalType<std::is_same<Pos1, FalseType >::value,
                                            MakeIndexSequence<commonDims>,
                                            Pos1>;
          using Contract2 = ConditionalType<std::is_same<Pos2, FalseType >::value,
                                            MakeIndexSequence<commonDims>,
                                            Pos2>;

          std::clog << "Sig1: " << TensorTraits<T1>::signature() << std::endl;
          std::clog << "Contract1: " << Contract1{} << std::endl;
          std::clog << "Sig2: " << TensorTraits<T2>::signature() << std::endl;
          std::clog << "Contract2: " << Contract2{} << std::endl;

          decltype(auto) result = einsum<Contract1, Contract2>(std::forward<T1>(t1), std::forward<T2>(t2));

          std::clog << Tensor::name(std::forward<T1>(t1)) << " * " << Tensor::name(std::forward<T2>(t2))
                    << " -> "
                    << result.name()
                    << std::endl
                    << "Signature: " << result.signature()
                    << std::endl
                    << result
                    << std::endl
                    << typeString(result)
                    << std::endl;
          auto fvt1 = fieldTensor(std::forward<T1>(t1));
          auto fvt2 = fieldTensor(std::forward<T2>(t2));
          auto difference = result - einsum<Contract1, Contract2>(fvt1, fvt2);
          std::clog << "Optimization is correct: " << (difference == zero(difference))
                    << std::endl
                    << std::endl;
          if (difference != zero(difference))  {
            std::clog << "  l RTEQ: " << Expressions::AreRuntimeEqual<decltype(result), decltype(fvt1)>::value << std::endl;
            std::clog << "  r RTEQ: " << Expressions::AreRuntimeEqual<decltype(result), decltype(fvt2)>::value << std::endl;
            std::clog << "  Difference:" << std::endl;
            printTensor(difference);
          }
        }

      } // NS Testing
    } // NS Tensor
  } // NS ACFem
} // NS Dune

#endif // header guard
