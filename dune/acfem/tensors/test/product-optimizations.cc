#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{
  template<class T1, class T2, class Pos1 = BoolConstant<false>, class Pos2 = BoolConstant<false> >
  auto testProductOptimization(T1&& t1, T2&& t2, Pos1 = Pos1{}, Pos2 = Pos2{})
  {
    constexpr std::size_t commonDims = CommonHead<typename TensorTraits<T1>::Signature,
                                                  typename TensorTraits<T2>::Signature>::value;
    using Contract1 = ConditionalType<std::is_same<Pos1, BoolConstant<false> >::value,
                                      MakeIndexSequence<commonDims>,
                                      Pos1>;
    using Contract2 = ConditionalType<std::is_same<Pos2, BoolConstant<false> >::value,
                                      MakeIndexSequence<commonDims>,
                                      Pos2>;

    std::clog << "Sig1: " << TensorTraits<T1>::signature() << std::endl;
    std::clog << "Contract1: " << Contract1{} << std::endl;
    std::clog << "Sig2: " << TensorTraits<T2>::signature() << std::endl;
    std::clog << "Contract2: " << Contract2{} << std::endl;

    decltype(auto) result = multiply<Contract1, Contract2>(std::forward<T1>(t1), std::forward<T2>(t2));

    std::clog << Tensor::name(std::forward<T1>(t1)) << " . " << Tensor::name(std::forward<T2>(t2))
              << " -> "
              << result.name()
              << std::endl
              << "Signature: " << result.signature()
              << std::endl
              << result
              << std::endl
              << typeString(result)
              << std::endl;
    auto fvt1 = fieldTensor(std::forward<T1>(t1));
    auto fvt2 = fieldTensor(std::forward<T2>(t2));
    auto difference = result - multiply<Contract1, Contract2>(fvt1, fvt2);
    std::clog << "Optimization is correct: " << (difference == zero(difference))
              << std::endl
              << std::endl;
    if (difference != zero(difference))  {
      std::clog << "  l RTEQ: " << Expressions::AreRuntimeEqual<decltype(result), decltype(fvt1)>::value << std::endl;
      std::clog << "  r RTEQ: " << Expressions::AreRuntimeEqual<decltype(result), decltype(fvt2)>::value << std::endl;
      std::clog << "  Difference:" << std::endl;
      printTensor(difference);
    }
  }

  void productOptimizations()
  {
    testProductOptimization(zeros<3,4>(), eye<3,4,5,6>());
    testProductOptimization(eye<3,4,5,6>(), zeros<3,4>());
    testProductOptimization(ones<3,4>(), eye<3,4,5,6>());
    testProductOptimization(eye<3,4,5,6>(), ones<3,4>());
    testProductOptimization(ones<3,4>(), ones<3,4>());
    testProductOptimization(ones<3,4>(), FieldTensor<double,3,4,5,6>());
    testProductOptimization(FieldTensor<double,3,4,5,6>(), ones<3,4>());
    testProductOptimization(eye<3,3,3>(), eye<3,3,4>());
    testProductOptimization(blockEye<2,3,4>(), blockEye<3,3,4>());

    // The following should be converted to einsum
    testProductOptimization(3, eye<2,2>());
    testProductOptimization(eye<2,2>(), 3);
    // testProductOptimization(3, 3); // should yield a constant FieldTensor (via einsum ...)
  }
}
