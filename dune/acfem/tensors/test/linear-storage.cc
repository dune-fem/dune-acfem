#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{

  void linearStorage()
  {
    {
      auto t0 = linearStorageTensor<3, 4, 5, 6>(std::vector<double>());
      FieldTensor<double, 3, 4, 5, 6> t1;

      t0 = eye<3, 4, 5, 6>();
      t1 = eye<3, 4, 5, 6>();

      printTensor(t0, std::string("LinearStorageTensor"));
      printTensor(t1, std::string("FieldTensor"));

      if (t0 != t1) {
        DUNE_THROW(Dune::MathError,
                   "t0 != t1, "
                   + typeString(t0)
                   + ", "
                   + typeString(t1));
      }
    }
    {
      auto t0 = linearStorageTensor<3, 4, 5, 6>(std::array<double, 3*4*5*6>());
      FieldTensor<double, 3, 4, 5, 6> t1;

      t0 = eye<3, 4, 5, 6>();
      t1 = eye<3, 4, 5, 6>();

      printTensor(t0, std::string("LinearStorageTensor"));
      printTensor(t1, std::string("FieldTensor"));

      if (t0 != t1) {
        DUNE_THROW(Dune::MathError,
                   "t0 != t1, "
                   + typeString(t0)
                   + ", "
                   + typeString(t1));
      }
    }
    {
      auto t0 = linearStorageTensor<3, 4, 5, 6>(Dune::FieldVector<double, 3*4*5*6>());
      FieldTensor<double, 3, 4, 5, 6> t1;

      t0 = eye<3, 4, 5, 6>();
      t1 = eye<3, 4, 5, 6>();

      printTensor(t0, std::string("LinearStorageTensor"));
      printTensor(t1, std::string("FieldTensor"));

      if (t0 != t1) {
        DUNE_THROW(Dune::MathError,
                   "t0 != t1, "
                   + typeString(t0)
                   + ", "
                   + typeString(t1));
      }
    }
    {
      auto storage = Dune::FieldVector<double, 3*4*5*6>();
      auto t0 = linearStorageTensor<3, 4, 5, 6>(storage);
      FieldTensor<double, 3, 4, 5, 6> t1;

      t0 = eye<3, 4, 5, 6>();
      t1 = eye<3, 4, 5, 6>();

      printTensor(t0, std::string("LinearStorageTensor"));
      printTensor(t1, std::string("FieldTensor"));

      if (t0 != t1) {
        DUNE_THROW(Dune::MathError,
                   "t0 != t1, "
                   + typeString(t0)
                   + ", "
                   + typeString(t1));
      }
    }
  }
}
