#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{
  template<class T1, class T2, class Pos1 = BoolConstant<false>, class Pos2 = BoolConstant<false> >
  auto testEinsumOptimization(T1&& t1, T2&& t2, Pos1 = Pos1{}, Pos2 = Pos2{})
  {
    constexpr std::size_t commonDims = CommonHead<typename TensorTraits<T1>::Signature,
                                                  typename TensorTraits<T2>::Signature>::value;
    using Contract1 = ConditionalType<std::is_same<Pos1, BoolConstant<false> >::value,
                                      MakeIndexSequence<commonDims>,
                                      Pos1>;
    using Contract2 = ConditionalType<std::is_same<Pos2, BoolConstant<false> >::value,
                                      MakeIndexSequence<commonDims>,
                                      Pos2>;

    std::clog << "Sig1: " << TensorTraits<T1>::signature() << std::endl;
    std::clog << "Contract1: " << Contract1{} << std::endl;
    std::clog << "Sig2: " << TensorTraits<T2>::signature() << std::endl;
    std::clog << "Contract2: " << Contract2{} << std::endl;

    decltype(auto) result = einsum<Contract1, Contract2>(std::forward<T1>(t1), std::forward<T2>(t2));

    std::clog << Tensor::name(std::forward<T1>(t1)) << " * " << Tensor::name(std::forward<T2>(t2))
              << " -> "
              << result.name()
              << std::endl
              << "Signature: " << result.signature()
              << std::endl
              << result
              << std::endl
              << typeString(result)
              << std::endl;
    auto fvt1 = fieldTensor(std::forward<T1>(t1));
    auto fvt2 = fieldTensor(std::forward<T2>(t2));
    auto difference = result - einsum<Contract1, Contract2>(fvt1, fvt2);
    std::clog << "Optimization is correct: " << (difference == zero(difference))
              << std::endl
              << std::endl;
    if (difference != zero(difference))  {
      std::clog << "  l RTEQ: " << Expressions::AreRuntimeEqual<decltype(result), decltype(fvt1)>::value << std::endl;
      std::clog << "  r RTEQ: " << Expressions::AreRuntimeEqual<decltype(result), decltype(fvt2)>::value << std::endl;
      std::clog << "  Difference:" << std::endl;
      printTensor(difference);
    }
  }

  void einsumOptimizations()
  {
    // total contraction yielding a tensor with integral constant as value
    testEinsumOptimization(ones<3,4,5>(), ones<3,4,5>());

    // constant folding, basic test
    testEinsumOptimization(eye<3,3>(), 3); // should commute
    testEinsumOptimization(3, 3 * eye<3,3>()); // should fold

    // identity operations with eye
    testEinsumOptimization(eye<3,3>(), eye<3,4,5,6>(), Seq<0>{}, Seq<0>{});
    testEinsumOptimization(eye<3,3>(), eye<3,4,5,6>(), Seq<1>{}, Seq<0>{});
    testEinsumOptimization(eye<3,4,5,6>(), eye<6,6>(), Seq<3>{}, Seq<0>{});
    testEinsumOptimization(eye<3,4,5,6>(), eye<6,6>(), Seq<3>{}, Seq<1>{});

    // identity operations with blockEye
    testEinsumOptimization(blockEye<2, 3, 4>(), eye<3,4,5,6>(), Seq<0,1>{}, Seq<0,1>{});
//      testEinsumOptimization(blockEye<2, 3, 4>(), eye<3,4,5,6>(), Seq<2,1>{}, Seq<0,1>{});
    testEinsumOptimization(blockEye<2, 3, 4>(), eye<3,4,5,6>(), Seq<0,3>{}, Seq<0,1>{});
    testEinsumOptimization(blockEye<2, 3, 4>(), eye<3,4,5,6>(), Seq<2,3>{}, Seq<0,1>{});

    testEinsumOptimization(eye<3,4,5,6>(), blockEye<2, 5, 6>(), Seq<2,3>{}, Seq<0,1>{});
//      testEinsumOptimization(eye<3,4,5,6>(), blockEye<2, 5, 6>(), Seq<2,3>{}, Seq<2,1>{});
    testEinsumOptimization(eye<3,4,5,6>(), blockEye<2, 5, 6>(), Seq<2,3>{}, Seq<0,3>{});
    testEinsumOptimization(eye<3,4,5,6>(), blockEye<2, 5, 6>(), Seq<2,3>{}, Seq<2,3>{});

    // Check the diagonal cases
    testEinsumOptimization(eye<3,3>(), eye<3,3>(), Seq<0>{}, Seq<0>{});
    testEinsumOptimization(blockEye<2, 5, 6>(), blockEye<2, 5, 6>(), Seq<0,1>{}, Seq<0,1>{});
  }
}
