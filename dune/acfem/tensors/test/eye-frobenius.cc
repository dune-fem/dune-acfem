#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{
  void eyeFrobenius()
  {
    std::clog << "frobenius(eye<3, 3, 3>()): " << frobenius2(eye<3, 3, 3>()) << std::endl;
    std::clog << "frobenius(ones<3, 3, 3>()): " << frobenius2(ones<3, 3, 3>()) << std::endl;
  }
}
