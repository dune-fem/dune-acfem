set(__MODULE__ tensor)
set(SEPARATETESTS ON)
set(DUNE_ACFEM_COMPILE_TIME_ITERATIONS 1 CACHE STRING "Number of sample during compilation timings.")

# set default grid type for make all
#set(GRIDTYPE YASPGRID)
#set(GRIDTYPE PARALLELGRID_ALUGRID_SIMPLEX)
#set(GRIDTYPE ALUGRID_SIMPLEX)
set(GRIDTYPE ALUGRID_CONFORM)
#set(GRIDTYPE ALUGRID_CUBE)
#set(GRIDTYPE ALBERTAGRID)
set(GRIDDIM 2)
set(POLORDER 2)
set(WANT_ISTL 1)
set(WANT_PETSC 0)

add_definitions(
 -D${GRIDTYPE}
 -DPOLORDER=${POLORDER}
 -DGRIDDIM=${GRIDDIM}
 -DWANT_ISTL=${WANT_ISTL}
 -DWANT_PETSC=${WANT_PETSC}
 -DSRCDIR=\"${CMAKE_CURRENT_SOURCE_DIR}\"
 -DMODULE=\"${__MODULE__}\"
 )

set(TESTPROGRAMS
  associative-products
  autodiff-basic
  autodiff-subexpressions
  const-restriction
  const-restriction-cascade
  constant-access
  constant-operand-promotion
  constant-tensors
  densevectorview
  densematrixassigner
  derivative-basic
  derivative-cos
  derivative-cos-product-2d
  derivative-cos-product-3d
  derivative-euclidean-norm
  derivative-gaussian
  derivative-math-functions
  derivative-powers-of-x
  divergence
  einsum-optimizations
  eye-frobenius
  tree-extract
  field-matrix
  indeterminate-basic
  linear-storage
  math-functions
  operand-promotion-basic
  optimize-instantiation
  ostream
  placeholder-basic
  playground
  pow-timings
  product-optimizations
  reorder-scalars
  restriction-basic
  restriction-cascade
  restriction-operators
  restriction-optimizations
  runtime-equal-factors
  scalar-folding
  subexpressions-basic
  transposition-basic
  unary-minus
  zero-propagation
  )

set(TORTURETESTPROGRAMS
  derivative-cos-product-4d
  derivative-cos-product-5d
  )

if(${SEPARATETESTS})
  foreach(i ${TORTURETESTPROGRAMS})
    list(APPEND TESTPROGRAMS ${i})
  endforeach()
endif()

set(TESTDRIVER ${CMAKE_SOURCE_DIR}/scripts/testdriver.sh)
set(TESTRUNNER ${__MODULE__}-test-runner)

# compound target to rebuild all expectations
add_custom_target(${__MODULE__}-expectations)

# compile all testprograms
add_custom_target(${__MODULE__}-tests)

if(NOT ${SEPARATETESTS})
  add_executable(${TESTRUNNER} test-template.cc)
  dune_target_enable_all_packages(${TESTRUNNER})
  if(${GRIDTYPE} STREQUAL ALBERTAGRID)
    add_dune_alberta_flags(${TESTRUNNER} WORLDDIM ${GRIDDIM})
  endif()
endif()

# for-loop for the case of multiple test
set(TESTEXPECTS "")
foreach(i ${TESTPROGRAMS})
  set(NAME ${i})
  set(TEST ${__MODULE__}-${NAME})
  list(FIND TORTURETESTPROGRAMS ${NAME} TORTURE_INDEX)
  if(${ACFEM_TORTURE_TESTS} OR TORTURE_INDEX EQUAL -1)
    set(DOTEST on)
  else()
    set(DOTEST off)
  endif()
  if(${NAME} STREQUAL optimize-instantiation)
    add_executable(${TEST} ${TEST}.cc)
    dune_target_enable_all_packages(${TEST})
    if(${GRIDTYPE} STREQUAL ALBERTAGRID)
      add_dune_alberta_flags(${TEST} WORLDDIM ${GRIDDIM})
    endif()
    if(${DOTEST})
      dune_add_test(NAME ${TEST}
	TARGET ${TEST}
	COMMAND ${TESTDRIVER}
	CMD_ARGS check ${NAME})
    endif()
  else()
    # generate upper case test-name
    string(REPLACE - _ TESTDEF ${NAME})
    string(TOUPPER ${TESTDEF} TESTDEF)
    # generate camel case test-name
    to_camel_case(CCNAME ${NAME})
    if(${SEPARATETESTS})
      set(TESTRUNNER ${TEST})
      add_executable(${TESTRUNNER} test-template.cc)
      dune_target_enable_all_packages(${TESTRUNNER})
      if(${GRIDTYPE} STREQUAL ALBERTAGRID)
	add_dune_alberta_flags(${TESTRUNNER} WORLDDIM ${GRIDDIM})
      endif()
      set(DEFSEPARATE 1)
      if(NOT ${DOTEST})
	set_target_properties(${TESTRUNNER} PROPERTIES EXCLUDE_FROM_ALL "TRUE")
      endif()
    else()
      add_custom_target(${TEST})
      add_dependencies(${TEST} ${TESTRUNNER})
      set(DEFSEPARATE 0)
    endif()
    target_sources(${TESTRUNNER} PUBLIC ${NAME}.cc)
    if(${NAME} STREQUAL densevectorview)
      target_sources(${TESTRUNNER} PUBLIC assign1.cc assign2.cc assign3.cc assign4.cc assign5.cc)
    endif()
    set_property(TARGET ${TESTRUNNER}
      APPEND_STRING
      PROPERTY COMPILE_FLAGS
      " -D${TESTDEF}=1 -D${TESTDEF}_FUNCTION=${CCNAME} -DSEPARATETESTS=${DEFSEPARATE}")
    if(${DOTEST})
      dune_add_test(NAME ${TEST}
	TARGET ${TESTRUNNER}
	COMMAND ${TESTDRIVER}
	CMD_ARGS check "${TESTRUNNER}" "testName:${NAME}")
    endif()
  endif()
  if(${DOTEST})
    set_tests_properties(${TEST}
      PROPERTIES ENVIRONMENT "srcdir=${CMAKE_CURRENT_SOURCE_DIR};builddir=${CMAKE_CURRENT_BINARY_DIR};testname=${NAME};")
  endif()
  set(TESTEXPECTS ${TESTEXPECTS} ${CMAKE_CURRENT_SOURCE_DIR}/${NAME}.expected)

  if(${NAME} STREQUAL optimize-instantiation)
# not yet ready
#    add_custom_target(${NAME}-expectations
#      DEPENDS ${NAME}
#      COMMAND env srcdir=${CMAKE_CURRENT_SOURCE_DIR} builddir=${CMAKE_CURRENT_BINARY_DIR} testname=${NAME} ${TESTDRIVER} generate ${NAME})
#    add_dependencies(tensorexpectations ${NAME}-expectations)
  elseif(${SEPARATETESTS})
    add_custom_target(${TEST}-expectations
      DEPENDS ${TEST}
      COMMAND env srcdir=${CMAKE_CURRENT_SOURCE_DIR} builddir=${CMAKE_CURRENT_BINARY_DIR} testname=${NAME} ${TESTDRIVER} generate ${TEST} testName:${NAME})
    add_dependencies(${__MODULE__}-expectations ${TEST}-expectations)
  else()
    add_custom_target(${TEST}-expectations
      DEPENDS ${TEST}
      COMMAND env srcdir=${CMAKE_CURRENT_SOURCE_DIR} builddir=${CMAKE_CURRENT_BINARY_DIR} testname=${NAME} ${TESTDRIVER} generate ${TESTRUNNER} testName:${NAME})
    add_dependencies(${__MODULE__}-expectations ${TEST}-expectations)
  endif()
  if(${DOTEST})
    add_dependencies(${__MODULE__}-tests ${TEST})
  endif()

  add_custom_target(${TEST}-compile-time
    COMMAND CNT=0 \; while [ $$CNT -lt ${DUNE_ACFEM_COMPILE_TIME_ITERATIONS} ] \; do cmake -P CMakeFiles/${TEST}.dir/cmake_clean.cmake \; { time -p cmake --build ${CMAKE_CURRENT_BINARY_DIR} --target ${NAME}.o \; } 2>&1 | grep user \; CNT=$$\(\( $$CNT + 1 \)\) \; done
    )
  add_custom_target(${TEST}-clean
    COMMAND cmake -P CMakeFiles/${TEST}.dir/cmake_clean.cmake
    )
  add_custom_target(${TEST}-new
    COMMAND cmake -P CMakeFiles/${TEST}.dir/cmake_clean.cmake \; cmake --build ${CMAKE_CURRENT_BINARY_DIR} --target ${TEST}
    )
endforeach()

SET_DIRECTORY_PROPERTIES(PROPERTIES
  ADDITIONAL_MAKE_CLEAN_FILES "acfem-expression-profile.dump")
