#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{

  using namespace Optimization::Sums;

  void constantOperandPromotion()
  {
    printTensor(tensor(7_f / 21_f), std::string("FractionConstant"));
    printTensor(M::pi, std::string("PI"));
    printTensor(tensor(11_f) * M::pi, std::string("11_c * PI"));
    printTensor(M::pi * 11_f / M::pi, std::string(" PI *11_c / PI"));
    printTensor(11_f * M::pi / tensor(M::pi), std::string(" 11_c * PI / PI"));
    printTensor(1_f / M::pi * M::pi * 11_f, std::string("1_c / PI * PI * 11_c"));
    printTensor((1_f / M::pi * 11_f / M::sqrt2) * M::pi, std::string("1_c / PI * 11_c / sqrt(2) * PI"));
    printTensor(2_f * (2_f * M::pi) * (M::pi * 2_f), std::string("2_c * (2_c * PI) * (PI * 2_c)"));
  }
}
