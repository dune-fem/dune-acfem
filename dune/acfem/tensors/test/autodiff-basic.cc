#include "test-template.hh"

#include "../operations/autodiff/placeholdertraits.hh"
#include "../operations/autodiff/placeholder.hh"
#include "../operations/autodiff/util.hh"
#include "../operations/autodiff/fad.hh"

namespace Dune::ACFem::Tensor::Testing
{
  void autodiffBasic()
  {
    auto x0 = indeterminate<0>(eye<3>());

#if 0
    auto blah1 = autoDiffPlaceholder<0,Seq<3,3> >(zeros<3>(), eye<3,3>());
    auto blah2 = autoDiffPlaceholder<0,Seq<3,3> >(eye<3>(), eye<3,3>());
    std::clog << ExpressionTraits<decltype(blah1)>::Sign{} << std::endl;
    std::clog << ExpressionTraits<decltype(blah2)>::Sign{} << std::endl;
    std::clog << ExpressionTraits<decltype(blah1*blah2)>::Sign{} << std::endl;
    std::clog << (blah1*blah2).name() << std::endl;
    std::clog << ExpressionTraits<decltype(blah2*blah2)>::Sign{} << std::endl;
    std::clog << (blah2*blah2).name() << std::endl;
    std::clog << "normZ: " << Expressions::IsNormalizableZero<decltype(tensor(blah1*blah2))>{} << std::endl;
    std::clog << "Z: " << blah1.isZero() << std::endl;
    std::clog << "TV eye: " << IsTypedValue<decltype(eye<3>())>{} << std::endl;
    std::clog << "TV eye: " << IsTypedValue<decltype(tensor(eye<3>()))>{} << std::endl;
    std::clog << "TV balh1: " << IsTypedValue<decltype(blah1)>{} << std::endl;
    std::clog << "TV blah2: " << IsTypedValue<decltype(blah2)>{} << std::endl;
#endif
#if 1
    std::clog << "*** Unevaluated AutoDiffPlaceholder: " << std::endl;
    printTensor(ensureAutoDiff<0,0>(falseType(), x0*x0*x0, Seq<3>{}, Seq<3>{}));

    std::clog << "*** Evaluated AutoDiffPlaceholder: " << std::endl;
    printTensor(ensureAutoDiff<0,0>(trueType(), x0*x0*x0, Seq<3>{}, Seq<3>{}));

    auto expr = x0*x0*x0*x0;
    std::clog << "Symbolic 4th derivivative of ... " << std::endl;
    printTensor(expr);
    std::clog << "... is ... " << std::endl;
    printTensor(derivative<4>(expr, x0));

    auto fadExp = forwardAutoDiff<5>(trueType(), expr, x0);
    std::clog << "FAD 4th derivivative of ... " << std::endl;
    printTensor(fadExp);
    std::clog << "... is ... " << std::endl;
    printTensor(derivative<4>(fadExp, x0));
#endif

  }
}
