#include <config.h>

#include <libgen.h>

#include <dune/fem/io/parameter.hh>

#include "../../common/scopedredirect.hh"

/**@addtogroup LinearAlgebra
 * @{
 */
/**@addtogroup Tensors
 * @{
 */
/**@addtogroup TensorTests
 * @{
 */

namespace Dune::ACFem::Tensor::Testing
{
  extern void associativeProducts();
  extern void autodiffBasic();
  extern void autodiffSubexpressions();
  extern void blockEyeBasic();
  extern void constRestriction();
  extern void constRestrictionCascade();
  extern void constantAccess();
  extern void constantOperandPromotion();
  extern void constantTensors();
  extern void densematrixassigner();
  extern void densevectorview();
  extern void derivativeBasic();
  extern void derivativeCos();

  template<std::size_t DIM>
  void derivativeCosProduct();

  extern void derivativeEuclideanNorm();
  extern void derivativeGaussian();
  extern void derivativeMathFunctions();
  extern void derivativePowersOfX();
  extern void divergence();
  extern void einsumOptimizations();
  extern void eyeFrobenius();
  extern void fieldMatrix();
  extern void indeterminateBasic();
  extern void linearStorage();
  extern void mathFunctions();
  extern void operandPromotionBasic();
  extern void ostream();
  extern void pLaplacian();
  extern void playground();
  extern void placeholderBasic();
  extern void powTimings();
  extern void productOptimizations();
  extern void reorderScalars();
  extern void restrictionBasic();
  extern void restrictionCascade();
  extern void restrictionOperators();
  extern void restrictionOptimizations();
  extern void runtimeEqualFactors();
  extern void scalarFolding();
  extern void subexpressionsBasic();
  extern void transposition();
  extern void treeExtract();
  extern void unaryMinus();
  extern void zeroPropagation();
}

using namespace Dune;
using namespace ACFem;
using namespace Tensor;
using namespace Testing;

/** Test-template main program. Instantiate a single expression
 * template and evaluate it on a simple grid.
 */
int main(int argc, char *argv[])
  try {
    // General setup
    Fem::MPIManager::initialize(argc, argv);

    // append parameter
    Fem::Parameter::append(argc, argv);

    // append default parameter file
    Fem::Parameter::append(SRCDIR "/parameter");

    //redirect the stream cerr to cout
    //so we only get the output of clog on stderr
    ScopedRedirect redirect(std::cerr, std::cout);

#if SEPARATETESTS
    const std::string testName = Fem::Parameter::getValue<std::string>(
      "testName",
      std::string(basename(argv[0])).substr(std::string("tensor-").size())
      );
#endif
    if (testName == "operand-promotion-basic") {
#if OPERAND_PROMOTION_BASIC
      operandPromotionBasic();
#endif
    } else if (testName == "restriction-operators") {
#if RESTRICTION_OPERATORS
      restrictionOperators();
#endif
    } else if (testName == "restriction-basic") {
#if RESTRICTION_BASIC
      restrictionBasic();
#endif
    } else if (testName == "const-restriction") {
#if CONST_RESTRICTION
      constRestriction();
#endif
    } else if (testName == "restriction-optimizations") {
#if RESTRICTION_OPTIMIZATIONS
      restrictionOptimizations();
#endif
    } else if (testName == "restriction-cascade") {
#if RESTRICTION_CASCADE
      restrictionCascade();
#endif
    } else if (testName == "const-restriction-cascade") {
#if CONST_RESTRICTION_CASCADE
      constRestrictionCascade();
#endif
    } else if (testName == "pow-timings") {
#if POW_TIMINGS
      powTimings();
#endif
    } else if (testName == "product-optimizations") {
#if PRODUCT_OPTIMIZATIONS
      productOptimizations();
#endif
    } else if (testName == "einsum-optimizations") {
#if EINSUM_OPTIMIZATIONS
      einsumOptimizations();
#endif
    } else if (testName == "zero-propagation") {
#if ZERO_PROPAGATION
      zeroPropagation();
#endif
    } else if (testName == "field-matrix") {
#if FIELD_MATRIX
      fieldMatrix();
#endif
    } else if (testName == "scalar-folding") {
#if SCALAR_FOLDING
      scalarFolding();
#endif
    } else if (testName == "unary-minus") {
#if UNARY_MINUS
      unaryMinus();
#endif
    } else if (testName == "transposition-basic") {
#if TRANSPOSITION_BASIC
      transposition();
#endif
    } else if (testName == "constant-access") {
#if CONSTANT_ACCESS
      constantAccess();
#endif
    } else if (testName == "eye-frobenius") {
#if EYE_FROBENIUS
      eyeFrobenius();
#endif
    } else if (testName == "ostream") {
#if OSTREAM
      ostream();
#endif
    } else if (testName == "math-functions") {
#if MATH_FUNCTIONS
      mathFunctions();
#endif
    } else if (testName == "derivative-math-functions") {
#if DERIVATIVE_MATH_FUNCTIONS
      derivativeMathFunctions();
#endif
    } else if (testName == "indeterminate-basic") {
#if INDETERMINATE_BASIC
      indeterminateBasic();
#endif
    } else if (testName == "placeholder-basic") {
#if PLACEHOLDER_BASIC
      placeholderBasic();
#endif
    } else if (testName == "autodiff-basic") {
#if AUTODIFF_BASIC
      autodiffBasic();
#endif
    } else if (testName == "derivative-basic") {
#if DERIVATIVE_BASIC
      derivativeBasic();
#endif
    } else if (testName == "constant-tensors") {
#if CONSTANT_TENSORS
      constantTensors();
#endif
    } else if (testName == "constant-operand-promotion") {
#if CONSTANT_OPERAND_PROMOTION
      constantOperandPromotion();
#endif
    } else if (testName == "derivative-cos-product-2d") {
#if DERIVATIVE_COS_PRODUCT_2D
      derivativeCosProduct<2>();
#endif
    } else if (testName == "derivative-cos-product-3d") {
#if DERIVATIVE_COS_PRODUCT_3D
      derivativeCosProduct<3>();
#endif
    } else if (testName == "derivative-cos-product-4d") {
#if DERIVATIVE_COS_PRODUCT_4D
      derivativeCosProduct<4>();
#endif
    } else if (testName == "derivative-cos-product-5d") {
#if DERIVATIVE_COS_PRODUCT_5D
      derivativeCosProduct<5>();
#endif
    } else if (testName == "derivative-gaussian") {
#if DERIVATIVE_GAUSSIAN
      derivativeGaussian();
#endif
    } else if (testName == "derivative-powers-of-x") {
#if DERIVATIVE_POWERS_OF_X
      derivativePowersOfX();
#endif
    } else if (testName == "derivative-cos") {
#if DERIVATIVE_COS
      derivativeCos();
#endif
    } else if (testName == "derivative-euclidean-norm") {
#if DERIVATIVE_EUCLIDEAN_NORM
      derivativeEuclideanNorm();
#endif
    } else if (testName == "divergence") {
#if DIVERGENCE
      divergence();
#endif
    } else if (testName == "densevectorview") {
#if DENSEVECTORVIEW
      densevectorview();
#endif
    } else if (testName == "densematrixassigner") {
#if DENSEMATRIXASSIGNER
      densematrixassigner();
#endif
    } else if (testName == "associative-products") {
#if ASSOCIATIVE_PRODUCTS
      associativeProducts();
#endif
    } else if (testName == "reorder-scalars") {
#if REORDER_SCALARS
      reorderScalars();
#endif
    } else if (testName == "autodiff-subexpressions") {
#if AUTODIFF_SUBEXPRESSIONS
      autodiffSubexpressions();
#endif
    } else if (testName == "subexpressions-basic") {
#if SUBEXPRESSIONS_BASIC
      subexpressionsBasic();
#endif
    } else if (testName == "runtime-equal-factors") {
#if RUNTIME_EQUAL_FACTORS
      runtimeEqualFactors();
#endif
    } else if (testName == "tree-extract") {
#if TREE_EXTRACT
      treeExtract();
#endif
    } else if (testName == "playground") {
#if PLAYGROUND
      playground();
#endif
    } else if (testName == "linear-storage") {
#if LINEAR_STORAGE
      linearStorage();
#endif
    } else {
      DUNE_THROW(Dune::NotImplemented, "Test \""+testName+"\" not implemented");
    }
    return EXIT_SUCCESS;
  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
    return EXIT_FAILURE;
  }


//!@} TensorTests

//!@} Tensors

//!@} LinearAlgebra
