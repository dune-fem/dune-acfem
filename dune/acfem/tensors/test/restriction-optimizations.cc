#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{
  void restrictionOptimizations()
  {
    std::clog << "const pr<1@2>(ones<3,4>()): " << restriction<1>(ones<3, 4>(), Seq<2>{}) << std::endl;
    std::clog << "const pr<1@2>(ones<3,4>()): " << typeString(restriction<1>(ones<3, 4>(), Seq<2>{})) << std::endl;

    std::clog << "const pr<1@2>(eye<3,4>()): " << restriction<1>(eye<3, 4>(), Seq<2>{}) << std::endl;
    std::clog << "const pr<1@2>(eye<3,4>()): " << typeString(restriction<1>(eye<3, 4>(), Seq<2>{})) << std::endl;

    std::clog << "const pr<1,2@0,1>(eye<3,3,3>()): " << restriction<1,2>(eye<3,3,3>(), Seq<0,1>{}) << std::endl;
    std::clog << "const pr<1,2@0,1>(eye<3,3,3>()): " << typeString(restriction<1,2>(eye<3,3,3>(), Seq<0,1>{})) << std::endl;

    std::clog << "const pr<1,2@1,1>(eye<3,3,3>()): " << restriction<1,2>(eye<3,3,3>(), Seq<1,1>{}) << std::endl;
    std::clog << "const pr<1,2@1,1>(eye<3,3,3>()): " << typeString(restriction<1,2>(eye<3,3,3>(), Seq<1,1>{})) << std::endl;

    std::clog << "const pr<0,1@1,1>(blockEye<3,3,3>()): " << restriction<0,1>(blockEye<3,3,3>(), Seq<1,1>{}) << std::endl;
    std::clog << "const pr<0,1@1,1>(blockEye<3,3,3>()): " << typeString(restriction<0,1>(blockEye<3,3,3>(), Seq<1,1>{})) << std::endl;

    std::clog << "const pr<0,2@1,2>(blockEye<3,3,3>()): " << restriction<0,2>(blockEye<3,3,3>(), Seq<1,2>{}) << std::endl;
    std::clog << "const pr<0,2@1,2>(blockEye<3,3,3>()): " << typeString(restriction<0,2>(blockEye<3,3,3>(), Seq<1,2>{})) << std::endl;

    std::clog << "dyn pr<1@2>(ones<3,4>()): " << restriction<1>(ones<3, 4>(), {2}) << std::endl;
    std::clog << "dyn pr<1@2>(ones<3,4>()): " << typeString(restriction<1>(ones<3, 4>(), {2})) << std::endl;

    std::clog << "dyn pr<1@2>(eye<3,4>()): " << restriction<1>(eye<3, 4>(), {2}) << std::endl;
    std::clog << "dyn pr<1@2>(eye<3,4>()): " << typeString(restriction<1>(eye<3, 4>(), {2})) << std::endl;
    //printTensor(restriction<1>(eye<3, 4>(), {2}), "dyn pr<1@2>(eye<3,4>())");
  }
}
