#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{
  void constRestrictionCascade()
  {
    FieldTensor<double, 3, 4, 5, 6> t;
    auto v1 = restriction<0, 2>(t, Seq<1, 1>{});
    auto v2 = restriction<1>(v1, Seq<1>{});
    auto v3 = restriction<0>(v2, Seq<1>{});

    std::clog << "Looking at: " << v3->lookAt() << std::endl;
    v3() = 42.;

    std::clog << "View 1: " << typeString(v1) << std::endl;
    std::clog << "View 2: " << typeString(v2) << std::endl;
    std::clog << "View 3: " << typeString(v3) << std::endl;

    std::clog << "v3() / t(1, 1, 1, 1) " << v3() << " / " << t(1, 1, 1, 1) << std::endl;
    t(1, 1, 1, 1) = 3.0;
    std::clog << "v3 / t(1, 1, 1, 1) " << v3 << " / " << t(1, 1, 1, 1) << std::endl;
  }
}
