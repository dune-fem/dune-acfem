#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{
  void derivativeEuclideanNorm()
  {
    auto x0 = indeterminate<0,3>();

    std::clog << "********** derivatives of Euclidean norm **********" << std::endl;
    printTensor(sqrt(x0*x0));
    printTensor(derivative(sqrt(x0*x0), x0));
    printTensor(derivative<2>(sqrt(x0*x0), x0));
    printTensor(derivative<3>(sqrt(x0*x0), x0));
    printTensor(derivative<4>(sqrt(x0*x0), x0));
  }
}
