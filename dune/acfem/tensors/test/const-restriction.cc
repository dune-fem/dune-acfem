#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{
  void constRestriction()
  {
    FieldTensor<double, 3, 4, 5, 6> t;
    auto tv = restriction<1, 3>(t, Seq<3, 5>{});

    std::clog << "Tensor Type: " << typeString(tv) << std::endl;
    std::clog << "rank(tv): " << tv.rank << std::endl;
    std::clog << "signature(tv): " << typeString(decltype(tv)::Signature{}) << std::endl;

    std::clog << "Looking at: " << tv->lookAt() << std::endl;
    tv(1, 1) = 42;
    std::clog << "tv(1, 1) / t(1, 3, 1, 5): " << tv(1, 1) << " / " << t(1, 3, 1, 5) << std::endl;
  }
}
