#if HAVE_CONFIG_H
# include <config.h>
#endif

#include <dune/common/timer.hh>
#include <dune/fem/io/parameter.hh>

#include "test-template.hh"

volatile double writeMe;
volatile double pi = M_PI;

namespace Dune::ACFem::Tensor::Testing
{

  void powTimings()
  {
    std::size_t count = Dune::Fem::Parameter::getValue<std::size_t>("timing.iterations", 10000);

    Dune::Timer timer;
#if 0
    forLoop<10>([&](auto i) {
        using I = decltype(i);
        timer.reset();
        timer.start();
        for (std::size_t counter = 0; counter < count; ++counter) {
          writeMe = pow(M::pi, i.value);
        }
        auto elapsed = timer.elapsed();
        timer.stop();
        std::cout << "pow(M::pi, " << i.value << ")#" << count << " needs " << elapsed << " s" << std::endl;
//        std::cout << "value: " << pow(M::pi, i.value) << std::endl;

        timer.reset();
        timer.start();
        for (std::size_t counter = 0; counter < count; ++counter) {
          writeMe = multiplyLoop<I::value>([](auto i) { return M::pi; }, 1_f);
        }
        elapsed = timer.elapsed();
        timer.stop();
        std::cout << "(1_f * ... * M::pi)#" << count << " needs " << elapsed << " s" << std::endl;
//        std::cout << "value: " << multiplyLoop<I::value>([](auto i) { return M::pi; }, 1_f) << std::endl;
      });

    std::cout << std::endl;

    forLoop<10>([&](auto i) {
        using I = decltype(i);
        timer.reset();
        timer.start();
        for (std::size_t counter = 0; counter < count; ++counter) {
          writeMe = pow(M::pi, i.value/2.0);
        }
        auto elapsed = timer.elapsed();
        timer.stop();
        std::cout << "pow(M::pi, " << i.value << "/2.0)#" << count << " needs " << elapsed << " s" << std::endl;
//        std::cout << "value: " << pow(M::pi, i.value) << std::endl;

        timer.reset();
        timer.start();
        for (std::size_t counter = 0; counter < count; ++counter) {
          writeMe = sqrt(multiplyLoop<I::value>([](auto i) { return M::pi; }, tensor(1_f)));
        }
        elapsed = timer.elapsed();
        timer.stop();
        std::cout << "sqrt(1_f * ... * M::pi)#" << count << " needs " << elapsed << " s" << std::endl;
//        std::cout << "value: " << multiplyLoop<I::value>([](auto i) { return M::pi; }, 1_f) << std::endl;
      });

    std::cout << std::endl;
#endif

    forLoop<100>([&](auto i) {
        using I = decltype(i);

        timer.reset();
        timer.start();
        for (std::size_t counter = 0; counter < count; ++counter) {
          writeMe = std::pow(pi, -(double)I::value/2.0);
        }
        auto elapsed = timer.elapsed();
        timer.stop();
        std::cout << "pow(M::pi, -" << i.value << "/2.0)#" << count << " needs " << elapsed << " s" << std::endl;
        std::cout << "value: " << writeMe << std::endl;

        timer.reset();
        timer.start();
        for (std::size_t counter = 0; counter < count; ++counter) {
//          writeMe = invSqrt(multiplyLoop<I::value>([](auto i) { return M::pi; }, tensor(1_f)));
          writeMe = 1.0 / std::sqrt(multiplyLoop<I::value>(1.0, [](auto i) { return pi; }));
        }
        elapsed = timer.elapsed();
        timer.stop();
        std::cout << "invSqrt(1_f * ... * M::pi)#" << count << " needs " << elapsed << " s" << std::endl;
        std::cout << "value: " << writeMe << std::endl;

        std::cout << std::endl;
      });

  }
}
