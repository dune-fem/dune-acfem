#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{
  void restrictionOperators()
  {
    FieldTensor<double, 3, 4, 5, 6> t;

    printTensor(t[2_c]);
    printTensor(t[2_c][1_c]);
    printTensor(t[2_c][1_c][0_c]);
    printTensor(t[2_c][1_c][0_c][1_c]);
  }
}
