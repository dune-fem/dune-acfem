#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{
  void densematrixassigner()
  {
    Dune::FieldMatrix<double, 1, 2> m1;
    Dune::FieldMatrix<double, 2, 2> m2;

    auto t1 = m1+m1;
    std::clog << "t1 type: " << typeString(t1) << std::endl;

    //std::clog << " t1 const_iter: " << !std::is_same<typename RHS1::const_iterator, void>::value << std::endl;
    //std::clog << " t1 conv: " << std::is_convertible<typename RHS1::const_iterator::value_type, typename DenseMatrix1::iterator::value_type >::value << std::endl;
    //std::clog << " t1 iterval: " << typeString((typename RHS1::const_iterator::value_type *)nullptr) << std::endl;
    //std::clog << " m1 iterval: " << typeString((typename DenseMatrix1::iterator::value_type *)nullptr) << std::endl;
    std::clog << " t1[0]: " << typeString(t1.storage()[0]) << std::endl;
    std::clog << " t1[0][0]: " << typeString(t1.storage()[0][0]) << std::endl;

    Dune::Impl::DenseMatrixAssigner<
      std::decay_t<decltype(m1)>,
      std::decay_t<decltype(t1)>
      >::apply(m1, t1);

// /Users/heinecj/Projects/DUNE/head/dune-acfem/dune/acfem/models/test/test-template.cc:451:37:   required from here
// /Users/heinecj/Projects/DUNE/head/dune-acfem/dune/acfem/models/test/test-template.cc:148:15: error: ambiguous template instantiation for 'class Dune::Impl::DenseMatrixAssigner<Dune::FieldMatrix<double, 2, 2>, Dune::ACFem::Tensor::TensorResult<Dune::ACFem::Tensor::BinaryTensorExpression<Dune::ACFem::OperationTraits<Dune::ACFem::PlusOperation>, Dune::ACFem::Tensor::BinaryTensorExpression<Dune::ACFem::OperationTraits<Dune::ACFem::PlusOperation>, Dune::ACFem::Tensor::FieldVectorTensor<Dune::FieldMatrix<double, 2, 2> >, Dune::ACFem::Tensor::FieldVectorTensor<Dune::FieldMatrix<double, 2, 2> > >, Dune::ACFem::Tensor::FieldVectorTensor<Dune::FieldMatrix<double, 2, 2> > > >, void>'
//      Dune::Impl::DenseMatrixAssigner<
//      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//        JacobianRangeType,
//        ~~~~~~~~~~~~~~~~~~
//        std::decay_t<decltype(model.flux(*localCenter, value, jacobian))>
//        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//        >::apply(flux, model.flux(*localCenter, value, jacobian));
//        ~~~~~~~~^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    auto t2 = m2 + m2 + m2;
    std::clog << "t2 type: " << typeString(t2) << std::endl;

    //using RHS = std::decay_t<decltype(t2)>;
    //std::clog << " t2 const_iter: " << !std::is_same<typename RHS::const_iterator, void >::value << std::endl;
    //std::clog << " t2 conv: " << std::is_convertible<typename RHS::const_iterator::value_type, typename DenseMatrix::iterator::value_type >::value << std::endl;
    //std::clog << " t2 iterval: " << typeString((typename RHS::const_iterator::value_type *)nullptr) << std::endl;
    //std::clog << " m2 iterval: " << typeString((typename DenseMatrix::iterator::value_type *)nullptr) << std::endl;
    std::clog << " t2[0]: " << typeString(t2.storage()[0]) << std::endl;
    std::clog << " t2[0][0]: " << typeString(t2.storage()[0][0]) << std::endl;

    Dune::Impl::DenseMatrixAssigner<
      std::decay_t<decltype(m2)>,
      std::decay_t<decltype(t2)>
      >::apply(m2, t2);
  }
}
