#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{
  void restrictionBasic()
  {
    auto t = fieldTensor<3, 4, 5, 6>();
    auto tv = restriction<1, 3>(t);
    auto tv2 = restriction(t, Seq<1,3>{}); (void)tv2;
    auto tv3 = restriction(t, {3, 5}, Seq<1,3>{}); (void)tv3;
    auto tv4 = restriction<1,3>(t, {3, 5}); (void)tv4;

    std::clog << "Tensor Type: " << typeString(tv) << std::endl;
    std::clog << "rank(tv): " << tv.rank << std::endl;
    std::clog << "signature(tv): " << typeString(decltype(tv)::Signature{}) << std::endl;

    tv->lookAt(3, 5);
    std::clog << "Looking at: " << tv->lookAt() << std::endl;
    tv(1, 1) = 42;
    std::clog << "tv(1, 1) / t(1, 3, 1, 5): " << tv(1, 1) << " / " << t(1, 3, 1, 5) << std::endl;
  }
}
