#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{
  void derivativePowersOfX()
  {
    auto x0 = indeterminate<0,3>();

    std::clog << std::endl << "**** Derivatives of X ****" << std::endl;
    printTensor(x0);
    printTensor(derivative<1>(x0, x0));
    printTensor(derivative<2>(x0, x0));
    std::clog << std::endl;

    std::clog << std::endl << "**** Derivatives of X*X ****" << std::endl;
    printTensor(x0*x0);
    printTensor(derivative<1>(x0*x0, x0));
    printTensor(derivative<2>(x0*x0, x0));
    printTensor(derivative<3>(x0*x0, x0));
    std::clog << std::endl;

    std::clog << std::endl << "**** Derivatives of X*X*X ****" << std::endl;
    printTensor(x0*x0*x0);
    printTensor(derivative<1>(x0*x0*x0, x0));
    printTensor(derivative<2>(x0*x0*x0, x0));
    printTensor(derivative<3>(x0*x0*x0, x0));
    printTensor(derivative<4>(x0*x0*x0, x0));
    std::clog << std::endl;

    std::clog << std::endl << "**** Derivatives of X*X*X*X ****" << std::endl;
    printTensor(x0*x0*x0*x0);
    printTensor(derivative<1>(x0*x0*x0*x0, x0));
    printTensor(derivative<2>(x0*x0*x0*x0, x0));
    printTensor(derivative<3>(x0*x0*x0*x0, x0));
    printTensor(derivative<4>(x0*x0*x0*x0, x0));
    printTensor(derivative<5>(x0*x0*x0*x0, x0));
    std::clog << std::endl;

    std::clog << std::endl << "**** Derivatives of X*X*X*X*X ****" << std::endl;
    printTensor(x0*x0*x0*x0*x0);
    printTensor(derivative<1>(x0*x0*x0*x0*x0, x0));
    printTensor(derivative<2>(x0*x0*x0*x0*x0, x0));
    printTensor(derivative<3>(x0*x0*x0*x0*x0, x0));
    printTensor(derivative<4>(x0*x0*x0*x0*x0, x0));
    printTensor(derivative<5>(x0*x0*x0*x0*x0, x0));
    printTensor(derivative<6>(x0*x0*x0*x0*x0, x0));
    std::clog << std::endl;
  }
}
