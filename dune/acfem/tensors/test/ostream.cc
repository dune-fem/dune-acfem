#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{
  void ostream()
  {
    std::clog << "Dim 0 Tensor: " << eye<2,3,0>() << std::endl;
    std::clog << "Scalar Tensor: " << tensor(42) << std::endl;
    std::clog << "Rank 1 Tensor: " << ones<4>() << std::endl;
    std::clog << "Rank 2 Tensor: " << std::endl << ones<4,4>() << std::endl;
    std::clog << "Rank 4 Eye Tensor: " << std::endl << eye<3,3,4,4>() << std::endl;
    std::clog << "Rank 4 BlockEye Tensor: " << std::endl << blockEye<2,3,3>() << std::endl;
  }
}
