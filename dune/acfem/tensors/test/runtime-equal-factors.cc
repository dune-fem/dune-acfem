#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{
  void runtimeEqualFactors()
  {
    auto x0 = indeterminate<0,3>();

    auto t = 2_f*x0 + x0 * 3_f * sqrt(x0 * x0);
    printTensor(t, std::string("2_f*x0 + x0 * 3_f * sqrt(x0 * x0)"));
  }
}
