#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{
  void divergence()
  {
    auto X1 = indeterminate<0, 3>();

    printTensor(divergence(X1, X1), "div(X<3>)");

    printTensor(divergence(outer(X1, X1), X1), "div(X<3>.X<3>)");

    auto X2 = indeterminate<0, 3, 3>();

    printTensor(divergence(X2, X2), "div(X<3,3>)");

    printTensor(divergence(outer(X2, X2), X2), "div(X<3,3>.X<3,3>)");

    auto X3 = indeterminate<0, 3, 3, 3>();

    printTensor(divergence(X3, X3), "div(X<3,3,3>)");

    printTensor(divergence(outer(X3, X3), X3), "div(X<3,3,3>.X<3,3,3>)");

  }
}
