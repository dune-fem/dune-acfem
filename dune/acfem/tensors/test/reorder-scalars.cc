#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{
  template<class T>
  void printTensorAffinity(T&& t_)
  {
    auto t = asExpression(t_);
    std::clog << "Tensor" << std::endl
              << "  " << t.name() << std::endl
              << "  affinity: " << Optimization::Einsum::scalarLeftAffinity(t) << std::endl
              << "  weight:   " << Expressions::weight(t) << std::endl
              << std::endl;
  }

  void reorderScalars()
  {
    auto x = indeterminate<0>(FieldTensor<double, 2>());
    auto t0 = asExpression(8_f * M::pi);
    auto t1 = asExpression(inner(x, x)); (void)t1;
    auto t2 = asExpression(cos(2_f * M::pi * x[0_c])); (void)t2;
    auto t3 = asExpression(cos(2_f * M::pi * x[1_c])); (void)t3;

    printTensorAffinity(t0);
    printTensorAffinity(t1);
    printTensorAffinity(t2);
    printTensorAffinity(t3);
    printTensorAffinity(t2*t3);
    printTensorAffinity(t1*t2);
    printTensorAffinity(t1*t3);

    printTensor(t0 * t1 * t2 * eye<2,2>() * t3 * eye<2,2>());

    printTensor(ones<2,2>() * 2_f);
  }
}
