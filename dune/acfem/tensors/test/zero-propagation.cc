#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{
  void zeroPropagation()
  {
    auto e = eye(Seq<3, 4, 5, 6>{});
    auto v1 = restriction<0, 2>(e);
    std::clog << "ViewType: " << typeString(v1) << std::endl;
    std::clog << "view == 0: " << v1.isZero(Seq<>{}) << std::endl;
    std::clog << "view(0, 1) == 0: " << v1->isZero<0, 1>(/*Seq<0, 1>{}*/) << std::endl;
    std::clog << "eye(0, 0, 0, 1) == 0: " << e.isZero(Seq<0, 0, 0, 1>{}) << std::endl;
    auto m = einsum<Seq<0, 2>, Seq<0, 2> >(e, e);
    std::clog << "EinSum Type: " << typeString(m) << std::endl;
    std::clog << "einsum(1, 1) == 0: " << m.isZero(Seq<1, 1>{}) << std::endl;
    std::clog << "einsum(0, 1) == 0: " << m.isZero(Seq<0, 1>{}) << std::endl;

    auto t = kroneckerDelta(Seq<3, 4, 5, 6>{}, Seq<1, 2, 3, 4>{});
    std::clog << "delta Type: " << typeString(t) << std::endl;
    std::clog << "d(1, 2, 3, 4) == 0: " << t.isZero(Seq<1, 2, 3, 4>{}) << std::endl;
    std::clog << "d(1, 2, 1) == 0: " << t.isZero(Seq<1, 2, 1>{}) << std::endl;

    std::clog << "3.*d Type: " << typeString(3. * t) << std::endl;
    std::clog << "3.*d(1, 2, 3, 4) == 0: " << (3. * t).isZero(Seq<1, 2, 3, 4>{}) << std::endl;
    std::clog << "3.*d(1, 2, 1) == 0: " << (3. * t).isZero(Seq<1, 2, 1>{}) << std::endl;
  }
}
