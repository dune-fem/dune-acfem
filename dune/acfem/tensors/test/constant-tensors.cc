#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{
  void constantTensors()
  {
    printTensor(1_f / tensor(3_f), "one half");
    printTensor(2_f / tensor(3_f), "2/3");
    printTensor(1_f / tensor(2.0), "one half");
    printTensor(sqr(tensor(2_f)), "4_f");
    printTensor(reciprocal(tensor(2_f)), "1/2");
    printTensor(pow(tensor(2_f), tensor(3_f)), "8");
    printTensor(cos(tensor(2.0)), "cos(tensor(2.0))");
    printTensor(cos(M::pi), "cos(tensor(2.0))");

    {
      auto t = constantTensor<3,3>(2_f);
      printTensor(t+t, "3x3 4_f");
      printTensor(sqr(t), "3x3 4_f");
      printTensor(einsum(t, t), "contraction");
      printTensor(multiply(t, t), "total 4x4 tensor product");
    }
    {
      auto t = constantTensor<3,3>(M::pi());
      printTensor(t+t, "3x3 2 pi");
      printTensor(sqr(t), "3x3 pi^2");
      printTensor(einsum(t, t), "contraction");
      printTensor(multiply(t, t), "total 4x4 tensor product");
    }
    {
      auto t = constantTensor(3.0, Seq<3,3>{}, Expressions::Disclosure{});
      printTensor(sqr(t), "3x3 9");
      printTensor(einsum(t, t), "contraction");
      printTensor(multiply(t, t), "total 4x4 tensor product");
    }
    {
      const double c = 3.0;
      auto t = constantTensor<3,3>(c);
      printTensor(std::move(t)+std::move(t), "3x3 4_f");
      printTensor(sqr(std::move(t)), "3x3 4_f");
      printTensor(einsum(std::move(t), std::move(t)), "contraction");
      printTensor(multiply(std::move(t), std::move(t)), "total 4x4 tensor product");
    }
  }
}
