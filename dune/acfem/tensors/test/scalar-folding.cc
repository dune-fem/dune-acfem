#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{
  void scalarFolding()
  {
    // Test folding of constant scalars into a single scalar.
    double c = 3.;

    std::string ts = "3 * c * eye<3, 3, 3>() * 3 * std::move(c) * c * 3 * ones<3>()";
    auto t = 3 * c * eye<3, 3, 3>() * 3 * std::move(c) * c * 3 * ones<3>();
    printTensor(t, ts);

    // Test removal of scalar one and minus one
    std::string t2s = "1_c * eye<3,3,3>() + -1_c * eye<3,3,3>()";
    auto t2 = ones<>() * eye<3,3,3>() + constantTensor<>(intFraction<-1>()) * eye<3,3,3>();
    printTensor(t2, t2s);

    // Test division of RuntimeEqual expressions
    auto xs = indeterminate<0>(tensor(3.0));
    std::string t3s = "xs / xs";
    auto t3 = xs / xs;
    printTensor(t3, t3s);
  }
}
