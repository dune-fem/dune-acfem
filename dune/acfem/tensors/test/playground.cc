#include "test-template.hh"

// factor out code
//
// explode, sort factors, reassemble
//
// explode, search for factor contained in the max. number of operands

namespace Dune::ACFem::Tensor::Testing
{

  void playground()
  {
    auto z0 = zeros(Seq<3>{}, Disclosure{});
    auto z1 = zeros(Seq<3>{}, Disclosure{});
    using T0 = decltype(z0);
    using T1 = decltype(z1);

    std::clog << ReturnFirstV<MinusFunctor, T0, T1> << std::endl;
    std::clog << ReturnMinusSecondV<MinusFunctor, T0, T1> << std::endl;
    std::clog << IsBinaryZeroV<MinusFunctor, T0, T1> << std::endl;
    std::clog << (z0 - z1).name() << std::endl;
  }

}
