#include <config.h>

#include <dune/common/dynvector.hh>
#include <dune/fem/io/parameter.hh>

#include "../../common/ostream.hh"
#include "../../common/scopedredirect.hh"

#include "../tensor.hh"

#include "../bindings/dune/densevectorview.hh"

/**@addtogroup LinearAlgebra
 * @{
 */
/**@addtogroup Tensors
 * @{
 */
/**@addtogroup TensorTests
 * @{
 */

using namespace Dune;
using namespace ACFem;
using namespace Tensor;

extern Dune::FieldVector<Dune::FieldVector<Dune::FieldVector<double, 5>, 4>, 3>* O0Ptr;

void denseVectorAssign3(const std::decay_t<decltype(eye<3,4,5>())>& t)
{
  *O0Ptr = t;
}
