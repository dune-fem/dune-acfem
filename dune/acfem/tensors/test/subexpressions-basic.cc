#include "../../expressions/examineutil.hh"

#include "test-template.hh"

namespace Dune::ACFem::Tensor::Testing
{
  void subexpressionsBasic()
  {
#if 0
    auto x0 = indeterminate<0>(FieldTensor<double, 3>());
    auto lengthX0 = sqrt(x0*x0);
    auto sExp = subExpression(lengthX0, Seq<>{});
    auto nestedSExp = subExpression(x0*x0*sExp, Seq<>{});
    printTensor(nestedSExp);
    printTensor(derivative(nestedSExp, x0));
#if 0
    auto expr = Expressions::asExpression(derivative(nestedSExp, x0));
    using T = decltype(expr);
    std::clog << "la final: " << IsLeftAssociativeProductFinal<Functor<T>, Operand<0, T>, Operand<1, T> >::value
              << std::endl;
#endif

#elif 0
    auto x0 = indeterminate<0>(FieldTensor<double, 3>());

    {
      auto lengthX0 = sqrt(x0*x0);

      auto cascade = commonSubExpressionCascade(lengthX0);

      static_assert(cascade.isEmpty(), "Cascade should be empty");

      forLoop<cascade.depth()>([&](auto i) {
          constexpr size_t depth = cascade.depth() - i.value - 1;
          std::clog << "****** depth " << depth << std::endl;
          forEach(cascade.get<depth>(), [&](auto&& t) { // mind the &
              std::clog << t.name() << std::endl;
              auto tExp = expandAllSubExpressions(t);
              std::clog << "#" << subExpressionMultiplicity(lengthX0, tExp) << " " << tExp.name() << std::endl;
            });
        });
    }

    {
      auto expr = sqrt(x0*x0)*cos(x0*x0);

      auto cascade = commonSubExpressionCascade(expr);

      forLoop<cascade.depth()>([&](auto i) {
          constexpr size_t depth = cascade.depth() - i.value - 1;
          std::clog << "****** depth " << depth << std::endl;
          forEach(cascade.get<depth>(), [&](auto&& t) { // mind the &
              std::clog << t.name() << std::endl;
              auto tExp = expandAllSubExpressions(t);
              std::clog << "#" << subExpressionMultiplicity(expr, tExp) << " " << tExp.name() << std::endl;
            });
        });
    }

    {
      auto expr = sqrt(x0*x0)*x0;

      auto cascade = commonSubExpressionCascade(expr);

      forLoop<cascade.depth()>([&](auto i) {
          constexpr size_t depth = cascade.depth() - i.value - 1;
          std::clog << "****** depth " << depth << std::endl;
          forEach(cascade.get<depth>(), [&](auto&& t) { // mind the &
              std::clog << t.name() << std::endl;
              auto tExp = expandAllSubExpressions(t);
              std::clog << "#" << subExpressionMultiplicity(expr, tExp) << " " << tExp.name() << std::endl;
            });
        });
    }

    {
      auto expr = sqrt(x0*x0)*x0;

      auto cascade = commonSubExpressionCascade(expr);

      forLoop<cascade.depth()>([&](auto i) {
          constexpr size_t depth = cascade.depth() - i.value - 1;
          std::clog << "****** depth " << depth << std::endl;
          forEach(cascade.get<depth>(), [&](auto&& t) { // mind the &
              std::clog << t.name() << std::endl;
              auto tExp = expandAllSubExpressions(t);
              std::clog << "#" << subExpressionMultiplicity(expr, tExp) << " " << tExp.name() << std::endl;
            });
        });
    }

    {
      auto expr = sqrt(x0*x0)*11_f * M::pi * eye<3,3>();
      std::clog << expr.name() << std::endl;

      auto cascade = commonSubExpressionCascade(expr);

      forLoop<cascade.depth()>([&](auto i) {
          constexpr size_t depth = cascade.depth() - i.value - 1;
          std::clog << "****** depth " << depth << std::endl;
          forEach(cascade.get<depth>(), [&](auto&& t) { // mind the &
              std::clog << t.name() << std::endl;
              auto tExp = expandAllSubExpressions(t);
              std::clog << "#" << subExpressionMultiplicity(expr, tExp) << " " << tExp.name() << std::endl;
            });
        });
    }

#elif 0
    constexpr std::size_t dim = 2;
    auto x = indeterminate<0>(FieldTensor<double, dim>());
    auto exactSolution =  multiplyLoop<dim>(1_f, [x](auto i){
        return cos(2*M_PI*x[i]);
      });
    auto F = (8*M_PI*M_PI+1)*std::move(exactSolution);

    auto cascade = commonSubExpressionCascade(F);

    forLoop<cascade.depth()>([&](auto i) {
        constexpr size_t depth = cascade.depth() - i.value - 1;
        std::clog << "****** depth " << depth << std::endl;
        forEach(cascade.get<depth>(), [&](auto&& t) { // mind the &
            auto tExp = expandAllSubExpressions(t);
            std::clog << "#" << subExpressionMultiplicity(F, tExp) << " " << tExp.name() << std::endl;
          });
      });
#elif 0
    auto sExp = subExpression(tensor(2_f)*tensor(M::pi));

    evaluateAllSubExpressions(sExp); // should we?

    printTensor(sExp);
#else
    auto x0 = indeterminate<0>(FieldTensor<double, 3>());
    auto lengthX0 = sqrt(x0*x0);
    printTensor(lengthX0);

    FieldTensor<double, 3> v1, v2;
    auto expr = lengthX0*x0 + (lengthX0+x0*x0)*cos(x0)+v1*v2*v1*v2*v1;
//      auto expr = lengthX0*x0;
    std::clog << "*** Original expression: ***" << std::endl;
    printTensor(expr);

    std::clog << "********* mult " << subExpressionMultiplicity(expr, x0) << std::endl;

    std::clog << "**** SubExpressionCascade. ****" << std::endl;
    auto cascade = commonSubExpressionCascade(expr);

    forLoop<cascade.depth()>([&](auto i) {
        constexpr size_t depth = cascade.depth() - i.value - 1;
        std::clog << "****** depth " << depth << std::endl;
        std::clog << "  Evaluate should not change SExp's at lower depth." << std::endl;
        forEach(cascade.get<depth>(), [&](auto&& t) { // mind the &
            std::clog << "  Tensor: " << t.name() << std::endl;
            std::clog << "  Multiplicity: " << subExpressionMultiplicity(expr, t) << std::endl;
            x0 = zeros<3>();
            std::clog << "  evaluate at " << x0 << std::endl;
            evaluateSubExpression(t);
            printTensor(t);

            x0 = kroneckerDelta<0>(Seq<3>{});
            std::clog << "  evaluate at " << x0 << std::endl;
            evaluateSubExpression(t);
            printTensor(t);
          });
      });

    std::clog << "  ** substitute cascade into given expression:" << std::endl;
    std::clog << "   ** Orig:" << std::endl;
    printTensor(expr);
    auto sExprExpr = injectSubExpressions(expr, cascade);
    std::clog << "   ** Subs:" << std::endl;
    printTensor(sExprExpr);
    std::clog << "  ** change value of x:" << std::endl;
    x0 = 2 * kroneckerDelta<1>(Seq<3>{});
    printTensor(x0);
    std::clog << "  ** unevaluated sExpExpr:" << std::endl;
    printTensor(sExprExpr);
    std::clog << "  ** evaluated sExpExpr:" << std::endl;
    cascade.evaluate();
    printTensor(sExprExpr);
    std::clog << "  ** difference to original expr:" << std::endl;
    std::clog << "  ** RTEQ: " << Expressions::AreRuntimeEqual<decltype(expr), decltype(sExprExpr)>::value << std::endl;
    printTensor(expr - sExprExpr);
    std::clog << "  ** difference to fieldTensor(expr):" << std::endl;
    printTensor(fieldTensor(expr) - sExprExpr);
    std::clog << std::endl;

    std::clog << "  ** copy Cascade and update dangling references:" << std::endl;
    auto& cascadeTmp(*new std::decay_t<decltype(cascade)>(cascade));
    auto sExprExprTmp = replaceSubExpressions(sExprExpr, cascadeTmp);

    auto& cascadeCopy(*new std::decay_t<decltype(cascade)>(cascadeTmp));
    delete &cascadeTmp;

    auto sExprExprCopy = replaceSubExpressions(sExprExprTmp, cascadeCopy);
    x0 = 3 * kroneckerDelta<2>(Seq<3>{});
    cascadeCopy.evaluate();
    std::clog << "  ** does it work with the copy?" << std::endl;
    printTensor(sExprExprCopy);
    std::clog << "  ** difference to original expr:" << std::endl;
    std::clog << "  ** RTEQ: " << Expressions::AreRuntimeEqual<decltype(expr), decltype(sExprExprCopy)>::value << std::endl;
    printTensor(expr - sExprExprCopy);
    std::clog << "  ** difference to fieldTensor(expr):" << std::endl;
    printTensor(fieldTensor(expr) - sExprExprCopy);
    std::clog << std::endl;

    delete &cascadeCopy;

    ////////////////////////////////////////////////////////

    std::clog << "**** Test evaluation of one simple sub-expression ****" << std::endl;
    auto sExp = subExpression(lengthX0, Seq<>{});
    printTensor(sExp);
    printTensor(expandAllSubExpressions(sExp));
    std::clog << typeString(sExp) << std::endl;
    evaluateSubExpression(sExp, Expressions::SExpZero{});
    std::clog << "  after clear:" << std::endl;
    printTensor(sExp);
    evaluateSubExpression(sExp);
    std::clog << "  after evaluate:" << std::endl;
    printTensor(sExp);
    std::clog << "  derivative:" << std::endl;
    printTensor(derivative(sExp, x0));

    std::clog << std::endl;

    std::clog << "**** Test evaluation of one nested sub-expression ****" << std::endl;
    auto nestedSExp = subExpression(x0*x0*sExp, Seq<>{});
    printTensor(nestedSExp);
    printTensor(expandAllSubExpressions(nestedSExp));
    evaluateSubExpression(nestedSExp, Expressions::SExpZero{});
    std::clog << "  after clear:" << std::endl;
    printTensor(nestedSExp);
    evaluateSubExpression(nestedSExp);
    std::clog << "  after evaluate:" << std::endl;
    printTensor(nestedSExp);
    std::clog << "  derivative:" << std::endl;
    printTensor(derivative(nestedSExp, x0));

    std::clog << std::endl;

    std::clog << "**** Test evaluation of all contained sub-expressions ****" << std::endl;
    auto nestedSExps = subExpression(x0*x0)+nestedSExp;
    // evaluateSubExpression(nestedSExps); <-- this fails as nestedSExp is not a subexpression
    printTensor(nestedSExps);
    printTensor(expandAllSubExpressions(nestedSExps));
    evaluateAllSubExpressions(nestedSExps, Expressions::SExpZero{});
    std::clog << "  after clear:" << std::endl;
    printTensor(nestedSExps);
    evaluateAllSubExpressions(nestedSExps);
    std::clog << "  after evaluate:" << std::endl;
    printTensor(nestedSExps);
    std::clog << "  derivative:" << std::endl;
    printTensor(derivative(nestedSExps, x0));

    std::clog << std::endl;

    std::clog << "**** Test partial evaluation without const expressions ****" << std::endl;
    const auto& constNestedSExp = nestedSExp;
    auto nestedSExpsC = subExpression(x0*x0)+constNestedSExp;

    // evaluateSubExpression(nestedSExpsC); <-- this fails as nestedSExp is not a subexpression
    printTensor(nestedSExpsC);
    printTensor(expandAllSubExpressions(nestedSExpsC));
    evaluateAllSubExpressions(nestedSExpsC, Expressions::SExpZero{});
    std::clog << "  after clear:" << std::endl;
    printTensor(nestedSExpsC);
    evaluateAllSubExpressions(nestedSExpsC);
    std::clog << "  after evaluate:" << std::endl;
    printTensor(nestedSExpsC);
    std::clog << "  derivative:" << std::endl;
    printTensor(derivative(nestedSExpsC, x0));
#endif
  }
}
