#include <config.h>

#include <dune/fem/io/parameter.hh>

#include "../../common/ostream.hh"
#include "../../common/scopedredirect.hh"

#include "../tensor.hh"

/**@addtogroup LinearAlgebra
 * @{
 */
/**@addtogroup Tensors
 * @{
 */
/**@addtogroup TensorTests
 * @{
 */

using namespace Dune;
using namespace ACFem;
using namespace Tensor;

auto x0 = indeterminate<0,3>();
auto test_tensor_1 = einsum(ones<3,4,5>(), ones<3,4,5>());
auto test_tensor_2 = derivative<1>(x0*x0*x0*x0*x0, x0);
auto test_tensor_3 = derivative<2>(x0*x0*x0*x0*x0, x0);
auto test_tensor_4 = derivative<3>(x0*x0*x0*x0*x0, x0);
auto test_tensor_5 = derivative<4>(x0*x0*x0*x0*x0, x0);
auto test_tensor_6 = derivative<5>(x0*x0*x0*x0*x0, x0);
auto test_tensor_7 = derivative<6>(x0*x0*x0*x0*x0, x0);

/** Test-template main program. Instantiate a single expression
 * template and evaluate it on a simple grid.
 */
int main(int argc, char *argv[])
{
  std::clog << "I say nothing, there is (almost) no output from this program" << std::endl;
  //std::clog << typeString(test_tensor_1) << std::endl;
  return 0;
}

//!@} TensorTests

//!@} Tensors

//!@} LinearAlgebra
