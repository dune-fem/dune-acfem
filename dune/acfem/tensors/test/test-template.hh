#ifndef __DUNE_ACFEM_TENSORS_TEST_TEST_TEMPLATE_HH__
#define __DUNE_ACFEM_TENSORS_TEST_TEST_TEMPLATE_HH__

#include "../../common/ostream.hh"
#include "../../expressions/polynomialtraits.hh"
#include "../tensor.hh"

namespace Dune
{
  namespace ACFem
  {
    namespace Tensor
    {
      namespace Testing
      {
        using ACFem::operator<<;

        template<class T>
        void printTensor(T&& t, const std::string& msg, bool values = true, bool degree = false)
        {
          if (msg != "") {
            std::clog << msg << std::endl;
          }
          std::clog << (IsTensor<T>::value ? name(t) : typeString(std::forward<T>(t))) << std::endl;
          if (degree) {
            std::clog << " order: " << polynomialDegree<0>(std::forward<T>(t)) << std::endl;
          }
          //std::clog << " depth: " << Expressions::Depth<T>::value << std::endl;
          if (values) {
            std::clog << t << std::endl;
          }
        }

        template<class T>
        void printTensor(T&& t, const char* msg, bool values = true, bool degree = false)
        {
          printTensor(std::forward<T>(t), std::string(msg), values, degree);
        }

        template<class T>
        void printTensor(T&& t, bool values = true, bool degree = false)
        {
          printTensor(std::forward<T>(t), "", values, degree);
        }

      } // NS Testing
    } // NS Tensor
  } // NS ACFem
} // NS Dune

#endif // header guard
