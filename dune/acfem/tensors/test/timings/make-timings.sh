#!/bin/bash

set -x

PROG=$1

PREFIX=$HOME/Projects/DUNE/head/
#PREFIX=/storage/localhost/heinecj/projects/dune/head
SOURCEDIR=$PREFIX/dune-acfem/dune/acfem/
BUILDDIR=$PREFIX/dune-acfem/build-optim/dune/acfem/tensors/test

VARIANTS="
HEAD
sequenceslice-bool-usingtype-containsv.hh
sequenceslice-bool-usingtype.hh
sequenceslice-filtered.hh
sequenceslice-new-complement-new-reverse.hh
sequenceslice-new-complement-only-new-reverse.hh
sequenceslice-new-impl.hh
sequenceslice-old-impl-new-reverse.hh
sequenceslice-old-impl-special-complement-new-reverse.hh
sequenceslice-old-impl.hh
sequenceslice-sfinae-usingtype-containsv.hh
sequenceslice-sfinae-usingtype.hh
"

#VARIANTS="
#sequenceslice-filtered.hh
#sequenceslice-sfinae-inheritence.hh
#sequenceslice-sfinae-usingtype.hh
#sequenceslice-bool-inheritence.hh
#sequenceslice-bool-usingtype.hh"

#HEAD
#sequenceslice-new-impl.hh
#sequenceslice-old-impl-new-reverse.hh
#sequenceslice-old-impl.hh

exec > $SOURCEDIR/tensors/test/timings/${PROG}.log 2>&1

for f in $VARIANTS; do
    echo "commit $f"
    if [ "$f" = HEAD ]; then
	pushd $BUILDDIR > /dev/null 2>&1
	git stash savei > /dev/null 2>&1
	make > /dev/null 2>&1
	make tensor-${PROG}-compile-time | fgrep "user "
	git stash pop > /dev/null 2>&1
	make > /dev/null 2>&1
	popd > /dev/null 2>&1
    else
	pushd $SOURCEDIR/mpl  > /dev/null 2>&1
	ln -sf $f sequenceslice.hh  > /dev/null 2>&1
	popd  > /dev/null 2>&1
	pushd $BUILDDIR  > /dev/null 2>&1
	make tensor-${PROG}-compile-time | fgrep "user "
	popd > /dev/null 2>&1
    fi
done
