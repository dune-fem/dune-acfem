#!/bin/bash
TEST=tensor-derivative-powers-of-x
ORIG=feature/dispatch-without-constexpr-if
BUILDDIR=../../../..
SRCDIR=..
NUMITER=25

pushd $BUILDDIR
cmake -DDUNE_ACFEM_COMPILE_TIME_ITERATIONS=$NUMITER $SRCDIR
popd

exit 0

for i in $(cut -d' ' -f1 commits.txt); do
    exec > $i-timings.log 2>&1 
    set -x
    git clean -f /storage/localhost/heinecj/projects/dune/head/dune-acfem
    git checkout $i
    make && make $TEST && make $TEST-compile-time > nmh114-$TEST-gcc7-$i-timings
    git checkout $ORIG
    git clean -f /storage/localhost/heinecj/projects/dune/head/dune-acfem
    git reset --hard $ORIG
done
