#!/bin/bash

echo commit HEAD
egrep 'user|ln -sf' derivative-cos-product-2d.log|sed -r -e 's/^[+] ln -sf sequenceslice-([a-z-]+).*/commit \1/g'
