#!/opt/local/bin/gawk -f

BEGIN {
  min = 1e15;
  max = 0;
  sum = 0;
  count = 0;
  commit = "";
  state = 0;
  commitCount = 0;
  printf("no min max avg sum #samples commit\n");
}

/^commit/ {
  if (count > 0) {
    printf("%d %.3e %.3e %.3e %.3e %d %s\n", commitCount, min, max, sum/count, sum, count, commit);
    commitCount++;
  }
  commit = $2;
  // reset
  min = 1e15;
  max = 0;
  sum = 0;
  count = 0;
}

/^user/ {
  sum += $2;
  min = $2 < min ? $2 : min;
  max = $2 > max ? $2 : max;
  ++count;
}

END {
  if (count > 0) {
    printf("%d %.3e %.3e %.3e %.3e %d %s\n", commitCount, min, max, sum/count, sum, count, commit);
  }
}
