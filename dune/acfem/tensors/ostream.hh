#ifndef __DUNE_ACFEM_TENSORS_OSTREAM_HH__
#define __DUNE_ACFEM_TENSORS_OSTREAM_HH__

#include <ostream>
#include <dune/common/hybridutilities.hh>

#include "tensorbase.hh"
#include "operations/restriction.hh"
#include "../mpl/transform.hh"

namespace Dune {

  namespace ACFem {

    namespace Tensor {

      /**Output an empty dim 0 tensor.*/
      template<class T, std::enable_if_t<TensorTraits<T>::dim() == 0, int> = 0>
      std::ostream& printTensor(std::ostream& out, T&& t)
      {
        return out << "DIMZEROTENSOR";
      }

      /**Output of a rank 0 tensor, a scalar.*/
      template<class T, std::enable_if_t<(TensorTraits<T>::dim() > 0
                                          && TensorTraits<T>::rank == 0)
                                         , int> = 0>
      std::ostream& printTensor(std::ostream& out, T&& t)
      {
        return out << t();
      }

      /**Output of a rank 1 tensor, a vector.*/
      template<class T, std::enable_if_t<(TensorTraits<T>::dim() > 0
                                          && TensorTraits<T>::rank == 1)
                                         , int> = 0>
      std::ostream& printTensor(std::ostream& out, T&& t)
      {
        out << t(0);
        for (unsigned i = 1; i < t.dim(); ++i) {
          out << " " << t(i);
        }
        return out;
      }

      /**Output of a rank 2 tensor, a matrix.*/
      template<class T, std::enable_if_t<(TensorTraits<T>::dim() > 0
                                          && TensorTraits<T>::rank == 2)
                                         , int> = 0>
      std::ostream& printTensor(std::ostream& out, T&& t)
      {
        if (/*t.isZero() ||*/ t == zero(t) && TensorTraits<T>::dim() > 9) {
          return out << "NULLTENSOR";
        }
        auto rows = restriction<0>(std::forward<T>(t));
        for (unsigned i = 0; i < TensorTraits<T>::template dim<0>(); ++i) {
          rows->lookAt(i);
          out << rows << std::endl;
        }
        return out;
      }

      /**Output of a general tensor.*/
      template<class T, std::enable_if_t<(TensorTraits<T>::dim() > 0
                                          && TensorTraits<T>::rank > 2)
                                         , int> = 0>
      std::ostream& printTensor(std::ostream& out, T&& t)
      {
        if (/*t.isZero() ||*/ t == zero(t)) {
          return out << "NULLTENSOR";
        }
        constexpr std::size_t rank = TensorTraits<T>::rank;
        using Signature = typename TensorTraits<T>::Signature;
        using LeadingPositions =  MakeIndexSequence<rank-2>;
        using LeadingSignature = SequenceSlice<Signature, LeadingPositions>;
#if 0
        forLoop<multiDim(LeadingSignature{})>([&t,&out](auto i) {
            using Index = MultiIndex<i, LeadingSignature>;
            if (!t.isZero(Index{})) {
              auto subTensor = restriction(t, Index{}, LeadingPositions{});
              if (subTensor != zero(subTensor)) {
                out << "(" << toString(Index{}) << ")" << std::endl;
                out << subTensor << std::endl;
              }
            }
          });
#else
        for(unsigned i = 0; i < multiDim(LeadingSignature{}); ++i) {
          auto index = multiIndex(i, LeadingSignature{});
          auto subTensor = restriction(std::forward<T>(t), index, LeadingPositions{});
          if (subTensor != zero(subTensor)) {
            out << toString(index) << std::endl;
            out << subTensor << std::endl;
          }
        }
#endif
        return out;
      }

      /**Output operator for tensors, entry point forwarding to
       * printTensor().
       */
      template<class T, std::enable_if_t<IsProperTensor<T>::value, int> = 0>
      std::ostream& operator<<(std::ostream& out, T&& t)
      {
        return printTensor(out, std::forward<T>(t));
      }

    } // NS Tensor

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_TENSORS_OSTREAM_HH__
