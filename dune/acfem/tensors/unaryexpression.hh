#ifndef __DUNE_ACFEM_TENSORS_UNARYEXPRESSION_HH__
#define __DUNE_ACFEM_TENSORS_UNARYEXPRESSION_HH__

#include "../common/literals.hh"
#include "../expressions/storage.hh"
#include "../expressions/expressionoperations.hh"

#include "tensorbase.hh"
#include "expressionoperations.hh"

namespace Dune {

  namespace ACFem {

    namespace Tensor {

      using ACFem::operator==;

      /**@addtogroup Tensors
       * @{
       */

      /**@addtogroup TensorExpressions
       * @{
       */

      template<class F, class T>
      class UnaryTensorExpression
        : public TensorBase<typename F::template ResultType<typename TensorTraits<T>::FieldType>,
                            typename TensorTraits<T>::Signature,
                            UnaryTensorExpression<F, T> >
        , public Expressions::Storage<F, T>
      // Expressions model rvalues. We are constant if the underlying
      // expression claims to be so or if we store a copy and the
      // underlying expression is "independent" (i.e. determined at
      // runtime only by the contained data), or if the value of the
      // expression is just a compile-time constant.
        , public MPL::UniqueTags<typename AssumeTraits<F>::Properties>
      {
        using ThisType = UnaryTensorExpression;
        using Traits = TensorTraits<T>;
        using ArgType = typename Traits::TensorType;
       public:
        using Field = typename F::template ResultType<typename TensorTraits<T>::FieldType>;
        using BaseType = TensorBase<Field, typename Traits::Signature, UnaryTensorExpression>;
        using StorageType = Expressions::Storage<F, T>;
       public:
        using typename StorageType::FunctorType;
        using StorageType::operand;
        using StorageType::operation;
        using typename BaseType::Signature;
        using typename BaseType::FieldType;
        using BaseType::rank;

        static_assert(IsTensor<T>::value,
                      "T does not qualify for a unary tensor operation.");

        UnaryTensorExpression(T&& t, F&& f = F{})
          : StorageType(std::forward<F>(f), std::forward<T>(t))
        {}

        /**Allow default construction if contained types fulfill
         * IsTypedValue and the operation is not dynamic.
         */
        template<
          class... Dummy,
          std::enable_if_t<(sizeof...(Dummy) == 0
                            && IsTypedValue<T>::value
                            && !FunctorHas<IsDynamicOperation, F>::value
            ), int> = 0>
        UnaryTensorExpression(Dummy&&...)
          : StorageType(T{})
        {}

#if DUNE_ACFEM_TENSOR_WORKAROUND_GCC(7)
        struct GCCBugCompensator
        {
          template<class Dst, class Src>
          void operator()(Dst& dst, Src&& src)
          {
            this_.operation()(dst) = src;
          }

          ThisType& this_;
        };
#endif

        template<class Other,
                 std::enable_if_t<(IsTensorOperand<Other>::value
                                   && BaseType::signature() == TensorTraits<Other>::signature()
                                   && std::is_assignable<decltype(F{}(std::declval<typename Traits::ElementZero>())),
                                                         typename TensorTraits<Other>::FieldType>::value
                   ), int> = 0>
        UnaryTensorExpression& operator=(Other&& other)
        {
          assign(operand(0_c), tensor(std::forward<Other>(other)),
#if DUNE_ACFEM_TENSOR_WORKAROUND_GCC(7)
                 GCCBugCompensator{*this}
#else
                 [this](auto& dst, auto&& src) {
                   operation()(dst) = src;
                 }
#endif
            );
          return *this;
        }

        template<class... Dims,
                 std::enable_if_t<(rank == sizeof...(Dims)
                                   &&
                                   IsIntegralPack<Dims...>::value)
                                  , int> = 0>
        decltype(auto) operator()(Dims... indices)
        {
          return operation()(operand(0_c)(indices...));
        }

        template<class... Dims,
                 std::enable_if_t<(rank == sizeof...(Dims)
                                   &&
                                   IsIntegralPack<Dims...>::value)
                                  , int> = 0>
        decltype(auto) operator()(Dims... indices) const
        {
          return operation()(operand(0_c)(indices...));
        }

        template<std::size_t... Indices,
                 std::enable_if_t<(rank == sizeof...(Indices)), int> = 0>
        decltype(auto) operator()(Seq<Indices...>)
        {
          return operation()(operand(0_c)(Seq<Indices...>{}));
        }

        template<std::size_t... Indices,
                 std::enable_if_t<(rank == sizeof...(Indices)), int> = 0>
        decltype(auto) operator()(Seq<Indices...>) const
        {
          return operation()(operand(0_c)(Seq<Indices...>{}));
        }

        /**Maybe include the sign-propagation hacks ... */
        template<std::size_t... Indices, class Pos = MakeIndexSequence<sizeof...(Indices)> >
        static bool constexpr isZero(Seq<Indices...> seq = Seq<Indices...>{}, Pos pos = Pos{})
        {
          // This is WRONG for more complicated stuff
          if (ArgType::isZero(seq, pos)) {
            return FunctorType::signPropagation(ZeroExpressionSign{}).isZero;
          } else {
            return FunctorType::signPropagation(UnknownExpressionSign{}).isZero;
          }

          return false;
        }

        // in order to support projection optimizations we fake a non-const lookAt()
        template<class... Whatever>
        void lookAt(Whatever... indices)
        {
          operand(0_c).lookAt(indices...);
        }

        std::string name() const
        {
          std::string pfx = std::is_reference<T>::value ? (RefersConst<T>::value ? "cref" : "ref") : "";

#if 0
          if constexpr (std::is_reference<T>::value) {
            pfx += "[@"+toString(&operand(0_c))+"]";
          }
#endif

          return operationName(operation(), pfx+operand(0_c).name());
        }

      };

      /**Generate a unary tensor expression.
       *
       * @param t The operand.
       *
       * @param f The operation functor.
       */
      template<class F, class T, std::enable_if_t<IsProperTensor<T>::value && !FunctorHas<IsTensorOperation, F>::value, int> = 0>
      constexpr auto operate(Expressions::DontOptimize, F&& f, T&& t)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        using FType = std::decay_t<F>;
        return UnaryTensorExpression<FType, T>(std::forward<T>(t), FType(f));
      }

      /// @} TensorExpressions

      /// @} Tensors

    } // NS Tensor

  } // NS ACFem

  template<class T, class F>
  struct FieldTraits<ACFem::Tensor::UnaryTensorExpression<F, T> >
    : FieldTraits<typename F::template ResultType<typename ACFem::TensorTraits<T>::FieldType> >
  {};

} // NS Dune

#endif // __DUNE_ACFEM_TENSORS_UNARYEXPRESSION_HH__
