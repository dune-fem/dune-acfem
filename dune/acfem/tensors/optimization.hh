#ifndef __DUNE_ACFEM_TENSORS_OPTIMIZATION_HH__
#define __DUNE_ACFEM_TENSORS_OPTIMIZATION_HH__

#include "optimization/associativity.hh"
#include "optimization/constants.hh"
#include "optimization/distributivity.hh"
#include "optimization/einsum.hh"
#include "optimization/scalareinsum.hh"
#include "optimization/policy.hh"
#include "optimization/powers.hh"
#include "optimization/product.hh"
#include "optimization/reshape.hh"
#include "optimization/restriction.hh"
#include "optimization/subexpression.hh"
#include "optimization/sum.hh"
#include "optimization/transpose.hh"

#endif // __DUNE_ACFEM_TENSORS_OPTIMIZATION_HH__
