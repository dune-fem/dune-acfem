#ifndef __DUNE_ACFEM_TENSORS_EXPRESSIONTRAITS_HH__
#define __DUNE_ACFEM_TENSORS_EXPRESSIONTRAITS_HH__

#include "../expressions/expressiontraits.hh"
#include "../expressions/traitsdefault.hh"
#include "../expressions/runtimeequal.hh"
#include "tensorbase.hh"
#include "modules/constant.hh"

namespace Dune
{
  namespace ACFem
  {

    namespace Tensor
    {
      // forward
      template<class Field, class Seq>
      class ConstantTensor;

      //! @c TrueType if T is not a tensor but can be promoted to a tensor.
      template<class T>
      using IsNonTensorTensorOperand = BoolConstant<(IsTensorOperand<T>::value && !IsTensor<T>::value)>;

      template<class T, class SFINAE = void>
      struct ShouldIgnoreUnaryOperand
        : FalseType
      {};

      template<class T>
      struct ShouldIgnoreUnaryOperand<
        T,
        std::enable_if_t<(// ignore non-tensors-likes
                          !IsTensorOperand<T>::value
                          // ignore scalars
                          || IsScalar<T>::value
        )> >
        : TrueType
      {};

      /**We should not inject scalars into unary expressions as this
       * very easily will lead to ambiguities.
       */
      template<class T>
      struct IsUnaryTensorOperand
        : BoolConstant<(!Expressions::IsClosure<T>::value
                        && IsTensorOperand<T>::value
                        && !ShouldIgnoreUnaryOperand<std::decay_t<T> >::value)>
      {};

      template<class T>
      struct IsProperTensor
        : BoolConstant<(IsTensor<T>::value && !IsPromotedTopLevel<T>::value)>
      {};

      template<class T0, class T1>
      struct AreProperTensors
        : BoolConstant<(IsProperTensor<T0>::value && IsProperTensor<T1>::value)>
      {};

      /**@internal For binary expressions require that one of the two
       * operands is a "real" tensor.
       */
      template<class T1, class T2, class SFINAE = void>
      struct AreBinaryTensorOperands
        : FalseType
      {};

      /**@internal For binary expressions require that one of the two
       * operands is a "real" tensor.
       */
      template<class T1, class T2>
      struct AreBinaryTensorOperands<
        T1, T2,
        std::enable_if_t<(!(IsClosure<T1>::value || IsClosure<T2>::value)
                          && IsTensorOperand<T1>::value
                          && IsTensorOperand<T2>::value
                          && (IsTensor<T1>::value || IsTensor<T2>::value)
        )> >
        : TrueType
      {};

      /**Generate the rank of the N-th operand if applicable.*/
      template<std::size_t N, class T, class SFINAE = void>
      struct OperandRank
        : IndexConstant<TensorTraits<Operand<N, T> >::rank>
      {};

      /**Generate ~0UL if T has not so many operands.*/
      template<std::size_t N, class T>
      struct OperandRank<N, T, std::enable_if_t<(N >= Arity<T>::value)> >
        : IndexConstant<std::numeric_limits<std::size_t>::max()>
      {};

      /**Refer to the wrapper tensor in order to decide whether
       * operands are runtime equal.
       */
      template<class T1, class T2>
      using AreRuntimeEqualOperands =
        Expressions::AreRuntimeEqual<typename TensorTraits<T1>::TensorType,
                                     typename TensorTraits<T2>::TensorType>;

      /**Inject FractionConstant as valid tensor operand.*/
      template<class Int, Int N, Int D>
      struct TensorTraits<TypedValue::FractionConstant<Int, N, D> >
        : public TensorTraits<ConstantTensor<TypedValue::FractionConstant<Int, N, D>, Seq<> > >
      {
        //static_assert(!IsFieldObject<T>::value, "");

        template<class Fraction>
        static constexpr auto toTensor(Fraction)
        {
          return ConstantTensor<Fraction, Seq<> >();
        }
      };

      /**Inject FractionConstant as valid tensor operand.*/
      template<class Int, Int N, Int D>
      struct IsTensorOperand<TypedValue::FractionConstant<Int, N, D> >
        : TrueType
      {};

      /**Inject NamedConstant as valid tensor operand.*/
      template<class T, char... Name>
      struct TensorTraits<TypedValue::NamedConstant<T, Name...> >
        : public TensorTraits<ConstantTensor<TypedValue::NamedConstant<T, Name...>, Seq<> > >
      {
        template<class C>
        static constexpr auto toTensor(C)
        {
          return ConstantTensor<C, Seq<> >();
        }
      };

      /**Inject NamedConstant as valid tensor operand.*/
      template<class T, char... Name>
      struct IsTensorOperand<TypedValue::NamedConstant<T, Name...> >
        : TrueType
      {};

      /**TrueType for unary tensor expressions.*/
      template<class T>
      using IsUnaryTensorExpression = BoolConstant<IsTensor<T>::value && IsUnaryExpression<T>::value>;

      /**TrueType for binary tensor expressions.*/
      template<class T>
      using IsBinaryTensorExpression = BoolConstant<IsTensor<T>::value && IsBinaryExpression<T>::value>;

#define SPECIALIZEDTRAITS_DEFINED 1
      /**Should be specialized to TrueType if for a specific tensor
       * ExpressionTraits need to be re-implemented.
       */
      template<class T, class SFINAE = void>
      struct HasSpecializedExpressionTraits
        : FalseType
      {};

    } // NS Tensor

    template<class T>
    struct ExpressionTraits<
      T,
      std::enable_if_t<(!Tensor::HasSpecializedExpressionTraits<T>::value
                        && Tensor::IsTensorNotZero<T>::value
                        && Expressions::IsSelfExpression<T>::value
      )> >
    {
      using ExpressionType = std::decay_t<T>;
     private:
      using Traits = TensorTraits<T>;
      using DefaultTraits = Expressions::TraitsOfTags<T>;
      using FieldTraits = ExpressionTraits<typename Traits::FieldType>;
     public:
      static constexpr bool isZero = DefaultTraits::isZero || FieldTraits::isZero;
      static constexpr bool isNonZero = DefaultTraits::isNonZero || FieldTraits::isNonZero;
      static constexpr bool isOne = (DefaultTraits::isOne || FieldTraits::isOne) && Traits::rank == 0;
      static constexpr bool isMinusOne = (DefaultTraits::isMinusOne || FieldTraits::isMinusOne) && Traits::rank == 0;
      static constexpr bool isSemiPositive = DefaultTraits::isSemiPositive || FieldTraits::isSemiPositive;
      static constexpr bool isSemiNegative = DefaultTraits::isSemiNegative || FieldTraits::isSemiNegative;
      static constexpr bool isPositive = DefaultTraits::isPositive || FieldTraits::isPositive;
      static constexpr bool isNegative = DefaultTraits::isNegative || FieldTraits::isNegative;

      static constexpr bool isVolatile = DefaultTraits::isVolatile;
      static constexpr bool isIndependent = DefaultTraits::isIndependent;
      static constexpr bool isTypedValue = DefaultTraits::isTypedValue || FieldTraits::isTypedValue;
      static constexpr bool isConstant = DefaultTraits::isConstant;

      using Sign = ExpressionSign<isNonZero, isSemiPositive, isSemiNegative>;
    };

    template<class T>
    struct ExpressionTraits<
      T,
      std::enable_if_t<(!Tensor::HasSpecializedExpressionTraits<T>::value
                        && Tensor::IsTensorZero<T>::value
      )> >
      : ZeroExpressionTraits<T>
    {};

    namespace Expressions
    {

      template<class T>
      struct IsOperand<
        T, std::enable_if_t<(IsDecay<T>::value
                             && !IsExpression<T>::value
                             && Tensor::IsTensorOperand<T>::value
          )> >
        : TrueType
      {};

      template<class T>
      struct InjectionTraits<T, std::enable_if_t<Tensor::IsTensorOperand<T>::value> >
        : ExpressionTraits<T>
      {
        using ExpressionType = typename TensorTraits<T>::TensorType;

        template<class E>
        static decltype(auto) asExpression(E&& e)
        {
          return Tensor::tensor(std::forward<E>(e));
        }
      };

    } // NS Expressions

  } // NS ACFem

} // NS Dune

#endif //  __DUNE_ACFEM_TENSORS_EXPRESSIONTRAITS_HH__
