#ifndef __DUNE_ACFFEM_TENSORS_POLYNOMIALTRAITS_HH_
#define __DUNE_ACFFEM_TENSORS_POLYNOMIALTRAITS_HH_

#include "../expressions/polynomialtraits.hh"
#include "binaryexpression.hh"

namespace Dune {

  namespace ACFem {

    namespace Tensor {

      using Expressions::polynomialDegree;

      /**Exponentiation of a scalar by a scalar.*/
      template<std::size_t IndeterminateId, class E,
               std::enable_if_t<(IsPowExpression<E>::value
                                 && IsTensor<E>::value
                                 && TensorTraits<E>::rank == 0
                                 && !Expressions::HasOrderMethod<E, IndeterminateId>::value
                 ), int> = 0>
      constexpr auto polynomialDegree(PriorityTag<10>, E&& e, IndexConstant<IndeterminateId> = IndexConstant<IndeterminateId>{})
      {
        using Exponent = Operand<1, E>;
        using Base =  Operand<0, E>;
        if constexpr (hasConstantPolynomialDegree<Base, IndeterminateId>()
                      && IsFractionConstantTensor<Exponent>::value // exponent
                      && hasConstantPolynomialDegreeOf<0, Exponent>()) {
          if constexpr (Exponent::data() < 0_f || ceil(Exponent::data()) != Exponent::data()) {
            return (std::size_t)polynomialDegree<IndeterminateId>(std::forward<E>(e).operand(0_c));
          } else {
            return ceil(polynomialDegree<IndeterminateId>(std::forward<E>(e).operand(0_c)) * Exponent::data());
          }
        } else {
          const auto exp = exponent(std::forward<E>(e))();
          if (polynomialDegree<IndeterminateId>(exp) != 0 || exp < 0  || (std::ptrdiff_t)exp != exp) {
            // don't know what to do
            return (std::size_t)polynomialDegree<IndeterminateId>(std::forward<E>(e).operand(0_c));
          } else {
            return (std::size_t)(polynomialDegree<IndeterminateId>(std::forward<E>(e).operand(0_c)) * exp + exp / 2_f);
          }
        }
      }

      /**Exponentiation by a scalar.*/
      template<std::size_t IndeterminateId, class E,
               std::enable_if_t<(IsPowExpression<E>::value
                                 && IsTensor<E>::value
                                 && TensorTraits<E>::rank > 0
                                 && IsEinsumExpression<ExponentOfPower<E> >::value
                                 && TensorTraits<Operand<0, ExponentOfPower<E> > >::rank == 0
                                 && IsOnes<Operand<1, ExponentOfPower<E> > >::value
                                 && !Expressions::HasOrderMethod<E, IndeterminateId>::value
                 ), int> = 0>
      constexpr auto polynomialDegree(PriorityTag<10>, E&& e, IndexConstant<IndeterminateId>)
      {
        using Exponent = Operand<0, Operand<1, E> >;
        using Base = Operand<0, E>;
        if constexpr (hasConstantPolynomialDegree<Base, IndeterminateId>()
                      && IsFractionConstantTensor<Exponent>::value
                      && hasConstantPolynomialDegreeOf<0, Exponent>()) {
          if constexpr (Exponent::data() < 0 || ceil(Exponent::data()) != Exponent::data()) {
            // don't know what to do
            return (std::size_t)polynomialDegree<IndeterminateId>(std::forward<E>(e).operand(0_c));
          } else {
            return ceil(polynomialDegree<IndeterminateId>(std::forward<E>(e).operand(0_c)) * Exponent::data());
          }
        } else {
          auto exponent = std::forward<E>(e).operand(1_c).operand(0_c)();
          if (polynomialDegree<IndeterminateId>(exponent) != 0 || exponent < 0 || (std::ptrdiff_t)exponent != exponent) {
            // don't know what to do
            return (std::size_t)polynomialDegree<IndeterminateId>(std::forward<E>(e).operand(0_c));
          } else {
            return (std::size_t)(polynomialDegree<IndeterminateId>(std::forward<E>(e).operand(0_c)) * exponent + exponent / 2_f);
          }
        }
      }

    } // NS Tensor

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFFEM_TENSORS_POLYNOMIALTRAITS_HH_
