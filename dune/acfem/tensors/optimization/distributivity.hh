#ifndef __DUNE_ACFEM_TENSORS_OPTIMIZATION_DISTRIBUTIVITY_HH__
#define __DUNE_ACFEM_TENSORS_OPTIMIZATION_DISTRIBUTIVITY_HH__

#include "../../mpl/typetupleuniq.hh"
#include "../../mpl/lexcompare.hh"
#include "../operations/reassociatetree.hh"
#include "explodesum.hh"
#include "sumoperandtuple.hh"
#include "policy.hh"

#ifdef STATIC_ASSERT
# undef STATIC_ASSERT
#endif
//#define STATIC_ASSERT(a, b) static_assert(a, b)
#define STATIC_ASSERT(a, b) /* nothing */

#define SKIP_ONE_ELEMENT_TUPLES true

namespace Dune::ACFem {

  namespace Tensor::Optimization::Distributivity {

    using namespace ProductOperations;
    using Sums::ProcessSumOperands;
    using Sums::OnesNode;
    using Sums::IsOnesNodeV;
    using Sums::ReassembleProduct;
    using Sums::Explode;
    using Expressions::Sums::sumUp;

    using FactorizeTag = Expressions::OptimizeGap2;
    using SumUpTag = OptimizeOrdinary;

    namespace Left {

      /**@internal (left-)Factor-out work-horse. @a Operands is an
       * exploded sum-expression as generatated by
       * ProductOperations::ExplodeSum. The resulting object is an
       * optimized expression composed of TreeData and OperationPair
       * structures which can then be passed on to
       * Expressions::Sums::sumUp().
       *
       * @param ScalarTodo Operands which may or may not contain
       * tuples of scalar factors.
       *
       * @param NonScalarTodo Operands which are known not to contain
       * tuples of scalar facrtors.
       *
       * @param Done Operands from which our code cannot factor out
       * common factors.
       */
      template<class ScalarTodo, class NonScalarTodo, class Done>
      struct FactorOutHelper;

#if SKIP_ONE_ELEMENT_TUPLES
      template<class T>
      struct FactorOutHelper<MPL::TypeTuple<T>, void, void>
      {
        using Type = MPL::TypeTuple<T>;
      };

      template<class T>
      struct FactorOutHelper<void, MPL::TypeTuple<T>, void>
      {
        using Type = MPL::TypeTuple<T>;
      };
#endif
    }

    namespace Right {

      /**@internal (right-)Factor-out work-horse. @a Operands is an
       * exploded sum-expression as generatated by
       * ProductOperations::ExplodeSum. The resulting object is an
       * optimized expression composed of TreeData and OperationPair
       * structures which can then be passed on to
       * Expressions::Sums::sumUp().
       */
      template<class Todo, class Done>
      struct FactorOutHelper;

#if SKIP_ONE_ELEMENT_TUPLES
      template<class T>
      struct FactorOutHelper<MPL::TypeTuple<T>, void>
      {
        using Type = MPL::TypeTuple<T>;
      };
#endif

    }

    template<class Todo, class NonScalarTodo>
    using FactorOutLeft = typename Left::FactorOutHelper<Todo, NonScalarTodo, void>::Type;

    template<class Todo>
    using FactorOutRight = typename Right::FactorOutHelper<Todo, void>::Type;

    /**@internal Factor-out work-horse. @a Operands is an
     * exploded sum-expression as generatated by
     * ProductOperations::ExplodeSum. The resulting object is an
     * optimized expression composed of TreeData and OperationPair
     * structures which can then be passed on to
     * Expressions::Sums::sumUp().
     */
    template<class Todo, class NonScalarTodo = void>
    using FactorOut = FactorOutRight<FactorOutLeft<Todo, NonScalarTodo> >;

    /**@internal Lexicographical ordering of contraction/product index
     * positions.
     */
    template<class A, class B>
    constexpr inline std::ptrdiff_t LexCompareProductOperationsV =
      MPL::LexCompareV<SequenceCat<typename A::LeftIndexPositions, typename A::RightIndexPositions>,
                       SequenceCat<typename B::LeftIndexPositions, typename B::RightIndexPositions> >;

    template<class T>
    struct CleanupHelper
    {
      using Type = T;
    };

    template<class T>
    struct CleanupHelper<MPL::TypeTuple<T> >
    {
      using Type = T;
    };

    template<class T>
    using CleanupRecursion = typename CleanupHelper<T>::Type;

    template<class TreeNode, class OtherSwitch>
    struct SignSwitch;

    template<>
    struct SignSwitch<void, void>
    {
      using PlusOperands = MPL::TypeTuple<>;
      using MinusOperands = MPL::TypeTuple<>;
    };

    template<class F, class Left, class Right, class Tag, class OtherSwitch>
    struct SignSwitch<OperationPair<PlusFunctor, F, Left, Right, Tag>, OtherSwitch>
    {
      using TreeNode = OperationPair<PlusFunctor, F, Left, Right, Tag>;
      using PlusOperands = MPL::AddFrontType<TreeNode, typename OtherSwitch::PlusOperands>;
      using MinusOperands = typename OtherSwitch::MinusOperands;
    };

    template<class F, class Left, class Right, class Tag, class OtherSwitch>
    struct SignSwitch<OperationPair<MinusFunctor, F, Left, Right, Tag>, OtherSwitch>
    {
      using TreeNode = OperationPair<MinusFunctor, F, Left, Right, Tag>;
      using PlusOperands = typename OtherSwitch::PlusOperands;
      using MinusOperands = MPL::AddFrontType<TreeNode, typename OtherSwitch::MinusOperands>;
    };

    template<class Switch>
    using SignedOperands = MPL::TypeTupleCat<typename Switch::PlusOperands, typename Switch::MinusOperands>;

    template<class Factorizer>
    using GetFactorizations = SignedOperands<typename Factorizer::Factorizations>;

#define USE_SIGN_SWITCH false

    namespace Right {

      /**@internal Compare operands according to their "left affinity"
       */
      template<class A, class B, class SFINAE = void>
      struct FactorSwap
      {
        static constexpr bool value = Einsum::leftAffinity<TreeExpression<typename A::TreeNode1> >() < Einsum::leftAffinity<TreeExpression<typename B::TreeNode1> >();
      };

      /**@internal Move contractions to the left.*/
      template<class A, class B>
      struct FactorSwap<A, B, std::enable_if_t<(!HasEinsumFunctorV<A> && HasEinsumFunctorV<B>)> >
        : TrueType
      {};

      /**@internal Compare according to the lexicographical ordering
       * of the contraction/multiplication indices.
       */
      template<class A, class B>
      struct FactorSwap<A, B, std::enable_if_t<(
        HasEinsumFunctorV<A> == HasEinsumFunctorV<B>
        && LexCompareProductOperationsV<typename A::FunctorType, typename B::FunctorType> > 0
        )> >
        : TrueType
      {};

      template<class T, class SFINAE = void>
      struct CandidateFilter
      {
        using Candidate = MPL::TypeTuple<>;
        using Dummy = MPL::TypeTuple<T>;
      };

      template<class T>
      struct CandidateFilter<
        T,
        std::enable_if_t<!IsOnesNodeV<T> && IsRuntimeEqual<RightMostTreeFactor<T> >::value>
        >
      {
        using Candidate = MPL::TypeTuple<T>;
        using Dummy = MPL::TypeTuple<>;
      };

      /**@internal In order to simplify factoring out we sort
       * everything s.t. operands with identical left-most factors
       * appear next to each other.
       */
      template<class Operands>
      using SortedCandidates =
        MPL::MergeSort<MPL::TypeTupleTransform<Operands, TreeFactorOutRight>, FactorSwap>;

      /**@internal Match two equal left operands and functors.
       */
      template<class A, class B, class SFINAE = void>
      constexpr inline bool AreMatchingCandidatesV = AreRuntimeEqual<TreeExpression<typename A::TreeNode1>, TreeExpression<typename B::TreeNode1> >::value;

      template<class T>
      constexpr inline bool AreMatchingCandidatesV<void, T> = false;

      /**@internal Left-factors with non-matching functors cannot be
       * factored out.
       */
      template<class A, class B>
      constexpr inline bool AreMatchingCandidatesV<
        A, B, std::enable_if_t<!std::is_same<typename A::FunctorType, typename B::FunctorType>::value> > = false;

      /**@internal Collect all operands with positive sign.*/
      template<class T, class PrevOps, class SFINAE = void>
      struct CollectOperands
      {
        using LeftFactor = typename T::TreeNode0;
        using PlusOperands = MPL::AddFrontType<LeftFactor, typename PrevOps::PlusOperands>;
        using MinusOperands = typename PrevOps::MinusOperands;
      };

      /**@internal Collect all operands with negative sign.*/
      template<class T, class PrevOps>
      struct CollectOperands<T, PrevOps, std::enable_if_t<!IsPositiveTreeNodeV<T> > >
      {
        using LeftFactor = NegativeTreeNode<typename T::TreeNode0>;
        using PlusOperands = typename PrevOps::PlusOperands;
        using MinusOperands = MPL::AddFrontType<LeftFactor, typename PrevOps::MinusOperands>;
      };

      template<>
      struct CollectOperands<void, void>
      {
        using PlusOperands = MPL::TypeTuple<>;
        using MinusOperands = MPL::TypeTuple<>;
      };

      /**@internal If we have operands with positive sign move them to
       * the left to avoid an unary minus operation.
       */
      template<class T0, class PlusOperands, class MinusOperands>
      struct FactorOutCandidate
      {
        using Factorization = OperationPair<
          PlusFunctor, typename T0::FunctorType,
          MPL::TypeTupleCat<PlusOperands, MinusOperands>,
          typename T0::TreeNode1,
          SumUpTag>;
      };

      /**@internal If we do not have operands with positive sign w
       * shift the minus sign to the outside and make the inner
       * operands positive.
       */
      template<class T0, class MinusOperands>
      struct FactorOutCandidate<T0, MPL::TypeTuple<>, MinusOperands>
      {
        using Factorization = OperationPair<
          MinusFunctor, typename T0::FunctorType,
          SetTreeNodeSign<PlusFunctor, MinusOperands>,
          typename T0::TreeNode1,
          SumUpTag>;
      };

      template<class T0, class Operands>
      using CreateFactorization = typename FactorOutCandidate<T0, typename Operands::PlusOperands, typename Operands::MinusOperands>::Factorization;

      template<class T, class Factor, class SFINAE = void>
      struct ProcessCandidates;

      /**@internal Endpoint with no candidates left.*/
      template<class Factor>
      struct ProcessCandidates<MPL::TypeTuple<>, Factor>
      {
        using Operands = CollectOperands<void, void>;
#if USE_SIGN_SWITCH
        using Factorizations = SignSwitch<void, void>;
#else
        using Factorizations = MPL::TypeTuple<>;
#endif
        using Remaining = MPL::TypeTuple<>;
      };

      /**@internal No-match one element end-point.*/
      template<class T0>
      struct ProcessCandidates<MPL::TypeTuple<T0>, void>
      {
        using Operands = CollectOperands<void, void>;
#if USE_SIGN_SWITCH
        using Factorizations = SignSwitch<void, void>;
#else
        using Factorizations = MPL::TypeTuple<>;
#endif
        using Remaining = MPL::TypeTuple<CollapseOnes<T0> >;
      };

      /**@internal No match continue searching.*/
      template<class T0, class T1, class... T>
      struct ProcessCandidates<
        MPL::TypeTuple<T0, T1, T...>, void,
        std::enable_if_t<!AreMatchingCandidatesV<T0, T1> > >
      {
        using Recursion = ProcessCandidates<MPL::TypeTuple<T1, T...>, void>;

        using Factorizations = typename Recursion::Factorizations;

        using Remaining = MPL::AddFrontType<CollapseOnes<T0>, typename Recursion::Remaining>;
      };

      /**@internal Found two equal factors, start with the
       * factorization.
       */
      template<class T0, class T1, class... T>
      struct ProcessCandidates<
        MPL::TypeTuple<T0, T1, T...>, void,
        std::enable_if_t<AreMatchingCandidatesV<T0, T1> > >
      {
        using Recursion = ProcessCandidates<MPL::TypeTuple<T1, T...>, MPL::TypeWrapper<T0> >;
        using Operands = CollectOperands<T0, typename Recursion::Operands>;
        using Factorization = CreateFactorization<T0, Operands>;
#if USE_SIGN_SWITCH
        using Factorizations = SignSwitch<Factorization, typename Recursion::Factorizations>;
#else
        using Factorizations = MPL::AddFrontType<Factorization, typename Recursion::Factorizations>;
#endif
        using Remaining = typename Recursion::Remaining;
      };

      /**@internal Continue collecting matching factors for the
       * current factorization.
       */
      template<class Factor, class T0, class... T>
      struct ProcessCandidates<
        MPL::TypeTuple<T0, T...>, MPL::TypeWrapper<Factor>,
        std::enable_if_t<AreMatchingCandidatesV<Factor, T0> > >
      {
        using Recursion = ProcessCandidates<MPL::TypeTuple<T...>, MPL::TypeWrapper<Factor> >;
        using Operands = CollectOperands<T0, typename Recursion::Operands>;
        using Factorizations = typename Recursion::Factorizations;
        using Remaining = typename Recursion::Remaining;
      };

      /**@internal End of current factorization, start searching for a
       * new one.
       */
      template<class Factor, class T0, class... T>
      struct ProcessCandidates<
        MPL::TypeTuple<T0, T...>, MPL::TypeWrapper<Factor>,
        std::enable_if_t<!AreMatchingCandidatesV<Factor, T0> > >
      {
        using Recursion = ProcessCandidates<MPL::TypeTuple<T0, T...>, void>;
        using Operands = CollectOperands<void, void>;
        using Factorizations = typename Recursion::Factorizations;
        using Remaining = typename Recursion::Remaining;
      };

      /////////////////////////

      template<class... Todo>
      struct FactorOutHelper<MPL::TypeTuple<Todo...>, void>
      {
        using Factorizer = ProcessCandidates<SortedCandidates<MPL::TypeTupleCat<typename CandidateFilter<Todo>::Candidate...> >, void>;
#if USE_SIGN_SWITCH
        using Factorizations = GetFactorizations<Factorizer>;
#else
        using Factorizations = typename Factorizer::Factorizations;
#endif
        using Remaining = MPL::TypeTupleCat<typename Factorizer::Remaining, typename CandidateFilter<Todo>::Dummy...>;

        using Type = typename FactorOutHelper<Factorizations, Remaining>::Type;
      };

      ///////////////////////////

      //!@internal
      template<class Node>
      struct Recursion;

      /**@internal Try to factor out further factors for the given list of
       * sum-operands.
       */
      template<class Sign, class F, class... TreeData0, class TreeData1, class Tag>
      struct Recursion<OperationPair<Sign, F, MPL::TypeTuple<TreeData0...>, TreeData1, Tag> >
      {
        using Type = OperationPair<
          Sign, F,
          CleanupRecursion<FactorOutRight<MPL::TypeTuple<TreeData0...> > >,
          TreeData1,
          Tag>;
        STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
      };

      //////////////////////

      /**@internal Recursion end-point at one level. We enter here with
       * @a Candidates being a type-tuple with operands with a constant
       * left factor. @a Done is a list of operands which cannot be
       * factored to the left.
       *
       * Postprocess the factorizations and potentially recurse to
       * collect further factors.
       */
      template<class... Factorizations, class... Done>
      struct FactorOutHelper<MPL::TypeTuple<Factorizations...>,  MPL::TypeTuple<Done...> >
      {
        // XXXX Done... are the inner-most operands which cannot be
        // factored further left or right. This could be used to
        // hook-in optimizations working on a tuple of sum-operands.

        using Type = MPL::TypeTupleCat<
          MPL::TypeTuple<typename Recursion<Factorizations>::Type...>,
          typename ProcessSumOperands<MPL::TypeTuple<Done...> >::Type
          >;
        STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
      };

    }

    namespace Left {

      /////////////////////////////

      template<class T>
      constexpr inline bool HasScalarFactorTupleV = false;

      template<class Sign, class... Factors, class Node1, class Tag>
      constexpr inline bool HasScalarFactorTupleV<OperationPair<Sign, ScalarEinsumFunctor, MPL::TypeTuple<Factors...>, Node1, Tag> > = true;

      //////////////////////

      template<class>
      struct ScalarsFromExplodeHelper;

      template<class... T>
      struct ScalarsFromExplodeHelper<MPL::TypeTuple<T...> >
      {
        using Type = MPL::MergeSorted<MPL::TypeTuple<MPL::Unique<typename T::TreeNode0, Einsum::Unequal>...>, Einsum::Swap>;
      };

      /**@internal Merge all scalar factor tuples into one sorted
       * MPL::TypeTuple in order to find the common factors with the
       * highest multiplicity.
       */
      template<class ScalarExplode>
      using ScalarsFromExplode = typename ScalarsFromExplodeHelper<ScalarExplode>::Type;

      /////////////////////

      template<class T, class SFINAE = void>
      struct CandidateFilter
      {
        using ScalarCandidate = MPL::TypeTuple<>;
        using NonScalarCandidate = MPL::TypeTuple<>;
        using DummyCandidate = MPL::TypeTuple<T>;
      };

      template<class T>
      struct CandidateFilter<T, std::enable_if_t<HasScalarFactorTupleV<T> && IsRuntimeEqual<LeftMostTreeFactor<T> >::value> >
      // Perhaps the following would be faster ...
      //
      // IsRuntimeEqual<TreeExpression<MPL::FrontType<typename T::TreeNode0> > >::value
      {
        using ScalarCandidate = MPL::TypeTuple<T>;
        using NonScalarCandidate = MPL::TypeTuple<>;
        using DummyCandidate = MPL::TypeTuple<>;
      };

      template<class T>
      struct CandidateFilter<
        T,
        std::enable_if_t<HasScalarFactorTupleV<T>
                         && !IsRuntimeEqual<LeftMostTreeFactor<T> >::value> >
        : CandidateFilter<ReassembleProduct<T> >
      {};

      template<class T>
      struct CandidateFilter<
        T,
        std::enable_if_t<(!HasScalarFactorTupleV<T>
                          && IsRuntimeEqual<LeftMostTreeFactor<T> >::value
                          && TensorTraits<LeftMostTreeFactor<T> >::rank > 0
//                          && !IsOnesNodeV<T>
        )> >
      {
        using ScalarCandidate = MPL::TypeTuple<>;
        using NonScalarCandidate = MPL::TypeTuple<T>;
        using DummyCandidate = MPL::TypeTuple<>;
      };

      /**@internal
       *
       * @param[in] LeftFactor The scalar expression we try to factor
       *        out to the left.
       *
       * @param[in] Tag OptimizationTag for the generated OperationPair.
       *
       * @param[out] Input An MPL::TypeTuple of candidates which may
       *        contain @a LeftFactor. The input-tuple contains only
       *        expressions with scalar factors to the left, but some
       *        expressions may not contain @a LeftFactor.
       *
       * @param[out] RightFactors What remains from the expressions in
       *        the @a Input tuple after factoring out @a LeftFactor
       *
       * @param[out] Remaining The remaining expressions from the @a
       *       Input tuple which did not contain a @a LeftFactor.
       *
       * @param[in] SFINAE Flow-control.
       *
       * This is the defalut implementation and the recursion
       * end-point. @a SFINAE is used to specialize for the two cases
       * contains/does not contain a @a LeftFactor.
       */
      template<class Input, class LeftFactor, class PlusOperands = MPL::TypeTuple<>, class MinusOperands = MPL::TypeTuple<>, class Remaining = MPL::TypeTuple<>, class SFINAE = void>
      struct FactorOutScalar;
#if 0
      {
        static_assert(!MPL::HasMatchingTypeV<LeftFactor, typename MPL::FrontType<Input>::TreeNode0, Einsum::Unequal>, "blah");
      };
#endif

      /**@internal Recursion end-point with positive and possibly also
       * negative sum-operands, The positive operands are moved to the
       * left in order to avoid an unary minus.
       */
      template<class LeftFactor, class PlusOperands, class MinusOperands, class Rem>
      struct FactorOutScalar<MPL::TypeTuple<>, LeftFactor, PlusOperands, MinusOperands, Rem>
      {
        using Factorization = OperationPair<PlusFunctor, ScalarEinsumFunctor, LeftFactor, MPL::TypeTupleCat<PlusOperands, MinusOperands>, SumUpTag>;
        using Remaining = Rem;
      };

      /**@internal Recursion end-point without positive operands. Move
       *the resulting unary minus to the front of the factorization
       *and make all inner sum-operands positive.
       *
       */
      template<class LeftFactor, class MinusOperands, class Rem>
      struct FactorOutScalar<MPL::TypeTuple<>, LeftFactor, MPL::TypeTuple<>, MinusOperands, Rem>
      {
        using Factorization = OperationPair<MinusFunctor, ScalarEinsumFunctor, LeftFactor, SetTreeNodeSign<PlusFunctor, MinusOperands>, SumUpTag>;
        using Remaining = Rem;
      };

      /**@internal Found a match with positive sign.*/
      template<class Node0, class Node1, class Tag, class... T,
               class LeftFactor, class PlusOperands, class MinusOperands,
               class Rem>
      struct FactorOutScalar<
        MPL::TypeTuple<OperationPair<PlusFunctor, ScalarEinsumFunctor, Node0, Node1, Tag>, T...>,
        LeftFactor, PlusOperands, MinusOperands,
        Rem,
        std::enable_if_t<MPL::HasMatchingTypeV<LeftFactor, Node0, Einsum::Unequal> >
        >
      {
        using RemainingFactors = MPL::RemoveFirstMatching<LeftFactor, Node0, Einsum::Unequal, Einsum::Swap>;
        using RightFactor = ConditionalType<
          RemainingFactors::size() == 0,
          Node1,
          OperationPair<PlusFunctor, ScalarEinsumFunctor, RemainingFactors, Node1, Tag>
          >;
        using Recursion = FactorOutScalar<
          MPL::TypeTuple<T...>,
          LeftFactor,
          MPL::PushBackType<RightFactor, PlusOperands>,
          MinusOperands,
          Rem>;
        using Factorization = typename Recursion::Factorization;
        using Remaining = typename Recursion::Remaining;
      };

      /**@internal Found a match with negative sign.*/
      template<class Node0, class Node1, class Tag, class... T,
               class LeftFactor, class PlusOperands, class MinusOperands,
               class Rem>
      struct FactorOutScalar<
        MPL::TypeTuple<OperationPair<MinusFunctor, ScalarEinsumFunctor, Node0, Node1, Tag>, T...>,
        LeftFactor, PlusOperands, MinusOperands,
        Rem,
        std::enable_if_t<MPL::HasMatchingTypeV<LeftFactor, Node0, Einsum::Unequal> >
        >
      {
        using RemainingFactors = MPL::RemoveFirstMatching<LeftFactor, Node0, Einsum::Unequal, Einsum::Swap>;
        using RightFactor = ConditionalType<
          RemainingFactors::size() == 0,
          MergeTreeNodeSign<MinusFunctor, Node1>,
          OperationPair<MinusFunctor, ScalarEinsumFunctor, RemainingFactors, Node1, Tag>
          >;
        using Recursion = FactorOutScalar<
          MPL::TypeTuple<T...>,
          LeftFactor,
          PlusOperands,
          MPL::PushBackType<RightFactor, MinusOperands>,
          Rem>;
        using Factorization = typename Recursion::Factorization;
        using Remaining = typename Recursion::Remaining;
      };

      /**@internal Mismatch for the current factor @a
       * LeftFactor. Remember the mis-match for the next round.
       */
      template<class T0, class... T, class LeftFactor, class PlusOperands, class MinusOperands, class Rem>
      struct FactorOutScalar<
        MPL::TypeTuple<T0, T...>,
        LeftFactor, PlusOperands, MinusOperands,
        Rem,
        std::enable_if_t<!MPL::HasMatchingTypeV<LeftFactor, typename T0::TreeNode0, Einsum::Unequal> >
        >
      {
        using Recursion = FactorOutScalar<
          MPL::TypeTuple<T...>,
          LeftFactor,
          PlusOperands, MinusOperands,
          MPL::PushBackType<T0, Rem>
          >;
        using Factorization = typename Recursion::Factorization;
        using Remaining = typename Recursion::Remaining;
      };

      ////////////////////////////////

      /**@internal Process all operands with a scalar-factor tuple.
       *
       * This is the "default" implementation which acts as recursion
       * end-point and constructs the factor with the highest
       * multiplicity by the default value for the @a Factor
       * parameter.
       *
       * @param T An MPL::TypeTuple with candidates with a scalar
       * factor tuple.
       *
       * @param Factor An MPL::TypePair which is here initialized to
       * the factor with the highest multiplicity. A specialization of
       * the class-template takes effect if the multiplicity of the
       * factor is at least 2 in which case it is factored out to the
       * left.
       */
      template<class T, class Factor = MPL::MultiplicitySelect<ScalarsFromExplode<T>, Einsum::Unequal>, class SFINAE = void>
      struct ProcessScalars
      {
        /**An SignSwitch holding all factorizations from this
         * recursion level and all lower levels (this is the recursion
         * end-point, so it is empty).
         */
#if USE_SIGN_SWITCH
        using Factorizations = SignSwitch<void, void>;
#else
        using Factorizations = MPL::TypeTuple<>;
#endif

        /**An MPL::TypeTuple holding all sum-operands which could not
         * be factorized by the scalar-factorization code. As this is
         * the recursion end-point it just consists of all candidates.
         */
        using Remaining = T;
      };

      /**@internal Process all operands with a scalar-factor tuple.
       *
       * The code first factors out the scalar RTEQ factor with the
       * hightes "multiplicity" (meaning the number of sum-operands
       * containing the factor).
       *
       * Then it continues with the remaining sum-operands and
       * examines their "highest multiplicity" factorization.
       *
       * The recursion stops if no factor with multiplicity >= 2 can
       * be found.
       *
       * @param T An MPL::TypeTuple of candidates.
       *
       * @param Factor An MPL::TypePair with the factor with the
       * highest "multiplicity" as second component and the
       * multiplicity as first argument.
       */
      template<class T, class Factor>
      struct ProcessScalars<T, Factor, std::enable_if_t<(Factor::First::value > 1)> >
      {
        using LeftFactor = typename Factor::Second;
        using Factorizer = FactorOutScalar<T, LeftFactor>;
        using Factorization = typename Factorizer::Factorization;
        using Recursion = ProcessScalars<typename Factorizer::Remaining>;

        /**An MPL::TypeTuple with the union of the factorization found
         * at this recursion level and all the factorization s found
         * at deeper recursion levels.
         */
#if USE_SIGN_SWITCH
        using Factorizations = SignSwitch<Factorization, typename Recursion::Factorizations>;
#else
        using Factorizations = MPL::AddFrontType<Factorization, typename Recursion::Factorizations>;
#endif

        /**An MPL::TypeTuple with the could-not-be-factorized elements
         * from the deepest recursion level.
         */
        using Remaining = typename Recursion::Remaining;
      };

      /////////////////////////

      /**@internal Lexicographical ordering of contraction/product
       * index positions.
       */
      template<class A, class B>
      constexpr inline std::ptrdiff_t LexCompareProductOperationsV =
        MPL::LexCompareV<SequenceCat<typename A::LeftIndexPositions, typename A::RightIndexPositions>,
                         SequenceCat<typename B::LeftIndexPositions, typename B::RightIndexPositions> >;

      /**@internal Compare operands according to their "left affinity"
       */
      template<class A, class B, class SFINAE = void>
      struct FactorSwap
      {
        static constexpr bool value = Einsum::leftAffinity<TreeExpression<typename A::TreeNode0> >() < Einsum::leftAffinity<TreeExpression<typename B::TreeNode0> >();
      };

      /**@internal Move contractions to the left.*/
      template<class A, class B>
      struct FactorSwap<A, B, std::enable_if_t<(!HasEinsumFunctorV<A> && HasEinsumFunctorV<B>)> >
        : TrueType
      {};

      /**@internal Compare according to the lexicographical ordering
       * of the contraction/multiplication indices.
       */
      template<class A, class B>
      struct FactorSwap<A, B, std::enable_if_t<(
        HasEinsumFunctorV<A> == HasEinsumFunctorV<B>
        && LexCompareProductOperationsV<typename A::FunctorType, typename B::FunctorType> > 0
        )> >
        : TrueType
      {};

      /**@internal In order to simplify factoring out we sort
       * everything s.t. operands with identical left-most factors
       * appear next to each other.
       */
      template<class Operands>
      using NonScalarCandidates =
        MPL::MergeSort<MPL::TypeTupleTransform<Operands, TreeFactorOutLeft>, FactorSwap>;

      /**@internal Match two equal left operands and functors.
       */
      template<class A, class B, class SFINAE = void>
      constexpr inline bool AreMatchingNonScalarsV = AreRuntimeEqual<TreeExpression<typename A::TreeNode0>, TreeExpression<typename B::TreeNode0> >::value;

      template<class T>
      constexpr inline bool AreMatchingNonScalarsV<void, T> = false;

      /**@internal Left-factors with non-matching functors cannot be
       * factored out.
       */
      template<class A, class B>
      constexpr inline bool AreMatchingNonScalarsV<
        A, B, std::enable_if_t<!std::is_same<typename A::FunctorType, typename B::FunctorType>::value> > = false;

      /**@internal Collect all operands with positive sign.*/
      template<class T, class PrevOps, class SFINAE = void>
      struct CollectOperands
      {
        using RightFactor = typename T::TreeNode1;
        using PlusOperands = MPL::AddFrontType<RightFactor, typename PrevOps::PlusOperands>;
        using MinusOperands = typename PrevOps::MinusOperands;
      };

      /**@internal Collect all operands with negative sign.*/
      template<class T, class PrevOps>
      struct CollectOperands<T, PrevOps, std::enable_if_t<!IsPositiveTreeNodeV<T> > >
      {
        using RightFactor = NegativeTreeNode<typename T::TreeNode1>;
        using PlusOperands = typename PrevOps::PlusOperands;
        using MinusOperands = MPL::AddFrontType<RightFactor, typename PrevOps::MinusOperands>;
      };

      template<>
      struct CollectOperands<void, void>
      {
        using PlusOperands = MPL::TypeTuple<>;
        using MinusOperands = MPL::TypeTuple<>;
      };

      /**@internal If we have operands with positive sign move them to
       * the left to avoid an unary minus operation.
       */
      template<class T0, class PlusOperands, class MinusOperands>
      struct FactorOutNonScalar
      {
        using Factorization = OperationPair<
          PlusFunctor, typename T0::FunctorType,
          typename T0::TreeNode0,
          MPL::TypeTupleCat<PlusOperands, MinusOperands>,
          SumUpTag>;
      };

      /**@internal If we do not have operands with positive sign w
       * shift the minus sign to the outside and make the inner
       * operands positive.
       */
      template<class T0, class MinusOperands>
      struct FactorOutNonScalar<T0, MPL::TypeTuple<>, MinusOperands>
      {
        using Factorization = OperationPair<
          MinusFunctor, typename T0::FunctorType,
          typename T0::TreeNode0,
          SetTreeNodeSign<PlusFunctor, MinusOperands>,
          SumUpTag>;
      };

      template<class T0, class Operands>
      using CreateNonScalarFactorization = typename FactorOutNonScalar<T0, typename Operands::PlusOperands, typename Operands::MinusOperands>::Factorization;

      ///////////////////////////

      template<class T, class Factor, class SFINAE = void>
      struct ProcessNonScalars;

      /**@internal Endpoint with no candidates left.*/
      template<class Factor>
      struct ProcessNonScalars<MPL::TypeTuple<>, Factor>
      {
        using Operands = CollectOperands<void, void>;
#if USE_SIGN_SWITCH
        using Factorizations = SignSwitch<void, void>;
#else
        using Factorizations = MPL::TypeTuple<>;
#endif
        using Remaining = MPL::TypeTuple<>;
      };

      /**@internal No-match one element end-point.*/
      template<class T0>
      struct ProcessNonScalars<MPL::TypeTuple<T0>, void>
      {
        using Operands = CollectOperands<void, void>;
#if USE_SIGN_SWITCH
        using Factorizations = SignSwitch<void, void>;
#else
        using Factorizations = MPL::TypeTuple<>;
#endif
        using Remaining = MPL::TypeTuple<CollapseOnes<T0> >;
      };

      /**@internal No match continue searching.*/
      template<class T0, class T1, class... T>
      struct ProcessNonScalars<
        MPL::TypeTuple<T0, T1, T...>,
        std::enable_if_t<!AreMatchingNonScalarsV<T0, T1> > >
      {
        using Recursion = ProcessNonScalars<MPL::TypeTuple<T1, T...>, void>;

        using Factorizations = typename Recursion::Factorizations;

        using Remaining = MPL::AddFrontType<CollapseOnes<T0>, typename Recursion::Remaining>;
      };

      /**@internal Found two equal factor, start with the
       * factorization.
       */
      template<class T0, class T1, class... T>
      struct ProcessNonScalars<
        MPL::TypeTuple<T0, T1, T...>, void,
        std::enable_if_t<AreMatchingNonScalarsV<T0, T1> > >
      {
        using Recursion = ProcessNonScalars<MPL::TypeTuple<T1, T...>, MPL::TypeWrapper<T0> >;
        using Operands = CollectOperands<T0, typename Recursion::Operands>;
        using Factorization = CreateNonScalarFactorization<T0, Operands>;
#if USE_SIGN_SWITCH
        using Factorizations = SignSwitch<Factorization, typename Recursion::Factorizations>;
#else
        using Factorizations = MPL::AddFrontType<Factorization, typename Recursion::Factorizations>;
#endif
        using Remaining = typename Recursion::Remaining;
      };

      /**@internal Continue collecting matching factors for the
       * current factorization.
       */
      template<class Factor, class T0, class... T>
      struct ProcessNonScalars<
        MPL::TypeTuple<T0, T...>, MPL::TypeWrapper<Factor>,
        std::enable_if_t<AreMatchingNonScalarsV<Factor, T0> > >
      {
        using Recursion = ProcessNonScalars<MPL::TypeTuple<T...>, MPL::TypeWrapper<Factor> >;
        using Operands = CollectOperands<T0, typename Recursion::Operands>;
        using Factorizations = typename Recursion::Factorizations;
        using Remaining = typename Recursion::Remaining;
      };

      /**@internal End of current factorization, start searching for a
       * new one.
       */
      template<class Factor, class T0, class... T>
      struct ProcessNonScalars<
        MPL::TypeTuple<T0, T...>, MPL::TypeWrapper<Factor>,
        std::enable_if_t<!AreMatchingNonScalarsV<Factor, T0> > >
      {
        using Recursion = ProcessNonScalars<MPL::TypeTuple<T0, T...>, void>;
        using Operands = CollectOperands<void, void>;
        using Factorizations = typename Recursion::Factorizations;
        using Remaining = typename Recursion::Remaining;
      };

      /////////////////////////

      /**@internal Setup and factorization work-horse:
       *
       * - split the list of sum-operands into "dummies" which do not
       *   have a left factor which qualifies for factorization (not
       *   "runtime equal"), operands with run-time-equal left scalar
       *   factor tuple (generated by the sum-explode code) and
       *   operands with runtime-equal left factor which is not a
       *   scalar.
       * - Process the scalar candidates and feed the left-overs from
       *   this "scalar factorization attempt" into the non-scalar
       *   candidates (a scalar factor may be composed of non-scalars
       *   by means of a contraction).
       * - Process the non-scalar candidates and feed the left-overs
       *   from the non-scalar factorization into the list of not
       *   (further) factorizable operands.
       */
      template<class... Todo>
      struct FactorOutHelper<MPL::TypeTuple<Todo...>, void, void>
      {
        using Scalars = ProcessScalars<MPL::TypeTupleCat<typename CandidateFilter<Todo>::ScalarCandidate...> >;
        using NonScalars = ProcessNonScalars<
          NonScalarCandidates<
            MPL::TypeTupleCat<
              ReassembleProduct<typename Scalars::Remaining>,
              typename CandidateFilter<Todo>::NonScalarCandidate...>
            >,
          void
          >;
        using Remaining = MPL::TypeTupleCat<typename NonScalars::Remaining, typename CandidateFilter<Todo>::DummyCandidate...>;

#if USE_SIGN_SWITCH
        using Type = typename FactorOutHelper<GetFactorizations<Scalars>, GetFactorizations<NonScalars>, Remaining>::Type;
#else
        using Type = typename FactorOutHelper<typename Scalars::Factorizations, typename NonScalars::Factorizations, Remaining>::Type;
#endif
      };

      /**Variant where we know that Todo does not contain scalar
       * factor tuples.
       */
      template<class... Todo>
      struct FactorOutHelper<void, MPL::TypeTuple<Todo...>, void>
      {
        using NonScalars = ProcessNonScalars<
          NonScalarCandidates<
            MPL::TypeTupleCat<
              typename CandidateFilter<Todo>::NonScalarCandidate...>
            >,
          void
          >;
        using Remaining = MPL::TypeTupleCat<typename NonScalars::Remaining, typename CandidateFilter<Todo>::DummyCandidate...>;

#if USE_SIGN_SWITCH
        using Type = typename FactorOutHelper<MPL::TypeTuple<>, GetFactorizations<NonScalars>, Remaining>::Type;
#else
        using Type = typename FactorOutHelper<MPL::TypeTuple<>, typename NonScalars::Factorizations, Remaining>::Type;
#endif
      };

      ////////////////////////////

      template<class Node>
      struct ScalarRecursion;

      template<class Node>
      struct NonScalarRecursion;

      /**@internal Try to factor out further factors for the given
       * list of scalar sum-operands.
       */
      template<class Sign, class F, class TreeData0, class... TreeData1, class Tag>
      struct ScalarRecursion<OperationPair<Sign, F, TreeData0, MPL::TypeTuple<TreeData1...>, Tag> >
      {
        using Type = OperationPair<
          Sign, F, TreeData0,
          CleanupRecursion<FactorOut<MPL::TypeTuple<TreeData1...>, void> >,
          Tag>;
        STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
      };

      /**@internal Try to factor out further factors for the given list of
       * sum-operands.
       */
      template<class Sign, class F, class TreeData0, class... TreeData1, class Tag>
      struct NonScalarRecursion<OperationPair<Sign, F, TreeData0, MPL::TypeTuple<TreeData1...>, Tag> >
      {
        using Type = OperationPair<
          Sign, F, TreeData0,
          CleanupRecursion<FactorOut<void, MPL::TypeTuple<TreeData1...> > >,
          Tag>;
        STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
      };

      ////////////////////////////

      /**@internal Recursion end-point at one level.
       *
       * @param Scalars Scalar factorizations which still may carry a
       * scalar factor tuple.
       *
       * @param Non-scalar factorizations which no longer carry a
       * scalar factor tuple.
       *
       * @parm Done Operands which cannot be factorized further.
       */
      template<class... Done, class... Scalars, class... NonScalars>
      struct FactorOutHelper<MPL::TypeTuple<Scalars...>, MPL::TypeTuple<NonScalars...>, MPL::TypeTuple<Done...> >
      {
        using Type = MPL::TypeTuple<typename ScalarRecursion<Scalars>::Type..., typename NonScalarRecursion<NonScalars>::Type..., Done...>;
        STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
      };

    } // Left::

    ///////////////////////////////////////////////

    template<class F, class T0, class T1, class SFINAE = void>
    constexpr inline bool ShouldFactorizeV =
      ComplexityV<FactorOut<MPL::TypeTupleCat<Explode<T0, TreePosition<0> >, MergeTreeNodeSign<F, Explode<T1, TreePosition<1> > > > > >
      <
      complexity<F, T0, T1>();

    template<class F, class T0, class T1>
    constexpr inline bool ShouldFactorizeV<
      F, T0, T1,
      std::enable_if_t<(!FunctorHas<IsPlusOrMinusOperation, F>::value
                        || !IsTensor<T0>::value || !IsTensor<T1>::value
        )> > = false;

    //static std::size_t explodeCount = 0;

#if DUNE_ACFEM_IS_GCC(9,10) || DUNE_ACFEM_IS_CLANG(0, 10)
    template<std::size_t Level, class F, class T0, class T1, class SFINAE = void>
    constexpr inline bool LevelShouldFactorizeV = ShouldFactorizeV<F, T0, T1>;

    template<std::size_t Level, class F, class T0, class T1>
    constexpr inline bool LevelShouldFactorizeV<Level, F, T0, T1, std::enable_if_t<(Level < OptimizeGap2::level_)> > = false;

    template<std::size_t Level, class F, class T0, class T1, std::enable_if_t<LevelShouldFactorizeV<Level, F, T0, T1>, int> = 0>
    constexpr auto operate(OptimizeTag<Level>, F&& f, T0&& t0, T1&& t1)
# if 0
    {}
# endif
#else
# if DUNE_ACFEM_IS_CLANG(0, 10)
#  warning This construct was known to fail for clang <= V10
# endif
# if DUNE_ACFEM_IS_GCC(9, 10)
#  warning This construct was known to fail for gcc >= V9 <= V10
# endif
    template<class F, class T0, class T1, std::enable_if_t<ShouldFactorizeV<F, T0, T1>, int> = 0>
    constexpr auto operate(OptimizeGap2, F&& f, T0&& t0, T1&& t1)
#endif
    {
      DUNE_ACFEM_RECORD_OPTIMIZATION;

      using Expl = MPL::TypeTupleCat<Explode<T0, TreePosition<0> >, MergeTreeNodeSign<F, Explode<T1, TreePosition<1> > > >;
      using Operands = FactorOut<Expl>;

      STATIC_ASSERT((!std::is_same<Operands, MPL::TypeTuple<> >::value), "");

#if DUNE_ACFEM_TRACE_OPTIMIZATION
      {
        std::clog << operationName(F{}/*std::forward<F>(f)*/, std::forward<T0>(t0).name(), std::forward<T1>(t1).name()) << std::endl;

        using Expl0 = Explode<T0, TreePosition<0> >;
        using Expl1 = Explode<T1, TreePosition<1> >;

        std::clog << typeString(Expl0{}) << std::endl;
        std::clog << typeString(MergeTreeNodeSign<F, Expl1>{}) << std::endl;
        std::clog << typeString(Operands{}) << std::endl;
        std::clog << typeString(MPL::TypeTuple<T0>{}, false) << std::endl;
        std::clog << typeString(MPL::TypeTuple<T1>{}, false) << std::endl;
      }
#endif

#if DUNE_ACFEM_TRACE_OPTIMIZATION
      {
        std::clog << "Complexity Improvement: " << (
          complexity<F, T0, T1>()
          -
          ComplexityV<FactorOut<MPL::TypeTupleCat<Explode<T0, TreePosition<0> >, MergeTreeNodeSign<F, Explode<T1, TreePosition<1> > > > > >) << std::endl;
      }
#endif

#if 0
      ++explodeCount;
      std::clog << "#" << explodeCount << std::endl;
      std::clog << "Explode:" << std::endl << typeString(Expl{}, false) << std::endl;
      std::clog << "Factors:" << std::endl << typeString(Operands{}, false) << std::endl;

      decltype(auto) result = sumUp(
        std::forward<T0>(t0), std::forward<T1>(t1), Operands{},
        OptimizeTop{} // OptimizeNext<OptimizeExpensive2>{}
        );

      std::clog << "#" << explodeCount << " EXIT" << std::endl;
      --explodeCount;
      return forwardReturnValue<decltype(result)>(result);
#else
      DUNE_ACFEM_EXPRESSION_RESULT(
        sumUp(
          std::forward<T0>(t0), std::forward<T1>(t1), Operands{},
          OptimizeTop{} // OptimizeNext<OptimizeExpensive2>{}
          )
        ,
        std::string("explode") + "t0: " + t0.name() + (std::is_same<PlusFunctor, F>::value ? " PLUS " : " MINUS ") + "t1: " + t1.name()
        );
#endif

    }

  } // Tensor::Optimization::Distributivity

  namespace Expressions {

    using Tensor::Optimization::Distributivity::operate;

  }

} // Dune::ACFem::

#endif // __DUNE_ACFEM_TENSORS_OPTIMIZATION_DISTRIBUTIVITY_HH__
