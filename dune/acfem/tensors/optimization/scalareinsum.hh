#ifndef __DUNE_ACFEM_TENSORS_OPTMIZATION_SCALAREINSUM_HH__
#define __DUNE_ACFEM_TENSORS_OPTMIZATION_SCALAREINSUM_HH__

#include "../../common/ostream.hh"
#include "../../mpl/foreach.hh"
#include "../../mpl/typetuplesort.hh"
#include "../../expressions/complexity.hh"
#include "../../expressions/operationpair.hh"
#include "../../expressions/treeextract.hh"

#include "../operationtraits.hh"
#include "einsum.hh"

namespace Dune {

  namespace ACFem {

    namespace Tensor::Optimization {

      namespace Einsum {

        using ScalarReduceTag = Expressions::OptimizeGap1;

        ///////////////////////////////////////////////////////

        /**@internal Identify einsums of scalar operands. These must
         * be exclude from elementary commutation patterns as this
         * would interfere with the associativity optimizations (the
         * weight of an expression is a sorting criterion and is
         * loosely related to the complexity of an expression).
         */
        template<class T, class SFINAE = void>
        constexpr inline bool IsEinsumOfScalarsV =
          (TensorTraits<Operand<0, T> >::rank + TensorTraits<Operand<1, T> >::rank == 0);

        template<class T>
        constexpr inline bool IsEinsumOfScalarsV<T, std::enable_if_t<!IsEinsumExpression<T>::value> > = false;

        template<class T, class SFINAE = void>
        constexpr inline bool IsPowerOfEinsumOfScalarsV = IsEinsumOfScalarsV<Operand<0, T> >;

        template<class T>
        constexpr inline bool IsPowerOfEinsumOfScalarsV<T, std::enable_if_t<!IsExponentiationExpression<T>::value> > = false;

        ///////////////////////////////////////////////////////

        /**@internal Define ordering of multiplications w.r.t. scalars.*/
        template<class T>
        constexpr inline std::size_t scalarLeftAffinity()
        {
          constexpr std::size_t byKind = ((std::size_t)(TensorTraits<T>::rank == 0)
                                          *
                                          ((std::size_t)(TensorTraits<T>::rank == 0) + constness<T>()));
          constexpr std::size_t byWeight = (~0UL >> 16) - Expressions::weight<T>() * (std::size_t)(TensorTraits<T>::rank == 0);

          return (byKind << 48) + byWeight;
        }

        template<class T>
        constexpr inline std::size_t scalarLeftAffinity(T&& t)
        {
          return scalarLeftAffinity<T>();
        }

        ///////////////////////////////////////////////////////

        /**Optimize t1*t2 to t2*t1 if requested by "policy" and if T0
         * or T1 is scalar. Do not commute the operands if both are
         * scalar and any is an einsum of scalars as this would
         * interfere with the associativity optmizations.
         */
        template<class Seq0, class Seq1, class Dims, class T0, class T1,
                 std::enable_if_t<(scalarLeftAffinity<T1>() > scalarLeftAffinity<T0>()
                                   && (TensorTraits<T0>::rank + TensorTraits<T1>::rank > 0
                                       ||
                                       (!IsEinsumOfScalarsV<T0>
                                        && !IsEinsumOfScalarsV<T1>
                                         )
                                     )
          ), int> = 0>
        constexpr decltype(auto) operate(CommuteTag, OperationTraits<EinsumOperation<Seq0, Seq1, Dims> >, T0&& t0, T1&& t1)
        {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          //std::clog << "einsum reorder" << std::endl;
          //std::clog << "  " << t0.name() << " * " << t1.name() << std::endl;
          DUNE_ACFEM_EXPRESSION_RESULT(
            (operate<EinsumOperation<Seq1, Seq0, Dims> >(std::forward<T1>(t1), std::forward<T0>(t0)))
            , "elementary scalar commute"
            );
        }

        ////////////////////////////////////////

        template<class TreeData0, class TreeData1, class OptimizeTag = OptimizeTop>
        using OperationPair = Expressions::OperationPair<IdentityFunctor, ScalarEinsumFunctor, TreeData0, TreeData1, OptimizeTag>;

        template<class E, class Pos>
        using TreeData = Expressions::TreeData<IdentityFunctor, E, Pos>;

        template<class A, class B>
        using Swap = BoolConstant<(scalarLeftAffinity<TreeExpression<A> >() < scalarLeftAffinity<TreeExpression<B> >())>;

        template<class A, class B>
        using Unequal = BoolConstant<!AreRuntimeEqual<TreeExpression<A>, TreeExpression<B> >::value>;

        template<class T, class Parent>
        using IsScalarEinsumOperand = BoolConstant<(IsEinsumOfScalarsV<Parent>
                                                    && !IsEinsumOfScalarsV<T>)>;

        template<class T, class TreePos, class Parent = void>
        using PackEinsumOperand = TreeData<T, TreePos>;

        template<class T>
        using PackLeftEinsumOperand = PackEinsumOperand<T, TreePosition<0> >;

        template<class T>
        using PackRightEinsumOperand = PackEinsumOperand<T, TreePosition<1> >;

        template<class T>
        using LeftScalarEinsumOperands = TreeExtract<IsScalarEinsumOperand, T, PackEinsumOperand, TreePosition<0> >;

        template<class T>
        using RightScalarEinsumOperands = TreeExtract<IsScalarEinsumOperand, T, PackEinsumOperand, TreePosition<1> >;

        //!@internal Read from T0
        template<class T0, class T1, class Arg, std::size_t I0, std::size_t... I>
        constexpr decltype(auto) getOperand(T0&& t0, T1&& t1, TreeData<Arg, Seq<I0, I...> >)
        {
          if constexpr (I0 == 0) {
            return treeOperand(std::forward<T0>(t0), Seq<I...>{});
          } else if constexpr (I0 == 1) {
            return treeOperand(std::forward<T1>(t1), Seq<I...>{});
          }
        }

        //!@internal Ordinary optimization pair
        template<
          class T0, class T1,
          class E0, std::size_t Pos00, std::size_t... Pos0,
          class E1, std::size_t Pos10, std::size_t... Pos1,
          class OptimizeTag>
        constexpr decltype(auto) getOperand(
          T0&& t0, T1&& t1,
          OperationPair<TreeData<E0, Seq<Pos00, Pos0...> >, TreeData<E1, Seq<Pos10, Pos1...> >, OptimizeTag>)
        {
          if constexpr (Pos00 == 0) {
            return operate(
              OptimizeTag{},
              ScalarEinsumFunctor{},
              treeOperand(std::forward<T0>(t0), Seq<Pos0...>{}),
              treeOperand(std::forward<T1>(t1), Seq<Pos1...>{})
              );
          } else {
            return operate(
              OptimizeTag{},
              ScalarEinsumFunctor{},
              treeOperand(std::forward<T1>(t1), Seq<Pos0...>{}),
              treeOperand(std::forward<T0>(t0), Seq<Pos1...>{})
              );
          }
        }

#if 0
        //!@internal Left-right-nested optimization pair. This should not happen.
        template<class T0, class T1,
                 class OptPair0, class OptPair1, class OptimizeTag>
        constexpr decltype(auto) getOperand(T0&& t0, T1&& t1, OperationPair<OptPair0, OptPair1, OptimizeTag>)
        {
          return operate(
            ScalarEinsumFunctor{},
            getOperand(std::forward<T0>(t0), std::forward<T1>(t1), OptPair0{}),
            getOperand(std::forward<T0>(t0), std::forward<T1>(t1), OptPair1{})
            );
        }
#endif

        //!@internal Left-nested optimization pair.
        template<class T0, class T1,
                 class OptPair0,
                 class E1, std::size_t Pos10, std::size_t... Pos1,
                 class OptimizeTag,
                 std::enable_if_t<IsOperationPairV<OptPair0>, int> = 0>
        constexpr decltype(auto) getOperand(
          T0&& t0, T1&& t1,
          OperationPair<OptPair0, TreeData<E1, Seq<Pos10, Pos1...> >, OptimizeTag>)
        {
          if constexpr (Pos10 == 0) {
            return operate(
              OptimizeTag{},
              ScalarEinsumFunctor{},
              getOperand(std::forward<T0>(t0), std::forward<T1>(t1), OptPair0{}),
              treeOperand(std::forward<T0>(t0), Seq<Pos1...>{})
              );
          } else {
            return operate(
              OptimizeTag{},
              ScalarEinsumFunctor{},
              getOperand(std::forward<T0>(t0), std::forward<T1>(t1), OptPair0{}),
              treeOperand(std::forward<T1>(t1), Seq<Pos1...>{})
              );
          }
        }

        //!@internal Right-nested optimization pair.
        template<class T0, class T1,
                 class E0, std::size_t Pos00, std::size_t... Pos0,
                 class OptPair1,
                 class OptimizeTag,
                 std::enable_if_t<IsOperationPairV<OptPair1>, int> = 0>
        constexpr decltype(auto) getOperand(
          T0&& t0, T1&& t1,
          OperationPair<TreeData<E0, Seq<Pos00, Pos0...> >, OptPair1, OptimizeTag>)
        {
          if constexpr (Pos00 == 0) {
            return operate(
              OptimizeTag{},
              ScalarEinsumFunctor{},
              treeOperand(std::forward<T0>(t0), Seq<Pos0...>{}),
              getOperand(std::forward<T0>(t0), std::forward<T1>(t1), OptPair1{})
              );
          } else {
            return operate(
              OptimizeTag{},
              ScalarEinsumFunctor{},
              treeOperand(std::forward<T1>(t1), Seq<Pos0...>{}),
              getOperand(std::forward<T0>(t0), std::forward<T1>(t1), OptPair1{})
              );
          }
        }

        template<class T0, class T1, class Arg0, class Arg1>
        constexpr auto scalarEinsumExpander(
          T0&& t0, T1&& t1,
          MPL::TypeTuple<Arg0, Arg1>
          )
        {
          return operate(
            DontOptimize{},
            ScalarEinsumFunctor{},
            getOperand(std::forward<T0>(t0), std::forward<T1>(t1), Arg0{}),
            getOperand(std::forward<T0>(t0), std::forward<T1>(t1), Arg1{})
            );
        }

        template<class T0, class T1, class Arg0, class Arg1, class Arg2, class... ArgRest>
        constexpr auto scalarEinsumExpander(
          T0&& t0, T1&& t1,
          MPL::TypeTuple<Arg0, Arg1, Arg2, ArgRest...>
          )
        {
          return operate(
            DontOptimize{},
            ScalarEinsumFunctor{},
            getOperand(std::forward<T0>(t0), std::forward<T1>(t1), Arg0{}),
            scalarEinsumExpander(
              std::forward<T0>(t0), std::forward<T1>(t1),
              MPL::TypeTuple<Arg1, Arg2, ArgRest...>{}
              )
            );
        }

        template<class T0, class T1, class Operands>
        constexpr decltype(auto) makeScalarEinsum(T0&& t0, T1&& t1, Operands)
        {
          //std::clog << typeString(operands) << std::endl;
          return
            Expressions::evaluate(
              //OptimizeNext<ScalarReduceTag>{},
              scalarEinsumExpander(
                std::forward<T0>(t0), std::forward<T1>(t1),
                Operands{}
                )
              );
        }

        template<class T, class Expr, class Pos>
        constexpr decltype(auto) makeScalarEinsum(T&& t, MPL::TypeTuple<TreeData<Expr, Pos> >)
        {
          return treeOperand(std::forward<T>(t), Pos{});
        }

        template<class T, class Expr, class Pos, class Node, class... NodeRest>
        constexpr decltype(auto) makeScalarEinsum(T&& t, MPL::TypeTuple<TreeData<Expr, Pos>, Node, NodeRest...>)
        {
          return
            operate(
              DontOptimize{},
              ScalarEinsumFunctor{},
              treeOperand(std::forward<T>(t), Pos{}),
              makeScalarEinsum(std::forward<T>(t), MPL::TypeTuple<NodeRest...>{})
              );
        }

        ////////////////////////////////////////

        struct ScalarEinsumTraits
        {
          template<class New, class Old>
          struct Operate
          {
            using Type = OperationPair<New, Old>;
            static constexpr std::size_t delta =
              complexity<ScalarEinsumFunctor, TreeExpression<New>, TreeExpression<Old> >() - ComplexityV<Type>;
          };
          constexpr static std::size_t threshold = 1;
        };

        template<class T, class SFINAE = void>
        struct PowerTraits
        {
          template<class New, class Old>
          struct Operate
          {
            using Type = Old;
            using Power = ExpressionType<DontOptimize, Functor<T>, TreeExpression<Old> >;
            using Expr = ExpressionType<ScalarEinsumFunctor, New, Power>;
            static constexpr std::size_t delta =
              complexity<ScalarEinsumFunctor, New, Power>()
              -
              complexity<Expr>();
          };
          constexpr static std::size_t threshold = 2;

          template<class... Operands, class Power>
          static constexpr decltype(auto) generatePower(Power&& p, MPL::TypeTuple<Operands...> operands = MPL::TypeTuple<Operands...>{})
          {
            return operate(
              DontOptimize{},
              Functor<Power>{},
              makeScalarEinsum(std::forward<Power>(p), operands)
              );
          }
        };

        template<class T>
        struct PowerTraits<T, std::enable_if_t<std::is_same<PowOperation, Operation<T> >::value> >
        {
          template<class New, class Old>
          struct Operate
          {
            using Type = Old;
            using Power = ExpressionType<DontOptimize, Functor<T>, TreeExpression<Old>, Operand<1, T> >;
            using Expr = ExpressionType<ScalarEinsumFunctor, New, Power>;
            static constexpr std::size_t delta =
              complexity<ScalarEinsumFunctor, New, Power>()
              -
              complexity<Expr>();
          };
          constexpr static std::size_t threshold = 2 + complexity<Operand<1, T> >();

          template<class... Operands, class Power>
          static constexpr decltype(auto) generatePower(Power&& p, MPL::TypeTuple<Operands...> operands = MPL::TypeTuple<Operands...>{})
          {
            return operate(
              DontOptimize{},
              Functor<T>{},
              makeScalarEinsum(std::forward<Power>(p), operands),
              std::forward<Power>(p).operand(1_c)
              );
          }
        };

        /**@internal Recursion end-point after testing all elements of
         * Tuple with a T. The operation pair with the least
         * complexity is added to the front of the tuple.
         */
        template<class TreeData, class OperationTraits, class OldOperation, class... Head, class Undo>
        constexpr decltype(auto) tryAddScalarEinsumOperand(MPL::TypeTuple<>, MPL::TypeTuple<Head...>, Undo)
        {
          return MPL::TypeTuple<typename OldOperation::Type, Head...>{};
        }

        template<class TreeData, class OperationTraits, class... Head>
        constexpr decltype(auto) tryAddScalarEinsumOperand(MPL::TypeTuple<>, MPL::TypeTuple<Head...>)
        {
          return MPL::TypeTuple<>{};
        }

        /**@internal Recursion work-horse after finding one
         * optimization, continue searching for a better one.
         */
        template<class TreeData, class Traits, class OldOperation, class Tail0, class... Tail, class... Head, class... UndoHead>
        constexpr decltype(auto) tryAddScalarEinsumOperand(
          MPL::TypeTuple<Tail0, Tail...>,
          MPL::TypeTuple<Head...>,
          MPL::TypeTuple<UndoHead...>)
        {
          using Operation = typename Traits::template Operate<TreeData, Tail0>;
          if constexpr (Operation::delta > OldOperation::delta) {
            // found an improvement
            return tryAddScalarEinsumOperand<TreeData, Traits, Operation>(
              MPL::TypeTuple<Tail...>{},
              MPL::TypeTuple<UndoHead...>{},
              MPL::TypeTuple<UndoHead..., Tail0>{}
              );
          } else {
            return tryAddScalarEinsumOperand<TreeData, Traits, OldOperation>(
              MPL::TypeTuple<Tail...>{},
              MPL::TypeTuple<Head... , Tail0>{},
              MPL::TypeTuple<UndoHead..., Tail0>{}
              );
          }
        }

        ///////////////////////////////////////

        /**@internal Start of the recursion:
         */
        template<class TreeData, class Traits, class Tail0, class... Tail, class... Head>
        constexpr decltype(auto) tryAddScalarEinsumOperand(MPL::TypeTuple<Tail0, Tail...>, MPL::TypeTuple<Head...>)
        {
          using Operation = typename Traits::template Operate<TreeData, Tail0>;
          if constexpr (Operation::delta >= Traits::threshold) {
            // found one optimization, continue searching
            return tryAddScalarEinsumOperand<TreeData, Traits, Operation>(
              MPL::TypeTuple<Tail...>{},
              MPL::TypeTuple<Head...>{},
              MPL::TypeTuple<Head..., Tail0>{} // undo tuple
              );
          } else {
            // no match branch
            return tryAddScalarEinsumOperand<TreeData, Traits>(
              MPL::TypeTuple<Tail...>{},
              MPL::TypeTuple<Head... , Tail0>{}
              );
          }
        }

        template<class TreeData, class Tuple, class OperationTraits = ScalarEinsumTraits>
        using TryAddScalarEinsumOperand = decltype(tryAddScalarEinsumOperand<TreeData, OperationTraits>(Tuple{}, MPL::TypeTuple<>{}));

        template<class TreeData, class Tuple, class OperationTraits = ScalarEinsumTraits>
        constexpr inline bool IsReducingScalarEinsumOperandV =
          TryAddScalarEinsumOperand<TreeData, Tuple, OperationTraits>::size() > 0;

        template<class LeftDone,
                 class RightProbeSet>
        constexpr decltype(auto) tryReduceScalarEinsumOperands(LeftDone, RightProbeSet, MPL::TypeTuple<>)
        {
          return MPL::TypePair<LeftDone, RightProbeSet>{};
        }

        template<class... LeftDone,
                 class RightProbeSet,
                 class LeftProbeSet0, class... LeftProbeSet>
        constexpr decltype(auto) tryReduceScalarEinsumOperands(
          MPL::TypeTuple<LeftDone...>, // unoptimizable operands of left probe-set
          RightProbeSet, // probe-set including already found optimization pairs
          MPL::TypeTuple<LeftProbeSet0, LeftProbeSet...> // to-be examined operands of left probe-set
          )
        {
          using TryReduce = TryAddScalarEinsumOperand<LeftProbeSet0, RightProbeSet>;

          if constexpr (TryReduce::size() == 0) {
            return tryReduceScalarEinsumOperands(
              MPL::TypeTuple<LeftDone..., LeftProbeSet0>{},
              RightProbeSet{},
              MPL::TypeTuple<LeftProbeSet...>{}
              );
          } else {
            return tryReduceScalarEinsumOperands(
              MPL::TypeTuple<LeftDone...>{},
              TryReduce{},
              MPL::TypeTuple<LeftProbeSet...>{}
              );
          }
        }

        template<class Tuple0, class Tuple1>
        using TryReduceScalarEinsumOperands =
          decltype(tryReduceScalarEinsumOperands(
                     MPL::TypeTuple<>{},
                     Tuple1{}, Tuple0{}
                     ));

        template<class Tuple0, class Tuple1>
        constexpr inline bool AreReducibleScalarEinsumOperandsV =
          TryReduceScalarEinsumOperands<Tuple0, Tuple1>::First::size() < Tuple0::size();

        ///////////////////////////////////////////////////////

        /**@internal Pattern:
         *
         * a * (b0 ..... bn)^s, complexity: \sum c(bk) + n + 1 + c(s) + 1 + c(a)
         *
         * If we select e.g. b0 and pair it with a then we arrive at
         *
         * (a * b0^s) *  (b1 ... bn)^s
         *
         * Complexity: (\sum1 c(bk) + (n-1) + 1 + c(s) + 1 + (c(a)+ c(b0) + c(s) + 2)
         *
         * Difference: 1 + c(s)
         *
         * So in terms of complexity we need a reduction >= 2 + c(s).
         */

        template<class T0, class T1, class SFINAE = void>
        constexpr inline bool IsReducibleScalarEinsumWithPowerV =
          IsReducingScalarEinsumOperandV<T1, LeftScalarEinsumOperands<Operand<0, T0> >, PowerTraits<T0> >;

        /**a^s * b^s is handled in the special powers-optimization
         * branch, so exclude it here.
         */
        template<class T0, class T1>
        constexpr inline bool IsReducibleScalarEinsumWithPowerV<
          T0, T1,
          std::enable_if_t<(IsExponentiationExpression<T0>::value
                            && IsExponentiationExpression<T1>::value
                            && AreRuntimeEqual<ExponentOfPower<T0>, ExponentOfPower<T1> >::value
          )> > = false;

        /**@internal Make sure we have a power of a scalar einsum on
         * the left and a factor on the right.
         */
        template<class T0, class T1>
        constexpr inline bool IsReducibleScalarEinsumWithPowerV<
          T0, T1,
          std::enable_if_t<(!IsExponentiationExpression<T0>::value
                            || !IsEinsumOfScalarsV<Operand<0, T0> >
                            || TensorTraits<T1>::rank > 0
                            || IsEinsumOfScalarsV<T1>
          )> > = false;

        template<class Seq0, class Seq1, class Dims, class T0, class T1,
                 std::enable_if_t<IsReducibleScalarEinsumWithPowerV<T0, T1>, int> = 0>
        constexpr decltype(auto) operate(ScalarReduceTag, OperationTraits<EinsumOperation<Seq0, Seq1, Dims> >, T0&& t0, T1&& t1)
        {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          using OperationTraits = PowerTraits<T0>;
          using Expressions = TryAddScalarEinsumOperand<T1, LeftScalarEinsumOperands<Operand<0, T0> >, OperationTraits>;

          DUNE_ACFEM_EXPRESSION_RESULT(
            operate(
              // DontOptimize{}, ???
              ScalarEinsumFunctor{},
              operate(
                ScalarEinsumFunctor{},
                std::forward<T1>(t1),
                OperationTraits::template generatePower<MPL::FrontType<Expressions> >(std::forward<T0>(t0))
                ),
              OperationTraits::generatePower(std::forward<T0>(t0), MPL::TailPart<Expressions>{})
              )
            , "reducible einsum with left power"
            );
        }

        template<class Seq0, class Seq1, class Dims, class T0, class T1,
                 std::enable_if_t<IsReducibleScalarEinsumWithPowerV<T1, T0>, int> = 0>
        constexpr decltype(auto) operate(ScalarReduceTag, OperationTraits<EinsumOperation<Seq0, Seq1, Dims> >, T0&& t0, T1&& t1)
        {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          using OperationTraits = PowerTraits<T1>;
          using Expressions = TryAddScalarEinsumOperand<T0, LeftScalarEinsumOperands<Operand<0, T1> >, OperationTraits>;

          DUNE_ACFEM_EXPRESSION_RESULT(
            operate(
              // DontOptimize{}, ???
              ScalarEinsumFunctor{},
              operate(
                ScalarEinsumFunctor{},
                std::forward<T0>(t0),
                OperationTraits::template generatePower<MPL::FrontType<Expressions> >(std::forward<T1>(t1))
                ),
              OperationTraits::generatePower(std::forward<T1>(t1), MPL::TailPart<Expressions>{})
              )
            , "reducible scalar einsum with right power"
            );
        }

        ///////////////////////////////////////////////////////

        template<class T0, class T1, class SFINAE = void>
        constexpr inline bool IsReducibleEinsumOfScalarEinsumsV =
          AreReducibleScalarEinsumOperandsV<LeftScalarEinsumOperands<T0>, RightScalarEinsumOperands<T1> >;

        template<class T0, class T1>
        constexpr inline bool IsReducibleEinsumOfScalarEinsumsV<
          T0, T1,
          std::enable_if_t<(!IsEinsumOfScalarsV<T0>
                            || !IsEinsumOfScalarsV<T1>
          )> > = false;

        /**Sort operands of scalar einsum-operations according to their weight.*/
        template<class Seq0, class Seq1, class Dims, class T0, class T1,
                 std::enable_if_t<IsReducibleEinsumOfScalarEinsumsV<T0, T1>, int> = 0>
        constexpr decltype(auto) operate(ScalarReduceTag, OperationTraits<EinsumOperation<Seq0, Seq1, Dims> >, T0&& t0, T1&& t1)
        {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          using ArgsPair = TryReduceScalarEinsumOperands<LeftScalarEinsumOperands<T0>, RightScalarEinsumOperands<T1> >;

          //std::clog << "red scl einsums " << std::endl;
          DUNE_ACFEM_EXPRESSION_RESULT(
            makeScalarEinsum(
              std::forward<T0>(t0),
              std::forward<T1>(t1),
              MPL::TypeTupleCat<typename ArgsPair::First, typename ArgsPair::Second>{}
              )
            , "reducible einsum of scalar einsums"
            );
        }


        //////////////////////////////////////////////////////////////////////

        template<class T0, class T1, class SFINAE = void>
        constexpr inline bool IsReducibleScalarEinsumWithScalarV =
          IsReducingScalarEinsumOperandV<PackLeftEinsumOperand<T1>, RightScalarEinsumOperands<T0> >;

        /**@internal Make sure we have a scalar einsum on the left and a factor on the right.*/
        template<class T0, class T1>
        constexpr inline bool IsReducibleScalarEinsumWithScalarV<
          T0, T1,
          std::enable_if_t<(!IsEinsumOfScalarsV<T0>
                            || TensorTraits<T1>::rank > 0
                            || IsEinsumOfScalarsV<T1>
          )> > = false;

        /**Sort operands of scalar einsum-operations according to their weight.*/
        template<class Seq0, class Seq1, class Dims, class T0, class T1,
                 std::enable_if_t<IsReducibleScalarEinsumWithScalarV<T0, T1>, int> = 0>
        constexpr decltype(auto) operate(ScalarReduceTag, OperationTraits<EinsumOperation<Seq0, Seq1, Dims> >, T0&& t0, T1&& t1)
        {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          DUNE_ACFEM_EXPRESSION_RESULT(
            makeScalarEinsum(
              std::forward<T1>(t1),
              std::forward<T0>(t0),
              TryAddScalarEinsumOperand<PackLeftEinsumOperand<T1>, RightScalarEinsumOperands<T0> >{}
              )
            , "t0: " + t0.name() + "; t1: " + t1.name() + "; reducible scalar einsum with right scalar"
            );
        }

        ///////////////////////////////////////////////////////

        /**Try to reduce each pair of factors..*/
        template<class Seq0, class Seq1, class Dims, class T0, class T1,
                 std::enable_if_t<IsReducibleScalarEinsumWithScalarV<T1, T0>, int> = 0>
        constexpr decltype(auto) operate(ScalarReduceTag, OperationTraits<EinsumOperation<Seq0, Seq1, Dims> >, T0&& t0, T1&& t1)
        {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          //std::clog << " scl * left factor: " << t0.name() << " * " << t1.name() << std::endl;
          //std::clog << "  op: " << typeString(TryAddScalarEinsumOperand<T0, RightScalarEinsumOperands<T1> >{}) << std::endl;
          DUNE_ACFEM_EXPRESSION_RESULT(
            makeScalarEinsum(
              std::forward<T0>(t0),
              std::forward<T1>(t1),
              TryAddScalarEinsumOperand<PackLeftEinsumOperand<T0>, RightScalarEinsumOperands<T1> >{}
              )
            , "reducible scalar einsum with left scalar"
            );
        }

        ////////////////////////////////////////////////////////////

      } // Einsum::

    } // Tensor::Optimization::

    namespace Expressions {

      using Tensor::Optimization::Einsum::operate;

    } // Expressions::

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_TENSORS_OPTMIZATION_SCALAREINSUM_HH__
