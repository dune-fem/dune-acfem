#ifndef __DUNE_ACFEM_TENSORS_OPTIMIZATION_ASSOCIATIVITY_HH__
#define __DUNE_ACFEM_TENSORS_OPTIMIZATION_ASSOCIATIVITY_HH__

#include "../../expressions/complexity.hh"
#include "../../expressions/reassemblesum.hh"
#include "../expressionoperations.hh"
#include "../operationtraits.hh"
#include "../operations/einsum.hh"
#include "../operations/product.hh"
#include "../operations/associativity.hh"

namespace Dune {

  namespace ACFem {

    namespace Tensor::Optimization {

      namespace Associativity {

        using AssociativityTag = Tensor::Policy::SubExpressionOptimizationTag;

        using AssRightTag = AssociativityTag;
        using AssLeftTag = OptimizeNext<AssRightTag>;
        using AssLeftFinalTag = OptimizeNext<AssLeftTag>;

        ///////////////////////////////////////////////////////////////

        template<class F, class T0, class T1, class SFINAE = void>
        struct RightAssociatedTypeHelper;

        template<class F, class T0, class T1>
        struct RightAssociatedTypeHelper<F, T0, T1, std::enable_if_t<IsRightAssociative<F, T0, T1>::value> >
        {
          using InnerOperation = AssociateRightOperation<1, F, T0, T1>;
          using Type = std::decay_t<decltype(
            operate(
              std::declval<InnerOperation>(), std::declval<Operand<1, T0> >(), std::declval<T1>()
              )
            )>;
        };

        template<class F, class T0, class T1>
        using RightAssociatedType = typename RightAssociatedTypeHelper<F, T0, T1>::Type;

        template<class F, class T0, class T1>
        constexpr std::size_t rightAssociatedComplexity()
        {
          return complexity<AssociateRightOperation<1, F, T0, T1>, Operand<1, T0>, T1>();
        }

        /**Check for (A*B)*C -> A*(B*C) */
        template<class F, class T0, class T1, class SFINAE = void>
        struct IsRightAssociativeProduct
          : BoolConstant<(// keep scalars grouped together.
                          !(TensorTraits<T0>::rank == 0 && TensorTraits<Operand<0, T0> >::rank > 0)
                          && !(TensorTraits<T0>::rank == 0 && TensorTraits<T1>::rank > 0)
                          // Here the negation of the respective
                          // conditions in IsLeftAssociativeProduct
                          // should be inserted in order to avoid
                          // ping-pong ("use of auto blah() before
                          // deduction of auto").
                          && ((complexity<RightAssociatedType<F, T0, T1> >()
                               <
                               rightAssociatedComplexity<F, T0, T1>())
                              || TensorTraits<T0>::rank > 0
                              || !(IsConstantExprArg<Operand<0, T0> >::value == IsConstantExprArg<T0>::value)
                              || !(IsRuntimeEqual<Operand<0, T0> >::value == IsRuntimeEqual<T0>::value)
                            )
            )>
        {};

        template<class F, class T0, class T1>
        struct IsRightAssociativeProduct<F, T0, T1, std::enable_if_t<!IsRightAssociative<F, T0, T1>::value> >
          : FalseType
        {};

        ///////////////////////////////////////////////////////////////

        /**@c TrueType is the operation can be optimized by
         * associating the double product left, i.e. by transforming a
         * right associated product A.(B.C) to a left associated
         * product (A.B).C.
         */
        template<class F, class T0, class T1, class SFINAE = void>
        struct LeftAssociatedTypeHelper;

        // Form the inner product
        template<class F, class T0, class T1>
        struct LeftAssociatedTypeHelper<F, T0, T1, std::enable_if_t<IsLeftAssociative<F, T0, T1>::value> >
        {
          using InnerOperation = AssociateLeftOperation<0, F, T0, T1>;
          using Type = std::decay_t<decltype(
            operate(
              std::declval<InnerOperation>(), std::declval<T0>(), std::declval<Operand<0, T1> >()
              )
            )>;
        };

        template<class F, class T0, class T1>
        using LeftAssociatedType = typename LeftAssociatedTypeHelper<F, T0, T1>::Type;

        template<class F, class T0, class T1>
        constexpr std::size_t leftAssociatedComplexity()
        {
          return complexity<AssociateLeftOperation<0, F, T0, T1>, T0, Operand<0, T1> >();
        }

        /**Check for A*(B*C) -> (A*B)*C */
        template<class F, class T0, class T1, class SFINAE = void>
        struct IsLeftAssociativeProduct
          : BoolConstant<(complexity<LeftAssociatedType<F, T0, T1> >()
                          <
                          leftAssociatedComplexity<F, T0, T1>())>
        {};

        //If the operation cannot be associated to the left, then it
        //cannot be optimized by this operation.
        template<class F, class T0, class T1>
        struct IsLeftAssociativeProduct<F, T0, T1, std::enable_if_t<!IsLeftAssociative<F, T0, T1>::value> >
          : FalseType
        {};

        /**Check for A*(B*C) -> (A*B)*C */
        template<class F, class T0, class T1, class SFINAE = void>
        struct IsLeftAssociativeProductFinal
          : BoolConstant<(TensorTraits<LeftAssociatedType<F, T0, T1> >::rank == 0
                          && (TensorTraits<Operand<1, T1> >::rank > 0
                              ||
                              ((IsConstantExprArg<T0>::value == IsConstantExprArg<LeftAssociatedType<F, T0, T1> >::value)
                               && (IsRuntimeEqual<T0>::value == IsRuntimeEqual<LeftAssociatedType<F, T0, T1> >::value)
                                )
                            )
            )>
        {};

        //If the operation cannot be associated to the left, then it
        //cannot be optimized by this operation.
        template<class F, class T0, class T1>
        struct IsLeftAssociativeProductFinal<F, T0, T1, std::enable_if_t<!IsLeftAssociative<F, T0, T1>::value> >
          : FalseType
        {};

        ///////////////////////////////////////////////////////////////

        /**Unconditionally associate all products to the right in
         * order to have a defined ordering.
         */
        template<class F, class T0, class T1,
                 std::enable_if_t<IsRightAssociativeProduct<F, T0, T1>::value, int> = 0>
        constexpr decltype(auto) operate(AssRightTag, F&& f, T0&& t0, T1&& t1)
        {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          //std::clog << "ass right" << std::endl;
          DUNE_ACFEM_EXPRESSION_RESULT(
            associateRight(std::forward<F>(f), std::forward<T0>(t0), std::forward<T1>(t1))
            , "Associate Right"
            );
        }

        /**Do local optimization by associating left. This is used to
         * merge constants.
         */
        template<class F, class T0, class T1,
                 std::enable_if_t<IsLeftAssociativeProduct<F, T0, T1>::value, int> = 0>
        constexpr decltype(auto) operate(AssLeftTag, F&& f, T0&& t0, T1&& t1)
        {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          //std::clog << "ass left" << std::endl;
          //std::clog << "  " << t0.name() << " * " << t1.name() << std::endl;
          DUNE_ACFEM_EXPRESSION_RESULT(
            associateLeft(std::forward<F>(f), std::forward<T0>(t0), std::forward<T1>(t1))
            , "Associate Left"
            );
        }

        template<class F, class T0, class T1,
                 std::enable_if_t<IsLeftAssociativeProductFinal<F, T0, T1>::value, int> = 0>
        constexpr decltype(auto) operate(AssLeftFinalTag, F&& f, T0&& t0, T1&& t1)
        {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          //std::clog << "ass left final" << std::endl;
          //std::clog << "  " << t0.name() << " * " << t1.name() << std::endl;

          DUNE_ACFEM_EXPRESSION_RESULT(
            associateLeft(std::forward<F>(f), std::forward<T0>(t0), std::forward<T1>(t1))
            , "Associate Left Final"
            );
        }

        ///////////////////////////////////////////////////////////////

      } // Associativity::

    } // Tensor::Optimization::

    namespace Expressions {

      using Tensor::Optimization::Associativity::operate;

    } // NS Expressions

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_TENSORS_OPTIMIZATION_ASSOCIATIVITY_HH__
