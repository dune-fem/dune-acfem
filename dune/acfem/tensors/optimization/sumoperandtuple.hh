#ifndef __DUNE_ACFEM_TENSOR_OPTIMIZATION_SUMOPERANDTUPLE_HH__
#define __DUNE_ACFEM_TENSOR_OPTIMIZATION_SUMOPERANDTUPLE_HH__

#include "../../expressions/operationpair.hh"
#include "../../expressions/optimizesums.hh"
#include "../modules/kronecker.hh"
#include "../modules/eye.hh"

namespace Dune::ACFem::Tensor::Optimization::Sums {

  using namespace Expressions;

  /**Hook into the innermost operand tuple of the factor out code. The
   * idea is that at this step the operands are available in a
   * type-tuple anyway so this should in principle a good place for
   * multi-operand optimization.
   */
  template<class Tuple, class SFINAE = void>
  struct ProcessSumOperands
  {
    using Type = Tuple;
  };

  /////////////////////////////////////////////////////////////////////////////

  /*
(delta<3,3@[2,2]> + (delta<3,3@[0,0]> + delta<3,3@[1,1]>)))
  */

  template<class Tuple, class SFINAE = void>
  constexpr inline bool IsCompleteSumOfKroneckersV = false;

  template<class T>
  constexpr inline bool IsKroneckerNodeV = false;

  template<class T, class Pos>
  constexpr inline bool IsKroneckerNodeV<TreeData<PlusFunctor, T, Pos> > =
    IsDiagonalKroneckerDelta<T>::value;

  /**@internal Optimize a "complete sum of (diagonal)
   * kroneckers" to an eye tensor.
   */
  template<class T0, class... T>
  constexpr bool isCompleteSumOfKroneckers(MPL::TypeTuple<T0, T...>)
  {
    constexpr std::size_t minDim = Head<typename TensorTraits<T0>::Signature>::value;
    // constexpr std::size_t minDim = min(TensorTraits<T0>::signature());
    if constexpr (minDim == (sizeof...(T)+1)) {
      return ((Head<PivotSequence<T0> >::value + ... + Head<PivotSequence<T> >::value)
              ==
              minDim * (minDim - 1) / 2);
    } else {
      return false;
    }
  }

  template<class T0, class... T>
  constexpr inline bool IsCompleteSumOfKroneckersV<
    MPL::TypeTuple<T0, T...>,
    std::enable_if_t<(IsKroneckerNodeV<T0> && ... && IsKroneckerNodeV<T>)>
    > = isCompleteSumOfKroneckers(MPL::TypeTuple<typename T0::Operand, typename T::Operand...>{});

  template<class Tuple>
  struct ProcessSumOperands<Tuple, std::enable_if_t<IsCompleteSumOfKroneckersV<Tuple> > >
  {
    using Signature = typename TensorTraits<typename MPL::FrontType<Tuple>::Operand>::Signature;
    using Type = MPL::TypeTuple<TreeData<PlusFunctor, Eye<Signature>, TreePosition<~0UL> > >;
  };


#if 1

  template<class In, class Out>
  struct OptimizeAll;

  template<class In0, class... In, class... Out>
  struct OptimizeAll<MPL::TypeTuple<In0, In...>, MPL::TypeTuple<Out...> >
  {
    using Try = Expressions::Sums::TryAddSumOperand<In0, MPL::TypeTuple<Out...> >;
    using Output = ConditionalType<(Try::size() > 0), Try, MPL::TypeTuple<Out..., In0> >;
    using Type = typename OptimizeAll<MPL::TypeTuple<In...>, Output>::Type;
  };

  template<class Out>
  struct OptimizeAll<MPL::TypeTuple<>, Out>
  {
    using Type = Out;
  };

  template<class In0, class In1, class... In>
  struct ProcessSumOperands<MPL::TypeTuple<In0, In1, In...>, std::enable_if_t<!IsCompleteSumOfKroneckersV<MPL::TypeTuple<In0, In1, In...> > > >
  {
    using Type = typename OptimizeAll<MPL::TypeTuple<In1, In...>, MPL::TypeTuple<In0> >::Type;
  };

#endif
}

#endif // __DUNE_ACFEM_TENSOR_OPTIMIZATION_SUMOPERANDTUPLE_HH__
