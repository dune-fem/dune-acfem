#ifndef __DUNE_ACFEM_TENSORS_OPTIMIZATION_CONSTANTS_HH__
#define __DUNE_ACFEM_TENSORS_OPTIMIZATION_CONSTANTS_HH__

#include "../../expressions/optimization.hh"
#include "../modules/constant.hh"
#include "policy.hh"

namespace Dune {

  namespace ACFem {

    namespace Tensor::Optimization {

      namespace Constants {

        using DefaultTag = Tensor::Policy::DefaultOptimizationTag;
        using ConstantsTag = OptimizeNext<DefaultTag>;

        template<class F, class T>
        using UnaryResult = std::decay_t<typename TensorTraits<ExpressionType<DontOptimize, F, T> >::ElementZero>;

        template<class F, class T, class SFINAE = void>
        constexpr inline bool IsUnaryConstantV = (
          false
          || !IsRuntimeEqual<T>::value
          || IsFractionConstant<UnaryResult<F, T> >::value
          );

        template<class F, class T>
        constexpr inline bool IsUnaryConstantV<
          F, T,
          std::enable_if_t<(!IsFunctor<F>::value
                            || !IsConstantExprArg<T>::value
                            || !IsConstantTensor<T>::value
          )> > = false;

        template<class F, class T, std::enable_if_t<IsUnaryConstantV<F, T>, int> = 0>
        constexpr auto operate(OptimizeTerminal1, F&& f, T&& t)
        {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          ///@@@OptimizeFinal Candidate
          using Traits = TensorTraits<ExpressionType<DontOptimize, F, T> >;
          DUNE_ACFEM_EXPRESSION_RESULT(
            constantTensor(std::move(operate(DontOptimize{}, f, std::forward<T>(t))(typename Traits::ZeroIndex{})),
                           Traits::signature(),
                           Disclosure{})
            , "unary operation on compile time constant"
            );
        }

        ////////////////////////

        template<class F, class T0, class T1>
        using BinaryResult = std::decay_t<typename TensorTraits<ExpressionType<DontOptimize, F, T0, T1> >::ElementZero>;

        template<class F, class T0, class T1, class SFINAE = void>
        constexpr inline bool IsBinaryConstantV = (!(IsRuntimeEqual<T0>::value || IsRuntimeEqual<T1>::value)
                                                   || IsFractionConstant<BinaryResult<F, T0, T1> >::value
          );

        template<class F, class T0, class T1>
        constexpr inline bool IsBinaryConstantV<
          F, T0, T1,
          std::enable_if_t<(!IsConstantExprArg<T0>::value
                            || !IsConstantExprArg<T1>::value
                            || !IsConstantTensor<T0>::value
                            || !IsConstantTensor<T1>::value
          )> > = false;

        template<class F, class T0, class T1, std::enable_if_t<IsBinaryConstantV<F, T0, T1>, int> = 0>
        constexpr auto operate(OptimizeTerminal0, F&& f, T0&& t0, T1&& t1)
        {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          ///@@@OptimizeFinal Candidate
          using Traits = TensorTraits<ExpressionType<DontOptimize, F, T0, T1> >;
          DUNE_ACFEM_EXPRESSION_RESULT(
            constantTensor(
              std::move(
                operate(
                  DontOptimize{},
                  f,
                  std::forward<T0>(t0),
                  std::forward<T1>(t1)
                  )(typename Traits::ZeroIndex{})),
              Traits::signature(),
              Disclosure{})
            , "binary operation on compile time constants"
            );
        }

      } // Constants::

    } // Tensor.:Optimization::

    namespace Expressions {

      using Tensor::Optimization::Constants::operate;

    } // NS Expressions

  } // ACFem

} // Dune

#endif // __DUNE_ACFEM_TENSORS_OPTIMIZATION_CONSTANTS_HH__
