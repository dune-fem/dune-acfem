#ifndef __DUNE_ACFFEM_TENSORS_OPTIMIZATION_EXPLODESUM_HH__
#define __DUNE_ACFFEM_TENSORS_OPTIMIZATION_EXPLODESUM_HH__

#include "../../expressions/explodesum.hh"
#include "../operations/reassociatetree.hh"
#include "scalareinsum.hh"

#ifdef STATIC_ASSERT
# undef STATIC_ASSERT
#endif
//#define STATIC_ASSERT(a, b) static_assert(a, b)
#define STATIC_ASSERT(a, b) /* nothing */

namespace Dune::ACFem::Tensor::Optimization::Sums
{
  //using ExplodeTag = Expressions::DontOptimize;
  using ExplodeTag = Expressions::OptimizeGeneric;

  using ProductOperations::OnesNode;
  using ProductOperations::IsOnesNodeV;

  template<class Sign, class F, class Arg0, class Arg1, class SFINAE = void>
  struct OperateHelper
  {
    using Type = OperationPair<Sign, F, Arg0, Arg1, ExplodeTag>;
    STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
  };

  /**@internal Collect all operands of nested scalar products in a
   * sorted MPL::TypeTuple in order to aid attempts to find an
   * "optimal" factorization.
   */
  template<class Sign, class F, class Arg0, class Arg1>
  using DoOperate = typename OperateHelper<Sign, F, Arg0, Arg1>::Type;

  /**@internal Combine the scalar "pre-factors" and compbine the
   * remaining two non-scalars @a E01 and E11 by the given non-scalar
   * product operation @a F.
   *
   * Signs: on entry all "inner" signs in E00, E01, E10, E11 are
   * positive, the remaining 3 signs are merged into the outer sign.
   */
  template<class Sign, class F, class ProdF, class Sign0, class... E00, class E01, class Sign1, class... E10, class E11>
  struct OperateHelper<Sign, F,
                       OperationPair<Sign0, ProdF, MPL::TypeTuple<E00...>, E01, ExplodeTag>,
                       OperationPair<Sign1, ProdF, MPL::TypeTuple<E10...>, E11, ExplodeTag> >
  {
    using Type = DoOperate<
      NestedSumFunctor<Sign, NestedSumFunctor<Sign0, Sign1> >,
      ProdF,
      MPL::TypeTupleMerge<MPL::TypeTuple<E00...>, MPL::TypeTuple<E10...>, Einsum::Swap>,
      DoOperate<PlusFunctor, F, E01, E11>
      >;
    STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
  };

  /**@internal Detect scalar factorizations.*/
  template<class TreeNode, class SFINAE = void>
  constexpr inline bool IsScalarFactorizationV = false;

  template<class Sign, class... Scalars, class Node1, class Tag>
  constexpr inline bool IsScalarFactorizationV<OperationPair<Sign, ScalarEinsumFunctor, MPL::TypeTuple<Scalars...>, Node1, Tag> > = true;

  /**@internal Detect scalars which aren't products of other
   * scalars. This is needed to filter out fancy
   * "pseudo-OperationPair" constructs where the left operand is an
   * MPL::TypeTuple and which do not have an Operation type-alias.
   */
  template<class TreeNode, class SFINAE = void>
  constexpr inline bool IsUndecomposableScalarV = !std::is_same<ScalarEinsumFunctor, Functor<TreeNode> >::value;


  //!@internal Not an undecomposable scalar if it is a "scalar einsum".
  template<class TreeNode>
  constexpr inline bool IsUndecomposableScalarV<
    TreeNode,
    std::enable_if_t<(TensorTraits<TreeExpression<TreeNode> >::rank > 0)>
      > = false;

  //!@internal Type-tuples can be operands...
  template<class... T>
  constexpr inline bool IsUndecomposableScalarV<MPL::TypeTuple<T...> > = false;

  /**@internal Add the given scalar @a TreeNode0 -- which is
   * "undecomposable", not a product of other scalars -- to the tuple
   * of scalar factors.
   *
   * Signs: on entry E10 and E11 have positive sign. TreeNode0 is unknown.
   */
  template<class Sign, class TreeNode0, class Sign1, class... E10, class E11>
  struct OperateHelper<
    Sign, ScalarEinsumFunctor,
    TreeNode0,
    OperationPair<Sign1, ScalarEinsumFunctor, MPL::TypeTuple<E10...>, E11, ExplodeTag>,
    std::enable_if_t<IsUndecomposableScalarV<TreeNode0> >
    >
  {
    using Sign0 = typename TreeNode0::Sign;
    using Type = OperationPair<
      NestedSumFunctor<Sign, NestedSumFunctor<Sign0, Sign1> >,
      ScalarEinsumFunctor,
      MPL::TypeTupleMergeOneLeft<PositiveTreeNode<TreeNode0>, MPL::TypeTuple<E10...>, Einsum::Swap>,
      E11,
      ExplodeTag
      >;
    STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
  };

  /**@internal Add the given scalar @a TreeNode1 to the tuple of scalar factors.
   *
   * Signs: TreeNode1 is unknown, rest positive or as specified.
   */
  template<class Sign, class Sign0, class... E00, class E01, class TreeNode1>
  struct OperateHelper<Sign, ScalarEinsumFunctor,
                       OperationPair<Sign0, ScalarEinsumFunctor, MPL::TypeTuple<E00...>, E01, ExplodeTag>,
                       TreeNode1,
                       std::enable_if_t<IsUndecomposableScalarV<TreeNode1> >
                       >
  {
    using Sign1 = typename TreeNode1::Sign;
    using Type = OperationPair<
      NestedSumFunctor<Sign, NestedSumFunctor<Sign0, Sign1> >,
      ScalarEinsumFunctor,
      MPL::TypeTupleMergeOneRight<MPL::TypeTuple<E00...>, PositiveTreeNode<TreeNode1>, Einsum::Swap>,
      E01, ExplodeTag
      >;
    STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
  };

  /**@internal Add the given scalar E10 to the list of scalars.
   */
  template<class Sign, class Sign0, class... E00, class E01, class Sign1, class E10, class E11>
  struct OperateHelper<Sign, ScalarEinsumFunctor,
                       OperationPair<Sign0, ScalarEinsumFunctor, MPL::TypeTuple<E00...>, E01, ExplodeTag>,
                       OperationPair<Sign1, ScalarEinsumFunctor, E10, E11, ExplodeTag>,
                       std::enable_if_t<IsUndecomposableScalarV<E10> >
                       >
  {
    using Type = DoOperate<
      NestedSumFunctor<Sign, NestedSumFunctor<Sign0, Sign1> >,
      ScalarEinsumFunctor,
      MPL::TypeTupleMergeOneRight<MPL::TypeTuple<E00...>, E10, Einsum::Swap>,
      DoOperate<PlusFunctor, ScalarEinsumFunctor, E01, E11>
      >;
    STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
  };

  ///////////////////////////////////

  /**@internal Combine a given list of scalar factors @a E0... with a
   * non-scalar @a E1
   */
  template<class Sign, class Sign0, class... E0, class TreeNode1>
  struct OperateHelper<Sign, ScalarEinsumFunctor,
                       OperationPair<Sign0, ScalarEinsumFunctor, MPL::TypeTuple<E0...>, OnesNode<>, ExplodeTag>,
                       TreeNode1,
                       std::enable_if_t<(TensorTraits<TreeExpression<TreeNode1> >::rank > 0)>
    >
  {
    STATIC_ASSERT(IsTensor<TreeExpression<TreeNode1>::value, "");

    using Sign1 = typename TreeNode1::Sign;
    using Type = OperationPair<
      NestedSumFunctor<NestedSumFunctor<Sign, Sign0>, Sign1>,
      ScalarEinsumFunctor,
      MPL::TypeTuple<E0...>,
      PositiveTreeNode<TreeNode1>,
      ExplodeTag
      >;
    STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
  };

  ///////////////////////////////////

  /**@internal Combine a given list of scalar factors @a E0... with a
   * non-scalar @a E1
   */
  template<class Sign, class TreeNode0, class Sign1, class... E1>
  struct OperateHelper<Sign, ScalarEinsumFunctor,
                       TreeNode0,
                       OperationPair<Sign1, ScalarEinsumFunctor, MPL::TypeTuple<E1...>, OnesNode<>, ExplodeTag>,
                       std::enable_if_t<(TensorTraits<TreeExpression<TreeNode0> >::rank > 0)>
    >
  {
    STATIC_ASSERT(IsTensor<TreeExpression<TreeNode0>::value, "");

    using Sign0 = typename TreeNode0::Sign;
    using Type = OperationPair<
      NestedSumFunctor<NestedSumFunctor<Sign, Sign0>, Sign1>,
      ScalarEinsumFunctor,
      MPL::TypeTuple<E1...>,
      PositiveTreeNode<TreeNode0>,
      ExplodeTag
      >;
    STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
  };

  ///////////////////////////////////

  template<class Sign, class ProdF, class Sign0, class... E00, class E01, class TreeNode1>
  struct OperateHelper<Sign, ProdF,
                       OperationPair<Sign0, ScalarEinsumFunctor, MPL::TypeTuple<E00...>, E01, ExplodeTag>,
                       TreeNode1,
                       std::enable_if_t<(TensorTraits<TreeExpression<TreeNode1> >::rank > 0)>
    >
  {
    STATIC_ASSERT(IsTensor<TreeExpression<TreeNode1> >::value, "");

    using Sign1 = typename TreeNode1::Sign;
    using Type = DoOperate<
      NestedSumFunctor<NestedSumFunctor<Sign, Sign0>, Sign1>,
      ScalarEinsumFunctor,
      OperationPair<PlusFunctor, ScalarEinsumFunctor, MPL::TypeTuple<E00...>, OnesNode<>, ExplodeTag>,
      DoOperate<PlusFunctor, ProdF, E01, PositiveTreeNode<TreeNode1> >
      >;
    STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
  };

  ///////////////////////////////////

  template<class Sign, class ProdF, class TreeNode0, class Sign1, class... E10, class E11>
  struct OperateHelper<Sign, ProdF,
                       TreeNode0,
                       OperationPair<Sign1, ScalarEinsumFunctor, MPL::TypeTuple<E10...>, E11, ExplodeTag>,
                       std::enable_if_t<(TensorTraits<TreeExpression<TreeNode0> >::rank > 0)>
    >
  {
    STATIC_ASSERT(IsTensor<TreeExpression<TreeNode0> >::value, "");

    using Sign0 = typename TreeNode0::Sign;
    using Type = DoOperate<
      NestedSumFunctor<NestedSumFunctor<Sign, Sign0>, Sign1>,
      ScalarEinsumFunctor,
      OperationPair<PlusFunctor, ScalarEinsumFunctor, MPL::TypeTuple<E10...>, OnesNode<>, ExplodeTag>,
      DoOperate<PlusFunctor, ProdF, PositiveTreeNode<TreeNode0>, E11>
      >;
    STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
  };

  ///////////////////////////////////

  /**!@internal Combine two scalars @a E0 and @a E1 by shifting them
   * into an MPL::TypeTuple. Add a scalar one as right factor.
   */
  template<class Sign, class F, class Node0, class Node1>
  struct OperateHelper<
    Sign, F, Node0, Node1,
    std::enable_if_t<(IsUndecomposableScalarV<Node0>
                      && IsUndecomposableScalarV<Node1>
                      && !IsOnesNodeV<Node0>
                      && !IsOnesNodeV<Node1>
    )>
                      >
  {
    using Sign0 = typename Node0::Sign;
    using Sign1 = typename Node1::Sign;
    using T0 = PositiveTreeNode<Node0>;
    using T1 = PositiveTreeNode<Node1>;
    using Type = OperationPair<
      NestedSumFunctor<Sign, NestedSumFunctor<Sign0, Sign1> >,
      ScalarEinsumFunctor,
      ConditionalType<
        Einsum::Swap<T0, T1>::value,
        MPL::TypeTuple<T1, T0>,
        MPL::TypeTuple<T0, T1>
        >,
      OnesNode<>,
      ExplodeTag
      >;
    STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
  };

  /**!@internal Combine two scalars @a E0 and @a E10 by shifting them
   * into an MPL::TypeTuple.
   */
  template<class Sign, class Node0, class Sign1, class E10, class E11>
  struct OperateHelper<
    Sign, ScalarEinsumFunctor,
    Node0,
    OperationPair<Sign1, ScalarEinsumFunctor, E10, E11, ExplodeTag>,
    std::enable_if_t<(IsUndecomposableScalarV<Node0>
                      && IsUndecomposableScalarV<E10>
    )>
    >
  {
    using Sign0 = typename Node0::Sign;
    using T0 = PositiveTreeNode<Node0>;
    using Type = OperationPair<
      NestedSumFunctor<Sign, Sign0>,
      ScalarEinsumFunctor,
      ConditionalType<
        Einsum::Swap<T0, E10>::value,
        MPL::TypeTuple<E10, T0>,
        MPL::TypeTuple<T0, E10>
        >,
      E11,
      ExplodeTag
      >;
    STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
  };

  //!@internal Shift a single scalar into a type-tuple
  template<class Sign, class Node0, class Node1>
  struct OperateHelper<
    Sign, ScalarEinsumFunctor, Node0, Node1,
    std::enable_if_t<(IsUndecomposableScalarV<Node0>
                      && !IsOnesNodeV<Node0>
                      && !IsUndecomposableScalarV<Node1>
                      && !IsScalarFactorizationV<Node1>
    )>
    >
  {
    using Sign0 = typename Node0::Sign;
    using Sign1 = typename Node1::Sign;
    using Type = OperationPair<
      NestedSumFunctor<Sign, NestedSumFunctor<Sign0, Sign1> >,
      ScalarEinsumFunctor,
      MPL::TypeTuple<PositiveTreeNode<Node0> >,
      PositiveTreeNode<Node1>,
      ExplodeTag
      >;
    STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
  };

  //!@internal Eliminate multiplication by "artificial" scalar ones,
  template<class Sign, class Node, class OnesSign>
  struct OperateHelper<Sign, ScalarEinsumFunctor, Node, OnesNode<OnesSign> >
  {
    STATIC_ASSERT(!MPL::IsTypeTupleV<Node>, "");
    using Type = MergeTreeNodeSign<NestedSumFunctor<Sign, OnesSign>, Node>;
    STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
  };

  //!Keep ones as placeholder to the right of tuples of scalar factors.
  template<class Sign, class... Node, class OnesSign>
  struct OperateHelper<Sign, ScalarEinsumFunctor, MPL::TypeTuple<Node...>, OnesNode<OnesSign> >
  {
    using Type = OperationPair<
      NestedSumFunctor<Sign, OnesSign>,
      ScalarEinsumFunctor,
      MPL::TypeTuple<Node...>,
      OnesNode<>,
      ExplodeTag>;
    STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
  };

  //!@internal Eliminate multiplication by "artificial" scalar ones.
  template<class Sign, class Node, class OnesSign>
  struct OperateHelper<Sign, ScalarEinsumFunctor, OnesNode<OnesSign>, Node>
  {
    STATIC_ASSERT(!MPL::IsTypeTupleV<Node>, "");
    using Type = MergeTreeNodeSign<NestedSumFunctor<Sign, OnesSign>, Node>;
    STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
  };

  template<class Sign, class LeftSign, class RightSign>
  struct OperateHelper<Sign, ScalarEinsumFunctor, OnesNode<LeftSign>, OnesNode<RightSign> >
  {
    using Type = OnesNode<NestedSumFunctor<Sign, NestedSumFunctor<LeftSign, RightSign> > >;
    STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
  };

  //////////////////////////

  //!@internal All-to-all explode of a product of sums.
  template<class F, class E, class TreePos, class Tuple0, class Tuple1>
  struct ExplodeProduct;

  template<class F, class E, class TreePos>
  struct ExplodeProduct<F, E, TreePos, MPL::TypeTuple<>, MPL::TypeTuple<> >
  {
    using Type = MPL::TypeTuple<>;
    STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
  };

  template<class F, class E, std::size_t... TreePos, class T10, class... T1Rest>
  struct ExplodeProduct<F, E, Expressions::TreePosition<TreePos...>, MPL::TypeTuple<>, MPL::TypeTuple<T10, T1Rest...> >
  {
    using Data0 = TreeData<PlusFunctor, Operand<0, E>, TreePosition<TreePos..., 0> >;
    using Type = MPL::TypeTuple<
      DoOperate<PlusFunctor, F, Data0, T10>,
      DoOperate<PlusFunctor, F, Data0, T1Rest>...
      >;
    STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
  };

  template<class F, class E, std::size_t... TreePos, class T00, class... T0Rest>
  struct ExplodeProduct<F, E, TreePosition<TreePos...>, MPL::TypeTuple<T00, T0Rest...>, MPL::TypeTuple<> >
  {
    using Data1 = TreeData<PlusFunctor, Operand<1, E>, TreePosition<TreePos..., 1> >;
    using Type = MPL::TypeTuple<
      DoOperate<PlusFunctor, F, T00, Data1>,
      DoOperate<PlusFunctor, F, T0Rest, Data1>...
      >;
    STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
  };

  template<class F, class E, class TreePos, class T00, class... T1>
  struct ExplodeProduct<F, E, TreePos, MPL::TypeTuple<T00>, MPL::TypeTuple<T1...> >
  {
    using Type = MPL::TypeTuple<DoOperate<PlusFunctor, F, T00, T1>...>;
    STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
  };

  template<class F, class E, class TreePos, class... T0, class... T1>
  struct ExplodeProduct<F, E, TreePos, MPL::TypeTuple<T0...>, MPL::TypeTuple<T1...> >
  {
    using Type = MPL::TypeTupleCat<
      typename ExplodeProduct<F, E, TreePos, MPL::TypeTuple<T0>, MPL::TypeTuple<T1...> >::Type...
      >;
    STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
  };

  /////////////////////////////////////////

  template<class Product>
  struct ReassembleProductHelper
  {
    using Type = Product;
    STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
  };

  /**@internal Reassemble the MPL::TypeTuple<> of scalar factor into
   * ordinary tree-nodes.
   */
  template<class Sign, class Left0, class Left1, class... Left, class Right, class Tag>
  struct ReassembleProductHelper<OperationPair<Sign, ScalarEinsumFunctor, MPL::TypeTuple<Left0, Left1, Left...>, Right, Tag> >
  {
    using Type = OperationPair<
      Sign,
      ScalarEinsumFunctor,
      Left0,
      typename ReassembleProductHelper<
        OperationPair<PlusFunctor, ScalarEinsumFunctor, MPL::TypeTuple<Left1, Left...>, Right, Tag>
        >::Type,
      Tag>;
    STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
  };

  /**@internal Recursion end-point for general right factors.*/
  template<class Sign, class Left, class Right, class Tag>
  struct ReassembleProductHelper<OperationPair<Sign, ScalarEinsumFunctor, MPL::TypeTuple<Left>, Right, Tag> >
  {
    using Type = OperationPair<Sign, ScalarEinsumFunctor, Left, Right, Tag>;
    STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
  };

  /**@internal Recursion end-point for artificial ones.*/
  template<class Sign, class Left, class Tag>
  struct ReassembleProductHelper<OperationPair<Sign, ScalarEinsumFunctor, MPL::TypeTuple<Left>, OnesNode<>, Tag> >
  {
    using Type = MergeTreeNodeSign<Sign, Left>;
    STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
  };

  template<class... T>
  struct ReassembleProductHelper<MPL::TypeTuple<T...> >
  {
    using Type = MPL::TypeTuple<typename ReassembleProductHelper<T>::Type...>;
  };

  /**@internal Reassemble @a Product given as
   * Expressions::OperationPair where the left operand may be an
   * MPL::TypeTuple of scalar left factors into an expression tree
   * composed of TreeData and OperationPair structures.
   */
  template<class Product>
  using ReassembleProduct = typename ReassembleProductHelper<Product>::Type;

  /////////////////////////////////////////

  /**@internal Post-process the tuples of sum-operands "returned" from
   * deeper recursion levels. This version in particular collects
   * scalar factors in a sorted MPL::TypeTuple in order to aid
   * factorization optimizations.
   */
  template<class F, class E, class...>
  struct ProcessExplodeHelper;

  //!@internal Tuple0 / Tuple1 represent "exploded" sum operands.
  template<class F, class E, class TreePos, class Tuple0, class Tuple1>
  struct ProcessExplodeHelper<F, E, TreePos, Tuple0, Tuple1>
  {
    static_assert(FunctorHas<IsProductOperation, F>::value, "");

    using Type = typename ExplodeProduct<F, E, TreePos, Tuple0, Tuple1>::Type;
    STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
  };

  template<class E, class TreePos, class T0, class... TRest>
  struct ProcessExplodeHelper<SquareFunctor, E, TreePos, MPL::TypeTuple<T0, TRest...> >
  {
    using Type = typename ExplodeProduct<typename SquareTraits<T0>::ExplodeFunctor, E, TreePos, MPL::TypeTuple<T0, TRest...>, MPL::TypeTuple<T0, TRest...> >::Type;
  };

  template<class E, class TreePos, class... T0, class... T1>
  struct ProcessExplodeHelper<PlusFunctor, E, TreePos, MPL::TypeTuple<T0...>, MPL::TypeTuple<T1...> >
  {
    using Type = MPL::TypeTuple<T0..., T1...>;
    STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
  };

  template<class E, class TreePos, class... T0, class... T1>
  struct ProcessExplodeHelper<MinusFunctor, E, TreePos, MPL::TypeTuple<T0...>, MPL::TypeTuple<T1...> >
  {
    using Type = MPL::TypeTuple<
      T0...,
      MergeTreeNodeSign<MinusFunctor, T1>...
      >;
    STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
  };

  template<class E, class TreePos, class... T0>
  struct ProcessExplodeHelper<MinusFunctor, E, TreePos, MPL::TypeTuple<T0...> >
  {
    using Type = MPL::TypeTuple<MergeTreeNodeSign<MinusFunctor, T0>...>;
    STATIC_ASSERT(sizeof(std::declval<Type>()) >= 0, "");
  };

  //!@internal Process the results found at lower level
  template<class E, class TreePos, class... T>
  using ProcessExplode = typename ProcessExplodeHelper<Functor<E>, E, TreePos, T...>::Type;

  /**@internal "Explode" @a expression by undoing all
   * factorizations. The resulting type is an MPL::TypeTuple which
   * stores the resulting sum-operands as Expressions::TreeData
   * resp. Expressions::OperationPair.
   */
  template<class Expr, class Pos = Expressions::TreePosition<> >
  using Explode = Expressions::Sums::Explode<Expr, Pos, ProcessExplode>;

} // namespace Dune::ACFem::Tensor::Optimization::Sum::

#endif //  __DUNE_ACFFEM_TENSORS_OPTIMIZATION_EXPLODESUM_HH__
