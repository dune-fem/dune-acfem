#ifndef __DUNE_ACFEM_TENSORS_OPTIMIZATION_RESHAPE_HH__
#define __DUNE_ACFEM_TENSORS_OPTIMIZATION_RESHAPE_HH__

#include "../expressionoperations.hh"
#include "../operations/reshape.hh"
#include "policy.hh"

namespace Dune {

  namespace ACFem {

    namespace Tensor::Optimization {

      namespace Reshape {

        /**@addtogroup Tensors
         * @{
         */

        /**@addtogroup TensorExpressions
         * @{
         */

        /**@addtogroup TensorExpressionOptimizations
         * @{
         */

        using DefaultTag = Policy::DefaultOptimizationTag;

        ////////////////////////////////////////////////////////////////

        /**@internal Reshaping a reshaped tensor to its original
         * signature yields the original tensor.
         */
        template<class ReshapeSignature, class T,
                 std::enable_if_t<std::is_same<ReshapeSignature, typename TensorTraits<T>::Signature>::value, int> = 0>
        constexpr decltype(auto) operate(OptimizeTerminal1, OperationTraits<ReshapeOperation<ReshapeSignature> >, T&& t)
        {
          ///@@@OptimizeFinal Candidate
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          DUNE_ACFEM_EXPRESSION_RESULT(
            std::forward<T>(t)
            , "undo reshape"
            );
        }

        //!@internal Collapse nested reshapes.
        template<class ReshapeSignature, class T,
                 std::enable_if_t<(ReshapeTraits<T>::Signature::size() >= 0), int> = 0>
        constexpr decltype(auto) operate(DefaultTag, OperationTraits<ReshapeOperation<ReshapeSignature> >, T&& t)
        {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          DUNE_ACFEM_EXPRESSION_RESULT(
            operate<ReshapeOperation<ReshapeSignature> >(std::forward<T>(t).operand(0_c))
            , "merge reshape");
        }

        ////////////////////////////////////////////////////////////////

        //!@} TensorExpressionOptimizations
        //!@} TensorExpressions
        //!@} Tensors

      } // Reshape::

    } // Tensor::Optimization::

    namespace Expressions {

      using Tensor::Optimization::Reshape::operate;

    } // Expressions::

  } // ACFem::

} // Dune::

#endif  // __DUNE_ACFEM_TENSORS_OPTIMIZATION_RESHAPE_HH__
