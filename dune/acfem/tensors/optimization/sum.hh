#ifndef __DUNE_ACFEM_TENSORS_OPTIMIZATION_SUM_HH__
#define __DUNE_ACFEM_TENSORS_OPTIMIZATION_SUM_HH__

#include "../../expressions/optimization.hh"
#include "../../expressions/treeextract.hh"
#include "../../expressions/examineutil.hh"
#include "../../mpl/compare.hh"
#include "../expressionoperations.hh"
#include "../operations/einsum.hh"
#include "../operationtraits.hh"
#include "../modules.hh"
#include "policy.hh"

namespace Dune {

  namespace ACFem {

    namespace Tensor::Optimization {

      namespace Sums {

        using SubExprTag = Policy::SubExpressionOptimizationTag;
        using DefaultTag = Policy::DefaultOptimizationTag;
        using CommuteTag = OptimizeNext<DefaultTag>;

        /**@addtogroup Tensors
         * @{
         */

        /**@addtogroup TensorExpressions
         * @{
         */

        /**@addtogroup TensorExpressionOptimizations
         * @{
         */

        /**@addtogroup TensorSumOptimizations
         *
         * Perform the following substitutions:
         *
         * + A + A to 2A, see IsSumOfIdentical
         * + OP(A + B) to OP(A) + OP(B) if simplifying, see IsUnaryOperationOnSum
         * + OP(A, B+C) to OP(A, B) + OP(A, C) if simplifying, see IsBinaryOperationOnRightSum
         * + OP(A+B, C) to OP(A, C) + OP(B, C) if simplifying, see IsBinaryOperationOnLeftSum
         *
         * @{
         */

        ///////////////////////////////////////////

        /**True for expressions which can be moved inside or outside of sums.*/
        template<class Operation>
        struct IsDistributiveOperation
          : BoolConstant<(IsTransposeOperation<Operation>::value
                          || IsRestrictionOperation<Operation>::value
                          || IsProductOperation<Operation>::value
                          //|| std::is_same<IdentityOperation, Operation>::value
                          //|| std::is_same<MinusOperation, Operation>::value
          )>
        {};

        //////////////////////////////////////////

        /**Optimize a+a to 2a.
         *
         * @note This should be obsolete and covered by the general factorization code.
         */
        template<class T0, class T1, std::enable_if_t<AreRuntimeEqual<T0, T1>::value, int> = 0>
        constexpr auto operate(OptimizeNext<DefaultTag>, PlusFunctor, T0&& t0, T1&& t1)
        {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          DUNE_ACFEM_EXPRESSION_RESULT(
            operate(ScalarEinsumFunctor{}, tensor(2_f), std::forward<T0>(t0))
            , "elementary distributivity"
            );
        }

        //////////////////////////////////////////

        template<class F, class T0, class T1>
        constexpr bool isSumOfMergableScalars()
        {
          if constexpr (!FunctorHas<IsPlusOrMinusOperation, F>::value) {
            return false;
          } else if constexpr (TensorTraits<T0>::rank != 0) {
            return false;
          } else if constexpr (IsConstantTensor<T0>::value) {
            return false;
          } else if constexpr (IsConstantTensor<T1>::value) {
            return false;
          } else if constexpr (IsConstantExprArg<T0>::value && IsConstantExprArg<T1>::value) {
            return true;
          } else {
            return false;
          }
        }

        template<class F, class T0, class T1,
                 std::enable_if_t<isSumOfMergableScalars<F, T0, T1>(), int> = 0>
        constexpr auto operate(OptimizeTerminal1, F&& f, T0&& t0, T1&& t1)
        {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          ///@@@OptimizeFinal Candidate
          DUNE_ACFEM_EXPRESSION_RESULT(
            constantTensor(f(t0(Seq<>{}), t1(Seq<>{})), Seq<>{}, Disclosure{})
            , "sum of mergable scalars"
            );
        }

        /////////////////////////////////////////////////////////////

        //!@} TensorSumumOptimizations
        //!@} TensorExpressionOptimizations
        //!@} TensorExpressions
        //!@} Tensors

      } // Sums::

    } // Tensor::Optimization::

    namespace Expressions {

      using Tensor::Optimization::Sums::operate;

    } // Expressions

  } // ACFem

} // Dune

#endif // __DUNE_ACFEM_TENSORS_OPTIMIZATION_SUM_HH__
