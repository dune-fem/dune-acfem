#ifndef __DUNE_ACFEM_TENSORS_OPTIMIZATION_PRODUCT_HH__
#define __DUNE_ACFEM_TENSORS_OPTIMIZATION_PRODUCT_HH__

#include "../../expressions/optimizegeneral.hh"
#include "../expressionoperations.hh"
#include "../operations/product.hh"
#include "../operationtraits.hh"
#include "../operations/associativity.hh"

namespace Dune {

  namespace ACFem {

    template<>
    constexpr inline bool MultiplicationAdmitsScalarsV<TensorProductOperation<Tensor::Seq<>, Tensor::Seq<>, Tensor::Seq<> > > = true;

    namespace Expressions {

      template<class Seq0, class Seq1, class Dims, class T0, class T1>
      constexpr inline bool ReturnSecondV<
        OperationTraits<TensorProductOperation<Seq0, Seq1, Dims> >,
        T0, T1, std::enable_if_t<(Dims::size() > 0)> > = (
          Tensor::IsOnes<T0>::value
          && TensorTraits<T0>::rank == Dims::size()
          && isSimple(Seq1{}));

      template<class Seq0, class Seq1, class Dims, class T0, class T1>
      constexpr inline bool ReturnFirstV<
        OperationTraits<TensorProductOperation<Seq0, Seq1, Dims> >,
        T0, T1, std::enable_if_t<(Dims::size() > 0)> > = (
          Tensor::IsOnes<T1>::value
          && !Tensor::IsOnes<T0>::value
          && TensorTraits<T1>::rank == Dims::size()
          && isSimple(Seq0{}));

    }

    namespace Tensor::Optimization::Products {

      using ProductTag = Tensor::Policy::DefaultOptimizationTag;
      using SubExprTag = Tensor::Policy::SubExpressionOptimizationTag;

      template<class F, class T0, class T1>
      constexpr inline bool EquivalentScalarEinsumV = false;

      template<class Seq0, class Seq1, class Dims, class T0, class T1>
      constexpr inline bool EquivalentScalarEinsumV<
        OperationTraits<TensorProductOperation<Seq0, Seq1, Dims> >,
        T0, T1> = (TensorTraits<T0>::rank * TensorTraits<T1>::rank == 0);

      template<class F, class T0, class T1, std::enable_if_t<EquivalentScalarEinsumV<F, T0, T1>, int> = 0>
      constexpr decltype(auto) operate(ProductTag, F&&, T0&& t0, T1&& t1)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;
        DUNE_ACFEM_EXPRESSION_RESULT(
          operate<ScalarEinsumOperation>(std::forward<T0>(t0), std::forward<T1>(t1))
          , "product -> einsum"
          );
      }

      //////////////////////////////////

      template<class Seq0, class Seq1, class Dims, class T0, class T1,
               std::enable_if_t<(true
                                 && IsEye<T0>::value
                                 && IsEye<T1>::value
                                 // eyes have to overlap in order to chain them
                                 && Dims::size() > 0
        ), int> = 0>
      constexpr auto operate(OptimizeNext<ProductTag>, OperationTraits<TensorProductOperation<Seq0, Seq1, Dims> > f, T0&& t0, T1&& t1) noexcept
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        using Signature = ProductSignature<T0, Seq0, T1, Seq1>;
        DUNE_ACFEM_EXPRESSION_RESULT(
          Eye<Signature>{}
          , "merge eye product"
          );
      }

      template<class Seq0, class Seq1, class Dims, class T0, class T1,
               std::enable_if_t<(true
                                 && IsBlockEye<T0>::value
                                 && IsBlockEye<T1>::value
                                 // eyes have to overlap in order to chain them
                                 && Dims::size() > 0
                                 // building block for tensor signature has to be the same
                                 && typename BlockEyeTraits<T0>::BlockSignature{}
                                 ==
                                 typename BlockEyeTraits<T1>::BlockSignature{}
                                 // product indices must be "sub-ordinate" to block eye structure
                                 && IsBlockEyePositionsV<Seq0, BlockEyeTraits<T0>::blockRank_, typename BlockEyeTraits<T0>::BlockSignature>
                                 && IsBlockEyePositionsV<Seq1, BlockEyeTraits<T1>::blockRank_, typename BlockEyeTraits<T1>::BlockSignature>
        ), int> = 0>
      constexpr auto operate(OptimizeTerminal0, OperationTraits<TensorProductOperation<Seq0, Seq1, Dims> > f, T0&& t0, T1&& t1) noexcept
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        // block signatures are the same for both operands
        using BlockSignature = typename BlockEyeTraits<T0>::BlockSignature;

        constexpr std::size_t blockSize = BlockSignature::size();
        constexpr std::size_t prodRank = Dims::size() / blockSize;
        constexpr std::size_t blockRank =
        BlockEyeTraits<T0>::blockRank_
        +
        BlockEyeTraits<T1>::blockRank_
        -
        prodRank;

        ///@@@OptimizeFinal Candidate
        DUNE_ACFEM_EXPRESSION_RESULT(
          (BlockEye<blockRank, BlockSignature>{})
          , "merge block eye"
          );
      }

      ////////////////////////////////////////

      template<class Seq0, class Seq1, class Dims, class T0, class T1,
               std::enable_if_t<(IsTensorProductExpression<T0>::value
                                 //
                                 // As eye .[i][j] eye is again an eye
                                 // there should not be any compatibility
                                 // problems with the index ordering of the
                                 // first operatnd if the expression was
                                 // not ill-formed
                                 //
                                 && IsEye<Operand<1, T0> >::value
                                 && IsEye<T1>::value
                                 // eyes have to overlap in order to chain them
                                 && (Dims::size() > 0)
        ), int> = 0>
      constexpr auto operate(OptimizeNext<ProductTag>, OperationTraits<TensorProductOperation<Seq0, Seq1, Dims> > f, T0&& t0, T1&& t1)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        DUNE_ACFEM_EXPRESSION_RESULT(
          operate<Operation<T0> >(
            std::forward<T0>(t0).operand(0_c),
            operate<TensorProductOperation<Seq0, Seq1, Dims> >(
              std::forward<T0>(t0).operand(1_c),
              std::forward<T1>(t1)
              )
            )
          , "obsolete associativity pattern"
          );
      }

    } // Tensor::Optimization::Products::

    namespace Expressions {

      using Tensor::Optimization::Products::operate;

    } // NS Expressions

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_TENSORS_OPTIMIZATION_PRODUCT_HH__
