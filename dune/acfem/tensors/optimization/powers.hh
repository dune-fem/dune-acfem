#ifndef __DUNE_ACFEM_TENSORS_OPTIMIZATION_POWERS_HH__
#define __DUNE_ACFEM_TENSORS_OPTIMIZATION_POWERS_HH__

#include "../../expressions/optimizationbase.hh"
#include "../../expressions/runtimeequal.hh"
#include "../expressionoperations.hh"
#include "../operationtraits.hh"
#include "../modules.hh"
#include "einsum.hh"
#include "policy.hh"

namespace Dune {

  namespace ACFem {

    namespace Tensor::Optimization {

      namespace Powers {

        using SubExprTag = Policy::SubExpressionOptimizationTag;
        using DefaultTag = Policy::DefaultOptimizationTag;
        using PowTag = OptimizeNext<DefaultTag>;

        /**@addtogroup Tensors
         * @{
         */

        /**@addtogroup TensorExpressions
         * @{
         */

        /**@addtogroup TensorExpressionOptimizations
         * @{
         */

        /**@addtogroup TensorPowerOptimizations
         *
         * Perform the following substitutions:
         *
         * + s1 * s1 to s1^2
         *   is decltype(s1) qualifies for runtime-equal.
         * + 1/s1 * 1/s2 to 1/(s1 * s2)
         * + s*s^alpha to s^(alhpa+1)
         * + s*(s*t)^alpha to s^(alpha+1)*t^alpha
         * + s^alpha*s^beta to s^(alhpa+beta)
         * + s1^alpha * s2^alpha to (s1*s2)^alpha
         * + (s^alpha)^beta to s^(alpha*beta)
         * + s^1 to s
         * + s^{-1} to 1/s
         * + s^{1/2} to sqrt(s)
         * + s^2 to sqr(s)
         * + s^0 to 1
         *
         * @{
         */

        ////////////////////////////////////////////////////////////////

        /**@internal Generate a proper exponent for componentwise pow operation.*/
        template<class T>
        constexpr decltype(auto) powExponent(T&& t)
        {
          if constexpr (std::is_same<Operation<T>, PowOperation>::value) {
            return forwardReturnValue<T>(t).operand(1_c);
          } else if constexpr (TensorTraits<T>::rank > 0) {
            return operate(
              ScalarEinsumFunctor{},
              tensor(exponent<Operation<T> >()),
              ones<T, Disclosure>()
              );
          } else {
            return tensor(exponent<Operation<T> >());
          }
        }

        ////////////////////////////////////////////////////////////////

        template<class F, class T0, class T1>
        constexpr inline bool IsScalarEinsumSquareV = false;

        template<class T0, class T1>
        constexpr inline bool IsScalarEinsumSquareV<ScalarEinsumFunctor, T0, T1> =
          (AreRuntimeEqual<T0, T1>::value
           && !IsReciprocalExpression<T0>::value
           // identify scalars
           && TensorTraits<T0>::rank == 0);


        /**Change (s1*s1) to square(s1).*/
        template<class F, class T0, class T1, std::enable_if_t<IsScalarEinsumSquareV<F, T0, T1>, int> = 0>
        constexpr decltype(auto) operate(DefaultTag, F&&, T0&& t0, T1&& t1)
        {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          DUNE_ACFEM_EXPRESSION_RESULT(
            operate(OperationTraits<SquareOperation>{}, std::forward<T0>(t0))
            , "einsum square power"
            );
        }

        ////////////////////////////////////////////////////////////////

        template<class F, class T0, class T1>
        constexpr inline bool IsScalarEinsumOfReciprocalsV = false;

        template<class T0, class T1>
        constexpr inline bool IsScalarEinsumOfReciprocalsV<ScalarEinsumFunctor, T0, T1> =
          (IsReciprocalExpression<T0>::value
           && IsReciprocalExpression<T1>::value);

        /**@internal Change 1/s1 * 1/s2 to 1/(s1 * s2).*/
        template<class F, class T0, class T1, std::enable_if_t<IsScalarEinsumOfReciprocalsV<F, T0, T1>, int> = 0>
        constexpr decltype(auto) operate(DefaultTag, F&& f, T0&& t0, T1&& t1)
        {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          DUNE_ACFEM_EXPRESSION_RESULT(
            operate(
              OperationTraits<ReciprocalOperation>{},
              operate(
                std::forward<F>(f),
                std::forward<T0>(t0).operand(0_c),
                std::forward<T1>(t1).operand(0_c))
              )
            , "reciprocal square power"
            );
        }

        ////////////////////////////////////////////////////////////////

        template<class F, class T0, class T1>
        constexpr inline bool IsTensorProductSquareV = false;

        template<class Seq0, class Seq1, class Dims, class T0, class T1>
        constexpr inline bool IsTensorProductSquareV<
          OperationTraits<TensorProductOperation<Seq0, Seq1, Dims> >, T0, T1> =
          (AreRuntimeEqual<T0, T1>::value
           // identify non-scalars
           && TensorTraits<T0>::rank > 0
           // identify square, i.e. product over full signature.
           && Dims::size() == TensorTraits<T0>::rank);

        /**Change (s1*s1) to square(s1).*/
        template<class F, class T0, class T1, std::enable_if_t<IsTensorProductSquareV<F, T0, T1>, int> = 0>
        constexpr decltype(auto) operate(DefaultTag, F&&, T0&& t0, T1&& t1)
        {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          return operate(OperationTraits<SquareOperation>{}, std::forward<T0>(t0));
        }

        ////////////////////////////////////////////////////////////////

        template<class F, class T0, class T1>
        constexpr inline bool IsScalarEinsumWithPowerV = false;

        template<class T0, class T1>
        constexpr inline bool IsScalarEinsumWithPowerV<ScalarEinsumFunctor, T0, T1> =
          (IsExponentiationExpression<T0>::value
           && AreRuntimeEqual<Operand<0, T0>, T1>::value);

        /**Change s^alpha*s to s^(alhpa+1).*/
        template<class F, class T0, class T1, std::enable_if_t<IsScalarEinsumWithPowerV<F, T0, T1>, int> = 0>
        constexpr decltype(auto) operate(DefaultTag, F&&, T0&& t0, T1&& t1)
        {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          DUNE_ACFEM_EXPRESSION_RESULT(
            operate(
              OperationTraits<PowOperation>{},
              std::forward<T0>(t0).operand(0_c),
              operate(
                PlusFunctor{},
                ones<T0, Disclosure>(),
                powExponent(std::forward<T0>(t0))
                )
              )
            , "power times factor"
            );
        }

        /**Change s*s^alpha to s^(alhpa+1).*/
        template<class F, class T0, class T1, std::enable_if_t<IsScalarEinsumWithPowerV<F, T1, T0>, int> = 0>
        constexpr decltype(auto) operate(DefaultTag, F&& f, T0&& t0, T1&& t1)
        {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          DUNE_ACFEM_EXPRESSION_RESULT(
            operate(
              OperationTraits<PowOperation>{},
              std::forward<T1>(t1).operand(0_c),
              operate(
                PlusFunctor{},
                ones<T1, Disclosure>(),
                powExponent(std::forward<T1>(t1)))
              )
            , "factor x power"
            );
        }

        ////////////////////////////////////////////////////////////////

        template<class F, class T0, class T1, class SFINAE = void>
        constexpr inline bool IsScalarEinsumOfPowersV = false;

        template<class T0, class T1>
        constexpr inline bool IsScalarEinsumOfPowersV<
          ScalarEinsumFunctor, T0, T1,
          std::enable_if_t<(IsExponentiationExpression<T0>::value
                            && IsExponentiationExpression<T1>::value
          )> > =
          (AreRuntimeEqual<Operand<0, T0>, Operand<0, T1> >::value
           && !AreRuntimeEqual<ExponentOfPower<T0>, ExponentOfPower<T1> >::value);

        /**@internal Change s^alpha * s^beta to s^(alpha+beta).*/
        template<class F, class T0, class T1, std::enable_if_t<IsScalarEinsumOfPowersV<F, T0, T1>, int> = 0>
        constexpr decltype(auto) operate(DefaultTag, F&&, T0&& t0, T1&& t1)
        {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          DUNE_ACFEM_EXPRESSION_RESULT(
            operate(
              OperationTraits<PowOperation>{},
              std::forward<T0>(t0).operand(0_c),
              operate(
                PlusFunctor{},
                tensor(exponent(std::forward<T0>(t0))),
                tensor(exponent(std::forward<T1>(t1)))
                )
              )
            , "power x power"
            );
        }

        ////////////////////////////////////////////////////////////////

        template<class F, class T0, class T1, class SFINAE = void>
        constexpr inline bool IsScalarEinsumWithSameExponentV = false;

        template<class T0, class T1>
        constexpr inline bool IsScalarEinsumWithSameExponentV<
          ScalarEinsumFunctor, T0, T1,
          std::enable_if_t<(IsExponentiationExpression<T0>::value
                            && IsExponentiationExpression<T1>::value
          )> > =
          AreRuntimeEqual<ExponentOfPower<T0>, ExponentOfPower<T1> >::value;

        /**@internal Change s1^alpha * s2^alpha to (s1*s2)^alpha.*/
        template<class F, class T0, class T1, std::enable_if_t<IsScalarEinsumWithSameExponentV<F, T0, T1>, int> = 0>
        constexpr decltype(auto) operate(PowTag, F&&, T0&& t0, T1&& t1)
        {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          DUNE_ACFEM_EXPRESSION_RESULT(
            operate(
              OperationTraits<PowOperation>{},
              operate(
                ScalarEinsumFunctor{},
                std::forward<T0>(t0).operand(0_c),
                std::forward<T1>(t1).operand(0_c)
                ),
              tensor(exponent(std::forward<T0>(t0)))
              )
            , "power product same exponent"
            );
        }

        ////////////////////////////////////////////////////////////////

        /**Change (s^alpha)^beta to s^(alpha*beta).*/
        template<class F, class T,
                 std::enable_if_t<(FunctorHas<IsExponentiationOperation, F>::value
                                   && IsExponentiationExpression<T>::value
                   ), int> = 0>
        constexpr auto operate(DefaultTag, F&& f, T&& t)
        {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          DUNE_ACFEM_EXPRESSION_RESULT(
            operate(
              OperationTraits<PowOperation>{},
              std::forward<T>(t).operand(0_c),
              operate(
                ScalarEinsumFunctor{},
                tensor(exponent<F>()),
                powExponent(std::forward<T>(t))
                )
              )
            , "nested power"
            );
        }

        ////////////////////////////////////////////////////////////////

        template<class F, class T0, class T1>
        constexpr inline bool IsPowOfPowerV = false;

        template<class T0, class T1>
        constexpr inline bool IsPowOfPowerV<OperationTraits<PowOperation>, T0, T1> =
          IsExponentiationExpression<T0>::value;

        /**Change (s^alpha)^beta to s^(alpha*beta).*/
        template<class F, class T0, class T1, std::enable_if_t<IsPowOfPowerV<F, T0, T1>, int> = 0>
        constexpr auto operate(DefaultTag, F&&, T0&& t0, T1&& t1)
        {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          DUNE_ACFEM_EXPRESSION_RESULT(
            operate(
              OperationTraits<PowOperation>{},
              std::forward<T0>(t0).operand(0_c),
              operate(
                OperationTraits<TotalProductOperation<T0> >{},
                powExponent(std::forward<T0>(t0)),
                std::forward<T1>(t1)
                )
              )
            , "nested pow"
            );
        }

        ////////////////////////////////////////////////////////////////

        template<class F, class T0, class T1>
        constexpr inline bool IsPowOfOnesV = false;

        template<class T0, class T1>
        constexpr inline bool IsPowOfOnesV<OperationTraits<PowOperation>, T0, T1> =
          IsConstantTensorV<T0, IntFraction<1> >;

        /**@internal Change 1^s to 1.*/
        template<class F, class T0, class T1, std::enable_if_t<IsPowOfOnesV<F, T0, T1>, int> = 0>
        constexpr decltype(auto) operate(OptimizeTerminal1, F&&, T0&& t0, T1&& t1)
        {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          DUNE_ACFEM_EXPRESSION_RESULT(
            forwardReturnValue<T0>(t0)
            , "pow of one"
            );
        }

        /**@internal Change 1^s to 1.*/
        template<
          class F, class T,
          std::enable_if_t<(IsConstantTensorV<T, IntFraction<1> >
                            && FunctorHas<IsExponentiationOperation, F>::value
          ), int> = 0>
        constexpr decltype(auto) operate(OptimizeTerminal1, F&&, T&& t)
        {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          DUNE_ACFEM_EXPRESSION_RESULT(
            forwardReturnValue<T>(t)
            , "power of one"
            );
        }

        ////////////////////////////////////////////////////////////////

        template<class F, class T0, class T1>
        constexpr inline bool IsPowToOnesV = false;

        template<class T0, class T1>
        constexpr inline bool IsPowToOnesV<OperationTraits<PowOperation>, T0, T1> =
          IsConstantTensorV<T1, IntFraction<1> >;

        /**@internal Change s^1 to s.*/
        template<class F, class T0, class T1, std::enable_if_t<IsPowToOnesV<F, T0, T1>, int> = 0>
        constexpr decltype(auto) operate(OptimizeTerminal1, F&&, T0&& t0, T1&& t1)
        {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          DUNE_ACFEM_EXPRESSION_RESULT(
            forwardReturnValue<T0>(t0)
            , "exponent is one"
            );
        }

        ////////////////////////////////////////////////////////////////

        template<class F, class T0, class T1>
        constexpr inline bool IsReciprocalPowV = false;

        template<class T0, class T1>
        constexpr inline bool IsReciprocalPowV<OperationTraits<PowOperation>, T0, T1> =
          IsConstantTensorV<T1, IntFraction<-1> >;

        /**@internal Change s^{-1} to 1/s.*/
        template<class F, class T0, class T1, std::enable_if_t<IsReciprocalPowV<F, T0, T1>, int> = 0>
        constexpr decltype(auto) operate(DefaultTag, F&&, T0&& t0, T1&& t1)
        {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          DUNE_ACFEM_EXPRESSION_RESULT(
            operate(OperationTraits<ReciprocalOperation>{}, std::forward<T0>(t0))
            , "exponent is -1"
            );
        }

        ////////////////////////////////////////////////////////////////

        template<class F, class T0, class T1>
        constexpr inline bool IsSqrtPowV = false;

        template<class T0, class T1>
        constexpr inline bool IsSqrtPowV<OperationTraits<PowOperation>, T0, T1> =
          IsConstantTensorV<T1, IntFraction<1, 2> >;

        /**@internal Change s^{1/2} to sqrt(s).*/
        template<class F, class T0, class T1, std::enable_if_t<IsSqrtPowV<F, T0, T1>, int> = 0>
        constexpr decltype(auto) operate(DefaultTag, F&&, T0&& t0, T1&& t1)
        {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          DUNE_ACFEM_EXPRESSION_RESULT(
            operate(OperationTraits<SqrtOperation>{}, std::forward<T0>(t0))
            , "exponent is 0.5"
            );
        }

        ////////////////////////////////////////////////////////////////

        template<class F, class T0, class T1>
        constexpr inline bool IsSquarePowV = false;

        template<class T0, class T1>
        constexpr inline bool IsSquarePowV<OperationTraits<PowOperation>, T0, T1> =
          (!IsExponentiationExpression<T0>::value
           && IsConstantTensorV<T1, IntFraction<2> >);

        /**@internal Change s^2 to sqr(s).*/
        template<class F, class T0, class T1, std::enable_if_t<IsSquarePowV<F, T0, T1>, int> = 0>
        constexpr decltype(auto) operate(DefaultTag, F&&, T0&& t0, T1&& t1)
        {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          DUNE_ACFEM_EXPRESSION_RESULT(
            operate(
              OperationTraits<SquareOperation>{},
              std::forward<T0>(t0)
              )
            , "exponent is 2"
            );
        }

        ////////////////////////////////////////////////////////////////

        template<class F, class T0, class T1>
        constexpr inline bool IsPowToZerosV = false;

        template<class T0, class T1>
        constexpr inline bool IsPowToZerosV<OperationTraits<PowOperation>, T0, T1> =
          IsConstantTensorV<T1, IntFraction<0> >;

        /**@internal Change s^0 to ones.*/
        template<class F, class T0, class T1, std::enable_if_t<IsPowToZerosV<F, T0, T1>, int> = 0>
        constexpr decltype(auto) operate(OptimizeTerminal1, F&&, T0&&, T1&& t1)
        {
          DUNE_ACFEM_RECORD_OPTIMIZATION;

          DUNE_ACFEM_EXPRESSION_RESULT(
            (ones<T0, Disclosure>())
            , "exponent is 0"
            );
        }

        ////////////////////////////////////////////////////////////////

        //!@} TensorPowerOptimizations
        //!@} TensorExpressionOptimizations
        //!@} TensorExpressions
        //!@} Tensors

      } // Powers::

    } // Tensor::Optimization::

    namespace Expressions {

      using Tensor::Optimization::Powers::operate;

    } // Expressions

  } // ACFem

} // Dune

#endif // __DUNE_ACFEM_TENSORS_OPTIMIZATION_POWERS_HH__
