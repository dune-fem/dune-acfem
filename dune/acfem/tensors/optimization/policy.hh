#ifndef __DUNE_ACFEM_TENSORS_OPTIMIZATION_POLICY_HH__
#define __DUNE_ACFEM_TENSORS_OPTIMIZATION_POLICY_HH__

#include "../../common/types.hh"
#include "../../expressions/optimizationbase.hh"
#include "../../expressions/examineutil.hh"
#include "../tensorbase.hh"

namespace Dune {

  namespace ACFem {

    namespace Tensor {

      namespace Policy {

        /**Default optimization-level is secondary.*/
        using DefaultOptimizationTag = Expressions::OptimizeFurther;

        /**Recurse into sub-expression optimization only at a later
         * (i.e. lower) level in order to avoid infinite loops.
         */
        using SubExpressionOptimizationTag = Expressions::OptimizeNext<DefaultOptimizationTag, 2>;

        /**@internal Prefer run-time loops over templated forEach for large
         * loops.
         */
        using TemplateForEachLimit = IndexConstant<100>;

      } // NS Policy

    } // NS Tensor

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_TENSORS_OPTIMIZATION_MULTIPY_HH__
