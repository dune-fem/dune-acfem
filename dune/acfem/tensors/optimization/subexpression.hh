#ifndef __DUNE_ACFEM_TENSORS_OPTIMIZATION_SUBEXPRESSION_HH__
#define __DUNE_ACFEM_TENSORS_OPTIMIZATION_SUBEXPRESSION_HH__

#include "../../expressions/optimizationbase.hh"
#include "../../expressions/subexpression.hh"
#include "../bindings.hh" // for one argument subExpression() operation.
#include "../operations/autodiff/fad.hh"
#include "../operations/autodiff/subexpression.hh"
#include "../../expressions/cseoptimization.hh"

namespace Dune {

  namespace ACFem {

    namespace Tensor {

      namespace Optimization::SubExpression {

        ////////////////////////////////////////////////////////////////

        /**@internal Identify non-elementary scalars. T is the current
         * expression, Parent its direct parent, Expr the top-level
         * expression currently examined.
         */
        template<class T, class Parent, class Expr>
        using IsNonAtomicScalar = BoolConstant<
          (// identify scalars
           IsExpression<T>::value
           && TensorTraits<T>::rank == 0
           && !IsSelfExpression<T>::value
           && !IsSubExpressionExpression<T>::value
           && !IsIndeterminateExpression<T>::value
           && !IsPlaceholderExpression<T>::value
           && (// generate sub-expressions in front of real tensors
               TensorTraits<Parent>::dim() > 1
               // generate sub-expressions if the same
               // expression occurs more than once.
               || Expressions::SubExpressionMultiplicity<Expr, T>::value > 1
               // generate sub-expressions for
               // unevaluated constants, but try to
               // avoid unnecessary sub-expressions.
               || (Expressions::IsConstantExprArg<T>::value
                   &&
                   (!Expressions::IsConstantExprArg<Parent>::value
                    ||
                    SameDecay<T, Parent>::value))
             )
            )>;

        struct TensorCSETraits
          : Expressions::SExpForward
        {
          template<class T, class Parent, class Expr>
          using IsCandidate = IsNonAtomicScalar<T, Parent, Expr>;

          template<class T>
          using Storage = DenseStorageType<T>;

          //!Compatibility with FAD-operation, saves some extra-ifs
          template<class... DontCare>
          static constexpr auto upTo(IndexConstant<0UL> = IndexConstant<0UL>{}, DontCare&&...)
          {
            return TensorCSETraits{}; // just return ourselves
          }
        };

        template<class T, std::enable_if_t<IsTensor<T>::value, int> = 0>
        constexpr auto commonSubExpressionCascade(T&& t)
        {
          return Expressions::commonSubExpressionCascade(std::forward<T>(t), TensorCSETraits{});
        }

        template<class FadOperation>
        struct AutoDiffCSETraits
          : EvalSExpFAD<FadOperation> // subexpression evaluation
        {
          using FadOperationType = FadOperation;

          template<class T, class Parent, class Expr>
          using IsCandidate = IsNonAtomicScalar<T, Parent, Expr>;

          /**@internal Storage will be just the result of the FAD operation
           */
          template<class T>
          using Storage = std::decay_t<decltype(asExpression(forwardAutoDiff(std::declval<T&&>(), FadOperation{})))>;
        };

        template<
          std::size_t... Orders, bool Evaluate, class T, class... Xs,
          std::enable_if_t<((0UL + ... + Orders) > 0 && IsTensor<T>::value), int> = 0>
        constexpr auto autoDiffCSECascade(BoolConstant<Evaluate>, T&& t, Xs&&... xs)
        {
          using FadOperation = decltype(fadOperation<Orders...>(BoolConstant<Evaluate>{}, std::forward<Xs>(xs)...));
          using Traits = AutoDiffCSETraits<FadOperation>;
          return commonSubExpressionCascade(std::forward<T>(t), Traits{});
        }

        template<
          std::size_t... Orders, class T, class... Xs,
          std::enable_if_t<((0UL + ... + Orders) > 0 && IsTensor<T>::value), int> = 0>
        constexpr decltype(auto) autoDiffCSECascade(T&& t, Xs&&... xs)
        {
          return autoDiffCSECascade<Orders...>(trueType(), std::forward<T>(t), std::forward<Xs>(xs)...);
        }

        //!Collapse to ordinary CSE if no derivatives are requested.
        template<std::size_t N, bool Evaluate, class T, class X,
                 std::enable_if_t<(N == 0 && IsTensor<T>::value), int> = 0>
        constexpr decltype(auto) autoDiffCSECascade(BoolConstant<Evaluate>, T&& t, X&&)
        {
          return commonSubExpressionCascade(std::forward<T>(t));
        }

        //!Collapse to ordinary CSE if no derivatives are requested.
        template<std::size_t N, class T, class X,
                 std::enable_if_t<(N == 0 && IsTensor<T>::value), int> = 0>
        constexpr decltype(auto) autoDiffCSECascade(T&& t, X&&)
        {
          return commonSubExpressionCascade(std::forward<T>(t));
        }

      } // Optimization::SubExpression::

      using Optimization::SubExpression::commonSubExpressionCascade;
      using Optimization::SubExpression::autoDiffCSECascade;

    } // NS Tensor

  } // ACFem

} // Dune

#endif // __DUNE_ACFEM_TENSORS_OPTIMIZATION_SUBEXPRESSION_HH__
