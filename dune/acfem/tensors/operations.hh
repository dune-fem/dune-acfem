#ifndef __DUNE_ACFEM_TENSOR_OPERATIONS_HH__
#define __DUNE_ACFEM_TENSOR_OPERATIONS_HH__

#include "operations/associativity.hh"
#include "operations/assume.hh"
#include "operations/einsum.hh"
#include "operations/einsumtraces.hh"
#include "operations/derivative.hh"
#include "operations/divergence.hh"
#include "operations/indeterminate.hh"
#include "operations/placeholder.hh"
#include "operations/product.hh"
#include "operations/reciprocal.hh"
#include "operations/reshape.hh"
#include "operations/restriction.hh"
#include "operations/transpose.hh"
#include "operations/transposetraits.hh"

#endif // __DUNE_ACFEM_TENSOR_OPERATIONS_HH__
