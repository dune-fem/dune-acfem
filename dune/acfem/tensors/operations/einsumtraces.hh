#ifndef __DUNE_ACFEM_TENSORS_OPERATIONS_EINSUMTRACES_HH__
#define __DUNE_ACFEM_TENSORS_OPERATIONS_EINSUMTRACES_HH__

#include "../../mpl/accumulate.hh" // for min(Sequence)
#include "../modules/eye.hh"
#include "../modules/blockeye.hh"
#include "einsum.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup Tensors
     * @{
     */

    namespace Tensor {

      using ACFem::min;

      /**@addtogroup TensorOperations
       * @{
       */

      template<class Tensor, std::size_t... Indices, std::size_t BlockRank, std::size_t... BlockDims>
      struct EinsumOperationTraits<Tensor, Seq<Indices...>, BlockEye<BlockRank, Seq<BlockDims...> >, MakeIndexSequence<BlockRank * sizeof...(BlockDims)>, std::enable_if_t<(sizeof...(Indices) > TensorTraits<Tensor>::rank)> >
      {
        using Dimensions = typename BlockEye<BlockRank, Seq<BlockDims...> >::Signature;
        using RightIndices = MakeIndexSequence<BlockRank * sizeof...(BlockDims)>;
        using Signature = SubSequenceComplement<typename TensorTraits<Tensor>::Signature, Indices...>;
        static constexpr std::size_t rank_ = Signature::size();
        using Operation = EinsumOperation<Seq<Indices...>, RightIndices, Dimensions>;
        using Functor = OperationTraits<Operation>;
      };

      template<class Tensor, class ContractPos, std::size_t BlockRank, class BlockDims>
      struct EinsumOperationTraits<Tensor, ContractPos, BlockEye<BlockRank, BlockDims>, MakeIndexSequence<BlockRank * BlockDims::size()>, std::enable_if_t<(ContractPos::size() == TensorTraits<Tensor>::rank)> >
      {
        using Dimensions = typename BlockEye<BlockRank, BlockDims>::Signature;
        using EyeContractPos = MakeIndexSequence<BlockRank * BlockDims::size()>;
        using Signature = Seq<>;
        static constexpr std::size_t rank_ = 0;
        using Operation = EinsumOperation<ContractPos, EyeContractPos, Dimensions>;
        using Functor = OperationTraits<Operation>;
      };

      template<class Tensor, class Positions, std::size_t BlockRank, class BlockDims>
      using BlockTraceTraits = EinsumOperationTraits<Tensor, Positions, BlockEye<BlockRank, BlockDims>, MakeIndexSequence<BlockRank * BlockDims::size()> >;

      template<class Tensor, class Positions, std::size_t BlockRank, class BlockDims>
      using BlockTraceSignature = typename BlockTraceTraits<Tensor, Positions, BlockRank, BlockDims>::Signature;

      template<class Tensor, class Positions, std::size_t BlockRank, class BlockDims>
      using BlockTraceFunctor = typename BlockTraceTraits<Tensor, Positions, BlockRank, BlockDims>::Functor;

      template<class Tensor, std::size_t... ContractPos, std::size_t BlockRank, std::size_t... BlockDims, bool IdenticalOperands>
      class EinsteinSummation<Tensor, Seq<ContractPos...>,
                              BlockEye<BlockRank, Seq<BlockDims...> >,
                              MakeIndexSequence<BlockRank * sizeof...(BlockDims)>,
                              IdenticalOperands>
        : public TensorBase<typename TensorTraits<Tensor>::FieldType,
                            BlockTraceSignature<Tensor, Seq<ContractPos...>, BlockRank, Seq<BlockDims...> >,
                            EinsteinSummation<Tensor, Seq<ContractPos...>, BlockEye<BlockRank, Seq<BlockDims...> >, MakeIndexSequence<BlockRank * sizeof...(BlockDims)> > >
        , public Expressions::Storage<BlockTraceFunctor<Tensor, Seq<ContractPos...>, BlockRank, Seq<BlockDims...> >, Tensor, BlockEye<BlockRank, Seq<BlockDims...> > >
      {
       public:
        using Signature = BlockTraceSignature<Tensor, Seq<ContractPos...>, BlockRank, Seq<BlockDims...> >;
       private:
        using ThisType = EinsteinSummation;
        using ArgumentType = std::decay_t<Tensor>;
        using TraceTensor = BlockEye<BlockRank, Seq<BlockDims...> >;
        using BaseType = TensorBase<typename ArgumentType::FieldType, Signature, ThisType>;
        using StorageType = Expressions::Storage<BlockTraceFunctor<Tensor, Seq<ContractPos...>, BlockRank, Seq<BlockDims...> >, Tensor, TraceTensor>;
        using TensorType = std::decay_t<Tensor>;
       private:
        // Mmmh. Should we really support this?
        using IndexPositions = Seq<ContractPos...>;
        using Sorted = SortSequence<IndexPositions>;
        using Permutation = typename Sorted::Permutation;
        using SortedPos = typename Sorted::Result;
       public:
        using BlockSignature = Seq<BlockDims...>;
        using StorageType::operation;
        using StorageType::operand;
        using BaseType::rank;
        using typename BaseType::FieldType;

       public:

        /**Einsum constructor from two arguments.*/
        template<class LeftArg, class RightArg,
                 std::enable_if_t<std::is_constructible<Tensor, LeftArg>::value && std::is_constructible<TraceTensor, RightArg>::value, int> = 0>
        EinsteinSummation(LeftArg&& left, RightArg&& right)
          : StorageType(std::forward<LeftArg>(left), TraceTensor{})
        {}

        /**Constructor from a given tensor.*/
        template<class Arg, std::enable_if_t<std::is_constructible<Tensor, Arg>::value, int> = 0>
        EinsteinSummation(Arg&& arg)
          : StorageType(std::forward<Arg>(arg), TraceTensor{})
        {}

        /**Allow default construction if contained types fulfill IsTypedValue.*/
        template<
          class... Dummy,
          std::enable_if_t<(sizeof...(Dummy) == 0
                            && IsTypedValue<Tensor>::value), int> = 0>
        EinsteinSummation(Dummy&&...)
          : StorageType(Tensor{}, TraceTensor{})
        {}

       private:
        template<std::size_t IndexNum>
        using TraceIndices = PermuteSequence<SequenceProd<BlockRank, MultiIndex<IndexNum, BlockSignature> >, Permutation>;

        template<std::size_t IndexNum, class... Dims>
        static constexpr decltype(auto) makeIndices(std::tuple<Dims...>&& indices)
        {
          return insertAt(indices, TraceIndices<IndexNum>{}, SortedPos{});
        }

        template<std::size_t... IndexNum>
        constexpr auto valueHelper(Seq<IndexNum...>) const
        {
          return (0_f + ... + tensorValue(operand(0_c), TraceIndices<IndexNum>{}));
        }

        template<std::size_t... IndexNum, class... Dims>
        constexpr auto valueHelper(Seq<IndexNum...>, std::tuple<Dims...>&& indices) const
        {
          return (0_f + ... + tensorValue(operand(0_c), makeIndices<IndexNum>(std::move(indices))));
        }

        template<std::size_t IndexNum, class Indices>
        using MakeIndices = InsertAt<Indices, TraceIndices<IndexNum>, SortedPos>;

        template<std::size_t... IndexNum, class Indices>
        constexpr auto valueHelper(Seq<IndexNum...>, Indices) const
        {
          return  (0_f + ... + (intFraction<!TensorType::isZero(MakeIndices<IndexNum, Indices>{})>() * operand(0_c)(MakeIndices<IndexNum, Indices>{})));
        }

       public:
        /**Runtime-dynamic element access.*/
        template<class... Dims,
                 std::enable_if_t<(sizeof...(Dims) == rank
                                   &&
                                   IsIntegralPack<Dims...>::value), int> = 0>
        constexpr auto operator()(Dims... indices) const
        {
          constexpr std::size_t numDiagonals = (1 * ... * BlockDims);
          if constexpr (rank > 0) {
            return valueHelper(MakeIndexSequence<numDiagonals>{}, std::forward_as_tuple(indices...));
          } else {
            return valueHelper(MakeIndexSequence<numDiagonals>{});
          }
        }

        /**Compile-time static element access.*/
        template<std::size_t... Indices>
        constexpr auto operator()(Seq<Indices...>) const
        {
          constexpr std::size_t numDiagonals = (1 * ... * BlockDims);
          return valueHelper(MakeIndexSequence<numDiagonals>{}, Seq<Indices...>{});
        }

       private:
        template<class Ind, class Inj, class Pos, std::size_t... N>
        static bool constexpr isZeroExpander(Ind, Inj, Pos, Seq<N...>)
        {
          return (... && (TensorType::isZero(InsertAt<TraceIndices<N>, Ind, Inj>{}, Pos{})));
        }

        template<std::size_t... Indices, class Pos = MakeIndexSequence<sizeof...(Indices)> >
        static bool constexpr isZeroWorker(Seq<Indices...> = Seq<Indices...>{}, Pos = Pos{})
        {
          // This ain't pretty ;)

          // Truncate Pos to size of Indices and sort it, otherwise
          // the JoinedDefects stuff will not work properly
          using TruncatedPos = HeadPart<sizeof...(Indices), Pos>;

          using RealPos = typename SortSequence<TruncatedPos>::Result;
          using Permutation = typename SortSequence<TruncatedPos>::Permutation;
          using RealIndices = PermuteSequence<Seq<Indices...>, Permutation>;

          // The total set of left index positions, contraction and taken from Indices...
          using TotalPos = JoinedDefects<SortedPos, RealPos>;
          using Inj = JoinedInjections<SortedPos, RealPos>;

          return isZeroExpander(RealIndices{}, Inj{}, TotalPos{}, MakeIndexSequence<(1 * ... * BlockDims)>{});
        }

       public:
        template<std::size_t... Indices, class Pos = MakeIndexSequence<sizeof...(Indices)> >
        static bool constexpr isZero(Seq<Indices...> = Seq<Indices...>{}, Pos = Pos{})
        {
          if constexpr (ExpressionTraits<TensorType>::isZero) {
            return true;
          } else {
            return isZeroWorker(Seq<Indices...>{}, Pos{});
          }
        }

        std::string name() const
        {
          std::string pfx = std::is_reference<Tensor>::value ? (RefersConst<Tensor>::value ? "cref" : "ref") : "";
          return "blockTrace<"+toString(IndexPositions{})+"|"+toString(BlockRank)+">("+pfx+operand(0_c).name()+")";
        }
      };

      /////////////////////////////////////////////////////////////////////////

      template<class Tensor, std::size_t... Indices, std::size_t... EyeDims>
      struct EinsumOperationTraits<Tensor, Seq<Indices...>, Eye<Seq<EyeDims...> >, MakeIndexSequence<sizeof...(EyeDims)>, std::enable_if_t<(sizeof...(Indices) > TensorTraits<Tensor>::rank)> >
      {
        using Dimensions = typename Eye<Seq<EyeDims...> >::Signature;
        using RightIndices = MakeIndexSequence<sizeof...(EyeDims)>;
        using Signature = SubSequenceComplement<typename TensorTraits<Tensor>::Signature, Indices...>;
        static constexpr std::size_t rank_ = Signature::size();
        using Operation = EinsumOperation<Seq<Indices...>, RightIndices, Dimensions>;
        using Functor = OperationTraits<Operation>;
      };

      template<class Tensor, class ContractPos, class EyeDims>
      struct EinsumOperationTraits<Tensor, ContractPos, Eye<EyeDims>, MakeIndexSequence<EyeDims::size()>, std::enable_if_t<(ContractPos::size() == TensorTraits<Tensor>::rank)> >
      {
        using Dimensions = typename Eye<EyeDims>::Signature;
        using EyeContractPos = MakeIndexSequence<EyeDims::size()>;
        using Signature = Seq<>;
        static constexpr std::size_t rank_ = 0;
        using Operation = EinsumOperation<ContractPos, EyeContractPos, Dimensions>;
        using Functor = OperationTraits<Operation>;
      };

      template<class Tensor, class Positions, class EyeDims>
      using TraceTraits = EinsumOperationTraits<Tensor, Positions, Eye<EyeDims>, MakeIndexSequence<EyeDims::size()> >;

      template<class Tensor, class Positions, class EyeDims>
      using TraceSignature = typename TraceTraits<Tensor, Positions, EyeDims>::Signature;

      template<class Tensor, class Positions, class EyeDims>
      using TraceFunctor = typename TraceTraits<Tensor, Positions, EyeDims>::Functor;

      template<class Tensor, std::size_t... ContractPos, std::size_t... EyeDims, bool IdenticalOperands>
      class EinsteinSummation<Tensor, Seq<ContractPos...>,
                              Eye<Seq<EyeDims...> >,
                              MakeIndexSequence<sizeof...(EyeDims)>,
                              IdenticalOperands>
        : public TensorBase<typename TensorTraits<Tensor>::FieldType,
                            TraceSignature<Tensor, Seq<ContractPos...>, Seq<EyeDims...> >,
                            EinsteinSummation<Tensor, Seq<ContractPos...>, Eye<Seq<EyeDims...> >, MakeIndexSequence<sizeof...(EyeDims)> > >
        , public Expressions::Storage<TraceFunctor<Tensor, Seq<ContractPos...>, Seq<EyeDims...> >, Tensor, Eye<Seq<EyeDims...> > >
      {
       public:
        using Signature = TraceSignature<Tensor, Seq<ContractPos...>, Seq<EyeDims...> >;
       private:
        using ThisType = EinsteinSummation;
        using ArgumentType = std::decay_t<Tensor>;
        using TraceTensor = Eye<Seq<EyeDims...> >;
        using BaseType = TensorBase<typename ArgumentType::FieldType, Signature, ThisType>;
        using StorageType = Expressions::Storage<TraceFunctor<Tensor, Seq<ContractPos...>, Seq<EyeDims...> >, Tensor, TraceTensor>;
        using TensorType = std::decay_t<Tensor>;
       private:
        // Mmmh. Should we really support this?
        using IndexPositions = Seq<ContractPos...>;
        using Sorted = SortSequence<IndexPositions>;
        using Permutation = typename Sorted::Permutation;
        using SortedPos = typename Sorted::Result;
       public:
        static constexpr std::size_t traceDim_ = ACFem::min(Seq<EyeDims...>{});
        using StorageType::operation;
        using StorageType::operand;
        using BaseType::rank;
        using typename BaseType::FieldType;

       public:

        /**Einsum constructor from two arguments.*/
        template<class LeftArg, class RightArg,
                 std::enable_if_t<std::is_constructible<Tensor, LeftArg>::value && std::is_constructible<TraceTensor, RightArg>::value, int> = 0>
        EinsteinSummation(LeftArg&& left, RightArg&& right)
          : StorageType(std::forward<LeftArg>(left), TraceTensor{})
        {}

        /**Constructor from a given tensor.*/
        template<class Arg, std::enable_if_t<std::is_constructible<Tensor, Arg>::value, int> = 0>
        EinsteinSummation(Arg&& arg)
          : StorageType(std::forward<Arg>(arg), TraceTensor{})
        {}

        /**Allow default construction if contained types fulfill IsTypedValue.*/
        template<
          class... Dummy,
          std::enable_if_t<(sizeof...(Dummy) == 0
                            && IsTypedValue<Tensor>::value), int> = 0>
        EinsteinSummation(Dummy&&...)
          : StorageType(Tensor{}, TraceTensor{})
        {}

       private:
        template<std::size_t IndexNum>
        using TraceIndices = MakeConstantSequence<sizeof...(EyeDims), IndexNum>;

        template<std::size_t IndexNum, class... Dims>
        static constexpr decltype(auto) makeIndices(std::tuple<Dims...>&& indices)
        {
          return insertAt(indices, TraceIndices<IndexNum>{}, SortedPos{});
        }

        template<std::size_t... IndexNum>
        constexpr auto valueHelper(Seq<IndexNum...>) const
        {
          return (0_f + ... + tensorValue(operand(0_c), TraceIndices<IndexNum>{}));
        }

        template<std::size_t... IndexNum, class... Dims>
        constexpr auto valueHelper(Seq<IndexNum...>, std::tuple<Dims...>&& indices) const
        {
          return (0_f + ... + tensorValue(operand(0_c), makeIndices<IndexNum>(std::move(indices))));
        }

        template<std::size_t IndexNum, class Indices>
        using MakeIndices = InsertAt<Indices, TraceIndices<IndexNum>, SortedPos>;

        template<std::size_t... IndexNum, class Indices>
        constexpr auto valueHelper(Seq<IndexNum...>, Indices) const
        {
          return  (0_f + ... + (intFraction<!TensorType::isZero(MakeIndices<IndexNum, Indices>{})>() * operand(0_c)(MakeIndices<IndexNum, Indices>{})));
        }

       public:
        /**Runtime-dynamic element access.*/
        template<class... Dims,
                 std::enable_if_t<(sizeof...(Dims) == rank
                                   &&
                                   IsIntegralPack<Dims...>::value), int> = 0>
        constexpr auto operator()(Dims... indices) const
        {
          if constexpr (rank > 0) {
            return valueHelper(MakeIndexSequence<traceDim_>{}, std::forward_as_tuple(indices...));
          } else {
            return valueHelper(MakeIndexSequence<traceDim_>{});
          }
        }

        /**Compile-time static element access.*/
        template<std::size_t... Indices>
        constexpr auto operator()(Seq<Indices...>) const
        {
          return valueHelper(MakeIndexSequence<traceDim_>{}, Seq<Indices...>{});
        }

       private:
        template<class Ind, class Inj, class Pos, std::size_t... N>
        static bool constexpr isZeroExpander(Ind, Inj, Pos, Seq<N...>)
        {
          return (... && (TensorType::isZero(InsertAt<TraceIndices<N>, Ind, Inj>{}, Pos{})));
        }

        template<std::size_t... Indices, class Pos = MakeIndexSequence<sizeof...(Indices)> >
        static bool constexpr isZeroWorker(Seq<Indices...> = Seq<Indices...>{}, Pos = Pos{})
        {
          // This ain't pretty ;)

          // Truncate Pos to size of Indices and sort it, otherwise
          // the JoinedDefects stuff will not work properly
          using TruncatedPos = HeadPart<sizeof...(Indices), Pos>;

          using RealPos = typename SortSequence<TruncatedPos>::Result;
          using Permutation = typename SortSequence<TruncatedPos>::Permutation;
          using RealIndices = PermuteSequence<Seq<Indices...>, Permutation>;

          // The total set of left index positions, contraction and taken from Indices...
          using TotalPos = JoinedDefects<SortedPos, RealPos>;
          using Inj = JoinedInjections<SortedPos, RealPos>;

          return isZeroExpander(RealIndices{}, Inj{}, TotalPos{}, MakeIndexSequence<traceDim_>{});
        }

       public:
        template<std::size_t... Indices, class Pos = MakeIndexSequence<sizeof...(Indices)> >
        static bool constexpr isZero(Seq<Indices...> = Seq<Indices...>{}, Pos = Pos{})
        {
          if constexpr (ExpressionTraits<TensorType>::isZero) {
            return true;
          } else {
            return isZeroWorker(Seq<Indices...>{}, Pos{});
          }
        }

        std::string name() const
        {
          std::string pfx = std::is_reference<Tensor>::value ? (RefersConst<Tensor>::value ? "cref" : "ref") : "";
          return "trace<"+toString(IndexPositions{})+">("+pfx+operand(0_c).name()+")";
        }
      };

    } // NS Tensor

    ///@} TensorOperations

  } // NS ACFem

  ///@} Tensors

} // NS Dune

#endif // __DUNE_ACFEM_TENSORS_OPERATIONS_EINSUMTRACES_HH__
