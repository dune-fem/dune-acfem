#ifndef __DUNE_ACFEM_TENSORS_OPERATIONS_DEPENDSON_HH__
#define __DUNE_ACFEM_TENSORS_OPERATIONS_DEPENDSON_HH__

#include "../../expressions/examine.hh"
#include "derivative.hh"

namespace Dune {

  namespace ACFem {

    namespace Tensor {

      /**@internal @c TrueType if T either depends explicitly on an
       * indeterminate with id @a Id and tensor-signature @a Signature.
       */
      template<class T, class Id, class Signature, class SFINAE = void>
      struct TerminalDependsOn
        : FalseType
      {};

      /**@internal Only indeterminates and placeholder tensors can
       * depend on an indeterminate.
       */
      template<class T, std::size_t Id, class Signature>
      struct TerminalDependsOn<
        T, IndexConstant<Id>, Signature,
        std::enable_if_t<(!IndeterminateMatch<T, IndexConstant<Id> >::value
                          && !IsPlaceholderExpression<T>::value
          )> >
        : FalseType
      {};

      template<class T, std::size_t Id, class Signature>
      struct TerminalDependsOn<
        T, IndexConstant<Id>, Signature,
        std::enable_if_t<(IndeterminateMatch<T, IndexConstant<Id> >::value
                          || (IsPlaceholderExpression<T>::value
                              &&
                              !ExpressionTraits<decltype(doDerivative<Id, Signature>(std::declval<T>(), PriorityTag<42>{}))>::isZero)
        )> >
        : TrueType
      {};

      /**@c TrueType if T either directly or through a placeholder
       * tensor depends on an indeterminate with given Id and Signature.
       */
      template<std::size_t Id, class Signature, class T>
      using DependsOn = Expressions::ExamineOr<T, TerminalDependsOn, IndexConstant<Id>, Signature>;

      template<class X, class T>
      constexpr auto dependsOn(X&& x, T&& t)
      {
        using XType = EnclosedType<X>;
        constexpr std::size_t id = IndeterminateTraits<XType>::id_;
        using Signature = typename TensorTraits<XType>::Signature;

        return DependsOn<id, Signature, T>{};
      }

    } // NS Tensor

  } // NS Tensor

} // NS Dune

#endif // __DUNE_ACFEM_TENSORS_OPERATIONS_DEPENDSON_HH__
