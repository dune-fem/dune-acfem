#ifndef __DUNE_ACFEM_TENSORS_OPERATIONS_DERIVATIVE_HH__
#define __DUNE_ACFEM_TENSORS_OPERATIONS_DERIVATIVE_HH__

#include "../tensorbase.hh"
#include "../modules.hh"

#include "derivativetraits.hh"
#include "indeterminate.hh"
#include "placeholder.hh"

namespace Dune {

  namespace ACFem {

    namespace Tensor {

      /**Starting point for symbolic and automatic differentiation:
       * the derivative of an indeterminate w.r.t. itself is just the
       * identity mapping.
       */
      template<std::size_t Id, class Signature, class T,
               std::enable_if_t<IndeterminateMatch<T, IndexConstant<Id> >::value, int> = 0>
      constexpr auto doDerivative(T&& t, PriorityTag<0>)
      {
        static_assert(TensorTraits<T>::signature() == Signature{},
                      "Mismatching tensor signature for indeterminate.");
        return blockEye<2>(TensorTraits<T>::signature(), Disclosure{});
      }

      /**Starting point for symbolic and automic differentiation:
       * terminal expressions have zero derivative.
       */
      template<std::size_t Id, class Signature, class T,
               std::enable_if_t<Expressions::IsTerminal<T>::value, int> = 0>
      constexpr auto doDerivative(T&& t, PriorityTag<0>)
      {
        using DerivativeSignature = SequenceCat<typename TensorTraits<T>::Signature, Signature>;
        return zeros(DerivativeSignature{}, Disclosure{});
      }

      /**Define derivatives for unary tensor expressions. This also
       * implements the chain-rule when using an indeterminate storage
       * where the storage depends on other indeterinates.
       */
      template<
        std::size_t Id, class Signature, class T,
        std::enable_if_t<(IsUnaryTensorExpression<T>::value
                          && !IndeterminateMatch<T, IndexConstant<Id> >::value
                          && !Expressions::IsTerminal<T>::value
        ), int> = 0>
      constexpr decltype(auto) doDerivative(T&& t, PriorityTag<0>)
      {
        using Traits = DerivativeTraits<Expressions::Operation<T> >;

        return Traits::derivative(subExpressionExpression(std::forward<T>(t).operand(0_c)),
                                  doDerivative<Id, Signature>(std::forward<T>(t).operand(0_c), PriorityTag<42>{}),
                                  std::forward<T>(t).operation());
      }

      /**Define derivatives for binary tensor expressions.*/
      template<
        std::size_t Id, class Signature, class T,
        std::enable_if_t<IsBinaryTensorExpression<T>::value, int> = 0>
      constexpr decltype(auto) doDerivative(T&& t, PriorityTag<0>)
      {
        using Traits = DerivativeTraits<Expressions::Operation<T> >;
        return Traits::derivative(
          subExpressionExpression(std::forward<T>(t).operand(0_c)),
          doDerivative<Id, Signature>(std::forward<T>(t).operand(0_c), PriorityTag<42>{}),
          subExpressionExpression(std::forward<T>(t).operand(1_c)),
          doDerivative<Id, Signature>(std::forward<T>(t).operand(1_c), PriorityTag<42>{}),
          std::forward<T>(t).operation()
          );
      }

      /**Define derivatives for any tensor expression.*/
      template<
        class T, class X,
        std::enable_if_t<(IsTensorOperand<T>::value
                          && IsTensor<X>::value
                          && IsIndeterminateExpression<EnclosedType<X> >::value
        ), int> = 0>
      constexpr decltype(auto) derivative(T&& t, X&&)
      {
        using XType = EnclosedType<X>;
        constexpr std::size_t id = IndeterminateTraits<XType>::id_;
        using Signature = typename TensorTraits<XType>::Signature;

#if DUNE_ACFEM_TRACE_OPTIMIZATION
        std::clog << "D[" << asExpression(t).name() << "] = ... " << std::endl;
#endif

        DUNE_ACFEM_EXPRESSION_RESULT(
          expressionClosure(doDerivative<id, Signature>(asExpression(std::forward<T>(t)), PriorityTag<42>{}))
          , "derivative w.r.t. " + toString(id)
          );
      }

      template<std::size_t Id, std::size_t... Dims, class T,
               std::enable_if_t<IsTensorOperand<T>::value, int> = 0>
      constexpr decltype(auto) derivative(T&& t, Seq<Dims...> = Seq<Dims...>{}, IndexConstant<Id> = IndexConstant<Id>{})
      {
        return derivative(std::forward<T>(t), indeterminate<Id>(Seq<Dims...>{}));
      }

      template<std::size_t Order, std::size_t... Orders, class T, class X0, class... Xs,
               std::enable_if_t<IsIndeterminateExpression<EnclosedType<X0> >::value, int> = 0>
      constexpr decltype(auto) derivative(T&& t, X0&& x0, Xs&&... xs)
      {
        if constexpr (Order > 0) {
          return derivative<Order-1, Orders...>(derivative(std::forward<T>(t), std::forward<X0>(x0)), std::forward<X0>(x0), std::forward<Xs>(xs)...);
        } else if constexpr (sizeof...(Orders) > 0) {
          return derivative<Orders...>(std::forward<T>(t), std::forward<Xs>(xs)...);
        } else {
          return forwardReturnValue<T>(t);
        }
      }

      template<std::size_t... Order, class T, class X0, class... Xs,
               std::enable_if_t<IsIndeterminateExpression<EnclosedType<X0> >::value, int> = 0>
      constexpr decltype(auto) derivative(T&& t, IndexSequence<Order...>, X0&& x0, Xs&&... xs)
      {
        static_assert(sizeof...(Order) == sizeof...(Xs)+1,
                      "Orders have to be specified for each indeterminate.");
        return derivative<Order...>(std::forward<T>(t), std::forward<X0>(x0), std::forward<Xs>(xs)...);
      }

      template<class T, class X0, class... Xs,
               std::enable_if_t<IsIndeterminateExpression<EnclosedType<X0> >::value, int> = 0>
      constexpr decltype(auto) derivative(T&& t, X0&& x0, Xs&&... xs)
      {
        return derivative(std::forward<T>(t), MakeConstantSequence<sizeof...(Xs)+1, 1>{}, std::forward<X0>(x0), std::forward<Xs>(xs)...);
      }

      template<std::size_t Id0, std::size_t... Id, class T, class Signature0, class... Signature,
               std::enable_if_t<IsSequence<Signature0>::value, int> = 0>
      constexpr decltype(auto) derivative(T&& t, Signature0, Signature...)
      {
        return derivative(std::forward<T>(t),
                          MakeConstantSequence<sizeof...(Id), 1>{},
                          indeterminate<Id0>(Signature0{}),
                          indeterminate<Id>(Signature{})...);
      }

    } // NS Tensor

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_TENSORS_OPERATIONS_DERIVATIVE_HH__
