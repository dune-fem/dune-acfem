#ifndef __DUNE_ACFEM_TENSORS_OPERATIONS_SUBEXPRESSION_HH__
#define __DUNE_ACFEM_TENSORS_OPERATIONS_SUBEXPRESSION_HH__

#include "../tensorbase.hh"
#include "../expressiontraits.hh"

namespace Dune {

  namespace ACFem {

    namespace Tensor {

      /**Generate a subexpression check-point with cache T1, wrapping
       * the expression T2. This is useful in order to force
       * pre-evaluation of non-trivial contractions which survived the
       * optimization machinery.
       *
       * @param t1 Storage cache for the values of t2.
       *
       * @param t2 The wrapped sub-expression.
       *
       * @return The sub-expression operation wrapping a T2 with cache
       * type T1.
       *
       * @note @b Requirements: As subExpression() takes part in
       * global overload resolution the requirement are implemented
       * such that the template will not be instantiated instead of
       * placing a static_assert() inside the function.
       *
       * + A T1 has to be assignable by a T2.
       *    + This means that @a t1 must be an rvalue reference (in
       *      which case T1 is just the referenced type) or a mutable
       *      lvalue reference.
       *    + The tensor signatures of T1 and T2 must match
       * + T1 and T2 must qualify as valid operand via
       *   AreBindaryTensorOperands
       */
      template<std::size_t... OperandPos, class T1, class T2,
               std::enable_if_t<(AreBinaryTensorOperands<T1, T2, void>::value
                                 && !RefersConst<T1>::value
        ), int> = 0>
      constexpr auto subExpression(T1&& t1, T2&& t2, Seq<OperandPos...> = Seq<OperandPos...>{})
      {
        static_assert((IsTypedValue<T1>::value && IsTypedValue<T2>::value)
                      || std::is_assignable<T1, T2>::value,
                      "Storage cache T1 cannot be assigned a T2.");

        static_assert(!IsSubExpressionExpression<T2>::value,
                      "Attempt to nest sub-expressions.");

        using TreePos = Seq<OperandPos...>;

        auto sExp = operate(Expressions::DontOptimize{},
                            OperationTraits<SubExpressionOperation<TreePos> >{},
                            tensor(std::forward<T1>(t1)),
                            tensor(std::forward<T2>(t2)));

        // Evaluate constant sub-expressions once during construction.
        //if constexpr (IsConstantExprArg<T2>::value) {
          sExp.operand(0_c) = sExp.operand(1_c);
        //}

        return expressionClosure(std::move(sExp));
      }

      template<std::size_t... OperandPos, class T1, class T2,
               std::enable_if_t<(AreBinaryTensorOperands<T1, T2, void>::value
                                 && RefersConst<T1>::value
        ), int> = 0>
      constexpr decltype(auto) subExpression(T1&& t1, T2&& t2, Seq<OperandPos...> treePos= Seq<OperandPos...>{})
      {
        return subExpression(std::remove_const_t<std::decay_t<T1> >{}, std::forward<T2>(t2), treePos);
      }

      template<class T1, class T2, class TreePos = Seq<> >
      using SubExpressionTensor = decltype(subExpression(std::declval<T1>(), std::declval<T2>(), TreePos{}));

    } // NS Tensor

    using Tensor::subExpression;

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_TENSORS_OPERATIONS_SUBEXPRESSION_HH__
