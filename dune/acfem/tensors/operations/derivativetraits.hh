#ifndef __DUNE_ACFEM_TENSORS_OPERATIONS_DERIVATIVETRAITS_HH__
#define __DUNE_ACFEM_TENSORS_OPERATIONS_DERIVATIVETRAITS_HH__

#include "../../mpl/generators.hh"
#include "../../expressions/constantoperations.hh"
#include "../expressiontraits.hh"
#include "../expressionoperations.hh"
#include "../modules.hh"
#include "../expressions.hh"
#include "../operandpromotion.hh"
#include "../literals.hh"
#include "einsum.hh"
#include "product.hh"
#include "reshape.hh"
#include "restriction.hh"
#include "transpose.hh"

namespace Dune::ACFem::Tensor {

  using namespace Expressions;

  template<class Operation>
  struct DerivativeTraits;

  namespace {

    /**Helper in order to formulate derivatives for component-wise
     * operations.
     */
    template<class T, class DT>
    constexpr decltype(auto) componentWiseDerivativeHelper(T&& outerDt, DT&& innerDt)
    {
      return operate<TotalProductOperation<T> >(std::forward<T>(outerDt), std::forward<DT>(innerDt));
    }
  }

  template<>
  struct DerivativeTraits<IdentityOperation>
  {
    template<class T, class DT, class Functor>
    static constexpr decltype(auto) derivative(T&& t, DT&& dt, Functor&&)
    {
      return forwardReturnValue<DT>(dt);
    }
  };

  /**@internal Implement the chain rule.*/
  template<std::size_t Id>
  struct DerivativeTraits<IndeterminateOperation<Id> >
  {
    template<class T, class DT, class Functor>
    static constexpr decltype(auto) derivative(T&&, DT&& dt, Functor&&)
    {
      return forwardReturnValue<DT>(dt);
    }
  };

  template<class Placeholder>
  struct DerivativeTraits<PlaceholderOperation<Placeholder> >
  {
    template<class T, class DT, class Functor>
    static constexpr auto derivative(T&& t, DT&& dt, Functor&&)
    {
      return zeros<DT>(Disclosure{});
    }
  };

  template<class... Property>
  struct DerivativeTraits<AssumeOperation<Property...> >
  {
    template<class T, class DT, class Functor>
    static constexpr decltype(auto) derivative(T&& t, DT&& dt, Functor&&)
    {
      return forwardReturnValue<DT>(dt);
    }
  };

  template<class Id>
  struct DerivativeTraits<SubExpressionOperation<Id> >
  {
    // Just forward the second component.
    template<class T1, class DT1, class T2, class DT2, class Functor>
    static constexpr decltype(auto) derivative(T1&& t1, DT1&& dt1, T2&& t2, DT2&& dt2, Functor&&)
    {
      return forwardReturnValue<DT2>(dt2);
    }
  };

  /**Derivative expression for component wise invert operation.*/
  template<>
  struct DerivativeTraits<ReciprocalOperation>
  {
    template<class T, class DT, class Functor>
    static constexpr decltype(auto) derivative(T&& t, DT&& dt, Functor&&)
    {
      return operate<MinusOperation>(
        componentWiseDerivativeHelper(
          operate<ReciprocalOperation>(
            operate<SquareOperation>(
              std::forward<T>(t)
              )
            ),
          std::forward<DT>(dt))
        );
    }
  };

  template<>
  struct DerivativeTraits<SquareOperation>
  {
    template<class T, class DT, class Functor>
    static constexpr decltype(auto) derivative(T&& t, DT&& dt, Functor&&)
    {
      return operate<ScalarEinsumOperation>(
        tensor(2_f),
        componentWiseDerivativeHelper(std::forward<T>(t), std::forward<DT>(dt))
        );
    }
  };

  template<>
  struct DerivativeTraits<SqrtOperation>
  {
    template<class T, class DT, class Functor>
    static constexpr decltype(auto) derivative(T&& t, DT&& dt, Functor&&)
    {
      return componentWiseDerivativeHelper(
        operate<ScalarEinsumOperation>(
          tensor(intFraction<1,2>()),
          operate<ReciprocalOperation>(
            operate<SqrtOperation>(
              std::forward<T>(t)
              )
            )
          ),
        std::forward<DT>(dt)
        );
    }
  };

  template<>
  struct DerivativeTraits<ExpOperation>
  {
    template<class T, class DT, class Functor>
    static constexpr decltype(auto) derivative(T&& t, DT&& dt, Functor&&)
    {
      return componentWiseDerivativeHelper(
        operate<ExpOperation>(std::forward<T>(t)),
        std::forward<DT>(dt)
        );
    }
  };

  template<>
  struct DerivativeTraits<LogOperation>
  {
    template<class T, class DT, class Functor>
    static constexpr decltype(auto) derivative(T&& t, DT&& dt, Functor&&)
    {
      return componentWiseDerivativeHelper(
        operate<ReciprocalOperation>(std::forward<T>(t)),
        std::forward<DT>(dt)
        );
    }
  };

  template<>
  struct DerivativeTraits<ErfOperation>
  {
    template<class T, class DT, class Functor>
    static constexpr decltype(auto) derivative(T&& t, DT&& dt, Functor&&)
    {
      return componentWiseDerivativeHelper(
        operate<ScalarEinsumOperation>(
          tensor(2_f),
          operate<ReciprocalOperation>(
            operate<ScalarEinsumOperation>(
              operate<SqrtOperation>(asExpression(M::pi)),
              operate<ExpOperation>(
                operate<MinusOperation>(
                  operate<SquareOperation>(
                    std::forward<T>(t)
                    )))))),
        std::forward<DT>(dt)
        );
    }
  };

  template<>
  struct DerivativeTraits<SinOperation>
  {
    template<class T, class DT, class Functor>
    static constexpr decltype(auto) derivative(T&& t, DT&& dt, Functor&&)
    {
      return componentWiseDerivativeHelper(
        operate<CosOperation>(std::forward<T>(t)),
        std::forward<DT>(dt)
        );
    }
  };

  template<>
  struct DerivativeTraits<CosOperation>
  {
    template<class T, class DT, class Functor>
    static constexpr decltype(auto) derivative(T&& t, DT&& dt, Functor&&)
    {
      return componentWiseDerivativeHelper(
        operate<MinusOperation>(
          operate<SinOperation>(
            std::forward<T>(t)
            )),
        std::forward<DT>(dt));
    }
  };

  template<>
  struct DerivativeTraits<TanOperation>
  {
    template<class T, class DT, class Functor>
    static constexpr decltype(auto) derivative(T&& t, DT&& dt, Functor&&)
    {
      return componentWiseDerivativeHelper(
        operate<ReciprocalOperation>(
          operate<SquareOperation>(
            operate<CosOperation>(
              std::forward<T>(t)
              ))),
        std::forward<DT>(dt));
    }
  };

  template<>
  struct DerivativeTraits<AsinOperation>
  {
    template<class T, class DT, class Functor>
    static constexpr decltype(auto) derivative(T&& t, DT&& dt, Functor&&)
    {
      return componentWiseDerivativeHelper(
        operate<ReciprocalOperation>(
          operate<SqrtOperation>(
            operate<MinusOperation>(
              ones<T>(Disclosure{}),
              operate<SquareOperation>(std::forward<T>(t))
              ))),
        std::forward<DT>(dt)
        );
    }
  };

  template<>
  struct DerivativeTraits<AcosOperation>
  {
    template<class T, class DT, class Functor>
    static constexpr decltype(auto) derivative(T&& t, DT&& dt, Functor&&)
    {
      return componentWiseDerivativeHelper(
        operate<MinusOperation>(
          operate<ReciprocalOperation>(
            operate<SqrtOperation>(
              operate<MinusOperation>(
                ones<T>(Disclosure{}),
                operate<SquareOperation>(std::forward<T>(t))
                )))),
        std::forward<DT>(dt)
        );
    }
  };

  template<>
  struct DerivativeTraits<AtanOperation>
  {
    template<class T, class DT, class Functor>
    static constexpr decltype(auto) derivative(T&& t, DT&& dt, Functor&&)
    {
      return componentWiseDerivativeHelper(
        operate<ReciprocalOperation>(
          operate<PlusOperation>(
            ones<T>(Disclosure{}),
            operate<SquareOperation>(std::forward<T>(t))
            )),
        std::forward<DT>(dt)
        );
    }
  };

  template<>
  struct DerivativeTraits<SinhOperation>
  {
    template<class T, class DT, class Functor>
    static constexpr decltype(auto) derivative(T&& t, DT&& dt, Functor&&)
    {
      return componentWiseDerivativeHelper(
        operate<CoshOperation>(std::forward<T>(t)),
        std::forward<DT>(dt)
        );
    }
  };

  template<>
  struct DerivativeTraits<CoshOperation>
  {
    template<class T, class DT, class Functor>
    static constexpr decltype(auto) derivative(T&& t, DT&& dt, Functor&&)
    {
      return componentWiseDerivativeHelper(
        operate<SinhOperation>(std::forward<T>(t)),
        std::forward<DT>(dt));
    }
  };

  template<>
  struct DerivativeTraits<TanhOperation>
  {
    template<class T, class DT, class Functor>
    static constexpr decltype(auto) derivative(T&& t, DT&& dt, Functor&&)
    {
      return componentWiseDerivativeHelper(
        operate<ReciprocalOperation>(
          operate<SquareOperation>(
            operate<CoshOperation>(
              std::forward<T>(t)
              ))),
          std::forward<DT>(dt));
    }
  };

  template<>
  struct DerivativeTraits<AsinhOperation>
  {
    template<class T, class DT, class Functor>
    static constexpr decltype(auto) derivative(T&& t, DT&& dt, Functor&&)
    {
      // TODO: enforce real type
      return componentWiseDerivativeHelper(
        operate<ReciprocalOperation>(
          operate<SqrtOperation>(
            operate<PlusOperation>(
              operate<SquareOperation>(std::forward<T>(t)),
              ones<T>(Disclosure{})
              ))),
          std::forward<DT>(dt));
    }
  };

  template<>
  struct DerivativeTraits<AcoshOperation>
  {
    template<class T, class DT, class Functor>
    static constexpr decltype(auto) derivative(T&& t, DT&& dt, Functor&&)
    {
      // TODO: enforce real type
      return componentWiseDerivativeHelper(
        operate<ReciprocalOperation>(
          operate<SqrtOperation>(
            operate<MinusOperation>(
              operate<SquareOperation>(std::forward<T>(t)),
              ones<T>(Disclosure{})
              ))),
          std::forward<DT>(dt));
    }
  };

  template<>
  struct DerivativeTraits<AtanhOperation>
  {
    template<class T, class DT, class Functor>
    static constexpr decltype(auto) derivative(T&& t, DT&& dt, Functor&&)
    {
      return componentWiseDerivativeHelper(
        operate<ReciprocalOperation>(
          operate<MinusOperation>(
            ones<T>(Disclosure{}),
            operate<SquareOperation>(std::forward<T>(t))
            )),
        std::forward<DT>(dt));
    }
  };

  template<>
  struct DerivativeTraits<Atan2Operation>
  {
    template<class T1, class DT1, class T2, class DT2, class Functor>
    static constexpr decltype(auto) derivative(T1&& t1, DT1&& dt1, T2&& t2, DT2&& dt2, Functor&&)
    {
      return
        operate<PlusOperation>(
          componentWiseDerivativeHelper(
            operate<TotalProductOperation<T2> >(
              std::forward<T2>(t2),
              operate<ReciprocalOperation>(
                operate<PlusOperation>(
                  operate<SquareOperation>(std::forward<T1>(t1)),
                  operate<SquareOperation>(std::forward<T2>(t2))
                  )
                )
              ),
            std::forward<DT1>(dt1)
            ),
          componentWiseDerivativeHelper(
            operate<TotalProductOperation<T1> >(
              operate<MinusOperation>(std::forward<T1>(t1)),
              operate<ReciprocalOperation>(
                operate<PlusOperation>(
                  operate<SquareOperation>(std::forward<T1>(t1)),
                  operate<SquareOperation>(std::forward<T2>(t2)))
                )
              ),
            std::forward<DT2>(dt2)
            )
          );
    }
  };

  template<class Sig>
  struct DerivativeTraits<ReshapeOperation<Sig> >
  {
    template<class T, class DT, class Functor>
    static constexpr decltype(auto) derivative(T&& t, DT&& dt, Functor&&) {
      constexpr auto tailrank = TensorTraits<DT>::rank - TensorTraits<T>::rank;
      using Signature = SequenceCat<Sig, TailPart<tailrank, typename TensorTraits<DT>::Signature>>;

      return operate<ReshapeOperation<Signature>>(std::forward<DT>(dt));
    }
  };

  template<class Perm>
  struct DerivativeTraits<TransposeOperation<Perm> >
  {
    template<class T, class DT, class Functor>
    static constexpr decltype(auto) derivative(T&& t, DT&& dt, Functor&&)
    {
      using Permutation = ResizedPermutation<Perm, TensorTraits<DT>::rank>;

      return operate<TransposeOperation<Permutation> >(std::forward<DT>(dt));
    }
  };

  /**Compile-time constant variant.*/
  template<class Positions, class Indices>
  struct DerivativeTraits<RestrictionOperation<Positions, Indices> >
  {
    template<class T, class DT, class Functor>
    static constexpr decltype(auto) derivative(T&& t, DT&& dt, const Functor& f)
    {
      return operate<RestrictionOperation<Positions, Indices> >(std::forward<DT>(dt));
    }
  };

  /**Runtime-dynamic variant.*/
  template<std::size_t... Dims>
  struct DerivativeTraits<RestrictionOperation<Tensor::Seq<Dims...>, Tensor::Seq<> > >
  {
    template<class T, class DT, class Functor>
    static constexpr decltype(auto) derivative(T&& t, DT&& dt, const Functor& f)
    {
      return operate(f, std::forward<DT>(dt));
    }
  };

  template<>
  struct DerivativeTraits<PowOperation>
  {
    template<class T1, class DT1, class T2, class DT2, class Functor>
    static constexpr decltype(auto) derivative(T1&& t1, DT1&& dt1, T2&& t2, DT2&& dt2, Functor&&)
    {
      return operate<PlusOperation>(
        componentWiseDerivativeHelper(
          operate<TotalProductOperation<T2> >(
            std::forward<T2>(t2),
            operate<PowOperation>(
              std::forward<T1>(t1),
              operate<MinusOperation>(
                std::forward<T2>(t2),
                asExpression(ones<T2>())
                ))),
        std::forward<DT1>(dt1)),
        componentWiseDerivativeHelper(
          operate<TotalProductOperation<T1> >(
            operate<LogOperation>(std::forward<T1>(t1)),
            operate<PowOperation>(std::forward<T1>(t1), std::forward<T2>(t2))
            ),
          std::forward<DT2>(dt2))
        );
    }
  };

  template<>
  struct DerivativeTraits<PlusOperation>
  {
    template<class T1, class DT1, class T2, class DT2, class Functor>
    static constexpr decltype(auto) derivative(T1&& t1, DT1&& dt1, T2&& t2, DT2&& dt2, Functor&&)
    {
      return operate<PlusOperation>(std::forward<DT1>(dt1), std::forward<DT2>(dt2));
    }
  };

  template<>
  struct DerivativeTraits<MinusOperation>
  {
    template<class T, class DT, class Functor>
    static constexpr decltype(auto) derivative(T&& t, DT&& dt, Functor&&)
    {
      return operate<MinusOperation>(std::forward<DT>(dt));
    }

    template<class T1, class DT1, class T2, class DT2, class Functor,
             std::enable_if_t<(IsTensorOperand<T2>::value
                               && IsTensorOperand<DT2>::value
               ), int> = 0>
    static constexpr decltype(auto) derivative(T1&& t1, DT1&& dt1, T2&& t2, DT2&& dt2, Functor&&)
    {
      return operate<MinusOperation>(std::forward<DT1>(dt1), std::forward<DT2>(dt2));
    }
  };

  template<class Pos1, class Pos2, class Dims>
  struct DerivativeTraits<EinsumOperation<Pos1, Pos2, Dims> >
  {
    template<class T1, class DT1, class T2, class DT2, class Functor>
    static constexpr decltype(auto) derivative(T1&& t1, DT1&& dt1, T2&& t2, DT2&& dt2, Functor&&)
    {
      static_assert(Dims{} == SequenceSlice<typename TensorTraits<T1>::Signature, Pos1>{}
                    &&
                    Dims{} == SequenceSlice<typename TensorTraits<T2>::Signature, Pos2>{},
                    "Mismatching dimensions.");

      constexpr std::size_t rangeRank1 = TensorTraits<T1>::rank;
      constexpr std::size_t derivativeRank1 = TensorTraits<DT1>::rank;
      constexpr std::size_t rangeRank2 = TensorTraits<T2>::rank;
      constexpr std::size_t derivativeRank2 = TensorTraits<DT2>::rank;

      static_assert((derivativeRank2 - rangeRank2) == (derivativeRank1 - rangeRank1),
                    "Inconsistent derivative types.");
      constexpr std::size_t domainRank = derivativeRank1 - rangeRank1;
      constexpr std::size_t defectSize = Dims::size();

      // NB: the values of the permutation are the index positions
      // where the transpose tensor fetches the values from.
      using Perm = SequenceCat<
        // keep range positions of left factor
        MakeIndexSequence<rangeRank1 - defectSize>,
        // fetch index values for derivatives from end of index tuple
        MakeIndexSequence<domainRank, rangeRank1 + rangeRank2 - 2*defectSize>,
        // move indices for right factor left by domainrank
        MakeIndexSequence<rangeRank2 - defectSize, rangeRank1 - defectSize>
        >;

      return
        operate<PlusOperation>(
          operate(
            EinsumFunctor<T1, Pos1, DT2, Pos2>{},
            std::forward<T1>(t1),
            std::forward<DT2>(dt2)
            ),
          operate<TransposeOperation<Perm> >(
            operate(
              EinsumFunctor<DT1, Pos1, T2, Pos2>{},
              std::forward<DT1>(dt1),
              std::forward<T2>(t2)
              )
            )
          );
    }
  }; // traits class

  template<class Pos1, class Pos2, class Dims>
  struct DerivativeTraits<TensorProductOperation<Pos1, Pos2, Dims> >
  {
    template<class T1, class DT1, class T2, class DT2, class Functor>
    static constexpr decltype(auto) derivative(T1&& t1, DT1&& dt1, T2&& t2, DT2&& dt2, Functor&&)
    {
      static_assert(Dims{} == SequenceSlice<typename TensorTraits<T1>::Signature, Pos1>{}
                    &&
                    Dims{} == SequenceSlice<typename TensorTraits<T2>::Signature, Pos2>{},
                    "Mismatching dimensions.");

      constexpr std::size_t rangeRank1 = TensorTraits<T1>::rank;
      constexpr std::size_t derivativeRank1 = TensorTraits<DT1>::rank;
      constexpr std::size_t rangeRank2 = TensorTraits<T2>::rank;
      constexpr std::size_t derivativeRank2 = TensorTraits<DT2>::rank;

      static_assert((derivativeRank2 - rangeRank2) == (derivativeRank1 - rangeRank1),
                    "Inconsistent derivative types.");

      constexpr std::size_t domainRank = derivativeRank1 - rangeRank1;
      constexpr std::size_t defectSize = Dims::size();

      // NB: the values of the permutation are the index positions
      // where the transpose tensor fetches the values from.
      // This is the "extended tensor product" case. The defect
      // positions are added to the front of the product tensor.
      using Perm = SequenceCat<
        // keep range positions of left factor and defect positions.
        MakeIndexSequence<rangeRank1>,
        // fetch index values for derivatives from end of index tuple
        MakeIndexSequence<domainRank, rangeRank1 + rangeRank2 - defectSize>,
        // move indices for right factor left by domainrank
        MakeIndexSequence<rangeRank2 - defectSize, rangeRank1>
        >;

      return
        operate<PlusOperation>(
          operate(
            ProductFunctor<T1, Pos1, DT2, Pos2>{},
            std::forward<T1>(t1),
            std::forward<DT2>(dt2)
            ),
          // The situtation is as follows: after applying sort-of a
          // productrule we end up with a tensor signature as follows
          //
          // T1Rest | DT1Ind | T2Rest
          //
          // where T1Rest are the remaining indices of T1 after
          // Einstein-summation, T2Rest likewise, DT1Ind denotes the
          // new dimensions added by the partial derivatives of T1.
          //
          // The desired ordering is
          //
          // T1Rest | T2Rest | DT1Ind
          //
          // as our convention is to add all indices added by partial
          // differentiation are added at the end.
          //
          // The transposition can be expressed by a contraction with
          // a suitable unity-tensor
          operate<TransposeOperation<Perm> >(
            operate(
              ProductFunctor<DT1, Pos1, T2, Pos2>{},
              std::forward<DT1>(dt1),
              std::forward<T2>(t2)
              )
            )
          );
    }
  }; // traits class

} // NS

#endif // __DUNE_ACFEM_TENSORS_OPERATIONS_DERIVATIVETRAITS_HH__
