#ifndef __DUNE_ACFEM_TENSORS_OPERATIONS_ASSUME_HH__
#define __DUNE_ACFEM_TENSORS_OPERATIONS_ASSUME_HH__

#include "../../expressions/optimizationbase.hh"
#include "../bindings.hh"
#include "../tensorbase.hh"
#include "../bindings.hh"
#include "../modules/eye.hh"

namespace Dune {

  namespace ACFem {

    namespace Tensor {

      /**Generate an assume expression attaching properties to a given
       * expression.
       */
      template<class... Property, class T, std::enable_if_t<IsProperTensor<T>::value, int> = 0>
      constexpr decltype(auto) assume(T&& t, MPL::TagContainer<Property...> = MPL::TagContainer<Property...>{})
      {
        if constexpr (sizeof...(Property) == 0) {
          return expressionClosure(std::forward<T>(t));
        } else if constexpr (!std::is_same<MPL::TagContainer<Property...>, MPL::UniqueTags<Property...> >::value) {
          return assume(std::forward<T>(t), MPL::UniqueTags<Property...>{});
        } else {
          return Expressions::finalize<AssumeOperation<Property...> >(std::forward<T>(t));
        }
      }

      /**The type of the associated assume.*/
      template<class T, class... Properties>
      using AssumeTensor = Expressions::EnclosedType<std::decay_t<decltype(assume<Properties...>(std::declval<T>()))> >;

    } // NS Tensor

    using Tensor::assume;

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_TENSORS_OPERATIONS_ASSUME_HH__
