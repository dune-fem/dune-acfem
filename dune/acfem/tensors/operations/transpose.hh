#ifndef __DUNE_ACFEM_TENSORS_OPERATIONS_TRANSPOSE_HH__
#define __DUNE_ACFEM_TENSORS_OPERATIONS_TRANSPOSE_HH__

#include "../../expressions/storage.hh"
#include "../../mpl/permutation.hh"
#include "../../mpl/sort.hh"
#include "../tensorbase.hh"
#include "../modules.hh"

namespace Dune {

  namespace ACFem {

    // forward
    template<class Perm>
    struct TransposeOperation;

    /**@addtogroup Tensors
     * @{
     */

    /**@addtogroup TensorOperations
     * @{
     */

    namespace Tensor {

      template<class Tensor, class Perm>
      class Transposition;

      /**A class applying a transposition to the given tensor's
       * indices. A "transposition" here refers to a permutation of
       * the positions of the indices of the tensors, as
       * generalization of a matrix-transposition which exchanges the
       * position of the first and the second index.
       *
       * @param Tensor The tensor the transposition is applied to.
       *
       * @param Perm The permutation. The number of permutation
       * indices must equal the rank of the tensor. The permutation
       * (Pk,...) is applied as follows to a tensor A:
       *
       * @f[
       * tr<P_0,\dots,P_k>(A)_{i_0,\dots,i_k} = A_{i_{P_0},\dots,i_{P_k}}
       * @f]
       *
       * The Transposition class just permutes the indices and
       * forwards everything to the underlying tensor.
       */
      template<class Tensor, std::size_t... Perm>
      class Transposition<Tensor, Seq<Perm...> >
        : public TensorBase<typename TensorTraits<Tensor>::FieldType,
                            Seq<Get<Perm, typename TensorTraits<Tensor>::Signature>::value...>,
                            Transposition<Tensor, Seq<Perm...> > >
        , public Expressions::Storage<OperationTraits<TransposeOperation<Seq<Perm...> > >, Tensor>
      {
        static_assert(sizeof...(Perm) == TensorTraits<Tensor>::rank,
                      "A transposition must be provided for all tensor dimensions.");

        using ArgType = Tensor;
        using HostType = std::decay_t<Tensor>;
        using BaseType = TensorBase<typename HostType::FieldType,
                                    Seq<Get<Perm, typename TensorTraits<Tensor>::Signature>::value...>,
                                    Transposition<Tensor, Seq<Perm...> > >;
        using StorageType = Expressions::Storage<OperationTraits<TransposeOperation<Seq<Perm...> > >, Tensor>;
      public:
        using Permutation = Seq<Perm...>;
        using StorageType::operation;
        using StorageType::operand;
        using BaseType::rank;
        using typename BaseType::Signature;
        using typename BaseType::FieldType;

       public:
        /**Constructor from a given host-tensor.
         */
        template<class Arg, std::enable_if_t<std::is_constructible<ArgType, Arg>::value, int> = 0>
        Transposition(Arg&& host)
          : StorageType(std::forward<Arg>(host))
        {}

        /**Allow default construction if contained types fulfill IsTypedValue.*/
        template<
          class... Dummy,
          std::enable_if_t<(sizeof...(Dummy) == 0
                            && IsTypedValue<ArgType>::value), int> = 0>
        Transposition(Dummy&&...)
          : StorageType(ArgType{})
        {}

        /**Insert the current view-indices at their proper positions and
         * foward to the underlying "host" tensor.
         */
        template<class... Dims,
                 std::enable_if_t<(sizeof...(Dims) == rank
                                   &&
                                   IsIntegralPack<Dims...>::value)
                                  , int> = 0>
        decltype(auto) operator()(Dims... indices)
        {
          return operand(0_c)(std::get<Perm>(std::forward_as_tuple(indices...))...);
        }

        /**Insert the current view-indices at their proper positions and
         * foward to the underlying "host" tensor.
         */
        template<class... Dims,
                 std::enable_if_t<(sizeof...(Dims) == rank
                                   &&
                                   IsIntegralPack<Dims...>::value)
                                  , int> = 0>
        decltype(auto) operator()(Dims... indices) const
        {
          return operand(0_c)(std::get<Perm>(std::forward_as_tuple(indices...))...);
        }

        template<std::size_t... Indices, std::enable_if_t<sizeof...(Indices) == rank, int> = 0>
        decltype(auto) constexpr operator()(Seq<Indices...>) const
        {
          return operand(0_c)(Seq<Get<Perm, Seq<Indices...> >::value...>{});
        }

        template<std::size_t... Indices, class Pos = MakeIndexSequence<sizeof...(Indices)> >
        static bool constexpr isZero(Seq<Indices...> = Seq<Indices...>{}, Pos = Pos{})
        {
          // Truncate Pos to size of Indices...
          using RealPos = HeadPart<sizeof...(Indices), Pos>;

          // Map to the transposed positions.
          using TransposedPos = PermuteSequenceValues<RealPos, Seq<Perm...> >;

          using SortedPos = typename SortSequence<TransposedPos>::Result;
          using Permutation = typename SortSequence<TransposedPos>::Permutation;
          using SortedIndices = PermuteSequence<Seq<Indices...>, Permutation>;

          return HostType::isZero(SortedIndices{}, SortedPos{});
        }

        std::string name() const
        {
          using T = Tensor;
          std::string pfx = std::is_reference<T>::value ? (RefersConst<T>::value ? "cref" : "ref") : "";
          return operationName(operation(), pfx+operand(0_c).name());
        }

      };

      /**Traits in order to identify a TensorView class.*/
      template<class T>
      struct IsTransposition
        : FalseType
      {};

      template<class T, std::size_t... Perm>
      struct IsTransposition<Transposition<T, Seq<Perm...> > >
        : TrueType
      {};

      /**Generator function for Transposition class.*/
      template<std::size_t... Perm, class T,
               std::enable_if_t<(IsProperTensor<T>::value
                                 && isPermutation(ResizedPermutation<Seq<Perm...>, TensorTraits<T>::rank>{})
                                 && sizeof...(Perm) <= TensorTraits<T>::rank
        ), int> = 0>
      constexpr decltype(auto) transpose(T&& t, Seq<Perm...> = Seq<Perm...>{})
      {
        using Operation = TransposeOperation<ResizedPermutation<Seq<Perm...>, TensorTraits<T>::rank> >;

        return Expressions::finalize<Operation>(std::forward<T>(t));
      }

      /**@internal Return the tensor resulting from the given transposition operation.
       */
      template<class Perm, class T, std::enable_if_t<Perm::size() == TensorTraits<T>::rank, int> = 0>
      constexpr auto operate(Expressions::DontOptimize, OperationTraits<TransposeOperation<Perm> >, T&& t)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        return Transposition<T, Perm>(std::forward<T>(t));
      }

    } // NS Tensor

    //using Tensor::transpose;

  } // NS ACFem

  template<class T, class P>
  struct FieldTraits<ACFem::Tensor::Transposition<T, P> >
    : FieldTraits<std::decay_t<T> >
  {};

  ///@} TensorOperations

  ///@} Tensors

} // NS Dune

#endif // __DUNE_ACFEM_TENSORS_OPERATIONS_TRANSPOSE_HH__
