#ifndef __DUNE_ACFEM_TENSORS_OPERATIONS_RECIPROCAL_HH__
#define __DUNE_ACFEM_TENSORS_OPERATIONS_RECIPROCAL_HH__

#include "../../expressions/optimizationbase.hh"
#include "../tensorbase.hh"
#include "../expressiontraits.hh"

namespace Dune {

  namespace ACFem {

    namespace Tensor {

      /**Generate the reciprocal of a scalar tensor.*/
      template<class T, std::enable_if_t<IsUnaryTensorOperand<T>::value, int> = 0>
      constexpr decltype(auto) reciprocal(T&& t)
      {
        return finalize<ReciprocalOperation>(std::forward<T>(t));
      }

      /**The type of the associated reciprocal.*/
      template<class T>
      using ReciprocalTensor = Expressions::EnclosedType<std::decay_t<decltype(reciprocal(std::declval<T>()))> >;

    } // NS Tensor

    using Tensor::reciprocal;

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_TENSORS_OPERATIONS_RECIPROCAL_HH__
