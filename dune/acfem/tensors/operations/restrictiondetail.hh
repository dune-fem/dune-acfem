#ifndef __DUNE_ACFEM_TENSORS_OPERATIONS_RESTRICTIONDETAIL_HH__
#define __DUNE_ACFEM_TENSORS_OPERATIONS_RESTRICTIONDETAIL_HH__

#include "../tensorbase.hh"

namespace Dune {

  namespace ACFem {

    namespace Tensor {

      namespace {
        // Helpers for joining tensor Restrictions
        //
        // Could most probably formulated in a more compact way. The
        // helper classes iterate over the index positions. We have two
        // index sets: the old "defect indices", marking fixed indices
        // at the given positions, and the new "defect indices" which
        // mark fixed indices relative to the new index positions after
        // removing the indices fixed by the "old defects".

        template<std::size_t NextIndex,
                 std::size_t HoleCount,
                 class JoinedDefects,
                 class InjectAt,
                 class NewJoinedDefect,
                 class OldDefects,
                 class NewDefects,
                 class SFINAE = void>
        struct DefectIndexJoin;


        // No match, just continue, input data still available
        template<std::size_t N, std::size_t H,
                 std::size_t... JoinedDefects,
                 std::size_t... InjectAt,
                 std::size_t... NewJoinedDefects,
                 std::size_t First, std::size_t... Rest,
                 std::size_t NewFirst, std::size_t... NewRest>
        struct DefectIndexJoin<N, H,
                               Seq<JoinedDefects...>,
                               Seq<InjectAt...>,
                               Seq<NewJoinedDefects...>,
                               Seq<First, Rest...>,
                               Seq<NewFirst, NewRest...>,
                               std::enable_if_t<(N < First
                                                 &&
                                                 H < NewFirst)> >
        {
          using Child = DefectIndexJoin<N+1, H+1,
                                        Seq<JoinedDefects...>,
                                        Seq<InjectAt...>,
                                        Seq<NewJoinedDefects...>,
                                        Seq<First, Rest...>,
                                        Seq<NewFirst, NewRest...> >;
          using Type = typename Child::Type;
          using Inject = typename Child::Inject;
          using New = typename Child::New;
        };

        // No match, just continue, input data exhausted.
        template<std::size_t N, std::size_t H,
                 std::size_t... JoinedDefects,
                 std::size_t... InjectAt,
                 std::size_t... NewJoinedDefects,
                 std::size_t First, std::size_t... Rest>
        struct DefectIndexJoin<N, H,
                               Seq<JoinedDefects...>,
                               Seq<InjectAt...>,
                               Seq<NewJoinedDefects...>,
                               Seq<First, Rest...>,
                               Seq<>,
                               std::enable_if_t<(N < First)> >
        {
          using Child = DefectIndexJoin<N+1, H+1,
                                        Seq<JoinedDefects...>,
                                        Seq<InjectAt...>,
                                        Seq<NewJoinedDefects...>,
                                        Seq<First, Rest...>,
                                        Seq<> >;
          using Type = typename Child::Type;
          using Inject = typename Child::Inject;
          using New = typename Child::New;
        };

        // Move past current index and keep hole-count
        template<std::size_t N, std::size_t H,
                 std::size_t... JoinedDefects,
                 std::size_t... InjectAt,
                 std::size_t... NewJoinedDefects,
                 std::size_t First, std::size_t... Rest,
                 std::size_t... NewRest>
        struct DefectIndexJoin<N, H,
                               Seq<JoinedDefects...>,
                               Seq<InjectAt...>,
                               Seq<NewJoinedDefects...>,
                               Seq<First, Rest...>,
                               Seq<NewRest...>,
                               std::enable_if_t<(N == First)> >
        {
          using Child =
            DefectIndexJoin<N+1, H,
                            Seq<JoinedDefects..., First>,
                            Seq<InjectAt...>,
                            Seq<NewJoinedDefects...>,
                            Seq<Rest...>,
                            Seq<NewRest...> >;
          using Type = typename Child::Type;
          using Inject = typename Child::Inject;
          using New = typename Child::New;
        };

        // New index match
        template<std::size_t N, std::size_t H,
                 std::size_t... JoinedDefects,
                 std::size_t... InjectAt,
                 std::size_t... NewJoinedDefects,
                 std::size_t First, std::size_t... Rest,
                 std::size_t NewFirst, std::size_t... NewRest>
        struct DefectIndexJoin<N, H,
                               Seq<JoinedDefects...>,
                               Seq<InjectAt...>,
                               Seq<NewJoinedDefects...>,
                               Seq<First, Rest...>,
                               Seq<NewFirst, NewRest...>,
                               std::enable_if_t<(N < First
                                                 &&
                                                 H == NewFirst)> >
        {
          using Child =
            DefectIndexJoin<N+1, H+1,
                            Seq<JoinedDefects..., N>,
                            Seq<InjectAt..., sizeof...(JoinedDefects)>,
                            Seq<NewJoinedDefects..., N>,
                            Seq<First, Rest...>,
                            Seq<NewRest...> >;
          using Type = typename Child::Type;
          using Inject = typename Child::Inject;
          using New = typename Child::New;
        };

        // New index match with input data exhausted.
        template<std::size_t N, std::size_t H,
                 std::size_t... JoinedDefects,
                 std::size_t... InjectAt,
                 std::size_t... NewJoinedDefects,
                 std::size_t NewFirst, std::size_t... NewRest>
        struct DefectIndexJoin<N, H,
                               Seq<JoinedDefects...>,
                               Seq<InjectAt...>,
                               Seq<NewJoinedDefects...>,
                               Seq<>,
                               Seq<NewFirst, NewRest...>,
                               std::enable_if_t<H == NewFirst> >
        {
          using Child =
            DefectIndexJoin<N+1, H+1,
                            Seq<JoinedDefects..., N>,
                            Seq<InjectAt..., sizeof...(JoinedDefects)>,
                            Seq<NewJoinedDefects..., N>,
                            Seq<>,
                            Seq<NewRest...> >;
          using Type = typename Child::Type;
          using Inject = typename Child::Inject;
          using New = typename Child::New;
        };

        // No match with input data exhausted
        template<std::size_t N, std::size_t H,
                 std::size_t... JoinedDefects,
                 std::size_t... InjectAt,
                 std::size_t... NewJoinedDefects,
                 std::size_t NewFirst, std::size_t... NewRest>
        struct DefectIndexJoin<N, H,
                               Seq<JoinedDefects...>,
                               Seq<InjectAt...>,
                               Seq<NewJoinedDefects...>,
                               Seq<>,
                               Seq<NewFirst, NewRest...>,
                               std::enable_if_t<(H < NewFirst)> >
        {
          using Child =
            DefectIndexJoin<N+1, H+1,
                            Seq<JoinedDefects...>,
                            Seq<InjectAt...>,
                            Seq<NewJoinedDefects...>,
                            Seq<>,
                            Seq<NewFirst, NewRest...> >;
          using Type = typename Child::Type;
          using Inject = typename Child::Inject;
          using New = typename Child::New;
        };

        /**@internal Recursion endpoint.*/
        template<std::size_t N, std::size_t H,
                 std::size_t... JoinedDefects,
                 std::size_t... InjectAt,
                 std::size_t... NewJoinedDefects>
        struct DefectIndexJoin<N, H,
                               Seq<JoinedDefects...>,
                               Seq<InjectAt...>,
                               Seq<NewJoinedDefects...>,
                               Seq<>, Seq<> >
        {
          using Type = Seq<JoinedDefects...>;
          using Inject = Seq<InjectAt...>;
          using New = Seq<NewJoinedDefects...>;
        };

      } // NS anonymous

      /**@internal
       *
       * @param Defects An index set marking fixed indices at the designated
       * positions.
       *
       * @param NewDefects An index set marking fixed indices where
       * the index positions are relative to the numbering after
       * removing the indices marked by the "original" defect indices
       * specified by @a Defects.
       *
       * @return A single index sequence marking all fixed indices
       * relative to the original tensor signature.
       */
      template<class Defects, class NewDefects>
      using JoinedDefects = typename DefectIndexJoin<0UL, 0UL, Seq<>, Seq<>, Seq<>, Defects, NewDefects>::Type;

      /**@internal Only the new defect positions, see JoinedDefects.*/
      template<class Defects, class NewDefects>
      using NewJoinedDefects = typename DefectIndexJoin<0UL, 0UL, Seq<>, Seq<>, Seq<>, Defects, NewDefects>::New;

      /**@internal See also JoinedDefects. This class-template gives
       * the index sequence of positions in the "defect" index tuple
       * relative to the original @a Defects indices where the indices
       * corresponding to @a NewDefects need to be inserted. See also
       * Restriction::lookAt().
       */
      template<class Defects, class NewDefects>
      using JoinedInjections = typename DefectIndexJoin<0UL, 0UL, Seq<>, Seq<>, Seq<>, Defects, NewDefects>::Inject;

    } // NS Tensor

  }  // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_TENSORS_OPERATIONS_RESTRICTIONSDETAIL_HH__
