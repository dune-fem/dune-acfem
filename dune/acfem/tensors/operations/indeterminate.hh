#ifndef __DUNE_ACFEM_TENSORS_OPERATIONS_INDETERMINATE_HH__
#define __DUNE_ACFEM_TENSORS_OPERATIONS_INDETERMINATE_HH__

#include "../../expressions/optimizationbase.hh"
#include "../bindings.hh"
#include "../tensorbase.hh"
#include "../bindings.hh"
#include "../modules/eye.hh"

namespace Dune {

  namespace ACFem {

    namespace Tensor {

      /**Generate an indeterminate tensor wrappinp storage for a T.*/
      template<std::size_t Id, class T, std::enable_if_t<IsProperTensor<T>::value, int> = 0>
      auto indeterminate(T&& t, IndexConstant<Id> = IndexConstant<Id>{})
      {
        return finalize(
          Expressions::DontOptimize{},
          OperationTraits<IndeterminateOperation<Id> >{},
          std::forward<T>(t)
          //Expressions::DynamicStorageType<T>(std::forward<T>(t))
          );
      }

      /**Generate an indeterminate tensor wrapping an eye tensor of the given signature.*/
      template<std::size_t Id, std::size_t... Dims>
      auto indeterminate(Seq<Dims...> = Seq<Dims...>{}, IndexConstant<Id> = IndexConstant<Id>{})
      {
        return finalize(
          Expressions::DontOptimize{},
          OperationTraits<IndeterminateOperation<Id> >{},
          eye<Dims...>(Disclosure{}));
      }

      /**The type of the associated indeterminate.*/
      template<std::size_t Id, class T>
      using IndeterminateTensor = Expressions::EnclosedType<std::decay_t<decltype(indeterminate<Id>(std::declval<T>()))> >;

    } // NS Tensor

    using Tensor::indeterminate;

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_TENSORS_OPERATIONS_INDETERMINATE_HH__
