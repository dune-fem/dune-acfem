#ifndef __DUNE_ACFEM_TENSORS_ASSIGN_HH__
#define __DUNE_ACFEM_TENSORS_ASSIGN_HH__

#include "../../mpl/foreach.hh"
#include "../tensorbase.hh"
#include "../optimization/policy.hh"

namespace Dune {

  namespace ACFem {

    namespace Tensor {

      /**General assign() functionality with a supplied function in
       * order to implement operator=() variants.
       *
       * @param[out] dst Destination tensor.
       *
       * @param[in] src Source tensor.
       *
       * @param[in] f Assignment functor. Called as f(dst(...),
       * src(...)) for each particular tensor component in turn. A
       * simple operator=() can be implemented using
       *
       * @code
       * [](auto& dst, auto&& src) { dst = src; }
       * @endcode
       *
       * @return @a dst.
       */
      template<class Dst, class Src, class F,
               std::enable_if_t<(IsTensor<Dst>::value
                                 && IsTensor<Src>::value
                                 && TensorTraits<Dst>::rank*TensorTraits<Src>::rank > 0
                                 && TensorTraits<Dst>::dim() <= Policy::TemplateForEachLimit::value
        ), int> = 0>
      Dst& assign(Dst& dst, Src&& src, F&& f)
      {
        static_assert(Dst::signature() == TensorTraits<Src>::signature(),
                      "Tensors must have the same signature.");
        static_assert(TensorTraits<Dst>::hasMutableComponents,
                      "Destination tensor must be component-wise assignable.");

        forLoop<Dst::dim()>([&] (auto i) {
            using I = decltype(i);
            using Index = MultiIndex<I::value, typename Dst::Signature>;
            //if (false && std::decay_t<Src>::isZero(Index{})) {
            //  f(tensorValue(dst, Index{}), (typename Dst::FieldType)0);
            //} else {
              f(tensorValue(dst, Index{}), tensorValue(std::forward<Src>(src), Index{}));
            //}
          });
        return dst;
      }

      /**General assign() functionality with a supplied function in
       * order to implement operator=() variants.
       *
       * @param[out] dst Destination tensor.
       *
       * @param[in] src Source tensor.
       *
       * @param[in] f Assignment functor. Called as f(dst(...),
       * src(...)) for each particular tensor component in turn. A
       * simple operator=() can be implemented using
       *
       * @code
       * [](auto& dst, auto&& src) { dst = src; }
       * @endcode
       *
       * @return @a dst.
       */
      template<class Dst, class Src, class F,
               std::enable_if_t<(IsTensor<Dst>::value
                                 && IsTensor<Src>::value
                                 && TensorTraits<Dst>::rank*TensorTraits<Src>::rank > 0
                                 && TensorTraits<Dst>::dim() > Policy::TemplateForEachLimit::value
        ), int> = 0>
      Dst& assign(Dst& dst, Src&& src, F&& f)
      {
        static_assert(Dst::signature() == TensorTraits<Src>::signature(),
                      "Tensors must have the same signature.");
        static_assert(TensorTraits<Dst>::hasMutableComponents,
                      "Destination tensor must be component-wise assignable.");

        for (std::size_t i = 0; i < Dst::dim(); ++i) {
          auto index = multiIndex(i, Dst::signature());
          f(tensorValue(dst, index), tensorValue(std::forward<Src>(src), index));
        }
        return dst;
      }

      /**Assign from rank-0. */
      template<class Dst, class Src, class F,
               std::enable_if_t<(IsTensor<Dst>::value
                                 && IsTensor<Src>::value
                                 && TensorTraits<Dst>::rank > 0
                                 && TensorTraits<Src>::rank == 0
                                 && TensorTraits<Dst>::dim() <= Policy::TemplateForEachLimit::value
                 ), int> = 0>
      Dst& assign(Dst& dst, Src&& src, F&& f)
      {
        static_assert(TensorTraits<Dst>::hasMutableComponents,
                      "Destination tensor must be component-wise assignable.");

        decltype(auto) value = std::forward<Src>(src)();
        forLoop<Dst::dim()>([&] (auto i) {
            using IType = decltype(i);
            using Index = MultiIndex<IType::value, typename Dst::Signature>;
            f(tensorValue(dst, Index{}), value);
          });
        return dst;
      }

      /**Assign from rank-0. */
      template<class Dst, class Src, class F,
               std::enable_if_t<(IsTensor<Dst>::value
                                 && IsTensor<Src>::value
                                 && TensorTraits<Dst>::rank > 0
                                 && TensorTraits<Src>::rank == 0
                                 && TensorTraits<Dst>::dim() > Policy::TemplateForEachLimit::value
                 ), int> = 0>
      Dst& assign(Dst& dst, Src&& src, F&& f)
      {
        static_assert(TensorTraits<Dst>::hasMutableComponents,
                      "Destination tensor must be component-wise assignable.");

        decltype(auto) value = std::forward<Src>(src)();
        for(std::size_t i; i < Dst::dim(); ++i) {
          auto index = multiIndex(i, Dst::signature());
          f(tensorValue(dst, index), value);
        }
        return dst;
      }

      /**See assign(). This version is the specialization for empty-rank
       * tensors which are allowed and used to embed scalars into the
       * tensor stuff.
       */
      template<class Dst, class Src, class F,
               std::enable_if_t<(IsTensor<Dst>::value
                                 && IsTensor<Src>::value
                                 && TensorTraits<Dst>::rank == 0
                                 && TensorTraits<Src>::rank == 0)
                              , int> = 0>
      Dst& assign(Dst& dst, Src&& src, F&& f)
      {
        f(dst(), src());
        return dst;
      }

    } // NS Tensor

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_TENSORS_ASSIGN_HH__
