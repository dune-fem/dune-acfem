#ifndef __DUNE_ACFEM_TENSORS_OPERATIONS_RESTRICTIONOPERATORS_HH__
#define __DUNE_ACFEM_TENSORS_OPERATIONS_RESTRICTIONOPERATORS_HH__

#include "../../common/types.hh"
#include "../../expressions/optimizationbase.hh"

namespace Dune {

  namespace ACFem {

    template<class Dims, class Pivot>
    struct RestrictionOperation;

    namespace Tensor {

      using namespace Expressions;

      /**Shortcut for an index-sequence, short three-letter abbreviation
       * as this is needed frequently.
       */
      template<std::size_t... Ints>
      using Seq = IndexSequence<Ints...>;

      // forward
      template<class Tensor, class Dims, class Indices>
      class Restriction;

      template<class T>
      class RestrictionOperators
      {
       public:

        /**Implement kind of a nested vector interface via operator[].*/
        template<std::size_t I>
        auto operator[](IndexConstant<I>)&&
        {
          return finalize<RestrictionOperation<Seq<0>, Seq<I> > >(std::forward<T&&>(static_cast<T&&>(*this)));
        }

        /**Implement kind of a nested vector interface via operator[].*/
        template<std::size_t I>
        auto operator[](IndexConstant<I>)&
        {
          return finalize<RestrictionOperation<Seq<0>, Seq<I> > >(static_cast<T&>(*this));
        }

        /**Implement kind of a nested vector interface via operator[].*/
        template<std::size_t I>
        auto operator[](IndexConstant<I>)const&
        {
          return finalize<RestrictionOperation<Seq<0>, Seq<I> > >(static_cast<const T&>(*this));
        }
      };

    } // NS Tensor

  } // NS ACFem

} // Dune

#endif // __DUNE_ACFEM_TENSORS_OPERATIONS_RESTRICTIONOPERATORS_HH__
