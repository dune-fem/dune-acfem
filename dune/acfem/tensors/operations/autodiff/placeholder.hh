#ifndef __DUNE_ACFEM_TENSORS_OPERATIONS_AUTODIFF_PLACEHOLDER_HH__
#define __DUNE_ACFEM_TENSORS_OPERATIONS_AUTODIFF_PLACEHOLDER_HH__

#include "../indeterminate.hh"
#include "../placeholder.hh"
#include "placeholdertraits.hh"

namespace Dune {

  namespace ACFem {

    namespace Tensor {

      template<std::size_t Id, class Signature, class T, class DT>
      class AutoDiffPlaceholder
        : public PlaceholderTensor<T, AutoDiffPlaceholder<Id, Signature, T, DT> >
      {
        using ThisType = AutoDiffPlaceholder;
        using BaseType = PlaceholderTensor<T, ThisType>;
        using DerivativeStorage = Expressions::Storage<OperationTraits<IdentityOperation>, DT>;
       public:
        using TensorType = T;
        using DerivativeType = DT;
        static constexpr std::size_t id_ = Id;
        using IndeterminateSignature = Signature;
        using BaseType::operand;

        AutoDiffPlaceholder()
          : BaseType(T{}, Expressions::Functor<BaseType>{}), dt_(DT{})
        {}

        AutoDiffPlaceholder(T&& t)
          : BaseType(std::forward<T>(t), Expressions::Functor<BaseType>{}), dt_(DT{})
        {}

        AutoDiffPlaceholder(T&& t, DT&& dt)
          : BaseType(std::forward<T>(t), Expressions::Functor<BaseType>{}),
            dt_(std::forward<DT>(dt))
        {}

        AutoDiffPlaceholder(const ThisType& other)
          : BaseType(other), dt_(other.dt_)
        {}

        AutoDiffPlaceholder(ThisType&& other)
          : BaseType(std::forward<ThisType>(other)), dt_(std::move(other.dt_))
        {}

        ThisType& operator=(const ThisType& other)
        {
          operand(0_c) = other.operand(0_c);
          if constexpr (std::is_assignable<DT, DT>::value) {
            dt_ = other.dt_;
          }
          return *this;
        }

        template<
          class P,
          std::enable_if_t<(!IsAutoDiffPlaceholder<P>::value
                            && std::is_assignable<T, P>::value
          ), int> = 0>
        ThisType& operator=(P&& other)
        {
          operand(0_c) = asExpression(std::forward<P>(other));
          return *this;
        }

        template<
          class P,
          std::enable_if_t<(IsAutoDiffPlaceholder<P>::value
                            && std::is_same<typename AutoDiffTraits<P>::ValuesType, T>::value
                            && std::is_same<typename AutoDiffTraits<P>::IndeterminateSignature, Signature>::value
                            && AutoDiffTraits<P>::id_ == Id
          ), int> = 0>
        ThisType& operator=(P&& other)
        {
          operand(0_c) = std::forward<P>(other).operand(0_c);
          if constexpr (std::is_assignable<DerivativeType, typename std::decay_t<P>::DerivativeType>::value) {
            derivative() = std::forward<P>(other).derivative(); // recurse
          }
          return *this;
        }

        std::string name() const
        {
          return "{ " + operand(0_c).name() + ", " + dt_.operand(0_c).name() + " }";
        }

        DT derivative()&&
        {
          return dt_.operand(0_c);
        }

        decltype(auto) derivative()&
        {
          return dt_.operand(0_c);
        }

        decltype(auto) derivative()const&
        {
          return dt_.operand(0_c);
        }

        constexpr auto indeterminate() const
        {
          return Tensor::indeterminate<id_>(IndeterminateSignature{});
        }

       protected:
        DerivativeStorage dt_;
      };

      /**Return the cached derivative if the id matches.*/
      template<
        std::size_t Id, class Signature, class T,
        std::enable_if_t<(Id == std::decay_t<T>::id_
                          && IsAutoDiffPlaceholder<T>::value
        ), int> = 0>
      constexpr decltype(auto) doDerivative(T&& t, Dune::PriorityTag<1>)
      {
        static_assert(std::is_same<Signature, typename std::decay_t<T>::IndeterminateSignature>{},
                      "Inconsistent indeterminate signature.");
        return std::forward<T>(t).derivative();
      }

      /**Return zero if the id does not match.*/
      template<
        std::size_t Id, class Signature, class T,
        std::enable_if_t<(Id != std::decay_t<T>::id_
                          && IsAutoDiffPlaceholder<T>::value
        ), int> = 0>
      constexpr auto doDerivative(T&& t, Dune::PriorityTag<1>)
      {
        using DerivativeSignature = SequenceCat<typename TensorTraits<T>::Signature, Signature>;
        return zeros(DerivativeSignature{});
      }

      template<std::size_t Id, class Signature, class T, class DT, class Evaluate = TrueType,
               std::enable_if_t<(IsTensor<T>::value && IsTensor<DT>::value && !IsClosure<T>::value && !IsClosure<DT>::value), int> = 0>
      auto autoDiffPlaceholder(T&& t, DT&& dt)
      {
        return AutoDiffPlaceholder<Id, Signature, T, DT>(std::forward<T>(t), std::forward<DT>(dt));
      }

      template<std::size_t Id, class Signature, class T, class DT,
               class Evaluate = TrueType,
               std::enable_if_t<!(IsTensor<T>::value && IsTensor<DT>::value && !IsClosure<T>::value && !IsClosure<DT>::value), int> = 0>
      auto autoDiffPlaceholder(T&& t, DT&& dt)
      {
        return autoDiffPlaceholder<Id, Signature>(tensor(std::forward<T>(t)), tensor(std::forward<DT>(dt)));
      }

      template<class T, std::enable_if_t<IsAutoDiffPlaceholder<Expressions::EnclosedType<T> >::value, int> = 0>
      constexpr auto indeterminate(T&&)
      {
        using Traits = AutoDiffTraits<Expressions::EnclosedType<T> >;
        return indeterminate<Traits::id_>(typename Traits::IndeterminateSignature{});
      }

    } // NS Tensor

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_TENSORS_OPERATIONS_AUTODIFF_PLACEHOLDER_HH__
