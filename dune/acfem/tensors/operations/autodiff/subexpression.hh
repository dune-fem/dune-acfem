#ifndef __DUNE_ACFEM_TENSORS_OPERATIONS_AUTODIFF_SUBEXPRESSION_HH__
#define __DUNE_ACFEM_TENSORS_OPERATIONS_AUTODIFF_SUBEXPRESSION_HH__

// subexpression helpers

#include "fad.hh"
// #include "bad.hh" if ever

namespace Dune {

  namespace ACFem {

    namespace Tensor {

      // forward
      template<class FadOperation>
      struct EvalSExpFAD;

      /**Functor implementation for evaluateSubExpression() which does
       * forward autodiff.
       */
      template<bool Evaluate, std::size_t... Id, class... Signature>
      struct EvalSExpFAD<OperateFAD<Evaluate, Seq<Id...>, Signature...> >
      {
        using FadOperationType = OperateFAD<Evaluate, Seq<Id...>, Signature...>;

        template<class Src>
        constexpr decltype(auto) operator()(Src&& src) const
        {
          //std::clog << "evalFADSexp: " << forwardAutoDiff(std::forward<Src>(src), FadOperationType{}).name() << std::endl;
          return forwardAutoDiff(std::forward<Src>(src), FadOperationType{});
        }

        template<std::size_t Order, bool EvalSub = Evaluate>
        static constexpr auto upTo(IndexConstant<Order> = IndexConstant<Order>{},
                                   BoolConstant<Evaluate> = BoolConstant<EvalSub>{})
        {
          static_assert(Order <= FadOperationType::order_,
                        "Underlying FAD operation is lower order.");
          return EvalSExpFAD<OperateFADOfOrder<Order, FadOperationType, EvalSub> >{};
        }
      };

    } // NS Tensor

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_TENSORS_OPERATIONS_AUTODIFF_SUBEXPRESSION_HH__
