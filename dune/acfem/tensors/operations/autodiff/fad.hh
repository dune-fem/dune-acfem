#ifndef __DUNE_ACFEM_TENSORS_OPERATIONS_AUTODIFF_FAD_HH__
#define __DUNE_ACFEM_TENSORS_OPERATIONS_AUTODIFF_FAD_HH__

#include "../../../expressions/interface.hh"

#include "util.hh"

namespace Dune {

  namespace ACFem {

    namespace Tensor {

      template<bool Evaluate, class Id, class... Signature>
      struct OperateFAD;

      /**Implement forward AD by transforming an expression to its
       * evaluated AD expression. Evaluation is performed via the
       * Expressions::transform() function where the standard
       * evaluation functor is replaced by this one.
       */
      template<bool Evaluate, std::size_t... Id, class... Signature>
      struct OperateFAD<Evaluate, Seq<Id...>, Signature...>
      {
        static_assert(sizeof...(Id) == sizeof...(Signature),
                      "Number of indeterminate Ids must match the number of indeterminate tensor signatures.");
        constexpr static std::size_t order_ = sizeof...(Id);
        using IndeterminateIds = Seq<Id...>;
        using IndeterminateSignatures = std::tuple<Signature...>;
        constexpr static bool evaluate_ = Evaluate;
        using Optimize = ConditionalType<Evaluate, Expressions::DontOptimize, Expressions::OptimizeTop>;

        // This is called with terminal expressions.
        template<class F, class... Arg>
        constexpr auto operator()(F&& f, Arg&&... arg) const
        {
          return ensureAutoDiff<Id...>(
            BoolConstant<Evaluate>{},
            operate(Optimize{}, std::forward<F>(f),
                    ensureAutoDiff<Id...>(BoolConstant<Evaluate>{}, std::forward<Arg>(arg), Signature{}...)...
              ),
            Signature{}...);
        }
      };

      namespace {
        template<class Idx, class FadOperation, bool Evaluate>
        struct OperateFADOfOrderHelper;

        template<std::size_t... Idx, class FadOperation, bool Evaluate>
        struct OperateFADOfOrderHelper<Seq<Idx...>, FadOperation, Evaluate>
        {
          static_assert(sizeof...(Idx) <= FadOperation::order_,
                        "Underlying OperateFAD does not provide so many derivatives.");
          using Type = OperateFAD<Evaluate,
                                  Seq<Get<Idx, typename FadOperation::IndeterminateIds>::value...>,
                                  TupleElement<Idx, typename FadOperation::IndeterminateSignatures>...>;
        };
      }

      /**Obtain an OperateFAD functor from a given higher-order
       * OperateFAD functor which only computes derivatives up to
       * order Order.
       */
      template<std::size_t Order, class FadOperation, bool Evaluate = FadOperation::evaluate_>
      using OperateFADOfOrder =
        typename OperateFADOfOrderHelper<MakeIndexSequence<Order>, FadOperation, Evaluate>::Type;

      /**Utility function, perform FAD given a filled in OperateFAD
       * functor. An OperatorFAD functor can be optained via the
       * fadOperation() utitlity function. This function is rather low
       * level, there are more convenient front-end forwardAutodDiff()
       * functions.
       *
       * @param Id... Parameter pack with indeterminate ids. The
       * function will compute the potentially mixed potentially
       * higher order derivative with respect to all indeterminate ids
       * in the pack.
       *
       * @param Signature... The respective tensor signature of the
       * corresponding indeterminate indicated by the pack @a Id...
       *
       * @param e The tensor expression to differentiate.
       *
       * @param op The OperateFAD functor defining which derivatives
       * to compute.
       *
       * @param Evaluate If @c false the expressions for the
       * derivatives are not evaluated (i.e. not necessarily collapsed
       * to floating point values.)
       *
       * @param TreePos Starting position the in the expression
       * tree. Unused yet.
       *
       * @return An evaluated AD expression will all the derivatives
       * requested by OperateFAD. If Evaluate is @c false then the
       * result may contain unevaluated tensor expressions.
       *
       */
      template<std::size_t... Id, class... Signature, class E,
               bool Evaluate, class TreePos = Seq<> >
      auto forwardAutoDiff(E&& e,
                           OperateFAD<Evaluate, Seq<Id...>, Signature...>
                           op = OperateFAD<Evaluate, Seq<Id...>, Signature...>{},
                           BoolConstant<Evaluate> = BoolConstant<Evaluate>{},
                           TreePos = TreePos{})
      {
        static_assert(sizeof...(Id) == sizeof...(Signature),
                      "Need a signature for each indeterminate id and vice versa.");

        return expressionClosure(
          transform(std::forward<E>(e), Expressions::TraverseFunctor<>{}, op, TreePos{})
          );
      }

      /**Perform FAD for one given indeterminate.*/
      template<
        class T, class X, class Evaluate = TrueType,
        std::enable_if_t<(IsTensorOperand<T>::value
                          && IsTensor<X>::value
                          && IsIndeterminateExpression<EnclosedType<X> >::value
        ), int> = 0>
      constexpr auto forwardAutoDiff(T&& t, X&&, Evaluate = Evaluate{})
      {
        using XType = EnclosedType<X>;
        constexpr std::size_t id = IndeterminateTraits<XType>::id_;
        using Signature = typename TensorTraits<XType>::Signature;
        return forwardAutoDiff<id>(std::forward<T>(t), Signature{}, Evaluate{});
      }

      /**@internal Recursion work-horse.*/
      template<std::size_t Order, std::size_t... Orders, class X0, class... Xs,
               bool Evaluate, std::size_t... Id, class... Sig,
               std::enable_if_t<Order != 0, int> = 0>
      constexpr auto fadOperation(OperateFAD<Evaluate, Seq<Id...>, Sig...> operation, X0&& x0, Xs&&... xs)
      {
        using X0Type = EnclosedType<X0>;
        constexpr std::size_t id0 = IndeterminateTraits<X0Type>::id_;
        using Signature0 = typename TensorTraits<X0Type>::Signature;
        return fadOperation<Order-1, Orders...>(OperateFAD<Evaluate, Seq<Id..., id0>, Sig..., Signature0>{}, std::forward<X0>(x0), std::forward<Xs>(xs)...);
      }

      /**@internal Recursion end point where Order reaches 0.*/
      template<std::size_t Order, std::size_t... Orders, class X0, class... Xs,
               bool Evaluate, std::size_t... Id, class... Sig,
               std::enable_if_t<Order == 0, int> = 0>
      constexpr auto fadOperation(OperateFAD<Evaluate, Seq<Id...>, Sig...> operation, X0&& x0, Xs&&... xs)
      {
        return fadOperation<Orders...>(OperateFAD<Evaluate, Seq<Id...>, Sig...>{}, std::forward<Xs>(xs)...);
      }

      /**@internal Recursion end-point where all indeterminates and
       * their orders have been processed pushed as template
       * parameters into the OperateFAD functor.
       */
      template<std::size_t... Id, bool Evaluate, class... Sig>
      constexpr auto fadOperation(OperateFAD<Evaluate, Seq<Id...>, Sig...> operation)
      {
        return operation;
      }

      /**Generate an OperateFAD functor as indicated by the specified
       * indeterminates and their respective orders.
       *
       * @param Order, Orders... The number of times to differentiate
       * w.r.t. to the respective indeterminate.
       *
       * @param X0, Xs... The indeterminates.
       *
       * @return The OperateFAD functor where the Order parameters
       * have been eliminated by repeating the signature of the
       * respective indeterminate the number of times indicated by
       * Order, Orders...
       */
      template<std::size_t... Orders, bool Evaluate, class... Xs>
      constexpr auto fadOperation(BoolConstant<Evaluate>, Xs&&... xs)
      {
        return fadOperation<Orders...>(OperateFAD<Evaluate, Seq<> >{}, std::forward<Xs>(xs)...);
      }

      template<std::size_t... Orders, class... Xs,
               std::enable_if_t<AllAre<IsIndeterminateExpression, Xs...>{}, int> = 0>
      constexpr auto fadOperation(Xs&&... xs)
      {
        return fadOperation<Orders...>(OperateFAD<true, Seq<> >{}, std::forward<Xs>(xs)...);
      }

      /**Perform FAD by iteratively auto-differentiating @a t @a
       * Orders many times by the respective indeterminate.
       */
      template<std::size_t... Orders, bool Evaluate, class T, class... Xs>
      constexpr auto forwardAutoDiff(BoolConstant<Evaluate>, T&& t, Xs&&... xs)
      {
        return expressionClosure(
          transform(std::forward<T>(t),
                    Expressions::TraverseFunctor<>{},
                    fadOperation<Orders...>(BoolConstant<Evaluate>{}, std::forward<Xs>(xs)...),
                    Seq<>{})
          );
      }

      template<std::size_t... Orders, class T, class... Xs>
      constexpr decltype(auto) forwardAutoDiff(T&& t, Xs&&... xs)
      {
        return forwardAutoDiff<Orders...>(trueType(), std::forward<T>(t), std::forward<Xs>(xs)...);
      }

    } // NS Tensor

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_TENSORS_OPERATIONS_AUTODIFF_FAD_HH__
