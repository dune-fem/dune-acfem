#ifndef __DUNE_ACFEM_TENSORS_OPERATIONS_AUTODIFF_PLACEHOLDERTRAITS_HH__
#define __DUNE_ACFEM_TENSORS_OPERATIONS_AUTODIFF_PLACEHOLDERTRAITS_HH__

#include "../indeterminate.hh"
#include "../placeholder.hh"

namespace Dune {

  namespace ACFem {

    namespace Tensor {

      template<std::size_t Id, class Signature, class T, class DT>
      class AutoDiffPlaceholder;

      template<class T>
      struct IsAutoDiffPlaceholder
        : FalseType
      {};

      template<class T>
      struct IsAutoDiffPlaceholder<T&>
        : IsAutoDiffPlaceholder<std::decay_t<T> >
      {};

      template<class T>
      struct IsAutoDiffPlaceholder<T&&>
        : IsAutoDiffPlaceholder<std::decay_t<T> >
      {};

      template<std::size_t Id, class Signature, class T, class DT>
      struct IsAutoDiffPlaceholder<AutoDiffPlaceholder<Id, Signature, T, DT> >
        : TrueType
      {};

      template<class T, class SFINAE = void>
      struct AutoDiffTraits
      {
        static constexpr std::size_t depth_ = 0;
      };

      template<class T>
      struct AutoDiffTraits<T, std::enable_if_t<!IsDecay<T>::value> >
        : AutoDiffTraits<std::decay_t<T> >
      {};

      template<std::size_t Id, class Signature, class T, class DT>
      struct AutoDiffTraits<AutoDiffPlaceholder<Id, Signature, T, DT> >
      {
        static constexpr std::size_t id_ = Id;
        using IdType = IndexConstant<Id>;
        using IndeterminateSignature = Signature;
        using ValuesType = T;
        using DerivativeType = DT;
        static constexpr std::size_t depth_ = AutoDiffTraits<DT>::depth_ + 1;
        using DepthType = IndexConstant<depth_>;
      };

      namespace {
        template<std::size_t N, class T, class SFINAE = void>
        struct AutoDiffOfDepthHelper;

        template<class T>
        struct AutoDiffOfDepthHelper<0, T, std::enable_if_t<!IsAutoDiffPlaceholder<T>::value> >
        {
          using Type = T;
        };

        template<class T>
        struct AutoDiffOfDepthHelper<0, T, std::enable_if_t<IsAutoDiffPlaceholder<T>::value> >
        {
          using Type = typename AutoDiffTraits<T>::ValuesType;
        };

        template<std::size_t N, class T>
        struct AutoDiffOfDepthHelper<N, T, std::enable_if_t<IsAutoDiffPlaceholder<T>::value> >
        {
          static_assert(N <= AutoDiffTraits<T>::depth_, "Not so many levels of AD available...");
          using Type = AutoDiffPlaceholder<
            AutoDiffTraits<T>::id_,
            typename AutoDiffTraits<T>::IndeterminateSignature,
            typename AutoDiffTraits<T>::ValuesType,
            typename AutoDiffOfDepthHelper<N-1, typename AutoDiffTraits<T>::DerivativeType>::Type>;
        };
      }

      /**Generate a type representing a partial AutoDiff cascade.*/
      template<std::size_t N, class T>
      using AutoDiffOfDepth = typename AutoDiffOfDepthHelper<N, T>::Type;

      /**Check whehter T is a placeholder which matches the givens
       * indeterminate ids and signature.
       */
      template<class T, class Ids, class... Signature>
      struct IsMatchingAutoDiffPlaceholder
        : FalseType
      {};

      template<class T, std::size_t Id, class Signature, class SFINAE = void>
      struct IsSingleMatchingAutoDiffPlaceholder
        : BoolConstant<(AutoDiffTraits<T>::id_ == Id
                        && typename AutoDiffTraits<T>::IndeterminateSignature{} == Signature{})>
      {};

      template<class T, std::size_t Id, class Signature>
      struct IsSingleMatchingAutoDiffPlaceholder<
        T, Id, Signature, std::enable_if_t<!IsAutoDiffPlaceholder<T>::value> >
        : FalseType
      {};

      template<class T>
      struct IsMatchingAutoDiffPlaceholder<T, Seq<> >
        : TrueType
      {};

      template<class T, std::size_t Id, class Signature>
      struct IsMatchingAutoDiffPlaceholder<T, Seq<Id>, Signature>
        : IsSingleMatchingAutoDiffPlaceholder<T, Id, Signature>
      {};

      template<class T,
               std::size_t Id0, std::size_t Id1, std::size_t... RestId,
               class Sig0, class Sig1, class... RestSig>
      struct IsMatchingAutoDiffPlaceholder<T, Seq<Id0, Id1, RestId...>, Sig0, Sig1, RestSig...>
        : BoolConstant<(IsSingleMatchingAutoDiffPlaceholder<T, Id0, Sig0>::value
                        && IsMatchingAutoDiffPlaceholder<T, Seq<Id1, RestId...>, Sig1, RestSig...>::value
        )>
      {};

      template<std::size_t Id, class Signature, class T, class DT>
      struct HasSpecializedExpressionTraits<Tensor::AutoDiffPlaceholder<Id, Signature, T, DT> >
        : TrueType
      {};

    } // NS Tensor

    /**@internal Forward everything to the first argument.*/
    template<std::size_t Id, class Signature, class T, class DT>
    struct ExpressionTraits<Tensor::AutoDiffPlaceholder<Id, Signature, T, DT> >
      : ExpressionTraits<T>
    {
      static constexpr bool isVolatile = true;
    };

  } // NS ACFem

  template<std::size_t Id, class Signature, class T, class DT>
  struct FieldTraits<ACFem::Tensor::AutoDiffPlaceholder<Id, Signature, T, DT> >
    : FieldTraits<std::decay_t<T> >
  {};

} // NS Dune

#endif // __DUNE_ACFEM_TENSORS_OPERATIONS_AUTODIFF_PLACEHOLDERTRAITS_HH__
