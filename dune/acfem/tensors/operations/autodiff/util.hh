#ifndef __DUNE_ACFEM_TENSORS_OPERATIONS_AUTODIFF_UTIL_HH__
#define __DUNE_ACFEM_TENSORS_OPERATIONS_AUTODIFF_UTIL_HH__

#include <ostream>
#include "placeholder.hh"
#include "../derivative.hh"

namespace Dune {

  namespace ACFem {

    namespace Tensor {

      namespace {

        template<class T, class Evaluate>
        constexpr decltype(auto) evaluateAutoDiff(T&& t, Evaluate, PriorityTag<0>)
        {
          return std::forward<T>(t);
        }

        /**Convert NamedConstant's to dense floating point storage.*/
        template<class T, std::enable_if_t<Expressions::ExamineOr<T, IsNamedConstantTensor>::value, int> = 0>
        constexpr decltype(auto) evaluateAutoDiff(T&& t, TrueType, PriorityTag<1>)
        {
          return Expressions::DenseStorageType<T>(std::forward<T>(t));
        }

        /**@internal Convert non typed-values to dense floating point storage.*/
        template<class T, std::enable_if_t<!IsTypedValue<T>::value, int> = 0>
        constexpr decltype(auto) evaluateAutoDiff(T&& t, TrueType, PriorityTag<2>)
        {
          return Expressions::DenseStorageType<T>(std::forward<T>(t));
        }

        /**Replace remaining placeholders.*/
        template<class T, class Evaluate, std::enable_if_t<IsPlaceholderExpression<T>::value, int> = 0>
        constexpr auto evaluateAutoDiff(T&& t, Evaluate, PriorityTag<25>)
        {
          return evaluateAutoDiff(std::forward<T>(t).operand(0_c), Evaluate{});
        }

        /**Replace indeterminates.*/
        template<class T, class Evaluate, std::enable_if_t<IsIndeterminateExpression<T>::value, int> = 0>
        constexpr auto evaluateAutoDiff(T&& t, Evaluate, PriorityTag<30>)
        {
          return evaluateAutoDiff(std::forward<T>(t).operand(0_c), Evaluate{});
        }

        /**Replace all AutoDiffPlaceholder's in t by their values. This is
         * the "finalize" after differentiating elementary expressions.
         */
        template<class T, class Evaluate, std::enable_if_t<Expressions::ExamineOr<T, IsAutoDiffPlaceholder>::value, int> = 0>
        constexpr auto evaluateAutoDiff(T&& t, Evaluate, PriorityTag<35>)
        {
          return evaluateAutoDiff(
            transform(std::forward<T>(t), Expressions::ReplaceByOperand<0, IsAutoDiffPlaceholder>{}),
            Evaluate{}
            );
        }

        template<class T, class Evaluate, std::enable_if_t<IsClosure<T>::value, int> = 0>
        constexpr auto evaluateAutoDiff(T&& t, Evaluate, PriorityTag<40>)
        {
          return evaluateAutoDiff(Expressions::asExpression(std::forward<T>(t)), Evaluate{});
        }

      } // NS anonymous

      /**@internal Evaluate one component of an AD placeholder.*/
      template<class T, class Evaluate>
      constexpr decltype(auto) evaluateAutoDiff(T&& t, Evaluate eval = Evaluate{})
      {
        return evaluateAutoDiff(std::forward<T>(t), eval, PriorityTag<42>{});
      }

      namespace {

        template<
          std::size_t... Id, bool Evaluate, class T, class... Signature,
          std::enable_if_t<(IsMatchingAutoDiffPlaceholder<T, Seq<Id...>, Signature...>::value
          ), int> = 0>
        constexpr decltype(auto) ensureAutoDiffRecursion(BoolConstant<Evaluate>, T&& t, Signature...)
        {
          return std::forward<T>(t);
        }

        template<std::size_t Id0, bool Evaluate, class T, class Signature0,
                 std::enable_if_t<(IsSequence<Signature0>::value
                                   && !IsMatchingAutoDiffPlaceholder<T, Seq<Id0>, Signature0>::value
                   ), int> = 0>
        constexpr auto ensureAutoDiffRecursion(BoolConstant<Evaluate> eval, T&& t, Signature0)
        {
          return
            autoDiffPlaceholder<Id0, Signature0>(
              evaluateAutoDiff(std::forward<T>(t), eval),
              evaluateAutoDiff(
                derivative(std::forward<T>(t), asExpression(indeterminate<Id0>(Signature0{}))),
                eval
                )
              );
        }

        template<std::size_t Id0, std::size_t... Id, bool Evaluate, class T, class Signature0, class... Signature,
                 std::enable_if_t<((sizeof...(Id) > 0)
                                   && IsSequence<Signature0>::value
                                   && !IsMatchingAutoDiffPlaceholder<T, Seq<Id0, Id...>, Signature0, Signature...>::value
                   ), int> = 0>
        constexpr auto ensureAutoDiffRecursion(BoolConstant<Evaluate> eval, T&& t, Signature0, Signature...)
        {
          static_assert(sizeof...(Id) == sizeof...(Signature), "Each Id needs a signature and vice versa.");
          return
            autoDiffPlaceholder<Id0, Signature0>(
              evaluateAutoDiff(std::forward<T>(t), eval),
              ensureAutoDiffRecursion<Id...>(
                eval,
                derivative(std::forward<T>(t), asExpression(indeterminate<Id0>(Signature0{}))),
                Signature{}...
                )
              );
        }

      } // NS Anonymous

      /**@internal Allow unevaluated autodiff.*/
      template<std::size_t... Id, bool Evaluate, class T, class... Signature>
      constexpr decltype(auto) ensureAutoDiff(BoolConstant<Evaluate> eval, T&& t, Signature...)
      {
        static_assert(sizeof...(Id) == sizeof...(Signature), "Each Id needs a signature and vice versa.");
        return ensureAutoDiffRecursion<Id...>(
          eval,
          Expressions::asExpression(std::forward<T>(t)),
          Signature{}...
          );
      }

      /**Generate a new AD placeholder from T or just forward it in
       * case it is already an AD placeholder which matches the
       * specified derivative ids and signatures.
       *
       * NOTA BENE: this is not AD, this function's purpose is to
       * convert expression terminals into elementary AD expressions,
       * although it works via symbolic differentiation with all tensor
       * expressions.
       */
      template<std::size_t... Id, class T, class... Signature>
      constexpr decltype(auto) ensureAutoDiff(T&& t, Signature... sig)
      {
        static_assert(sizeof...(Id) == sizeof...(Signature), "Each Id needs a signature and vice versa.");
        return ensureAutoDiff<Id...>(trueType(), std::forward<T>(t), sig...);
      }

    } // NS Tensor

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_TENSORS_OPERATIONS_AUTODIFF_UTIL_HH__
