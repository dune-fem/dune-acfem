#ifndef __DUNE_ACFEM_TENSORS_OPERATIONS_RESHAPE_HH__
#define __DUNE_ACFEM_TENSORS_OPERATIONS_RESHAPE_HH__

#include "../../expressions/storage.hh"
#include "../../mpl/multiindex.hh"
#include "../tensorbase.hh"
#include "../modules.hh"

namespace Dune::ACFem {

  // forward
  template<class Signature>
  struct ReshapeOperation;

}

namespace Dune::ACFem::Tensor {

  /**@addtogroup Tensors
   * @{
   */

  /**@addtogroup TensorOperations
   * @{
   */

  template<class Parent, class Signature>
  class Reshape;

  template<class T, class SFINAE = void>
  struct IsReshape
    : FalseType
  {};

  template<class T>
  struct IsReshape<T, std::enable_if_t<!IsDecay<T>::value> >
    : IsReshape<std::decay_t<T> >
  {};

  template<class Parent, class Signature>
  struct IsReshape<Reshape<Parent, Signature> >
    : TrueType
  {};


/** wraps a tensor using the same data but with a different signature */
  template<class Parent, std::size_t... Dimensions>
  class Reshape<Parent, Seq<Dimensions...> >
    : public TensorBase<typename std::decay_t<Parent>::FieldType,
                        Seq<Dimensions...>,
                        Reshape<Parent, Seq<Dimensions...> > >
    , public Expressions::Storage<OperationTraits<ReshapeOperation<Seq<Dimensions...> > >, Parent>
  {
    using ThisType = Reshape;
    using ParentType = std::decay_t<Parent>;
    using BaseType = TensorBase<typename ParentType::FieldType,
                                Seq<Dimensions...>,
                                Reshape<Parent, Seq<Dimensions...> > >;
    using StorageType = Expressions::Storage<OperationTraits<ReshapeOperation<Seq<Dimensions...> > >, Parent>;
   public:
    using BaseType::rank;
    using BaseType::dim;
    using typename BaseType::FieldType;
    using Signature = TensorSignature<Dimensions...>;
    using ParentSignature = typename ParentType::Signature;
    using StorageType::operation;
    using StorageType::operand;

    static_assert(dim() == ParentType::dim(), "reshaped tensors total dimension needs to match parents");

    template<class Arg, std::enable_if_t<std::is_constructible<ParentType, Arg>::value, int> = 0>
    Reshape(Arg&& parent)
      : StorageType(std::forward<Arg>(parent))
    {}

    /**Allow default construction if contained types fulfill IsTypedValue.*/
    template<
      class... Dummy,
      std::enable_if_t<(sizeof...(Dummy) == 0
                        && IsTypedValue<ParentType>::value), int> = 0>
    Reshape(Dummy&&...)
      : StorageType(ParentType{})
    {}

    /**Translate the given multi-index positions into a linear
     * index and forward to the underlying storage.
     */
    template<class... Indices,
             std::enable_if_t<(sizeof...(Indices) == rank
                               &&
                               IsIntegralPack<Indices...>::value)
                              , int> = 0>
    constexpr decltype(auto) operator()(Indices... indices) const
    {
      const auto index = flattenMultiIndex(Signature{}, indices...);
      const auto parentIndexArray = multiIndex(index, ParentSignature{});

      // array -> tuple -> parameter pack function call
      return tensorValue(operand(0_c), parentIndexArray);
    }

    /**Translate the given multi-index positions into a linear
     * index and forward to the underlying storage.
     */
    template<class... Indices,
             std::enable_if_t<(sizeof...(Indices) == rank
                               &&
                               IsIntegralPack<Indices...>::value)
                              , int> = 0>
    constexpr decltype(auto) operator()(Indices... indices)
    {
      const auto index = flattenMultiIndex(Signature{}, indices...);
      const auto parentIndexArray = multiIndex(index, ParentSignature{});

      // array -> tuple -> parameter pack function call
      return tensorValue(operand(0_c), parentIndexArray);
    }

    /**Constant access from index-sequence.*/
    template<std::size_t... Indices,
             std::enable_if_t<sizeof...(Indices) == rank, int> = 0>
    decltype(auto) constexpr operator()(Seq<Indices...>) const
    {
      constexpr std::size_t index = flattenMultiIndex<Indices...>(Signature{});

      return tensorValue(operand(0_c), multiIndex<index>(ParentSignature{}));
    }

    /**Constant access from index-sequence.*/
    template<std::size_t... Indices,
             std::enable_if_t<sizeof...(Indices) == rank, int> = 0>
    decltype(auto) constexpr operator()(Seq<Indices...>)
    {
      constexpr std::size_t index = flattenMultiIndex<Indices...>(Signature{});

      return tensorValue(operand(0_c), multiIndex<index>(ParentSignature{}));
    }

    std::string name() const
    {
      return "reshape<" + toString(Signature{}) + ">(" + refPrefix() + toString(operand(0_c)) + ")";
    }

   private:
    std::string refPrefix() const
    {
      constexpr bool isRef = std::is_lvalue_reference<Parent>::value;
      constexpr bool isConst = std::is_const<std::remove_reference_t<Parent> >::value;

      return isRef ? (isConst ? "cref" : "ref") : "";
    }
  };

  template<std::size_t... Dimensions, class T, std::enable_if_t<IsProperTensor<T>::value, int> = 0>
  constexpr decltype(auto) reshape(T&& t, Seq<Dimensions...> = Seq<Dimensions...>{})
  {
    using Operation = ReshapeOperation<Seq<Dimensions...> >;

    return Expressions::finalize<Operation>(std::forward<T>(t));
  }

  template<class Sig, class T>
  constexpr auto operate(Expressions::DontOptimize, OperationTraits<ReshapeOperation<Sig> >, T&& t)
  {
    DUNE_ACFEM_RECORD_OPTIMIZATION;

    return Reshape<T, Sig>(std::forward<T>(t));
  }

} // namespace Dune::ACFem::Tensor

namespace Dune {

  template<class Parent, class Signature>
  struct FieldTraits<ACFem::Tensor::Reshape<Parent, Signature> >
    : FieldTraits<typename std::decay_t<Parent>::FieldType>
  {};

}

#endif // __DUNE_ACFEM_TENSORS_OPERATIONS_RESHAPE_HH__
