#ifndef __DUNE_ACFEM_TENSORS_OPERATIONS_REASSOCIATETREE_HH__
#define __DUNE_ACFEM_TENSORS_OPERATIONS_REASSOCIATETREE_HH__

#include "../../expressions/operationpair.hh"
#include "associativity.hh"

#define ALLOW_TUPLES_AS_FACTORS true

namespace Dune {

  namespace ACFem {

    namespace Tensor {

      namespace ProductOperations {

        /**@internal Helper template to conveniently construct a
         * signed scalar one.
         */
        template<class Sign = PlusFunctor, class Pos = TreePosition<~0UL> >
        using OnesNode = TreeData<Sign, Ones<Seq<> >, Pos>;

        //!@internal Idendify "not a" signed scalar one.
        template<class T>
        constexpr inline bool IsOnesNodeV = false;

        //!@internal Idendify a signed scalar one.
        template<class Sign, class Pos>
        constexpr inline bool IsOnesNodeV<OnesNode<Sign, Pos> > = true;

        template<class T>
        struct CollapseOnesHelper
        {
          using Type = T;
        };

        template<class Sign, class E, class Pos, class OnesSign, class OnesPos, class Tag>
        struct CollapseOnesHelper<OperationPair<Sign, ScalarEinsumFunctor, TreeData<PlusFunctor, E, Pos>, OnesNode<OnesSign, OnesPos>, Tag> >
        {
          using Type = TreeData<NestedSumFunctor<Sign, OnesSign>, E, Pos>;
        };

        template<class Sign, class E, class Pos, class OnesSign, class OnesPos, class Tag>
        struct CollapseOnesHelper<OperationPair<Sign, ScalarEinsumFunctor, OnesNode<OnesSign, OnesPos>, TreeData<PlusFunctor, E, Pos>, Tag> >
        {
          using Type = TreeData<NestedSumFunctor<Sign, OnesSign>, E, Pos>;
        };

#if 0
        template<class Sign, class Sign0, class Pos0, class Sign1, class Pos1, class Tag>
        struct CollapseOnesHelper<OperationPair<Sign, OnesNode<Sign0, Pos0>, OnesNode<Sign1, Pos1>, Tag> >
        {
          using Type = OnesNode<NestedSumFunctor<Sign, NestedSumFunctor<Sign0, Sign1> > >;
        };
#endif

        template<class T>
        using CollapseOnes = typename CollapseOnesHelper<T>::Type;

        //////////////////////////////////////////////////////

        template<class TreeNode, class SFINAE = void>
        constexpr inline std::size_t TreeTensorRankV = TensorTraits<TreeExpression<TreeNode> >::rank;

        template<class T0, class... T>
        constexpr inline std::size_t TreeTensorRankV<MPL::TypeTuple<T0, T...> > = TreeTensorRankV<T0>;

        template<class Sign, class F, class TreeNode0, class TreeNode10, class... TreeNode1, class Tag>
        constexpr inline std::size_t TreeTensorRankV<OperationPair<Sign, F, TreeNode0, MPL::TypeTuple<TreeNode10, TreeNode1...>, Tag> > =
          TreeTensorRankV<OperationPair<Sign, F, TreeNode0, TreeNode10, Tag> >;

        template<class Sign, class F, class TreeNode00, class... TreeNode0, class TreeNode1, class Tag>
        constexpr inline std::size_t TreeTensorRankV<OperationPair<Sign, F, MPL::TypeTuple<TreeNode00, TreeNode0...>, TreeNode1, Tag> > =
          TreeTensorRankV<OperationPair<Sign, F, TreeNode00, TreeNode1, Tag> >;

        //////////////////////////////////////////////////////

        template<class TreeData, class SFINAE = void>
        struct AssociateTreeRightHelper
        {
          using Type = TreeData;
        };

        // (A*B)*C -> A*(B*C)
        template<class Sign, class Functor,
                 class InnerSign, class InnerFunctor, class TreeData00, class TreeData01, class Tag0,
                 class TreeData1,
                 class OptimizeTag>
        struct AssociateTreeRightHelper<
          OperationPair<Sign, Functor, OperationPair<InnerSign, InnerFunctor, TreeData00, TreeData01, Tag0>, TreeData1, OptimizeTag>,
          std::enable_if_t<IsRightAssociative<Functor, OperationPair<InnerSign, InnerFunctor, TreeData00, TreeData01, Tag0>, TreeData1>::value>
          >
        {
#if !ALLOW_TUPLES_AS_FACTORS
          static constexpr std::size_t rankLeft = TensorTraits<TreeExpression<TreeData00> >::rank;
          static constexpr std::size_t rankMiddle = TensorTraits<TreeExpression<TreeData01> >::rank;
          static constexpr std::size_t rankRight = TensorTraits<TreeExpression<TreeData1> >::rank;
#else
          static constexpr std::size_t rankLeft = TreeTensorRankV<TreeData00>;
          static constexpr std::size_t rankMiddle = TreeTensorRankV<TreeData01>;
          static constexpr std::size_t rankRight = TreeTensorRankV<TreeData1>;
#endif
          using Operations = decltype(associateRightOperations(
                                        InnerFunctor{}, Functor{}, IndexSequence<rankLeft, rankMiddle, rankRight>{}));

          using LeftFunctor = TupleElement<0, Operations>;
          using RightFunctor = TupleElement<1, Operations>;
          using Inner = OperationPair<PlusFunctor, RightFunctor, TreeData01, TreeData1, OptimizeTag>;
          using Type = OperationPair<NestedSumFunctor<Sign, InnerSign>, LeftFunctor, TreeData00, Inner, OptimizeTag>;
        };

        template<class T>
        using AssociateTreeRight = typename AssociateTreeRightHelper<T>::Type;

        ///////////////////////////////

        template<class TreeData, class SFINAE = void>
        struct AssociateTreeLeftHelper
        {
          using Type = TreeData;
        };

        // A*(B*C) -> (A*B)*C
        template<class Sign, class Functor,
                 class TreeData0,
                 class InnerSign, class InnerFunctor, class TreeData10, class TreeData11, class Tag1,
                 class OptimizeTag>
        struct AssociateTreeLeftHelper<
          OperationPair<Sign, Functor, TreeData0, OperationPair<InnerSign, InnerFunctor, TreeData10, TreeData11, Tag1>, OptimizeTag>,
          std::enable_if_t<IsLeftAssociative<Functor, TreeData0, OperationPair<InnerSign, InnerFunctor, TreeData10, TreeData11, Tag1> >::value>
          >
        {
#if !ALLOW_TUPLES_AS_FACTORS
          static constexpr std::size_t rankLeft = TensorTraits<TreeExpression<TreeData0> >::rank;
          static constexpr std::size_t rankMiddle = TensorTraits<TreeExpression<TreeData10> >::rank;
          static constexpr std::size_t rankRight = TensorTraits<TreeExpression<TreeData11> >::rank;
#else
          static constexpr std::size_t rankLeft = TreeTensorRankV<TreeData0>;
          static constexpr std::size_t rankMiddle = TreeTensorRankV<TreeData10>;
          static constexpr std::size_t rankRight = TreeTensorRankV<TreeData11>;
#endif
          using Operations = decltype(associateLeftOperations(
                                        Functor{}, InnerFunctor{},
                                        IndexSequence<rankLeft, rankMiddle, rankRight>{}));

          using LeftFunctor = TupleElement<0, Operations>;
          using RightFunctor = TupleElement<1, Operations>;
          using Inner = OperationPair<PlusFunctor, LeftFunctor, TreeData0, TreeData10, OptimizeTag>;
          using Type = OperationPair<NestedSumFunctor<Sign, InnerSign>, RightFunctor, Inner, TreeData11, OptimizeTag>;
        };

        template<class T>
        using AssociateTreeLeft = typename AssociateTreeLeftHelper<T>::Type;

        ///////////////////////////////

#if 0

        template<class T, class SFINAE = void>
        struct LeftMostTreeFactorHelper
        {
          using Type = T;
        };

        template<class Sign, class InnerF, class TreeData0, class TreeData1, class OptimizeTag>
        struct LeftMostTreeFactorHelper<
          OperationPair<Sign, InnerF, TreeData0, TreeData1, OptimizeTag>,
          std::enable_if_t<FunctorHas<IsProductOperation, InnerF>::value> >
        {
          template<class T, class SFINAE = void>
          struct Helper
          {
            using Type = T;
          };

          template<class T>
          struct Helper<T, std::enable_if_t<IsRightAssociative<InnerF, T, TreeData1>::value> >
          {
            using Type = typename LeftMostTreeFactorHelper<T>::Type;
          };
          using Type = typename Helper<TreeData0>::Type;
        };

        template<class Sign, class Left0, class... Left, class Right, class Tag>
        struct LeftMostTreeFactorHelper<OperationPair<Sign, ScalarEinsumFunctor, MPL::TypeTuple<Left0, Left...>, Right, Tag> >
        {
          using Type = Left0;
        };

        template<class T>
        using LeftMostTreeFactor = TreeExpression<typename LeftMostTreeFactorHelper<T>::Type>;

#else

        template<class F, class E, class Pos>
        constexpr auto leftMostTreeFactor(TreeData<F, E, Pos>)
        {
          return TreeData<F, E, Pos>{};
        }

        template<class Sign, class InnerF, class TreeData0, class TreeData1, class OptimizeTag>
        constexpr auto leftMostTreeFactor(OperationPair<Sign, InnerF, TreeData0, TreeData1, OptimizeTag>)
        {
          if constexpr (!FunctorHas<IsProductOperation, InnerF>::value) {
            return OperationPair<Sign, InnerF, TreeData0, TreeData1, OptimizeTag>{};
          } else if constexpr (!IsRightAssociative<InnerF, TreeData0, TreeData1>::value) {
            return TreeData0{};
          } else {
            return leftMostTreeFactor(TreeData0{});
          }
        }

        /**@internal Left-most factor for a type-tuple of scalar
         * factors as created by Explode<>.
         */
        template<class Sign, class Left0, class... Left, class Right, class Tag>
        constexpr auto leftMostTreeFactor(OperationPair<Sign, ScalarEinsumFunctor, MPL::TypeTuple<Left0, Left...>, Right, Tag>)
        {
          return Left0{};
        }

        template<class T>
        using LeftMostTreeFactor = TreeExpression<decltype(leftMostTreeFactor(std::declval<T>()))>;
#endif

        ///////////////////////////////

        template<class F, class E, class Pos>
        constexpr auto rightMostTreeFactor(TreeData<F, E, Pos>)
        {
          return TreeData<F, E, Pos>{};
        }

        template<class Sign, class InnerF, class TreeData0, class TreeData1, class OptimizeTag>
        constexpr decltype(auto) rightMostTreeFactor(OperationPair<Sign, InnerF, TreeData0, TreeData1, OptimizeTag>)
        {
          if constexpr (!FunctorHas<IsProductOperation, InnerF>::value) {
            return OperationPair<Sign, InnerF, TreeData0, TreeData1, OptimizeTag>{};
          } else if constexpr (!IsLeftAssociative<InnerF, TreeData0, TreeData1>::value) {
            return TreeData1{};
          } else {
            return rightMostTreeFactor(TreeData1{});
          }
        }

#if 0
        template<class Sign, class Left, class Right0, class... Right, class Tag>
        constexpr auto rightMostTreeFactor(OperationPair<Sign, ScalarEinsumFunctor, Left, MPL::TypeTuple<Right0, Right...>, Tag>)
        {
          return Right0{};
        }
#endif

        template<class T>
        using RightMostTreeFactor = TreeExpression<decltype(rightMostTreeFactor(std::declval<T>()))>;

        ///////////////////////////////

        /**Default case: TreeData is already associated to the left
         * and not a simple TreeData.
         */
        template<class TreeData, class SFINAE = void>
        struct TreeFactorOutRightHelper
        {
          using Type = TreeData;
        };

        /**Blow-up a simple TreeData by adding a scalar one node at
         * the left
         */
        template<class S, class E, class Pos>
        struct TreeFactorOutRightHelper<TreeData<S, E, Pos> >
        {
          using Type = OperationPair<S, ScalarEinsumFunctor, OnesNode<>, TreeData<PlusFunctor, E, Pos> >;
        };

        /**@internal Give access to the right-most factor of a product
         * by recursively associating nested products to the left.
         *
         * The resulting type is an OperationPair where
         * OperationPair::TreeNode0 is the left-most factor and
         * OperationPair::FunctorType is the corresponding
         * expression-functor
         * (OperationTraits). OperationPair::TreeNode1 is the
         * corresponding right factor.
         */
        template<class Sign, class InnerF, class TreeData0, class TreeData1, class OptimizeTag>
        struct TreeFactorOutRightHelper<
          OperationPair<Sign, InnerF, TreeData0, TreeData1, OptimizeTag>,
          std::enable_if_t<IsLeftAssociative<InnerF, TreeData0, TreeData1>::value>
          >
        {
          // A*(B*C) -> (A*B)*C
          using Type = typename TreeFactorOutRightHelper<AssociateTreeLeft<OperationPair<Sign, InnerF, TreeData0, TreeData1, OptimizeTag> > >::Type;
        };

        template<class T>
        using TreeFactorOutRight = typename TreeFactorOutRightHelper<T>::Type;

        ///////////////////////////////

        /**Default case: TreeData is already associated to the right
         * and not a simple TreeData.
         */
        template<class TreeData, class SFINAE = void>
        struct TreeFactorOutLeftHelper
        {
          using Type = TreeData;
        };

        /**Blow-up a simple TreeData by adding a scalar one node at
         * the right.
         */
        template<class S, class E, class Pos>
        struct TreeFactorOutLeftHelper<TreeData<S, E, Pos> >
        {
          using Type = OperationPair<S, ScalarEinsumFunctor, TreeData<PlusFunctor, E, Pos>, OnesNode<> >;
        };

        /**@internal Give access to the left-most factor of a product
         * by repeatedly associating nested products to the right.
         *
         * The resulting type is an OperationPair where
         * OperationPair::TreeNode0 is the left-most factor and
         * OperationPair::FunctorType is the corresponding
         * expression-functor
         * (OperationTraits). OperationPair::TreeNode1 is the
         * corresponding right factor.
         */
        template<class Sign, class InnerF, class TreeData0, class TreeData1, class OptimizeTag>
        struct TreeFactorOutLeftHelper<
          OperationPair<Sign, InnerF, TreeData0, TreeData1, OptimizeTag>,
          std::enable_if_t<IsRightAssociative<InnerF, TreeData0, TreeData1>::value>
          >
        {
          // (A*B)*C -> A*(B*C)
          using Type = typename TreeFactorOutLeftHelper<AssociateTreeRight<OperationPair<Sign, InnerF, TreeData0, TreeData1, OptimizeTag> > >::Type;
        };

        template<class T>
        using TreeFactorOutLeft = typename TreeFactorOutLeftHelper<T>::Type;

      } // ProductOperations::

        //////////////////////////////////////////////////////

      template<class Sign, class T, class Pos>
      struct TensorTraits<Expressions::TreeData<Sign, T, Pos> >
        : TensorTraits<T>
      {};

      template<class Sign, class F, class T0, class T1, class Tag>
      struct TensorTraits<Expressions::OperationPair<Sign, F, T0, T1, Tag> >
        : TensorTraits<Expressions::TreeExpression<Expressions::OperationPair<Sign, F, T0, T1, Tag> > >
      {};

      template<class Sign, class T00, class... T0Rest, class T1, class Tag>
      struct TensorTraits<Expressions::OperationPair<Sign, ScalarEinsumFunctor, MPL::TypeTuple<T00, T0Rest...>, T1, Tag> >
        : TensorTraits<Expressions::OperationPair<Sign, ScalarEinsumFunctor, T00, T1, Tag> >
      {};

    } // Tensor::

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_TENSORS_OPERATIONS_REASSOCIATETREE_HH__
