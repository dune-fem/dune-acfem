#ifndef __DUNE_ACFEM_TENSORS_OPERATIONS_TRANSPOSETRAITS_HH__
#define __DUNE_ACFEM_TENSORS_OPERATIONS_TRANSPOSETRAITS_HH__

#include "../../mpl/compare.hh"
#include "../../expressions/storage.hh"
#include "../expressionoperations.hh"
#include "../modules.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup Tensors
     * @{
     */

    namespace Tensor {

      /**@addtogroup TensorOperations
       * @{
       */

      /**@addtogroup TensorOperationTraits
       * @{
       */

      /**@name TranspositionTraits
       *
       * @{
       */

      /**@internal Evaluate to std::true_type if the given permuation
       * is subordinate to the given tensor expression. In particular
       * for product expressions determine if the permutation does not
       * mix the index positions of the operands.
       */
      template<class Perm, class T, class SFINAE = void>
      struct IsSubordinateTransposition
        : FalseType
      {};

      template<class Perm, class T, class SFINAE>
      struct IsSubordinateTransposition<Perm, T&, SFINAE>
        : IsSubordinateTransposition<Perm, std::decay_t<T>, SFINAE>
      {};

      template<class Perm, class T, class SFINAE>
      struct IsSubordinateTransposition<Perm, T&&, SFINAE>
        : IsSubordinateTransposition<Perm, std::decay_t<T>, SFINAE>
      {};

      template<class Perm, class T>
      struct IsSubordinateTransposition<
        Perm, T,
        std::enable_if_t<IsDecay<T>::value && Perm::size() != TensorTraits<T>::rank> >
        : IsSubordinateTransposition<ResizedPermutation<Perm, TensorTraits<T>::rank>, T>
      {};

      /**Component-wise unary operations are ok if the permutation
       * operates on the signature.
       */
      template<class Perm, class T>
      struct IsSubordinateTransposition<
        Perm, T,
        std::enable_if_t<(IsDecay<T>::value
                          && IsUnaryExpression<T>::value
                          && IsComponentWiseExpression<T>::value
                          && isPermutation(Perm{})
                          && Perm::size() == TensorTraits<T>::rank
        )> >
        : TrueType
      {};

      /**Inspect einsum expressions.*/
      template<class Perm, class T>
      struct IsSubordinateTransposition<
        Perm, T,
        std::enable_if_t<(IsDecay<T>::value
                          && isPermutation(Perm{})
                          // Could be relaxed by using a ResizedPermutation
                          && Perm::size() == TensorTraits<T>::rank
                          && IsEinsumExpression<T>::value
        )> >
        : BoolConstant<(isPermutation(SequenceSlice<Perm, typename T::LeftArgs>{})
                        &&
                        isPermutation(SequenceCat<typename T::LeftArgs, SequenceSlice<Perm, typename T::RightArgs> >{}))>
      {};

      /**Inspect product expressions.*/
      template<class Perm, class T>
      struct IsSubordinateTransposition<
        Perm, T,
        std::enable_if_t<(IsDecay<T>::value
                          && isPermutation(Perm{})
                          && Perm::size() == TensorTraits<T>::rank
                          && IsTensorProductExpression<T>::value
          )> >
        : BoolConstant<(// Defect | RestLeft | RestRight
                        isPermutation(SequenceSlice<Perm, typename T::FrontArgs>{})
                        && isPermutation(OffsetSequence<-(int)T::frontRank_, SequenceSlice<Perm, typename T::LeftArgs> >{})
                        && isPermutation(OffsetSequence<-(int)(T::frontRank_+T::leftRank_), SequenceSlice<Perm, typename T::RightArgs> >{}))>
      {};

      /**Construct the induced transposition for operand N if Perm is
       * subordinate to the expression structure of T.
       */
      template<std::size_t N, class Perm, class T, class SFINAE = void>
      struct TranspositionForOperandHelper;

      template<std::size_t N, class Perm, class T>
      using TranspositionForOperand = typename TranspositionForOperandHelper<N, Perm, T>::Type;

      /**@internal Generate permutations for EinsteinSummation operands.*/
      template<std::size_t N, class PermArg, class T>
      struct TranspositionForOperandHelper<
        N, PermArg, T,
        std::enable_if_t<(IsSubordinateTransposition<PermArg, T>::value
                          && IsEinsumExpression<T>::value
                          && (N < Arity<T>::value)
          )> >
      {
        using Perm = ResizedPermutation<PermArg, TensorTraits<T>::rank>;
        using Traits = EinsumTraits<T>;
        using LeftIndexPositions = typename Traits::LeftIndexPositions;
        using RightIndexPositions = typename Traits::RightIndexPositions;
        static constexpr std::ptrdiff_t leftRank_ = TensorTraits<Operand<0, T> >::rank;
        static constexpr std::ptrdiff_t leftDefectRank_ = leftRank_ - Traits::defectRank_;
        static constexpr std::ptrdiff_t rightRank_ = TensorTraits<Operand<1, T> >::rank;
        static constexpr std::ptrdiff_t rightDefectRank_ = rightRank_ - Traits::defectRank_;

        // Positions of left and right indices in index tuple
        using LeftOperandPos = MakeIndexSequence<leftDefectRank_>;
        using RightOperandPos = MakeIndexSequence<rightDefectRank_, leftDefectRank_>;
        using DefectPos = MakeIndexSequence<Traits::defectRank_>;

        // Parts of the permutation referring to the respective operand
        using LeftOuterPerm =  SequenceSlice<Perm, LeftOperandPos>;
        using RightOuterPerm = OffsetSequence<-leftDefectRank_, SequenceSlice<Perm, RightOperandPos> >;

        // Construct the lookup tables to map the outer permutations
        // to the original index posisitions.
        using LeftLookup = SequenceSliceComplement<MakeIndexSequence<leftRank_>, LeftIndexPositions>;
        using RightLookup = SequenceSliceComplement<MakeIndexSequence<rightRank_>, RightIndexPositions>;

        // Map the outer permutations to the correct values.
        using LeftMappedPerm = TransformedSequence<MapSequenceFunctor<LeftLookup>, LeftOuterPerm>;
        using RightMappedPerm = TransformedSequence<MapSequenceFunctor<RightLookup>, RightOuterPerm>;

        // Now inject the defect positions as identity at the proper locations
        using LeftInnerPerm = InsertAt<LeftMappedPerm, LeftIndexPositions, LeftIndexPositions>;
        using RightInnerPerm = InsertAt<RightMappedPerm, RightIndexPositions, RightIndexPositions>;

        using Type = ConditionalType<(N == 0), LeftInnerPerm, RightInnerPerm>;
      };

      /**@internal Generate permutations for ProductTensor operands.*/
      template<std::size_t N, class Perm , class T>
      struct TranspositionForOperandHelper<
        N, Perm, T,
        std::enable_if_t<(IsSubordinateTransposition<Perm, T>::value
                          && IsTensorProductExpression<T>::value
                          && (N < Arity<T>::value)
          )> >
      {
        using Traits = TensorProductTraits<Operation<T> >;
        using LeftIndexPositions = typename Traits::LeftIndexPositions;
        using RightIndexPositions = typename Traits::RightIndexPositions;
        static constexpr std::size_t frontRank_ = LeftIndexPositions::size();
        static constexpr std::size_t leftRank_ = TensorTraits<Operand<0, T> >::rank;
        static constexpr std::size_t leftDefectRank_ = leftRank_ - Traits::defectRank_;
        static constexpr std::size_t rightRank_ = TensorTraits<Operand<1, T> >::rank;
        static constexpr std::size_t rightDefectRank_ = rightRank_ - Traits::defectRank_;

        // Positions of left and right indices in index tuple
        using FrontOperandPos = MakeIndexSequence<frontRank_>;
        using LeftOperandPos = MakeIndexSequence<leftDefectRank_, frontRank_>;
        using RightOperandPos = MakeIndexSequence<rightDefectRank_, leftDefectRank_ + frontRank_>;
        using DefectPos = MakeIndexSequence<Traits::defectRank_>;

        // Parts of the permutation referring to the respective operand
        using FrontOuterPerm = SequenceSlice<Perm, FrontOperandPos>;
        using LeftOuterPerm =  OffsetSequence<-(int)frontRank_, SequenceSlice<Perm, LeftOperandPos> >;
        using RightOuterPerm = OffsetSequence<-(int)(frontRank_+leftDefectRank_), SequenceSlice<Perm, RightOperandPos> >;

        // Construct the lookup tables to map the outer permutations
        // to the original index posisitions. The lookup-table for the
        // front indices is just LeftIndexPositions
        // resp. RightIndexPositions
        using LeftLookup = SequenceSliceComplement<MakeIndexSequence<leftRank_>, LeftIndexPositions>;
        using RightLookup = SequenceSliceComplement<MakeIndexSequence<rightRank_>, RightIndexPositions>;

        // Map the outer permutations to the correct values.
        using LeftMappedPerm = TransformedSequence<MapSequenceFunctor<LeftLookup>, LeftOuterPerm>;
        using LeftMappedFrontPerm = TransformedSequence<MapSequenceFunctor<LeftIndexPositions>, FrontOuterPerm>;
        using RightMappedPerm = TransformedSequence<MapSequenceFunctor<RightLookup>, RightOuterPerm>;
        using RightMappedFrontPerm = TransformedSequence<MapSequenceFunctor<RightIndexPositions>, FrontOuterPerm>;

        // Now inject the defect positions as identity at the proper locations
        using LeftInnerPerm = InsertAt<LeftMappedPerm, LeftMappedFrontPerm, LeftIndexPositions>;
        using RightInnerPerm = InsertAt<RightMappedPerm, RightMappedFrontPerm, RightIndexPositions>;

        using Type = ConditionalType<(N == 0), LeftInnerPerm, RightInnerPerm>;
      };

      template<class Perm, class T, class SFINAE = void>
      struct IsSelfTransposed
        : FalseType
      {};

      template<class Perm, class T>
      struct IsSelfTransposed<Perm, T, std::enable_if_t<(!IsDecay<Perm>::value || !IsDecay<T>::value)> >
        : IsSelfTransposed<std::decay_t<Perm>, std::decay_t<T> >
      {};

      /**Ignore do-nothing permutations.*/
      template<class Perm, class T>
      struct IsSelfTransposed<
        Perm, T,
        std::enable_if_t<(IsDecay<Perm>::value
                          && IsDecay<T>::value
                          && IsTensorOperand<T>::value
                          && isSimple(Perm{}))> >
        : TrueType
      {};

      /**Don't transpose constant tensors if their dimenensions are
       * invariant under the permutation.
       */
      template<class Perm, class F, class Dims>
      struct IsSelfTransposed<Perm, ConstantTensor<F, Dims>,
                              std::enable_if_t<(!isSimple(Perm{})
                                                && isInvariant(Dims{}, Perm{})
                                )> >
        : TrueType
      {};

      /**Don't transpose eye tensors if their dimensions are
       * invariant under the permutation.
       */
      template<class Perm, class F, class Dims>
      struct IsSelfTransposed<Perm, Eye<Dims, F>,
                              std::enable_if_t<(!isSimple(Perm{})
                                                && isInvariant(Dims{}, Perm{})
      )> >
        : TrueType
      {};

      /**Don't transpose constant tensors if their dimenensions are
       * invariant under the permutation.
       */
      template<std::size_t... Perm, class Field, std::size_t Rank, class Dims>
      struct IsSelfTransposed<Seq<Perm...>, BlockEye<Rank, Dims, Field>,
                              std::enable_if_t<(!isSimple(Seq<Perm...>{})
                                                && std::is_same<SequenceProd<Rank, MakeIndexSequence<Dims::size()> >, Seq<Get<Perm, SequenceProd<Rank, MakeIndexSequence<Dims::size()> > >::value...> >::value
                                )> >
        : TrueType
      {};

      /**Don't transpose constant tensors.*/
      template<class Perm, class Field, class Dims, class Index>
      struct IsSelfTransposed<Perm, KroneckerDelta<Dims, Index, Field>,
                              std::enable_if_t<(!isSimple(Perm{})
                                                && isInvariant(Dims{}, Perm{})
                                                && isInvariant(Index{}, Perm{})
        )> >
        : TrueType
      {};

      template<class Perm, std::size_t N, class T, class SFINAE = void>
      struct HasSelfTransposedOperand
        : FalseType
      {};

      template<class Perm, std::size_t N, class T>
      struct HasSelfTransposedOperand<
        Perm, N, T,
        std::enable_if_t<IsSelfTransposed<
                           Perm,
                           std::enable_if_t<(!Expressions::IsSelfExpression<T>::value
                                             && IsExpression<T>::value
                                             && Expressions::Arity<T>::value > N
                                             && Perm::size() <= TensorTraits<Operand<N, T> >::rank
                             ), Expressions::Operand<N, T> > >::value> >
        : TrueType
      {};

      /**Products of scalars with self-transposed tensors remain
       * self-transposed.
       */
      template<class Perm, class T>
      struct IsSelfTransposed<
        Perm, T,
        std::enable_if_t<(IsDecay<Perm>::value
                          && IsDecay<T>::value
                          && !isSimple(Perm{})
                          && IsProductExpression<T>::value
                          && ((TensorTraits<Expressions::Operand<0, T> >::rank == 0
                               &&
                               HasSelfTransposedOperand<Perm, 1, T>::value)
                              ||
                              (TensorTraits<Expressions::Operand<1, T> >::rank == 0
                               &&
                               HasSelfTransposedOperand<Perm, 0, T>::value))
          )> >
        : TrueType
      {};

      /**Sums- and differences of self-transposed tensors remain self-transposed.
       */
      template<class Perm, class T>
      struct IsSelfTransposed<
        Perm, T,
        std::enable_if_t<(IsDecay<Perm>::value
                          && IsDecay<T>::value
                          && !isSimple(Perm{})
                          && IsBinaryExpression<T>::value
                          && IsComponentWiseExpression<T>::value
                          && HasSelfTransposedOperand<Perm, 0, T>::value
                          && HasSelfTransposedOperand<Perm, 1, T>::value
          )> >
        : TrueType
      {};

      /**Products (ProductTensor, EinsteinSummation) are
       * selftransposed if the permutation respects the product
       * structure and the arguments are self-transposed with respect
       * to the induced permutation.
       */
      template<class Perm, class T>
      struct IsSelfTransposed<
        Perm, T,
        std::enable_if_t<(IsDecay<Perm>::value
                          && IsDecay<T>::value
                          && !isSimple(Perm{})
                          && IsProductExpression<T>::value
                          && (TensorTraits<Expressions::Operand<0, T> >::rank > 0
                              &&
                              TensorTraits<Expressions::Operand<1, T> >::rank > 0)
                          && HasSelfTransposedOperand<TranspositionForOperand<0, Perm, T>, 0, T>::value
                          && HasSelfTransposedOperand<TranspositionForOperand<1, Perm, T>, 1, T>::value
          )> >
        : TrueType
      {};

      /**Unary operations acting componentwise do not alter the
       * self-transposedness.
       */
      template<class Perm, class T>
      struct IsSelfTransposed<
        Perm, T,
        std::enable_if_t<(IsDecay<Perm>::value
                          && IsDecay<T>::value
                          && !isSimple(Perm{})
                          && IsUnaryExpression<T>::value
                          && IsComponentWiseExpression<T>::value
                          && HasSelfTransposedOperand<Perm, 0, T>::value
          )> >
        : TrueType
      {};

      /**Einsum operations over the same set of indices of identical
       * tensors are self-transposed when exchanging the complete
       * index set.
       */
      template<class Perm, class T>
      struct IsSelfTransposed<
        Perm, T,
        std::enable_if_t<(IsDecay<Perm>::value
                          && IsDecay<T>::value
                          && !isSimple(Perm{})
                          && IsEinsumExpression<T>::value
                          && IsRuntimeEqualExpression<Operand<0, T> >::value
                          && IsRuntimeEqualExpression<Operand<1, T> >::value
                          && std::is_same<std::decay_t<Operand<0, T> >, std::decay_t<Operand<1, T> > >::value
                          && std::is_same<typename T::LeftIndexPositions, typename T::RightIndexPositions>::value
                          && std::is_same<Perm, SequenceCat<typename T::RightArgs, typename T::LeftArgs> >::value
        )> >
        : TrueType
      {};

      template<std::size_t P0, std::size_t P1, std::size_t... RestPos, class T>
      constexpr bool isSelfTransposed(T&& t, Seq<P0, P1, RestPos...> = Seq<P0, P1, RestPos...>{})
      {
        return IsSelfTransposed<Seq<P0, P1, RestPos...>, T>::value;
      }

      template<class T>
      constexpr bool isSelfTransposed(T&& t)
      {
        return isSelfTransposed<1,0>(std::forward<T>(t));
      }

      ///@} TranspositionTraits

      ///@} TensorOperationTraits

      ///@} TensorOperations

    } // NS Tensor

    ///@} Tensors

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_TENSORS_OPERATIONS_TRANSPOSE_HH__
