#ifndef __DUNE_ACFEM_TENSORS_OPERATIONS_PLACEHOLDER_HH__
#define __DUNE_ACFEM_TENSORS_OPERATIONS_PLACEHOLDER_HH__

#include "../../expressions/optimizationbase.hh"
#include "../tensorbase.hh"
#include "../expressiontraits.hh"
#include "../unaryexpression.hh"

namespace Dune {

  namespace ACFem {

    namespace Tensor {

      /**Generate a placeholder tensor wrapping a T.
       *
       * - One use of placeholder tensors is to define an
       *   "unevaluated" object with a special derivative. This is,
       *   e.g., used to inject Fem functions into the
       *   tensor-expression framework by wrapping them in to a
       *   placeholder tensor with a special derivative.
       * - Another use is during auto-differentiation where a
       *   place-holder is used to store a nested chain of (higher)
       *   derivative values and overloads the derivate() call to just
       *   return the pre-evaluated derivative it stores
       * - Further, this can be used to implement a chain-rule in
       *   order to differentiate intergrands of variational
       *   formulations w.r.t. either the space or time variables
       *   while not loosing the possibility to differentiate
       *   w.r.t. to the unknown function for computing the first
       *   variation.
       */
      template<class T, class Placeholder = T, std::enable_if_t<IsProperTensor<T>::value, int> = 0>
      auto placeholder(T&& t, PlaceholderOperation<Placeholder> = PlaceholderOperation<Placeholder>{})
      {
        static_assert(TensorTraits<T>::signature() == TensorTraits<Placeholder>::signature(),
                      "Signature of placeholder and storage tensor must match.");
        return finalize(
          Expressions::DontOptimize{},
          OperationTraits<PlaceholderOperation<Placeholder> >{},
          std::forward<T>(t));
      }

      //Forward
      template<class Functor, class T>
      class UnaryTensorExpression;

      /**The type of the associated placeholder.*/
      template<class T, class Placeholder>
      using PlaceholderTensor = UnaryTensorExpression<OperationTraits<PlaceholderOperation<Placeholder> >, T>;

      template<class T, class SFINAE = void>
      struct IsPlaceholderTensor
        : FalseType
      {};

      template<class T>
      struct IsPlaceholderTensor<T, std::enable_if_t<!IsDecay<T>::value> >
        : IsPlaceholderTensor<std::decay_t<T> >
      {};

      template<class T>
      struct IsPlaceholderTensor<
        T,
        std::enable_if_t<(IsDecay<T>::value
                          && IsPlaceholderExpression<T>::value
                          && IsTensor<T>::value
          )> >
        : TrueType
      {};

    } // NS Tensor

    using Tensor::placeholder;

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_TENSORS_OPERATIONS_PLACEHOLDER_HH__
