#ifndef __DUNE_ACFEM_TENSORS_EXPRESSIONOPERATIONS_HH__
#define __DUNE_ACFEM_TENSORS_EXPRESSIONOPERATIONS_HH__

/**@file Add-ons for expressions operations which do not make sense without tensors.*/

#include "../mpl/compare.hh"
#include "../expressions/storage.hh"
#include "../expressions/expressionoperations.hh"
#include "operations/einsum.hh"
#include "operations/product.hh"
#include "operations/reshape.hh"
#include "operations/restriction.hh"
#include "operations/transpose.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup ExpressionTemplates
     *
     * @{
     *
     */

    /**@addtogroup ExpressionOperations
     *
     * "Tag"-structures for each supported algebraic operation, used
     * to distinguish the various expression template classes from
     * each other.
     *
     * @{
     */

    template<class T>
    struct IsTensorOperation
      : FalseType
    {};

    ///////////////////////////////////////////////////////////////////////////

    /**AutoDiff operation. This is a little bit like SubExpression,
     * but is intended for use during forward auto-diff. An expression
     * with this operation will hold the values in operand 0 and the
     * derivative in operand 1, where the actuall type of value and
     * derivative are unspecified. Support for higher order autodiff
     * is achieved by using again an auto-diff operation for operand
     * 1. The storage type is left unspecified here. The corresponding
     * BinaryTensorExpression will provide storage space.
     */

    template<class Dims, class Pivot>
    struct RestrictionOperation
    {};

    template<class Dims, class Pivot>
    struct IsTensorOperation<RestrictionOperation<Dims, Pivot> >
      : TrueType
    {};

    template<class T>
    struct IsRestrictionOperation
      : FalseType
    {};

    template<class Dims, class Pivot>
    struct IsRestrictionOperation<RestrictionOperation<Dims, Pivot> >
      : TrueType
    {};

    template<class T, class SFINAE = void>
    struct IsRestrictionExpression
      : FalseType
    {};

    template<class T>
    struct IsRestrictionExpression<
      T,
      std::enable_if_t<(IsExpressionOfArity<1, T>::value
                        && IsRestrictionOperation<Expressions::Operation<T> >::value
      )> >
      : TrueType
    {};

    template<class T, class SFINAE = void>
    struct RestrictionTraits
    {};

    template<class Dims, class Pivot>
    struct RestrictionTraits<RestrictionOperation<Dims, Pivot> >
    {
      using DefectPositions = Dims;
      using PivotSequence = Pivot;
    };

    template<class Dims, class Pivot>
    struct RestrictionTraits<OperationTraits<RestrictionOperation<Dims, Pivot> > >
      : RestrictionTraits<RestrictionOperation<Dims, Pivot> >
    {};

    template<class T>
    struct RestrictionTraits<T, std::enable_if_t<IsRestrictionExpression<T>::value> >
      : RestrictionTraits<Expressions::Operation<T> >
    {};

    template<class T, class SFINAE = void>
    struct IsConstRestrictionOperation
      : FalseType
    {};

    template<class T>
    struct IsConstRestrictionOperation<
      T,
      std::enable_if_t<(IsRestrictionOperation<T>::value
                        && (RestrictionTraits<T>::DefectPositions::size()
                            ==
                            RestrictionTraits<T>::PivotSequence::size())
        )> >
      : TrueType
    {};

    template<class T, class SFINAE = void>
    struct IsConstRestrictionExpression
      : FalseType
    {};

    template<class T>
    struct IsConstRestrictionExpression<
      T,
      std::enable_if_t<(IsRestrictionExpression<T>::value
                        && IsConstRestrictionOperation<Expressions::Operation<T> >::value
      )> >
      : TrueType
    {};

    template<class T, class SFINAE = void>
    struct IsDynamicRestrictionExpression
      : FalseType
    {};

    template<class T>
    struct IsDynamicRestrictionExpression<
      T,
      std::enable_if_t<(IsRestrictionExpression<T>::value
                        && !IsConstRestrictionOperation<Expressions::Operation<T> >::value
      )> >
      : TrueType
    {};

    template<class T>
    struct IsDynamicOperation<
      T,
      std::enable_if_t<(IsRestrictionOperation<T>::value
                        && !IsConstRestrictionOperation<T>::value
                        && (RestrictionTraits<T>::DefectPositions::size() > 0)
        )> >
      : TrueType
    {};

    ///////////////////////////////////////////////////////////////////////////

    /**Permutation of index positions of tensors.
     *
     * @param Perm The permuation to apply to the indices.
     */
    template<class Perm>
    struct TransposeOperation
    {
      static_assert(isPermutation(Perm{}),
                    "Given sequence is not a permutation of 0, 1, ...");
    };

    template<class Perm>
    struct IsTensorOperation<TransposeOperation<Perm> >
      : TrueType
    {};

    template<class T>
    struct IsTransposeOperation
      : FalseType
    {};

    template<class Perm>
    struct IsTransposeOperation<TransposeOperation<Perm> >
      : TrueType
    {};

    template<class T, class SFINAE = void>
    struct IsTransposeExpression
      : FalseType
    {};

    template<class T>
    struct IsTransposeExpression<
      T,
      std::enable_if_t<(IsExpression<T>::value
                        && Expressions::Arity<T>::value == 1
                        && IsTransposeOperation<Expressions::Operation<T> >::value
      )> >
      : TrueType
    {};

    template<class T, class SFINAE = void>
    struct TransposeTraits
    {
      // using Permutation = void;
    };

    template<class Perm>
    struct TransposeTraits<TransposeOperation<Perm> >
    {
      using Permutation = Perm;
    };

    template<class Perm>
    struct TransposeTraits<OperationTraits<TransposeOperation<Perm> > >
      : TransposeTraits<TransposeOperation<Perm> >
    {};

    template<class T>
    struct TransposeTraits<T, std::enable_if_t<IsTransposeExpression<T>::value> >
      : TransposeTraits<Expressions::Operation<T> >
    {};

    ///////////////////////////////////////////////////////////////////////////

    /**Signature of index positions of tensors.
     *
     * @param Perm The permuation to apply to the indices.
     */
    template<class Signature>
    struct ReshapeOperation
    {};

    template<class Signature>
    struct IsTensorOperation<ReshapeOperation<Signature> >
      : TrueType
    {};

    template<class T>
    struct IsReshapeOperation
      : FalseType
    {};

    template<class Sig>
    struct IsReshapeOperation<ReshapeOperation<Sig> >
      : TrueType
    {};

    template<class T, class SFINAE = void>
    struct IsReshapeExpression
      : FalseType
    {};

    template<class T>
    struct IsReshapeExpression<T, std::enable_if_t<!IsDecay<T>::value> >
      : IsReshapeExpression<std::decay_t<T> >
    {};

    template<class T>
    struct IsReshapeExpression<
      T,
      std::enable_if_t<(IsDecay<T>::value
                        && IsExpression<T>::value
                        && Expressions::Arity<T>::value == 1
                        && IsReshapeOperation<Expressions::Operation<T> >::value
      )> >
      : TrueType
    {};

    template<class T, class SFINAE = void>
    struct ReshapeTraits
    {
      // using Signature = void;
    };

    template<class Sig>
    struct ReshapeTraits<ReshapeOperation<Sig> >
    {
      using Signature = Sig;
    };

    template<class Sig>
    struct ReshapeTraits<OperationTraits<ReshapeOperation<Sig> > >
      : ReshapeTraits<ReshapeOperation<Sig> >
    {};

    template<class T>
    struct ReshapeTraits<T, std::enable_if_t<IsReshapeExpression<T>::value> >
      : ReshapeTraits<Expressions::Operation<T> >
    {};

    ///////////////////////////////////////////////////////////////////////////

    /**Einstein summation, i.e. contraction of tensors over given
     * index positions.
     *
     * @param Pos1 Index positions in first tensor.
     *
     * @param Pos2 Index positions in second tensor.
     *
     * @param ContractDims Shared dimensions of the index positions
     * specified in @a Pos1 and @a Pos2.
     *
     * Requirements: size<Pos1>() == size<Pos2>() == size<ContractDims>().
     */
    template<class Pos1, class Pos2, class ContractDims>
    struct EinsumOperation
    {
      static_assert(Pos1::size() == Pos2::size() && Pos2::size() == ContractDims::size(),
                    "Number of contraction indices and dimensions differs.");
    };

    template<class Pos1, class Pos2, class ContractDims>
    struct IsTensorOperation<EinsumOperation<Pos1, Pos2, ContractDims> >
      : TrueType
    {};

    template<class T>
    struct IsEinsumOperation
      : FalseType
    {};

    template<class Pos1, class Pos2, class ContractDims>
    struct IsEinsumOperation<EinsumOperation<Pos1, Pos2, ContractDims> >
      : TrueType
    {};

    template<class T>
    constexpr inline bool HasEinsumFunctorV = IsEinsumOperation<typename T::FunctorType::OperationType>::value;

    template<class T, class SFINAE = void>
    struct IsEinsumExpression
      : FalseType
    {};

    template<class T>
    struct IsEinsumExpression<
      T,
      std::enable_if_t<(IsExpression<T>::value
                        && Expressions::Arity<T>::value == 2
                        && IsEinsumOperation<Expressions::Operation<T> >::value
      )> >
      : TrueType
    {};

    template<class T, class SFINAE = void>
    struct EinsumTraits
    {};

    template<class Pos1, class Pos2, class Dims>
    struct EinsumTraits<EinsumOperation<Pos1, Pos2, Dims> >
    {
      static constexpr std::size_t defectRank_ = Dims::size();
      using LeftIndexPositions = Pos1;
      using RightIndexPositions = Pos2;
      using Dimensions = Dims;
    };

    template<class Pos1, class Pos2, class Dims>
    struct EinsumTraits<OperationTraits<EinsumOperation<Pos1, Pos2, Dims> > >
      : EinsumTraits<EinsumOperation<Pos1, Pos2, Dims> >
    {};

    template<class T>
    struct EinsumTraits<T, std::enable_if_t<IsEinsumExpression<T>::value> >
      : EinsumTraits<Expressions::Operation<T> >
    {};

    template<class Pos1, class Pos2, class Dims>
    struct IsProductOperation<EinsumOperation<Pos1, Pos2, Dims> >
      : TrueType
    {};

    ///////////////////////////////////////////////////////////////////////////

    /**Component-wise product over given index-set.
     *
     * @param Pos1 Index positions in first tensor.
     *
     * @param Pos2 Index positions in second tensor.
     *
     * @param ProductDims Shared dimensions of the index positions
     * specified in @a Pos1 and @a Pos2.
     *
     * Requirements: size<Pos1>() == size<Pos2>() == size<ProductDims>().
     */
    template<class Pos1, class Pos2, class ProductDims>
    struct TensorProductOperation
    {};

    template<class Pos1, class Pos2, class ProductDims>
    struct IsTensorOperation<TensorProductOperation<Pos1, Pos2, ProductDims> >
      : TrueType
    {};

    template<class T>
    struct IsTensorProductOperation
      : FalseType
    {};

    template<class Pos1, class Pos2, class ContractDims>
    struct IsTensorProductOperation<TensorProductOperation<Pos1, Pos2, ContractDims> >
      : TrueType
    {};

    template<class T, class SFINAE = void>
    struct IsTensorProductExpression
      : FalseType
    {};

    template<class T>
    struct IsTensorProductExpression<
      T,
      std::enable_if_t<(IsExpression<T>::value
                        && Expressions::Arity<T>::value == 2
                        && IsTensorProductOperation<Expressions::Operation<T> >::value
      )> >
      : TrueType
    {};

    template<class T>
    struct TensorProductTraits
    {};

    template<class Pos1, class Pos2, class Dims>
    struct TensorProductTraits<TensorProductOperation<Pos1, Pos2, Dims> >
    {
      static constexpr std::size_t defectRank_ = Dims::size();
      using LeftIndexPositions = Pos1;
      using RightIndexPositions = Pos2;
      using Dimensions = Dims;
    };

    template<class Pos1, class Pos2, class Dims>
    struct TensorProductTraits<OperationTraits<TensorProductOperation<Pos1, Pos2, Dims> > >
      : TensorProductTraits<TensorProductOperation<Pos1, Pos2, Dims> >
    {};

    template<class Pos1, class Pos2, class Dims>
    struct IsProductOperation<TensorProductOperation<Pos1, Pos2, Dims> >
      : TrueType
    {};

    ///////////////////////////////////////////////////////////////////////////

    namespace Tensor {

      template<class T, class SFINAE = void>
      struct IsComponentWiseOperation
        : TrueType
      {};

      /**Products, restrictions and transpositions do not act
       * componentwise.
       */
      template<class T>
      struct IsComponentWiseOperation<
        T,
        std::enable_if_t<(IsReshapeOperation<T>::value
                          || IsTransposeOperation<T>::value
                          || IsRestrictionOperation<T>::value
                          || IsProductOperation<T>::value
        )> >
        : FalseType
      {};

      template<class T, class SFINAE = void>
      struct IsProductWithScalarExpression
        : FalseType
      {};

      template<class T>
      struct IsProductWithScalarExpression<
        T,
        std::enable_if_t<(IsProductExpression<T>::value
                          && (TensorTraits<Operand<0, T> >::rank == 0
                              ||
                              TensorTraits<Operand<1, T> >::rank == 0)
        )> >
        : TrueType
      {};

      template<class T, class SFINAE = void>
      struct IsComponentWiseExpression
        : IsComponentWiseOperation<Expressions::Operation<T> >
      {};

      template<class T>
      struct IsComponentWiseExpression<T, std::enable_if_t<IsProductWithScalarExpression<T>::value> >
        : TrueType
      {};

    }

    template<class Operation>
    struct IsDistributiveOperation<
      Operation,
      std::enable_if_t<(IsTransposeOperation<Operation>::value
                        || IsRestrictionOperation<Operation>::value
      )> >
      : TrueType
    {};

    //! @} ExpressionOperations

    //! @} ExpressionTemplates

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_TENSORS_EXPRESSIONOPERATIONS_HH__
