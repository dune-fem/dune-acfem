#ifndef __DUNE_ACFEM_TENSOR_MODULES_HH__
#define __DUNE_ACFEM_TENSOR_MODULES_HH__

#include "modules/blockeye.hh"
#include "modules/constant.hh"
#include "modules/eye.hh"
#include "modules/kronecker.hh"
#include "modules/linearstorage.hh"

#endif // __DUNE_ACFEM_TENSOR_MODULES_HH__
