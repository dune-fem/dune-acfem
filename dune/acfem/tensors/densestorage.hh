#ifndef __DUNE_ACFEM_TENSORS_DENSESTORAGE_HH__
#define __DUNE_ACFEM_TENSORS_DENSESTORAGE_HH__

#include "bindings.hh"

namespace Dune::ACFem::Expressions {

  template<class T>
  struct DenseStorage<T, std::enable_if_t<IsTensorOperand<T>::value> >
  {
    using Type = Tensor::DenseStorage<T>;
  };

} // Dune::ACFem::Expressions::

#endif // __DUNE_ACFEM_TENSORS_DENSESTORAGE_HH__
