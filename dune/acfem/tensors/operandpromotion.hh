#ifndef __DUNE_ACFEM_TENSORS_OPERANDPROMOTION_HH__
#define __DUNE_ACFEM_TENSORS_OPERANDPROMOTION_HH__

#include "tensorbase.hh"
#include "expressiontraits.hh"
#include "expressions.hh"

namespace Dune
{
  namespace ACFem
  {
    namespace Tensor
    {
      template<std::size_t N, class... T,
               std::enable_if_t<(// Handled top-level
                                 !AnyIs<IsPromotedTopLevel, T...>::value
                                 // Don't if all arguments are allready tensors
                                 && !AllAre<IsTensor, T...>::value
                                 && ((// One is alread a tensor
                                         AnyIs<IsTensor, T...>::value
                                         // All are admissible in principle
                                         && AllAre<IsTensorOperand, T...>::value)
                                     || (// All are unary admissible
                                         AllAre<Tensor::IsUnaryTensorOperand, T...>::value)
                                   )
                 ), int> = 0>
      constexpr decltype(auto) operandPromotion(T&&... t)
      {
        return tensor(std::forward<TupleElement<N, std::tuple<T...> > >(
                        get<N>(std::forward_as_tuple(std::forward<T>(t)...))
                        )
          );
      }

      //!Promote operands to tensor einsum operation
      template<class Seq0, class Seq1, class T0, class T1,
               std::enable_if_t<(sizeof(
                                   einsum<Seq0, Seq1>(
                                     operandPromotion<0>(std::declval<T0>(), std::declval<T1>()),
                                     operandPromotion<1>(std::declval<T0>(), std::declval<T1>())
                                     )
                                   ) >= 0
        ), int> = 0>
      constexpr decltype(auto) einsum(T0&& t0, T1&& t1)
      {
        return einsum<Seq0, Seq1>(
          operandPromotion<0>(std::forward<T0>(t0), std::forward<T1>(t1)),
          operandPromotion<1>(std::forward<T0>(t0), std::forward<T1>(t1))
          );
      }

      //!Promote operands to tensor einsum operation
      template<class T0, class T1,
               std::enable_if_t<(sizeof(
                                   einsum(
                                     operandPromotion<0>(std::declval<T0>(), std::declval<T1>()),
                                     operandPromotion<1>(std::declval<T0>(), std::declval<T1>())
                                     )
                                   ) >= 0
        ), int> = 0>
      constexpr decltype(auto) einsum(T0&& t0, T1&& t1)
      {
        return einsum(
          operandPromotion<0>(std::forward<T0>(t0), std::forward<T1>(t1)),
          operandPromotion<1>(std::forward<T0>(t0), std::forward<T1>(t1))
          );
      }

      //!Promote operands to tensor outer product operation
      template<class T0, class T1,
               std::enable_if_t<(sizeof(
                                   outer(
                                     operandPromotion<0>(std::declval<T0>(), std::declval<T1>()),
                                     operandPromotion<1>(std::declval<T0>(), std::declval<T1>())
                                     )
                                   ) >= 0
        ), int> = 0>
      constexpr decltype(auto) outer(T0&& t0, T1&& t1)
      {
        return outer(
          operandPromotion<0>(std::forward<T0>(t0), std::forward<T1>(t1)),
          operandPromotion<1>(std::forward<T0>(t0), std::forward<T1>(t1))
          );
      }

      //!Promote operands to tensor inner product operation
      template<std::size_t N, class T0, class T1,
               std::enable_if_t<(sizeof(
                                   contractInner(
                                     operandPromotion<0>(std::declval<T0>(), std::declval<T1>()),
                                     operandPromotion<1>(std::declval<T0>(), std::declval<T1>()),
                                     IndexConstant<N>{}
                                     )
                                   ) >= 0
        ), int> = 0>
      constexpr decltype(auto) contractInner(T0&& t0, T1&& t1)
      {
        return contractInner<N>(
          operandPromotion<0>(std::forward<T0>(t0), std::forward<T1>(t1)),
          operandPromotion<1>(std::forward<T0>(t0), std::forward<T1>(t1))
          );
      }

      //!Promote operands to tensor inner contraction operation
      template<class T0, class T1,
               std::enable_if_t<(sizeof(
                                   contractInner(
                                     operandPromotion<0>(std::declval<T0>(), std::declval<T1>()),
                                     operandPromotion<1>(std::declval<T0>(), std::declval<T1>())
                                     )
                                   ) >= 0
        ), int> = 0>
      constexpr decltype(auto) contractInner(T0&& t0, T1&& t1)
      {
        return contractInner(
          operandPromotion<0>(std::forward<T0>(t0), std::forward<T1>(t1)),
          operandPromotion<1>(std::forward<T0>(t0), std::forward<T1>(t1))
          );
      }

      //!Promote operands to tensor inner product operation
      template<class T0, class T1,
               std::enable_if_t<(sizeof(
                                   inner(
                                     operandPromotion<0>(std::declval<T0>(), std::declval<T1>()),
                                     operandPromotion<1>(std::declval<T0>(), std::declval<T1>())
                                     )
                                   ) >= 0
        ), int> = 0>
      constexpr decltype(auto) inner(T0&& t0, T1&& t1)
      {
        return inner(
          operandPromotion<0>(std::forward<T0>(t0), std::forward<T1>(t1)),
          operandPromotion<1>(std::forward<T0>(t0), std::forward<T1>(t1))
          );
      }

      //!Promote operands to tensor trace operation
      template<class T0,
               std::enable_if_t<(sizeof(
                                   trace(
                                     operandPromotion<0>(std::declval<T0>())
                                     )
                                   ) >= 0
        ), int> = 0>
      constexpr decltype(auto) trace(T0&& t0)
      {
        return trace(operandPromotion<0>(std::forward<T0>(t0)));
      }

      //!Promote operands to tensor trace operation
      template<class T0,
               std::enable_if_t<(sizeof(
                                   frobenius2(
                                     operandPromotion<0>(std::declval<T0>())
                                     )
                                   ) >= 0
        ), int> = 0>
      constexpr decltype(auto) frobenius2(T0&& t0)
      {
        return frobenius2(operandPromotion<0>(std::forward<T0>(t0)));
      }

      //!Promote operands to tensor multiply operation
      template<class Seq0, class Seq1, class T0, class T1,
               std::enable_if_t<(sizeof(
                                   multiply<Seq0, Seq1>(
                                     operandPromotion<0>(std::declval<T0>(), std::declval<T1>()),
                                     operandPromotion<1>(std::declval<T0>(), std::declval<T1>())
                                     )
                                   ) >= 0
        ), int> = 0>
      constexpr decltype(auto) multiply(T0&& t0, T1&& t1)
      {
        return multiply<Seq0, Seq1>(
          operandPromotion<0>(std::forward<T0>(t0), std::forward<T1>(t1)),
          operandPromotion<1>(std::forward<T0>(t0), std::forward<T1>(t1))
          );
      }

      //!Promote operands to tensor multiply operation
      template<class T0, class T1,
               std::enable_if_t<(sizeof(
                                   multiply(
                                     operandPromotion<0>(std::declval<T0>(), std::declval<T1>()),
                                     operandPromotion<1>(std::declval<T0>(), std::declval<T1>())
                                     )
                                   ) >= 0
        ), int> = 0>
      constexpr decltype(auto) multiply(T0&& t0, T1&& t1)
      {
        return multiply(
          operandPromotion<0>(std::forward<T0>(t0), std::forward<T1>(t1)),
          operandPromotion<1>(std::forward<T0>(t0), std::forward<T1>(t1))
          );
      }

      //! argument promotion to tensors for reshape()
      template<std::size_t... Sig, class T,
               std::enable_if_t<(sizeof(
                                   reshape<Sig...>(
                                     operandPromotion<0>(std::declval<T>())
                                     )
                                   ) >= 0
        ), int> = 0>
      constexpr decltype(auto) reshape(T&& t, Seq<Sig...> = Seq<Sig...>{})
      {
        return reshape<Sig...>(operandPromotion<0>(std::forward<T>(t)));
      }

      //!Promote operands to tensor restriction operation
      template<std::size_t... IndexPositions, class T, std::size_t... Indices,
               std::enable_if_t<(sizeof...(IndexPositions) == sizeof...(Indices)
                                 && sizeof(
                                   restriction<IndexPositions...>(
                                     operandPromotion<0>(std::declval<T>()),
                                     Seq<Indices...>{}
                                     )
                                   ) >= 0
        ), int> = 0>
      constexpr decltype(auto) restriction(T&& t, Seq<Indices...>, Seq<IndexPositions...> = Seq<IndexPositions...>{})
      {
        return restriction<IndexPositions...>(
          operandPromotion<0>(std::forward<T>(t)),
          Seq<Indices...>{}
          );
      }

      /**Generate a compile-time dynamic restriction with position
       * given from a tuple-like object.
       */
      template<std::size_t... IndexPositions, class T, class Indices,
               std::enable_if_t<(IsTupleLike<Indices>::value
                                 && sizeof(
                                   restriction<IndexPositions...>(
                                     operandPromotion<0>(std::declval<T>()),
                                     std::declval<Indices>()
                                     )
                                   ) >= 0
        ), int> = 0>
      auto restriction(T&& t, Indices&& indices, Seq<IndexPositions...> = Seq<IndexPositions...>{})
      {
        return restriction<IndexPositions...>(
          operandPromotion<0>(std::forward<T>(t)),
          std::forward<Indices>(indices)
          );
      }

      /**Generate a compile-time dynamic restriction with position
       * given from an initializer list
       */
      template<std::size_t... IndexPositions, class T, class I, std::size_t N,
               std::enable_if_t<(sizeof(
                                   restriction<IndexPositions...>(
                                     operandPromotion<0>(std::declval<T>()),
                                     std::declval<const I (&)[N]>()
                                     )
                                   ) >= 0
                                ), int> = 0>
      auto restriction(T&& t, const I (&l)[N], Seq<IndexPositions...> = Seq<IndexPositions...>{})
      {
        return restriction<IndexPositions...>(
          operandPromotion<0>(std::forward<T>(t)),
          l
          );
      }

      /**Generate a compile-time dynamic restriction without positions.
       */
      template<std::size_t... IndexPositions, class T,
               std::enable_if_t<(sizeof(
                                   restriction<IndexPositions...>(
                                     operandPromotion<0>(std::declval<T>())
                                     )
                                   ) >= 0
        ), int> = 0>
      auto restriction(T&& t)
      {
        return restriction<IndexPositions...>(
          operandPromotion<0>(std::forward<T>(t))
          );
      }

      /**Generate a compile-time dynamic restriction without positions.
       */
      template<class T, class DefectPositions,
               std::enable_if_t<(sizeof(
                                   restriction(
                                     operandPromotion<0>(std::declval<T>()),
                                     DefectPositions{}
                                     )
                                   ) >= 0
        ), int> = 0>
      auto restriction(T&& t, DefectPositions)
      {
        return restriction(
          operandPromotion<0>(std::forward<T>(t), DefectPositions{})
          );
      }

      //!Promote operands to tensor transposition operation
      template<std::size_t... Perm, class T,
               std::enable_if_t<(sizeof(
                                   transpose<Perm...>(
                                     operandPromotion<0>(std::declval<T>())
                                     )
                                   ) >= 0
        ), int> = 0>
      constexpr decltype(auto) transpose(T&& t, Seq<Perm...> = Seq<Perm...>{})
      {
        return transpose<Perm...>(operandPromotion<0>(std::forward<T>(t)));
      }

      template<class F, class T0, class T1,
               class OptimizeOuter = Expressions::OptimizeTop,
               class OptimizeInner = Expressions::OptimizeTop,
               std::enable_if_t<(sizeof(
                                   associateRight(
                                     std::declval<F>(),
                                     operandPromotion<0>(std::declval<T0>(), std::declval<T1>()),
                                     operandPromotion<1>(std::declval<T0>(), std::declval<T1>()),
                                     OptimizeOuter{}, OptimizeInner{}
                                     )
                                   ) >= 0
        ), int> = 0>
      constexpr decltype(auto) associateRight(F&& f, T0&& t0, T1&& t1,
                                              OptimizeOuter = OptimizeOuter{},
                                              OptimizeInner = OptimizeInner{})
      {
        return associateRight(
          std::forward<F>(f),
          operandPromotion<0>(std::forward<T0>(t0), std::forward<T1>(t1)),
          operandPromotion<1>(std::forward<T0>(t0), std::forward<T1>(t1)),
          OptimizeOuter{}, OptimizeInner{}
          );
      }

      template<class F, class T0, class T1,
               class OptimizeOuter = Expressions::OptimizeTop,
               class OptimizeInner = Expressions::OptimizeTop,
               std::enable_if_t<(sizeof(
                                   associateLeft(
                                     std::declval<F>(),
                                     operandPromotion<0>(std::declval<T0>(), std::declval<T1>()),
                                     operandPromotion<1>(std::declval<T0>(), std::declval<T1>()),
                                     OptimizeOuter{}, OptimizeInner{}
                                     )
                                   ) >= 0
        ), int> = 0>
      constexpr decltype(auto) associateLeft(F&& f, T0&& t0, T1&& t1,
                                             OptimizeOuter = OptimizeOuter{},
                                             OptimizeInner = OptimizeInner{})
      {
        return associateLeft(
          std::forward<F>(f),
          operandPromotion<0>(std::forward<T0>(t0), std::forward<T1>(t1)),
          operandPromotion<1>(std::forward<T0>(t0), std::forward<T1>(t1)),
          OptimizeOuter{}, OptimizeInner{}
          );
      }

    } // NS Tensor

    using Tensor::reshape;
    using Tensor::restriction;
    using Tensor::transpose;
    using Tensor::outer;
    using Tensor::inner;
    using Tensor::contractInner;
    using Tensor::trace;
    using Tensor::frobenius2;
    using Tensor::einsum;

  } // NS ACFem

  using ACFem::Tensor::operandPromotion;

} // NS Dune

#endif //  __DUNE_ACFEM_TENSORS_OPERANDPROMOTION_HH__
