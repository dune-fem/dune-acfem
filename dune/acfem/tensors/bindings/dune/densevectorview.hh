#ifndef __DUNE_ACFEM_TENSORS_BINDINGS_DUNE_DENSEVECTORVIEW_HH__
#define __DUNE_ACFEM_TENSORS_BINDINGS_DUNE_DENSEVECTORVIEW_HH__

#include <dune/common/densevector.hh>
//#include "../../../expressions/operationtraits.hh"
#include "../../../common/literals.hh"
#include "../../tensorbase.hh"
#include "fieldvectortraits.hh"

namespace Dune {

  namespace ACFem {

    namespace Tensor {

      // forward
      template<class Tensor>
      class TensorResult;

      template<class T, class Storage = Expressions::Storage<OperationTraits<IdentityOperation>, T>,
               std::size_t N = std::decay_t<T>::rank,
               std::size_t Rank = std::decay_t<T>::rank,
               class SFINAE = void>
      class DenseVectorView;

      template<class T, class Storage, std::size_t N, std::size_t Rank>
      class DenseVectorViewElement;

      /**@internal Upcast recursion into elements.*/
      template<class T, class Storage, std::size_t N, std::size_t Rank>
      class DenseVectorViewElement
        : public DenseVectorView<T, Storage, N, Rank>
        , public Dune::DenseVector<DenseVectorViewElement<T, Storage, N, Rank> >
      {
        using ThisType = DenseVectorViewElement;
        using DenseVectorType = Dune::DenseVector<DenseVectorViewElement<T, Storage, N, Rank> >;
        using ValueBaseType = DenseVectorView<T, Storage, N-1, Rank>;
        using BaseType = DenseVectorView<T, Storage, N, Rank>;
       public:
        using const_iterator = void;
        using BaseType::lookAt;
        using ValueType = DenseVectorViewElement<T, Storage, N-1, Rank>;
        using IndexType = typename BaseType::IndexType;

        static constexpr IndexType rank_ = Rank;
        static constexpr IndexType fakedRank_ = N;
        static constexpr IndexType stage_ = std::decay_t<T>::rank - N;

        template<class... Args>
        DenseVectorViewElement(Args&&...) = delete;

        template<class C, std::enable_if_t<std::is_base_of<ThisType, C>::value, int> = 0>
        DenseVectorViewElement(const C&) = delete;

        static constexpr std::size_t size()
        {
          return Get<stage_, typename std::decay_t<T>::Signature>::value;
        }

        const ValueType& operator[](IndexType i) const
        {
          lookAt()[stage_] = i;
          return static_cast<const ValueType&>(static_cast<const ValueBaseType&>(*this));
        }
      };

      /**Recursion endpoint for elements.*/
      template<class T, class Storage, std::size_t Rank>
      class DenseVectorViewElement<T, Storage, 1, Rank>
        : public DenseVectorView<T, Storage, 1, Rank>
        , public Dune::DenseVector<DenseVectorViewElement<T, Storage, 1, Rank> >
      {
        using ThisType = DenseVectorViewElement;
        using BaseType = DenseVectorView<T, Storage, 1, Rank>;
        using DenseVectorType = Dune::DenseVector<DenseVectorViewElement<T, Storage, 1, Rank> >;
        using BaseType::lookAt;
        using BaseType::value_;
       public:
        using const_iterator = void;
        using BaseType::operand;
        using typename BaseType::ValueType;
        using IndexType = typename BaseType::IndexType;

        static constexpr IndexType rank_ = Rank;
        static constexpr IndexType fakedRank_ = 1;
        static constexpr IndexType stage_ = std::decay_t<T>::rank - fakedRank_;

        template<class... Args>
        DenseVectorViewElement(Args&&...) = delete;

        template<class C, std::enable_if_t<std::is_base_of<ThisType, C>::value, int> = 0>
        DenseVectorViewElement(const C&) = delete;

        static constexpr std::size_t size()
        {
          return TensorTraits<T>::template dim<stage_>();
        }

        const ValueType& operator[](IndexType i) const
        {
          lookAt()[stage_] = i;
          return value_ = tensorValue(operand(0_c), lookAt());
        }

      };

      /**@internal Downcast recursion until storage is reached. This
       * class does not store any data.
       */
      template<class T, class Storage, std::size_t N, std::size_t Rank, class SFINAE>
      class DenseVectorView
        : public DenseVectorView<T, Storage, N-1, Rank>
      {
        static_assert(N > 1, "Recursion bug.");
        static_assert(TensorTraits<T>::rank > 0, "This can only work if rank > 0.");

        using ThisType = DenseVectorView;
        using BaseType = DenseVectorView<T, Storage, N-1, Rank>;
      public:
        using ValueType = DenseVectorViewElement<T, Storage, N-1, Rank>;
        using IndexType = unsigned;

        static constexpr IndexType rank_ = Rank;
        static constexpr IndexType fakedRank_ = N;
        static constexpr IndexType stage_ = TensorTraits<T>::rank - N;

        template<class StorageArg,
                 std::enable_if_t<(std::is_constructible<Storage, StorageArg>::value
                                   && !SameDecay<StorageArg, ThisType>::value
                   ), int> = 0>
        explicit DenseVectorView(StorageArg&& storage)
          : BaseType(std::forward<StorageArg>(storage))
        {}

        DenseVectorView(const DenseVectorView& other)
          : BaseType(other)
        {}

        DenseVectorView(DenseVectorView&& other)
          : BaseType(std::move(other))
        {}

        template<class C, std::enable_if_t<std::is_base_of<ThisType, C>::value, int> = 0>
        explicit DenseVectorView(const C& other)
          : DenseVectorView(static_cast<const ThisType&>(other))
        {}

        static constexpr std::size_t size()
        {
          return TensorTraits<T>::template dim<stage_>();
        }

        auto& lookAt() const
        {
          return BaseType::lookAt();
        }
      };

      /**Storage provider, downcast end-point.
       *
       * This class provides the storage for T.
       */
      template<class T, class Storage, std::size_t Rank>
      class DenseVectorView<T, Storage, 1, Rank, std::enable_if_t<Rank != 1> >
        : public Storage
      {
        static_assert(std::decay_t<T>::rank > 0, "This can only work if rank > 0.");

        using ThisType = DenseVectorView;
       public:
        using ValueType = typename std::decay_t<T>::FieldType;
        using IndexType = unsigned;
        using IndicesType = std::array<IndexType, std::decay_t<T>::rank>;

        static constexpr IndexType rank_ = Rank;
        static constexpr IndexType fakedRank_ = 1;
        static constexpr IndexType stage_ = std::decay_t<T>::rank-fakedRank_;

        template<class StorageArg,
                 std::enable_if_t<(std::is_constructible<Storage, StorageArg>::value
                                   && !SameDecay<StorageArg, ThisType>::value
                   ), int> = 0>
        explicit DenseVectorView(StorageArg&& storage)
          : Storage(std::forward<StorageArg>(storage))
        {}

        DenseVectorView(const DenseVectorView& other)
          : Storage(other), value_(other.value_), indices_(other.indices_)
        {}

        DenseVectorView(DenseVectorView&& other)
          : Storage(std::move(other))
          , value_(std::move(other.value_))
          , indices_(std::move(other.indices_))
        {}

        template<class C, std::enable_if_t<std::is_base_of<ThisType, C>::value, int> = 0>
        explicit DenseVectorView(const C& other)
          : DenseVectorView(static_cast<const ThisType&>(other))
        {}

        static constexpr std::size_t size()
        {
          return TensorTraits<T>::template dim<stage_>();
        }

        auto& lookAt() const
        {
          return indices_;
        }

       protected:
        mutable ValueType value_;
        mutable IndicesType indices_;
      };

      /**Start of the affair. Downcast until storage is reached, but
       * as value return something which can be upcasted to our
       * BaseType in order to break ambiguous base-type lookups.
       */
      template<class T, class Storage, std::size_t N>
      class DenseVectorView<T, Storage, N, N, std::enable_if_t<N >= 2> >
        : public DenseVectorView<T, Storage, N-1, N>
        , public Dune::DenseVector<DenseVectorView<T, Storage, N, N> >
      {
        static_assert(TensorTraits<T>::rank > 0, "This can only work if rank > 0.");

        using ThisType = DenseVectorView;
        using BaseType = DenseVectorView<T, Storage, N-1, N>;
        using ValueType = DenseVectorViewElement<T, Storage, N-1, N>;
        using DenseVectorType = Dune::DenseVector<DenseVectorView<T, Storage, N, N> >;
       public:
        using const_iterator = void;

        using IndexType = typename BaseType::IndexType;

        static constexpr IndexType rank_ = N;
        static constexpr IndexType fakedRank_ = N;
        static constexpr IndexType stage_ = std::decay_t<T>::rank - fakedRank_;

        template<class StorageArg,
                 std::enable_if_t<(std::is_constructible<Storage, StorageArg>::value
                                   && !SameDecay<StorageArg, ThisType>::value
                   ), int> = 0>
        explicit DenseVectorView(StorageArg&& storage)
          : BaseType(std::forward<StorageArg>(storage))
        {}

        DenseVectorView(const DenseVectorView& other)
          : BaseType(other)
        {}

        DenseVectorView(DenseVectorView&& other)
          : BaseType(std::move(other))
        {}

        template<class C, std::enable_if_t<std::is_base_of<ThisType, C>::value, int> = 0>
        explicit DenseVectorView(const C& other)
          : DenseVectorView(static_cast<const ThisType&>(other))
        {}

        static constexpr std::size_t size()
        {
          return TensorTraits<T>::template dim<stage_>();
        }

        const ValueType& operator[](IndexType i) const
        {
          lookAt()[stage_] = i;
          return static_cast<const ValueType&>(static_cast<const BaseType&>(*this));
        }

        auto& lookAt() const
        {
          return BaseType::lookAt();
        }
      };

      /**Start of the affair. Downcast until storage is reached, but
       * as value return something which can be upcasted to our
       * BaseType in order to break ambiguous base-type lookups.
       */
      template<class T, class Storage>
      class DenseVectorView<T, Storage, 1, 1>
        : public Storage
        , public Dune::DenseVector<DenseVectorView<T, Storage, 1, 1> >
      {
        static_assert(TensorTraits<T>::rank > 0, "This can only work if rank > 0.");

        using ThisType = DenseVectorView;
        using ValueType = typename TensorTraits<T>::FieldType;
        using DenseVectorType = Dune::DenseVector<DenseVectorView<T, Storage, 1, 1> >;
       public:
        using const_iterator = void;
        using Storage::operand;
        using IndexType = unsigned;
        using IndicesType = std::array<IndexType, std::decay_t<T>::rank>;

        static constexpr IndexType rank_ = 1;
        static constexpr IndexType fakedRank_ = 1;
        static constexpr IndexType stage_ = std::decay_t<T>::rank-fakedRank_;

        template<class StorageArg,
                 std::enable_if_t<(std::is_constructible<Storage, StorageArg>::value
                                   && !SameDecay<StorageArg, ThisType>::value
                   ), int> = 0>
        explicit DenseVectorView(StorageArg&& storage)
          : Storage(std::forward<StorageArg>(storage))
        {}

        DenseVectorView(const DenseVectorView& other)
          : Storage(other), value_(other.value_), indices_(other.indices_)
        {}

        DenseVectorView(DenseVectorView&& other)
          : Storage(std::move(other))
          , value_(std::move(other.value_))
          , indices_(std::move(other.indices_))
        {}

        template<class C, std::enable_if_t<std::is_base_of<ThisType, C>::value, int> = 0>
        explicit DenseVectorView(const C& other)
          : DenseVectorView(static_cast<const ThisType&>(other))
        {}

        static constexpr std::size_t size()
        {
          return TensorTraits<T>::template dim<stage_>();
        }

        const ValueType& operator[](IndexType i) const
        {
          lookAt()[stage_] = i;
          return value_ = tensorValue(operand(0_c), lookAt());
        }

        auto& lookAt() const
        {
          return indices_;
        }

       protected:
        mutable ValueType value_;
        mutable IndicesType indices_;
      };

      /**Specialize for scalars and just cast to the field type.*/
      template<class T, class Storage>
      class DenseVectorView<T, Storage, 0, 0>
        : public Storage
      {
        static_assert(std::decay_t<T>::rank == 0, "This can only work if rank == 0.");

        using ThisType = DenseVectorView;
        using ValueType = typename std::decay_t<T>::FieldType;
       public:
        using Storage::operand;
        using IndexType = unsigned;

        static constexpr IndexType rank_ = 0;
        static constexpr IndexType fakedRank_ = 0;

        template<class StorageArg,
                 std::enable_if_t<(std::is_constructible<Storage, StorageArg>::value
                                   && !SameDecay<StorageArg, ThisType>::value
                   ), int> = 0>
        explicit DenseVectorView(StorageArg&& storage)
          : Storage(std::forward<StorageArg>(storage))
        {}

        DenseVectorView(const DenseVectorView& other)
          : Storage(other)
        {}

        DenseVectorView(DenseVectorView&& other)
          : Storage(std::move(other))
        {}

        template<class C, std::enable_if_t<std::is_base_of<ThisType, C>::value, int> = 0>
        explicit DenseVectorView(const C& other)
          : DenseVectorView(static_cast<const ThisType&>(other))
        {}

        static constexpr std::size_t size()
        {
          return 0;
        }

        /**Convert to 1d field vector.*/
        operator FloatingPointClosure<ValueType> () const&
        {
          return operand(0_c)();
        }
      };

      /**@internal Assign to a Dune::DenseMatrix. RHS is assumed to be
       * a tensor of rank 2. TODO: can be that this is no longer necessary.
       */
      struct DenseMatrixAssigner
      {
        template<class DenseMatrix, class T>
        static void apply(DenseMatrix& denseMatrix, const TensorResult<T> &rhs)
        {
          static_assert(TensorTraits<T>::rank == 2, "T should be a Tensor of rank 2.");

          forLoop<TensorTraits<T>::template dim<0>()>([&](auto i) {
              using IType = decltype(i);
              forLoop<TensorTraits<T>::template dim<1>()>([&](auto j) {
                  using JType = decltype(j);
                  using Indices = Seq<IType::value, JType::value>;
                  denseMatrix[IType::value][JType::value] =  rhs(Indices{});
                });
            });
        }

        template<class DenseMatrix, class T, class Storage, std::size_t Rank>
        static void apply(DenseMatrix& denseMatrix,
                          const DenseVectorViewElement<T, Storage, 2, Rank>& rhs)
        {
          using Traits = TensorTraits<T>;
          constexpr std::size_t rank = Traits::rank;
          forLoop<Traits::template dim<rank-2>()>([&](auto i) {
              using IType = decltype(i);
              forLoop<Traits::template dim<rank-1>()>([&](auto j) {
                  using JType = decltype(j);
                  denseMatrix[IType::value][JType::value] =  rhs[IType::value][JType::value];
                });
            });
        }

        template<class DenseMatrix, class T, class Storage, std::size_t Rank>
        static void apply(DenseMatrix& denseMatrix,
                          const DenseVectorView<T, Storage, 2, Rank>& rhs)
        {
          apply(denseMatrix, static_cast<const DenseVectorViewElement<T, Storage, 2, Rank>&>(rhs));
        }
      };

    } // NS Tensor

  } // NS ACFem

  /**@name PromotionTraits
   *
   * @internal Switch off promotion traits for our tensor proxies. The
   * idea is to disable arithmetic with these intermediate
   * objects. PromotionTraits does not have an SFINAE argument, so
   * hard code it to empty for all cases.
   */

  template<class Field, int Size, class T>
  struct PromotionTraits<FieldVector<Field, Size>, T>
  {};

  template<class Field, int Size, class T>
  struct PromotionTraits<T, FieldVector<Field, Size> >
  {};

  template<class Field, int Size>
  struct PromotionTraits<FieldVector<Field, Size>, FieldVector<Field, Size> >
  {
    using PromotedType = FieldVector<Field, Size>;
  };

  template<class Field, int Rows, int Cols, class T>
  struct PromotionTraits<FieldMatrix<Field, Rows, Cols>, T>
  {};

  template<class Field, int Rows, int Cols, class T>
  struct PromotionTraits<T, FieldMatrix<Field, Rows, Cols> >
  {};

  template<class Any, class T, class Storage, std::size_t N, std::size_t Rank>
  struct PromotionTraits<Any, ACFem::Tensor::DenseVectorView<T, Storage, N, Rank> >
  {};

  template<class Any, class T, class Storage, std::size_t N, std::size_t Rank>
  struct PromotionTraits<ACFem::Tensor::DenseVectorView<T, Storage, N, Rank>, Any>
  {};

  template<class T1, class Storage1, std::size_t N1, std::size_t Rank1,
           class T2, class Storage2, std::size_t N2, std::size_t Rank2>
  struct PromotionTraits<ACFem::Tensor::DenseVectorView<T1, Storage1, N1, Rank1>,
                         ACFem::Tensor::DenseVectorView<T2, Storage2, N2, Rank2> >
  {};

  template<class T, class Storage, std::size_t N, std::size_t Rank>
  struct PromotionTraits<ACFem::Tensor::DenseVectorView<T, Storage, N, Rank>,
                         ACFem::Tensor::DenseVectorView<T, Storage, N, Rank> >
  {};

  template<class Any, class T, class Storage, std::size_t N, std::size_t Rank>
  struct PromotionTraits<Any, ACFem::Tensor::DenseVectorViewElement<T, Storage, N, Rank> >
  {};

  template<class Any, class T, class Storage, std::size_t N, std::size_t Rank>
  struct PromotionTraits<ACFem::Tensor::DenseVectorViewElement<T, Storage, N, Rank>, Any>
  {};

  template<class T1, class Storage1, std::size_t N1, std::size_t Rank1,
           class T2, class Storage2, std::size_t N2, std::size_t Rank2>
  struct PromotionTraits<ACFem::Tensor::DenseVectorViewElement<T1, Storage1, N1, Rank1>,
                         ACFem::Tensor::DenseVectorViewElement<T2, Storage2, N2, Rank2> >
  {};

  template<class T, class Storage, std::size_t N, std::size_t Rank>
  struct PromotionTraits<ACFem::Tensor::DenseVectorViewElement<T, Storage, N, Rank>,
                         ACFem::Tensor::DenseVectorViewElement<T, Storage, N, Rank> >
  {};

  //!@}

  template<class T, class Storage, std::size_t N, std::size_t Rank>
  struct DenseMatVecTraits<
    ACFem::Tensor::DenseVectorViewElement<T, Storage, N, Rank> >
  {
    using derived_type = ACFem::Tensor::DenseVectorViewElement<T, Storage, N, Rank>;
    using container_type = derived_type;
    using value_type = ACFem::Tensor::DenseVectorViewElement<T, Storage, N-1, Rank>;
    using size_type = std::size_t;
  };

  template<class T, class Storage, std::size_t Rank>
  struct DenseMatVecTraits<ACFem::Tensor::DenseVectorViewElement<T, Storage, 1, Rank> >
  {
    using derived_type = ACFem::Tensor::DenseVectorViewElement<T, Storage, 1, Rank>;
    using container_type = derived_type;
    using value_type = typename std::decay_t<T>::FieldType;
    using size_type = std::size_t;
  };

  template<class T, class Storage, std::size_t Rank>
  struct DenseMatVecTraits<
    ACFem::Tensor::DenseVectorView<T, Storage, Rank, Rank> >
  {
    using derived_type = ACFem::Tensor::DenseVectorView<T, Storage, Rank, Rank>;
    using container_type = derived_type;
    using value_type = ACFem::Tensor::DenseVectorViewElement<T, Storage, Rank-1, Rank>;
    using size_type = std::size_t;
  };

  template<class T, class Storage>
  struct DenseMatVecTraits<
    ACFem::Tensor::DenseVectorView<T, Storage, 1, 1> >
  {
    using derived_type = ACFem::Tensor::DenseVectorView<T, Storage, 1, 1>;
    using container_type = derived_type;
    using value_type = typename std::decay_t<T>::FieldType;
    using size_type = std::size_t;
  };

#if 0

  template<class T, class Storage, std::size_t N, std::size_t Rank>
  struct FieldTraits<ACFem::Tensor::DenseVectorView<T, Storage, N, Rank> >
    : FieldTraits<T>
  {};

  template<class T, class Storage, std::size_t N, std::size_t Rank>
  struct FieldTraits<ACFem::Tensor::DenseVectorViewElement<T, Storage, N, Rank> >
    : FieldTraits<T>
  {};

#else

  // kludge the field-traits until the core-developers fix their things
  template<class T, class Storage, std::size_t N, std::size_t Rank>
  struct FieldTraits<ACFem::Tensor::DenseVectorView<T, Storage, N, Rank> >
  {
   private:
    using Signature = ACFem::TailPart<N, typename ACFem::TensorTraits<T>::Signature>;
   public:
    using field_type = ACFem::Tensor::FieldVectorSeqType<typename ACFem::TensorTraits<T>::FieldType,
                                                         Signature>;
    using real_type = field_type;
  };

  // kludge the field-traits until the core-developers fix their things
  template<class T, class Storage, std::size_t N, std::size_t Rank>
  struct FieldTraits<ACFem::Tensor::DenseVectorViewElement<T, Storage, N, Rank> >
  {
   private:
    using Signature = ACFem::TailPart<N, typename ACFem::TensorTraits<T>::Signature>;
   public:
    using field_type = ACFem::Tensor::FieldVectorSeqType<typename ACFem::TensorTraits<T>::FieldType,
                                                         Signature>;
    using real_type = field_type;
  };

#endif

  template<class T, int SIZE>
  struct IsFieldVectorSizeCorrect<ACFem::Tensor::TensorResult<T>, SIZE>
    : ACFem::BoolConstant<ACFem::TensorTraits<T>::template dim<0>() == SIZE>
  {};

  template<class T, class Storage, std::size_t N, std::size_t Rank, int SIZE>
  struct IsFieldVectorSizeCorrect<ACFem::Tensor::DenseVectorViewElement<T, Storage, N, Rank>, SIZE>
    : ACFem::BoolConstant<ACFem::TensorTraits<T>::template dim<ACFem::TensorTraits<T>::rank - N>() == SIZE>
  {};

  template<class T, class Storage, std::size_t N, std::size_t Rank, int SIZE>
  struct IsFieldVectorSizeCorrect<ACFem::Tensor::DenseVectorView<T, Storage, N, Rank>, SIZE>
    : ACFem::BoolConstant<ACFem::TensorTraits<T>::template dim<ACFem::TensorTraits<T>::rank - N>() == SIZE>
  {};

  namespace Impl {

    template<class DenseMatrix, class T>
    class DenseMatrixAssigner<
      DenseMatrix, ACFem::Tensor::TensorResult<T>,
      std::enable_if_t<std::decay_t<T>::rank == 2> >
      : public ACFem::Tensor::DenseMatrixAssigner
    {};

    template<class DenseMatrix, class T, class Storage, std::size_t Rank>
    class DenseMatrixAssigner<
      DenseMatrix, ACFem::Tensor::DenseVectorViewElement<T, Storage, 2, Rank> >
      : public ACFem::Tensor::DenseMatrixAssigner
    {};

    template<class DenseMatrix, class T, class Storage, std::size_t Rank>
    class DenseMatrixAssigner<
      DenseMatrix, ACFem::Tensor::DenseVectorView<T, Storage, 2, Rank> >
      : public ACFem::Tensor::DenseMatrixAssigner
    {};

  } // NS Impl

} // NS Dune

namespace std {

  //!@internal This is an ugly kludge until the core-developers fix
  // their FieldVector and DenseVector
  template<class T, class Field>
  struct is_convertible<Dune::ACFem::Tensor::TensorResult<T>, Dune::FieldVector<Field, 1> >
    : bool_constant<
       (   std::is_same<
             typename Dune::ACFem::TensorTraits<T>::Signature,
             typename Dune::ACFem::TensorTraits<Dune::FieldVector<Field, 1> >::Signature
             >::value
        && std::is_convertible<
             typename Dune::FieldTraits<T>::field_type,
             typename Dune::FieldTraits<Field>::field_type
             >::value
         )>
  {};

  //!@internal This is an ugly kludge until the core-developers fix
  // their FieldVector and DenseVector
  template<class Field, class T>
  struct is_same<Dune::FieldVector<Field, 1>, Dune::DenseVector<T> >
    : std::is_base_of<Dune::DenseVector<T>, Dune::FieldVector<Field, 1> >
  {};

  template<class T, class Storage, std::size_t N, std::size_t Rank, class Field>
  struct is_convertible<Dune::ACFem::Tensor::DenseVectorView<T, Storage, N, Rank>, Dune::FieldVector<Field, 1> >
    : bool_constant<
       (   std::is_same<
             Dune::ACFem::TailPart<N, typename Dune::ACFem::TensorTraits<T>::Signature>,
             typename Dune::ACFem::TensorTraits<Dune::FieldVector<Field, 1> >::Signature
             >::value
        && std::is_convertible<
             typename Dune::FieldTraits<T>::field_type,
             typename Dune::FieldTraits<Field>::field_type
             >::value
         )>
  {};

}

#endif // __DUNE_ACFEM_TENSORS_BINDINGS_DUNE_DENSEVECTORVIEW_HH__
