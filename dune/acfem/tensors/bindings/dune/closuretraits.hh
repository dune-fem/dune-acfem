#ifndef __DUNE_ACFEM_TENSORS_BINDINGS_DUNE_CLOSURETRAITS_HH__
#define __DUNE_ACFEM_TENSORS_BINDINGS_DUNE_CLOSURETRAITS_HH__

#include "../../../expressions/interface.hh"
#include "../../tensorbase.hh"

namespace Dune
{
  namespace ACFem
  {
    namespace Tensor
    {
      // Forward
      template<class Tensor>
      class TensorResult;

      /**Generate the actual return type for tensor expressions. The
       * "closure expression" primarily adds type-casts to
       * Dune::FieldVector and Dune::FieldMatrix.
       */
      template<class T, std::enable_if_t<(IsTensor<T>::value && !Expressions::IsClosure<T>::value), int> = 0>
      constexpr auto expressionClosure(T&& t)
      {
        //std::clog << "cls arg address: " << &t << std::endl;
        return TensorResult<T>(std::forward<T>(t));
      }

    } // NS Tensor

    namespace Expressions
    {

      /**Mark TensorResult as a closure expression.*/
      template<class T>
      struct IsClosure<Tensor::TensorResult<T> >
        : TrueType
      {};

    } // NS Expressions

  } // NS ACFem

} // NS Dune

#endif //  __DUNE_ACFEM_TENSORS_BINDINGS_DUNE_CLOSURETRAITS_HH__
