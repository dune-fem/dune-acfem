#ifndef __DUNE_ACFEM_TENSORS_BINDINGS_DUNE_DENSESTORAGE_HH__
#define __DUNE_ACFEM_TENSORS_BINDINGS_DUNE_DENSESTORAGE_HH__

#include "fieldtensor.hh"

namespace Dune::ACFem::Tensor {

  template<class T0, class T1>
  struct DenseStorageHelper;

  template<class T0, class T1 = Expressions::Policy::FloatingPointDefault>
  using DenseStorage = typename DenseStorageHelper<T0, T1>::Type;

  template<class T>
  struct DenseStorageHelper<T, std::enable_if_t<IsTensorOperand<T>::value, Expressions::Policy::FloatingPointDefault> >
  {
    using Type = DenseStorage<typename TensorTraits<T>::Signature, typename TensorTraits<T>::FieldType>;
  };

  template<std::size_t... Dims, class Field>
  struct DenseStorageHelper<Seq<Dims...>, Field>
  {
    using Type = FieldTensor<FloatingPointClosure<Field>, Dims...>;
  };

}

#endif // __DUNE_ACFEM_TENSORS_BINDINGS_DUNE_DENSESTORAGE_HH__
