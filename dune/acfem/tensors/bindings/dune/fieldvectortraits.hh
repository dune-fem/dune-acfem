#ifndef __DUNE_ACFEM_TENSORS_BINDINGS_DUNE_FIELDVECTORTRAITS_HH__
#define __DUNE_ACFEM_TENSORS_BINDINGS_DUNE_FIELDVECTORTRAITS_HH__

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/hybridutilities.hh>
#include <dune/fem/common/explicitfieldvector.hh>

#include "../../../common/types.hh"
#include "../../../common/tostring.hh"
#include "../../../common/fractionconstant.hh"
#include "../../../mpl/access.hh"
#include "../../../mpl/compare.hh"
#include "../../../expressions/expressionoperations.hh"

#include "../../tensorbase.hh"

#ifndef VECTOREXPRESSIONS_USE_FIELDMATRIX
/**Define to 1 to use FieldMatrix for rank 2 tensors and
 * FieldVector<FieldMatrix> for rank 3 tensors.
 */
# define VECTOREXPRESSIONS_USE_FIELDMATRIX 1
#endif

namespace Dune {

  namespace ACFem {

    namespace Tensor {

      template<std::size_t... I>
      using Seq = IndexSequence<I...>;

      using UseFieldMatrixDefault = BoolConstant<VECTOREXPRESSIONS_USE_FIELDMATRIX>;

      namespace {
        template<class Field, class Dims, class UseFieldMatrix>
        struct FVType;

        template<class Field, class UseFieldMatrix>
        struct FVType<Field, Seq<>, UseFieldMatrix>
        {
          using Type = Field;
        };

        // for compat we maybe should use FieldMatrix for rank 2 and
        // FieldVector<FieldMatrix for rank 3.
        template<class Field, std::size_t rows, std::size_t cols>
        struct FVType<Field, Seq<rows, cols>, TrueType>
        {
          using Type = FieldMatrix<Field, rows, cols>;
        };

        // for compat we maybe should use FieldMatrix for rank 2 and
        // FieldVector<FieldMatrix for rank 3.
        template<class Field, std::size_t dim, std::size_t rows, std::size_t cols>
        struct FVType<Field, Seq<dim, rows, cols>, TrueType>
        {
          using Type = FieldVector<FieldMatrix<Field, rows, cols>, dim>;
        };

        template<class Field, std::size_t InnerDim, std::size_t... RestDims, class UseFieldMatrix>
        struct FVType<Field, Seq<InnerDim, RestDims...>, UseFieldMatrix>
        {
          using Type = FieldVector<typename FVType<Field, Seq<RestDims...>, UseFieldMatrix>::Type, InnerDim>;
        };

        /**Obtain the tensor signature from a given dense nested
         * FieldVector of FieldMatrix object.
         */
        template<class FV>
        struct FVSignature
        {
          /**Could restrict this to scalar types ... */
          using Type = Seq<>;
        };

        template<class F, int dim>
        struct FVSignature<FieldVector<F, dim> >
        {
          using Type = PushFront<dim, typename FVSignature<F>::Type>;
        };

        template<class F, int dim>
        struct FVSignature<Fem::ExplicitFieldVector<F, dim> >
        {
          using Type = PushFront<dim, typename FVSignature<F>::Type>;
        };

        template<class F, int rows, int cols>
        struct FVSignature<FieldMatrix<F, rows, cols> >
        {
          using Type =
            PushFront<rows,
                      PushFront<cols,
                                typename FVSignature<F>::Type> >;
        };

      }

      template<class Field, std::size_t... Dimensions>
      using FieldVectorType = typename FVType<Field, Seq<Dimensions...>, UseFieldMatrixDefault>::Type;

      template<class Field, class Dims, class UseFieldMatrix = UseFieldMatrixDefault>
      using FieldVectorSeqType = typename FVType<Field, Dims, UseFieldMatrix>::Type;

      template<class Field, class Dims, class UseFieldMatrix = UseFieldMatrixDefault>
      using FieldVectorVectorType = FieldVectorSeqType<Field, ConditionalType<Dims::size() == 0, Seq<1>, Dims>, UseFieldMatrix>;

      template<class FV>
      using FieldVectorSignature = typename FVSignature<std::decay_t<FV> >::Type;

      template<class T>
      struct IsFieldVector
        : FalseType
      {};

      template<class F, int dim>
      struct IsFieldVector<FieldVector<F, dim> >
        : TrueType
      {};

      template<class F, int dim>
      struct IsFieldVector<Fem::ExplicitFieldVector<F, dim> >
        : TrueType
      {};

      template<class T>
      struct IsFieldMatrix
        : FalseType
      {};

      template<class F, int rows, int cols>
      struct IsFieldMatrix<FieldMatrix<F, rows, cols> >
        : TrueType
      {};

      /***/
      template<class F, class SFINAE = void>
      struct IsFieldObject
        : FalseType
      {};

      template<class FV>
      struct IsFieldObject<FV,
                           std::enable_if_t<(!IsFractionConstant<FV>::value
                                             && !IsNamedConstant<FV>::value
                                             && (IsScalar<std::decay_t<FV> >::value
                                                 || IsFieldVector<std::decay_t<FV> >::value
                                                 || IsFieldMatrix<std::decay_t<FV> >::value
                                               )
                           )> >
        : TrueType
      {};

    } // NS Tensor

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_TENSORS_BINDINGS_DUNE_FIELDVECTORTRAITS_HH__
