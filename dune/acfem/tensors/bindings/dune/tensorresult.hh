#ifndef __DUNE_ACFEM_TENSORS_TENSORRESULT_HH__
#define __DUNE_ACFEM_TENSORS_TENSORRESULT_HH__

#include "../../../expressions/storage.hh"
#include "../../../common/literals.hh"
#include "../../tensorbase.hh"
#include "densevectorview.hh"

namespace Dune
{
  namespace ACFem
  {
    namespace Tensor
    {

      /**The wrapper class wrapped around each expression result. The
       * task of the wrapper class is to separate interfacing to the
       * outside world from the actual tensor framework.
       *
       * For the particular Dune case one of the major tasks of the
       * wrapper is to provide the back-type-cast to Dune::FieldVector
       * resp. Dune::FieldMatrix.
       *
       * The wrapper by itself is an implementation of the tensor
       * concept by forwarding all tensor functionality to the wrapped
       * object.
       *
       * SFINAE not needed. Remove.
       */
      template<class Tensor>
      class TensorResult;

      template<class T>
      class TensorResult
        : public DenseVectorView<T, Expressions::Storage<OperationTraits<IdentityOperation>, T> >
        , public TensorBase<typename TensorTraits<T>::FieldType, typename TensorTraits<T>::Signature, TensorResult<T> >
      {
        static_assert(IsTensor<T>::value,
                      "TensorResult should only be applied to tensors.");

        using TensorType = std::decay_t<T>;
        using BaseType = TensorBase<typename TensorTraits<T>::FieldType, typename TensorTraits<T>::Signature, TensorResult>;
        using StorageType = DenseVectorView<T, Expressions::Storage<OperationTraits<IdentityOperation>, T> >;
       public:
        using StorageType::operation;
        using StorageType::operand;
        using StorageType::arity;
        using StorageType::OperandType;
        using typename StorageType::FunctorType;
        using typename StorageType::OperationType;
        using ExpressionType = std::decay_t<T>;
        using BaseType::rank;
        using typename BaseType::FieldType;
        using BaseType::dim;

        explicit TensorResult(T&& t)
          : StorageType(std::forward<T>(t))
        {}

        TensorResult(const TensorResult& other)
          : StorageType(other)
        {}

        template<class Other,
                 std::enable_if_t<(IsTensorOperand<Other>::value
                                   && std::is_assignable<typename TensorTraits<T>::ElementZero,
                                                         typename TensorTraits<Other>::FieldType>::value
                   ), int> = 0>
        TensorResult& operator=(Other&& other)
        {
          operand(0_c) = std::forward<Other>(other);
          return *this;
        }

       protected:
        using StorageType::t0_;
       public:

        ExpressionType* operator->()
        {
          return &t0_;
        }

        const ExpressionType* operator->() const
        {
          return &t0_;
        }

        decltype(auto) storage() &
        {
          return static_cast<StorageType&>(*this);
        }

        decltype(auto) storage() &&
        {
          return static_cast<StorageType&&>(*this);
        }

        decltype(auto) storage() const&
        {
          return static_cast<const StorageType&>(*this);
        }

        /**Run-time dynamic access to components.*/
        template<class... Dims,
                 std::enable_if_t<(sizeof...(Dims) == rank
                                   &&
                                   IsIntegralPack<Dims...>::value)
                                  , int> = 0>
        decltype(auto) operator()(Dims... indices)
        {
          return operand(0_c)(indices...);
        }

        /**Run-time dynamic access to components.*/
        template<class... Dims,
                 std::enable_if_t<(sizeof...(Dims) == rank
                                   &&
                                   IsIntegralPack<Dims...>::value)
                                  , int> = 0>
        decltype(auto) operator()(Dims... indices) const
        {
          return operand(0_c)(indices...);
        }

        /**Compile-time constant access from index-sequence.*/
        template<std::size_t... Indices, std::enable_if_t<sizeof...(Indices) == rank, int> = 0>
        decltype(auto) constexpr operator()(Seq<Indices...>)
        {
          return operand(0_c)(Seq<Indices...>{});
        }

        /**Compile-time constant access from index-sequence.*/
        template<std::size_t... Indices, std::enable_if_t<sizeof...(Indices) == rank, int> = 0>
        decltype(auto) constexpr operator()(Seq<Indices...>) const
        {
          return operand(0_c)(Seq<Indices...>{});
        }

        /**Implement kind of a nested vector interface via operator[].*/
        template<std::size_t I>
        decltype(auto) operator[](IndexConstant<I> i)&&
        {
          return operand(0_c)[i];
        }

        /**Implement kind of a nested vector interface via operator[].*/
        template<std::size_t I>
        decltype(auto) operator[](IndexConstant<I> i)&
        {
          return operand(0_c)[i];
        }

        /**Implement kind of a nested vector interface via operator[].*/
        template<std::size_t I>
        decltype(auto) operator[](IndexConstant<I> i)const&
        {
          return operand(0_c)[i];
        }

        /**Compile-time constant access from index-sequence.*/
        template<std::size_t... Indices, class Pos = MakeIndexSequence<sizeof...(Indices)> >
        static bool constexpr isZero(Seq<Indices...> = Seq<Indices...>{}, Pos = Pos{})
        {
          return TensorType::isZero(Seq<Indices...>{}, Pos{});
        }

        std::string name() const
        {
          std::string pfx = std::is_reference<T>::value ? (RefersConst<T>::value ? "cref" : "ref") : "";
          return "<cls "+pfx+operand(0_c).name()+"/>";
//          return pfx+operand(0_c).name();
        }

        operator auto () &&
        {
          return std::move(operand(0_c));
        }

        operator decltype(auto) () const&
        {
          return operand(0_c);
        }

        operator decltype(auto) ()&
        {
          return operand(0_c);
        }

      };

    } // NS Tensor

  } // NS ACFem

  template<class T>
  struct FieldTraits<ACFem::Tensor::TensorResult<T> >
    : FieldTraits<std::decay_t<T> >
  {};

} // NS Dune

#endif // __DUNE_ACFEM_TENSORS_TENSORBASE_HH__
