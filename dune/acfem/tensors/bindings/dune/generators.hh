#ifndef __DUNE_ACFEM_TENSORS_BINDINGS_DUNE_GENERATORS_HH__
#define __DUNE_ACFEM_TENSORS_BINDINGS_DUNE_GENERATORS_HH__

#include "closuretraits.hh"
#include "tensorresult.hh"
#include "densevectorview.hh"
#include "fieldtensor.hh"
#include "../../operations/subexpression.hh"

namespace Dune {

  namespace ACFem {

    namespace Tensor {

      template<std::size_t... Dimensions, class Field = double>
      constexpr auto fieldTensor(Seq<Dimensions...> = Seq<Dimensions...>{})
      {
        return expressionClosure(FieldTensor<Field, Dimensions...>{});
      }

      template<class Field, std::size_t... Dimensions>
      constexpr auto fieldTensor(Seq<Dimensions...> = Seq<Dimensions...>{})
      {
        return expressionClosure(FieldTensor<Field, Dimensions...>{});
      }

      /**Wrap a "field object" identified by IsFieldObject into a tensor
       * object.
       */
      template<class FV, std::enable_if_t<IsFieldObject<FV>::value, int> = 0>
      constexpr auto fieldTensor(FV&& t)
      {
        return expressionClosure(FieldVectorTensor<FV>(std::forward<FV>(t)));
      }

      /**Generate a suitable FieldVectorTensor for any given tensor as a copy.*/
      template<class T, class UseFieldMatrix = UseFieldMatrixDefault,
               std::enable_if_t<(!IsFieldObject<T>::value
                                 && !IsFieldVectorTensor<T>::value
                                 && IsTensorOperand<T>::value
        ), int> = 0>
      constexpr auto fieldTensor(T&& t, UseFieldMatrix = UseFieldMatrix{})
      {
        using FVType = FieldVectorSeqType<FloatingPointClosure<typename TensorTraits<T>::FieldType>, typename TensorTraits<T>::Signature, UseFieldMatrix>;

        return expressionClosure(FieldVectorTensor<FVType>(tensor(std::forward<T>(t))));
      }

      template<class T, std::enable_if_t<IsFieldVectorTensor<Expressions::EnclosedType<T> >::value, int> = 0>
      constexpr decltype(auto) fieldTensor(T&& t)
      {
        return std::forward<T>(t);
      }

      template<class T, std::enable_if_t<!Expressions::IsClosure<T>::value, int> = 0>
      DenseVectorView<T> denseVectorView(T&& t)
      {
        return DenseVectorView<T>(std::forward<T>(t));
      }

      template<class T, std::enable_if_t<Expressions::IsClosure<T>::value, int> = 0>
      auto denseVectorView(T&& t)
      {
        return denseVectorView(Expressions::asExpression(std::forward<T>(t)));
      }

    } // NS Tensor

  } // NS ACFem

} // NS Dune

#endif // __DUNE_ACFEM_TENSORS_BINDINGS_DUNE_GENERATORS_HH__
