#ifndef __DUNE_ACFEM_TENSORS_BINDINGS_DUNE_OPERANDPROMOTION_HH__
#define __DUNE_ACFEM_TENSORS_BINDINGS_DUNE_OPERANDPROMOTION_HH__

#include "../../tensorbase.hh"
#include "../../expressiontraits.hh"
#include "fieldvectortraits.hh"

namespace Dune {

  /**@internal Allow promotion of all "FieldObjects" to tensors.
   *
   * @note This must live in the Dune namespace, otherwise
   * argument dependent lookup will fail. The other possibility would
   * be to make sure that tensor/operandpromotion.hh would alwys be
   * included before expressions/operandpromotion.hh which is
   * difficult to achieve.
   */
  template<std::size_t N, class... T,
           std::enable_if_t<(ACFem::AllAre<ACFem::Tensor::IsFieldObject, T...>::value
                             && !ACFem::AllAre<ACFem::IsScalar, T...>::value
                             && !ACFem::AllAre<ACFem::Tensor::IsUnaryTensorOperand, T...>::value
             ), int> = 0>
  constexpr decltype(auto) operandPromotion(T&&... t)
  {
    return ACFem::Tensor::tensor(std::forward<ACFem::TupleElement<N, std::tuple<T...> > >(
                                   std::get<N>(std::forward_as_tuple(std::forward<T>(t)...))
                                   )
      );
  }

} // NS Dune

#endif // __DUNE_ACFEM_TENSORS_BINDINGS_DUNE_OPERANDPROMOTION_HH__
