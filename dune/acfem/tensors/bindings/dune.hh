#ifndef __DUNE_ACFEM_TENSORS_BINDINGS_DUNE_HH__
#define __DUNE_ACFEM_TENSORS_BINDINGS_DUNE_HH__

#include "dune/closuretraits.hh"
#include "dune/tensorresult.hh"
#include "dune/densestorage.hh"
#include "dune/fieldtensor.hh"
#include "dune/densevectorview.hh"
#include "dune/generators.hh"
#include "dune/operandpromotion.hh"

#endif //  __DUNE_ACFEM_TENSORS_BINDINGS_DUNE_HH__
