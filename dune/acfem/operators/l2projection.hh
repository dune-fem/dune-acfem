#ifndef __DUNE_ACFEM_L2PROJECTION_HH__
#define __DUNE_ACFEM_L2PROJECTION_HH__

#include "functionals/functionals.hh"
#include "../models/basicmodels.hh"
#include "../algorithms/ellipticfemscheme.hh"

namespace Dune {

  namespace ACFem {

    /**@addtogroup DiscreteOperators
     *
     *@{
     */

    /**@addtogroup PDE-Operators
     *
     *@{
     */

    /**Compute the L2-projection of the given functional to a discrete
     * space. This is meant for continuous finite
     * elements. Consequently, we have to do "hard" work here and
     * assemble and invert the mass matrix by means of a CG
     * method. Fortunately, the condition of the mass-matrix should be
     * bounded independently of the mesh width after applying Jacobian
     * preconditioning.
     *
     * @param Functional The Domain type, has to satisfiy the
     * DiscreteLinearFunctional interface.
     *
     * @param DiscreteFunction The Range type. Some Fem::DiscreteFunction.
     *
     * See, e.g., L2InnerProductFunctional for an example of what may
     * be passed as argument ot L2Projection::operator() in a more
     * concrete setting. However, any DiscreteLinearFunctional
     * realization should do. E.g. for the DiracDistribution this
     * projection also successfully computes an L2-Riesz
     * representation. Any linear functional will do, as a FEM space
     * is of finite dimension. However, if the underlying non-discrete
     * functional is discontinuous, then it is unlikely that there
     * will be a uniform bound for the L2-norm of the representative
     * which is independent from mesh refinement.
     *
     * \note We cannot obey the Fem::Operator interface as we map
     * DiscreteLinearFunctional instances, not Fem::Function
     * instances, to Fem::DiscreteFunction instances.
     */
    template<class Functional, class DiscreteFunction,
             std::enable_if_t<(IsLinearFunctional<Functional>::value
                               && IsDiscreteFunction<DiscreteFunction>::value
      ), int> = 0>
    void l2Projection(const Functional& phi, DiscreteFunction& result)
    {
      // skip the initialization, just do a quick loop over "result"
      // in order to determine that all values are finite
      for (auto&& dof : dofs(result)) {
        if (!std::isfinite(dof)) {
          dof = 0.;

        }
      }

      ellipticFemScheme(result, massModel(result), phi, "acfem.l2projection").solve();
    }

    //!Perform an L2-projection for any GridFunction
    template<class GridFunction, class DiscreteFunction,
             std::enable_if_t<(IsWrappableByConstLocalFunction<GridFunction>::value
                               && IsDiscreteFunction<DiscreteFunction>::value
      ), int> = 0>
    void l2Projection(GridFunction&& fct, DiscreteFunction& result)
    {
      // skip the initialization, just do a quick loop over "result"
      // in order to determine that all values are finite
      for (auto&& dof : dofs(result)) {
        if (!std::isfinite(dof)) {
          dof = 0.;

        }
      }

      ellipticFemScheme(result, massModel(result) - fct, "acfem.l2projection").solve();
    }

    //!@} PDE-Operators

    //!@} Operators

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_L2PROJECTION_HH__
