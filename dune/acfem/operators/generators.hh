#ifndef __DUNE_ACFEM_OPERATORS_GENERATORS_HH__
#define __DUNE_ACFEM_OPERATORS_GENERATORS_HH__

#include "ellipticoperator.hh"
#include "constraints/dirichletconstraints.hh"

namespace Dune {

  namespace ACFem {

    template<class Model,
             class DomainFunction,
             class RangeFunction = DomainFunction,
             class Constraints,
             template<class GridPart> class QuadratureTraits = DefaultQuadratureTraits>
    auto ellipticOperator(const Model& model,
                          const DomainFunction& domFct,
                          const RangeFunction& rangeFct,
                          Constraints&& constraints,
                          QuadratureTraits<typename DomainFunction::GridPartType> = QuadratureTraits<typename DomainFunction::GridPartType>{})
    {
      return EllipticOperator<Model, DomainFunction, RangeFunction, Constraints, QuadratureTraits>(model, std::forward<Constraints>(constraints));
    }

    template<class Model,
             class DomainFunction,
             class RangeFunction = DomainFunction,
             template<class GridPart> class QuadratureTraits = DefaultQuadratureTraits>
    auto ellipticOperator(const Model& model,
                          const DomainFunction& domFct,
                          const RangeFunction& rangeFct,
                          QuadratureTraits<typename DomainFunction::GridPartType> qt = QuadratureTraits<typename DomainFunction::GridPartType>{})
    {
      using DiscreteFunctionSpaceType = typename RangeFunction::DiscreteFunctionSpaceType;
      using ModelType = DiscreteModel<Model, DiscreteFunctionSpaceType>;
      using ConstraintsType = DirichletConstraints<DiscreteFunctionSpaceType, ModelType>;
      auto discModel = discreteModel(model, rangeFct.space());
      auto constraints = ConstraintsType(rangeFct.space(), discModel);

      return ellipticOperator(std::move(discModel), domFct, rangeFct, std::move(constraints), qt);
    }

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_OPERATORS_GENERATORS_HH__
