#ifndef __DUNE_ACFEM_OPERATORS_FUNCTIONALS_MODULES_ZERO_HH__
#define __DUNE_ACFEM_OPERATORS_FUNCTIONALS_MODULES_ZERO_HH__

#include "../../../common/literals.hh"
#include "../../../expressions/terminal.hh"

#include "../linearfunctional.hh"
#include "../functionaltraits.hh"

namespace Dune {

  namespace ACFem {

    namespace LinearFunctional {

      using namespace Literals;

      /**The zero functional.*/
      template<class DiscreteFunctionSpace>
      class ZeroFunctional
        : public LinearFunctionalBase<DiscreteFunctionSpace>
        , public Expressions::SelfExpression<ZeroFunctional<DiscreteFunctionSpace> >
        , public ZeroExpression
      {
        using BaseType = LinearFunctionalBase<DiscreteFunctionSpace>;
       public:
        using typename BaseType::DiscreteFunctionSpaceType;
        using typename BaseType::RangeFieldType;
        using BaseType::space;

        ZeroFunctional(const DiscreteFunctionSpace& space)
          : BaseType(space)
        {}

        template<class FieldArg, class DiscreteFunction,
                 std::enable_if_t<std::is_constructible<RangeFieldType, FieldArg>::value, int> = 0>
        void coefficients(FieldArg&& s, DiscreteFunction& values) const
        {
          // do nothing because functionals always add their coefficients to the given values
          static_assert(std::is_same<DiscreteFunctionSpace, typename DiscreteFunction::DiscreteFunctionSpaceType>::value,
                        "DiscreteFunctionSpaceType of functional and discrete storage has to coincide");
        }

        template<class DiscreteFunction>
        void coefficients(DiscreteFunction& df) const
        {
          coefficients(1_f, df);
        }

        template<class DiscreteFunction>
        auto operator()(const DiscreteFunction& df) const
        {
          return 0_f;
        }

        std::string name() const
        {
          return "<0,.>";
        }

      };

      template<class DiscreteFunctionSpaceType>
      auto zeroFunctional(const DiscreteFunctionSpaceType& space)
      {
        return ZeroFunctional<DiscreteFunctionSpaceType>(space);
      }

    } // LinearFunctional::

    template<class Space>
    using ZeroLinearFunctional = LinearFunctional::ZeroFunctional<Space>;

    using LinearFunctional::zeroFunctional;

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_OPERATORS_FUNCTIONALS_MODULES_ZERO_HH__
