#ifndef __DUNE_ACFEM_OPERATORS_FUNCTIONALS_MODULES_MODELFUNCTIONAL_HH__
#define __DUNE_ACFEM_OPERATORS_FUNCTIONALS_MODULES_MODELFUNCTIONAL_HH__

#include "../../../common/literals.hh"
#include "../../../expressions/terminal.hh"

#include "../../generators.hh"
#include "../linearfunctional.hh"

namespace Dune {

  namespace ACFem {

    namespace LinearFunctional {

      using namespace Literals;

      template<class Model, class DomainFunction, class DiscreteFunctionSpace>
      class ModelFunctional
        : public LinearFunctionalBase<DiscreteFunctionSpace>
        , Expressions::SelfExpression<ModelFunctional<Model, DomainFunction, DiscreteFunctionSpace> >
      {
        using BaseType = LinearFunctionalBase<DiscreteFunctionSpace>;
       public:
        using typename BaseType::DiscreteFunctionSpaceType;
        using typename BaseType::RangeFieldType;

        template<class ModelArg, class FctArg,
                 std::enable_if_t<(std::is_constructible<Model, ModelArg>::value
                                   && std::is_constructible<DomainFunction, FctArg>::value
          ), int> = 0>
        ModelFunctional(ModelArg&& model, FctArg&& fct, const DiscreteFunctionSpaceType& space)
          : BaseType(space)
          , model_(std::forward<ModelArg>(model))
          , domainFunction_(std::forward<FctArg>(fct))
        {}

        template<class FieldArg, class DiscreteFunction,
                 std::enable_if_t<std::is_constructible<RangeFieldType, FieldArg>::value, int> = 0>
        void coefficients(FieldArg&& s, DiscreteFunction& values) const
        {
          if constexpr (ExpressionTraits<FieldArg>::isZero) {
            values.clear();
          } else {
            auto pdeOperator = ellipticOperator(s*model_, domainFunction_, values);
            pdeOperator(domainFunction_, values);
          }
        }

        template<class DiscreteFunction>
        void coefficients(DiscreteFunction& df) const
        {
          coefficients(1_f, df);
        }

        template<class DiscreteFunction>
        auto operator()(const DiscreteFunction& df) const
        {
          return inner(*this, df);
        }

        std::string name() const
        {
          return "Op(" + domainFunction_.name() + ",.)";
        }

       protected:
        Model model_;
        DomainFunction domainFunction_;
      };

      template<class Model, class DomainFunction, class DiscreteFunctionSpace>
      auto modelFunctional(Model&& model, DomainFunction&& fct, const DiscreteFunctionSpace& space)
      {
        return ModelFunctional<Model, DomainFunction, DiscreteFunctionSpace>(
          std::forward<Model>(model), std::forward<DomainFunction>(fct), space
          );
      }

      template<class DomainFunction, class DiscreteFunctionSpace>
      auto l2InnerProductFunctional(DomainFunction&& fct, const DiscreteFunctionSpace& space)
      {
        // return modelFunctional(massModel(fct), std::forward<DomainFunction>(fct), space);
        return modelFunctional(bulkLoadFunctionModel(std::forward<DomainFunction>(fct)), zero(fct), space);
      }

      template<class DomainFunction, class Indicator, class DiscreteFunctionSpace>
      auto l2BoundaryFunctional(DomainFunction&& fct, Indicator&& indicator, const DiscreteFunctionSpace& space)
      {
        return modelFunctional(neumannBoundaryModel(std::forward<DomainFunction>(fct), std::forward<Indicator>(indicator)),
                               zero(fct), space);
      }

      //!Create a divergence functional, types deduced form arguments.
      template<class GridFunction, class DiscreteFunctionSpace>
      auto divergenceFunctional(GridFunction&& u, const DiscreteFunctionSpace& space)
      {
        return modelFunctional(divergenceLoadModel(std::forward<GridFunction>(u), zero(u), space));
      }

    } // LinearFunctional::

    using LinearFunctional::modelFunctional;
    using LinearFunctional::l2InnerProductFunctional;
    using LinearFunctional::l2BoundaryFunctional;
    using LinearFunctional::divergenceFunctional;

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_OPERATORS_FUNCTIONALS_MODULES_MODELFUNCTIONAL_HH__
