#ifndef __DUNE_ACFEM_OPERATORS_FUNCTIONALS_MODULES_DOFSTORAGE_HH__
#define __DUNE_ACFEM_OPERATORS_FUNCTIONALS_MODULES_DOFSTORAGE_HH__

#include "../../../expressions/terminal.hh"
#include "../../../common/literals.hh"

#include "../linearfunctional.hh"
#include "../functionaltraits.hh"
#include "../operations/inner.hh"

namespace Dune {

  namespace ACFem {

    namespace LinearFunctional {

      using namespace Literals;

      /**A functional which inherits from a proper discrete function.*/
      template<class DiscreteFunction>
      class DofStorageFunctional
        : public DiscreteFunction
        , public LinearFunctional<typename DiscreteFunction::DiscreteFunctionSpaceType>
        , public Expressions::SelfExpression<DofStorageFunctional<DiscreteFunction> >
        , public ConstantExpression
      {
        using ThisType = DofStorageFunctional;
        using BaseType = DiscreteFunction;
        using FunctionalBase = LinearFunctional<typename DiscreteFunction::DiscreteFunctionSpaceType>;
       public:
        using typename FunctionalBase::DiscreteFunctionSpaceType;
        using typename FunctionalBase::RangeFieldType;
        using BaseType::DiscreteFunction;
        using BaseType::space;
        using BaseType::dbegin;
        using BaseType::dend;

        template<class Other,
                 std::enable_if_t<(IsLinearFunctional<Other>::value
                                   && !SameDecay<Other, ThisType>::value
                                   && std::is_same<DiscreteFunctionSpaceType, typename std::decay_t<Other>::DiscreteFunctionSpaceType>::value
          ), int> = 0>
        DofStorageFunctional(Other&& other)
          : BaseType("functional dof storage", other.space())
        {
          BaseType::clear();
          other.coefficients(static_cast<BaseType&>(*this));
        }

        template<class Other,
                 std::enable_if_t<(IsLinearFunctional<Other>::value
                                   && !SameDecay<Other, ThisType>::value
                                   && std::is_same<DiscreteFunctionSpaceType, typename std::decay_t<Other>::DiscreteFunctionSpaceType>::value
          ), int> = 0>
        ThisType& operator=(Other&& other)
        {
          BaseType::clear();
          other.coefficients(1_f, static_cast<BaseType&>(*this));
        }

        template<class Other,
                 std::enable_if_t<(IsLinearFunctional<Other>::value
                                   && !SameDecay<Other, ThisType>::value
                                   && std::is_same<DiscreteFunctionSpaceType, typename std::decay_t<Other>::DiscreteFunctionSpaceType>::value
          ), int> = 0>
        ThisType& operator+=(Other&& other)
        {
          if constexpr (!ExpressionTraits<Other>::isZero) {
            other.coefficients(static_cast<BaseType&>(*this));
          }
          return *this;
        }

        template<class Other,
                 std::enable_if_t<(IsLinearFunctional<Other>::value
                                   && !SameDecay<Other, ThisType>::value
                                   && std::is_same<DiscreteFunctionSpaceType, typename std::decay_t<Other>::DiscreteFunctionSpaceType>::value
          ), int> = 0>
        ThisType& operator-=(Other&& other)
        {
          if constexpr (!ExpressionTraits<Other>::isZero) {
            other.coefficients(-1_f, static_cast<BaseType&>(*this));
          }
          return *this;
        }

        template<class FieldArg, class DF,
                 std::enable_if_t<std::is_constructible<RangeFieldType, FieldArg>::value, int> = 0>
        ThisType& operator*=(FieldArg&& s)
        {
          if constexpr (ExpressionTraits<FieldArg>::isZero) {
            BaseType::clear();
          } else if constexpr (!ExpressionTraits<FieldArg>::isOne) {
            for (auto&& dof : dofs(*this)) {
              dof *= s;
            }
          }
          return *this;
        }

        template<class FieldArg, class DF,
                 std::enable_if_t<std::is_constructible<RangeFieldType, FieldArg>::value, int> = 0>
        void coefficients(FieldArg&& s, DF& values) const
        {
          if constexpr (ExpressionTraits<FieldArg>::isZero) {
            values.clear();
          } else {
            auto dofsEnd = dend();
            for (auto&& dof = dbegin(), dest = values.dbegin(); dof != dofsEnd; ++dof, ++dest) {
              *dest += s * *dof;
            }
          }
        }

        template<class DF>
        void coefficients(DF& df) const
        {
          coefficients(1_f, df);
        }

        template<class DF>
        auto operator()(const DF& df) const
        {
          return inner(*this, df);
        }
      };

      template<class DiscreteFunction>
      class DofStorageFunctional<DiscreteFunction&>
        : public LinearFunctional<typename std::decay_t<DiscreteFunction>::DiscreteFunctionSpaceType>
        , public Expressions::SelfExpression<DofStorageFunctional<DiscreteFunction&> >
      {
        using BaseType = LinearFunctional<typename std::decay_t<DiscreteFunction>::DiscreteFunctionSpaceType>;
        using ThisType = DofStorageFunctional;
        using FunctionalBase = LinearFunctional<typename DiscreteFunction::DiscreteFunctionSpaceType>;
       public:
        using typename FunctionalBase::DiscreteFunctionSpaceType;
        using typename FunctionalBase::RangeFieldType;

        DofStorageFunctional(DiscreteFunction& dofs)
          : dofs_(dofs)
        {}

        template<class Other,
                 std::enable_if_t<(!std::is_const<DiscreteFunction>::value
                                   && IsLinearFunctional<Other>::value
                                   && !SameDecay<Other, ThisType>::value
                                   && std::is_same<DiscreteFunctionSpaceType, typename std::decay_t<Other>::DiscreteFunctionSpaceType>::value
          ), int> = 0>
        ThisType& operator=(Other&& other)
        {
          other.coefficients(1_f, dofs_);
        }

        template<class FieldArg, class DF,
                 std::enable_if_t<std::is_constructible<RangeFieldType, FieldArg>::value, int> = 0>
        void coefficients(FieldArg&& s, DF& values) const
        {
          auto&& dof = dofs_.dbegin();
          auto&& dest = values.dbegin();
          auto&& dofsEnd = dofs_.dend();
          while (dof != dofsEnd) {
            *dest += s * *dof;
            ++dof;
            ++dest;
          }
        }

        template<class DF>
        void coefficients(DF& df) const
        {
          coefficients(1_f, df);
        }

        template<class DF>
        auto operator()(const DF& df) const
        {
          return inner(*this, df);
        }

        auto dbegin() const
        {
          return dofs_.dbegin();
        }

        auto dend() const
        {
          return dofs_.dend();
        }

        const DiscreteFunctionSpaceType& space() const
        {
          return dofs_.space();
        }

        std::string name() const
        {
          return "<Uh,.>";
        }

        operator DiscreteFunction& () &
        {
          return dofs_;
        }

        operator std::add_const_t<DiscreteFunction>& () const&
        {
          return dofs_;
        }

        operator DiscreteFunction () &&
        {
          return dofs_;
        }

       protected:
        DiscreteFunction& dofs_;
      };

      template<class DiscreteFunction>
      auto dofStorageFunctional(DiscreteFunction&& df)
      {
        return DofStorageFunctional<DiscreteFunction>(std::forward<DiscreteFunction>(df));
      }

      template<class DiscreteFunction, class DiscreteFunctionSpace,
               std::enable_if_t<std::is_same<typename std::decay_t<DiscreteFunction>::DiscreteFunctionSpaceType,
                                             DiscreteFunctionSpace>::value, int> = 0>
      auto dofStorageFunctional(const DiscreteFunctionSpace& space, const std::string& name = "")
      {
        using DiscreteFunctionType = std::decay_t<DiscreteFunction>;
        return expressionClosure( DofStorageFunctional<DiscreteFunctionType>(DiscreteFunctionType(name, space)));
      }

    }

    using LinearFunctional::dofStorageFunctional;

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_OPERATORS_FUNCTIONALS_MODULES_DOFSTORAGE_HH__
