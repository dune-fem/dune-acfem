#ifndef __DUNE_ACFEM_OPERATORS_FUNCTIONALS_LINEARFUNCTIONAL_HH__
#define __DUNE_ACFEM_OPERATORS_FUNCTIONALS_LINEARFUNCTIONAL_HH__

namespace Dune {

  namespace ACFem {

    namespace LinearFunctional {

      /**Light-weight base class for all linear functionals.*/
      template<class DiscreteFunctionSpace>
      class LinearFunctional
      {
       public:
        using DiscreteFunctionSpaceType = DiscreteFunctionSpace;
        using FunctionSpaceType = typename DiscreteFunctionSpaceType::FunctionSpaceType;
        using GridPartType = typename DiscreteFunctionSpaceType::GridPartType;
        using RangeFieldType = typename FunctionSpaceType::RangeFieldType;
      };

      template<class DiscreteFunctionSpace>
      class LinearFunctionalBase
        : public LinearFunctional<DiscreteFunctionSpace>
      {
        using BaseType = LinearFunctional<DiscreteFunctionSpace>;
       public:
        using typename BaseType::DiscreteFunctionSpaceType;

        LinearFunctionalBase(const DiscreteFunctionSpaceType& space)
          : space_(space)
        {}

        const DiscreteFunctionSpaceType& space() const
        {
          return space_;
        }

       protected:
        const DiscreteFunctionSpaceType& space_;
      };

    }

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_OPERATORS_FUNCTIONALS_LINEARFUNCTIONAL_HH__
