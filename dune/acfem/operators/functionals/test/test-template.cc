#include <config.h>

#include <dune/acfem/common/scopedredirect.hh>

// iostream includes
#include <iostream>
#include <sstream>

#include <dune/common/exceptions.hh>
#include <dune/fem/io/parameter.hh>

// Helper to get to the reference element with reasonable effort
#include <dune/fem/space/common/allgeomtypes.hh>

// include grid part
#include <dune/fem/gridpart/adaptiveleafgridpart.hh>
#include <dune/fem/space/common/interpolate.hh>

// Simplification for discrete functions
#include <dune/acfem/common/discretefunctionselector.hh>

// All includes for grid-functions.
#include <dune/acfem/functions/basicfunctions.hh>

// All includes for functionals
#include "../functionals.hh"

#include "../../l2projection.hh"

/**@addtogroup Operators
 * @{
 */

/**@addtogroup LinearFunctionals
 * @{
 */

/**@addtogroup LinearFunctionalTests
 * @{
 */

using namespace Dune;
using namespace ACFem;
using namespace ACFem::Literals;

template<class LinearFunctional>
void runTests(LinearFunctional&& Psi)
{
  using FunctionSpaceType = typename LinearFunctional::FunctionSpaceType;
  using GridPartType = typename LinearFunctional::GridPartType;

  GridPartType gridPart = Psi.space().gridPart();

  auto space = discreteFunctionSpace<FunctionSpaceType::dimRange>(gridPart, lagrange<POLORDER>);
  auto Uh = discreteFunction(space, "Uh");
  auto dend(Uh.dend());
  for (auto it = Uh.dbegin(); it != dend; ++it) {
    *it = 1; // simply generate a constant function
  }

  *Uh.dbegin() = HUGE_VAL;

  l2Projection(Psi, Uh);
  std::clog << std::endl << "Pi(" << Psi.name() << "):" << std::endl;
  double sumPi = 0.0;
  dend = Uh.dend();
  for (auto it = Uh.dbegin(); it != dend; ++it) {
    std::clog << *it << std::endl;
    sumPi += *it;
  }
  std::clog << std::endl;
  std::clog << "Sum of coefficients of L2-projection: " << sumPi << std::endl;

  Uh.clear();
  Psi.coefficients(Uh);

  std::clog << std::endl << "Coefficients of " << Psi.name() << ":" << std::endl;
  dend = Uh.dend();
  for (auto it = Uh.dbegin(); it != dend; ++it) {
    std::clog << *it << std::endl;
  }
  std::clog << std::endl;

  dend = Uh.dend();
  for (auto it = Uh.dbegin(); it != dend; ++it) {
    *it = 1; // simply generate a constant function
  }

  std::clog << std::endl << Psi.name() << "(" << Uh.name() << ") = " << Psi(Uh) << std::endl;

  std::clog << std::endl;
}

//!Embedded grid-function test DGF-"file"
const std::string dgfData(
  "DGF\n"
  "\n"
  "Vertex\n"
  " 0   0\n"
  " 1   0\n"
  " 1   1\n"
  " 0   1\n"
  " 0.5 0.5\n"
  "#\n"
  "\n"
  "SIMPLEX\n"
  "0 1 4\n"
  "1 2 4\n"
  "2 3 4\n"
  "3 0 4\n"
  "#\n"
  "\n"
  "BoundaryDomain\n"
  "default 1\n"
  "1   1 0   1 1 : blah1  % right boundary\n"
  "2   0 1   1 1 : blah2  % upper boundary\n"
  "3   0 0   0 1 : blah3  % left boundary\n"
  "4   0 0   1 0 : blah4  % lower boundary\n"
  "#\n"
  "\n"
  "GridParameter\n"
  "% longest or arbitrary (see DGF docu)\n"
  "refinementedge longest\n"
  "% there is zarro information on this ... :(\n"
  "overlap 0\n"
  "% whatever this may be ...\n"
  "tolerance 1e-12\n"
  "% silence?\n"
  "verbose 0\n"
  "#\n");

/** Test-template main program. Instantiate a single expression
 * template and evaluate it on a simple grid.
 */
int main(int argc, char *argv[])
{
  try {
    // General setup
    Fem::MPIManager::initialize(argc, argv);

    // append parameter
    Fem::Parameter::append(argc, argv);

    // append default parameter file
    Fem::Parameter::append(SRCDIR "/parameter");

    //redirect the stream cerr to cout
    //so we only get the output of clog on stderr
    ScopedRedirect redirect(std::cerr, std::cout);

    // reduce precision and use normalized FP output
    std::clog << std::scientific << std::setprecision(3);

    // type of hierarchical grid
    typedef GridSelector::GridType HGridType;

    // the method rank and size from MPIManager are static
    if (Fem::MPIManager::rank() == 0) {
      std::clog << "Loading embedded macro grid: "
                << std::endl
                << "=================================" << std::endl
                << dgfData
                << "=================================" << std::endl
                << std::endl;
    }

    // we try to be self-contained an simply load the embedded simple dgf-file
    std::istringstream dgfStream(dgfData);

    // construct macro using the DGF Parser
    GridPtr<HGridType> gridPtr(dgfStream);
    HGridType& grid = *gridPtr;

    // do initial load balance
    grid.loadBalance();

    // initial grid refinement
    //const int level = Fem::Parameter::getValue<int>("globalRefinements", 0);

    // number of global refinements to bisect grid width
    //const int refineStepsForHalf = DGFGridInfo<HGridType>::refineStepsForHalf();

    // refine grid
    //grid.globalRefine(level * refineStepsForHalf);

    auto gridPart = leafGridPart<InteriorBorder_Partition>(grid);

    auto space = discreteFunctionSpace<HGridType::dimensionworld>(gridPart, lagrange<POLORDER>);
    auto scalarSpace = discreteFunctionSpace(gridPart, lagrange<POLORDER>);

    auto Uh = discreteFunction(space, "Uh");
    auto scalarUh = discreteFunction(scalarSpace, "Uh");

    Dune::Fem::interpolate(sqr(gridFunction(gridPart, [](auto&& x) { return x; }, 0_c)), Uh);
    Dune::Fem::interpolate(frobenius2(gridFunction(gridPart, [](auto&& x) { return x; }, 0_c)), scalarUh);

    std::string testName = Fem::Parameter::getValue<std::string>("testName", "default");

    if (testName == "default") {
      runTests(zeroFunctional(scalarSpace));
      runTests(zeroFunctional(space));
      runTests(dofStorageFunctional(scalarUh));
      runTests(dofStorageFunctional(Uh));
      auto X = gridFunction(gridPart,[](auto&& x) { return x; });
      runTests(modelFunctional(laplacianModel(space), X, space));
      auto op = ellipticOperator(laplacianModel(space), X, Uh);
      runTests(operatorFunctional(op, X, space));
      runTests(l2InnerProductFunctional(X, space));
      runTests(l2BoundaryFunctional(X, EntireBoundaryIndicator{}, space));
      runTests(diracFunctional({0.1, 0.1}, space));
    }
    else if (testName == "linear-combination") {
      runTests(-2.0*dofStorageFunctional(scalarUh));
      runTests(2.0*dofStorageFunctional(scalarUh) + 4_f*scalarUh);
      runTests(2.0*dofStorageFunctional(scalarUh) - 4_f*scalarUh);
    }
    else if (testName == "zero-optimization") {
      auto Phi = dofStorageFunctional(scalarUh);
      static_assert(ExpressionTraits<decltype(zero(Phi))>::isZero,
                    "Zero functional is not detected as zero.");
      std::clog << (Phi.name() + " + " + zero(Phi).name())
                << " => "
                << (Phi + zero(Phi)).name()
                << std::endl;
      std::clog << (toString(0_f) +" * " + Phi.name())
                << " => "
                << (0_f * Phi).name()
                << std::endl;
      std::clog << (toString(10_f) +" * " + zero(Phi).name())
                << " => "
                << (10_f * zero(Phi)).name() << std::endl;
    }
    else if (testName == "operate-assign") {
      Uh += dofStorageFunctional(Uh);
      Uh += zero(dofStorageFunctional(Uh));
      Uh -= dofStorageFunctional(Uh);
      Uh -= zero(dofStorageFunctional(Uh));
    }
    else if (testName == "constant-folding") {
      auto Phi = dofStorageFunctional(scalarUh);
      std::clog << (toString(3.0) + " * (" + toString(3.0) + " * " + Phi.name() + ") * " + toString(2_f) + " * " + toString(3.0) + " * " + toString(3_f))
                << " => "
                << (3.0 * (3.0  * Phi) * 2_f * 3.0 * 3_f).name()
                << std::endl;
    }
    else {
      throw std::invalid_argument("Test \""+testName+"\" not implemented");
    }

    return EXIT_SUCCESS;
  }
  catch (std::exception &e){
    std::cerr << "Test reported error: " << e.what() << std::endl;
    return EXIT_FAILURE;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
    return EXIT_FAILURE;
  }
}

//!@} LinearFunctionalTests

//!@} LinearFunctionals

//!@} Operators
