#ifndef __DUNE_ACFEM_OPERATORS_FUNCTIONALS_EXPRESSIONTRAITS_HH__
#define __DUNE_ACFEM_OPERATORS_FUNCTIONALS_EXPRESSIONTRAITS_HH__

#include "../../common/types.hh"
#include "../../expressions/interface.hh"

#include "functionaltraits.hh"

namespace Dune {

  namespace ACFem {

    namespace LinearFunctional {

    } // LinearFunctional::

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_OPERATORS_FUNCTIONALS_EXPRESSIONTRAITS_HH__
