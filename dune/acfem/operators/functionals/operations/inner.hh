#ifndef __DUNE_ACFEM_OPERATORS_FUNCTIONALS_OPERATIONS_INNER_HH__
#define __DUNE_ACFEM_OPERATORS_FUNCTIONALS_OPERATIONS_INNER_HH__

#include "../functionaltraits.hh"

namespace Dune {

  namespace ACFem {

    namespace LinearFunctional {

      template<class Phi, class DF,
               std::enable_if_t<(IsLinearFunctional<Phi>::value
                                 && IsDiscreteFunction<DF>::value
        ), int> = 0>
      constexpr auto inner(const Phi& phi, const DF& df)
      {
        DF tmp("tmp", df.space());
        tmp.clear();
        phi.coefficients(tmp);
        return df.scalarProductDofs(tmp);
      }

    } // LinearFunctional::

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_OPERATORS_FUNCTIONALS_OPERATIONS_INNER_HH__
