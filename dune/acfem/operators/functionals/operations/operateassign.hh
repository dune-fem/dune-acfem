#ifndef __DUNE_ACFEM_OPERATORS_FUNCTIONALS_OPERATIONS_OPERATEASSIGN_HH__
#define __DUNE_ACFEM_OPERATORS_FUNCTIONALS_OPERATIONS_OPERATEASSIGN_HH__

#include "../../../expressions/interface.hh"
#include "../../../common/literals.hh"
#include "../functionaltraits.hh"

namespace Dune {

  namespace ACFem {

    namespace LinearFunctional {

      using namespace Literals;

      using Expressions::DontOptimize;

      template<class DF, class Phi,
               std::enable_if_t<(IsDiscreteFunction<DF>::value
                                 && IsLinearFunctional<Phi>::value
        ), int> = 0>
      constexpr auto& operate(DontOptimize, OperationTraits<PlusEqOperation> op, DF& df, const Phi& phi)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        static_assert(!ExpressionTraits<Phi>::isZero, "+= <0,.> must be optimized away");
        phi.coefficients(df);
        return df;
      }

      template<class DF, class Phi,
               std::enable_if_t<(IsDiscreteFunction<DF>::value
                                 && IsLinearFunctional<Phi>::value
        ), int> = 0>
      constexpr auto& operate(DontOptimize, OperationTraits<MinusEqOperation>, DF& df, const Phi& phi)
      {
        DUNE_ACFEM_RECORD_OPTIMIZATION;

        static_assert(!ExpressionTraits<Phi>::isZero, "-= <0,.> must be optimized away");
        phi.coefficients(-1_f, df);
        return df;
      }

    } // LinearFunctional::

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_OPERATORS_FUNCTIONALS_OPERATIONS_OPERATEASSIGN_HH__
