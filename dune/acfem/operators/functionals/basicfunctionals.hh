#ifndef __DUNE_ACFEM_OPERATORS_FUNCTIONALS_BASICFUNCTIONALS_HH__
#define __DUNE_ACFEM_OPERATORS_FUNCTIONALS_BASICFUNCTIONALS_HH__

#include "modules/zero.hh"
#include "modules/dofstorage.hh"
#include "modules/operatorfunctional.hh"
#include "modules/modelfunctional.hh"
#include "modules/diracfunctional.hh"

namespace Dune::ACFem::LinearFunctional {}

#endif // __DUNE_ACFEM_OPERATORS_FUNCTIONALS_BASICFUNCTIONALS_HH__
