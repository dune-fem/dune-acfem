#ifndef __DUNE_ACFEM_OPERATORS_FUNCTIONALS_CLOSURETRAITS_HH__
#define __DUNE_ACFEM_OPERATORS_FUNCTIONALS_CLOSURETRAITS_HH__

#include "../../common/types.hh"
#include "../../expressions/interface.hh"

#include "functionaltraits.hh"

namespace Dune {

  namespace ACFem {

    namespace LinearFunctional {

      /**LinearFunctional's do not need a closure*/
      template<class T, std::enable_if_t<(IsLinearFunctional<T>::value && !Expressions::IsClosure<T>::value), int> = 0>
      constexpr decltype(auto) expressionClosure(T&& t)
      {
        return std::forward<T>(t);
      }

    } // LinearFunctional::

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_OPERATORS_FUNCTIONALS_CLOSURETRAITS_HH__
