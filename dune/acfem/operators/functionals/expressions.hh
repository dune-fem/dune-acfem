#ifndef __DUNE_ACFEM_OPERATORS_FUNCTIONALS_EXPRESSIONS_HH__
#define __DUNE_ACFEM_OPERATORS_FUNCTIONALS_EXPRESSIONS_HH__

#include "../../expressions/interface.hh"
#include "../../expressions/optimization.hh"

#include "functionalexpression.hh"
#include "functionaltraits.hh"
#include "operandpromotion.hh"
#include "expressiontraits.hh"
#include "closuretraits.hh"
#include "operations/inner.hh"
#include "operations/operateassign.hh"

namespace Dune {

  namespace ACFem {

    namespace LinearFunctional {

      using Expressions::operate;

      /**Provide the canonical zero object.*/
      template<class F, class T, std::enable_if_t<IsLinearFunctional<T>::value, int> = 0>
      auto zero(F&&, T&& t)
      {
        return zeroFunctional(t.space());
      }

      template<class T0, class T1, std::enable_if_t<IsLinearFunctional<T0>::value || IsLinearFunctional<T1>::value, int> = 0>
      auto zero(OperationTraits<SMultiplyOperation>, T0&& t0, T1&& t1)
      {
        if constexpr (IsLinearFunctional<T0>::value) {
          return zeroFunctional(std::forward<T0>(t0).space());
        } else {
          return zeroFunctional(std::forward<T1>(t1).space());
        }
      }

      template<class T, std::enable_if_t<IsProperLinearFunctional<T>::value, int> = 0>
      constexpr auto operator-(T&& t)
      {
        return operate<MinusOperation>(std::forward<T>(t));
      }

      template<class T0, class T1, std::enable_if_t<AreProperLinearFunctionals<T0, T1>::value, int> = 0>
      constexpr auto operator-(T0&& t0, T1&& t1)
      {
        return operate<MinusOperation>(std::forward<T0>(t0), std::forward<T1>(t1));
      }

      template<class T0, class T1, std::enable_if_t<AreProperLinearFunctionals<T0, T1>::value, int> = 0>
      constexpr auto operator+(T0&& t0, T1&& t1)
      {
        return operate<PlusOperation>(std::forward<T0>(t0), std::forward<T1>(t1));
      }

      template<class T0, class T1,
               std::enable_if_t<(Tensor::IsProperTensor<T0>::value
                                 && TensorTraits<T0>::rank == 0
                                 && IsProperLinearFunctional<T1>::value
        ), int> = 0>
      constexpr auto operator*(T0&& t0, T1&& t1)
      {
        return operate<SMultiplyOperation>(std::forward<T0>(t0), std::forward<T1>(t1));
      }

      template<class T0, class T1,
               std::enable_if_t<(Tensor::IsProperTensor<T1>::value
                                 && TensorTraits<T1>::rank == 0
                                 && IsProperLinearFunctional<T0>::value
        ), int> = 0>
      constexpr auto operator*(T0&& t0, T1&& t1)
      {
        return operate<SMultiplyOperation>(std::forward<T1>(t1), std::forward<T0>(t0));
      }

      template<class DF, class Phi,
               std::enable_if_t<(IsDiscreteFunction<DF>::value
                                 && IsProperLinearFunctional<Phi>::value
        ), int> = 0>
      auto& operator+=(DF& df, const Phi& phi)
      {
        return operate<PlusEqOperation>(df, phi);
      }

      template<class DF, class Phi,
               std::enable_if_t<(IsDiscreteFunction<DF>::value
                                 && IsProperLinearFunctional<Phi>::value
        ), int> = 0>
      auto& operator-=(DF& df, const Phi& phi)
      {
        return operate<MinusEqOperation>(df, phi);
      }

    }

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_OPERATORS_FUNCTIONALS_EXPRESSIONS_HH__
