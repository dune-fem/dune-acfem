#ifndef __DUNE_ACFEM_OPERATORS_FUNCTIONALS_FUNCTIONALTRAITS_HH__
#define __DUNE_ACFEM_OPERATORS_FUNCTIONALS_FUNCTIONALTRAITS_HH__

#include <dune/fem/function/common/discretefunction.hh>

#include "../../common/types.hh"
#include "../../expressions/interface.hh"

#include "linearfunctional.hh"

namespace Dune {

  namespace ACFem {

    namespace LinearFunctional {

      template<class T, class SFINAE = void>
      struct IsLinearFunctional
        : FalseType
      {};

      template<class T>
      struct IsLinearFunctional<
        T,
        std::enable_if_t<
          std::is_base_of<
            LinearFunctional<typename std::decay_t<T>::DiscreteFunctionSpaceType>, std::decay_t<T> >::value
          > >
        : TrueType
      {};

      template<class T>
      using IsProperLinearFunctional =
        BoolConstant<IsLinearFunctional<T>::value && !Expressions::IsPromotedTopLevel<T>::value>;

      template<class T0, class T1>
      using AreProperLinearFunctionals =
        BoolConstant<(IsProperLinearFunctional<T0>::value
                      && IsProperLinearFunctional<T1>::value)>;

      template<class T>
      using IsDiscreteFunction = std::is_base_of<Fem::IsDiscreteFunction, std::decay_t<T> >;

    } // LinearFunctional::

    using LinearFunctional::IsLinearFunctional;
    using LinearFunctional::IsDiscreteFunction;

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_OPERATORS_FUNCTIONALS_FUNCTIONALTRAITS_HH__
