#ifndef __DUNE_ACFEM_OPERATORS_FUNCTIONALS_OPERANDPROMOTION_HH__
#define __DUNE_ACFEM_OPERATORS_FUNCTIONALS_OPERANDPROMOTION_HH__

#include "../../common/types.hh"
#include "../../expressions/interface.hh"
#include "../../tensors/tensor.hh"
#include "../../functions/functions.hh"

#include "linearfunctional.hh"
#include "functionaltraits.hh"
#include "modules/dofstorage.hh"

namespace Dune {

  namespace ACFem {

    namespace LinearFunctional {

      using Expressions::Operand;
      using namespace Literals;

      template<class... T>
      using PromoteTensorOperands =
        BoolConstant<(// Handled top-level
                      !AnyIs<Expressions::IsPromotedTopLevel, T...>::value
                      // Something to do?
                      && AnyIs<Tensor::IsNonTensorTensorOperand, T...>::value
                      // One is already a PDE model
                      && AnyIs<IsLinearFunctional, T...>::value)>;

      /**@internal Forward non-tensor-args as is.*/
      template<std::size_t N, class... T,
               std::enable_if_t<(PromoteTensorOperands<T...>::value
                                 && !NthIs<N, Tensor::IsNonTensorTensorOperand, T...>::value
                 ), int> = 0>
      constexpr decltype(auto) operandPromotion(T&&... t)
      {
        return std::forward<TupleElement<N, std::tuple<T...> > >(
          get<N>(std::forward_as_tuple(t...))
          );
      }

      /**@forward non-tensor tensor args as tensors.*/
      template<std::size_t N, class... T,
               std::enable_if_t<(PromoteTensorOperands<T...>::value
                                 && NthIs<N, Tensor::IsNonTensorTensorOperand, T...>::value
                 ), int> = 0>
      constexpr auto operandPromotion(T&&... t)
      {
        return tensor(std::forward<TupleElement<N, std::tuple<T...> > >(
                        get<N>(std::forward_as_tuple(t...))
                        ));
      }

      template<class... T>
      using PromoteDiscreteFunctionOperands =
        BoolConstant<(// Handled top-level
                      !AnyIs<Expressions::IsPromotedTopLevel, T...>::value
                      // Something to do?
                      && AnyIs<IsDiscreteFunction, T...>::value
                      // One is already a PDE model
                      && AnyIs<IsLinearFunctional, T...>::value)>;

      /**@internal Forward non-discrete-function-args as is.*/
      template<std::size_t N, class... T,
               std::enable_if_t<(PromoteDiscreteFunctionOperands<T...>::value
                                 && !NthIs<N, IsDiscreteFunction, T...>::value
                 ), int> = 0>
      constexpr decltype(auto) operandPromotion(T&&... t)
      {
        return std::forward<TupleElement<N, std::tuple<T...> > >(
          get<N>(std::forward_as_tuple(t...))
          );
      }

      /**@forward Convert discrete function operadns to DofStorageFunctional's.*/
      template<std::size_t N, class... T,
               std::enable_if_t<(PromoteDiscreteFunctionOperands<T...>::value
                                 && NthIs<N, IsDiscreteFunction, T...>::value
                 ), int> = 0>
      constexpr auto operandPromotion(T&&... t)
      {
        return dofStorageFunctional(
          std::forward<TupleElement<N, std::tuple<T...> > >(
            get<N>(std::forward_as_tuple(t...))
            ));
      }

      template<class T, class SFINAE = void>
      struct IsProductWithDiscreteFunction
        : FalseType
      {};

      template<class T>
      struct IsProductWithDiscreteFunction<
        T,
        std::enable_if_t<(IsTensorFunction<T>::value
                          && IsProductExpression<GridFunction::RangeTensorType<T> >::value
        )> >
      : BoolConstant<(TensorTraits<Operand<0, GridFunction::RangeTensorType<T>> >::rank == 0
                      && IsLocalFunctionPlaceholder<Operand<1, GridFunction::RangeTensorType<T> > >::value
                      && IsDiscreteFunction<typename Operand<1, GridFunction::RangeTensorType<T> >::GridFunctionType>::value)>
      {};

      template<class... T>
      using PromoteScaledDiscreteFunctionOperands =
        BoolConstant<(// Handled top-level
                      !AnyIs<Expressions::IsPromotedTopLevel, T...>::value
                      // Something to do?
                      && AnyIs<IsProductWithDiscreteFunction, T...>::value
                      // One is already a PDE model
                      && AnyIs<IsLinearFunctional, T...>::value)>;

      /**@internal Forward non-discrete-function-args as is.*/
      template<std::size_t N, class... T,
               std::enable_if_t<(PromoteScaledDiscreteFunctionOperands<T...>::value
                                 && !NthIs<N, IsProductWithDiscreteFunction, T...>::value
                 ), int> = 0>
      constexpr decltype(auto) operandPromotion(T&&... t)
      {
        return std::forward<TupleElement<N, std::tuple<T...> > >(
          get<N>(std::forward_as_tuple(t...))
          );
      }

      /**@forward Convert discrete function operadns to DofStorageFunctional's.*/
      template<std::size_t N, class... T,
               std::enable_if_t<(PromoteScaledDiscreteFunctionOperands<T...>::value
                                 && NthIs<N, IsProductWithDiscreteFunction, T...>::value
                 ), int> = 0>
      constexpr auto operandPromotion(T&&... t)
      {
        decltype(auto) einsum = std::forward<TupleElement<N, std::tuple<T...> > >(
          get<N>(std::forward_as_tuple(t...))
          ).expression();
        return einsum.operand(0_c)*dofStorageFunctional(einsum.operand(1_c).gridFunction());
      }

    }

  } // ACFem::

} // Dune::

#endif // __DUNE_ACFEM_OPERATORS_FUNCTIONALS_OPERANDPROMOTION_HH__
