#ifndef _DUNE_ACFEM_BULK_BLOCKCONSTRAINTS_HH_
#define _DUNE_ACFEM_BULK_BLOCKCONSTRAINTS_HH_

#include "../../common/functor.hh"
#include "../../common/dofmappertupledatahandle.hh"
#include <dune/fem/operator/common/differentiableoperator.hh>
#include <dune/fem/function/blockvectorfunction/blockvectorfunction.hh>

namespace Dune {

  namespace ACFem {

    /**@addtogroup DiscreteOperators
     * @{
     */

    /**@addtogroup ContraintsOperators
     * @{
     */

    /**A default implementation for bulk block
     * constraints. BulkBlockConstraints maintain a flag vector of
     * masked-out DoFs and a DiscreteFunction which contains the
     * values the masked-out DoFs are constrained to. Despite their
     * name currently this class also serves as base for the
     * DirichletBlockConstraints class. The implementation should be
     * fairly efficient, but at the cost of the memory overhead caused
     * by storing the constaint-values in an ordinary
     * Dune::Fem::DiscreteFunction. We only implement the
     * differentiable variant which needs a JacobianOperator
     * (typically a matrix-class) as template argument.
     */
    template<class DiscreteFunctionSpace,
             template<class T, class Alloc = std::allocator<T> > class MaskStorage = std::vector>
    class BulkBlockConstraints
    {
      typedef BulkBlockConstraints ThisType;
     public:
      typedef DiscreteFunctionSpace DiscreteFunctionSpaceType;
      // AdaptiveDiscreteFunction does not work with our tuple data-handle.
      //typedef Fem::AdaptiveDiscreteFunction<DiscreteFunctionSpaceType> ConstraintsStorageType;
      typedef Fem::ISTLBlockVectorDiscreteFunction<DiscreteFunctionSpaceType> ConstraintsStorageType;
      enum { localBlockSize = DiscreteFunctionSpaceType::localBlockSize };
      typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
      typedef typename DiscreteFunctionSpaceType::EntityType EntityType;
      typedef std::bitset<localBlockSize> MaskType;
      typedef MaskStorage<MaskType> MaskStorageType;
     protected:
      typedef typename DiscreteFunctionSpaceType::BlockMapperType BlockMapperType;
      typedef typename BlockMapperType::GlobalKeyType GlobalKeyType;

     public:

      //! Construct an empty operator
      BulkBlockConstraints(const DiscreteFunctionSpaceType& space)
        : space_(space),
          values_("Constraint Values", space_),
          mask_(space_.blockMapper().size(), 0),
          numConstrained_(0),
          sequence_(-1)
      {}

      virtual ~BulkBlockConstraints()
      {}

      /**Update the set of constrained DoFs and their prescirbed
       * DoF-values. It is up to the implementation to decide how this
       * could be done ... ;)
       */
      virtual void update() const = 0;
#if 0
      {
        if (!force && sequence_ == space_.sequence()) {
          return;
        }

        mask_.resize(space_.blockMapper().size(), 0);

        numConstrained_ =
          std::count_if(mask_.begin(), mask_.end(),
                        [](const MaskType& value) { return value.count(); });

        communicate();

        sequence_ = space_.sequence();
      }
#endif

      /**Only update the prescribed values. The default implementation
       * is to reset the space-sequence and simply update values and
       * mask. Individual implementation may want to handle this more
       * efficiently.
       */
      virtual void updateValues() const {
        reset();
        update();
      }

      /**Apply the operator to the given argument. This operates as an
       * affine linear operator which computes
       *
       * w = u - g
       *
       * where g defines the target constraints values. The operator
       * acts only on the given subset of "active" DoFs (e.g. the
       * boundary DoFs) and leaves all other values of w alone.
       */
      template<class DomainDiscreteFunction, class RangeDiscreteFunction>
      void operator()(const DomainDiscreteFunction& u, RangeDiscreteFunction& w) const
      {
        checkUpdate();

        if (size() == 0) {
          return;
        }

        auto uIt      = u.dbegin();
        auto wIt      = w.dbegin();
        auto offsetIt = values_.dbegin();
        for (const auto& mask : mask_) {
          for (int l = 0; l < localBlockSize; ++l, ++wIt, ++offsetIt, ++uIt) {
            if (mask[l]) {
              *wIt = *uIt - *offsetIt;
            }
          }
        }
      }

      /**The Jacobian of the operator. As the operator acts as
       * identity minus affine offset, the Jacobian acts as
       * identity. The methods does not alter any lines of @c jOp
       * except for the "active" DoFs. The method handles slave-DoFs
       * correctly in parallel runs.
       */
      template<class DomainDiscreteFunction, class JacobianOperator>
      void jacobian(const DomainDiscreteFunction& u, JacobianOperator& jOp) const
      {
        checkUpdate();

        if (size() == 0) {
          return;
        }

        std::vector<size_t> rows;
        rows.reserve(size());
        size_t r = 0;
        for (const auto& mask : mask_) {
          for (int l = 0; l < localBlockSize; ++l) {
            if (mask[l]) {
              rows.push_back(r);
            }
            ++r;
          }
        }
        jOp.setUnitRows(rows);
      }

      /**Dune::Fem compat.*/
      template<class DomainDiscreteFunction, class JacobianOperator>
      void applyToOperator(const DomainDiscreteFunction& u, JacobianOperator& jOp) const
      {
        jacobian(u, jOp);
      }

      /**The solution operator; unconditionally install the given
       * constraints into the argument.
       *
       * This "solves"
       *
       * @f[
       * w - g = 0
       * @f]
       *
       * for w.
       */
      template<class DomainDiscreteFunction>
      void constrain(DomainDiscreteFunction& w) const
      {
        checkUpdate();

        if (size() == 0) {
          // nothing to do.
          return;
        }

        auto wIt      = w.dbegin();
        auto offsetIt = values_.dbegin();
        for (const auto& mask : mask_) {
          for (int l = 0; l < localBlockSize; ++l, ++wIt, ++offsetIt) {
            if (mask[l]) {
              *wIt = *offsetIt;
            }
          }
        }
      }

      /** Unconditionally set the values of all masked DoFs to zero. */
      template<class DomainDiscreteFunction>
      void zeroConstrain(DomainDiscreteFunction& w) const
      {
        checkUpdate();

        if (size() == 0) {
          return;
        }

        auto wIt = w.dbegin();
        for (const auto& mask : mask_) {
          for (int l = 0; l < localBlockSize; ++l, ++wIt) {
            if (mask[l]) {
              *wIt = 0;
            }
          }
        }
      }

      /**Return the number of constrained scalar DoFs. */
      size_t size() const {
        return numConstrained_;
      }

      /**Delete the cached "sequence" of the underlying discrete
       * space. The next call to checkUpdate() will then trigger an
       * update of mask and values.
       */
      void reset() const {
        sequence_ = -1;
      }

     protected:
      /**Update mask and values if the underlying space has
       * changed.
       */
      void checkUpdate() const
      {
        if (space_.sequence() != sequence_) {
          update();
        }
      }

      /**Communicate the mask-values and the Dirichlet values as
       * needed.
       */
      virtual void communicate() const
      {
        if (space_.gridPart().comm().size() <= 1) {
          return; // serial run
        }

        typedef
          DofMapperTupleDataHandleTraits<MaskStorageType,
                                         typename ConstraintsStorageType::DofVectorType>
          CommTraitsType;
        typedef typename CommTraitsType::DataItemType DataItemType;
        typedef typename CommTraitsType::DataItemScatterType DataItemScatterType;

        auto dataHandle = dofMapperTupleDataHandle(
          space_,
          [](const DataItemType& a, const DataItemScatterType& b) {
            for (int l = 0; l < localBlockSize; ++l) {
              // copy over data from a if it is constraint. Otherwise
              // keep our data. If both bits are set check for
              // consistency, otherwise we end up with
              // "constraint-ping-pong" with inconsistent values.
              assert(std::get<0>(b)[l] && std::get<0>(a)[l] &&
                     std::abs(std::get<1>(b)[l] - std::get<1>(a)[l]) < 1e-8);
              if (std::get<0>(a)[l]) {
                std::get<1>(b)[l] = std::get<1>(a)[l];
              }
            }
            std::get<0>(b) |= std::get<0>(a);
          },
          mask_,
          values_.dofVector());

        space_.gridPart().communicate(dataHandle,
                                      DiscreteFunctionSpaceType::GridPartType::indexSetInterfaceType,
                                      Dune::ForwardCommunication);
      }

     protected:
      const DiscreteFunctionSpaceType& space_;
      mutable ConstraintsStorageType values_;
      mutable MaskStorageType mask_;
      mutable unsigned numConstrained_;
      mutable int sequence_;
    };

    //! @} ContraintsOperators

    //! @} Operators

  } // ACFem::

} // Dune::


#endif // _DUNE_ACFEM_EMPTY_BLOCKCONSTRAINTS_HH_
