set(acfemconstraintsdir  ${CMAKE_INSTALL_INCLUDEDIR}/dune/acfem/operators/constraints)
set(acfemconstraints_HEADERS
 emptyblockconstraints.hh
 bulkblockconstraints.hh
 dirichletconstraints.hh)
set(SUBDIRS )
# include not needed for CMake
# include $(top_srcdir)/am/global-rules
install(FILES ${acfemconstraints_HEADERS} DESTINATION ${acfemconstraintsdir})
foreach(i ${SUBDIRS})
  if(${i} STREQUAL "test")
    set(opt EXCLUDE_FROM_ALL)
  endif(${i} STREQUAL "test")
  add_subdirectory(${i} ${opt})
  unset(opt)
endforeach(i ${SUBDIRS})
