/**@file
 *
 * Empty do-nothing constraints.
 */
#ifndef _DUNE_ACFEM_EMPTY_BLOCKCONSTRAINTS_HH_
#define _DUNE_ACFEM_EMPTY_BLOCKCONSTRAINTS_HH_

#include <dune/fem/operator/common/differentiableoperator.hh>

namespace Dune {

  namespace ACFem {

    /**@addtogroup DiscreteOperators
     * @{
     */

    /**@addtogroup ContraintsOperators
     * @{
     */

    //! A class modelling do-nothing constraints. Useful to define
    //! default-types in template arguments.
    template<class DiscreteFunctionSpace>
    class EmptyBlockConstraints
    {
      typedef EmptyBlockConstraints ThisType;
     public:
      typedef DiscreteFunctionSpace DiscreteFunctionSpaceType;
      typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
      typedef typename DiscreteFunctionSpaceType::EntityType EntityType;

      template <typename ...Args>
      EmptyBlockConstraints(const Args  & ...){}

      void update() const {}
      void updateValues() const {}

      template<class DomainDiscreteFunction, class RangeDiscreteFunction>
      void operator()(const DomainDiscreteFunction& u, RangeDiscreteFunction& w) const {}

      template<class DomainDiscreteFunction, class JacobianOperator>
      void jacobian(const DomainDiscreteFunction& u, JacobianOperator& jOp) const {}

      template<class DomainDiscreteFunction, class JacobianOperator>
      void applyToOperator(const DomainDiscreteFunction& u, JacobianOperator& jOp) const
      {
        jacobian(u, jOp);
      }

      template<class DomainDiscreteFunction>
      void constrain(DomainDiscreteFunction& w) const {}

      template<class DomainDiscreteFunction>
      void zeroConstrain(DomainDiscreteFunction& w) const {}

      size_t size() const { return 0; }
    };

    //! @} ContraintsOperators

    //! @} Operators

  } // ACFem::

} // Dune::


#endif // _DUNE_ACFEM_EMPTY_BLOCKCONSTRAINTS_HH_
