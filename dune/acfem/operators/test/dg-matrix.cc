#include <config.h>

// iostream includes
#include <iostream>

#include <dune/acfem/common/scopedredirect.hh>

// include header of adaptive scheme
#include <dune/acfem/common/discretefunctionselector.hh>
#include <dune/acfem/functions/basicfunctions.hh>
#include <dune/acfem/models/basicmodels.hh>
#include <dune/acfem/algorithms/ellipticfemscheme.hh>
#include <dune/acfem/algorithms/stationaryadaptivealgorithm.hh>

using namespace Dune::ACFem;

template<class EllipticFemSchemeType>
class GetMatrixFromScheme : public EllipticFemSchemeType
{
  typedef EllipticFemSchemeType BaseType;

  typedef typename BaseType::DiscreteFunctionType DiscreteFunctionType;
  typedef typename BaseType::LinearOperatorType LinearOperatorType;

  using BaseType::solution_;
  using BaseType::operator_;

 public:

  GetMatrixFromScheme(const BaseType& scheme)
    : BaseType(scheme) {}

  const void printMatrix() const
  {
    DiscreteFunctionType unitVectors(solution_);
    unitVectors.clear();
    LinearOperatorType linOperator("matrix", unitVectors.space(), unitVectors.space());
    operator_.jacobian(unitVectors,linOperator);

    MatrixHelper<LinearOperatorType> mh(linOperator);
    std::clog << "------ Matrix of the operator ------" << std::endl << std::endl;
    mh.printPretty(std::clog);
  }
};




/**Implement the most simple elliptic toy problem for a right hand
 * side constructed from an Gaussian "bell" function, in the sense that
 * the "exact solution" @f$u(x) = e^{-C\,|x|^2}@f$ is used in order
 * to construct all data. The discretization will reproduce the
 * "solution" on the unit-square with consecutive boundary ids, like
 * defined by the following DGF-file:
 *
@code
DGF

Interval
 0   0
 1   1
 1   1
#

GridParameter
% longest or arbitrary (see DGF docu)
refinementedge longest
overlap 0
#

#

#
@endcode
 *
 * The code imposes Dirichlet boundary condition on all
 * boundaries. The bulk-equations reads:
 *
 * \f[
 * -\Delta\,u = 2\,C\,(d-2\,C\,|x|^2)\,e^{-C\,|x|^2}
 * \f]
 *
 * After defining auxiliary variables the discrete model is finally constructed in symbolic notation
 *
 * @code
auto pdeModel = -Delta_U - F + (Dbc0 - gD);
@endcode
 *
 * and then passed via an EllipticFemScheme to an adaptiveAlgorithm():
 *
 * @code
SchemeType scheme(solution, pdeModel, exactSolution);

return adaptiveAlgorithm(scheme);
@endcode
 *
 * The constant @f$C@f$ is chosen as .5 in this example in order to
 * have "numerically non-zero" normal derivatives at the boundary.
 *
 */
template<class HGridType>
void operatorMatrix(HGridType &grid)
{
  auto gridPart = leafGridPart<Dune::All_Partition>(grid);
  auto discreteSpace = discreteFunctionSpace(gridPart, dG<1>);
  auto solution = discreteFunction(discreteSpace, "solution");

  // Define some basic ingredients
  auto X = GridFunction::identityGridFunction(gridPart);
  auto X0 = X[0_c];
  auto X1 = X[1_c];

  // auto exactSolution = exp(-C*sqr(X));
  auto exactSolution = X0+X1;
  auto& gD = exactSolution;
  //auto exactSolution = sin(2*M_PI*X0)*sin(2*M_PI*X1);

  // homogeneous boundary models
  auto Dbc = dirichletBoundaryModel(gD, EntireBoundaryIndicator{});

  // bulk contributions
  auto DUDPhi = laplacianModel(discreteSpace);
  auto Mass = massModel(DUDPhi);

  std::clog << "------Mass Operator------" << std::endl;
  // now define the discrete model ...
  auto pdeModel = Mass;
  auto scheme = ellipticFemScheme(solution, pdeModel, exactSolution);
  auto getMatrix = GetMatrixFromScheme<decltype(scheme)>(scheme);
  getMatrix.printMatrix();

  std::clog << "------Laplace Operator------" << std::endl;
  auto pdeModel2 = DUDPhi;
  auto scheme2 = ellipticFemScheme(solution, pdeModel2, exactSolution);
  auto getMatrix2 = GetMatrixFromScheme<decltype(scheme2)>(scheme2);
  getMatrix2.printMatrix();

  std::clog << "------Laplace + Nitsche Dirichlet Values------" << std::endl;
  auto pdeModel3 = DUDPhi + Dbc;
  auto scheme3 = ellipticFemScheme(solution, pdeModel3, exactSolution);
  auto getMatrix3 = GetMatrixFromScheme<decltype(scheme3)>(scheme3);
  getMatrix3.printMatrix();

  std::clog << "Writing a final line, so I dont have empty line at end of file for gitlab..." << std::endl;
}

// main
// ----

#ifndef SRCDIR
# define SRCDIR "."
#endif

int main (int argc, char **argv)
try
{
  // initialize MPI, if necessary
  Dune::Fem::MPIManager::initialize(argc, argv);

  //redirect the stream cerr to cout
  //so we only get the output of clog on stderr
  Dune::ACFem::ScopedRedirect redirect(std::cerr, std::cout);

  std::cerr << "MPI rank " << Dune::Fem::MPIManager::rank() << std::endl;

  // append overloaded parameters from the command line
  Dune::Fem::Parameter::append(argc, argv);

  // append possible given parameter files
  for (int i = 1; i < argc; ++i)
    Dune::Fem::Parameter::append(argv[ i ]);

  // append default parameter file
  Dune::Fem::Parameter::append(SRCDIR "/parameter");

  // type of hierarchical grid
  //typedef Dune::AlbertaGrid< 2 , 2 > GridType;
  typedef Dune::GridSelector::GridType  HGridType ;

  // create grid from DGF file
  const std::string gridkey = Dune::Fem::IOInterface::defaultGridKey(HGridType::dimension);
  const std::string gridfile = SRCDIR "/" + Dune::Fem::Parameter::getValue<std::string>(gridkey);

  // the method rank and size from MPIManager are static
  if (Dune::Fem::MPIManager::rank() == 0)
    std::cout << "Loading macro grid: " << gridfile << std::endl;

  // construct macro using the DGF Parser
  Dune::GridPtr< HGridType > gridPtr(gridfile);
  HGridType& grid = *gridPtr ;

  // let it go ... quasi adapt_method_stat()
  operatorMatrix(grid);

  return 0;
}
catch(const Dune::Exception &exception)
{
  std::cerr << "Error: " << exception << std::endl;
  return 1;
}
