#!/bin/bash

# Environment:
# builddir
# srcdir
# testname
#
# Arguments:
# $1 - test binary

env
set -x

MODE="$1"
TEST="$2"
ARGS="$3"
PREFIX="$4"
EXPECTED="${srcdir}/${testname}.expected"
GENERATED="${builddir}/${testname}.generated"
DIFFERENCE="${builddir}/${testname}.diff"
UDIFFERENCE="${builddir}/${testname}.udiff"

do_exit='rm -f ${GENERATED} ${DIFFERENCE}; (exit $st); exit $st'
trap "st=129; $do_exit" 1
trap "st=130; $do_exit" 2
trap "st=141; $do_exit" 13
trap "st=143; $do_exit" 15

if [ "${MODE}" = check ]; then
    test -f ${EXPECTED} || exit 77
fi

${PREFIX} "${builddir}/${TEST}" ${ARGS} 1> /dev/null 2> ${GENERATED}
sed -i -e 's/-nan/nan/g' ${GENERATED}
#sed -i -e 's/inf\( |$\)/nan /g' ${GENERATED}
#sed -i -E 's/-0(\.0+)?((\s)\s*|($))/0\1\3/g' ${GENERATED}

TESTERROR=$?
if [ "${MODE}" = generate ]; then
    if [ ${TESTERROR} -ne 0 ]; then
	echo "Test program failed to run, not generating ${EXPECTED}" 1>&2
	exit 1
    fi
    mv ${GENERATED} ${EXPECTED}
    exit 0
fi
numdiff -a 1e-10 ${EXPECTED} ${GENERATED} > ${DIFFERENCE}
DIFFERROR=$?

DIFF=$(cat ${DIFFERENCE})

# remove all mess in success and exit with success
if [ ${TESTERROR} -eq 0 ] && [ ${DIFFERROR} -eq 0 ]; then
    rm -f ${GENERATED} ${DIFFERENCE} ${UDIFFERENCE}
    exit 0 # hurray!
fi

# program exited with error
if [ ${TESTERROR} -ne 0 ]; then
    rm -f ${DIFFERENCE} # not of much use
    exit 99 # hard error
fi

# program exited with error
if [ ${DIFFERROR} -ne 0 ]; then
    diff -u ${EXPECTED} ${GENERATED} > ${UDIFFERENCE}
    UDIFF=$(cat ${UDIFFERENCE})
    exit 1 # failed
fi

# otherwise ????? we do not reach this point!
