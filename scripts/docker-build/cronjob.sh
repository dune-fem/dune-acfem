#!/bin/sh

set -xe

cd $(dirname $(realpath $0))

git pull
docker system prune -f --volumes
./generate-acfem-ci-images.sh
./tag-acfem-ci-images.sh
./distribute-acfem-ci-images.sh
