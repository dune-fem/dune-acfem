#! /bin/bash

# Build and tag "extend" duneci docker images which in particular contain
# numdiff.

VERBOSE=false
DRY=""
ONLY=""
ONLYIMAGE=""
ONLYRELEASE=""
REGISTRYUSER=dune
REGISTRYPASS=$(cat $HOME/.docker/registrypass)

# add 2.7, when dune-ci images are available
RELEASES="2.6 2.7 git"

declare -A NAMES BRANCHES PACKAGES

NAMES=(
    [default]="
 debian-10-gcc-8-17
 ubuntu-18.04-clang-6-17
 debian-10-clang-7-libcpp-17
"
    [2.6]="
 debian-9-gcc-6-14
 debian-10-gcc-7-noassert-14
 debian-10-gcc-7-14
"
    [2.7]="
 debian-11-gcc-9-20
 debian-11-gcc-10-20  
"
    [git]="
 debian-10-gcc-7-17
 debian-10-gcc-8-noassert-17
 debian-11-gcc-9-20
 debian-11-gcc-10-20  
 git-ubuntu-18.04-clang-5-17
 ubuntu-18.04-clang-6-17
 ubuntu-20.04-clang-10-20
"
)

BRANCHES=(
    [2.6]="releases/2.6"
    [2.7]="releases/2.7"
    [git]=master
)

PACKAGES=(
    [default]="vim less numdiff petsc-dev openmpi-bin"
    [debian-10]="libpsm2-2"
)

POSITIONAL=()
while [[ $# -gt 0 ]]; do
    key="$1"

    case $key in
        -v|--verbose)
            VERBOSE=true
            shift # past value
            ;;
        -d|--dry)
            DRY=echo
            shift
            ;;
        -r|--release)
            ONLYRELEASE="$2"
            shift
            shift
            ;;
        -i|--image)
            ONLYIMAGE="$2"
            shift # past argument
            shift # past value
            ;;
        -h|--help|*)    # unknown option
            set -- "${POSITIONAL[@]}"
            cat 1>&2<<EOF
Usage:
$0 [-dhv] [--verbose] [--dry] [--help] [--image IMAGENAME] [-i IMAGENAME] [--release RELNAME] [-r RELNAME]

Releases: ${RELEASES}
Images: ${NAMES[*]}
EOF
        exit 1
        ;;
    esac
done

cd $(dirname $0)

LOGFILE=$(mktemp)

function cleanup()
{
    rm -f $LOGFILE
}

trap cleanup EXIT

if ! $VERBOSE; then
    exec 6>&1 7>&2 1> $LOGFILE 2>&1
    MSGFD=6
    ERRFD=6
else
    MSGFD=1
    ERRFD=2
fi

SOMETHING_FAILED=false
for release in $RELEASES; do

    if [ -n "${ONLYRELEASE}" ] && ! [ $release = "$ONLYRELEASE" ]; then
        continue
    fi

    branch=${BRANCHES[$release]}
    for basename in ${NAMES[$release]} ${NAMES[default]}; do

        if [ -n "${ONLYIMAGE}" ] && ! [ $basename = "${ONLYIMAGE}" ]; then
            continue
        fi

        name="$release-$basename"
        distro=$(echo $basename|awk -F - '{ printf("%s-%s", $1, $2); }')

        BUILD_FAILURE=false
        REGISTRY_FAILURE=false
        IMAGETAG=gitlab.mathematik.uni-stuttgart.de:4430/ians-nmh/dune/ci/dune-fem:$name
        # Augment the dune-project core images by ALU and SP. We use the master branch for now ...
        # Disable cache and do pull here, all other images are then built from this one.
        ${DRY} docker image build\
                --no-cache --pull --tag $IMAGETAG\
                --build-arg NAME=$name\
                --build-arg BRANCH=$branch\
                --build-arg PACKAGES="${PACKAGES[default]} ${PACKAGES[$distro]} ${PACKAGES[$basename]}"\
                dockerfiles/fem || BUILD_FAILURE=true

        if ! $BUILD_FAILURE; then
            echo $REGISTRYPASS | $DRY docker login -u $REGISTRYUSER --password-stdin gitlab.mathematik.uni-stuttgart.de:4430 || REGISTRY_FAILURE=true
            $DRY docker push $IMAGETAG || REGISTRY_FAILURE=true

            # Augment the image by Fem, used in ACFem CI
            BUILD_FAILURE=false
            IMAGETAG=gitlab.mathematik.uni-stuttgart.de:4430/ians-nmh/dune/ci/dune-acfem:$name
            ${DRY} docker image build\
                    --no-cache\
                    --tag $IMAGETAG\
                    --build-arg NAME=$name\
                    --build-arg BRANCH=$branch\
                    dockerfiles/acfem || BUILD_FAILURE=true

            if ! $BUILD_FAILURE; then
                echo $REGISTRYPASS | $DRY docker login -u $REGISTRYUSER --password-stdin gitlab.mathematik.uni-stuttgart.de:4430 || REGISTRY_FAILURE=true
                $DRY docker push $IMAGETAG || REGISTRY_FAILURE=true
            fi
        fi

        if $BUILD_FAILURE || $REGISTRY_FAILURE; then
            SOMETHING_FAILED=true
            if $BUILD_FAILURE; then
              echo "Problem: Build $IMAGETAG Failed"
            fi
            if $REGISTRY_FAILURE; then
              echo "Problem: gitlab registry connection failed"
            fi
        fi

    done
done

if $SOMETHING_FAILED; then
    echo "**************** ACFem Docker Image Build Failed ****************************" 1>&$ERRFD
    echo 1>&$ERRFD
    cat $LOGFILE 1>&$ERRFD
else
    echo "**************** ACFem Docker Image Build Succeeded ****************************" 1>&$MSGFD
fi
