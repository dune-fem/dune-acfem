How these scripts are used

- AM00:
  - user gitlab-runner | home-directory /var/lib/gitlab-runner | has no standard shell -> needs login with -s /bin/bash
  - runs nightly cronjob:
    - pulls scarce git repo of dune-acfem only containing the scripts/docker-build directory
    - runs generate-acfem-ci-images.sh
      - redirects streams to only send logs on failure
      - uses base images from dune to
      - generate ci images for fem with alugrid and vim added used to
      - generate ci images for acfem with added fem
      - pushes images to gitlab.mathematik.... registry of dune-ci project
    - runs distribute-acfem-ci-images.sh
      - sshs onto runners (nmh110 and nmh111) (ssh/config configured in host-manager)
        - pulls the images with a deploy-token
        - option -a pulls all versions of a given image

- NMH110 & NMH110
  - user gitlab-runner | home-directory /var/lib/gitlab-runner | has no standard shell -> needs login with -s /bin/bash
  - are set up as gitlab runners for dune-project.org
  - special .toml file specifies gitlab runner usage
    - e.g. the pull= if-not-present option is set there, which uses the images that are already there and does not try to pull new images everytime
  - runs cronjob everyday at 12
    - renames images from long gitlab.mathematik.... name to dune-fem-ci, dune-acfem-ci
    - these names are used in the .gitlab-ci.yml files of dune-fem and dune-acfem


- Possible Improvements
  - docker pull directly on nmh110,nmh111
  - move images to dune gitlab OR improve  access to images (see email robert from 29.08.19)
