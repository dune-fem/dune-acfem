#!/bin/bash

for h in nmh110 nmh111 nmh113 nmh114; do
	echo $h
	ssh gitlab-runner@$h "docker login -u gitlab+deploy-token-2 -p ETCNDkkG9mVYsZ9LxKFN gitlab.mathematik.uni-stuttgart.de:4430 ; docker pull -a gitlab.mathematik.uni-stuttgart.de:4430/ians-nmh/dune/ci/dune-fem; docker pull -a gitlab.mathematik.uni-stuttgart.de:4430/ians-nmh/dune/ci/dune-acfem"
	ssh gitlab-runner@$h dune-acfem/scripts/docker-build/tag-acfem-ci-images.sh
done
