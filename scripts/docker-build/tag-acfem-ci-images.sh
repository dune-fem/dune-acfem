#!/bin/bash

TAGS=$(docker images | grep ci/dune-fem | awk '{if($2 != "<none>"){print $2}}')

echo $TAGS

for TAG in $TAGS ; do
	docker tag gitlab.mathematik.uni-stuttgart.de:4430/ians-nmh/dune/ci/dune-fem:$TAG dune-fem-ci:$TAG
	docker tag gitlab.mathematik.uni-stuttgart.de:4430/ians-nmh/dune/ci/dune-acfem:$TAG dune-acfem-ci:$TAG
done
