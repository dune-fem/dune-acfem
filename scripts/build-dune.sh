#!/bin/bash

##################
#
# This is a script to BUILD the current dune module from anywhere
# inside the dune. It tries to make a good guess for the options file.
# Also sets the --build-dir. If no config file is present it uses
# the standard config files from ACFEm.
#

#ACFem build types exist for
build_types="release debug profiling coverage optim config"

if [ "$1" = -h ]; then
cat<<EOF
Usage:

$0 [buildtype or .opts file] [dunecontrolcommand]

default arguments are

release all

Possible build-types are

$build_types

EOF

exit 0

fi

#
#
###################

set -e

BUILDTYPE=release

#We expect either a Buildtype, or an options (.opts) file.
#Name of the build is then first part of the options file
#also set the arguments for dunecontrol
if ! [ "$1" = "" ]; then
  if [[ "$1" =~ .*.opts ]]; then
    BUILDTYPE=${1/".opts"}
    #shift $1 out of $@
    shift
  else
    if [[ $build_types =~  (^|[[:space:]])$1($|[[:space:]]) ]]; then
      BUILDTYPE=$1
      #shift $1 out of $@
      shift
    fi
  fi
fi

#This enables the execution from anywhere within dune
#We search for a dune.module file to set the modulepath
# If found, we also search for dunecontrol to set the dunepath
path="$(realpath .)"
while [ $path != / ]; do
  if [ -f "$path/dune.module" ]; then
    modulepath="$(realpath $path)"
    break
  fi
  if [ -f "$path/../dune.module" ]; then
    #If we are inside a build directory
    # and $1 is not set
    #change BUILDTYPE accordingly
    if [ "$1" = "" ]; then
      if [[ "$(basename $path)" =~ build-* ]]; then
        BUILDTYPE="$(basename $path)"
        # The # removes the build- prefix
        BUILDTYPE=${BUILDTYPE#"build-"}
      fi
    fi
  fi
  path="$(realpath $path/..)"
done

path="$(realpath .)"
while [ $path != / ]; do
  if [ -f "$path/dune-common/bin/dunecontrol" ]; then
    dunepath=$path
  fi
  path="$(realpath $path/..)"
done

#reset path to have current context for config files
path="$(realpath .)"
echo $BUILDTYPE
echo $modulepath
echo $dunepath

#shortcut to dunecontrol
dunecontrol="$dunepath/dune-common/bin/dunecontrol"
#build the module we are currently in
module="$(grep Module: "$modulepath/dune.module" | cut -d" " -f 2)"

#brackets to get back to current directory
(
cd $dunepath
# if BuildType.opts is present in top level dune context or current context, take it
#otherwise choose the dune-acfem scripts dir opts file
if [ -f "$dunepath/$(basename ${BUILDTYPE}.opts)" ]; then
  config="$dunepath/$(basename ${BUILDTYPE}.opts)"
else
  if [ -f "$path/${BUILDTYPE}.opts" ]; then
    config="$path/${BUILDTYPE}.opts"
  else
    if [ -f $dunepath/dune-acfem/scripts/opts/${BUILDTYPE}.opts ]; then
      config="$dunepath/dune-acfem/scripts/opts/${BUILDTYPE}.opts"
    else
      echo "Could not find any valid .opts file. Either enter a valid opts file or build type. Or change the build-dune.sh script."
      exit 1
    fi
  fi
fi

# clear current module's caches for cleaner build
cmakecache="$modulepath/build-$BUILDTYPE/CMakeCache.txt"
[ -f $cmakecache ] && rm $cmakecache && echo "Removed \"$cmakecache\""

#finally run dunecontrol with set builddir and config file.
if [ -z "$module" ]; then
    set -x
    $dunecontrol --builddir="build-${BUILDTYPE}" --opts=$config "${@:-all}"
else
    set -x
    $dunecontrol --builddir="build-${BUILDTYPE}" --opts=$config --module=$module "${@:-all}"
fi
)
