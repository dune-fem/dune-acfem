#include CATCH_HEADER

#include <type_traits>

#include <dune/acfem/mpl/lexcompare.hh>

TEST_CASE( "Lexicographical compare of sequences.", "[lexcompare]" ) {

  using namespace Dune::ACFem;
  using namespace Dune::ACFem::MPL;

  CHECK( LexCompareV<IndexSequence<0, 0, 0>, IndexSequence<0, 0> > == 1 );
  CHECK( LexCompareV<IndexSequence<0, 0>, IndexSequence<0, 0, 0> > == -1 );
  CHECK( LexCompareV<IndexSequence<1, 2, 3, 4>, IndexSequence<2, 2, 3, 4> > == -1 );
  CHECK( LexCompareV<IndexSequence<2, 2, 4, 5>, IndexSequence<2, 2, 3, 4> > == 1 );
  CHECK( LexCompareV<IndexSequence<1, 2, 3, 4>, IndexSequence<1, 2, 3, 4> > == 0 );
}
