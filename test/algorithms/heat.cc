#include CATCH_HEADER

/**Dune::Fem implementation of the heat equation.
 */
#include <config.h>

// iostream includes
#include <iostream>

#include <dune/acfem/common/discretefunctionselector.hh>
#include <dune/acfem/functions/basicfunctions.hh>
#include <dune/acfem/algorithms/parabolicfemscheme.hh>
#include <dune/acfem/algorithms/transientadaptivealgorithm.hh>
#include <dune/acfem/models/basicmodels.hh>
#include <dune/acfem/parameters/timestep.hh>
#include <dune/acfem/parameters/timestepreciprocal.hh>
#include <dune/acfem/parameters/timeview.hh>

using namespace Dune::ACFem;

/**Implement a very simple heat-equation solver which reproduces the
 *"exact" "solution"
 *
 * @f[
 * u(x,t) = cos(\pi\,t)\,\prod_{k=1}^d \cos(2\,\pi\,x_k)
 * @f]
 *
 * The "algorithm" solves the simple parabolic problem
 *
 * @f[
 * \begin{split}
 * \partial_t u - \Delta u = f&\text{ in } \Omega_T:=(0,1)^d\times[0,T),\\
 *                    u = g&\text{ on } \partial\Omega\times[0,T),\\
 *                    u = u_0&\text{ on } \Omega\times\{0\}.
 * \end{split}
 * @f]
 *
 * The data (rhs, boundary values, initial value) is defined by the
 * given "exact" "solution".
 */
template<class HGridType>
double algorithm(HGridType &hGrid)
{
  typedef Dune::Fem::GridTimeProvider<HGridType> TimeProviderType;

  /****************************************************************************
   *
   * Initialize the parameters governing the method from the data file
   *
   */

  // Global simulation times
  const double endTime  = Dune::Fem::Parameter::getValue<double>("heat.endTime", 2.0);
  const double startTime = Dune::Fem::Parameter::getValue<double>("heat.startTime", 0.0);

  // The time-step and mu are intentionally non-const. timeStep is
  // only the initial time-step size for the "implicit"
  // time-adaptation method
  double timeStep = Dune::Fem::Parameter::getValue<double>("heat.timeStep", 0.125);

  // Create the timeProvider.
  TimeProviderType timeProvider(startTime, hGrid);
  timeProvider.init(timeStep);

  // get theta for theta scheme
  const double theta = Dune::Fem::Parameter::getValue<double>("heat.theta", 1.0);

  const double implicitTimeFactor = theta;
  const double explicitTimeFactor = (1.0 - theta);

  // Provide a parameter for the inverse time step which will be
  // injected into the implicit and explicit model-expression.
  auto mu = timeStepReciprocal(timeProvider);

  // Provide views into the current time step to inject the evaluation
  // time into the data functions
  auto t_old = timeView(timeProvider, 0.0);
  auto t_new = timeView(timeProvider, 1.0);
  auto t_theta = timeView(timeProvider, theta);

  /*
   *
   ****************************************************************************
   *
   * Generate a suitable discrete function to store the solution and
   * construct the fem-scheme from that and other stuff.
   *
   */

  auto discreteSpace = discreteFunctionSpace(hGrid, lagrange<POLORDER>);
  const auto& gridPart = discreteSpace.gridPart();
  auto solution = discreteFunction(discreteSpace, "solution");

  auto dimDomain = intFraction<discreteSpace.dimDomain>();

  /*
   *
   ****************************************************************************
   *
   * Generate the implicit and the explicit parts of the differential
   * operator, and finally generate the parabolic fem-scheme from that
   * information.
   *
   */

  /*
   *
   ****************************************************************************
   *
   * Construct implicit and explicit models by model expressions.
   *
   */

  // Define some basic ingredients
  auto X = gridFunction(gridPart, [](auto&& x) { return x; });

  // Define the "solution" for testing purposes. This is a little more
  // complicated than necessary here in order to provide the example
  // for dimension 1, 2, 3. In principle you would just say:
  //
  // auto exactSolution = cos(*t_old)*cos(2.*M_PI*X0)*cos(2.*M_PI*X1)*cos(2.*M_PI*X2);
  //
  // Arguably, template-recursion would be more C++-ish, but so what.
  auto cosineProduct = (cos(2.*M_PI*X[0_c])
#if GRIDDIM > 1
                        *
                        cos(2.*M_PI*X[1_c])
# if GRIDDIM > 2
                        *
                        cos(2.*M_PI*X[2_c])
# endif
#endif
    );

  // Forces function, u_t - Delta u. Built from the "solution"
  auto F = (M_PI*(4.*dimDomain*M_PI*cos(M_PI * t_theta) - sin(M_PI * t_theta))
            *
            cosineProduct);

  // boundary models
  auto Dbc = dirichletBoundaryModel((cos(M_PI * t_new) * cosineProduct));

  // bulk models
  auto Delta_U = -laplacianModel(discreteSpace);
  auto U = massModel(discreteSpace);

  /*
   *
   ****************************************************************************
   *
   * Define the implicit and explicit models. This is constructed such
   * that
   *
   * implicitModel(U) + explicitModel(U_old) = 0
   *
   * has finally to be solved for. This explains the signs.
   *
   */

  // implicit part, carrying rhs and Dirichlet data
  auto implicitModel(mu * U - implicitTimeFactor * Delta_U - F + Dbc);

  // explicit part, perhaps carrying part of the elliptic operator
  auto explicitModel(-mu * U - explicitTimeFactor * Delta_U);

  /*
   *
   ****************************************************************************
   *
   * Finally put everything together and define the transient adaptive
   * FEM schem.
   *
   * It is crucial that the implicit and explicit models
   * use the TimeStepReciprocal-class for mu. This ensures that
   * the models always use the current time-step size (which may be
   * changed by the adaptive algorithm).
   *
   * We use here the exactSolution as initial value. To be correct:
   * the ParabolicFemScheme always expects an initial value, and
   * optionally that parameter can also be used to provide an "exact"
   * "solution" for testing purposes. Hence the exact solution is
   * evaluated at t_old.
   */
  auto exactSolution = cos(M_PI * t_old) * cosineProduct; // aka initialValue
  auto& initialValue(exactSolution);

  auto scheme = parabolicFemScheme(solution, timeProvider, implicitModel, explicitModel, initialValue, "heat");
  /*
   *
   ****************************************************************************
   *
   * Algorithm starts from here. First try to create a good initial
   * approximation, then run the adaptive space-time method which is
   * also implemented in the ALBERTA toolbox
   *
   * The scheme-parameter defines the solve, estimate and adapt
   * ingredients
   *
   * The timeProvider is responsible to distribute the correct time
   * and time-step size to all ingredients of the algorithm.
   *
   * endTime -- guess what that means, you fancy ingenious person!
   *
   * "heat", this is just a prefix for the parameter file in order to
   * reduce the usual parameter chaos somewhat.
   *
   */

  return adaptiveAlgorithm(scheme, timeProvider, endTime, "heat");
}

// main
// ----

TEST_CASE( "Heat Equation" ) {

  int argc = 0;
  char **argv = nullptr;

  // initialize MPI, if necessary
  Dune::Fem::MPIManager::initialize(argc, argv);

  // append overloaded parameters from the command line
  Dune::Fem::Parameter::append(argc, argv);

  // append possible given parameter files
  for (int i = 1; i < argc; ++i)
    Dune::Fem::Parameter::append(argv[ i ]);

  // append default parameter file
  Dune::Fem::Parameter::append(DATADIR "/" "parameter");

  // type of hierarchical grid
  //typedef Dune::AlbertaGrid< 2 , 2 > GridType;
  typedef Dune::GridSelector::GridType  HGridType ;

  Dune::GridPtr< HGridType > gridPtr;

  bool restart = Dune::Fem::Parameter::getValue<bool>("heat.restart", false);

  if (restart) {
    std::string name = Dune::Fem::CheckPointerParameters().checkPointPrefix();
    gridPtr = Dune::Fem::CheckPointer<HGridType>::restoreGrid(name);
  } else {
    // create grid from DGF file
    const std::string gridkey = Dune::Fem::IOInterface::defaultGridKey(HGridType::dimension);
    const std::string gridfile = DATADIR "/" + Dune::Fem::Parameter::getValue< std::string >(gridkey);

    // the method rank and size from MPIManager are static
    if (Dune::Fem::MPIManager::rank() == 0)
      std::cout << "Loading macro grid: " << gridfile << std::endl;

    // construct macro using the DGF Parser
    gridPtr = Dune::GridPtr<HGridType>(gridfile);

    // do initial load balance
    gridPtr->loadBalance();

    // initial grid refinement
    const int level = Dune::Fem::Parameter::getValue<int>("heat.initialRefinementLevel", 0);

    // number of global refinements to bisect grid width
    const int refineStepsForHalf = Dune::DGFGridInfo< HGridType >::refineStepsForHalf();

    // refine grid
    gridPtr->globalRefine(level * refineStepsForHalf);
  }

  // start algorithm
  double maxError = algorithm( *gridPtr );
  std::cout << "Estimated maximum Error: " << maxError << std::endl;
}
