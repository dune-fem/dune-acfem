#include CATCH_HEADER

#include <dune/acfem/tensors/tensor.hh>
#include <dune/acfem/tensors/bindings.hh>

#include <dune/acfem/tensors/modules/linearstorage.hh>

using namespace Dune;
using namespace ACFem;
using namespace Tensor;

TEST_CASE( "basic expression test for tensors" ) {
  FieldVectorType<double, 3, 3> m1, m2, m3;

  using ResultType = LinearStorageTensor<std::array<double,9>, Seq<3, 3> >;
  ResultType res1({-5, 0, 0,
      0, -5, 0,
      0, 0, -5 });
  ResultType res2({0, 0, 0,
      0, 0, 0,
      0, 0, 0  });
  ResultType res3({-2, 0, 0,
      0, -2, 0,
      0, 0, -2 });

  CHECK( (-5*eye(m1)) == res1 );

  m1 = -5*eye(m1);
  m2 = 0;
  m3 = m1 + m2 + 3.*eye(m2);
  CHECK( m1 == res1 );
  CHECK( m2 == res2 );
  CHECK( m1 + m2 + 3*eye(m2) ==  res3 );
}
