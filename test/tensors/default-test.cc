#include CATCH_HEADER

#include <dune/acfem/tensors/tensor.hh>

using namespace Dune;
using namespace Dune::ACFem;
using namespace Tensor;

TEST_CASE( "default test for tensors" ) {
  FieldTensor<double, 3, 4, 5, 6> t;

  CHECK( operandPromotion<0>(intFraction<2>(), t).name() == "tensor<2_c>" );
  CHECK( operandPromotion<1>(intFraction<2>(), t).name() == "FT(<3,4,5,6>)");
  CHECK( (intFraction<2>()*t).name() == "<cls (tensor<2_c> * refFT(<3,4,5,6>))/>");

  CHECK( std::is_same<decltype(t), FieldVectorTensor<FieldVector<FieldVector<FieldMatrix<double, 5, 6>, 4>, 3> > >::value );
  t(1, 2, 3, 4) = 5.0;
  t(1, 2, 3, 4) *= 2.0;
  CHECK(t(1, 2, 3, 4) == 10);
  CHECK( t.rank == 4);
  CHECK( std::is_same<decltype(t)::Signature, IndexSequence<3, 4, 5, 6> >::value);
}
