#ifndef __DUNE_ACFEM_TEST_TENSORS_PRINT_HH__
#define __DUNE_ACFEM_TEST_TENSORS_PRINT_HH__

#include <dune/acfem/common/ostream.hh>
#include <dune/acfem/expressions/polynomialtraits.hh>
#include <dune/acfem/tensors/tensor.hh>

namespace Testing {

  using namespace Dune;
  using namespace ACFem;
  using namespace Tensor;

  template<class T>
  void printTensor(T&& t, const std::string& msg, bool values = true, bool degree = false)
  {
    if (msg != "") {
      std::clog << msg << std::endl;
    }
    std::clog << (IsTensor<T>::value ? name(t) : typeString(std::forward<T>(t))) << std::endl;
    if (degree) {
      std::clog << " order: " << polynomialDegree<0>(std::forward<T>(t)) << std::endl;
    }
    //std::clog << " depth: " << Expressions::Depth<T>::value << std::endl;
    if (values) {
      std::clog << t << std::endl;
    }
  }

  template<class T>
  void printTensor(T&& t, const char* msg, bool values = true, bool degree = false)
  {
    printTensor(std::forward<T>(t), std::string(msg), values, degree);
  }

  template<class T>
  void printTensor(T&& t, bool values = true, bool degree = false)
  {
    printTensor(std::forward<T>(t), "", values, degree);
  }

} // NS

#endif // header guard
