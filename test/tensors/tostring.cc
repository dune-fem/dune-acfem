#include CATCH_HEADER

#include <dune/acfem/tensors/tensor.hh>

using namespace Dune;
using namespace Dune::ACFem;
using namespace Tensor;

TEST_CASE( "to string for tensors" ) {
  FieldTensor<double, 3, 4, 5, 6> t;

  CHECK( t.name() == toString(t) );
}
