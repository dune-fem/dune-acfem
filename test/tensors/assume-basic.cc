#include CATCH_HEADER

#include <dune/acfem/tensors/tensor.hh>
#include <dune/acfem/tensors/bindings.hh>

using namespace Dune;
using namespace Dune::ACFem;
using namespace Tensor;

TEST_CASE( "Basic assume for expressions" ) {
  FieldTensor<double, 3, 4, 5, 6> t;

  auto positive = assume<PositiveExpression>(t);

  static_assert(positive > 0_c, "assume(Expr > 0) failed");
  static_assert(!(t > 0_c), "!(Expr > 0) failed");
  (void)positive;
}
