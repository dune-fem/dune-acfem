#include CATCH_HEADER

#include <dune/acfem/tensors/tensor.hh>
#include <dune/acfem/tensors/bindings.hh>



using namespace Dune;
using namespace Dune::ACFem;
using namespace Tensor;

TEST_CASE( "kronecker products" ) {
  auto t0 = kroneckerDelta<0>(Seq<3>{});
  auto t1 = kroneckerDelta<2>(Seq<3>{});
  auto t2 = kroneckerDelta<1,2>(Seq<3,3>{});
  auto t3 = kroneckerDelta<1,1>(Seq<3,3>{});
  using ResultType33 = LinearStorageTensor<std::array<double,9>, Seq<3, 3> >;
  using ResultType3 = LinearStorageTensor<std::array<double,3>, Seq<3> >;

  ResultType33 res1({0,0,1,0,0,0,0,0,0});
  ResultType3 res2({0,1,0});

  CHECK(name(outer(t0, t1)) == "<cls delta<3,3@[0,2]>/>");
  CHECK(outer(t0,t1) == res1);
  CHECK(std::is_same<decltype(contractInner(t0, t1)), decltype(zeros<>())>::value );
  CHECK(contractInner(t0,t1) == zeros<>());
  CHECK(name(contractInner(t2, t1)) == "<cls delta<3@[1]>/>");
  CHECK(contractInner(t2,t1) == res2);
  CHECK(std::is_same<decltype(contractInner(t2, t3)), decltype( zeros<3,3>())>::value);
  CHECK(contractInner(t2,t3) == zeros<3,3>());
}
