#include CATCH_HEADER

#include <dune/acfem/tensors/operations/reshape.hh>
#include <dune/acfem/tensors/tensor.hh>
#include <dune/acfem/tensors/bindings.hh>



using namespace Dune;
using namespace Dune::ACFem;
using namespace Tensor;

TEST_CASE( "reshape operation for tensors" ) {

  auto parent = tensor(Dune::FieldMatrix<double, 2, 3>{{1,2,3},{4,5,6}});

  auto reshaped = reshape<3, 2>(parent);

  SECTION( "correct component access" ) {

    CHECK( parent(1,0) == reshaped(1,1) );
    CHECK( parent(Seq<1,0>{}) == reshaped(Seq<1,1>{}) );

    {
      const auto parent = tensor(Dune::FieldMatrix<double, 2, 3>{{1,2,3},{4,5,6}});

      const auto reshaped = reshape<3, 2>(parent);

      CHECK( parent(1,0) == reshaped(1,1) );
    }

    auto reshapedLin = reshape<6>(parent);

    CHECK( parent(1,0) == reshapedLin(3) );

    parent(1,0) = 42;

    CHECK( parent(1,0) == 42 );
    CHECK( parent(1,0) == reshapedLin(3) );

    reshapedLin(3) = 24;

    CHECK( reshapedLin(3) == 24 );
    CHECK( parent(1,0) == reshapedLin(3) );

  }
  SECTION( "optimizations" ) {

    // Check for do nothing reshape.
    auto bareParent = asExpression(parent);

    CHECK(
      std::is_same<
        std::decay_t<decltype(bareParent)>,
        std::decay_t<decltype(asExpression(reshape(bareParent, bareParent.signature())))>
      >::value
    );

    auto& reshapedBareParent = asExpression(reshape(bareParent, bareParent.signature()));
    CHECK( &bareParent == &reshapedBareParent );

    // Check for reshape of reshape tensor
    static_assert(!IsReshape<Operand<0, decltype(asExpression(reshape<3,2>(reshape<6>(parent))))> >::value,
                  "Nested reshape should be optimized away.");

  }
  SECTION( "derivative" ) {

    auto x = indeterminate<0, 6>();
    auto expr = derivative(reshape<3, 2>(5*x), x);
    auto result = Expressions::replaceIndeterminate<0>(expr, tensor(Dune::FieldVector<double, 6>{1, 1, 1, 1, 1, 1}));

    CHECK( result(0, 0, 0) == 5 );
    CHECK( result(0, 0, 1) == 0 );
    CHECK( result(1, 0, 2) == 5 );

  }
}
