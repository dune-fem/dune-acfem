#include CATCH_HEADER

#include <dune/acfem/tensors/tensor.hh>
#include <dune/acfem/tensors/bindings.hh>


using namespace Dune;
using namespace Dune::ACFem;
using namespace Tensor;

TEST_CASE( "test inverse operations" ) {
  //CHECK( std::is_same<decltype(sqrt(sqr(cos(eye<2,2>()))+sqr(sin(eye<2,2>())))),decltype(eye<2,2>())>::value );
  CHECK( std::is_same<decltype(-(-(eye<2,2>()))),decltype(eye<2,2>())>::value );
  CHECK(  -(-(eye<2,2>())) == eye<2,2>());

  CHECK( std::is_same<decltype(exp(log(eye<2,2>()))), decltype(eye<2,2>())>::value);
  CHECK( exp(log(eye<2,2>())) == eye<2,2>());
  CHECK( std::is_same<decltype(log(exp(eye<2,2>()))),decltype(eye<2,2>())>::value );
  CHECK( log(exp(eye<2,2>())) == eye<2,2>());


  CHECK( std::is_same<decltype(acosh(cosh(eye<2,2>()))), decltype(acosh(cosh(eye<2,2>())))>::value );
  CHECK( frobenius2(acosh(cosh(eye<2,2>())) - eye<2,2>()) < 1e-8);
  CHECK( std::is_same<decltype(cosh(acosh(eye<2,2>()))),decltype(eye<2,2>())>::value );
  CHECK( cosh(acosh(eye<2,2>())) == eye<2,2>());


  CHECK( std::is_same<decltype(asinh(sinh(eye<2,2>()))),decltype(eye<2,2>())>::value );
  CHECK( asinh(sinh(eye<2,2>())) == eye<2,2>());
  CHECK( std::is_same<decltype(sinh(asinh(eye<2,2>()))),decltype(eye<2,2>())>::value );
  CHECK( sinh(asinh(eye<2,2>())) == eye<2,2>());


  CHECK( std::is_same<decltype(atanh(tanh(eye<2,2>()))),decltype(eye<2,2>())>::value );
  CHECK( atanh(tanh(eye<2,2>())) == eye<2,2>());
  CHECK( std::is_same<decltype(tanh(atanh(eye<2,2>()))),decltype(eye<2,2>())>::value );
  CHECK( tanh(atanh(eye<2,2>())) == eye<2,2>());


  CHECK( std::is_same<decltype(acos(cos(eye<2,2>()))), decltype(acos(cos(eye<2,2>())))>::value );
  CHECK( frobenius2(acos(cos(eye<2,2>())) - eye<2,2>()) < 1e-8);
  CHECK( std::is_same<decltype(cos(acos(eye<2,2>()))),decltype(eye<2,2>())>::value );
  CHECK( cos(acos(eye<2,2>())) == eye<2,2>());


  CHECK( std::is_same<decltype(asin(sin(eye<2,2>()))), decltype(asin(sin(eye<2,2>())))>::value );
  CHECK( asin(sin(eye<2,2>())) == eye<2,2>());
  CHECK( std::is_same<decltype(sin(asin(eye<2,2>()))),decltype(eye<2,2>())>::value );
  CHECK( sin(asin(eye<2,2>())) == eye<2,2>());


  CHECK( std::is_same<decltype(atan(tan(eye<2,2>()))), decltype(atan(tan(eye<2,2>())))>::value);
  CHECK( atan(tan(eye<2,2>())) == eye<2,2>());
  CHECK( std::is_same<decltype(tan(atan(eye<2,2>()))),decltype(eye<2,2>())>::value );
  CHECK( tan(atan(eye<2,2>())) == eye<2,2>());


  CHECK( std::is_same<decltype(sqrt(sqr(eye<2,2>()))), decltype(eye<2,2>())>::value);
  CHECK( sqrt(sqr(eye<2,2>())) ==  eye<2,2>());
}
