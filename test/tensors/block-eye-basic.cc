#include CATCH_HEADER

#include <dune/acfem/tensors/modules/blockeye.hh>
#include <dune/acfem/tensors/tensor.hh>
#include <dune/acfem/tensors/bindings.hh>


using namespace Dune;
using namespace Dune::ACFem;
using namespace Tensor;

TEST_CASE( "block eye basic test for tensors" ) {
  auto t = Tensor::blockEye<3, 2, 3>();

  CHECK(std::is_same< decltype(t), TensorResult<BlockEye<3, IndexSequence<2, 3>, FractionConstant<long, 1, 1> > > >::value );
  CHECK(t.isZero<0, 1, 0, 1, 0, 1>() == false);
  CHECK(t.isZero<0, 1, 1, 1>() == true);
}
