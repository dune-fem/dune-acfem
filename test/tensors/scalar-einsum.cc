#include CATCH_HEADER

#include <dune/acfem/tensors/tensor.hh>
#include <dune/acfem/tensors/bindings.hh>

using namespace Dune;
using namespace ACFem;
using namespace Tensor;

TEST_CASE( "Test rank-0 optimization of einsum") {
  auto t = FieldTensor<double, 3, 4, 5, 6>();

  auto m = frobenius2(t);
  CHECK( std::is_same<decltype(m), TensorResult<EinsteinSummation<FieldVectorTensor<FieldVector<FieldVector <FieldMatrix<double, 5, 6>, 4>, 3> >&, IndexSequence< 0, 1, 2, 3>, FieldVectorTensor<FieldVector<FieldVector<FieldMatrix<double, 5, 6>, 4>, 3> >&, IndexSequence<0, 1, 2, 3>, false> > >::value );
  CHECK( m.rank == 0);

  auto c = frobenius2(std::move(t));
  CHECK( std::is_same<decltype(c), TensorResult<FieldVectorTensor<double> > >::value );
  CHECK( c.rank == 0);
}
