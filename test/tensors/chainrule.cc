#include CATCH_HEADER

#include <config.h>
#include <dune/acfem/parameters/generic.hh>
#include <dune/acfem/tensors/tensor.hh>

#include "print.hh"

using namespace Dune;
using namespace Dune::ACFem;
using namespace Tensor;
using namespace Testing;

TEST_CASE( "P-laplacian test" ) {
  int argc = 0;
  char** argv = NULL;
  Dune::Fem::MPIManager::initialize(argc, argv);

  auto t = indeterminate<0>(FieldTensor<double>(1));
  auto x = indeterminate<1>(FieldTensor<double, 3>(2));
  auto uRaw = t*ones<3>() + 2_f * x;
  auto u = indeterminate<2>(std::move(uRaw));
  auto U = indeterminate<2>(denseStorage<true>(uRaw));

  CHECK( sqrt(u * u) == sqrt(uRaw * uRaw) );
  CHECK( derivative(sqrt(u * u), t) == derivative(sqrt(uRaw * uRaw), t) );
  CHECK( derivative(sqrt(u * u), x) == derivative(sqrt(uRaw * uRaw), x) );
  CHECK( derivative(sqrt(u * u), u) == derivative(sqrt(U * U), U) );
}
