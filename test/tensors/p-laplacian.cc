#include CATCH_HEADER

#include <config.h>
#include <dune/acfem/parameters/generic.hh>

#include <dune/acfem/tensors/tensor.hh>

using namespace Dune;
using namespace Dune::ACFem;
using namespace Tensor;

TEST_CASE( "P-laplacian test" ) {
  int argc = 0;
  char** argv = NULL;
  Dune::Fem::MPIManager::initialize(argc, argv);

  auto x = indeterminate<0>(FieldTensor<double, 3>()); (void)x;
  auto u = indeterminate<1>(FieldTensor<double>()); (void)u;
  auto Du = indeterminate<2>(FieldTensor<double, 3>());
  auto Dphi = placeholder(FieldTensor<double, 3>());
  auto p = parameter("p", 2.0);

  auto pLaplacian = inner(pow(Du*Du, (p-2_f)/2_f)*Du, Dphi);
  CHECK(name(pLaplacian) == "<cls ((refX[2](FT([0 0 0])) *[0][0] ref?!(FT([0 0 0]))) * ((refX[2](FT([0 0 0])) *[0][0] refX[2](FT([0 0 0]))) ^ ((tensor<(1/2)_c> * refp) - ones<>)))/>");
  CHECK(pLaplacian == 0);
  CHECK(name(derivative(pLaplacian, Du)) == "<cls (((((refp - tensor<2_c>) * ref(refX[2](FT([0 0 0])) *[0][0] ref?!(FT([0 0 0])))) * (ref(refX[2](FT([0 0 0])) *[0][0] refX[2](FT([0 0 0]))) ^ ((tensor<(1/2)_c> * refp) - tensor<2_c>))) * refX[2](FT([0 0 0]))) + (ref((refX[2](FT([0 0 0])) *[0][0] refX[2](FT([0 0 0]))) ^ ((tensor<(1/2)_c> * refp) - ones<>)) * ref?!(FT([0 0 0]))))/>");
}
