get_filename_component(MODULE ${CMAKE_CURRENT_SOURCE_DIR} NAME)

set(TESTS
  assume-basic
  basic
  basic-expressions
  block-eye-basic
  chainrule
  default-test
  inverse-operations
  kronecker-products
  p-laplacian
  reshape
  scalar-einsum
  tostring
)

add_custom_target("test-${MODULE}")

foreach(NAME ${TESTS})
  set(TEST ${MODULE}-${NAME})
  set(TARGET "test-${TEST}")

  # individual testcases are linked with the main object to create an executable
  add_executable(${TARGET} ${NAME}.cc $<TARGET_OBJECTS:test-main>)
  set_target_properties(${TARGET} PROPERTIES OUTPUT_NAME ${NAME})
  dune_target_enable_all_packages(${TARGET})

  dune_add_test(NAME ${TEST} TARGET ${TARGET} COMMAND ${TARGET})
  add_dependencies("test-${MODULE}" ${TARGET})
endforeach()
