#include CATCH_HEADER
#include <type_traits>

unsigned int Factorial(unsigned int number) {
    return number <= 1 ? number : Factorial(number-1)*number;
}

TEST_CASE( "Factorials are computed", "[factorial]" ) {
  int a = 5;
    //CHECK( std::is_same_v<double, decltype(a)> );
    //CHECK( Factorial(0) == 1 );
    CHECK( Factorial(1) == 1 );
    CHECK( Factorial(2) == 2 );
    CHECK( Factorial(3) == 6 );
    CHECK( Factorial(10) == 3628800 );
}
