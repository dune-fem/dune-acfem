#include CATCH_HEADER

#include <fstream>
#include <sstream>
#include <dune/acfem/common/csvwriter.hh>

using Catch::Matchers::Contains;

using namespace Dune::ACFem;

TEST_CASE( "plain constructor" ) {
  CSVWriter<int, double, std::string> csv{};
}

TEST_CASE( "write with header rows" ) {
  std::ostringstream csvSS;

  CSVWriter<int, double, std::string> csv{csvSS, "Integer", "Double", "String"};

  CHECK_THAT( csvSS.str(), Contains("Integer") );
  CHECK_THAT( csvSS.str(), Contains("Double") );
  CHECK_THAT( csvSS.str(), Contains("String") );

  csv.write(5, 6.8, "uiae");

  CHECK_THAT( csvSS.str(), Contains("5") );
  CHECK_THAT( csvSS.str(), Contains("6.8") );
  CHECK_THAT( csvSS.str(), Contains("uiae") );
}
