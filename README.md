# Dune-ACFem

Convenient discretization of Finite Element problems in C++ based on
[DUNE](https://dune-project.org) and
[dune-fem](https://dune-project.org/modules/dune-fem/).

Dune-ACFem is distinguished by its ability to build PDE models by forming
algebraic expressions out of components like
```cpp
auto pdeModel = -laplaceU - f;
```
The resulting code exhibits decent performance by effectively using C++
expression templates.


## Getting started

Dune-ACFem is a [module](https://dune-project.org/modules/dune-acfem/) of the
DUNE Numerics framework, please refer to the [dune installation
instructions](https://dune-project.org/doc/installation/).
Please note that Dune-ACFem requires a compiler with decent C++14 support (e.g.
gcc>=6.3, clang>=4.0).

Example usage of Dune-ACFem can be found in our [examples
repository](https://gitlab.dune-project.org/dune-fem/acfem-examples) or within
our testsuite.

There is also
[Doxygen-Documentation](https://dune-project.org/doxygen/dune-acfem/master/)
available.


## Authors

Dune-ACFem is currently being developed and maintained at the [University of
Stuttgart](https://www.uni-stuttgart.de/) by the following
individuals:

- [Martin Alkämper](https://www.ians.uni-stuttgart.de/institute/team/Alkaemper-00003/)
- [Claus-Justus Heine](https://www.ians.uni-stuttgart.de/institute/team/Heine-00001/)
- [Stephan Hilb](https://www.ians.uni-stuttgart.de/institute/team/Hilb-00003/)

## License

Dune-ACFem is licensed under the [GNU General Public License
v2.0](https://www.gnu.org/licenses/gpl-2.0.html), see [LICENSE.md](LICENSE.md)
for details.

## Publications

- [Alkämper](http://www.ians.uni-stuttgart.de/nmh/alkaemper),
  [Langer](http://www.ians.uni-stuttgart.de/nmh/langer):
  [Using DUNE-ACFem for Non-smooth Minimization of Bounded Variation
  Functions](https://doi.org/10.11588/ans.2017.1.27475)

## Acknowledgements

- [dune-fem-howto](https://www.dune-project.org/modules/dune-fem-howto/) for
  spirit and origin
- FEM-Toolbox [Alberta](http://www.alberta-fem.de)
