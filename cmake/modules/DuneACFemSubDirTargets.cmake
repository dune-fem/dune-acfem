macro(dune_acfem_subdir_headers)
  path_variable(SUBDIRVAR ${CMAKE_CURRENT_SOURCE_DIR} PREFIX "/")
  set(${SUBDIRVAR}_HEADERS ${ARGN})
#  message(STATUS ${${SUBDIRVAR}_HEADERS})
endmacro()

macro(dune_acfem_subdir_targets)

  path_variable(SUBDIRVAR ${CMAKE_CURRENT_SOURCE_DIR} PREFIX "/")

  string(REPLACE ${PROJECT_SOURCE_DIR} "" SRC_DIR_REL ${CMAKE_CURRENT_SOURCE_DIR})
  set(${SUBDIRVAR}_dir  ${CMAKE_INSTALL_INCLUDEDIR}/${SRC_DIR_REL})

  # has the following an effect?
  set(EXTRADIST *.dox)

  install(FILES ${${SUBDIRVAR}_HEADERS} DESTINATION ${${SUBDIRVAR}_dir})
  foreach(i ${SUBDIRS})
    if(${i} STREQUAL "test")
      set(opt EXCLUDE_FROM_ALL)
    endif(${i} STREQUAL "test")
    add_subdirectory(${i} ${opt})
    unset(opt)
  endforeach(i ${SUBDIRS})

  acfem_subdir_headercheck_target(${SUBDIRVAR}_HEADERS)

  SET_DIRECTORY_PROPERTIES(PROPERTIES
    ADDITIONAL_MAKE_CLEAN_FILES "acfem-expression-profile.dump")

endmacro()
