#message(AUTHOR_WARNING "TODO: Improve module test.")
#The macro dune_module_information gets as input the directory of the dune.module file
# and sets the following variables
#${DUNE_MOD_NAME}
#${DUNE_MOD_VERSION}
#${DUNE_VERSION_MAJOR}
#${DUNE_VERSION_MINOR}
#${DUNE_VERSION_REVISION}
#${DUNE_MAINTAINER}
dune_module_information(${CMAKE_SOURCE_DIR})

include(FindMathJax)
include(ACFemAddPAPIFlags)
include(DuneACFemDoxygen)
include(DuneACFemPathVariable)
include(DuneACFemHeaderCheck)
include(DuneACFemSubDirTargets)
include(DuneACFemCamelCase)
include(DuneACFemTortureTests)
include(DuneACFemCoreFixes)
include(DuneACFemProfileOptimization)
