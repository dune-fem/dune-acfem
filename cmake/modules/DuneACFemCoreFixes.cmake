#
# what a kludge ...
#
macro(dune_acfem_core_fixes)
#  file(READ ${CONFIG_H_CMAKE_FILE} _file)
  list(REMOVE_ITEM ALL_DEPENDENCIES dune-common)
  list(REMOVE_ITEM ALL_DEPENDENCIES dune-acfem-core-fixes)
  list(INSERT ALL_DEPENDENCIES 0 dune-acfem-core-fixes)
  list(INSERT ALL_DEPENDENCIES 0 dune-common)
  if("${dune-acfem_PREFIX}" STREQUAL "")
    set(dune-acfem-core-fixes_PREFIX "${CMAKE_SOURCE_DIR}/dune/acfem/core-fixes")
  else()
    set(dune-acfem-core-fixes_PREFIX "${dune-acfem_PREFIX}/dune/acfem/core-fixes")
  endif()
  message(STATUS "Setting core-fixes prefix to ${dune-acfem-core-fixes_PREFIX}")
endmacro(dune_acfem_core_fixes)
