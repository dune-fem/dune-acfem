# - Try to find MathJax
#
# Once done this will define
#  MATHJAX_FOUND
#  DOXYGEN_MATHJAX_RELPATH
#

if(NOT DOXYGEN_MATHJAX_RELPATH)
  find_path(DOXYGEN_MATHJAX_RELPATH
    NAMES MathJax.js
    PATHS "$ENV{MATHJAX_DIR}" "/usr/share/mathjax/"
    DOC "Path to local MathJax.js"
    )
endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(MathJax
  "Local MathJax could not be found, using CDN"
  DOXYGEN_MATHJAX_RELPATH)

if(NOT DOXYGEN_MATHJAX_RELPATH)
  set(DOXYGEN_MATHJAX_RELPATH "https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.2/")
endif()

if(NOT DEFINED DOXYGEN_USE_MATHJAX AND DOXYGEN_MATHJAX_RELPATH)
    set(DOXYGEN_USE_MATHJAX YES CACHE BOOL "Use MathJax")
    if(NOT DEFINED DOXYGEN_MATHJAX_FORMAT)
      set(DOXYGEN_MATHJAX_FORMAT NativeMML CACHE STRING "MathJax format, either NativeMML or HTML-CSS")
    endif()
    if(NOT DEFINED DOXYGEN_MATHJAX_EXTENSIONS)
      set(DOXYGEN_MATHJAX_EXTENSIONS "TeX/AMSmath TeX/AMSsymbols" CACHE STRING "MathJax extensions to use")
    endif()
endif()
