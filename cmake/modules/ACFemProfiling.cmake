#  This Module simplifies the profiling using the gperftools
#
#  https://github.com/gperftools/gperftools
#
#  You need to clone the repository and follow the installation
# instructions
#  (./autogen.sh && ./configure && make && make install)
#
#
# USAGE:
#
# 1. Copy this file into your cmake modules path.
#
# 2. Add the following line to your CMakeLists.txt:
#      include(ACFemProfiling)
#
# 3. Append necessary compiler flags:
#      APPEND_PROFILING_COMPILER_FLAGS()
#
# 4. Execute
#    CPUPROFILE=profilefilename <path/to/binary> [binary_args]
#
# 4b. If this does not create an output, try
#     LD_PRELOAD=path/to/libprofiler.so CPUPROFILE=profilefilename <path/to/binary> [binary_args]
#
# 5. Run (output-format is under pprof --help)
#    pprof --your-output-format <path/to/binary> profilefilename ( > profilefile.output-format )
#
#




include(CMakeParseArguments)

# Check prereqs
find_program(PPROF_PATH NAMES pprof google-pprof)
find_library(PROFILER_PATH profiler)

if(NOT PPROF_PATH)
    message(FATAL_ERROR "pprof not found! Aborting...")
endif() # NOT PPROF_PATH

if(NOT PROFILER_PATH)
    message(FATAL_ERROR "libprofiler not found! Aborting...")
endif() # NOT PROFILER_PATH

get_filename_component(PROFILER_LIBDIR ${PROFILER_PATH} DIRECTORY)

set(PROFILING_COMPILER_FLAGS "-g3 -O3 -L${PROFILER_LIBDIR} -lprofiler"
    CACHE INTERNAL "")

function(APPEND_PROFILING_COMPILER_FLAGS)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${PROFILING_COMPILER_FLAGS}" PARENT_SCOPE)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${PROFILING_COMPILER_FLAGS}" PARENT_SCOPE)
    message(STATUS "Appending profiling compiler flags: ${PROFILING_COMPILER_FLAGS}")
endfunction() # APPEND_PROFILING_COMPILER_FLAGS
